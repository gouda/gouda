from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from fabric.api import env
import os

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

env.hosts = [
    '172.17.8.101',
    '172.17.8.102',
    '172.17.8.103',
]
env.roledefs.update({
    'primary': [env.hosts[0]]
})
env.user = 'core'
env.forward_agent = True
env.app_root = '/opt/gouda/'

env.docker_auth_filename = os.path.join(PROJECT_ROOT, 'deploy/docker/auths/development.json')
env.docker_host = 'tcp://172.17.8.101:2375'
env.docker_registry_host = 'ez3softworks.com:5000'

docker_path = os.path.join(PROJECT_ROOT, 'deploy/docker/')
fleet_path = os.path.join(PROJECT_ROOT, 'deploy/fleet/')
env.services = {
    'redis': {
        'fleet': {
            'id': 6379,
            'unit_file_path': os.path.join(fleet_path, 'redis@.service'),
            'remote_unit_file_path': os.path.join(env.app_root, 'redis@.service'),
        },
        'docker': {
            'dockerfile_path': os.path.join(docker_path, 'redis/Dockerfile'),
            'build_context_path': os.path.join(docker_path, 'redis'),
            'registry_image_name': '%s/redis' % env.docker_registry_host,
        }
    },
    'postgres': {
        'fleet': {
            'id': 5432,
            'unit_file_path': os.path.join(fleet_path, 'postgres@.service'),
            'remote_unit_file_path': os.path.join(env.app_root, 'postgres@.service'),
        },
        'docker': {
            'dockerfile_path': os.path.join(docker_path, 'postgres/Dockerfile'),
            'build_context_path': os.path.join(docker_path, 'postgres'),
            'registry_image_name': '%s/postgres' % env.docker_registry_host,
        }
    },
    'rabbitmq': {
        'fleet': {
            'id': 5672,
            'unit_file_path': os.path.join(fleet_path, 'rabbitmq@.service'),
            'remote_unit_file_path': os.path.join(env.app_root, 'rabbitmq@.service'),
        },
        'docker': {
            'dockerfile_path': os.path.join(docker_path, 'rabbitmq/Dockerfile'),
            'build_context_path': os.path.join(docker_path, 'rabbitmq'),
            'registry_image_name': '%s/rabbitmq' % env.docker_registry_host,
        }
    },
    # 'redis-discovery': {
    #     'fleet': {
    #         'id': 6379,
    #         'unit_file_path': os.path.join(fleet_path, 'redis-discovery@.service'),
    #         'remote_unit_file_path': os.path.join(env.app_root, 'redis-discovery@.service'),
    #     }
    # }
}

for name, service in env.services.iteritems():
    file_name = os.path.basename(str(service.get('fleet').get('remote_unit_file_path')))
    service.get('fleet')['unit_name'] = file_name.replace('@', '@%s' % str(service.get('fleet').get('id')))
