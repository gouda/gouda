from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import os
from fabric.api import settings, env, run, cd, shell_env, local, sudo, lcd, task

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

# Build host
env.build_host = '127.0.0.1:2222'
env.build_host_user = 'vagrant'
env.build_host_password = 'vagrant'
env.build_host_key_filename = os.path.join(PROJECT_ROOT, '.vagrant/machines/default/virtualbox/private_key')
env.build_host_docker_kwargs = {
    # 'DOCKER_HOST': 'tcp://127.0.0.1:2375',
    # 'DOCKER_TLS': 'no',
    'COMPOSE_HTTP_TIMEOUT': '300',
}
env.remote_docker_kwargs = env.build_host_docker_kwargs
env.hosts = [
    env.build_host,
]

env.is_production = False

home_directory = '/Users/chukwuemeka/'
env.roledefs.update({
    'primary': [env.hosts[0]]
})
env.user = 'vagrant'
env.password = 'vagrant'
env.forward_agent = True
env.app_name = 'gouda'
env.app_root = '/opt/%s/' % env.app_name
env.key_filename = os.path.join(PROJECT_ROOT, '.vagrant/machines/default/virtualbox/private_key')
env.public_key_filename = os.path.join(home_directory, '.ssh/id_rsa.pub')
env.systemd_service_file_path = os.path.join(PROJECT_ROOT, 'deploy/systemd/gouda.development.service')

env.postgres_login = 'postgres'
env.postgres_password = 'f3eYUBU7WeST8xeTAtrA7EVujaDRe6hu'
env.postgres_db_name = 'gouda'

env.docker_auth_filename = os.path.join(PROJECT_ROOT, 'deploy/docker/auths/development.json')
env.docker_host = 'tcp://%s' % env.build_host
env.docker_registry_host = 'ez3softworks.com:5000'
env.docker_kwargs = {
    'DOCKER_HOST': env.docker_host,
    'DOCKER_TLS': 'no',
    'COMPOSE_HTTP_TIMEOUT': '300',
}
env.compose_file_path = os.path.join(PROJECT_ROOT, 'deploy/compose/development.yml')

env.digital_ocean_api_token = 'edd49018f5de01646212a86f1e731f3673741509b57c6dc34bf0272ebb7b9cd4'

docker_path = os.path.join(env.app_root, 'deploy/docker/')
fleet_path = os.path.join(PROJECT_ROOT, 'deploy/fleet/')


def get_image_tag(service, branch, tag='latest', app_name=env.app_name):
    branch = os.path.basename(branch)
    return '%s-%s:%s' % (app_name, service, branch)


def get_services(branch, tag='latest'):
    return {
        'redis': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'redis/Dockerfile'),
                'build_context_path': os.path.join(docker_path, 'redis'),
                # 'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('redis', branch, tag)),
                'image_tag': '%s/redis' % (env.docker_registry_host,),
            }
        },
        'postgres': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'postgres/Dockerfile'),
                'build_context_path': os.path.join(docker_path, 'postgres'),
                'image_tag': '%s/postgres' % (env.docker_registry_host,),
                # 'image_tag': '%s/postgres' % (env.docker_registry_host, ),
            }
        },
        'rabbitmq': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'rabbitmq/Dockerfile'),
                'build_context_path': os.path.join(docker_path, 'rabbitmq'),
                'image_tag': '%s/rabbitmq' % (env.docker_registry_host,),
                # 'image_tag': '%s/rabbitmq' % (env.docker_registry_host, ),
            }
        },
        'python': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'python/Dockerfile'),
                'build_context_path': env.app_root,
                'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('python', branch, tag)),
            }
        },
        'thumbor': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'thumbor/Dockerfile'),
                'build_context_path': os.path.join(env.app_root, 'deploy/docker/thumbor'),
                'image_tag': '%s/thumbor' % (env.docker_registry_host,),
                # 'image_tag': '%s/thumbor' % (env.docker_registry_host, ),
            }
        },
        'haproxy': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'haproxy/Dockerfile'),
                'build_context_path': os.path.join(env.app_root, 'deploy/docker/haproxy'),
                'image_tag': '%s/haproxy' % (env.docker_registry_host,),
                # 'image_tag': '%s/haproxy' % (env.docker_registry_host, ),
            }
        },
        # 'web': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'web/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('web', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
        # 'worker': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'worker/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('worker', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
        # 'celery': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'celery/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('celery', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
        # 'gulp': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'gulp/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('gulp', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
    }
env.get_services = get_services
