from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
from fabric.api import env
import os

PROJECT_ROOT = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

env.droplet_name = 'kalfu'

env.settings_module = 'gouda.settings.test'
env.base_settings_file_path = os.path.join(PROJECT_ROOT, 'gouda/gouda/settings/constants.py')
env.deployment_settings_module = 'gouda.settings.deploy.test'

env.aws_cloudfront_distribution_id = 'E1X0ZFQKACHY1N'
env.aws_access_key_id = 'AKIAIKUAAHMASYMUYDMA'
env.aws_secret_access_key = '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'

home_directory = '/Users/chukwuemeka/'
env.key_filename = os.path.join(home_directory, '.ssh/id_rsa')
env.public_key_filename = os.path.join(home_directory, '.ssh/id_rsa.pub')
env.is_production = False
env.user = 'gouda'
env.forward_agent = True
env.app_name = 'gouda'
env.app_root = '/opt/%s/' % env.app_name
env.systemd_service_file_path = os.path.join(PROJECT_ROOT, 'deploy/systemd/gouda.test.service')

env.postgres_login = 'postgres'
env.postgres_password = 'f3eYUBU7WeST8xeTAtrA7EVujaDRe6hu'
env.postgres_db_name = 'gouda'

do_use_build_server = False
env.build_compose_file_path = os.path.join(PROJECT_ROOT, 'deploy/compose/build.yml')
if do_use_build_server:
    env.build_host = 'ez3softworks.com:22'
    env.build_host_user = 'flagship'
    env.build_host_password = 'flagship123FLAGSHIP!@#'
    env.build_host_key_filename = os.path.join(home_directory, '.ssh/id_rsa')
    env.build_host_docker_kwargs = {
        'COMPOSE_HTTP_TIMEOUT': '300',
    }
else:
    env.build_host = '127.0.0.1:2222'
    env.build_host_user = 'vagrant'
    env.build_host_password = 'vagrant'
    env.build_host_key_filename = os.path.join(PROJECT_ROOT, '.vagrant/machines/default/virtualbox/private_key')
    env.build_host_docker_kwargs = {
        'DOCKER_HOST': 'tcp://127.0.0.1:2375',
        'DOCKER_TLS': 'no',
        'COMPOSE_HTTP_TIMEOUT': '300',
    }

env.docker_auth_filename = os.path.join(PROJECT_ROOT, 'deploy/docker/auths/development.json')
env.docker_registry_host = 'ez3softworks.com:5000'
env.remote_docker_kwargs = {
    'COMPOSE_HTTP_TIMEOUT': '300',
}
env.compose_file_path = os.path.join(PROJECT_ROOT, 'deploy/compose/test.yml')

env.digital_ocean_api_token = 'edd49018f5de01646212a86f1e731f3673741509b57c6dc34bf0272ebb7b9cd4'

docker_path = os.path.join(env.app_root, 'deploy/docker/')


def get_image_tag(service, branch, tag='latest', app_name=env.app_name):
    branch = os.path.basename(branch)
    return '%s-%s:%s' % (app_name, service, branch)


def get_services(branch, tag='latest'):
    return {
        'redis': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'redis/Dockerfile'),
                'build_context_path': os.path.join(docker_path, 'redis'),
                # 'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('redis', branch, tag)),
                'image_tag': '%s/%s-redis' % (env.docker_registry_host, env.app_name),
            }
        },
        'postgres': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'postgres/Dockerfile'),
                'build_context_path': os.path.join(docker_path, 'postgres'),
                'image_tag': '%s/%s-postgres' % (env.docker_registry_host, env.app_name),
                # 'image_tag': '%s/postgres' % (env.docker_registry_host, ),
            }
        },
        'rabbitmq': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'rabbitmq/Dockerfile'),
                'build_context_path': os.path.join(docker_path, 'rabbitmq'),
                'image_tag': '%s/%s-rabbitmq' % (env.docker_registry_host, env.app_name),
                # 'image_tag': '%s/rabbitmq' % (env.docker_registry_host, ),
            }
        },
        'python': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'python/Dockerfile'),
                'build_context_path': env.app_root,
                'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('python', branch, tag)),
            }
        },
        'thumbor': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'thumbor/Dockerfile'),
                'build_context_path': os.path.join(env.app_root, 'deploy/docker/thumbor'),
                'image_tag': '%s/%s-thumbor' % (env.docker_registry_host, env.app_name),
                # 'image_tag': '%s/thumbor' % (env.docker_registry_host, ),
            }
        },
        'haproxy': {
            'docker': {
                'dockerfile_path': os.path.join(docker_path, 'haproxy/Dockerfile'),
                'build_context_path': os.path.join(env.app_root, 'deploy/docker/haproxy'),
                'image_tag': '%s/%s-haproxy' % (env.docker_registry_host, env.app_name),
                # 'image_tag': '%s/haproxy' % (env.docker_registry_host, ),
            }
        },
        # 'web': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'web/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('web', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
        # 'worker': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'worker/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('worker', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
        # 'celery': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'celery/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('celery', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
        # 'gulp': {
        #     'docker': {
        #         'dockerfile_path': os.path.join(docker_path, 'gulp/Dockerfile'),
        #         'build_context_path': env.app_root,
        #         'image_tag': '%s/%s' % (env.docker_registry_host, get_image_tag('gulp', branch, tag)),
        #         'base_service': 'python'
        #     }
        # },
    }

env.get_services = get_services
