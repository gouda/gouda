FROM ubuntu:xenial

MAINTAINER Chukwuemeka Ezekwe <emeka@ez3softworks.com>

RUN apt-get update --fix-missing
RUN apt-get install -y build-essential cmake git pkg-config
RUN apt-get install -y libjpeg8-dev libtiff5-dev libjasper-dev libpng12-dev
RUN apt-get install -y libgtk2.0-dev
RUN apt-get install -y libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
RUN apt-get install -y libatlas-base-dev gfortran
RUN apt-get install -y python python-dev
RUN apt-get install -y libssl-dev
RUN apt-get install -y libcurl4-openssl-dev
RUN apt-get install -y vim

RUN apt-get install -y python-pip
RUN pip install --upgrade pip
RUN pip install numpy # Necessary...?!?!?!

# Download opencv repo
WORKDIR "/root"
RUN git clone https://github.com/Itseez/opencv.git
WORKDIR "/root/opencv"
RUN git checkout 3.1.0

# Download opencv contrib
WORKDIR "/root"
RUN git clone https://github.com/Itseez/opencv_contrib.git
WORKDIR "/root/opencv_contrib"
RUN git checkout 3.1.0

# Install opencv
WORKDIR "/root/opencv"
RUN mkdir build
WORKDIR "/root/opencv/build"
RUN cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D INSTALL_C_EXAMPLES=OFF -D INSTALL_PYTHON_EXAMPLES=ON -D OPENCV_EXTRA_MODULES_PATH=/root/opencv_contrib/modules -D BUILD_EXAMPLES=ON ..
RUN make
RUN make install

# Instal python packages
ADD requirements.txt /opt/thumbor/
RUN pip install -r /opt/thumbor/requirements.txt

# Patches and conf
ADD . /opt/thumbor/
#ADD opencv_engine/engine.py /usr/local/lib/python2.7/dist-packages/opencv_engine/engine.py

WORKDIR "/opt/thumbor/"

EXPOSE 8888

CMD ["thumbor", "-l", "debug", "-c", "/opt/thumbor/thumbor.conf"]