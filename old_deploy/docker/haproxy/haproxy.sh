#!/usr/bin/env bash

python /opt/haproxy/create_haproxy_cfg.py /opt/haproxy/haproxy.cfg.template /opt/haproxy/haproxy.cfg
cat /opt/haproxy/haproxy.cfg
haproxy -d -V -db -f /opt/haproxy/haproxy.cfg
