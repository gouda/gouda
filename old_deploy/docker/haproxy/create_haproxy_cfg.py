from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import argparse
import os
import re

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser(description='Create HAProxy cfg')
    argument_parser.add_argument('template_file_path', action='store')
    argument_parser.add_argument('output_file_path', action='store')

    args = argument_parser.parse_args()

    # Thumbor backend config
    thumbor_frontend_config = """
frontend thumbor
     bind *:8888
     bind *:8889 ssl crt /opt/haproxy/ssl/shotzu.com.pem
     mode http
     default_backend thumbor_nodes
     """

    # Build thumbor config
    thumbors = {}
    for k in os.environ.keys():
        match = re.match('THUMBOR_(\d+)_PORT_8888_TCP_ADDR', k)
        if match:
            thumbor = thumbors.get(match.group(1), dict())
            thumbor['ip_address'] = os.environ.get(k)
            thumbors[match.group(1)] = thumbor

        match = re.match('THUMBOR_(\d+)_PORT_8888_TCP_PORT', k)
        if match:
            thumbor = thumbors.get(match.group(1), dict())
            thumbor['port'] = os.environ.get(k)
            thumbors[match.group(1)] = thumbor

    thumbor_backend_config = """
backend thumbor_nodes
     reqrep ^([^\ ]*\ /)thumbor[/]?(.*)     \\1\\2
     timeout server 5m
     mode http
     balance roundrobin
     option forwardfor

     http-request set-header X-Forwarded-Port %[dst_port]
     http-request add-header X-Forwarded-Proto https if { ssl_fc }

     http-response add-header Access-Control-Allow-Origin *

     option httpchk HEAD /healthcheck HTTP/1.1\\r\\nHost:localhost
     """
    thumbor_server_config = ['server thumbor0%s %s:%s check' % (k, v.get('ip_address'), v.get('port')) for k, v in thumbors.iteritems()]
    thumbor_backend_config += '\n     ' + '\n     '.join(thumbor_server_config)

    with open(args.template_file_path) as template_file:
        with open(args.output_file_path, 'w') as output_file:
            for line in template_file:
                output_file.write(line)
            output_file.writelines(thumbor_frontend_config)
            output_file.writelines(thumbor_backend_config)
