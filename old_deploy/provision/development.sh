#!/usr/bin/env bash

sudo apt-get update
sudo apt-get install -y --force-yes linux-image-extra-$(uname -r)
sudo apt-get install -y --force-yes apt-transport-https ca-certificates
sudo apt-get install -y --force-yes htop
sudo apt-get install -y --force-yes python-pip
sudo apt-get install -y --force-yes curl
sudo apt-get install -y --force-yes fail2ban

sudo echo "deb https://apt.dockerproject.org/repo ubuntu-wily main" > /etc/apt/sources.list.d/docker.list
sudo apt-get update
sudo apt-get purge lxc-docker
sudo apt-get install -y --force-yes docker-engine
#sudo curl -sSL https://get.docker.com/ | sh
sudo pip install -U docker-compose
mkdir -p /etc/systemd/system/docker.service.d
cp /vagrant/deploy/systemd/docker.development.conf /etc/systemd/system/docker.service.d/docker.conf
systemctl stop docker
systemctl daemon-reload
systemctl start docker

usermod -aG docker vagrant
mkdir -p /opt/gouda/
chown vagrant /opt/gouda/

#echo """description "gouda"
#    author "Me"
#    start on filesystem and started docker
#    stop on runlevel [!2345]
#    script
#      /usr/local/bin/docker-compose -f /opt/gouda/docker-compose.yml --force-recreate up
#    end script
#""" > /etc/init/gouda.conf

# Create gouda service
#cp /vagrant/deploy/systemd/gouda.service /etc/systemd/system/gouda.service
#systemctl daemon-reload
#systemctl start gouda

# Copy docker auth
mkdir -p /home/vagrant/.docker/
cp -rfv /vagrant/deploy/docker/auths/development.json /home/vagrant/.docker/config.json
