#!/usr/bin/env bash

apt-get update
apt-get upgrade -y
apt-get install -y htop
apt-get install -y python-pip
apt-get install -y curl
apt-get install -y fail2ban
curl -sSL https://get.docker.com/ | sh
pip install -U docker-compose
usermod -aG docker cezekwe
mkdir -p /opt/gouda/
chown cezekwe /opt/gouda/

# Host
echo "jollof" > /etc/hostname
hostname -F /etc/hostname
ip=$(ip addr show eth0 | grep -Po 'inet \K[\d.]+')
echo "$ip jollof.thecageapp.com jollof" >> /etc/hosts

# Firewall
ufw allow ssh
ufw allow 80/tcp
ufw allow 443/tcp
ufw --force enable

# Disable root login
sed -i.bak "s/^PermitRootLogin.*/PermitRootLogin no/g" /etc/ssh/sshd_config
systemctl restart ssh

# Change docker storage driver
#systemctl stop docker
#rm /var/lib/docker -rf
#mkdir /etc/systemd/system/docker.service.d
#echo """
#[Service]
#ExecStart=
#ExecStart=/usr/bin/docker daemon -H fd:// --storage-driver overlay
#""" > /etc/systemd/system/docker.service.d/overlay.conf
#systemctl daemon-reload
#systemctl start docker