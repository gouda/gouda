// Include gulp
var gulp = require('gulp');
// Include plugins
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var sass = require('gulp-ruby-sass');
var compass = require('gulp-compass');
var exec = require('child_process').exec;
var argv = require('yargs').argv;
var gutil = require('gulp-util');
var templateCache = require('gulp-angular-templatecache');
var runSequence = require('run-sequence');
var plumber = require('gulp-plumber');
var path = require('path');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var watchify = require('watchify');
var assign = require('lodash.assign');
var src = path.join(__dirname, 'gouda/gouda/');
var dest = path.join(__dirname, 'gouda/gouda/static/');
var sourcemaps = require('gulp-sourcemaps');
var duration = require('gulp-duration');
var bro = require('gulp-bro');
var ext_replace = require('gulp-ext-replace');

var localStaticURL = '/static';
//var testStaticURL = 'https://s3.amazonaws.com/com.theislandcrew.gouda.test/static';
//var testStaticURL = '/static';
//var productionStaticURL = '/static';


gulp.task('watch', function() {
    // Watch .scss files
    gulp.watch(src + 'static/sass/**/*.scss', function(){
        runSequence('sass');
    });

    gulp.watch(src + 'static/js/react/**/*.jsx', function(){
        runSequence('reactjs');
    });
});


//gulp.task('watch', function() {
//    // Watch .js files
//    gulp.watch(src + '*/static/*/js/**/*.js', function(){
//        runSequence('angularjs');
//    });
//    gulp.watch(src + '*/static/*/js/**/*.html', function(){
//        runSequence('angularjs');
//    });
//    // Watch .scss files
//    gulp.watch(src + '*/static/*/sass/*.scss', function(){
//        runSequence('sass');
//    });
//    gulp.watch(src + 'static/sass/*.scss', function(){
//        runSequence('sass');
//    });
//
//    //// Watch .js files
//    //gulp.watch(src + '*/static/*/js/**/*.js', ['angularjs']);
//    //gulp.watch(src + '*/static/*/js/**/*.html', ['angularjs']);
//    //// Watch .scss files
//    //gulp.watch(src + '*/static/*/sass/*.scss', ['sass']);
//    //gulp.watch(src + 'static/sass/*.scss', ['sass']);
//});

gulp.task('openlayers', function(){
    var src = [
        'bower_components/openlayers/build/OpenLayers.js'
    ];
    gulp.src(src, {base: 'bower_components/'})
        .pipe(concat('OpenLayers.js'))
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())
        .pipe(gulp.dest(dest + '/gis/js'));
    return gulp.src('bower_components/openlayers/theme/default/*/*')
        .pipe(gulp.dest(dest + '/gis/theme/default/'));
});

function registerReactTask(inputFilePath, outputDirectoryPath) {
    var taskName = 'reactjs__' + inputFilePath;
    gulp.task(taskName, function () {
        browserify({
            entries: inputFilePath,
            extensions: ['.jsx'],
            debug: true
        })
            .transform('babelify', {presets: ['es2015', 'react']})
            .bundle()
            .on('error', gutil.log)
            .pipe(source(path.parse(inputFilePath).name + '.js'))
            .pipe(gulp.dest(outputDirectoryPath));
    });
    return taskName;
}

function registerAngularTask(inputDirectoryPath, appName) {
    var taskName = 'angular__' + inputDirectoryPath;
    gulp.task(taskName, function(){
        // Create templates.js
        var module = 'gouda.' + appName + '.templates';
        if (appName == '') {
            module = 'gouda.templates';
        }
        var templateCacheOptions = {
            module: module,
            standalone: true,
            root: appName
        };
        return gulp.src(inputDirectoryPath + 'partials/**/*.html')
            .pipe(templateCache('templates.js', templateCacheOptions))
            .pipe(gulp.dest(inputDirectoryPath + 'js/'));

        //// Uglify some thangs...
        //return gulp.src(inputDirectoryPath + 'js/**/*.js')
        //    .pipe(rename({suffix: '.min'}))
        //    .pipe(uglify({mangle: false}))
        //    .pipe(gulp.dest(inputDirectoryPath + 'dist/'));
    });
    return taskName;
}

function registerCompassTask(projectPath, appName) {
    var taskName = 'compass__' + projectPath;
    gulp.task(taskName, function() {
        var httpPath = '';
        if (typeof argv.httpPath != "undefined") {
            httpPath = argv.httpPath + '/' + appName;
        } else {
            httpPath = localStaticURL + '/' + appName;
        }
        gulp.src(projectPath + '*.scss')
            .pipe(plumber({
                errorHandler: function (error) {
                    gutil.log(error);
                    gutil.log(error.message);
                    this.emit('end');
                }}))
            .pipe(compass({
                project: projectPath,
                //config_file: path.join(projectPath, 'config.rb'),
                css: 'css',
                sass: 'sass',
                http_path: httpPath
            }))
    }).on('error', function(error) {
        //  gutil.log("WATH!!!");
        //gutil.log(error);
        this.emit('end');
    });
    return taskName;
}

gulp.task('sass', [
    //registerCompassTask(src + '/contacts/static/contacts/', 'contacts'),
    registerCompassTask(src + 'static/', ''),
    //registerCompassTask(src + '/listings/static/listings/', 'listings'),
    //registerCompassTask(src + '/profiles/static/profiles/', 'profiles'),
    //registerCompassTask(src + '/customers/static/customers/', 'customers'),
    //registerCompassTask(src + '/photographers/static/photographers/', 'photographers')
]);

gulp.task('angularjs', [
    //registerAngularTask(src + 'static/js/app/', '')
]);


//gulp.task('reactjs', [
//    registerReactTask(src + 'static/js/react/pages/profiles/BookingDetail.jsx', dest + 'js/react/dist/pages/profiles/'),
//    registerReactTask(src + 'static/js/react/pages/profiles/ShootDetail.jsx', dest + 'js/react/dist/pages/profiles/'),
//    registerReactTask(src + 'static/js/react/pages/photographers/ScheduleBooking.jsx', dest + 'js/react/dist/pages/photographers/'),
//    registerReactTask(src + 'static/js/react/pages/photographers/GetBulkBookingFile.jsx', dest + 'js/react/dist/pages/photographers/'),
//    registerReactTask(src + 'static/js/react/pages/photographers/GetFoodBookingDetails.jsx', dest + 'js/react/dist/pages/photographers/'),
//    registerReactTask(src + 'static/js/react/pages/photographers/CreateCustomer.jsx', dest + 'js/react/dist/pages/photographers/')
//]);

var isRunning = false;
gulp.task('collectstatic', function(){
    function callback(error, stdout, stderr) {
        console.log(stdout);
        isRunning = false;
    }
    if (isRunning) {
        return;
    }
    isRunning = true;
    exec('/Users/chukwuemeka/Documents/Projects/python/gouda-web/bin/python /Users/chukwuemeka/Documents/Projects/python/gouda-web/gouda/manage.py collectstatic --settings gouda.settings.default --noinput',
        {
            'PYTHONPATH': '/Users/chukwuemeka/Documents/Projects/python/gouda-web/gouda/'
        }, callback);
});

gulp.task('reactjs', function(){
    gulp.src([
        src + 'static/js/react/pages/profiles/Dashboard.jsx',
        src + 'static/js/react/pages/profiles/CustomerUpdate.jsx',
        src + 'static/js/react/pages/profiles/BookingList.jsx',
        src + 'static/js/react/pages/profiles/BookingDetail.jsx',
        src + 'static/js/react/pages/profiles/ShootDetail.jsx',
        src + 'static/js/react/pages/photographers/CreateFoodBooking.jsx',
        src + 'static/js/react/pages/photographers/ScheduleBooking.jsx',
        src + 'static/js/react/pages/photographers/GetBulkBookingFile.jsx',
        src + 'static/js/react/pages/photographers/GetFoodBookingDetails.jsx',
        src + 'static/js/react/pages/photographers/BraintreeMerchantAccountVerification.jsx',
        src + 'static/js/react/pages/photographers/CreateCustomer.jsx'
    ], {base: src + 'static/js/react/', basename: src + 'static/js/react/', read: false})
        .pipe(bro({
            transform: [babelify.configure({ presets: ['es2015', 'react', 'stage-0'] })]
        }))
        .pipe(ext_replace('.js'))
        .pipe(gulp.dest(dest + 'js/react/dist'));
});

//var customOptions = {
//    entries: [
//        src + 'static/js/react/pages/profiles/BookingDetail.jsx',
//        src + 'static/js/react/pages/profiles/ShootDetail.jsx',
//        src + 'static/js/react/pages/photographers/ScheduleBooking.jsx',
//        src + 'static/js/react/pages/photographers/GetBulkBookingFile.jsx',
//        src + 'static/js/react/pages/photographers/GetFoodBookingDetails.jsx',
//        src + 'static/js/react/pages/photographers/CreateCustomer.jsx'
//    ],
//    debug: true
//};
//var options = assign({}, watchify.args, customOptions);
//var reactify = watchify(browserify(options)).transform('babelify', {presets: ['es2015', 'react']});
//gulp.task('reactjs', reactTask);
//reactify.on('update', reactTask); // on any dep update, runs the bundler
//reactify.on('log', gutil.log); // output build logs to terminal
//function reactTask() {
//    return reactify.bundle()
//        .on('error', gutil.log.bind(gutil, 'Browserify Error'))
//        .pipe(source('bundle.js'))
//        // optional, remove if you don't need to buffer file contents
//        .pipe(vinylBuffer())
//        // optional, remove if you dont want sourcemaps
//        .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
//        // Add transformation tasks to the pipeline here.
//        .pipe(sourcemaps.write('./')) // writes .map file
//        .pipe(gulp.dest(dest + 'js/react/dist'));
//}

//gulp.task('reactjs', function(){
//    var entries = [
//        {
//            src: src + 'static/js/react/pages/profiles/BookingDetail.jsx',
//            dest: dest + 'js/react/dist/pages/profiles/BookingDetail.js'
//        },
//    ];
//
//    var tasks = entries.map(function(entry){
//        var reactify = browserify({
//            entries: [entry.src],
//            cache: {},
//            packageCache: {},
//            plugin: [watchify],
//            debug: true,
//
//        });
//        reactify.on('update', reactTask);
//        reactify.on('log', gutil.log);
//        reactTask();
//        function reactTask() {
//            return reactify
//                .transform('babelify', {presets: ['es2015', 'react']})
//                .bundle()
//                .on('error', gutil.log.bind(gutil, 'Browserify Error'))
//                .pipe(source(path.basename(entry.dest)))
//                // optional, remove if you don't need to buffer file contents
//                .pipe(vinylBuffer())
//                // optional, remove if you dont want sourcemaps
//                .pipe(sourcemaps.init({loadMaps: true})) // loads map from browserify file
//                // Add transformation tasks to the pipeline here.
//                .pipe(sourcemaps.write('./')) // writes .map file
//                .pipe(gulp.dest(path.dirname(entry.dest)));
//        }
//        return reactTask();
//    });
//    return es.merge.apply(null, tasks);
//});

//gulp.task('reactjs', function () {
//  browserify({
//    entries: src + 'static/js/react/index.jsx',
//    extensions: ['.jsx'],
//    debug: true
//  })
//  .transform("babelify", {presets: ["es2015", "react"]})
//  .bundle()
//  .on('error', gutil.log)
//  .pipe(source('index.js'))
//  .pipe(gulp.dest(dest + 'js/react/dist/'));
//});

// Default Task
gulp.task('default', function() {
        runSequence(['watch', 'sass', 'angularjs', 'reactjs']);
    }
);

// For deployment
gulp.task('dist', ['sass', 'openlayers', 'angularjs', 'reactjs']);
