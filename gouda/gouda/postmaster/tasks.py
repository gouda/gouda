from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from gouda.celeryapp import app
from django.core import management


@app.task
def process_mailbox():
    management.call_command('process_mailbox')
