from __future__ import absolute_import

import random
import math

from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from gouda.postmaster.models import User
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from gouda.utils import get_lorem_text


def create_messages(num_messages, num_threads, num_profiles=2):
    create_profiles(num_profiles)
    messages = list()
    uid = 0
    for i in range(num_threads):
        thread = Thread.objects.create()
        sender = random.choice(User.objects.all())
        recipient = random.choice(User.objects.exclude(pk=sender.pk))
        thread.users.add(sender)
        thread.users.add(recipient)
        for j in range(num_messages):
            messages.append(Message.objects.create(**{
                'thread': thread,
                'uid': uid,
                'body': get_lorem_text(random.randrange(30, 100), 'w'),
                'subject': get_lorem_text(random.randrange(1, 5), 'w'),
                'sender': random.choice([sender, recipient]),
            }))
            uid += 1
    return messages


def tear_down_messages():
    User.objects.all().delete()
    Message.objects.all().delete()
    Thread.objects.all().delete()
    tear_down_profiles()
