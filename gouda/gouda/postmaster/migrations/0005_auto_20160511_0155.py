# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def noop(apps, schema_editor):
    pass


def set_profiles(apps, schema_editor):
    Profile = apps.get_model("profiles", "Profile")
    User = apps.get_model("postmaster", "User")
    for user in User.objects.all():
        try:
            profile = Profile.objects.get(user__email_address=user.email_address)
            user.profile = profile
            user.save()
        except Profile.DoesNotExist:
            pass


class Migration(migrations.Migration):

    dependencies = [
        ('postmaster', '0004_auto_20160511_0143'),
        ('profiles', '0004_remove_profile_type'),
    ]

    operations = [
        migrations.RunPython(
            set_profiles,
            reverse_code=noop,
        ),
    ]
