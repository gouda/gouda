# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def noop(apps, schema_editor):
    pass


def delete_users(apps, schema_editor):
    User = apps.get_model("postmaster", "User")
    User.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_remove_profile_type'),
        ('postmaster', '0003_auto_20160511_0055'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='full_name',
        ),
        migrations.RunPython(
            delete_users,
            reverse_code=noop
        ),
        migrations.AddField(
            model_name='user',
            name='profile',
            field=models.OneToOneField(related_name='postmaster_user', null=True, to='profiles.Profile'),
        ),
    ]
