# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postmaster', '0008_remove_message_recipient'),
    ]

    operations = [
        migrations.AddField(
            model_name='thread',
            name='users',
            field=models.ManyToManyField(related_name='threads', to='postmaster.User'),
        ),
    ]
