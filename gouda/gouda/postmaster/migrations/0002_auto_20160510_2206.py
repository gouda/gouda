# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postmaster', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='subject',
            field=models.CharField(default='', max_length=998, blank=True),
        ),
        migrations.AlterField(
            model_name='message',
            name='uid',
            field=models.IntegerField(null=True),
        ),
    ]
