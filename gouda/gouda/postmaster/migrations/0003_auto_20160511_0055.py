# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postmaster', '0002_auto_20160510_2206'),
    ]

    operations = [
        migrations.RenameField(
            model_name='user',
            old_name='display_name',
            new_name='full_name',
        ),
    ]
