from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.core import mail
from django.test import TestCase
from freezegun import freeze_time
from gouda import signing
from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from gouda.postmaster.models import User
from gouda.postmaster.salt import REPLY_TO
from gouda.profiles.models import GenderType
from gouda.profiles.models import Profile
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import get_profile_data
from unittest import skip

EMAIL_DOMAIN = getattr(settings, 'POSTMASTER_EMAIL_DOMAIN')
FROM_EMAIL = getattr(settings, 'POSTMASTER_FROM_EMAIL')
BCC = getattr(settings, 'POSTMASTER_BCC')


class UserTestCase(TestCase):
    def test_init(self):
        User()

    def test_create_user(self):
        profile = Profile.objects.create(**get_profile_data({
            'full_name': 'Sterling Archer',
            'gender': GenderType.MALE
        }))
        user = profile.postmaster_user
        self.assertIsNotNone(user)
        self.assertIsNotNone(user.anonymous_email_address)
        self.assertFalse(user.anonymous_email_address == '')
        self.assertTrue(EMAIL_DOMAIN in user.anonymous_email_address)


class MessageTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(MessageTestCase, cls).setUpClass()
        create_profiles(10)
        #
        # User.objects.create(
        #     email_address='sterlingarcher@gmail.com',
        #     display_name='Sterling Archer'
        # )
        # User.objects.create(
        #     email_address='lanakane@gmail.com',
        #     display_name='Lana Kane'
        # )
        # User.objects.create(
        #     email_address='cyrilfiggis@gmail.com',
        #     display_name='Cyril Figgis'
        # )
        # User.objects.create(
        #     email_address='pampoovey@gmail.com',
        #     display_name='Pam Poovey'
        # )

    @classmethod
    def tearDownClass(cls):
        super(MessageTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def setUp(self):
        mail.outbox = list()

    def tearDown(self):
        Message.objects.all().delete()

    def test_init(self):
        Message()

    @freeze_time("2012-01-14")
    def test_create_message(self):
        users = User.objects.all()
        message = Message.objects.create(
            uid=0,
            subject='LANANNNANANAAAA!!!',
            body='DANGER ZONE!!!',
            sender=users[0],
            thread=Thread.objects.create()
        )

    # def test_send_message_from_unknown_user(self):
    #     users = User.objects.all()
    #     recipient = users[0]
    #     sender = User.objects.create(
    #         email_address='cheryltunt@gmail.com',
    #     )
    #     message = Message.objects.create(
    #         uid=0,
    #         subject='LANANNNANANAAAA!!!',
    #         body='DANGER ZONE!!!',
    #         sender=sender,
    #         thread=Thread.objects.create()
    #     )
    #     message.recipients.add(recipient)
    #     message.send()
    #
    #     # Make sure email gets sent
    #     self.assertEqual(len(mail.outbox), 1)
    #     message = mail.outbox[0]
    #     self.assertEqual(message.from_email, '%s' % sender.anonymous_email_address)
    #     self.assertEqual(message.to, [recipient.email_address])
    #     self.assertEqual(message.reply_to, ['%s' % sender.anonymous_email_address])
    #
    #     sender.delete()

    @skip('')
    @freeze_time("2012-01-14")
    def test_send_message(self):
        users = User.objects.all()
        sender = users[0]
        recipient = users[1]
        thread = Thread.objects.create()
        message = Message.objects.create(
            uid=0,
            subject='LANANNNANANAAAA!!!',
            body='DANGER ZONE!!!',
            sender=sender,
            recipient=recipient,
            thread=thread
        )
        message.send()

        # Calculate reply_to
        payload = {
            'message': message.id,
        }
        calculated_signature = signing.dumps(payload, salt=REPLY_TO)
        reply_to = '%s@shotzu.com' % calculated_signature

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1)
        msg = mail.outbox[0]

        self.assertEqual(msg.from_email, '%s <%s>' % (sender.display_name, reply_to))
        self.assertEqual(msg.to, [recipient.email_address])
        self.assertEqual(msg.reply_to, ['%s <%s>' % (sender.display_name, reply_to)])

        # Check signature
        signature = msg.reply_to[0].split('<')[1].split('@')[0]
        self.assertEqual(signature, calculated_signature)
        self.assertEqual(payload, signing.loads(signature, salt=REPLY_TO))

        # Check bcc for staff
        self.assertEqual(msg.bcc, BCC)


    # def test_send_message_to_multiple_users(self):
    #     users = User.objects.all()
    #     sender = users[0]
    #     recipients = [users[1], users[2], users[3]]
    #     cc_recipients = [users[2], users[3]]
    #     bcc_recipients = [users[2], users[3]]
    #     message = Message.objects.create(
    #         uid=0,
    #         subject='LANANNNANANAAAA!!!',
    #         body='DANGER ZONE!!!',
    #         sender=sender,
    #         thread=Thread.objects.create()
    #     )
    #     [message.recipients.add(r) for r in recipients]
    #     [message.cc_recipients.add(r) for r in cc_recipients]
    #     [message.bcc_recipients.add(r) for r in bcc_recipients]
    #     message.send()
    #
    #     # Make sure email gets sent
    #     self.assertEqual(len(mail.outbox), 1)
    #     message = mail.outbox[0]
    #     self.assertEqual(message.from_email, '%s <%s>' % (sender.display_name, sender.anonymous_email_address))
    #     self.assertEqual(message.to, [r.email_address for r in recipients])
    #     self.assertEqual(message.reply_to, ['%s <%s>' % (sender.display_name, sender.anonymous_email_address)])
    #     self.assertEqual(message.cc, [r.email_address for r in cc_recipients])
    #     self.assertEqual(message.bcc, [r.email_address for r in bcc_recipients])

