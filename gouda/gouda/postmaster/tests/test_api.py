from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from gouda.profiles.utils import create_profiles
from gouda.postmaster.utils import create_messages
from gouda.postmaster.utils import tear_down_messages
from gouda.users.models import User
from gouda.postmaster.models import User as PostmasterUser
from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from rest_framework import status
from rest_framework.test import APITestCase as ApiTestCase
from gouda.urlresolvers import reverse_with_query
import random
from django.core import mail
from django.conf import settings
from gouda.photographers.models import Booking
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')


class MessageCreationApiTestCase(ApiTestCase):
    list_view_name = 'api_postmaster:message_list'

    @classmethod
    def setUpClass(cls):
        super(MessageCreationApiTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(1, do_create_images=False, do_create_messages=False)
        for user in User.objects.all():
            user.is_superuser = False
            user.is_staff = False
            user.save()

    @classmethod
    def tearDownClass(cls):
        super(MessageCreationApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.sender = random.choice(PostmasterUser.objects.all())
        self.recipient = random.choice(PostmasterUser.objects.exclude(id=self.sender.id))
        self.user = User.objects.get(email_address=self.sender.email_address)
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        self.thread = Booking.objects.all()[0].thread
        self.thread.users.clear()
        self.thread.users.add(self.sender)
        self.thread.users.add(self.recipient)
        self.data = {
            'body': 'DANGERZONE!!!?!?!?!?!',
            'thread': self.thread.id
        }
        mail.outbox = list()

    def tearDown(self):
        self.thread.users.clear()
        Message.objects.all().delete()

    def test_create_message_with_sender_not_in_thread(self):
        self.thread.users.clear()
        self.assertFalse(self.thread.users.filter(id=self.sender.id).exists())
        self.thread.users.add(self.recipient)
        url = reverse(self.list_view_name)
        data = self.data.copy()
        response = self.client.post(url, data, format='json')
        self.assertTrue(self.thread.users.filter(id=self.sender.id).exists())

    def test_create_message(self):
        url = reverse(self.list_view_name)
        data = self.data.copy()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.filter(sender=self.sender, thread=self.thread).count(), 1)

    def test_create_message_with_query_params(self):
        url = reverse_with_query(self.list_view_name, {'thread': self.thread.id})
        data = self.data.copy()
        data.pop('thread')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.filter(thread=self.thread).count(), 1)

    # def test_create_message_with_invalid_sender(self):
    #     url = reverse_with_query(self.list_view_name, {'thread': self.thread.id})
    #     data = self.data.copy()
    #     data.pop('thread')
    #
    #     # Login invalid user
    #     postmaster_user = random.choice(PostmasterUser.objects.exclude(id=self.sender.id).exclude(id=self.recipient.id))
    #     user = User.objects.get(email_address=postmaster_user.email_address)
    #     self.client.logout()
    #     self.client.login(username=user.email_address, password='%s123' % user.profile.full_name.replace(' ', '').lower())
    #
    #     data['sender'] = {'id': postmaster_user.id}
    #     response = self.client.post(url, data, format='json')
    #     self.assertEqual(response.status_code, status.HTTP_201_CREATED)
    #     self.assertEqual(Message.objects.filter(sender=self.sender).count(), 0)

    def test_create_message_with_minimal_payload(self):
        url = reverse_with_query(self.list_view_name, {'thread': self.thread.id})
        data = self.data.copy()
        data.pop('thread')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Message.objects.filter(thread=self.thread).count(), 1)

    def test_recipient_notification(self):
        url = reverse_with_query(self.list_view_name, {'thread': self.thread.id})
        data = self.data.copy()
        data.pop('thread')
        response = self.client.post(url, data, format='json')

        message = Message.objects.filter(thread=self.thread)[0]
        outbox = [m for m in mail.outbox if 'sent you a new message' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(self.recipient.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)


class MessageRetrievalApiTestCase(ApiTestCase):
    list_view_name = 'api_postmaster:message_list'

    @classmethod
    def setUpClass(cls):
        super(MessageRetrievalApiTestCase, cls).setUpClass()
        create_messages(10, 10)

    @classmethod
    def tearDownClass(cls):
        super(MessageRetrievalApiTestCase, cls).tearDownClass()
        tear_down_messages()

    def setUp(self):
        self.user = User.objects.all()[0]
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())

    def test_never_cache_headers(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('Last-Modified', response)
        self.assertIn('Expires', response)
        self.assertIn('Cache-Control', response)
        self.assertIn('max-age=0', response['Cache-Control'])

    def test_message_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for message in response.data.get('results'):
            self.assertIn('id', message)
            self.assertIn('dateCreated', message)
            self.assertIn('body', message)
            self.assertIn('subject', message)
            self.assertIn('thread', message)
            self.assertIn('sender', message)
            self.assertIn('firstName', message.get('sender'))
            self.assertIn('thumbnailUrl', message.get('sender'))
            self.assertIn('id', message.get('sender'))

    def test_filter_by_thread(self):
        message = Message.objects.filter(sender__profile__user__email_address=self.user.email_address)[0]
        thread = message.thread
        url = reverse_with_query(self.list_view_name, {'thread': thread.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        self.assertEqual(len(response.data.get('results')), thread.messages.all().count())
        for message in response.data.get('results'):
            self.assertEqual(len([i for i in thread.messages.all() if i.pk == message.get('id')]), Message.objects.filter(id=message.get('id')).count())

    def test_login_required(self):
        url = reverse(self.list_view_name)
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_message_ownership(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for result in response.data.get('results'):
            message = Message.objects.get(id=result.get('id'))
            self.assertTrue(message.thread.users.filter(profile__user__email_address=self.user.email_address).exists())
