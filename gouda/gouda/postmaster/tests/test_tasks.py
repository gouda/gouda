from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.test import TestCase
from gouda.postmaster.models import User
from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from django.conf import settings
from django.core import mail
from gouda import signing
from gouda.postmaster.management.commands.process_mailbox import process_email
from freezegun import freeze_time
from gouda.profiles.models import GenderType
from gouda.profiles.models import Profile
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import get_profile_data
from unittest import skip

import re

from imbox.parser import Struct

EMAIL_DOMAIN = getattr(settings, 'POSTMASTER_EMAIL_DOMAIN')
FROM_EMAIL = getattr(settings, 'POSTMASTER_FROM_EMAIL')


@skip('Not used anyomore...')
class ProcessMailBoxTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ProcessMailBoxTestCase, cls).setUpClass()
        Profile.objects.create(**get_profile_data({
            'full_name': 'Sterling Archer',
            'gender': GenderType.MALE
        })),
        Profile.objects.create(**get_profile_data({
            'full_name': 'Lana Kane',
            'gender': GenderType.FEMALE
        })),
        Profile.objects.create(**get_profile_data({
            'full_name': 'Cyril Figgis',
            'gender': GenderType.MALE
        })),
        Profile.objects.create(**get_profile_data({
            'full_name': 'Pam Poovey',
            'gender': GenderType.FEMALE
        })),

    @classmethod
    def tearDownClass(cls):
        super(ProcessMailBoxTestCase, cls).tearDownClass()
        User.objects.all().delete()

    def tearDown(self):
        Message.objects.all().delete()
        Thread.objects.all().delete()

    def test_unknown_sender(self):
        users = User.objects.all()
        recipient = users[1]
        uid = 0
        email = Struct(**{
            'subject': 'DANGER ZONE!!!',
            'body': {'plain': 'DANGER ZONE!!!!!!!!!'},
            'sent_from': [{'name': 'Brett Bunsen Sr.', 'email': 'brettbunsensr@gmail.com'}],
            'sent_to': [{'name': recipient.display_name, 'email': recipient.anonymous_email_address}]
        })
        process_email(uid, email)
        self.assertTrue(Message.objects.all().count() == 1)
        self.assertTrue(Thread.objects.all().count() == 1)
        message = Message.objects.all()[0]
        self.assertEqual(message.sender.email_address, 'brettbunsensr@gmail.com')
        self.assertEqual(message.recipient, recipient)

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1)
        email_message = mail.outbox[0]
        self.assertEqual(email_message.from_email, message.reply_to)
        self.assertEqual(email_message.reply_to, [message.reply_to])

    def test_process_initial_email(self):
        users = User.objects.all()
        sender = users[0]
        recipient = users[1]
        uid = 0
        email = Struct(**{
            'subject': 'DANGER ZONE!!!',
            'body': {'plain': 'DANGER ZONE!!!!!!!!!'},
            'sent_from': [{'name': sender.display_name, 'email': sender.email_address}],
            'sent_to': [{'name': recipient.display_name, 'email': recipient.anonymous_email_address}]
        })
        process_email(uid, email)
        self.assertTrue(Message.objects.all().count() == 1)
        self.assertTrue(Thread.objects.all().count() == 1)
        message = Message.objects.all()[0]
        self.assertEqual(message.sender, sender)
        self.assertEqual(message.recipient, recipient)

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1)
        email_message = mail.outbox[0]
        self.assertEqual(email_message.from_email, message.reply_to)
        self.assertEqual(email_message.reply_to, [message.reply_to])

    def test_process_reply(self):
        users = User.objects.all()
        sender = users[0]
        recipient = users[1]
        email = Struct(**{
            'subject': 'DANGER ZONE!!!',
            'body': {'plain': 'DANGER ZONE!!!!!!!!!'},
            'sent_from': [{'name': sender.display_name, 'email': sender.email_address}],
            'sent_to': [{'name': recipient.display_name, 'email': recipient.anonymous_email_address}]
        })
        process_email(0, email)
        mail.outbox = list()

        # Create reply
        message = Message.objects.all()[0]
        email = Struct(**{
            'subject': 'DANGER ZONE REPLY!!!>!>!!!!',
            'body': {'plain': 'DANGER ZONE REPLYING!!!!!!!!!'},
            'sent_from': [{'name': recipient.display_name, 'email': recipient.email_address}],
            'sent_to': [{'name': sender.display_name, 'email': re.split('<|>', message.reply_to)[1]}]
        })
        process_email(1, email)

        self.assertTrue(Message.objects.all().count() == 2)
        self.assertTrue(Thread.objects.all().count() == 1)
        self.assertTrue(Thread.objects.all()[0].messages.all().count() == 2)

        # Make sure email gets sent
        message = Message.objects.all().order_by('-date_created')[0]
        self.assertEqual(len(mail.outbox), 1)
        email_message = mail.outbox[0]
        self.assertEqual(email_message.from_email, message.reply_to)
        self.assertEqual(email_message.reply_to, [message.reply_to])
