from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import random
import string

from django.conf import settings
from django.db.models.signals import pre_save
from django.dispatch import receiver
from gouda.postmaster.models import User

EMAIL_DOMAIN = getattr(settings, 'POSTMASTER_EMAIL_DOMAIN')


@receiver(pre_save, sender=User)
def handle_user_pre_save(instance, **kwargs):
    if instance.pk is not None:
        return

    # Create a key
    def create_key():
        choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
        return ''.join([random.choice(choices) for i in range(6)]).lower()

    # Keep making keys until one is unique
    key = create_key()
    while User.objects.filter(anonymous_email_address='%s@%s' % (key, EMAIL_DOMAIN)).exists():
        key = create_key()

    instance.anonymous_email_address = '%s@%s' % (key, EMAIL_DOMAIN)

