from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import django_filters
from django.db.models import Q
from drf_extra_fields import fields
from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from gouda.postmaster.models import User
from gouda.users.models import User as DjangoUser
from rest_framework import filters
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import pagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import ParseError
from easy_thumbnails.files import get_thumbnailer
from rest_framework import mixins
from gouda.profiles.models import Profile
from django.contrib.staticfiles.templatetags.staticfiles import static
from easy_thumbnails.files import get_thumbnailer
from django.conf import settings


THUMBNAIL_ALIASES = getattr(settings, 'THUMBNAIL_ALIASES')


class MessagePagination(pagination.LimitOffsetPagination):
    default_limit = 9000


class MessageFilter(filters.FilterSet):
    thread = django_filters.NumberFilter(name='thread__id')

    class Meta:
        model = Message
        fields = ['thread']


class UserSerializer(serializers.ModelSerializer):
    first_name = serializers.SerializerMethodField(read_only=True)
    thumbnail_url = serializers.SerializerMethodField(read_only=True)

    def to_internal_value(self, data):
        return User.objects.get(id=data.get('id'))

    def get_first_name(self, obj):
        return obj.first_name

    def get_thumbnail_url(self, obj):
        url = static('profiles/images/anonymous_profile.png.80x80_q85_crop.png')
        if obj.profile.image:
            thumbnailer = get_thumbnailer(obj.profile.image)
            url = self.context.get('request').build_absolute_uri(
                thumbnailer.get_thumbnail(thumbnail_options=THUMBNAIL_ALIASES.get('profiles.Profile.image').get('80x80')).url
            )
        return url

    class Meta:
        model = User
        depth = 1
        fields = (
            'first_name',
            'id',
            'thumbnail_url'
        )


class MessageSerializer(serializers.HyperlinkedModelSerializer):
    thread = serializers.PrimaryKeyRelatedField(queryset=Thread.objects.all())
    sender = UserSerializer()

    def create(self, validated_data):
        thread = validated_data.get('thread')
        sender = validated_data.get('sender')
        thread.users.add(sender)
        return super(MessageSerializer, self).create(validated_data)

    class Meta:
        model = Message
        fields = (
            'id',
            'body',
            'subject',
            'thread',
            'sender',
            'date_created',
        )
        extra_kwargs = {
            'url': {
                'view_name': 'api_postmaster:message_detail',
                'lookup_field': 'pk'
            }
        }


class MessageViewSet(mixins.CreateModelMixin,
                     mixins.RetrieveModelMixin,
                     mixins.ListModelMixin,
                     viewsets.GenericViewSet):
    serializer_class = MessageSerializer
    filter_backends = (filters.DjangoFilterBackend, filters.OrderingFilter,)
    filter_class = MessageFilter
    permission_classes = [IsAuthenticated]
    pagination_class = MessagePagination
    ordering = ('date_created',)

    def perform_create(self, serializer):
        message = serializer.save()
        message.send_recipient_notification()
        return message

    def create(self, request, *args, **kwargs):
        if 'thread' in request.query_params and 'thread' not in request.data:
            request.data['thread'] = request.query_params.get('thread')
        if 'recipient' in request.query_params and 'recipient' not in request.data:
            request.data['recipient'] = {'id': request.query_params.get('recipient')}

        # Check sender...
        try:
            sender = User.objects.get(profile__user__email_address=self.request.user.email_address)
            request.data['sender'] = {'id': sender.id}
        except User.DoesNotExist:
            raise ParseError()

        return super(MessageViewSet, self).create(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.is_staff:
            return Message.objects.all()
        else:
            return Message.objects.filter(thread__users__id=User.objects.get(profile__user__email_address=self.request.user.email_address).id)
            # return Message.objects.get_messages_by_user(User.objects.get(profile__user__email_address=self.request.user.email_address))


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserSerializer
    permission_classes = [IsAuthenticated]
