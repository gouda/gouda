from __future__ import absolute_import
from __future__ import unicode_literals

from django.core.management.base import BaseCommand
from django.conf import settings
import logging
from gouda.postmaster.imbox import Imbox
from gouda.postmaster.models import User
from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from datetime import date
from datetime import timedelta
from gouda import signing
from gouda.postmaster.salt import REPLY_TO

logger = logging.getLogger(__name__)

IMAP_HOST = getattr(settings, 'POSTMASTER_IMAP_HOST')
IMAP_USERNAME = getattr(settings, 'POSTMASTER_IMAP_USERNAME')
IMAP_PASSWORD = getattr(settings, 'POSTMASTER_IMAP_PASSWORD')
IMAP_USE_SSL = getattr(settings, 'POSTMASTER_IMAP_USE_SSL')


def process_email(uid, email):
    recipient = None
    thread = None
    if Message.objects.filter(uid=uid).exists():
        logger.info('Skipping message that was already processed (%d)' % int(uid))
        return
    # if User.objects.filter(email_address=email_message.sent_from[0].get('email')).exists():
    sender, is_created = User.objects.get_or_create(email_address=email.sent_from[0].get('email'))
    if len(email.sent_to) > 0:
        sent_to = email.sent_to[0]
        if User.objects.filter(anonymous_email_address=sent_to.get('email')).exists():
            recipient = User.objects.get(anonymous_email_address=sent_to.get('email'))
        else:
            payload = signing.loads(sent_to.get('email').split('@')[0], salt=REPLY_TO)
            message = Message.objects.get(id=payload.get('message'))
            thread = message.thread
            recipient = message.sender

    if not sender or not recipient:
        logger.info('Skipping message that has no valid recipient or sender')
        return

    message = Message.objects.create(
        uid=int(uid),
        subject=email.subject,
        body=email.body.get('plain')[0],
        sender=sender,
        recipient=recipient,
        thread=thread if thread else Thread.objects.create()
    )
    message.send()


class Command(BaseCommand):
    def handle(self, *args, **options):
        imbox = Imbox(IMAP_HOST, IMAP_USERNAME, IMAP_PASSWORD, IMAP_USE_SSL)

        for uid, email in imbox.messages(date__gt=date.today() - timedelta(days=1)):
            process_email(uid, email)
