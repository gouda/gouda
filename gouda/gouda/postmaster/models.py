from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.db import models
from django.template import loader
from gouda import signing
from gouda.context_processors import constants
from gouda.postmaster.salt import REPLY_TO
from gouda.profiles.models import Profile
from django.db.models import Q
from django.core.urlresolvers import reverse

FROM_EMAIL = getattr(settings, 'POSTMASTER_FROM_EMAIL')
BCC = getattr(settings, 'POSTMASTER_BCC')


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class UserManager(models.Manager):
    def get_users_by_thread(self, thread):
        query = Q()
        for message in thread.messages.all():
            query |= Q(id=message.sender.id)
        return User.objects.filter(query)


class User(BaseModel):
    profile = models.OneToOneField(Profile, related_name='postmaster_user')
    anonymous_email_address = models.EmailField()

    objects = UserManager()

    @property
    def email_address(self):
        return self.profile.email_address

    @property
    def full_name(self):
        return self.profile.full_name

    @property
    def first_name(self):
        return self.profile.first_name

    @property
    def last_name(self):
        return self.profile.last_name

    @property
    def display_name(self):
        return self.profile.display_name

    def __str__(self):
        return '%s <%s>' % (self.full_name, self.email_address) if self.full_name else self.email_address


class Thread(BaseModel):
    users = models.ManyToManyField(User, related_name='threads')


class MessageManager(models.Manager):
    def get_messages_by_user(self, user):
        query = Q()
        for thread in Thread.objects.all():
            for message in thread.messages.all():
                if message.sender == user:
                    query |= Q(thread__id=thread.id)
                    break
        return Message.objects.filter(query)


class Message(BaseModel):
    uid = models.IntegerField(null=True)
    body = models.TextField()
    subject = models.CharField(max_length=998, blank=True, default='')

    thread = models.ForeignKey(Thread, related_name='messages', default=None)
    sender = models.ForeignKey(User, related_name='sent_messages')

    objects = MessageManager()

    # @property
    # def reply_to(self):
    #     # Calculate reply_to
    #     payload = {
    #         'message': self.id,
    #     }
    #     reply_to = '%s@shotzu.com' % signing.dumps(payload, salt=REPLY_TO)
    #     return '%s <%s>' % (self.sender.display_name, reply_to) if self.sender.display_name else reply_to
    #
    # def send(self):
    #     current_site = Site.objects.get_current()
    #     context = dict({
    #         'domain': current_site.domain,
    #         'site_name': current_site.name,
    #         'protocol': 'http',
    #     })
    #     context.update({
    #         'message': self
    #     })
    #     context.update(constants(None))
    #
    #     email_message = EmailMessage(
    #         subject=loader.render_to_string('postmaster/emails/subject.txt', context),
    #         body=loader.render_to_string('postmaster/emails/body.txt', context),
    #         from_email=self.reply_to,
    #         to=[self.recipient.email_address],
    #         reply_to=[self.reply_to],
    #         bcc=BCC
    #     )
    #     email_message.send()

    def send_recipient_notification(self):
        from gouda.photographers.models import Booking
        from gouda.photographers.models import Photographer
        current_site = Site.objects.get_current()
        booking = Booking.objects.get(thread=self.thread)
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'message': self,
            'booking': booking,
            'view_name': 'profiles:thread_detail',
        })
        context.update(constants(None))

        # Get recipients
        recipients = self.thread.users.exclude(id=self.sender.id)

        # Render and send notification email
        for recipient in recipients:
            reply_url = reverse('profiles:booking_detail', kwargs={'pk': booking.id})
            if not recipient.profile.user.is_staff and Photographer.objects.filter(profile=recipient.profile).exists():
                reply_url = reverse('profiles:shoot_detail', kwargs={'pk': booking.id})
            context.update({
                'recipient': recipient,
                'reply_url': reply_url
            })
            subject = loader.render_to_string('postmaster/emails/messages/recipient_notification_subject.txt', context)
            subject = ''.join(subject.splitlines())
            body = loader.render_to_string('postmaster/emails/messages/recipient_notification_body.txt', context)
            email_message = EmailMessage(subject=subject, body=body, from_email=FROM_EMAIL, to=[recipient.email_address], bcc=BCC)
            email_message.send()


from gouda.postmaster.signals import *
