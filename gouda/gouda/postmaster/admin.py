from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
import logging
import pytz
from django.contrib import admin
from gouda.postmaster.models import User
from gouda.postmaster.models import Message
from gouda.postmaster.models import Thread
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import Photographer
from gouda.photographers.models import Tip
from gouda.photographers.models import Booking
from gouda.photographers.models import Review
from gouda.photographers.models import ReviewURL
from gouda.photographers.models import BookingCheckoutURL
from gouda.customers.models import BraintreeTransactionStatusType
from gouda.customers.models import BraintreeEscrowStatusType
from gouda.customers.utils import braintree


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = (
        'email_address',
        'anonymous_email_address',
        'full_name',
    )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'uid',
        'body',
        'subject',
        'sender',
    )

    readonly_fields = (
        'uid',
        'body',
        'subject',
        'sender',
    )


@admin.register(Thread)
class Thread(admin.ModelAdmin):
    list_display = (
        'id',
        'num_users',
        'date_created',
        'date_modified',
    )
    readonly_fields = (
        'users',

    )
    fieldsets = (
        (None, {
            'fields': (
                'users',
            )
        }),
    )

    def num_users(self, obj):
        return obj.users.all().count()
