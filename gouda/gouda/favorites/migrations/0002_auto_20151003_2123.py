# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('favorites', '0001_initial'),
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='favorite',
            name='profile',
            field=models.ForeignKey(to='profiles.Profile'),
        ),
        migrations.AlterUniqueTogether(
            name='favorite',
            unique_together=set([('profile', 'content_type', 'object_id')]),
        ),
    ]
