from __future__ import absolute_import

from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.apps import apps


class FavoriteManager(models.Manager):
    def get_for_profile(self, profile, app_label=None, model_name=None):
        queryset = self.get_queryset().filter(profile=profile)
        if model_name and app_label:
            model = apps.get_model(app_label=app_label, model_name=model_name)
            content_type = ContentType.objects.get_for_model(model)
            queryset = queryset.filter(content_type=content_type)

        return queryset.order_by('-date_created')