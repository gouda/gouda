from rest_framework import viewsets
from rest_framework import mixins
from gouda.favorites.serializers import FavoriteListSerializer
from gouda.favorites.models import Favorite
from gouda.pagination import LargeLimitOffsetPagination
from rest_framework import authentication
from rest_framework import permissions


class FavoriteViewSet(mixins.CreateModelMixin,
                      mixins.ListModelMixin,
                      mixins.DestroyModelMixin,
                      viewsets.GenericViewSet):
    serializer_class = FavoriteListSerializer
    pagination_class = LargeLimitOffsetPagination
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(profile=self.request.user.profile)

    def get_queryset(self):
        return Favorite.objects.get_for_profile(profile=self.request.user.profile)
