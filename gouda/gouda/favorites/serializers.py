from __future__ import absolute_import

from rest_framework import serializers
from gouda.favorites.models import Favorite
from gouda.listings.models import Listing


class FavoriteListSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField(view_name='api_favorites:favorite_detail')
    app_label = serializers.CharField(required=True, write_only=True)
    model_name = serializers.CharField(required=True, write_only=True)

    def create(self, validated_data):
        if validated_data.get('app_label') == 'listings':
            if validated_data.get('model_name') == 'Listing':
                profile = validated_data.get('profile')
                listing = Listing.objects.get(id=validated_data.get('object_id'))
                return Favorite.objects.create(profile=profile, content_object=listing)
        return super(FavoriteListSerializer, self).create(validated_data)

    class Meta:
        model = Favorite
        fields = ('id', 'url', 'object_id', 'app_label', 'model_name')
