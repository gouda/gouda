from __future__ import absolute_import

from gouda.favorites.models import Favorite
from gouda.users.models import User
from gouda.listings.models import Listing
from gouda.listings.utils import create_listings
from gouda.listings.utils import tear_down_listings
from gouda.favorites.utils import tear_down_favorites
from gouda.profiles.models import Profile
from rest_framework.test import APITestCase
from django.core.urlresolvers import reverse
from rest_framework import status


class BaseTestCases(object):
    class FavoriteAPITestCase(APITestCase):
        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.FavoriteAPITestCase, cls).setUpClass()
            create_listings(1)

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.FavoriteAPITestCase, cls).tearDownClass()
            tear_down_listings()

        def create_favorite(self, app_label, model_name, object_id, do_expect_errors=False):
            data = {
                'appLabel': app_label,
                'modelName': model_name,
                'objectId': object_id
            }
            url = reverse('api_favorites:favorite_list')
            response = self.client.post(url, data, format='json')
            if not do_expect_errors:
                self.assertEqual(response.status_code, status.HTTP_201_CREATED)
            return response

        def create_favorite_listing(self, object_id, do_expect_errors=False):
            return self.create_favorite('listings', 'Listing', object_id, do_expect_errors)

        def setUp(self):
            self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')
            self.user = User.objects.get(email_address='success+sterlingarcher@simulator.amazonses.com')
            self.profile = Profile.objects.get(user=self.user)
            self.listing = Listing.objects.all()[0]
            self.assertEqual(Favorite.objects.all().count(), 0)

        def tearDown(self):
            tear_down_favorites()


class FavoriteDeleteAPITestCase(BaseTestCases.FavoriteAPITestCase):
    def test_delete_favorite(self):
        # Create a favorite
        response = self.create_favorite_listing(self.listing.id)
        self.assertEqual(Favorite.objects.all().count(), 1)
        self.assertTrue(Favorite.objects.get_for_profile(profile=self.profile).filter(object_id=self.listing.id).exists())

        # Delete a favorite
        url = response.data.get('url')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(Favorite.objects.get_for_profile(profile=self.profile).filter(object_id=self.listing.id).exists())

    def test_delete_favorite_without_authentication(self):
        # Create a favorite
        response = self.create_favorite_listing(self.listing.id)
        self.assertEqual(Favorite.objects.all().count(), 1)
        self.assertTrue(Favorite.objects.get_for_profile(profile=self.profile).filter(object_id=self.listing.id).exists())

        # Delete a favorite
        self.client.logout()
        url = response.data.get('url')
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertTrue(Favorite.objects.get_for_profile(profile=self.profile).filter(object_id=self.listing.id).exists())


class FavoriteCreateAPITestCase(BaseTestCases.FavoriteAPITestCase):
    def test_create_favorite(self):
        response = self.create_favorite_listing(self.listing.id)
        self.assertEqual(Favorite.objects.all().count(), 1)
        self.assertTrue(Favorite.objects.get_for_profile(profile=self.profile).filter(object_id=self.listing.id).exists())

    def test_create_favorite_without_authentication(self):
        self.client.logout()
        response = self.create_favorite_listing(self.listing.id, do_expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Favorite.objects.all().count(), 0)
        self.assertFalse(Favorite.objects.get_for_profile(profile=self.profile).filter(object_id=self.listing.id).exists())
