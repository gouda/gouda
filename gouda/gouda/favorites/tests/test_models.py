from __future__ import absolute_import

from django.test import TestCase
from gouda.favorites.models import Favorite
from gouda.listings.utils import create_listings
from gouda.profiles.models import Profile
from gouda.listings.models import Listing


class FavoriteModelTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(FavoriteModelTestCase, cls).setUpClass()
        create_listings(1)
        profile = Profile.objects.all()[0]
        listing = Listing.objects.all()[0]
        Favorite.objects.create(profile=profile, content_object=listing)

    def test_init(self):
        Favorite()

    def test_get_favorites_by_profile(self):
        profile = Profile.objects.all()[0]
        self.assertTrue(Favorite.objects.get_for_profile(profile, app_label='listings', model_name='Listing').exists())
