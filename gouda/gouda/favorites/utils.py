from __future__ import absolute_import
from gouda.favorites.models import Favorite


def tear_down_favorites():
    Favorite.objects.all().delete()
