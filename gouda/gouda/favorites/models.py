from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from gouda.profiles.models import Profile
from gouda.favorites.managers import FavoriteManager

class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Favorite(BaseModel):
    profile = models.ForeignKey(Profile)
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey()

    objects = FavoriteManager()

    class Meta:
        unique_together = ('profile', 'content_type', 'object_id')

    def __unicode__(self):
        return u'%s favorited %s' % (self.profile, self.content_object)
