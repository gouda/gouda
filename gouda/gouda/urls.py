from django.conf.urls import include, url
from django.contrib import admin
from django.conf import settings
from django.views.decorators.cache import cache_page
from gouda.routers import Router
from gouda.favorites.api import FavoriteViewSet
from gouda.customers.api import CouponVerificationAPIView
from gouda.customers.api import BraintreeClientTokenView
from gouda.customers.api import BraintreeWebhookNotificationViewSet
from gouda.customers.api import StripeCouponVerificationView
from gouda.customers.api import StripeCustomerViewSet
from gouda.customers.api import StripeEventViewSet
from gouda.profiles.views import ProfileCreateView
from gouda.photographers.views import CreateFoodBookingFormView
from gouda.photographers.views import PhotographerSearchFormView
from gouda.photographers.views import BookingDetailsFormView
from gouda.photographers.api import FoodBookingViewSet
from gouda.photographers.api import BookingImageViewSet
from gouda.photographers.api import BookingAlbumViewSet
from gouda.listings.api import ListingViewSet
from gouda.listings.api import ReviewViewSet
from gouda.views import PrivacyPolicyView
from gouda.views import TermsOfServiceView
from gouda.views import AboutUsView
from gouda.postmaster.api import MessageViewSet
from django.http import HttpResponse
from django.views.generic import View
from django.contrib.sitemaps.views import sitemap
from gouda.sitemaps import StaticViewSitemap
from django.views import static
import django


class UnderConstructionView(View):
    def get(self, request, *args, **kwargs):
        return HttpResponse('Under Construction')
# Routing
never_cache_router = Router(do_never_cache=True, is_login_required=True)
never_cache_router.register(r'message', MessageViewSet, base_name='message')
never_cache_router.register(r'favorite', FavoriteViewSet, base_name='favorite')
never_cache_router.register(r'stripe_customer', StripeCustomerViewSet, base_name='stripe_customer')
never_cache_router.register(r'stripe_event', StripeEventViewSet, base_name='stripe_event')
cache_router = Router(cache_timeout=3600, is_login_required=True)
router_urls = never_cache_router.urls + cache_router.urls
router_urls.append(
    url(r'^stripe_coupon/(?P<id>\S+)/verify$', StripeCouponVerificationView.as_view(), name='verify_stripe_coupon'),
)


urls = list()

# # Under Construction
# urls.append(
#     url(r'^$', UnderConstructionView.as_view())
# )

# API
urls.append(
    url(r'^api/', include(router_urls, namespace='api')),
)

# Customers API
customer_urls = [
    url(r'^coupon/(?P<code>\S+)/verify$', CouponVerificationAPIView.as_view(), name='verify_coupon'),
]
urls.append(
    url(r'^api/customers/', include(customer_urls, namespace='api_customers')),
)

# Braintree API
braintree_urls = [
    url(r'^clienttoken/$', BraintreeClientTokenView.as_view(), name='client_token'),
]
braintree_router = Router(do_never_cache=True, is_login_required=False)
braintree_router.register(r'b8nt6q09pt', BraintreeWebhookNotificationViewSet, base_name='webhook_notification')
braintree_urls.extend(braintree_router.urls)
urls.append(
    url(r'^api/braintree/', include(braintree_urls, namespace='api_braintree')),
)

# Postmaster API
postmaster_router = Router(do_never_cache=True, is_login_required=True)
postmaster_router.register(r'messages', MessageViewSet, base_name='message')
urls.append(
    url(r'^api/postmaster/', include(postmaster_router.urls, namespace='api_postmaster'))
)

# Favorites API
favorites_router = Router(do_never_cache=True, is_login_required=True)
favorites_router.register(r'favorite', FavoriteViewSet, base_name='favorite')
urls.append(
    url(r'^api/favorites/', include(favorites_router.urls, namespace='api_favorites'))
)

# Listing API
listing_router = Router(cache_timeout=3600)
listing_router.register(r'listing', ListingViewSet, base_name='listing')
listing_router.register(r'review', ReviewViewSet, base_name='review')
urls.append(
    url(r'^api/listings/', include(listing_router.urls, namespace='api_listings'))
)

# Photographer API
photographers_router = Router(do_never_cache=True, is_login_required=True)
photographers_router.register(r'food_bookings', FoodBookingViewSet, base_name='food_booking')
photographers_router.register(r'booking_images', BookingImageViewSet, base_name='booking_image')
photographers_router.register(r'booking_albums', BookingAlbumViewSet, base_name='booking_album')
urls.append(
    url(r'^api/photographers/', include(photographers_router.urls, namespace='api_photographers'))
)

# Hijack
urls.append(
    url(r'^hijack/', include('hijack.urls'))
)

# Admin
urls.append(
    url(r'^cd72789d/', include(admin.site.urls)),
)
urls.append(
    url(r'^admin/', include('admin_honeypot.urls', namespace='admin_honeypot')),
)

# Photographers
urls.append(
    url(r'^photographers/', include('gouda.photographers.urls', namespace='photographers')),
)
urls.append(url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/$', BookingDetailsFormView.as_view(), name='checkout'))
urls.append(
    url(r'^postmates/$', CreateFoodBookingFormView.as_view(), name='create_food_booking')
)

# Listings
urls.append(
    url(r'^listings/', include('gouda.listings.urls', namespace='listings'))
)

# Profiles
urls.append(
    url(r'^profiles/', include('gouda.profiles.urls', namespace='profiles'))
)

# Static stuff
if settings.DEBUG:
    urls.append(
        url(r'^media/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT})
    )
    urls.append(
        url(r'^static/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.STATIC_ROOT})
    )

    # urls.append(
    #     url(r'^static/bower_components/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/opt/gouda/bower_components/'})
    # )
    # urls.append(
    #     url(r'^static/admin/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/opt/gouda/lib/python2.7/site-packages/django/contrib/admin/static/admin/'})
    # )
    # urls.append(
    #     url(r'^static/photographers/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/opt/gouda/gouda/gouda/photographers/static/photographers/'})
    # )
    # urls.append(
    #     url(r'^static/profiles/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/opt/gouda/gouda/gouda/photographers/static/profiles/'})
    # )
    # urls.append(
    #     url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': '/opt/gouda/gouda/gouda/static/'})
    # )

# # Contacts
# urls.append(
#     url(r'', include('gouda.contacts.urls'))
# )

urls.append(
    url(r'^privacy/$', cache_page(86400)(PrivacyPolicyView.as_view()), name='privacy_policy'),
)

urls.append(
    url(r'^terms/$', cache_page(86400)(TermsOfServiceView.as_view()), name='terms_of_service'),
)

urls.append(
    url(r'^about/$', cache_page(86400)(AboutUsView.as_view()), name='about_us'),
)

urls.append(
    url(r'^$', cache_page(60 * 60 * 24)(PhotographerSearchFormView.as_view()), name='index'),
)


sitemaps = {
    'static': StaticViewSitemap,
}
urls.append(
    url(r'^sitemap\.xml$', sitemap, {'sitemaps': sitemaps}, name='django.contrib.sitemaps.views.sitemap')
)

# Social
urls.append(url('', include('social.apps.django_app.urls', namespace='social')))

urlpatterns = urls
