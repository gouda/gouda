from __future__ import absolute_import

from django.utils.log import AdminEmailHandler
from django.utils.log import ExceptionReporter
from django.utils.log import get_exception_reporter_filter
from django.conf import settings
from django.utils.encoding import force_text
from fightnerd.users.management.commands.send_mail import send_mail

ADMINS = getattr(settings, 'ADMINS', None)


class AsyncAdminEmailHandler(AdminEmailHandler):
    """An exception log handler that asynchronously emails log entries to site admins.

    If the request is passed as the first argument to the log record,
    request data will be provided in the email report.
    """

    def emit(self, record):
        try:
            request = record.request
            subject = '%s (%s IP): %s' % (
                record.levelname,
                ('internal' if request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS
                 else 'EXTERNAL'),
                record.getMessage()
            )
            filter = get_exception_reporter_filter(request)
            request_repr = '\n{0}'.format(force_text(filter.get_request_repr(request)))
        except Exception:
            subject = '%s: %s' % (
                record.levelname,
                record.getMessage()
            )
            request = None
            request_repr = "unavailable"
        subject = self.format_subject(subject)

        if record.exc_info:
            exc_info = record.exc_info
        else:
            exc_info = (None, record.getMessage(), None)

        message = "%s\n\nRequest repr(): %s" % (self.format(record), request_repr)
        reporter = ExceptionReporter(request, is_email=True, *exc_info)
        html_message = reporter.get_traceback_html() if self.include_html else None
        if ADMINS:
            send_mail(subject, message, [admin[1] for admin in ADMINS], html_message=html_message)
