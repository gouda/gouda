from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf import settings
from django.contrib.gis.geoip2 import GeoIP2

from gouda.celeryapp import app
from gouda.users.models import LoginInfo

MAX_LOGIN_INFOS = getattr(settings, 'MAX_LOGIN_INFOS')


@app.task
def geocode_login_info(login_info_id):
    login_info = LoginInfo.objects.get(pk=login_info_id)
    ip_address = login_info.ip_address
    if not ip_address:
        return None

    geo_ip = GeoIP2()
    data = geo_ip.city(ip_address)
    latitude = data.pop('latitude', None)
    longitude = data.pop('longitude', None)

    if latitude and longitude:
        data['location'] = 'POINT (%s %s)' % (longitude, latitude)

    # Remove nulls...
    keys_to_remove = list()
    for key in data:
        if data[key] is None:
            keys_to_remove.append(key)
    for key in keys_to_remove:
        data.pop(key, None)

    LoginInfo.objects.filter(pk=login_info_id).update(**data)

    # Do some pruning
    for l in LoginInfo.objects.filter(user=login_info.user).order_by('-date_created')[MAX_LOGIN_INFOS:]:
        l.delete()

    return login_info
