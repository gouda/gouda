__author__ = 'chukwuemeka'

from rest_framework import permissions
from fightnerd.users.middleware import has_oauth2_authorization


class OAuthPermission(permissions.BasePermission):
    """
    Custom permission to only allow owners of an object to edit it.
    """

    def has_permission(self, request, view):
        return has_oauth2_authorization(request)

    def has_object_permission(self, request, view, obj):
        if hasattr(obj, 'user'):
            return obj.user == request.user
        else:
            return True
