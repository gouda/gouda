from __future__ import absolute_import

import logging
import base64

from django.middleware import csrf
from fightnerd.users.models import Application
from oauth2_provider.models import AccessToken

logger = logging.getLogger(__name__)


def has_oauth2_authorization(request):
    http_authorization = request.META.get('HTTP_AUTHORIZATION', None)
    if http_authorization:
        if 'Basic' in http_authorization:
            credentials = http_authorization.split('Basic ')[1]
            credentials = base64.b64decode(credentials)
            for application in Application.objects.all():
                if ('%s:%s' % (application.client_id, application.client_secret)) == credentials:
                    return True
        elif 'Bearer' in http_authorization:
            credentials = http_authorization.split('Bearer ')[1]
            try:
                access_token = AccessToken.objects.get(token=credentials)
            except AccessToken.DoesNotExist:
                return False
            if access_token.application:
                return True
    return False


def get_application(request):
    http_authorization = request.META.get('HTTP_AUTHORIZATION', None)
    if http_authorization:
        if 'Basic' in http_authorization:
            credentials = http_authorization.split('Basic ')[1]
            credentials = base64.b64decode(credentials)
            for application in Application.objects.all():
                if ('%s:%s' % (application.client_id, application.client_secret)) == credentials:
                    return application
        elif 'Bearer' in http_authorization:
            credentials = http_authorization.split('Bearer ')[1]
            try:
                access_token = AccessToken.objects.get(token=credentials)
            except AccessToken.DoesNotExist:
                return False
            if access_token.application:
                return access_token.application
    return None


def is_csrf_protection_enabled(request):
    application = get_application(request)
    if application:
        return application.is_csrf_protection_enabled
    return True


class CsrfViewMiddleware(csrf.CsrfViewMiddleware):

    def process_view(self, request, callback, callback_args, callback_kwargs):
        if not is_csrf_protection_enabled(request):
            return super(CsrfViewMiddleware, self)._accept(request)
        return super(CsrfViewMiddleware, self).process_view(request, callback, callback_args, callback_kwargs)
