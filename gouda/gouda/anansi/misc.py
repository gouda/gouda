from scrapy import log
from scrapy.contrib.spiders import *
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.selector import *
from scrapy.http import *
from sys import exit
import re
import collections
import datetime
#from django.utils.timezone import utc
import time
from scrapy.utils.iterators import xmliter, csviter
from django.db.utils import IntegrityError
import requests
import json
import requests
from scrapy.exceptions import DropItem

MONTH_ABBR_TO_NUMBER = {'jan':1,
                        'feb':2,
                        'mar':3,
                        'apr':4,
                        'may':5,
                        'jun':6,
                        'jul':7,
                        'aug':8,
                        'sep':9,
                        'oct':10,
                        'nov':11,
                        'dec':12}

def bulk_create(model,object):
    try:
        model.objects.select_for_update().bulk_create([object])
    except IntegrityError:
        pass
        

def get_nodes(xmlPath, nodeName):
    """ Scrapy really does have a VERY intuitive framework for parsing xml so imma
        just gonna hack it in...
    """
    content = str(open(xmlPath,'r').read())
    for n in xmliter(content,nodeName):
        yield n

def get_nodes_from_str(content, nodeName):
    """ Scrapy really does have a VERY intuitive framework for parsing xml so imma
        just gonna hack it in...
    """
    for n in xmliter(content,nodeName):
        yield n

def get_datetimez(s):
    toks = re.split("-|T|:|Z",s)
    return datetime.datetime(int(toks[0]),int(toks[1]),int(toks[2]),int(toks[3]),int(toks[4]),int(toks[5]))

def get_datetime(s):
    toks = re.split("-|T",s)
    return datetime.datetime(int(toks[0]),int(toks[1]),int(toks[2]))

def get_date(s):
    toks = re.split("-|T",s)
    return datetime.date(int(toks[0]),int(toks[1]),int(toks[2]))

def extract(node, s):
    try:
        return node.xpath(s).extract()[0]
    except:
        return None