# Define here the Field()
#
# See documentation in:
# http://doc.scrapy.org/topics/items.html

from scrapy.item import Item, Field
from scrapy.contrib_exp import *
from scrapy.contrib.loader import ItemLoader, XPathItemLoader
from scrapy.contrib.loader.processor import TakeFirst, MapCompose, Join, Identity
import re
from django.core.exceptions import ObjectDoesNotExist
from django.db import IntegrityError
from datetime import datetime
import os
import tempfile
from django.core.files import File
import datetime
from fightnerd.settings import *

class ArticleItem(Item):
    title = Field()
    author = Field()
    content = Field()
    imageUrl = Field()
    url = Field()
    date = Field()

class PublicationItem(Item):
    name = Field()
    articles = Field()
    image_urls = Field()
    images = Field()

class UpcomingFightItem(Item):
    """Upcoming Fight"""
    firstFighter = Field()
    secondFighter = Field()
    firstFighterOdds = Field()
    secondFighterOdds = Field()
    id = Field()

class UpcomingEventItem(Item):
    """Upcoming Event"""
    guid = Field()
    name = Field()
    date = Field()
    sherdogId = Field()
    location = Field()
    organization = Field()
    isTimeValid = Field()
    areOddsValid = Field()
    
    upcomingFights = Field()
    
    image_urls = Field()
    images = Field()
    
#    json = Field()

class EventItem(Item):
    """Event"""
    guid = Field()
    name = Field()
    date = Field()
    sherdogId = Field()
    
class RefereeItem(Item):
    """Referee"""
    guid = Field()
    lastName = Field()
    firstName = Field()
    
class FightItem(Item):
    """Fight"""
    guid = Field()
    result = Field()
    method = Field()
    round = Field()
    time = Field()
    type = Field()
    
    opponent = Field()
    event = Field()
    referee = Field()

class AssociationItem(Item):
    """Association"""
    name = Field()
    guid = Field()

class FighterItem(Item):
    """Fighter"""
    lastName = Field()
    firstName = Field()
    sherdogId = Field()
    guid = Field()
    
    nickName = Field()
    dob = Field()
    height = Field()
    weight = Field()
    weightClass = Field()
    winsByKnockout = Field()
    winsBySubmission = Field()
    winsByDecision = Field()
    winsByOther = Field()
    lossesByKnockout = Field()
    lossesBySubmission = Field()
    lossesByDecision = Field()
    lossesByOther = Field()
    draws = Field()
    noContests = Field()
    city = Field()
    state = Field()
    country = Field()
    
    associations = Field()
    image_urls = Field()
    images = Field()
    fights = Field()
    image = Field()
    imageUrl = Field()
    
    isPostRequest = Field()
    isPatchRequest = Field()
    
#    json = Field()
