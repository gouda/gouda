import scrapy
import logging
from scrapy.http import Request
from scrapy.contrib.spiders import CrawlSpider
from scrapy.contrib.spiders import Rule
from scrapy.linkextractors import LinkExtractor


class RealtorSpider(CrawlSpider):
    name = 'realtor'
    allowed_domains = ['realtor.com']
    start_urls = [
        'http://www.realtor.com/realestateagents/21045/'
    ]

    def parse(self, response):
        logging.info('================ %s =================' % response.body.strip())


class ZillowSpider(CrawlSpider):
    name = 'zillow'
    allowed_domains = ['zillow.com']
    rules = (
             Rule(LinkExtractor(deny=('Reviews', ), allow=('profile', )), callback='parse_realtor'),
             Rule(LinkExtractor(deny=('Reviews', ), allow=('profile', ))),
    )

    def __init__(self, name=None, **kwargs):
        super(ZillowSpider, self).__init__(name, **kwargs)
        self.start_urls = ['http://www.zillow.com/columbia-md/real-estate-agent-reviews/?showAdvancedItems=false&regionID=10917&locationText=Columbia%20MD' + '&page=%d' % i for i in range(1, 26)]
        logging.info(self.start_urls)

    def parse_realtor(self, response):
        logging.info(response.url)
