import scrapy
from scrapy.spiders import CrawlSpider
from scrapy.contrib.linkextractors.sgml import SgmlLinkExtractor
from scrapy.contrib.spiders import Rule
from craigslist.items import CraigslistItem

class CraigslistSpider(CrawlSpider):
    name = 'craigslist'
    allowed_domains = ['craigslist.org']
    start_urls = [
        'http://denver.craigslist.org/search/vac'
    ]
    rules = (
        # Extract links matching 'http://www.sherdog.com/news/news/UFC-183-Results-Silva-vs-Diaz-PlaybyPlay-Updates-80853'
        # These links will take me to a particular article
        Rule(SgmlLinkExtractor(allow=('vac/\d+\.html', )),),
        Rule(SgmlLinkExtractor(allow=('search/vac\?s=\d+', )), ),
        Rule(SgmlLinkExtractor(allow=('reply/[a-zA-Z]+/vac/\d+', )), callback='parse_email_address'),
    )

    def parse_email_address(self, response):
        item = CraigslistItem()
        item['email_address'] = response.xpath('//a[@class="mailapp"]/text()').extract()[0]
        print item
        yield item
        # try:
        #     item['email_address'] = response.xpath('//a[@class="mailapp"]/text()').extract()[0]
        # except:
        #     item = None
        # print item
        # return item
        # craigslist_item = CraigslistItem()