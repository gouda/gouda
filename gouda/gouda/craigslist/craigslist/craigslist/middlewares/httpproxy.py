from scrapy.downloadermiddlewares.httpproxy import HttpProxyMiddleware
from django.template import Context, Template
from django.conf import settings
import tempfile
import shlex
import subprocess
import time
import os
settings.configure()

PRIVOXY_CONFIG_TEMPLATE = """
forward-socks5 / 127.0.0.1:9050 .
listen-address 127.0.0.1:8118

"""

class TorProxyMiddleware(HttpProxyMiddleware):
    def __init__(self):
        # self.privoxy_process = self._run_privoxy()
        super(TorProxyMiddleware, self).__init__()

    @staticmethod
    def _run_privoxy():
        config_file = tempfile.NamedTemporaryFile(delete=False)
        template = Template(PRIVOXY_CONFIG_TEMPLATE)
        config_file.write(template.render(Context({})))
        print 'PRIVOXY name is', config_file.name
        args = shlex.split('/usr/local/Cellar/privoxy/3.0.21/sbin/privoxy --no-daemon %s' % config_file.name)
        process = subprocess.Popen(args, stderr=open(os.devnull))
        time.sleep(3)
        return process
