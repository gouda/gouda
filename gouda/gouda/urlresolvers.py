from __future__ import absolute_import

import urllib
from HTMLParser import HTMLParser

from django.contrib.sites.models import Site
from django.core.urlresolvers import reverse
from urlparse import urljoin

SCHEME = 'https://'


def reverse_with_query(view_name, query_params, do_escape=True, **kwargs):
    url = reverse(view_name, **kwargs)
    if query_params:
        url += '?'
        if do_escape:
            url += urllib.urlencode(query_params)
        else:
            url += HTMLParser().unescape(urllib.urlencode(query_params)).replace('%2F', '/')
    return url


def get_absolute_url(view_name=None, url=None, **kwargs):
    if view_name:
        url = reverse(view_name, **kwargs)
    site = Site.objects.get_current()
    return urljoin('%s%s' % (SCHEME, site.domain), url)
