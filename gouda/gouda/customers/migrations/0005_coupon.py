# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0004_auto_20151031_1940'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coupon',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('code', models.CharField(unique=True, max_length=50)),
                ('amount_off', models.DecimalField(max_digits=10, decimal_places=2)),
                ('currency', models.IntegerField(default=35)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
