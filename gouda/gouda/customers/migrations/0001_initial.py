# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='BraintreeCustomer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('braintree_id', models.CharField(unique=True, max_length=100)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BraintreeMerchantAccount',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('braintree_id', models.CharField(unique=True, max_length=100, validators=[django.core.validators.RegexValidator(regex=b'\\S'), django.core.validators.MinLengthValidator(1)])),
                ('status', models.IntegerField(default=2)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='BraintreeWebhookNotification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('timestamp', models.DateTimeField(null=True, blank=True)),
                ('kind', models.CharField(max_length=255)),
                ('signature', models.CharField(max_length=255)),
                ('payload', models.TextField()),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StripeCard',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('last4', models.CharField(max_length=4)),
                ('brand', models.CharField(max_length=20)),
                ('funding', models.CharField(max_length=20)),
                ('exp_month', models.IntegerField()),
                ('exp_year', models.IntegerField()),
                ('country', models.CharField(max_length=20, blank=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('address_line1', models.CharField(max_length=200, blank=True)),
                ('address_line2', models.CharField(max_length=200, blank=True)),
                ('address_city', models.CharField(max_length=200, blank=True)),
                ('address_state', models.CharField(max_length=200, blank=True)),
                ('address_zip', models.CharField(max_length=200, blank=True)),
                ('address_country', models.CharField(max_length=200, blank=True)),
                ('cvc_check', models.CharField(max_length=200, blank=True)),
                ('address_line1_check', models.CharField(max_length=200, blank=True)),
                ('address_zip_check', models.CharField(max_length=200, blank=True)),
                ('dynamic_last4', models.CharField(max_length=200, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StripeCustomer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('stripe_id', models.CharField(unique=True, max_length=50)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StripeEvent',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('stripe_id', models.CharField(unique=True, max_length=40)),
                ('is_processed', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='StripePlan',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('interval', models.CharField(max_length=50)),
                ('name', models.CharField(max_length=200)),
                ('currency', models.CharField(max_length=10)),
                ('stripe_id', models.CharField(unique=True, max_length=200)),
                ('statement_descriptor', models.CharField(max_length=200, blank=True)),
                ('amount', models.IntegerField()),
                ('interval_count', models.IntegerField()),
                ('trial_period_days', models.IntegerField(null=True, blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
