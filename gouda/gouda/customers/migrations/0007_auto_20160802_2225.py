# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.customers.models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0006_braintreemerchantaccount_photo_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='braintreemerchantaccount',
            name='photo_id',
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='government_issued_id',
            field=models.ImageField(default=None, null=True, upload_to=b'customers/braintree_merchant_account/government_issued_ids/', validators=[gouda.customers.models.validate_image]),
        ),
    ]
