# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.AddField(
            model_name='stripecustomer',
            name='user',
            field=models.OneToOneField(related_name='stripe_customer', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='stripecard',
            name='stripe_customer',
            field=models.ForeignKey(to='customers.StripeCustomer'),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='user',
            field=models.OneToOneField(related_name='braintree_merchant_account', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AddField(
            model_name='braintreecustomer',
            name='user',
            field=models.OneToOneField(related_name='braintree_customer', to=settings.AUTH_USER_MODEL),
        ),
    ]
