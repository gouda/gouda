# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0002_auto_20151003_2123'),
    ]

    operations = [
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_dba_name',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_legal_name',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_locality',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_postal_code',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_region',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_street_address',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='business_tax_id',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='funding_account_number',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='funding_email',
            field=models.EmailField(default='', max_length=254),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='funding_mobile_phone',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='funding_routing_number',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_day',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_email',
            field=models.EmailField(default='afd@gmail.com', max_length=254),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_first_name',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_last_name',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_locality',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_month',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_phone',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_postal_code',
            field=models.CharField(default='', max_length=20),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_region',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_street_address',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_year',
            field=models.IntegerField(default='1234'),
            preserve_default=False,
        ),
    ]
