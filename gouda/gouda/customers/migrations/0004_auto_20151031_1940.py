# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0003_auto_20151031_1938'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='braintreemerchantaccount',
            name='individual_day',
        ),
        migrations.RemoveField(
            model_name='braintreemerchantaccount',
            name='individual_month',
        ),
        migrations.RemoveField(
            model_name='braintreemerchantaccount',
            name='individual_year',
        ),
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='individual_date_of_birth',
            field=models.DateField(default=datetime.datetime(2015, 10, 31, 19, 40, 38, 391947, tzinfo=utc)),
            preserve_default=False,
        ),
    ]
