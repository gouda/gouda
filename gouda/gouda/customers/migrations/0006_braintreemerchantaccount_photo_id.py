# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.customers.models


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0005_coupon'),
    ]

    operations = [
        migrations.AddField(
            model_name='braintreemerchantaccount',
            name='photo_id',
            field=models.ImageField(default=None, null=True, upload_to=b'customers/braintree_merchant_account/photo_ids/', validators=[gouda.customers.models.validate_image]),
        ),
    ]
