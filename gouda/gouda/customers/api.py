from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import json
import traceback
import sys

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework.views import APIView
from rest_framework.response import Response
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.conf import settings
import stripe
import braintree
from rest_framework import status
from gouda.customers.models import Coupon
from gouda.customers.models import BraintreeWebhookNotification
from gouda.customers.models import StripeCustomer
from gouda.customers.models import StripeEvent
from gouda.customers.serializers import BraintreeWebhookNotificationSerializer
from gouda.customers.serializers import StripeTokenSerializer
from gouda.customers.serializers import StripeEventSerializer
from djangorestframework_camel_case.render import CamelCaseJSONRenderer
from gouda.customers.utils import braintree
from gouda.photographers.keys import BookingSessionKeys


logger = logging.getLogger(__name__)


STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')


class BraintreeWebhookNotificationViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = BraintreeWebhookNotification.objects.all()
    serializer_class = BraintreeWebhookNotificationSerializer
    authentication_classes = []
    permission_classes = []

    """
    Braintree verification
    """
    def list(self, request, *args, **kwargs):
        bt_challenge = request.GET.get('bt_challenge')
        if bt_challenge:
            response = HttpResponse(braintree.WebhookNotification.verify(bt_challenge))
        else:
            response = super(BraintreeWebhookNotificationViewSet, self).list(request, *args, **kwargs)
        return response


class StripeEventViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = StripeEvent.objects.all()
    serializer_class = StripeEventSerializer
    authentication_classes = []
    permission_classes = []


class StripeCustomerViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = StripeCustomer.objects.all()
    serializer_class = StripeTokenSerializer

    def get_success_headers(self, data):
        success_headers = super(StripeCustomerViewSet, self).get_success_headers(data)
        success_headers['Location'] = reverse('customers:success')
        return success_headers

    def perform_create(self, serializer):
        return serializer.save()

    def get_serializer_context(self):
        context = super(StripeCustomerViewSet, self).get_serializer_context()
        context['request'] = self.request
        return context

    def create(self, request, *args, **kwargs):
        response = None
        try:
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)
            stripe_customer = self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            response = Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except stripe.error.InvalidRequestError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except stripe.error.AuthenticationError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except stripe.error.APIConnectionError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except stripe.error.StripeError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except Exception, e:
            traceback.print_exc(file=sys.stdout)
            response = Response({'error': 'Unknown Error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
            request.POST = request.data
            logger.exception('Failed to create stripe customer!', extra={'request': request})
        finally:
            return response


class StripeCouponVerificationView(APIView):
    def get(self, request, format=None, **kwargs):
        response = None
        try:
            stripe.api_key = STRIPE_SECRET_KEY
            coupon = stripe.Coupon.retrieve(kwargs.get('id'))
            coupon_data = json.dumps(coupon)
            response = Response(coupon)
        except stripe.error.InvalidRequestError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except stripe.error.AuthenticationError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except stripe.error.APIConnectionError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except stripe.error.StripeError, e:
            response = Response({'error': e.message}, status=e.http_status)
        except Exception, e:
            response = Response({'error': 'Unknown Error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        finally:
            return response


class BraintreeClientTokenView(APIView):
    def get(self, request, format=None, **kwargs):
        client_token = braintree.ClientToken.generate()
        return Response({'clientToken': client_token})


class CouponVerificationAPIView(APIView):
    def get(self, request, format=None, **kwargs):
        response = None
        request.session[BookingSessionKeys.coupon_id] = None
        try:
            coupon = Coupon.objects.get(code=kwargs.get('code'))
            response = Response({'amount_off': coupon.amount_off})
            request.session[BookingSessionKeys.coupon_id] = coupon.id
        except Exception, e:
            response = Response({'error': 'Unknown Error'}, status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        finally:
            return response
