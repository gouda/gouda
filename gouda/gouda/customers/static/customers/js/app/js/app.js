var CheckoutApp = angular.module('gouda.customers.apps.CheckoutApp', [
        'angularPayments',
        'ngMaterial',
        'ngSanitize',
        'ngAnimate',
        'kendo.directives',
        'gouda.customers.controllers',
        'gouda.customers.services'
    ]
);

CheckoutApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);
