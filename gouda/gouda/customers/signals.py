from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from django.dispatch import receiver
from gouda.customers.models import StripeEvent
from gouda.customers.models import BraintreeWebhookNotification
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.customers.utils import braintree
from django.conf import settings
import logging

logger = logging.getLogger(__name__)
SITE_DISPLAY_NAME = getattr(settings, 'SITE_DISPLAY_NAME')
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')


@receiver(post_save, sender=StripeEvent)
def handle_stripe_event(sender, **kwargs):
    from gouda.customers.tasks import process_stripe_event
    stripe_event = kwargs.get('instance')
    process_stripe_event.delay(stripe_event.stripe_id)


@receiver(post_save, sender=BraintreeWebhookNotification)
def handle_braintree_webhook_notification_event(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return
    from gouda.customers.tasks import process_braintree_webhook_notification
    braintree_webhook_notification = instance
    process_braintree_webhook_notification.delay(braintree_webhook_notification.id)


@receiver(pre_save, sender=BraintreeMerchantAccount)
def handle_braintree_merchant_account_pre_save(instance, **kwargs):
    braintree_merchant_account = instance
    # if instance.pk is not None:
    #     return

    descriptor = SITE_DISPLAY_NAME
    destination = braintree.MerchantAccount.FundingDestination.Bank
    tos_accepted = True

    # Payload
    payload = {
        'individual': {},
        'business': {},
        'funding': {
            'descriptor': descriptor,
            'destination': destination
        },
        'tos_accepted': tos_accepted,
        'master_merchant_account_id': BRAINTREE_MERCHANT_ACCOUNT_ID,
    }
    for a in dir(braintree_merchant_account):
        if a.startswith('funding_'):
            key = a.split('funding_')[1]
            payload['funding'][key] = getattr(braintree_merchant_account, a)
        elif a.startswith('individual_'):
            key = a.split('individual_')[1]
            payload['individual'][key] = getattr(braintree_merchant_account, a)
        elif a.startswith('business_'):
            key = a.split('business_')[1]
            payload['business'][key] = getattr(braintree_merchant_account, a)
    for key in payload.keys():
        if key not in ['individual', 'business']:
            continue
        data = payload.get(key)
        address = {
            'locality': data.pop('locality', None),
            'postal_code': data.pop('postal_code', None),
            'region': data.pop('region', None),
            'street_address': data.pop('street_address', None),
        }
        data['address'] = address

    # Get rid of business if not filled in...
    for key in payload['business'].keys():
        if not payload['business'][key]:
            payload.pop('business')
            break

    # Use Braintree to create/update a merchant account on their servers
    if braintree_merchant_account.braintree_id:
        payload.pop('tos_accepted')
        result = braintree.MerchantAccount.update(braintree_merchant_account.braintree_id, payload)
    else:
        result = braintree.MerchantAccount.create(payload)

    # Check result
    try:
        assert result.is_success
    except AssertionError:
        logger.exception('Failed to create a Braintree merchant account (%s)' % result.message)
        raise

    braintree_merchant_account.braintree_id = result.merchant_account.id
    braintree_merchant_account.status = BraintreeMerchantAccountStatusType.get(result.merchant_account.status).value
