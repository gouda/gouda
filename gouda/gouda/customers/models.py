from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import braintree
import stripe
from django.conf import settings
from django.contrib.gis.db import models
from django.contrib.sites.models import Site
from django.core.exceptions import ValidationError
from django.core.mail import EmailMessage
from django.core.validators import MinLengthValidator
from django.core.validators import RegexValidator
from django.db import models
from django.template import loader
from django_enumfield import enum
from gouda.context_processors import constants
from gouda.customers.managers import BraintreeCustomerManager
from gouda.customers.managers import StripeCustomerManager
from gouda.models import CurrencyType
from gouda.users.models import User

# Braintree
BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')
braintree.Configuration.configure(
    BRAINTREE_ENVIRONMENT,
    BRAINTREE_MERCHANT_ID,
    BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY
)

STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
stripe.api_key = STRIPE_SECRET_KEY
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')


def validate_image(obj):
    if obj.file.size > MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES * 1024 * 1024:
        raise ValidationError('Maximum image file size is %d MB' % MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES)


class BraintreeEscrowStatusType(enum.Enum):
    HOLD_PENDING = 0
    HELD = 1
    RELEASE_PENDING = 2
    RELEASED = 3
    REFUNDED = 4
    NONE = 9000

    labels = {
        HOLD_PENDING: 'hold_pending',
        HELD: 'held',
        RELEASE_PENDING: 'release_pending',
        RELEASED: 'released',
        REFUNDED: 'refunded',
        NONE: 'none'
    }


class BraintreeTransactionStatusType(enum.Enum):
    AUTHORIZATION_EXPIRED = 0
    AUTHORIZED = 1
    AUTHORIZING = 2
    SETTLEMENT_PENDING = 3
    SETTLEMENT_CONFIRMED = 4
    SETTLEMENT_DECLINED = 5
    FAILED = 6
    GATEWAY_REJECTED = 7
    PROCESSOR_DECLINED = 8
    SETTLED = 9
    SETTLING = 10
    SUBMITTED_FOR_SETTLEMENT = 11
    VOIDED = 12
    LOCKED = 13  # Our flag...
    NONE = 9000

    labels = {
        AUTHORIZATION_EXPIRED: 'authorization_expired',
        AUTHORIZED: 'authorized',
        AUTHORIZING: 'authorizing',
        SETTLEMENT_PENDING: 'settlement_pending',
        SETTLEMENT_CONFIRMED: 'settlement_confirmed',
        SETTLEMENT_DECLINED: 'settlement_declined',
        FAILED: 'failed',
        GATEWAY_REJECTED: 'gateway_rejected',
        PROCESSOR_DECLINED: 'processor_declined',
        SETTLED: 'settled',
        SETTLING: 'settling',
        SUBMITTED_FOR_SETTLEMENT: 'submitted_for_settlement',
        VOIDED: 'voided',
        LOCKED: 'processing',
        NONE: 'none'
    }


class BraintreeMerchantAccountStatusType(enum.Enum):
    ACTIVE = 0
    SUSPENDED = 1
    PENDING = 2

    labels = {
        ACTIVE: 'Active',
        SUSPENDED: 'Suspended',
        PENDING: 'Pending'
    }


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class StripePlan(BaseModel):
    interval = models.CharField(max_length=50)
    name = models.CharField(max_length=200)
    currency = models.CharField(max_length=10)
    stripe_id = models.CharField(max_length=200, unique=True)
    statement_descriptor = models.CharField(max_length=200, blank=True)
    amount = models.IntegerField()
    interval_count = models.IntegerField()
    trial_period_days = models.IntegerField(null=True, blank=True)

    @property
    def dollar_amount(self):
        return float(self.amount) / 100.0

    def __unicode__(self):
        return self.name


class StripeCustomer(BaseModel):
    user = models.OneToOneField(User, related_name='stripe_customer')
    stripe_id = models.CharField(blank=False, max_length=50, unique=True)

    objects = StripeCustomerManager()

    def __unicode__(self):
        return '%s, %s' % (self.user.email_address, self.stripe_id)


class StripeCard(BaseModel):
    stripe_customer = models.ForeignKey(StripeCustomer)
    last4 = models.CharField(max_length=4)
    brand = models.CharField(max_length=20)
    funding = models.CharField(max_length=20)
    exp_month = models.IntegerField()
    exp_year = models.IntegerField()
    country = models.CharField(max_length=20, blank=True)
    name = models.CharField(max_length=100, blank=True)
    address_line1 = models.CharField(max_length=200, blank=True)
    address_line2 = models.CharField(max_length=200, blank=True)
    address_city = models.CharField(max_length=200, blank=True)
    address_state = models.CharField(max_length=200, blank=True)
    address_zip = models.CharField(max_length=200, blank=True)
    address_country = models.CharField(max_length=200, blank=True)
    cvc_check = models.CharField(max_length=200, blank=True)
    address_line1_check = models.CharField(max_length=200, blank=True)
    address_zip_check = models.CharField(max_length=200, blank=True)
    dynamic_last4 = models.CharField(max_length=200, blank=True)


class StripeEvent(BaseModel):
    stripe_id = models.CharField(max_length=40, unique=True)
    is_processed = models.BooleanField(default=False)

    def __unicode__(self):
        return self.stripe_id


class BraintreeWebhookNotification(BaseModel):
    timestamp = models.DateTimeField(null=True, blank=True)
    kind = models.CharField(max_length=255, blank=False)
    signature = models.CharField(max_length=255, blank=False)
    payload = models.TextField(blank=False)

    def __unicode__(self):
        return self.kind

    def send_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'braintree_webhook_notification': self,
        })
        context.update(constants(None))
        # Render and send notification email
        subject = loader.render_to_string('customers/emails/braintree_webhook_notification/notification/subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('customers/emails/braintree_webhook_notification/notification/body.txt', context)
        email_message = EmailMessage(
            subject=subject,
            body=body,
            from_email=SERVER_EMAIL,
            to=STAFF_EMAILS,
        )
        email_message.send()


class BraintreeMerchantAccount(BaseModel):
    user = models.OneToOneField(User, related_name='braintree_merchant_account')

    individual_email = models.EmailField()
    individual_first_name = models.CharField(max_length=500)
    individual_last_name = models.CharField(max_length=500)
    individual_phone = models.CharField(max_length=500)
    individual_locality = models.CharField(max_length=500)
    individual_postal_code = models.CharField(max_length=20)
    individual_region = models.CharField(max_length=500)
    individual_street_address = models.CharField(max_length=500)
    individual_date_of_birth = models.DateField()

    business_locality = models.CharField(max_length=500, blank=True)
    business_postal_code = models.CharField(max_length=500, blank=True)
    business_region = models.CharField(max_length=500, blank=True)
    business_street_address = models.CharField(max_length=500, blank=True)
    business_dba_name = models.CharField(max_length=500, blank=True)
    business_legal_name = models.CharField(max_length=500, blank=True)
    business_tax_id = models.CharField(max_length=500, blank=True)

    funding_email = models.EmailField()
    funding_mobile_phone = models.CharField(max_length=500)
    funding_account_number = models.CharField(max_length=500)
    funding_routing_number = models.CharField(max_length=500)

    braintree_id = models.CharField(blank=False, max_length=100, unique=True, validators=[RegexValidator(regex=r'\S'), MinLengthValidator(1)])
    status = enum.EnumField(BraintreeMerchantAccountStatusType, blank=False, default=BraintreeMerchantAccountStatusType.PENDING)

    government_issued_id = models.ImageField(default=None, null=True, upload_to='customers/braintree_merchant_account/government_issued_ids/', validators=[validate_image])

    def __unicode__(self):
        return '%s, %s' % (self.user.email_address, self.braintree_id)

    @property
    def has_business(self):
        attributes = [
            'business_locality',
            'business_postal_code',
            'business_region',
            'business_street_address',
            'business_dba_name',
            'business_legal_name',
            'business_tax_id',
        ]
        for a in attributes:
            if getattr(self, a) == '':
                return False
        return True


class BraintreeCustomer(BaseModel):
    user = models.OneToOneField(User, related_name='braintree_customer')
    braintree_id = models.CharField(blank=False, max_length=100, unique=True)

    objects = BraintreeCustomerManager()

    def __unicode__(self):
        return '%s, %s' % (self.user.email_address, self.braintree_id)

    @property
    def default_credit_card(self):
        customer = braintree.Customer.find(self.braintree_id)
        credit_cards = [c for c in customer.credit_cards if c.default]
        if credit_cards:
            return credit_cards[0]
        return None


class Coupon(BaseModel):
    code = models.CharField(unique=True, max_length=50)
    amount_off = models.DecimalField(max_digits=10, decimal_places=2)
    currency = enum.EnumField(CurrencyType, blank=False, default=CurrencyType.USD)

    def __str__(self):
        return '%s' % self.code


class BraintreeTransaction(BaseModel):
    braintree_id = models.CharField(blank=False, max_length=100)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    service_fee_amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, verbose_name='Amount withheld')
    transaction_status = enum.EnumField(BraintreeTransactionStatusType, blank=False, default=BraintreeTransactionStatusType.NONE)
    escrow_status = enum.EnumField(BraintreeEscrowStatusType, null=True, blank=True, default=BraintreeEscrowStatusType.NONE)
    currency = enum.EnumField(CurrencyType, blank=False, default=CurrencyType.USD)

    def release_from_escrow(self):
        old_transaction_status = self.transaction_status
        self.transaction_status = BraintreeTransactionStatusType.LOCKED
        self.save()
        try:
            result = braintree.Transaction.release_from_escrow(self.braintree_id)
            assert result.is_success
            self.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
            self.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None
            self.save()
        except:
            self.transaction_status = old_transaction_status
            self.save()
            logger.exception('Failed to release transaction from escrow (%d, %s)' % (self.id, self.braintree_id))
            raise AssertionError(result.message)

    @property
    def sub_merchant_payout(self):
        return self.amount - self.service_fee_amount

    @property
    def payout_status(self):
        # key = hashlib.sha224('gouda.customers.models.braintree_transaction.transaction.%d' % (self.id, ))
        # transaction = cache.get(key, None)
        # if not transaction:
        #     try:
        #         transaction = self.update_braintree_data()
        #         cache.set(key, simplejson.dumps(transaction), timeout=60 * 60 * 4)
        #     except:
        #         pass
        if self.transaction_status == BraintreeTransactionStatusType.SETTLED or self.transaction_status == BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT:
            return 'Settled'
        return 'Pending'

    def update_braintree_data(self):
        old_transaction_status = self.transaction_status
        self.transaction_status = BraintreeTransactionStatusType.LOCKED
        self.save()
        try:
            transaction = braintree.Transaction.find(self.braintree_id)
            assert transaction is not None
            # logger.info(transaction)
            # logger.info(transaction.status)
            # logger.info(transaction.escrow_status)
            self.transaction_status = BraintreeTransactionStatusType.get(transaction.status).value
            self.escrow_status = BraintreeEscrowStatusType.get(
                transaction.escrow_status).value if transaction.escrow_status else None
            self.save()
            return transaction
        except:
            self.transaction_status = old_transaction_status
            self.save()
            logger.exception('Failed to update transaction (%d, %s)' % (self.id, self.braintree_id))
            raise

    class Meta:
        abstract = True


from gouda.customers.signals import *
