from __future__ import absolute_import

from datetime import date
from datetime import timedelta

from django import forms
from gouda.users.models import User
from gouda.models import MonthType
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccountStatusType
from django.conf import settings
from gouda.forms.widgets import KendoDropDownList
from gouda.forms.widgets import KendoTextInput
from gouda.forms.widgets import TextInput
import braintree
from django.core.validators import RegexValidator
from django.core.validators import MaxValueValidator
from localflavor.us.forms import USStateField
from localflavor.us.forms import USZipCodeField
from localflavor.us.us_states import STATE_CHOICES
from django_countries.fields import countries
from gouda.forms.widgets import TextInput
import re
import logging
from gouda.forms.widgets import FileInput

logger = logging.getLogger(__name__)

# Braintree
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')
BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')
braintree.Configuration.configure(
    BRAINTREE_ENVIRONMENT,
    BRAINTREE_MERCHANT_ID,
    BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY
)


class PhoneNumberValidator(RegexValidator):
    regex = r'^[0-9\(\)\+\-\.\s]+$'


class PostalCodeValidator(RegexValidator):
    regex = r'^(\d{5}|\d{5}\-\d{4})$'


class BusinessNameValidator(RegexValidator):
    regex = r'^[0-9a-zA-Z\s&\-\!@#\$\(\)\'\./\+,"]+$'


class DateOfBirthValidator(MaxValueValidator):
    def __init__(self, min_age, message=None):
        limit_value = date.today() - timedelta(days=min_age * 365)
        super(DateOfBirthValidator, self).__init__(limit_value, message)


class TaxIDValidator(RegexValidator):
    regex = r'^\d{9}$'


class BraintreeCustomerForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.Select(attrs={'class': 'hidden'}))
    payment_method_nonce = forms.CharField(widget=TextInput(is_required=False, attrs={'class': 'hidden'}))
    cardholder_name = forms.CharField(max_length=500, widget=KendoTextInput('cardholderName'))

    # Billing address
    full_name = forms.CharField(max_length=500, widget=KendoTextInput('fullName'))
    street_address = forms.CharField(max_length=500, widget=KendoTextInput('streetAddress'))
    sub_premise = forms.CharField(required=False, max_length=500, widget=KendoTextInput('subPremise', is_required=False))
    postal_code = forms.CharField(required=False, max_length=20, widget=KendoTextInput('postalCode', is_required=False))
    locality = forms.CharField(max_length=500, widget=KendoTextInput('locality'))
    region = forms.CharField(max_length=500, widget=KendoTextInput('region'))
    country = forms.ChoiceField(choices=countries, widget=KendoDropDownList('country', choices=countries))

    def save(self):
        user = self.cleaned_data.get('user')
        payment_method_nonce = self.cleaned_data.get('payment_method_nonce')
        toks = self.cleaned_data.get('full_name').split()
        first_name = toks[0]
        last_name = ' '.join(toks[1:]) if len(toks) > 1 else None
        billing_address = {
            'first_name': first_name,
            'last_name': last_name,
            'street_address': self.cleaned_data.get('street_address'),
            'extended_address': self.cleaned_data.get('sub_premise'),
            'postal_code': self.cleaned_data.get('postal_code'),
            'locality': self.cleaned_data.get('locality'),
            'region': self.cleaned_data.get('region'),
            'country_code_alpha3': countries.alpha3(self.cleaned_data.get('country')),
        }

        # Update or create a Braintree customer
        braintree_customer = None
        # try:
        braintree_customer, is_created = BraintreeCustomer.objects.update_or_create_braintree_customer(
            user=user,
            payment_method_nonce=payment_method_nonce,
            cardholder_name=self.cleaned_data.get('cardholder_name'),
            billing_address=billing_address
        )
        # except AssertionError:
        #     # booking.delete()
        #     self.add_error(None, 'Error processing payment details.')
        #     raise
        return braintree_customer


class BraintreeMerchantAccountVerificationForm(forms.ModelForm):
    class Meta:
        model = BraintreeMerchantAccount
        fields = [
            'government_issued_id'
        ]
        widgets = {
            'government_issued_id': FileInput(attrs={'class': 'hidden'}),
        }


class BraintreeMerchantAccountForm(forms.Form):
    YEAR_ERROR_MESSAGES = {
        'min_value': 'Invalid year',
        'max_value': 'Invalid year',
    }

    DAY_ERROR_MESSAGES = {
        'min_value': 'Invalid day',
        'max_value': 'Invalid day',
    }

    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.Select(attrs={'class': 'hidden'}))
    status = forms.CharField(required=False, widget=TextInput(attrs={'readonly': True}))

    individual_locality = forms.CharField(max_length=255, widget=TextInput(attrs={'placeholder': 'e.g. Harlan'}))
    individual_postal_code = USZipCodeField(min_length=5, max_length=10, widget=TextInput(attrs={
        'placeholder': 'e.g. 40831',
        'data-parsley-pattern': '%s' % PostalCodeValidator().regex.pattern
    }))
    individual_region = USStateField(widget=forms.Select(choices=STATE_CHOICES))
    individual_street_address = forms.CharField(max_length=255, widget=TextInput(attrs={
        'data-parsley-maxlength': 255,
        'placeholder': 'e.g. 123 Main St.'
    }))
    individual_month = forms.ChoiceField(widget=forms.Select(choices=MonthType.choices()), choices=MonthType.choices())
    individual_day = forms.IntegerField(error_messages=DAY_ERROR_MESSAGES, min_value=1, max_value=31, widget=TextInput(attrs={
        'data-parsley-min': 1,
        'data-parsley-max': 31,
        'type': 'number'
    }))
    individual_year = forms.IntegerField(error_messages=YEAR_ERROR_MESSAGES, min_value=1900, max_value=date.today().year, widget=TextInput(attrs={
        'data-parsley-min': 1900,
        'data-parsley-max': date.today().year,
        'type': 'number',
        'placeholder': 'e.g. 1987'
    }))
    individual_email = forms.EmailField(widget=TextInput(attrs={
        'data-parsley-maxlength': 255,
        'placeholder': 'Enter your email address',
        'type': 'email'
    }))
    individual_first_name = forms.CharField(max_length=255, widget=TextInput(attrs={
        'data-parsley-maxlength': 255,
        'placeholder': 'Enter your first name'
    }))
    individual_last_name = forms.CharField(max_length=255, widget=TextInput(attrs={
        'data-parsley-maxlength': 255,
        'placeholder': 'Enter your last name'
    }))
    individual_phone = forms.CharField(widget=TextInput(attrs={
        'placeholder': 'e.g. 555-555-5555',
        'data-parsley-pattern': '%s' % PhoneNumberValidator().regex.pattern
    }))

    business_locality = forms.CharField(required=False, max_length=255, widget=TextInput(is_required=False, attrs={
        'data-parsley-maxlength': 255,
    }))
    business_postal_code = USZipCodeField(required=False, min_length=5, max_length=10, widget=TextInput(is_required=False, attrs={
        'placeholder': 'e.g. 40831',
        'data-parsley-minlength': 5,
        'data-parsley-maxlength': 10,
        'data-parsley-pattern': '%s' % PostalCodeValidator().regex.pattern,
    }))
    business_region = USStateField(required=False, widget=forms.Select(choices=STATE_CHOICES))
    business_street_address = forms.CharField(required=False, max_length=255, widget=TextInput(is_required=False, attrs={
        'data-parsley-maxlength': 255,
        'placeholder': 'e.g. 123 Main St.',
    }))
    business_dba_name = forms.CharField(required=False, max_length=40, validators=[BusinessNameValidator()], widget=TextInput(is_required=False, attrs={
        'data-parsley-maxlength': 40,
        'data-parsley-pattern': '%s' % BusinessNameValidator().regex.pattern,
    }))
    business_legal_name = forms.CharField(required=False, max_length=40, validators=[BusinessNameValidator()], widget=TextInput(is_required=False, attrs={
        'data-parsley-maxlength': 40,
        'data-parsley-pattern': '%s' % BusinessNameValidator().regex.pattern,
    }))
    business_tax_id = forms.CharField(required=False, min_length=9, max_length=9, validators=[TaxIDValidator()], widget=TextInput(is_required=False, attrs={
        'data-parsley-minlength': 9,
        'data-parsley-maxlength': 9,
        'data-parsley-pattern': '%s' % TaxIDValidator().regex.pattern,
    }))

    funding_email = forms.EmailField(widget=TextInput(is_required=False, attrs={'class': 'hidden'}))
    funding_mobile_phone = forms.CharField(validators=[PhoneNumberValidator()], widget=TextInput(is_required=False, attrs={
        'class': 'hidden',
        'data-parsley-pattern': '%s' % PhoneNumberValidator().regex.pattern
    }))
    funding_account_number = forms.CharField(validators=[RegexValidator(r'^\d+$')], widget=TextInput(attrs={
        'data-parsley-pattern': '^\d+$'
    }))
    funding_routing_number = forms.CharField(validators=[RegexValidator(r'^\d{9}$')], widget=TextInput(attrs={
        'data-parsley-pattern': '^\d{9}$'
    }))

    def __init__(self, is_readonly=False, *args, **kwargs):
        super(BraintreeMerchantAccountForm, self).__init__(*args, **kwargs)
        if is_readonly:
            for field in self.fields:
                self.fields.get(field).widget.attrs['readonly'] = True

    def _clean_phone_number(self, phone_number):
        return re.sub('\D', '', phone_number)

    def clean_individual_phone(self):
        phone_number = self.cleaned_data['individual_phone']
        return self._clean_phone_number(phone_number)

    def clean_funding_mobile_phone(self):
        phone_number = self.cleaned_data['funding_mobile_phone']
        return self._clean_phone_number(phone_number)

    def clean(self):
        cleaned_data = super(BraintreeMerchantAccountForm, self).clean()
        try:
            cleaned_data['individual_date_of_birth'] = date(
                year=int(cleaned_data.get('individual_year')),
                month=int(cleaned_data.get('individual_month')),
                day=int(cleaned_data.get('individual_day'))
            )
            DateOfBirthValidator(18, 'To sign up, you must be 18 or older.').__call__(cleaned_data['individual_date_of_birth'])
        except:
            raise forms.ValidationError({'individual_year': 'Please enter a valid date of birth.'})

        cleaned_data.pop('individual_year')
        cleaned_data.pop('individual_month')
        cleaned_data.pop('individual_day')
        cleaned_data.pop('status')

        return cleaned_data

    def save(self):
        cleaned_data = self.cleaned_data.copy()
        user = cleaned_data.pop('user')
        braintree_merchant_account, did_create = BraintreeMerchantAccount.objects.update_or_create(defaults=cleaned_data, user=user)
        return braintree_merchant_account
