from __future__ import absolute_import

from django.db import models
import stripe
from django.conf import settings
import braintree
import logging

logger = logging.getLogger(__name__)

# Braintree
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')
BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')
braintree.Configuration.configure(
    BRAINTREE_ENVIRONMENT,
    BRAINTREE_MERCHANT_ID,
    BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY
)

# Stripe setup
STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
stripe.api_key = STRIPE_SECRET_KEY


class StripeCustomerManager(models.Manager):
    def update_or_create_stripe_customer(self, user, stripe_token):
        if hasattr(user, 'stripe_customer'):
            stripe_customer = user.stripe_customer
            customer = stripe.Customer.retrieve(stripe_customer.stripe_id)
            customer.source = stripe_token
            customer.save()
        else:
            # Use Stripe to build a customer on their servers
            customer = stripe.Customer.create(
                source=stripe_token,
                email=user.email_address,
            )
        return super(StripeCustomerManager, self).update_or_create(user=user, defaults={
            'stripe_id': customer.id
        })


class BraintreeCustomerManager(models.Manager):
    def update_or_create_braintree_customer(self, user, payment_method_nonce, cardholder_name=None, billing_address=None):
        if hasattr(user, 'braintree_customer'):
            braintree_customer = user.braintree_customer
            payload = {
                'credit_card': {
                    'payment_method_nonce': payment_method_nonce,
                    'cardholder_name': cardholder_name,
                    'options': {
                        'make_default': True
                    }
                }
            }
            if billing_address:
                payload['credit_card']['billing_address'] = billing_address
            result = braintree.Customer.update(braintree_customer.braintree_id, payload)
        else:
            # Use Braintree to build a customer on their servers
            payload = {
                'email': user.email_address,
                'payment_method_nonce': payment_method_nonce
            }
            if billing_address:
                payload['credit_card'] = {
                    'billing_address': billing_address,
                    'cardholder_name': cardholder_name,
                    'options': {
                        'make_default': True
                    }
                }
            result = braintree.Customer.create(payload)
        assert result.is_success, result.message
        return super(BraintreeCustomerManager, self).update_or_create(user=user, defaults={
            'braintree_id': result.customer.id
        })
