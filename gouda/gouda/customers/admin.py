from django.contrib import admin

import logging
from gouda.customers.models import StripeCustomer
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import Coupon
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.customers.utils import braintree

logger = logging.getLogger(__name__)


@admin.register(Coupon)
class CouponAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'
    list_display = ('code', 'amount_off', 'date_created')


@admin.register(BraintreeCustomer)
class BraintreeCustomerAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'
    list_display = ('user', 'braintree_id', 'date_created')
    readonly_fields = (
        'user',
        'braintree_id',
    )


@admin.register(BraintreeMerchantAccount)
class BraintreeMerchantAccountAdmin(admin.ModelAdmin):
    date_hierarchy = 'date_created'
    list_display = ('user', 'braintree_id', 'status', 'date_created')
    readonly_fields = (
        'user',
        'braintree_id',
        'status',
    )
    fieldsets = (
        (None, {
            'fields': (
                'user',
                'braintree_id',
                'status',
                'government_issued_id'
            )
        }),
        ('Individual Details', {
            'fields': (
                'individual_email',
                'individual_first_name',
                'individual_last_name',
                'individual_phone',
                'individual_locality',
                'individual_postal_code',
                'individual_region',
                'individual_street_address',
                'individual_date_of_birth',
            )
        }),
        ('Business Details', {
            'fields': (
                'business_locality',
                'business_postal_code',
                'business_region',
                'business_street_address',
                'business_dba_name',
                'business_legal_name',
                'business_tax_id',
            )
        }),
        ('Funding Details', {
            'fields': (
                'funding_email',
                'funding_mobile_phone',
                # 'funding_account_number',
                # 'funding_routing_number',
            )
        }),
    )
    actions = ['update']

    def update(self, request, queryset):
        num_updated = 0
        for braintree_merchant_account in queryset:
            try:
                merchant_account = braintree.MerchantAccount.find(braintree_merchant_account.braintree_id)
                assert merchant_account is not None
                logger.info(merchant_account)
                logger.info(merchant_account.status)
                braintree_merchant_account.status = BraintreeMerchantAccountStatusType.get(merchant_account.status).value
                braintree_merchant_account.save()
            except:
                logger.exception('Failed to update merchant account (%d, %s)' % (braintree_merchant_account.id, braintree_merchant_account.braintree_id))
                raise
            num_updated += 1
        self.message_user(request, '%d Braintree merchant accounts were successfully updated.' % num_updated)
    update.short_description = 'Update selected Braintree merchant accounts'
