from __future__ import absolute_import

import stripe
from django.core.management.base import BaseCommand
from gouda.customers.models import StripePlan
from django.conf import settings

STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
MONTHLY_PLAN_ID = getattr(settings, 'MONTHLY_PLAN_ID')
YEARLY_PLAN_ID = getattr(settings, 'YEARLY_PLAN_ID')


class Command(BaseCommand):
    def handle(self, *args, **options):
        StripePlan.objects.all().delete()
        stripe.api_key = STRIPE_SECRET_KEY
        monthly_plan_data = stripe.Plan.retrieve(MONTHLY_PLAN_ID).__dict__.get('_previous').copy()
        yearly_plan_data = stripe.Plan.retrieve(YEARLY_PLAN_ID).__dict__.get('_previous').copy()
        for plan_data in list([monthly_plan_data, yearly_plan_data]):
            plan_data.pop('created', None)
            plan_data.pop('object', None)
            plan_data.pop('livemode', None)
            plan_data.pop('metadata', None)
            plan_data['stripe_id'] = plan_data.pop('id', None)
            if not plan_data.get('statement_descriptor'):
                plan_data['statement_descriptor'] = ''
            plan = StripePlan.objects.create(**plan_data)
