from __future__ import absolute_import

import random

from gouda.users.models import User
from gouda.customers.models import StripeCustomer
from gouda.customers.models import StripePlan
from django.core.management.base import BaseCommand
from django.conf import settings
import stripe

STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
stripe.api_key = STRIPE_SECRET_KEY


class Command(BaseCommand):
    def handle(self, *args, **options):
        email_address = 'sterlingarcher%d@gmail.com' % random.randint(0, 9000)
        # Create Stripe token
        stripe_token = stripe.Token.create(
            card={
                "number": '4242424242424242',
                "exp_month": 12,
                "exp_year": 2050,
                "cvc": '123'
            },
        )

        # Create Stripe customer
        stripe_customer = stripe.Customer.create(
            source=stripe_token.id,
            plan=StripePlan.objects.all()[0].stripe_id,
            email=email_address
        )
        print stripe_customer

        # Create user
        user = User.objects.create_user(
            email_address=email_address,
            password='sterlingarcher123',
            full_name='Sterling Archer'
        )

        # Add Stripe customer to database
        stripe_customer = StripeCustomer.objects.create(user=user, stripe_id=stripe_customer.id)
        print stripe_customer