from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.views.generic.edit import FormView
from django.core.urlresolvers import reverse_lazy
from gouda.views import LoginRequiredMixin
from gouda.views import NeverCacheMixin
from django import forms
from gouda.customers.forms import BraintreeCustomerForm
from django.contrib.auth.decorators import user_passes_test


class BraintreeMerchantAccountRequiredMixin(object):
    @staticmethod
    def has_braintree_merchant_account(user):
        if hasattr(user, 'braintree_merchant_account') and user.braintree_merchant_account:
            return True
        return False

    @classmethod
    def as_view(cls, **initkwargs):
        view = super(BraintreeMerchantAccountRequiredMixin, cls).as_view(**initkwargs)
        return user_passes_test(test_func=BraintreeMerchantAccountRequiredMixin.has_braintree_merchant_account)(view)


class BraintreeCustomerCreateView(NeverCacheMixin, LoginRequiredMixin, FormView):
    form_class = BraintreeCustomerForm
    braintree_customer = None

    def form_valid(self, form):
        response = super(BraintreeCustomerCreateView, self).form_valid(form)
        self.braintree_customer = form.save()
        return response
