from __future__ import absolute_import

import braintree
from django.conf import settings
from gouda.profiles.utils import tear_down_profiles
from gouda.users.models import User
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccountStatusType
from braintree.test.nonces import Nonces
from datetime import date
from datetime import timedelta


BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')

# Braintree
braintree.Configuration.configure(
    BRAINTREE_ENVIRONMENT,
    BRAINTREE_MERCHANT_ID,
    BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY
)


def create_customers():
    for user in User.objects.all():
        BraintreeCustomer.objects.update_or_create_braintree_customer(user, Nonces.Transactable)
        braintree_merchant_account = BraintreeMerchantAccount.objects.create(**{
            'user': user,
            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'CA',
            'individual_street_address': '1504 Parkpath Way',
            'individual_date_of_birth': date.today() - timedelta(days=365 * 28),
            'individual_email': user.email_address,
            'individual_first_name': user.first_name,
            'individual_last_name': user.last_name,
            'individual_phone': '4437421240',

            'business_locality': 'Bakersfield',
            'business_postal_code': '93311',
            'business_region': 'CA',
            'business_street_address': '1504 Parkpath Way',
            'business_dba_name': 'Danger Zone R\' Us',
            'business_legal_name': 'Danger Zone R\' Us',
            'business_tax_id': '123457689',

            'funding_email': user.email_address,
            'funding_mobile_phone': '5555555555',
            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        })
        braintree_merchant_account.status = BraintreeMerchantAccountStatusType.ACTIVE
        braintree_merchant_account.save()


def tear_down_customers():
    BraintreeMerchantAccount.objects.all().delete()
    BraintreeCustomer.objects.all().delete()
    tear_down_profiles()