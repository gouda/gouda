{% autoescape off %}Timestamp: {{ webhook_notification.timestamp|date:"c" }}
ID: {{ merchant_account.id }}
Full Name: {{ merchant_account.individual_details.first_name }} {{ merchant_account.individual_details.last_name }}
Email Address: {{ merchant_account.individual_details.email }}
{% endautoescape %}