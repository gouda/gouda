from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import datetime

from gouda.celeryapp import app
from django.conf import settings
from gouda.users.models import User
from gouda.customers.models import StripeEvent
import stripe
from celery.utils.log import get_task_logger
from requests.packages.urllib3 import disable_warnings
from psycopg2.extras import DateTimeTZRange
from gouda.customers.models import BraintreeTransaction
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.customers.models import BraintreeWebhookNotification
from gouda.customers.utils import braintree
from dateutil.parser import parse as parse_date
from django.core.mail import send_mail
from django.template import loader
from django.contrib.sites.models import Site
from celery import Task
from django.utils import timezone
from datetime import timedelta


STAFF = getattr(settings, 'STAFF')
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')

stripe.api_key = STRIPE_SECRET_KEY
logger = get_task_logger(__name__)
disable_warnings()


def process_payment_succeeded(user, customer, event):
    pass
    # # Update the user's current period...
    # current_period_start = datetime.utcfromtimestamp(customer.subscriptions.get('data')[0].get('current_period_start'))
    # current_period_end = datetime.utcfromtimestamp(customer.subscriptions.get('data')[0].get('current_period_end'))
    # user.stripe_customer.current_period = DateTimeTZRange(current_period_start, current_period_end)
    # user.stripe_customer.save()


@app.task(bind=True)
def process_stripe_event(self, event_id):
    # Verify Stripe event
    if not StripeEvent.objects.filter(stripe_id=event_id, is_processed=False).exists():
        logger.error('Received an invalid Stripe event')
        return
    event = stripe.Event.retrieve(
        id=event_id
    )

    # Get Stripe customer and verify
    user = None
    customer = None
    if 'customer' in event.data.get('object'):
        customer = stripe.Customer.retrieve(
            id=event.data.get('object').get('customer')
        )
        try:
            user = User.objects.get(stripe_customer__stripe_id=customer.id)
        except User.DoesNotExist:
            logger.exception('Received Stripe event for a user that does not exist')
            return

    # logger.info(event.type)
    if event.type == 'invoice.payment_succeeded':
        process_payment_succeeded(user, customer, event)

    StripeEvent.objects.filter(stripe_id=event_id).update(is_processed=True)


@app.task
def process_braintree_webhook_notification(id):
    braintree_webhook_notification = BraintreeWebhookNotification.objects.get(id=id)
    webhook_notification = braintree.WebhookNotification.parse(
        braintree_webhook_notification.signature,
        braintree_webhook_notification.payload
    )

    # Save metadata
    braintree_webhook_notification.kind = webhook_notification.kind
    braintree_webhook_notification.timestamp = webhook_notification.timestamp
    braintree_webhook_notification.save()

    # Handle a merchant account
    merchant_account_kinds = [
        braintree.WebhookNotification.Kind.SubMerchantAccountApproved,
        braintree.WebhookNotification.Kind.SubMerchantAccountDeclined
    ]
    disbursement_kinds = [
        braintree.WebhookNotification.Kind.DisbursementException,
    ]
    dispute_kinds = [
        braintree.WebhookNotification.Kind.DisputeOpened,
        braintree.WebhookNotification.Kind.DisputeWon,
        braintree.WebhookNotification.Kind.DisputeLost,
    ]
    if webhook_notification.kind in merchant_account_kinds:
        merchant_account = braintree.MerchantAccount.find(webhook_notification.merchant_account.id)
        # logger.info(merchant_account)
        try:
            braintree_merchant_account = BraintreeMerchantAccount.objects.get(braintree_id=merchant_account.id)
        except BraintreeMerchantAccount.DoesNotExist:
            logger.exception('Failed to find a matching Braintree merchant account with id (%s)' % merchant_account.id)
            raise

        # Build context
        current_site = Site.objects.get_current()
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'webhook_notification': webhook_notification,
            'merchant_account': merchant_account
        }

        if webhook_notification.kind == braintree.WebhookNotification.Kind.SubMerchantAccountApproved:
            braintree_merchant_account.status = BraintreeMerchantAccountStatusType.ACTIVE

            # Render

            subject = loader.render_to_string('customers/emails/braintree_webhook_notification/merchant_account_approved/subject.txt', context)
            subject = ''.join(subject.splitlines())
            body = loader.render_to_string('customers/emails/braintree_webhook_notification/merchant_account_approved/body.txt', context)

            # Send
            send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)
        elif webhook_notification.kind == braintree.WebhookNotification.Kind.SubMerchantAccountDeclined:
            braintree_merchant_account.status = BraintreeMerchantAccountStatusType.SUSPENDED

            # Render
            subject = loader.render_to_string('customers/emails/braintree_webhook_notification/merchant_account_declined/subject.txt', context)
            subject = ''.join(subject.splitlines())
            body = loader.render_to_string('customers/emails/braintree_webhook_notification/merchant_account_declined/body.txt', context)

            # Send
            send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)

        braintree_merchant_account.save()
    elif webhook_notification.kind in disbursement_kinds:
        disbursement = webhook_notification.disbursement
        try:
            merchant_account = braintree.MerchantAccount.find(disbursement.merchant_account.id)
        except:
            merchant_account = None

        # Build context
        current_site = Site.objects.get_current()
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'webhook_notification': webhook_notification,
            'disbursement': disbursement,
            'merchant_account': merchant_account
        }

        if webhook_notification.kind == braintree.WebhookNotification.Kind.DisbursementException:
            # Render
            subject = loader.render_to_string('customers/emails/braintree_webhook_notification/disbursement_exception/subject.txt', context)
            subject = ''.join(subject.splitlines())
            body = loader.render_to_string('customers/emails/braintree_webhook_notification/disbursement_exception/body.txt', context)

            # Send
            send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)
    elif webhook_notification.kind in dispute_kinds:
        dispute = webhook_notification.dispute

        # Build context
        current_site = Site.objects.get_current()
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'webhook_notification': webhook_notification,
            'dispute': dispute,
        }

        # Render
        subject = loader.render_to_string(
            'customers/emails/braintree_webhook_notification/dispute/subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(
            'customers/emails/braintree_webhook_notification/dispute/body.txt', context)

        # Send
        send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)
    elif webhook_notification.kind == braintree.WebhookNotification.Kind.Check:
        # Build context
        current_site = Site.objects.get_current()
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'webhook_notification': webhook_notification,
        }

        # Render
        subject = loader.render_to_string(
            'customers/emails/braintree_webhook_notification/check/subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(
            'customers/emails/braintree_webhook_notification/check/body.txt', context)

        # Send
        send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)
    return braintree_webhook_notification
