from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from unittest import skip

import stripe
from django.conf import settings
from django.core import mail
from django.core import management
from django.core.cache import cache
from django.core.urlresolvers import reverse
from gouda.customers.models import BraintreeWebhookNotification
from gouda.customers.models import Coupon
from gouda.customers.models import StripeCard
from gouda.customers.models import StripeCustomer
from gouda.customers.models import StripePlan
from gouda.customers.utils import braintree
from gouda.photographers.keys import BookingSessionKeys
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from rest_framework import status
from rest_framework.test import APITestCase

STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
COUPON_ID ='THANKYOU100'
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')


class CouponVerificationAPIViewTestCase(APITestCase):
    view_name = 'api_customers:verify_coupon'

    @classmethod
    def setUpClass(cls):
        cache.clear()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        tear_down_profiles()
        cache.clear()

    def setUp(self):
        self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')
        self.code = 'ILIKEPIE!!!'
        self.coupon = Coupon.objects.create(**{
            'code': self.code,
            'amount_off': 9000
        })

    def tearDown(self):
        Coupon.objects.all().delete()

    def test_verify_coupon(self):
        promo_code = self.code
        self.verify_coupon(promo_code)

    def verify_coupon(self, promo_code):
        url = reverse(self.view_name, kwargs={'code': promo_code})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_verify_non_existent_coupon(self):
        promo_code = '50FF'
        url = reverse(self.view_name, kwargs={'code': promo_code})
        response = self.client.get(url)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(BookingSessionKeys.coupon_id in self.client.session.keys())

    def test_verify_coupon_without_authentication(self):
        self.client.logout()
        promo_code = COUPON_ID
        url = reverse(self.view_name, kwargs={'code': promo_code})
        response = self.client.get(url)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)
        self.assertFalse(BookingSessionKeys.coupon_id in self.client.session.keys())

    def test_coupon_added_to_session_data(self):
        promo_code = self.code
        self.verify_coupon(promo_code)
        self.assertTrue(BookingSessionKeys.coupon_id in self.client.session.keys())


class BraintreeWebhookNotificationRetrieveAPITestCase(APITestCase):
    def test_verify_braintree_challenge(self):
        bt_challenge = 'ac70eab71729b1a63ac4'
        verification_token = braintree.WebhookNotification.verify(bt_challenge)
        url = '%s?bt_challenge=%s' % (reverse('api_braintree:webhook_notification_list'), bt_challenge)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(verification_token in response.content)


class BraintreeWebhookNotificationCreateAPITestCase(APITestCase):
    def setUp(self):
        BraintreeWebhookNotification.objects.all().delete()

    def create_webhook_notification(self, type=braintree.WebhookNotification.Kind.SubMerchantAccountApproved):
        webhook_notification = braintree.WebhookTesting.sample_notification(
            type,
            'dangerzone'
        )
        return webhook_notification

    def test_create_webhook_notification(self):
        url = reverse('api_braintree:webhook_notification_list')
        data = self.create_webhook_notification()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BraintreeWebhookNotification.objects.all().count(), 1)
        braintree_webhook_notification = BraintreeWebhookNotification.objects.all()[0]
        self.assertEqual(braintree_webhook_notification.signature, data.get('bt_signature'))
        self.assertEqual(braintree_webhook_notification.payload, data.get('bt_payload'))


class BraintreeClientTokenAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(BraintreeClientTokenAPITestCase, cls).setUpClass()
        cache.clear()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(BraintreeClientTokenAPITestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')

    def test_retrieve_client_token(self):
        url = reverse('api_braintree:client_token')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('clientToken', response.data)


@skip('Not needed')
class StripeCustomerCreateAPITestCase(APITestCase):
    token_data = {
        'card': {
            'address_city': None,
            'address_country': None,
            'address_line1': None,
            'address_line1_check': None,
            'address_line2': None,
            'address_state': None,
            'address_zip': None,
            'address_zip_check': None,
            'brand': 'Visa',
            'dynamic_last4': None,
            'name': None,
            'country': 'US',
            'cvcCheck': 'unchecked',
            'expMonth': 8,
            'expYear': 2016,
            'funding': 'credit',
            'id': 'card_16BCSvGHl46CH1gudy1acgfv',
            'last4': '4242'
        },
        'coupon': {
            'amountOff': None,
            'created': 1433694588,
            'currency': None,
            'duration': 'once',
            'durationInMonths': None,
            'id': COUPON_ID,
            'maxRedemptions': None,
            'percentOff': 100,
            'redeemBy': None,
            'timesRedeemed': 0,
            'valid': True
        },
        'clientIp': '71.244.248.228',
        'created': 1433716925,
        'livemode': False,
        'object': 'token',
        'type': 'card',
        'used': False,
        'plan_id': 'com.theislandcrew.gouda.plans.monthly'
    }

    @classmethod
    def setUpClass(cls):
        super(StripeCustomerCreateAPITestCase, cls).setUpClass()
        create_profiles(1)
        management.call_command('create_plans')

    @classmethod
    def tearDownClass(cls):
        super(StripeCustomerCreateAPITestCase, cls).tearDownClass()
        tear_down_profiles()
        cache.clear()

    def setUp(self):
        self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')

        # Create card token
        stripe.api_key = STRIPE_SECRET_KEY
        token = stripe.Token.create(
            card={
                'number': '4242424242424242',
                'exp_month': 12,
                'exp_year': 2050,
                'cvc': '123'
            },
        )
        self.token_data['id'] = token.id

        StripeCustomer.objects.all().delete()
        StripeCard.objects.all().delete()
        self.assertEqual(StripeCustomer.objects.all().count(), 0)
        self.assertEqual(StripeCard.objects.all().count(), 0)

    def test_create_stripe_customer(self):
        url = reverse('api:stripe_customer_list')
        response = self.client.post(url, self.token_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(StripeCustomer.objects.all().count(), 1)
        self.assertEqual(StripeCard.objects.all().count(), 1)

    def test_create_stripe_customer_without_coupon(self):
        url = reverse('api:stripe_customer_list')
        token_data = self.token_data.copy()
        token_data.pop('coupon')
        response = self.client.post(url, token_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(StripeCustomer.objects.all().count(), 1)
        self.assertEqual(StripeCard.objects.all().count(), 1)

    def test_create_stripe_customer_invalid_coupon(self):
        url = reverse('api:stripe_customer_list')
        token_data = self.token_data.copy()
        token_data.get('coupon')['valid'] = False
        response = self.client.post(url, token_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(StripeCustomer.objects.all().count(), 1)
        self.assertEqual(StripeCard.objects.all().count(), 1)

    def test_create_stripe_customer_without_authentication(self):
        self.client.logout()
        url = reverse('api:stripe_customer_list')
        response = self.client.post(url, self.token_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertNotEqual(StripeCustomer.objects.all().count(), 1)
        self.assertNotEqual(StripeCard.objects.all().count(), 1)

    def test_create_stripe_customer_with_invalid_token(self):
        url = reverse('api:stripe_customer_list')
        token_data = self.token_data.copy()
        token_data['id'] = 'thisismyfakeid'
        response = self.client.post(url, token_data, format='json')
        self.assertNotEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertNotEqual(StripeCustomer.objects.all().count(), 1)
        self.assertNotEqual(StripeCard.objects.all().count(), 1)


@skip('Not needed')
class StripeCouponVerificationAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        cache.clear()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        tear_down_profiles()
        cache.clear()

    def setUp(self):
        self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')

    def test_verify_coupon(self):
        promo_code = COUPON_ID
        url = reverse('api:verify_stripe_coupon', kwargs={'id': promo_code})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_verify_non_existent_coupon(self):
        promo_code = '50FF'
        url = reverse('api:verify_stripe_coupon', kwargs={'id': promo_code})
        response = self.client.get(url)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)

    def test_verify_coupon_without_authentication(self):
        self.client.logout()
        promo_code = COUPON_ID
        url = reverse('api:verify_stripe_coupon', kwargs={'id': promo_code})
        response = self.client.get(url)
        self.assertNotEqual(response.status_code, status.HTTP_200_OK)


@skip('Not using Stripe anymore')
class StripeEventCreateAPITestCase(APITestCase):
    token_data = {}
    event_data = {
        'id': 'evt_16BoYpGHl46CH1guuTHDcy8p',
        'created': 1433863363,
        'livemode': False,
        'type': 'customer.subscription.created',
        'data': {
            'object': {
                'id': 'sub_6ObmCo33KqcN76',
                'plan': {
                    'interval': 'month',
                    'name': 'Monthly Plan',
                    'created': 1433724538,
                    'amount': 1500,
                    'currency': 'usd',
                    'id': 'com.theislandcrew.gouda.plans.monthly',
                    'object': 'plan',
                    'livemode': False,
                    'interval_count': 1,
                    'trial_period_days': None,
                    'metadata': {
                    },
                    'statement_descriptor': None
                },
                'object': 'subscription',
                'start': 1433863362,
                'status': 'active',
                'customer': 'cus_6ObmPYt34ErfnZ',
                'cancel_at_period_end': False,
                'current_period_start': 1433863362,
                'current_period_end': 1436455362,
                'ended_at': None,
                'trial_start': None,
                'trial_end': None,
                'canceled_at': None,
                'quantity': 1,
                'application_fee_percent': None,
                'discount': None,
                'tax_percent': None,
                'metadata': {
                }
            }
        },
        'object': 'event',
        'pending_webhooks': 0,
        'request': 'iar_6Obmzg3BZQvLeV',
        'api_version': '2015-04-07'
    }

    @classmethod
    def setUpClass(cls):
        cache.clear()
        create_profiles(1)
        management.call_command('create_plans')

    @classmethod
    def tearDownClass(cls):
        tear_down_profiles()
        cache.clear()

    def setUp(self):
        self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')

        # Create card token
        stripe.api_key = STRIPE_SECRET_KEY
        token = stripe.Token.create(
            card={
                "number": '4242424242424242',
                "exp_month": 12,
                "exp_year": 2050,
                "cvc": '123'
            },
        )
        self.token_data['id'] = token.id

        StripeCustomer.objects.all().delete()
        StripeCard.objects.all().delete()
        self.assertEqual(StripeCustomer.objects.all().count(), 0)
        self.assertEqual(StripeCard.objects.all().count(), 0)

    def test_create_stripe_event(self):
        self.client.logout()
        url = reverse('api:stripe_event_list')
        response = self.client.post(url, self.event_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_webhook(self):
        stripe_customer = stripe.Customer.create(
            source=self.token_data.get('id'),
            plan=StripePlan.objects.all()[0].stripe_id,
            email='success+sterlingarcher@simulator.amazonses.com',
        )
