from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import date
from datetime import timedelta

from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeCustomer
from gouda.customers.forms import BraintreeMerchantAccountForm
from gouda.customers.forms import BraintreeMerchantAccountVerificationForm
from gouda.customers.forms import BraintreeCustomerForm
from gouda.users.models import User
from django.test import TestCase
from django.conf import settings
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from braintree.test.nonces import Nonces
from gouda.customers.utils import braintree
from gouda.customers.utils import tear_down_customers
from gouda.customers.utils import create_customers
import os
from django.core.files.base import ContentFile
from django.core.files import File

# # Braintree
# BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
# BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
# BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
# BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')
# braintree.Configuration.configure(
#     BRAINTREE_ENVIRONMENT,
#     BRAINTREE_MERCHANT_ID,
#     BRAINTREE_PUBLIC_KEY,
#     BRAINTREE_PRIVATE_KEY
# )
#
# STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
# MAX_RENTAL_IMAGES = getattr(settings, 'MAX_RENTAL_IMAGES')

FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')


class BraintreeCustomerFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BraintreeCustomerFormTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(BraintreeCustomerFormTestCase, cls).tearDownClass()
        tear_down_customers()

    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'user': self.user.pk,
            'payment_method_nonce': Nonces.Transactable,
            'cardholder_name': self.user.full_name,
            'full_name': self.user.full_name,
            'street_address': '1504 Parkpath Way',
            'sub_premise': 'Apt #7',
            'postal_code': '93311',
            'locality': 'Bakersfield',
            'region': 'California',
            'country': 'US'
        }
        self.assertEqual(BraintreeCustomer.objects.all().count(), 0)

    def tearDown(self):
        tear_down_customers()

    def test_init(self):
        BraintreeCustomerForm()

    def test_create_braintree_customer(self):
        braintree_customer_form = BraintreeCustomerForm(self.data)
        braintree_customer_form.is_valid()
        self.assertTrue(braintree_customer_form.is_valid())
        braintree_customer = braintree_customer_form.save()
        self.assertIsNotNone(braintree_customer)
        self.assertEqual(BraintreeCustomer.objects.all().count(), 1)
        customer = braintree.Customer.find(braintree_customer.braintree_id)
        self.assertIsNotNone(customer)
        self.assertEqual(len(customer.credit_cards), 1)
        credit_card = customer.credit_cards[0]

        first_name = self.data.get('full_name').split()[0]
        last_name = ' '.join(self.data.get('full_name').split()[1:])
        self.assertEqual(first_name, credit_card.billing_address.first_name)
        self.assertEqual(last_name, credit_card.billing_address.last_name)
        self.assertEqual(self.data.get('street_address'), credit_card.billing_address.street_address)
        self.assertEqual(self.data.get('sub_premise'), credit_card.billing_address.extended_address)
        self.assertEqual(self.data.get('locality'), credit_card.billing_address.locality)
        self.assertEqual(self.data.get('region'), credit_card.billing_address.region)
        self.assertEqual(self.data.get('postal_code'), credit_card.billing_address.postal_code)
        self.assertEqual(self.data.get('country'), credit_card.billing_address.country_code_alpha2)
        self.assertEqual(self.data.get('cardholder_name'), credit_card.cardholder_name)


class BraintreeMerchantAccountFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BraintreeMerchantAccountFormTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(BraintreeMerchantAccountFormTestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.user = User.objects.all()[0]
        date_of_birth = date.today() - timedelta(days=365 * 28)
        self.data = {
            'user': self.user.pk,

            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'CA',
            'individual_street_address': '1504 Parkpath Way',
            'individual_month': int(date_of_birth.strftime('%m')),
            'individual_day': int(date_of_birth.strftime('%d')),
            'individual_year': int(date_of_birth.strftime('%Y')),
            'individual_email': self.user.email_address,
            'individual_first_name': self.user.first_name,
            'individual_last_name': self.user.last_name,
            'individual_phone': '4437421240',

            # 'business_locality': 'Bakersfield',
            # 'business_postal_code': '93311',
            # 'business_region': 'CA',
            # 'business_street_address': '1504 Parkpath Way',
            # 'business_dba_name': 'Danger Zone R\' Us',
            # 'business_legal_name': 'Danger Zone R\' Us',
            # 'business_tax_id': '123457689',

            'funding_email': self.user.email_address,
            'funding_mobile_phone': '5555555555',
            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        }

    def tearDown(self):
        BraintreeMerchantAccount.objects.all().delete()

    def test_init(self):
        BraintreeMerchantAccountForm()

    def submit_form(self, key=None, value=None, is_form_valid=True, data=None):
        if data is None:
            data = self.data.copy()
        if key and value:
            data[key] = value
        form = BraintreeMerchantAccountForm(data=data)
        braintree_merchant_account = None
        if is_form_valid:
            self.assertTrue(form.is_valid())
            braintree_merchant_account = form.save()
            self.assertIsNotNone(braintree_merchant_account)
            self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
            self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))
        else:
            self.assertFalse(form.is_valid())
            self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)
        return braintree_merchant_account

    def test_create_braintree_merchant_account(self):
        self.submit_form()

    def test_invalid_funding_account_numbers(self):
        self.submit_form('funding_account_number', 'ABCD', False)
        self.submit_form('funding_account_number', '!@#$%^&*()', False)

    def test_invalid_funding_routing_numbers(self):
        self.submit_form('funding_routing_number', '12345678', False)
        self.submit_form('funding_routing_number', 'ABCD', False)
        self.submit_form('funding_routing_number', '!@#$%^&*()', False)

    # def test_nonexistant_funding_routing_number(self):
    #     data = self.data.copy()
    #     data['funding_routing_number'] = '123'
    #     form = BraintreeMerchantAccountForm(data=data)
    #     self.assertTrue(form.is_valid())
    #     self.assertRaises(AssertionError, form.save)
    #     self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)

    def test_create_braintree_merchant_account_with_business(self):
        data = self.data.copy()
        data.update({
            'business_locality': 'Bakersfield',
            'business_postal_code': '93311',
            'business_region': 'CA',
            'business_street_address': '1504 Parkpath Way',
            'business_dba_name': 'Danger Zone R\' Us',
            'business_legal_name': 'Danger Zone R\' Us',
            'business_tax_id': '123457689',
        })
        self.submit_form(data=data)

    def test_update_braintree_merchant_account(self):
        self.submit_form()

        # Update...
        data = self.data.copy()
        data.update({
            'funding_mobile_phone': '4435106349',
            'funding_account_number': '1123581322',
            'funding_routing_number': '071000013',
        })
        braintree_merchant_account = self.submit_form(data=data)
        merchant_account = braintree.MerchantAccount.find(braintree_merchant_account.braintree_id)
        self.assertEqual(merchant_account.individual_details.email, self.data.get('individual_email'))
        self.assertEqual(merchant_account.funding_details.email, self.data.get('funding_email'))
        self.assertEqual(merchant_account.funding_details.mobile_phone, data.get('funding_mobile_phone'))
        self.assertEqual(merchant_account.funding_details.account_number_last_4, data.get('funding_account_number')[-4:])
        self.assertEqual(merchant_account.funding_details.routing_number, data.get('funding_routing_number'))

    # def test_invalid_funding_mobile_phones(self):
    #     self.submit_form('funding_mobile_phone', 'ABCD', False)
    #     self.submit_form('funding_mobile_phone', '!@#$%^&*()', False)

    # def test_invalid_business_tax_ids(self):
    #     self.submit_form('business_tax_id', '!@#$%^&*()', False)
    #     self.submit_form('business_tax_id', 'fsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfs', False)
    #     self.submit_form('business_tax_id', '1234567890', False)
    #
    # def test_invalid_business_dba_names(self):
    #     self.submit_form('business_dba_name', '!@#$%^&*()', False)
    #     self.submit_form('business_dba_name', 'fsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfs', False)
    #
    # def test_invalid_business_legal_names(self):
    #     self.submit_form('business_legal_name', '!@#$%^&*()', False)
    #     self.submit_form('business_legal_name', 'fsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfsfsfsdfs', False)
    #
    # def test_invalid_business_street_addresses(self):
    #     self.submit_form('business_street_address', 'COW', False)
    #
    # def test_invalid_business_regions(self):
    #     self.submit_form('business_region', 'COW', False)
    #     self.submit_form('business_region', 'W', False)
    #
    # def test_invalid_business_postal_codes(self):
    #     self.submit_form('business_postal_code', '!', False)
    #     self.submit_form('business_postal_code', '12345678910', False)
    #     self.submit_form('business_postal_code', '!@#$%^&*', False)

    def test_invalid_age(self):
        self.submit_form('individual_year', (date.today() - timedelta(days=10 * 365)).strftime('%Y'), False)

    # def test_invalid_individual_phones(self):
    #     self.submit_form('individual_phone', 'ABCD', False)
    #     self.submit_form('individual_phone', '!@#$%^&*()', False)

    # def test_invalid_individual_street_addresses(self):
    #     self.submit_form('individual_street_address', 'COW', False)

    def test_invalid_individual_regions(self):
        self.submit_form('individual_region', 'COW', False)
        self.submit_form('individual_region', 'W', False)

    def test_invalid_individual_postal_codes(self):
        self.submit_form('individual_postal_code', '!', False)
        self.submit_form('individual_postal_code', '12345678910', False)
        self.submit_form('individual_postal_code', '123456', False)
        self.submit_form('individual_postal_code', '!@#$%^&*', False)


class BraintreeMerchantAccountVerificationFormTestCase(TestCase):
    def setUp(self):
        create_profiles(1)
        create_customers()
        self.braintree_merchant_account = BraintreeMerchantAccount.objects.all()[0]
        self.files = {
            'government_issued_id': ContentFile(
                open(os.path.join(FIXTURES_DIR, 'government_issued_ids/id.png')).read(),
                name='id.png'
            )
        }
        # self.assertIsNone(self.braintree_merchant_account.government_issued_id.file)

    def tearDown(self):
        tear_down_customers()

    def test_init(self):
        BraintreeMerchantAccountVerificationForm()

    def test_form_valid(self):
        form = BraintreeMerchantAccountVerificationForm(files=self.files.copy(), instance=self.braintree_merchant_account)
        self.assertTrue(form.is_valid())
        braintree_merchant_account = form.save()
        self.assertIsNotNone(braintree_merchant_account.government_issued_id)
