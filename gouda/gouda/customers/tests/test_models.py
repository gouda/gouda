from __future__ import absolute_import

import random
from datetime import date
from datetime import timedelta

from django import test
from gouda.users.models import User
from django.core.cache import cache
from gouda.customers.models import StripeCustomer
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.customers.models import Coupon
import stripe
import braintree
from django.conf import settings
from braintree.test.nonces import Nonces
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles

# Stripe
STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
stripe.api_key = STRIPE_SECRET_KEY

# Braintree
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')
BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')
braintree.Configuration.configure(
    BRAINTREE_ENVIRONMENT,
    BRAINTREE_MERCHANT_ID,
    BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY
)


class CouponTestCase(test.TestCase):
    def setUp(self):
        self.data = {
            'code': 'ILIKEPIE!!!',
            'amount_off': 9000
        }

    def tearDown(self):
        Coupon.objects.all().delete()

    def test_init(self):
        Coupon()

    def test_create_coupon(self):
        coupon = Coupon.objects.create(**self.data)
        self.assertIsNotNone(coupon)


class BraintreeMerchantAccountTestCase(test.TestCase):
    @classmethod
    def setUpClass(cls):
        super(BraintreeMerchantAccountTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(BraintreeMerchantAccountTestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'user': self.user,
            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'CA',
            'individual_street_address': '1504 Parkpath Way',
            'individual_date_of_birth': date.today() - timedelta(days=365 * 28),
            'individual_email': self.user.email_address,
            'individual_first_name': self.user.first_name,
            'individual_last_name': self.user.last_name,
            'individual_phone': '4437421240',

            'business_locality': 'Bakersfield',
            'business_postal_code': '93311',
            'business_region': 'CA',
            'business_street_address': '1504 Parkpath Way',
            'business_dba_name': 'Danger Zone R\' Us',
            'business_legal_name': 'Danger Zone R\' Us',
            'business_tax_id': '123457689',

            'funding_email': self.user.email_address,
            'funding_mobile_phone': '5555555555',
            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        }

    def tearDown(self):
        BraintreeMerchantAccount.objects.all().delete()

    def test_init(self):
        BraintreeMerchantAccount()

    def test_create_braintree_merchant_account_without_business(self):
        data = self.data.copy()
        data.pop('business_locality')
        data.pop('business_postal_code')
        data.pop('business_region')
        data.pop('business_street_address')
        data.pop('business_dba_name')
        data.pop('business_legal_name')
        data.pop('business_tax_id')
        braintree_merchant_account = BraintreeMerchantAccount.objects.create(**data)
        self.assertIsNotNone(braintree_merchant_account)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
        self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))

    def test_create_braintree_merchant_account(self):
        braintree_merchant_account = BraintreeMerchantAccount.objects.create(**self.data)
        self.assertIsNotNone(braintree_merchant_account)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
        self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))

    def test_update_braintree_merchant_account(self):
        braintree_merchant_account = BraintreeMerchantAccount.objects.create(**self.data)
        self.assertIsNotNone(braintree_merchant_account)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
        self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))

        # Update...
        data = {
            'funding_mobile_phone': '4435106349',
            'funding_account_number': '1123581322',
            'funding_routing_number': '071000013',
        }
        braintree_merchant_account.funding_mobile_phone = data.get('funding_mobile_phone')
        braintree_merchant_account.funding_account_number = data.get('funding_account_number')
        braintree_merchant_account.funding_routing_number = data.get('funding_routing_number')
        braintree_merchant_account.save()

        merchant_account = braintree.MerchantAccount.find(braintree_merchant_account.braintree_id)
        self.assertEqual(merchant_account.individual_details.email, self.data.get('individual_email'))
        self.assertEqual(merchant_account.funding_details.email, self.data.get('funding_email'))
        self.assertEqual(merchant_account.funding_details.mobile_phone, data.get('funding_mobile_phone'))
        self.assertEqual(merchant_account.funding_details.account_number_last_4, data.get('funding_account_number')[-4:])
        self.assertEqual(merchant_account.funding_details.routing_number, data.get('funding_routing_number'))

    def test_update_braintree_merchant_account_with_invalid_account_number(self):
        braintree_merchant_account = BraintreeMerchantAccount.objects.create(**self.data)
        self.assertIsNotNone(braintree_merchant_account)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
        self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))

        # Update...
        data = {
            'funding_mobile_phone': '4435106349',
            'funding_account_number': '1123581323',
            'funding_routing_number': '071000013',
        }
        braintree_merchant_account.funding_mobile_phone = data.get('funding_mobile_phone')
        braintree_merchant_account.funding_account_number = data.get('funding_account_number')
        braintree_merchant_account.funding_routing_number = data.get('funding_routing_number')
        braintree_merchant_account.save()
        braintree_merchant_account = BraintreeMerchantAccount.objects.get(id=braintree_merchant_account.id)

        merchant_account = braintree.MerchantAccount.find(braintree_merchant_account.braintree_id)
        self.assertEqual(merchant_account.individual_details.email, self.data.get('individual_email'))
        self.assertEqual(merchant_account.funding_details.email, self.data.get('funding_email'))
        self.assertEqual(merchant_account.funding_details.mobile_phone, data.get('funding_mobile_phone'))
        self.assertEqual(merchant_account.funding_details.account_number_last_4, data.get('funding_account_number')[-4:])
        self.assertEqual(merchant_account.funding_details.routing_number, data.get('funding_routing_number'))
        # self.assertEqual(
        #         BraintreeMerchantAccountStatusType.get(merchant_account.status).value,
        #         braintree_merchant_account.status
        # )

    # def test_update_braintree_merchant_account(self):
    #     num_updates = 5
    #     localities = [
    #         'Bakersfield',
    #         'The Woodlands',
    #         'Pittsburgh',
    #         'Beltsville',
    #         'Columbia'
    #     ]
    #     regions = [
    #         'CA',
    #         'TX',
    #         'PA',
    #         'MD'
    #     ]
    #     for i in range(num_updates):
    #         individual = self.individual.copy()
    #         business = self.business.copy()
    #         funding = self.funding.copy()
    #         individual['locality'] = random.choice(localities)
    #         individual['region'] = random.choice(regions)
    #         business['locality'] = random.choice(localities)
    #         business['region'] = random.choice(regions)
    #         business['tax_id'] = random.randint(100000000, 999999999)
    #         funding['account_number'] = random.randint(1000000000, 9999999999)
    #         braintree_merchant_account, is_created = BraintreeMerchantAccount.objects.update_or_create_braintree_merchant_account(
    #             user=self.user,
    #             tos_accepted=True,
    #             individual=individual,
    #             business=business,
    #             funding=funding
    #         )
    #         self.assertIsNotNone(braintree_merchant_account)
    #         self.assertFalse(is_created and i > 0)
    #         self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
    #         self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))


class BraintreeCustomerTestCase(test.TestCase):
    @classmethod
    def setUpClass(cls):
        super(BraintreeCustomerTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(BraintreeCustomerTestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'payment_method_nonce': Nonces.Transactable,
            'user': self.user
        }

    def tearDown(self):
        BraintreeCustomer.objects.all().delete()

    def test_init(self):
        BraintreeCustomer()

    def test_create_braintree_customer(self):
        braintree_customer, is_created = BraintreeCustomer.objects.update_or_create_braintree_customer(**self.data)
        self.assertIsNotNone(braintree_customer)
        self.assertTrue(is_created)
        self.assertEqual(BraintreeCustomer.objects.all().count(), 1)
        self.assertIsNotNone(braintree.Customer.find(braintree_customer.braintree_id))

    def test_multiple_payment_methods(self):
        data = self.data.copy()
        num_payment_methods = 5
        braintree_customer = None
        for i in range(num_payment_methods):
            data['payment_method_nonce'] = Nonces.Transactable
            braintree_customer, is_created = BraintreeCustomer.objects.update_or_create_braintree_customer(**self.data)
            self.assertFalse(is_created and i > 0)
            self.assertIsNotNone(braintree_customer)
            self.assertIsNotNone(braintree.Customer.find(braintree_customer.braintree_id))
        self.assertEqual(BraintreeCustomer.objects.all().count(), 1)
        self.assertEqual(len(braintree.Customer.find(braintree_customer.braintree_id).credit_cards), num_payment_methods)

    def test_invalid_payment_nonce(self):
        data = self.data.copy()
        data['payment_method_nonce'] = Nonces.Consumed
        self.assertRaises(AssertionError, BraintreeCustomer.objects.update_or_create_braintree_customer, **data)
