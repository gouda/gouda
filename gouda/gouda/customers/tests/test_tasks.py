from __future__ import absolute_import

from datetime import date
from datetime import timedelta

from django.core import mail
from django import test
from gouda.users.models import User
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from django.core.cache import cache
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.customers.models import BraintreeWebhookNotification
from gouda.customers.signals import handle_braintree_webhook_notification_event
from gouda.customers.tasks import process_braintree_webhook_notification
import stripe
from gouda.customers.utils import braintree
from django.db.models.signals import post_save
from django.conf import settings

STAFF = getattr(settings, 'STAFF')
STRIPE_SECRET_KEY = getattr(settings, 'STRIPE_SECRET_KEY')
stripe.api_key = STRIPE_SECRET_KEY
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')


class ProcessBraintreeWebhookNotificationTestCase(test.TestCase):
    @classmethod
    def setUpClass(cls):
        super(ProcessBraintreeWebhookNotificationTestCase, cls).setUpClass()
        post_save.disconnect(handle_braintree_webhook_notification_event, sender=BraintreeWebhookNotification)
        cache.clear()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(ProcessBraintreeWebhookNotificationTestCase, cls).tearDownClass()
        tear_down_profiles()

    def create_braintree_webhook_notification_data(self, kind=braintree.WebhookNotification.Kind.SubMerchantAccountApproved, id='dangerzone'):
        webhook_notification = braintree.WebhookTesting.sample_notification(
            kind,
            id
        )
        data = {
            'signature': webhook_notification.get('bt_signature'),
            'payload': webhook_notification.get('bt_payload')
        }
        return data

    def create_braintree_webhook_notification(self, kind=braintree.WebhookNotification.Kind.SubMerchantAccountApproved, id='dangerzone'):
        return BraintreeWebhookNotification.objects.create(**self.create_braintree_webhook_notification_data(kind, id))

    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'user': self.user,
            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'CA',
            'individual_street_address': '1504 Parkpath Way',
            'individual_date_of_birth': date.today() - timedelta(days=365 * 28),
            'individual_email': self.user.email_address,
            'individual_first_name': self.user.first_name,
            'individual_last_name': self.user.last_name,
            'individual_phone': '4437421240',

            'business_locality': 'Bakersfield',
            'business_postal_code': '93311',
            'business_region': 'CA',
            'business_street_address': '1504 Parkpath Way',
            'business_dba_name': 'Danger Zone R\' Us',
            'business_legal_name': 'Danger Zone R\' Us',
            'business_tax_id': '123457689',

            'funding_email': self.user.email_address,
            'funding_mobile_phone': '5555555555',
            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        }
        self.braintree_merchant_account = BraintreeMerchantAccount.objects.create(**self.data)

    def tearDown(self):
        BraintreeMerchantAccount.objects.all().delete()
        BraintreeWebhookNotification.objects.all().delete()

    def test_fill_out_braintree_webhook_notification(self):
        kind = braintree.WebhookNotification.Kind.SubscriptionExpired
        braintree_webhook_notification = process_braintree_webhook_notification(self.create_braintree_webhook_notification(kind=kind).id)
        self.assertEqual(braintree_webhook_notification.kind, kind)
        self.assertIsNotNone(braintree_webhook_notification.timestamp)

    def test_braintree_merchant_account_approved(self):
        self.assertEqual(self.braintree_merchant_account.status, BraintreeMerchantAccountStatusType.PENDING)
        kind = braintree.WebhookNotification.Kind.SubMerchantAccountApproved
        braintree_id = self.braintree_merchant_account.braintree_id
        braintree_webhook_notification_id = self.create_braintree_webhook_notification(kind=kind, id=braintree_id).id
        process_braintree_webhook_notification(braintree_webhook_notification_id)

        # self.assertTrue(BraintreeMerchantAccount.objects.filter(braintree_id=braintree_id, status=BraintreeMerchantAccountStatusType.ACTIVE).exists())
        # self.assertEqual(BraintreeMerchantAccount.objects.filter(braintree_id=braintree_id, status=BraintreeMerchantAccountStatusType.ACTIVE).count(), 1)

        # Make sure emails get sent
        self.assertEqual(len(mail.outbox), 1)
        notification_mail = mail.outbox[0]
        self.assertEqual(notification_mail.from_email, SERVER_EMAIL)
        self.assertEqual(notification_mail.to, STAFF_EMAILS)
        self.assertEqual(notification_mail.subject, 'Sub-Merchant Account Approved')

    def test_braintree_merchant_account_declined(self):
        self.assertEqual(self.braintree_merchant_account.status, BraintreeMerchantAccountStatusType.PENDING)
        kind = braintree.WebhookNotification.Kind.SubMerchantAccountDeclined
        braintree_id = self.braintree_merchant_account.braintree_id
        braintree_webhook_notification_id = self.create_braintree_webhook_notification(kind=kind, id=braintree_id).id
        process_braintree_webhook_notification(braintree_webhook_notification_id)

        # Merchant should be declined
        # print(BraintreeMerchantAccount.objects.filter(braintree_id=braintree_id)[0])
        # print(BraintreeMerchantAccount.objects.filter(braintree_id=braintree_id)[0].status)
        # self.assertTrue(BraintreeMerchantAccount.objects.filter(braintree_id=braintree_id, status=BraintreeMerchantAccountStatusType.SUSPENDED).exists())
        # self.assertEqual(BraintreeMerchantAccount.objects.filter(braintree_id=braintree_id, status=BraintreeMerchantAccountStatusType.SUSPENDED).count(), 1)

        # Make sure emails get sent
        self.assertEqual(len(mail.outbox), 1)
        notification_mail = mail.outbox[0]
        self.assertEqual(notification_mail.from_email, SERVER_EMAIL)
        self.assertEqual(notification_mail.to, STAFF_EMAILS)
        self.assertEqual(notification_mail.subject, 'Sub-Merchant Account Declined')

    def test_notification_for_disbursement_exception(self):
        kind = braintree.WebhookNotification.Kind.DisbursementException
        self.create_and_process_braintree_webhook_notification(kind)

    def test_notification_for_dispute_lost(self):
        kind = braintree.WebhookNotification.Kind.DisputeLost
        self.create_and_process_braintree_webhook_notification(kind)

    def test_notification_for_check(self):
        kind = braintree.WebhookNotification.Kind.Check
        self.create_and_process_braintree_webhook_notification(kind)

    def create_and_process_braintree_webhook_notification(self, kind):
        braintree_id = 'dangerzone'
        braintree_webhook_notification_id = self.create_braintree_webhook_notification(kind=kind, id=braintree_id).id
        process_braintree_webhook_notification(braintree_webhook_notification_id)

        # Make sure emails get sent
        self.assertEqual(len(mail.outbox), 1)
        notification_mail = mail.outbox[0]
        self.assertEqual(notification_mail.from_email, SERVER_EMAIL)
        self.assertEqual(notification_mail.to, STAFF_EMAILS)
