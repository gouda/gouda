from __future__ import absolute_import

import uuid

from django.conf import settings
from django.core.files import storage
import os
from storages.backends.s3boto import S3BotoStorage
from django.contrib.staticfiles import storage as staticfiles_storage
from django.core.files.storage import get_storage_class
from django.utils.deconstruct import deconstructible

STATICFILES_LOCATION = getattr(settings, 'STATICFILES_LOCATION', 'static')
MEDIAFILES_LOCATION = getattr(settings, 'MEDIAFILES_LOCATION', 'media')


class StaticFilesStorage(staticfiles_storage.StaticFilesStorage):
    def get_valid_name(self, name):
        random_name, ext = os.path.splitext(name)
        random_name = '%s%s' % (uuid.uuid1().hex, ext)
        return super(StaticFilesStorage, self).get_valid_name(random_name)

class FileSystemStorage(storage.FileSystemStorage):
    def get_valid_name(self, name):
        random_name, ext = os.path.splitext(name)
        random_name = '%s%s' % (uuid.uuid1().hex, ext)
        return super(FileSystemStorage, self).get_valid_name(random_name)


class S3StaticStorage(S3BotoStorage):
    location = STATICFILES_LOCATION


@deconstructible
class S3MediaStorage(S3BotoStorage):
    location = MEDIAFILES_LOCATION

    def get_valid_name(self, name):
        random_name, ext = os.path.splitext(name)
        random_name = '%s%s' % (uuid.uuid1().hex, ext)
        return super(S3MediaStorage, self).get_valid_name(random_name)


@deconstructible
class S3NamedMediaStorage(S3BotoStorage):
    location = MEDIAFILES_LOCATION


class CachedS3BotoStaticStorage(S3BotoStorage):
    location = STATICFILES_LOCATION
    """
    S3 storage backend that saves the files locally, too.
    """
    def __init__(self, *args, **kwargs):
        super(CachedS3BotoStaticStorage, self).__init__(*args, **kwargs)
        self.local_storage = get_storage_class(
            "compressor.storage.CompressorFileStorage")()

    def save(self, name, content):
        name = super(CachedS3BotoStaticStorage, self).save(name, content)
        self.local_storage._save(name, content)
        return name


class CachedS3BotoStaticGzipStorage(CachedS3BotoStaticStorage):
    gzip = True
