from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from gouda.photographers.tasks import smart_crop_booking_image
from libthumbor import CryptoURL
from django.conf import settings
import requests
from django.core.files.base import File
import os
import string
import random


THUMBOR_SERVER = getattr(settings, 'THUMBOR_SERVER')
THUMBOR_SECURITY_KEY = getattr(settings, 'THUMBOR_SECURITY_KEY')


def generate_url(image_url, **kwargs):
    thumbor_url = THUMBOR_SERVER + CryptoURL(key=THUMBOR_SECURITY_KEY).generate(
        image_url=image_url,
        **kwargs
    )
    return thumbor_url
