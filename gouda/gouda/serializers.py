# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from drf_haystack import serializers


class HaystackSerializer(serializers.HaystackSerializer):
    def get_fields(self):
        """
        Get the required fields for serializing the result.
        """
        field_mapping = super(HaystackSerializer, self).get_fields()
        ignore_fields = getattr(self.Meta, "ignore_fields", [])

        field_names = list(field_mapping.keys())
        for field_name in field_names:
            if field_name in ignore_fields:
                field_mapping.pop(field_name)

        return field_mapping
