from __future__ import absolute_import
from __future__ import unicode_literals

from django.forms import widgets
from django.contrib.gis.forms import widgets as gis_widgets

from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe


class NgSelectMultiple(widgets.SelectMultiple):
    def __init__(self, ng_model, attrs=None, choices=()):
        if not attrs:
            attrs = {}
        attrs['ng-model'] = ng_model
        super(NgSelectMultiple, self).__init__(attrs=attrs, choices=choices)


class NgTextarea(widgets.Textarea):
    def __init__(self, ng_model, attrs=None):
        if not attrs:
            attrs = {}
        attrs['ng-model'] = ng_model
        super(NgTextarea, self).__init__(attrs)


class NgTextInput(widgets.TextInput):
    def __init__(self, ng_model, attrs=None):
        if not attrs:
            attrs = {}
        attrs['ng-model'] = ng_model
        super(NgTextInput, self).__init__(attrs)

class NgSelect(widgets.Select):
    def __init__(self, ng_model, attrs=None, choices=()):
        if not attrs:
            attrs = {}
        attrs['ng-model'] = ng_model
        super(NgSelect, self).__init__(attrs, choices)


class KendoUIDatePicker(widgets.DateInput):
    def render(self, name, value, attrs=None):
        if value is None:
            value = ''
        final_attrs = self.build_attrs(attrs, type=self.input_type, name=name)
        if value != '':
            # Only add the 'value' attribute if a value is non-empty.
            final_attrs['value'] = widgets.force_text(self._format_value(value))
        return widgets.format_html('<input{} />', widgets.flatatt(final_attrs))


class OpenLayersWidget(gis_widgets.BaseGeometryWidget):
    template_name = 'gis/openlayers.html'

    class Media:
        js = (
            'gis/js/OpenLayers.min.js',
            'gis/js/OLMapWidget.js',
        )


class KendoTextarea(widgets.Textarea):
    def __init__(self, ng_model, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        attrs['ng-model'] = ng_model

        if 'class' in attrs:
            attrs['class'] += ' k-textbox'
        else:
            attrs['class'] = 'k-textbox'

        attrs['ng-cloak'] = ''
        if is_required:
            attrs['required'] = ''
        super(KendoTextarea, self).__init__(attrs)


class KendoTimePicker(widgets.TextInput):
    def __init__(self, ng_model, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        attrs['ng-model'] = ng_model

        if 'class' in attrs:
            attrs['class'] += ' kendo-time-picker'
        else:
            attrs['class'] = 'kendo-time-picker'

        attrs['ng-cloak'] = ''
        if is_required:
            attrs['required'] = ''
        super(KendoTimePicker, self).__init__(attrs)


class KendoDatePicker(widgets.TextInput):
    def __init__(self, ng_model, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        attrs['ng-model'] = ng_model

        if 'class' in attrs:
            attrs['class'] += ' kendo-date-picker'
        else:
            attrs['class'] = 'kendo-date-picker'

        attrs['ng-cloak'] = ''
        if is_required:
            attrs['required'] = ''
        super(KendoDatePicker, self).__init__(attrs)


class KendoTextInput(widgets.TextInput):
    def __init__(self, ng_model, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        attrs['ng-model'] = ng_model

        if 'class' in attrs:
            attrs['class'] += ' k-textbox'
        else:
            attrs['class'] = 'k-textbox'

        attrs['ng-cloak'] = ''
        if is_required:
            attrs['required'] = ''
        super(KendoTextInput, self).__init__(attrs)


class KendoDropDownList(widgets.Select):
    def __init__(self, ng_model, is_required=True, attrs=None, choices=()):
        if not attrs:
            attrs = dict()
        attrs['ng-model'] = ng_model
        attrs['kendo-drop-down-list'] = ''
        attrs['ng-cloak'] = ''
        if is_required:
            attrs['required'] = ''
        super(KendoDropDownList, self).__init__(attrs, choices)


class CheckboxInput(widgets.CheckboxInput):
    def __init__(self, attrs=None):
        if not attrs:
            attrs = dict()
        # if 'class' in attrs:
        #     attrs['class'] += ' k-checkbox'
        # else:
        #     attrs['class'] = 'k-checkbox'
        attrs['data-parsley-errors-messages-disabled'] = ''
        super(CheckboxInput, self).__init__(attrs)


class DateInput(widgets.DateInput):
    def __init__(self, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        if is_required:
            attrs['required'] = ''
        attrs['onkeydown'] = 'return false;'
        attrs['data-parsley-errors-messages-disabled'] = ''
        super(DateInput, self).__init__(attrs)


class TimeInput(DateInput):
    pass


class PasswordInput(widgets.PasswordInput):
    def __init__(self, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        if is_required:
            attrs['required'] = ''
        if 'class' in attrs:
            attrs['class'] += ' k-textbox'
        else:
            attrs['class'] = 'k-textbox'
        attrs['data-parsley-errors-messages-disabled'] = ''
        super(PasswordInput, self).__init__(attrs)


class TextInput(widgets.TextInput):
    def __init__(self, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        if is_required:
            attrs['required'] = ''
        if 'class' in attrs:
            attrs['class'] += ' k-textbox'
        else:
            attrs['class'] = 'k-textbox'
        attrs['data-parsley-errors-messages-disabled'] = ''
        super(TextInput, self).__init__(attrs)


class Textarea(widgets.Textarea):
    def __init__(self, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        if is_required:
            attrs['required'] = ''
        if 'class' in attrs:
            attrs['class'] += ' k-textbox'
        else:
            attrs['class'] = 'k-textbox'
        attrs['data-parsley-errors-messages-disabled'] = ''
        super(Textarea, self).__init__(attrs)


class FileInput(widgets.FileInput):
    def __init__(self, is_required=True, attrs=None):
        if not attrs:
            attrs = dict()
        if is_required:
            attrs['required'] = ''
        attrs['data-parsley-errors-messages-disabled'] = ''
        super(FileInput, self).__init__(attrs)


class MaterialCheckboxFieldRenderer(widgets.CheckboxFieldRenderer):
    def render(self):
        """
        Outputs a <ul> for this set of choice fields.
        If an id was given to the field, it is applied to the <ul> (each
        item in the list will get an id of `$id_$i`).
        """
        id_ = self.attrs.get('id', None)
        output = []
        for i, choice in enumerate(self.choices):
            choice_value, choice_label = choice
            if isinstance(choice_label, (tuple, list)):
                attrs_plus = self.attrs.copy()
                if id_:
                    attrs_plus['id'] += '_{}'.format(i)
                sub_ul_renderer = MaterialCheckboxFieldRenderer(name=self.name,
                                                                value=self.value,
                                                                attrs=attrs_plus,
                                                                choices=choice_label)
                sub_ul_renderer.choice_input_class = self.choice_input_class
                output.append(format_html(self.inner_html, choice_value=choice_value,
                                          sub_widgets=sub_ul_renderer.render()))
            else:
                w = self.choice_input_class(self.name, self.value,
                                            self.attrs.copy(), choice, i)
                output.append(format_html(self.inner_html,
                                          choice_value=force_text(w), sub_widgets=''))
        return format_html(self.outer_html,
                           id_attr=format_html(' id="{}"', id_) if id_ else '',
                           content=mark_safe('\n'.join(output)))


class MaterialCheckboxSelectMultiple(widgets.CheckboxSelectMultiple):
    renderer = MaterialCheckboxFieldRenderer
