from __future__ import absolute_import

import base64
import uuid
import logging
import imghdr
import binascii


from django import forms
from django.core.files.uploadedfile import SimpleUploadedFile
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from django.utils.safestring import mark_safe
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils import six

logger = logging.getLogger(__name__)
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')


class Select(forms.Select):
    def __init__(self, attrs=None, choices=()):
        super(Select, self).__init__(attrs=attrs, choices=choices)

    def render_option(self, selected_choices, option_value, option_label):
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                # Only allow for a single selection.
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        if option_value is None:
            return format_html('<option value="" disabled selected="selected">{0}</option>',
                               force_text(option_label))
        else:
            return super(Select, self).render_option(selected_choices, option_value, option_label)


class Base64ImageField(forms.ImageField):
    ALLOWED_IMAGE_TYPES = (
        'jpeg',
        'jpg',
        'png',
    )
    EMPTY_VALUES = (None, '', [], (), {})
    default_error_messages = {
        'invalid_image': _("Upload a valid image. Images must be a PNG or JPG file that is less than %d MB." % MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES),
    }

    def __init__(self, *args, **kwargs):
        kwargs['widget'] = kwargs.get('widget', forms.Textarea())
        super(Base64ImageField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        # Check if this is a base64 string
        if value in self.EMPTY_VALUES:
            return None

        if not isinstance(value, six.string_types):
            raise forms.ValidationError(self.error_messages['invalid_image'], code='invalid_image')
        try:
            decoded_file = binascii.a2b_base64(value)
        except (TypeError, binascii.Error):
            raise forms.ValidationError(self.error_messages['invalid_image'], code='invalid_image')

        file_name = uuid.uuid1().hex
        file_extension = self.get_file_extension(file_name, decoded_file)
        if file_extension not in self.ALLOWED_IMAGE_TYPES:
            raise forms.ValidationError(self.error_messages['invalid_image'], code='invalid_image')

        uploaded_file = SimpleUploadedFile('%s.%s' % (file_name, file_extension), decoded_file, content_type='application/octet-stream')
        return super(Base64ImageField, self).to_python(uploaded_file)

    def get_file_extension(self, filename, decoded_file):
        extension = imghdr.what(filename, decoded_file)
        extension = "jpg" if extension == "jpeg" else extension
        return extension