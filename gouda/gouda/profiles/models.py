from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from gouda.users.models import User
from gouda.users.models import LoginInfo
from easy_thumbnails.fields import ThumbnailerImageField
from django_countries.fields import CountryField
from django_enumfield import enum
from gouda.addresses.models import Address
from django.contrib.postgres.fields import ArrayField

SCREEN_NAME_MAX_LENGTH = 32
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')


def validate_image(obj):
    if obj.file.size > MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES * 1024 * 1024:
        raise ValidationError('Maximum image file size is %d MB' % MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES)


IMAGE_VALIDATORS = [validate_image]


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class StatusType(enum.Enum):
    UNVERIFIED = 0
    ACTIVE = 1
    PENDING = 2


class GenderType(enum.Enum):
    MALE = 0
    FEMALE = 1
    OTHER = 2

    labels = {
        MALE: 'Male',
        FEMALE: 'Female',
        OTHER: 'Other',
    }


class ReferralType(enum.Enum):
    GOOGLE = 0
    FACEBOOK = 1
    INSTAGRAM = 2
    TWITTER = 3
    LINKEDIN = 4
    WORD_OF_MOUTH = 5
    OTHER = 6

    labels = {
        GOOGLE: 'Google',
        FACEBOOK: 'Facebook',
        INSTAGRAM: 'Instagram',
        TWITTER: 'Twitter',
        LINKEDIN: 'LinkedIn',
        WORD_OF_MOUTH: 'Word of mouth',
        OTHER: 'Other'
    }


class ProfileType(enum.Enum):
    CLIENT = 0
    PHOTOGRAPHER = 1
    labels = {
        CLIENT: 'Client',
        PHOTOGRAPHER: 'Photographer',
    }


class Profile(BaseModel):
    user = models.OneToOneField(User)
    gender = enum.EnumField(GenderType, null=True, blank=False, default=GenderType.MALE)
    date_of_birth = models.DateField(null=True)
    image = ThumbnailerImageField(null=True, upload_to='profile_images/', blank=True, validators=IMAGE_VALIDATORS)
    locality = models.CharField(blank=False, default='', max_length=256)
    region = models.CharField(blank=False, default='', max_length=256)
    country = CountryField(default='US')
    status = enum.EnumField(StatusType, blank=False, default=StatusType.UNVERIFIED)
    referral = enum.EnumField(ReferralType, blank=False, default=ReferralType.OTHER)
    cc_recipients = ArrayField(models.EmailField(), blank=True, default=list())
    # phone_number = models.CharField(blank=True, default='', max_length=256)

    shipping_addresses = models.ManyToManyField(Address, through='ProfileShippingAddress')

    @property
    def latest_shipping_address(self):
        queryset = self.shipping_addresses.all().order_by('-date_created')
        return queryset[0] if queryset else None

    @property
    def braintree_customer(self):
        return self.user.braintree_customer

    @property
    def braintree_merchant_account(self):
        return self.user.braintree_merchant_account

    @property
    def has_rentals(self):
        return False
        # from gouda.rentals.models import Rental
        # return Rental.objects.filter(profile=self).exists()

    @property
    def is_active(self):
        return self.status == StatusType.ACTIVE

    @property
    def is_verified(self):
        return self.status != StatusType.UNVERIFIED

    @property
    def is_pending(self):
        return self.status == StatusType.PENDING

    @property
    def full_name(self):
        return self.user.full_name

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def email_address(self):
        return self.user.email_address

    @property
    def last_login_info(self):
        queryset = LoginInfo.objects.filter(user=self.user).order_by('date_created')[:1]
        if queryset.exists():
            return queryset[0]
        else:
            return None

    @property
    def last_city(self):
        last_login_info = self.last_login_info
        return last_login_info.city if last_login_info else None

    @property
    def last_region(self):
        last_login_info = self.last_login_info
        return last_login_info.region if last_login_info else None

    @property
    def last_country(self):
        last_login_info = self.last_login_info
        return last_login_info.country if last_login_info else self.country

    @property
    def last_ip_address(self):
        last_login_info = self.last_login_info
        return last_login_info.ip_address if last_login_info else None

    @property
    def gender_label(self):
        return GenderType.label(self.gender)

    @property
    def referral_label(self):
        return ReferralType.label(self.referral)

    @property
    def anonymous_email_address(self):
        if self.postmaster_user:
            return self.postmaster_user.anonymous_email_address
        else:
            return None

    @property
    def display_name(self):
        return '%s %s.' % (self.first_name, self.last_name[0].upper())

    def __str__(self):
        return self.user.full_name


class ProfileShippingAddress(BaseModel):
    is_primary = models.BooleanField(default=False)
    profile = models.ForeignKey(Profile)
    address = models.ForeignKey(Address)


class Thread(BaseModel):
    pass


class Message(BaseModel):
    body = models.TextField()
    thread = models.ForeignKey(Thread)
    sender = models.ForeignKey(Profile, related_name='sent_messages')
    receiver = models.ForeignKey(Profile, related_name='received_messages')
    is_read = models.BooleanField(default=False)

    @property
    def truncated_body(self):
        return self.body[:100]

from gouda.profiles.signals import *
