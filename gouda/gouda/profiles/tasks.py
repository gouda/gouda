from __future__ import absolute_import

from datetime import timedelta

from gouda.celeryapp import app
from django.utils import timezone
from gouda.profiles.models import Profile
from gouda.profiles.models import StatusType
from django.conf import settings
from django.core.mail import send_mail
from django.template import loader
from django.contrib.sites.models import Site
from celery.utils.log import get_task_logger

PROFILE_APPROVED_EMAIL_DELAY = getattr(settings, 'PROFILE_APPROVED_EMAIL_DELAY_SECONDS')
IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
logger = get_task_logger(__name__)

@app.task
def send_profile_approved_emails():
    if not IS_TEST_MODE:
        current_site = Site.objects.get_current()
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
        }
    else:
        context = {
            'domain': 'testserver.com',
            'site_name': 'testserver',
            'protocol': 'http'
        }

    cutoff_date = timezone.now() - timedelta(seconds=PROFILE_APPROVED_EMAIL_DELAY)
    logger.info('Cut off date is %s' % cutoff_date)
    logger.info('There are %d pending profiles!' % Profile.objects.filter(status=StatusType.PENDING).count())
    for profile in Profile.objects.filter(status=StatusType.PENDING, date_created__lte=cutoff_date):
        context['user'] = profile.user
        subject = loader.render_to_string('profiles/emails/profile_approved_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('profiles/emails/profile_approved_body.txt', context)
        send_mail(subject=subject, message=body, from_email=DEFAULT_FROM_EMAIL, recipient_list=[profile.email_address])
        profile.status = StatusType.ACTIVE
        profile.save()
