from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import braintree
import os
from dateutil.parser import parse as parse_date
from django.conf import settings
from django.contrib import messages
from django.contrib.auth import authenticate, login
from django.contrib.auth import (get_user_model)
from django.contrib.auth.forms import SetPasswordForm
from django.contrib.auth.tokens import default_token_generator
from django.core.exceptions import PermissionDenied
from wsgiref.util import FileWrapper
from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.core.exceptions import SuspiciousOperation
from django.shortcuts import resolve_url
from django.template.defaultfilters import slugify
from django.template.response import TemplateResponse
from django.utils.http import urlsafe_base64_decode
from django.utils.translation import ugettext as _
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.views.generic import ListView
from django.views.generic import DetailView
from django.views.generic import UpdateView
from django.views.generic import base
from django.views.generic import edit
from django.views.generic.base import TemplateView
from django.views.generic.edit import FormMixin
from gouda.customers.forms import BraintreeMerchantAccountForm
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.photographers.models import Booking
from gouda.photographers.models import Photographer
from gouda.photographers.models import BookingRequest
from gouda.photographers.forms import BookingUpdateForm
from gouda.profiles.forms import ImageUpdateForm
from gouda.profiles.forms import MessageForm
from gouda.profiles.forms import ProfileEditForm
from gouda.profiles.forms import ProfileForm
from gouda.profiles.models import Message
from gouda.profiles.models import Profile
from gouda.profiles.models import ReferralType
from gouda.profiles.models import StatusType
from gouda.profiles.tokens import VerifyEmailAddressTokenGenerator
from gouda.photographers import views as photographer_views
from gouda.views import LoginRequiredMixin
from localflavor.us.us_states import STATES_NORMALIZED
from localflavor.us.us_states import STATE_CHOICES
from django.utils import six
from gouda.photographers.forms import BraintreeCustomerForm
from gouda.photographers.forms import BookingListForm
from gouda.customers.views import BraintreeCustomerCreateView
from gouda.customers.models import BraintreeCustomer
import django_filters
from django.db.models import Q
from localflavor.us import us_states
from datetime import date
from datetime import timedelta
from django.db.models import Q
import logging
from django.utils.encoding import force_text

logger = logging.getLogger(__name__)

# Braintree
BRAINTREE_MERCHANT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ID')
BRAINTREE_PUBLIC_KEY = getattr(settings, 'BRAINTREE_PUBLIC_KEY')
BRAINTREE_PRIVATE_KEY = getattr(settings, 'BRAINTREE_PRIVATE_KEY')
BRAINTREE_ENVIRONMENT = getattr(settings, 'BRAINTREE_ENVIRONMENT')
braintree.Configuration.configure(
    BRAINTREE_ENVIRONMENT,
    BRAINTREE_MERCHANT_ID,
    BRAINTREE_PUBLIC_KEY,
    BRAINTREE_PRIVATE_KEY
)


# Doesn't need csrf_protect since no-one can guess the URL
@sensitive_post_parameters()
@never_cache
def password_reset_confirm(request, uidb64=None, token=None,
                           template_name='registration/password_reset_confirm.html',
                           token_generator=default_token_generator,
                           set_password_form=SetPasswordForm,
                           post_reset_redirect=None,
                           current_app=None, extra_context=None, do_login_user=True):
    """
    View that checks the hash in a password reset link and presents a
    form for entering a new password.
    """
    UserModel = get_user_model()
    assert uidb64 is not None and token is not None  # checked by URLconf
    if post_reset_redirect is None:
        post_reset_redirect = reverse('password_reset_complete')
    else:
        post_reset_redirect = resolve_url(post_reset_redirect)
    try:
        # urlsafe_base64_decode() decodes to bytestring on Python 3
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = UserModel._default_manager.get(pk=uid)
    except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
        user = None

    if user is not None and token_generator.check_token(user, token):
        validlink = True
        title = _('Enter new password')
        if request.method == 'POST':
            form = set_password_form(user, request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect(post_reset_redirect)
        else:
            form = set_password_form(user)
    else:
        validlink = False
        form = None
        title = _('Password reset unsuccessful')
    context = {
        'form': form,
        'title': title,
        'validlink': validlink,
    }
    if extra_context is not None:
        context.update(extra_context)

    if current_app is not None:
        request.current_app = current_app

    return TemplateResponse(request, template_name, context)


class BraintreeMerchantAccountUpdateView(LoginRequiredMixin, edit.FormView):
    template_name = 'profiles/manage_account/braintree_merchant_account_update.html'
    success_url = reverse_lazy('profiles:braintree_merchant_account_update')
    form_class = BraintreeMerchantAccountForm
    merchant_account = None

    def get_merchant_account(self):
        user = self.request.user
        if hasattr(user, 'braintree_merchant_account') and not self.merchant_account:
            self.merchant_account = braintree.MerchantAccount.find(user.braintree_merchant_account.braintree_id)
        return self.merchant_account

    def get_form_kwargs(self):
        kwargs = super(BraintreeMerchantAccountUpdateView, self).get_form_kwargs()

        # Make form readonly if merchant account exists
        kwargs['is_readonly'] = False

        # Plug in user info server side...
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['funding_email'] = data.get('individual_email')
            data['funding_mobile_phone'] = data.get('individual_phone')
            data['user'] = self.request.user.pk
            kwargs['data'] = data
        return kwargs

    def get_initial(self):
        initial = super(BraintreeMerchantAccountUpdateView, self).get_initial()

        user = self.request.user
        initial['user'] = user.pk

        # Get merchant data
        merchant_account = self.get_merchant_account()
        if merchant_account:
            individual = merchant_account.individual_details.__dict__.copy()
            funding = merchant_account.funding_details.__dict__.copy()
            braintree_merchant_account_data = {
                'individual': individual,
                'funding': funding,
            }
            for prefix, data in braintree_merchant_account_data.iteritems():
                if prefix == 'individual':
                    data.update(data.get('address_details').__dict__.copy())
                    data.pop('_setattrs', None)
                    data.pop('address_details', None)
                    data.pop('address', None)
                    date_of_birth = parse_date(data.pop('date_of_birth'))
                    data.update({
                        'month': int(date_of_birth.strftime('%m')),
                        'day': int(date_of_birth.strftime('%d')),
                        'year': int(date_of_birth.strftime('%Y'))
                    })
                data.pop('_setattrs', None)
                for key in data.keys():
                    initial['%s_%s' % (prefix, key)] = data.get(key)
            initial['funding_account_number'] = '************%s' % initial.get('funding_account_number_last_4')
            initial['status'] = BraintreeMerchantAccountStatusType.get(user.braintree_merchant_account.status)
        else:
            profile = self.request.user.profile
            region = profile.region
            if region.lower() in STATES_NORMALIZED:
                region = STATES_NORMALIZED.get(region.lower())
            initial.update({
                'individual_email': profile.email_address,
                'individual_first_name': profile.first_name,
                'individual_last_name': profile.last_name,
                'individual_month': int(profile.date_of_birth.strftime('%m')),
                'individual_day': int(profile.date_of_birth.strftime('%d')),
                'individual_year': int(profile.date_of_birth.strftime('%Y')),
                'individual_region': region,

                'funding_email': profile.email_address,
            })
        return initial

    def form_valid(self, form):
        try:
            form.save()
            messages.info(self.request, 'Your payout information has been updated.')
        except AssertionError:
            messages.error(self.request, 'There was an error processing your payout information. Please contact us at support@shotzu.com.')
            logger.exception('Failed to create a Braintree merchant account')
        return super(BraintreeMerchantAccountUpdateView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context_data = super(BraintreeMerchantAccountUpdateView, self).get_context_data(**kwargs)
        initial = self.get_initial()
        context_data.update({
            'individual_month': initial.get('individual_month'),
            'individual_region': initial.get('individual_region'),
            'status': initial.get('status')
        })
        return context_data

    # def get(self, request, *args, **kwargs):
    #     # profile = self.request.user.profile
    #     # if not profile.has_rentals:
    #     #     raise PermissionDenied()
    #     return super(BraintreeMerchantAccountUpdateView, self).get(request, *args, **kwargs)

    # def post(self, request, *args, **kwargs):
    #     user = self.request.user
    #     if hasattr(user, 'braintree_merchant_account'):
    #         raise PermissionDenied()
    #     return super(BraintreeMerchantAccountUpdateView, self).post(request, *args, **kwargs)


class BookingAlbumDownloadView(LoginRequiredMixin, DetailView):
    model = Booking

    def get_queryset(self):
        if self.request.user.is_staff:
            return Booking.objects.all()
        return Booking.objects.filter(client=self.request.user.profile)

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        booking = self.object
        if not booking.media_file:
            booking.create_media_file()
        response = HttpResponseRedirect(request.build_absolute_uri(booking.media_file.url))
        # zip_file_path = booking.create_media_file()
        # if booking.display_name:
        #     zip_file_name = '%s.zip' % slugify(booking.display_name.lower())
        # else:
        #     zip_file_name = os.path.basename(zip_file_path)
        # with open(zip_file_path, 'r') as zip_file:
        #     response = HttpResponse(FileWrapper(zip_file), content_type='application/zip')
        # response['Content-Disposition'] = 'attachment; filename=%s' % zip_file_name
        return response


class BookingCsvDownloadView(LoginRequiredMixin, ListView):
    model = Booking

    def get_queryset(self):
        return Booking.objects.filter(client=self.request.user.profile)

    def get(self, request, *args, **kwargs):
        csv_file_path = Booking.create_csv_file(self.get_queryset())
        csv_file_name = os.path.basename(csv_file_path)
        with open(csv_file_path, 'r') as zip_file:
            response = HttpResponse(FileWrapper(zip_file), content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s' % csv_file_name
        return response


class CustomerUpdateView(BraintreeCustomerCreateView):
    form_class = BraintreeCustomerForm
    template_name = 'profiles/customer_update.html'
    success_url = reverse_lazy('profiles:customer_update')

    def get_initial(self):
        initial = super(CustomerUpdateView, self).get_initial()
        return initial

    def get_form_kwargs(self):
        kwargs = super(CustomerUpdateView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['user'] = self.request.user.pk
            kwargs['data'] = data
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super(CustomerUpdateView, self).get_context_data(**kwargs)

        # Get transaction info
        qs = BraintreeCustomer.objects.filter(user=self.request.user)
        if qs.exists():
            braintree_customer = qs[0]
            context_data['credit_card'] = braintree_customer.default_credit_card
        return context_data

    def form_valid(self, form):
        try:
            response = super(CustomerUpdateView, self).form_valid(form)
            messages.info(self.request, 'Your credit card information has been updated.')
        except AssertionError:
            response = self.form_invalid(form)
            messages.error(self.request, 'There was an error processing your payment details.')
        return response


class BookingDetailView(LoginRequiredMixin, DetailView):
    template_name = 'profiles/booking_detail.html'
    context_object_name = 'booking'
    model = Booking
    queryset = Booking.objects.all()

    def has_permission(self, object):
        return self.request.user.is_staff or object.client == self.request.user.profile

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        if not self.has_permission(booking):
            raise SuspiciousOperation('User does not have permission to access this booking')
        return super(BookingDetailView, self).get(request, *args, **kwargs)


class BookingFilter(django_filters.FilterSet):
    query = django_filters.MethodFilter(action='filter_query')
    region = django_filters.MethodFilter(action='filter_region')
    locality = django_filters.MethodFilter(action='filter_locality')

    def filter_query(self, queryset, query):
        if not query:
            return queryset
        lookup = Q(display_name__icontains=query)
        lookup = lookup | Q(foodbooking__restaurant_name__icontains=query)
        lookup = lookup | Q(address__normalized_locality__icontains=query)
        lookup = lookup | Q(address__normalized_region__icontains=query)
        lookup = lookup | Q(address__normalized_street_address__icontains=query)
        return queryset.filter(lookup)

    def filter_locality(self, queryset, locality):
        if locality:
            return queryset.filter(address__normalized_locality__iexact=locality)
        return queryset

    def filter_region(self, queryset, region):
        if region:
            return queryset.filter(address__normalized_region__iexact=region)
        return queryset

    def get_order_by(self, order_value):
        if 'locality' in order_value:
            return ['address__normalized_locality'] if '-' not in order_value else ['-address__normalized_locality']
        elif 'region' in order_value:
            return ['address__normalized_region'] if '-' not in order_value else ['-address__normalized_region']
        elif 'photographer' in order_value:
            return ['photographer__profile__user__full_name'] if '-' not in order_value else ['-photographer__profile__user__full_name']
        return super(BookingFilter, self).get_order_by(order_value)

    class Meta:
        model = Booking
        fields = ['region', 'locality']
        order_by = [
            'time_range',
            'photographer',
            'locality',
            'region',
            'display_name',
            '-time_range',
            '-photographer',
            '-locality',
            '-region',
            '-display_name',
        ]


class BookingListView(LoginRequiredMixin, FormMixin, ListView):
    template_name = 'profiles/booking_list.html'
    context_object_name = 'bookings'
    paginate_by = 20
    form_class = BookingListForm

    def get_initial(self):
        initial = super(BookingListView, self).get_initial()
        if self.request.GET:
            if self.request.GET.get('region'):
                initial['region'] = self.request.GET.get('region')
            if self.request.GET.get('locality'):
                initial['locality'] = self.request.GET.get('locality')
            if self.request.GET.get('query'):
                initial['query'] = self.request.GET.get('query')
        return initial

    def get_form_kwargs(self):
        form_kwargs = super(BookingListView, self).get_form_kwargs()

        queryset = Booking.objects.filter(client=self.request.user.profile)
        locality_choices = []
        region_choices = []
        for booking in queryset:
            if booking.address.normalized_locality:
                if self.request.GET.get('region'):
                    if booking.address.normalized_region == self.request.GET.get('region'):
                        locality_choices.append((booking.address.normalized_locality, booking.address.normalized_locality))
                # else:
                #     locality_choices.append((booking.address.normalized_locality, booking.address.normalized_locality))
            if booking.address.normalized_region:
                try:
                    region_choices.append((
                        booking.address.normalized_region,
                        [c[1] for c in STATE_CHOICES if c[0] == booking.address.normalized_region][0]
                    ))
                except Exception:
                    pass

        locality_choices = sorted(list(set(locality_choices)))
        locality_choices.insert(0, ('', 'All'))
        region_choices = sorted(list(set(region_choices)))
        region_choices.insert(0, ('', 'All'))

        form_kwargs.update({
            'locality_choices': locality_choices,
            'region_choices': region_choices,
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context_data = super(BookingListView, self).get_context_data(**kwargs)
        context_data['form'] = self.get_form()
        return context_data

    def get_queryset(self):
        booking_filter = BookingFilter(self.request.GET, queryset=Booking.objects.filter(client=self.request.user.profile))
        return booking_filter.qs
        # queryset = Booking.objects.filter(client=self.request.user.profile)
        # ordering = self.get_ordering()
        # if ordering:
        #     if isinstance(ordering, six.string_types):
        #         ordering = (ordering,)
        #     queryset = queryset.order_by(*ordering)
        #
        # return queryset


class ShootDetailView(LoginRequiredMixin, UpdateView):
    template_name = 'profiles/shoot_detail.html'
    context_object_name = 'booking'
    form_class = BookingUpdateForm
    model = Booking

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        if not self.has_permission(booking):
            raise SuspiciousOperation('User does not have permission to access this booking')
        return super(ShootDetailView, self).get(request, *args, **kwargs)

    def has_permission(self, object):
        return self.request.user.is_staff or object.photographer.profile == self.request.user.profile

    def form_valid(self, form):
        response = super(ShootDetailView, self).form_valid(form)
        messages.info(self.request, 'Your shoot has been rescheduled.')
        return response

    def get_queryset(self):
        return Booking.objects.all()

    def get_success_url(self):
        booking = self.get_object()
        return reverse('profiles:shoot_detail', kwargs={'pk': booking.id})

    def get_initial(self):
        initial = super(ShootDetailView, self).get_initial()
        booking = self.get_object()
        if booking.time_range and booking.address.time_zone:
            time_zone = booking.address.time_zone
            start_time = booking.time_range.lower.astimezone(time_zone)
            stop_time = booking.time_range.upper.astimezone(time_zone)
            initial['date'] = start_time.strftime('%m/%d/%Y')
            initial['start_time'] = start_time.strftime('%-I:%M %p')
            initial['stop_time'] = stop_time.strftime('%-I:%M %p')
        else:
            initial['date'] = (date.today() + timedelta(days=14)).strftime('%m/%d/%Y')
            initial['start_time'] = '8:00 AM'
            initial['stop_time'] = '11:00 AM'
        return initial


class ShootListView(LoginRequiredMixin, ListView):
    template_name = 'profiles/shoot_list.html'
    context_object_name = 'bookings'
    paginate_by = 20

    def get_queryset(self):
        booking_filter = BookingFilter(self.request.GET, queryset=Booking.objects.filter(photographer__profile=self.request.user.profile))
        return booking_filter.qs


class PayoutFilter(django_filters.FilterSet):
    def get_order_by(self, order_value):
        if 'date' in order_value:
            return ['client_approval_date'] if '-' not in order_value else ['-client_approval_date']
        elif 'amount' in order_value:
            return ['-service_fee_amount'] if '-' not in order_value else ['service_fee_amount']
        elif 'status' in order_value:
            return ['transaction_status'] if '-' not in order_value else ['-transaction_status']
        return super(PayoutFilter, self).get_order_by(order_value)

    class Meta:
        model = Booking
        order_by = [
            'date',
            'amount',
            'status',
            '-date',
            '-amount',
            '-status',
        ]


class PayoutListView(LoginRequiredMixin, ListView):
    template_name = 'profiles/payout_list.html'
    context_object_name = 'bookings'
    paginate_by = 20

    def get_queryset(self):
        booking_filter = PayoutFilter(self.request.GET, queryset=Booking.objects.filter(photographer__profile=self.request.user.profile))
        return booking_filter.qs


class InboxView(LoginRequiredMixin, ListView):
    template_name = 'profiles/inbox.html'
    context_object_name = 'messages'
    paginate_by = 20

    def get_queryset(self):
        return Message.objects.filter(receiver=self.request.user.profile).order_by('-date_created')


class ThreadView(LoginRequiredMixin, FormMixin, ListView):
    template_name = 'profiles/thread.html'
    context_object_name = 'messages'
    form_class = MessageForm
    paginate_by = 20

    def get_success_url(self):
        return reverse('profiles:thread', kwargs={'pk': self.kwargs.get('pk')})

    def form_valid(self, form):
        form.save()
        return super(ThreadView, self).form_valid(form)

    def get_receiver(self):
        profile = self.request.user.profile
        thread_id = self.kwargs.get('pk')
        message = Message.objects.filter(thread__id=thread_id)[0]
        return message.sender if profile.id != message.sender.id else message.receiver

    def get_initial(self):
        initial = super(ThreadView, self).get_initial()
        initial['sender'] = self.request.user.profile.id
        initial['receiver'] = self.get_receiver()
        initial['thread'] = self.kwargs.get('pk')
        return initial

    def get_context_data(self, **kwargs):
        context_data = super(ThreadView, self).get_context_data(**kwargs)
        context_data['form'] = self.get_form()
        context_data['receiver'] = self.get_receiver()
        return context_data

    def get(self, request, *args, **kwargs):
        profile = request.user.profile
        queryset = self.get_queryset()
        if not queryset:
            raise PermissionDenied()

        message = queryset[0]
        if profile not in [message.receiver, message.sender]:
            raise PermissionDenied()

        return super(ThreadView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        """
        Handles POST requests, instantiating a form instance with the passed
        POST variables and then checked for validity.
        """
        self.object_list = self.get_queryset()
        form = self.get_form()
        if form.is_valid():
            return self.form_valid(form)
        else:
            return self.form_invalid(form)

    # PUT is a valid HTTP verb for creating (with a known URL) or editing an
    # object, note that browsers only support POST for now.
    def put(self, *args, **kwargs):
        return self.post(*args, **kwargs)

    def get_queryset(self):
        thread_id = self.kwargs.get('pk')
        queryset = Message.objects.filter(thread__id=thread_id).order_by('-date_created')
        queryset.filter(receiver__id=self.request.user.profile.id).update(is_read=True)
        return Message.objects.filter(thread__id=thread_id).order_by('-date_created')


class ProfileUpdateView(LoginRequiredMixin, edit.UpdateView):
    template_name = 'profiles/update_profile.html'
    form_class = ProfileEditForm
    model = Profile
    success_url = reverse_lazy('profiles:update_profile')

    def get_context_data(self, **kwargs):
        profile = self.get_object()
        context_data = super(ProfileUpdateView, self).get_context_data(**kwargs)
        if profile.date_of_birth:
            context_data['month'] = int(profile.date_of_birth.strftime('%m'))
        return context_data

    def form_invalid(self, form):
        return super(ProfileUpdateView, self).form_invalid(form)

    def get_object(self, queryset=None):
        return self.request.user.profile

    def get_initial(self):
        initial = super(ProfileUpdateView, self).get_initial()
        profile = self.get_object()
        initial.update({
            'full_name': profile.full_name,
            'gender': profile.gender,

            'referral': profile.referral,
            # 'type': profile.type,

            'locality': profile.locality,
            'region': profile.region,
            'country': profile.country,

            'image': None
        })
        if profile.date_of_birth:
            initial.update({
                'day': int(profile.date_of_birth.strftime('%d')),
                'month': int(profile.date_of_birth.strftime('%m')),
                'year': int(profile.date_of_birth.strftime('%Y')),
            })
        return initial


class AccountEditView(LoginRequiredMixin, TemplateView):
    template_name = 'profiles/edit_account.html'

    # def get(self, request, *args, **kwargs):
    #     profile = request.user.profile
    #     if profile.id != int(kwargs.get('pk')):
    #         raise PermissionDenied()
    #     return super(InboxView, self).get(request, args, kwargs)


class ImageUpdateView(LoginRequiredMixin, edit.UpdateView):
    template_name = 'profiles/image_upload.html'
    model = Profile
    form_class = ImageUpdateForm

    def get_success_url(self):
        success_url = reverse('index')
        next_url = self.request.GET.get('next', None)
        if next_url:
            success_url = next_url
        return success_url

    def has_object_permissions(self):
        return self.request.user.pk == self.object.user.pk

    def get(self, request, *args, **kwargs):
        response = super(ImageUpdateView, self).get(request, *args, **kwargs)
        if self.has_object_permissions():
            return response
        else:
            raise PermissionDenied()

    def post(self, request, *args, **kwargs):
        response = super(ImageUpdateView, self).post(request, *args, **kwargs)
        if self.has_object_permissions():
            return response
        else:
            raise PermissionDenied()


class ProfileCreateView(edit.CreateView):
    template_name = 'profiles/create.html'
    form_class = ProfileForm
    model = Profile

    # def get_context_data(self, **kwargs):
    #     context_data = super(ProfileCreateView, self).get_context_data(**kwargs)
    #     context_data['next'] = self.request.GET.get('next')
    #     return context_data

    def get_success_url(self):
        success_url = reverse('profiles:image_update', kwargs={'pk': self.object.pk})
        next_url = self.request.GET.get('next', None)
        if next_url:
            success_url = '%s?next=%s' % (success_url, next_url)
        return success_url

    def get_initial(self):
        initial = super(ProfileCreateView, self).get_initial()
        initial['country'] = 'US'
        return initial

    def form_valid(self, form):
        self.object = form.save()
        user = authenticate(username=self.request.POST.get('email_address'), password=self.request.POST.get('password'))
        if user is not None:
            if user.is_active:
                login(self.request, user)
            else:
                raise Exception('Failed to log in a user with an inactive profile')
        else:
            raise Exception('Failed to log authenticate a user after creating their profile')

        return HttpResponseRedirect(self.get_success_url())


class VerifyEmailAddressView(base.RedirectView):
    permanent = False
    url = reverse_lazy('index')

    def get(self, request, uidb64=None, token=None, *args, **kwargs):
        # Use tokens and id to get user
        assert uidb64 is not None and token is not None  # checked by URLconf
        UserModel = get_user_model()
        try:
            uid = urlsafe_base64_decode(uidb64)
            user = UserModel._default_manager.get(pk=uid)
        except (TypeError, ValueError, OverflowError, UserModel.DoesNotExist):
            user = None

        # Set profile as verified if user exists else ...?
        if user is not None and VerifyEmailAddressTokenGenerator().check_token(user, token):
            user = authenticate(username=user.email_address, token=token)
            login(request, user)
            profile = user.profile
            profile.status = StatusType.ACTIVE
            profile.save()
        else:
            raise Http404()
        messages.info(request, 'Your email address has been verified.')
        return super(VerifyEmailAddressView, self).get(request, *args, **kwargs)


class BookingRequestDetailView(photographer_views.BookingRequestDetailView):
    template_name = 'profiles/booking_request_detail.html'


class BookingRequestFilter(django_filters.FilterSet):
    region = django_filters.MethodFilter(action='filter_region')
    locality = django_filters.MethodFilter(action='filter_locality')

    def filter_locality(self, queryset, locality):
        if locality:
            return queryset.filter(booking__address__normalized_locality__iexact=locality)
        return queryset

    def filter_region(self, queryset, region):
        if region:
            return queryset.filter(booking__address__normalized_region__iexact=region)
        return queryset

    def get_order_by(self, order_value):
        if 'time_range' in order_value:
            return ['booking__time_range'] if '-' not in order_value else ['-booking__time_range']
        elif 'locality' in order_value:
            return ['booking__address__normalized_locality'] if '-' not in order_value else ['-booking__address__normalized_locality']
        elif 'region' in order_value:
            return ['booking__address__normalized_region'] if '-' not in order_value else ['-booking__address__normalized_region']
        elif 'display_name' in order_value:
            return ['booking__display_name'] if '-' not in order_value else ['-booking__display_name']
        elif 'status' in order_value:
            return ['date_created']
        return super(BookingRequestFilter, self).get_order_by(order_value)

    class Meta:
        model = BookingRequest
        fields = ['region', 'locality']
        order_by = [
            'time_range',
            'locality',
            'region',
            'display_name',
            'status',
            '-time_range',
            '-locality',
            '-region',
            '-display_name',
            '-status'
        ]


class BookingRequestListView(LoginRequiredMixin, ListView):
    template_name = 'profiles/booking_request_list.html'
    context_object_name = 'booking_requests'
    paginate_by = 20

    def get(self, request, *args, **kwargs):
        try:
            photographer = self.request.user.profile.photographer
        except Photographer.DoesNotExist:
            raise SuspiciousOperation('%s does not have a photographer account' % self.request.user.profile.full_name)
        return super(BookingRequestListView, self).get(request, *args, **kwargs)

    def get_queryset(self):
        queryset = BookingRequest.objects.filter(photographer=self.request.user.profile.photographer)
        if 'status' in self.request.GET:
            query = Q(id=-1)
            for booking_request in queryset:
                if booking_request.status == self.request.GET['status']:
                    query = Q(id=booking_request.id) | query
            queryset = queryset.filter(query)
        booking_request_filter = BookingRequestFilter(self.request.GET, queryset=queryset)
        queryset = booking_request_filter.qs

        # Handle status ordering
        ordering = self.request.GET.get(BookingRequestFilter.order_by_field)
        if ordering and 'status' in ordering:
            def get_status_rank(booking_request):
                if booking_request.status == 'Pending':
                    return 0
                elif booking_request.status == 'Accepted':
                    return 1
                elif booking_request.status == 'Taken':
                    return 2
                else:
                    return 3
            queryset = sorted(queryset, key=get_status_rank, reverse='-' in ordering)

        return queryset

        # queryset = BookingRequest.objects.filter().order_by('-date_created')
        # ordering = self.get_ordering()
        # if ordering:
        #     if 'status' in ordering:
        #         def get_status_rank(booking_request):
        #             if booking_request.status == 'Pending':
        #                 return 0
        #             elif booking_request.status == 'Accepted':
        #                 return 1
        #             elif booking_request.status == 'Taken':
        #                 return 2
        #             else:
        #                 return 3
        #         queryset = sorted(queryset, key=get_status_rank, reverse='-' in ordering)
        #     else:
        #         if isinstance(ordering, six.string_types):
        #             ordering = (ordering,)
        #         queryset = queryset.order_by(*ordering)
        #
        # return queryset
