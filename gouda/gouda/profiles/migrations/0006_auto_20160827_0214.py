# -*- coding: utf-8 -*-
# Generated by Django 1.10 on 2016-08-27 02:14
from __future__ import unicode_literals

from django.db import migrations
import django_enumfield.db.fields
import gouda.profiles.models


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0005_profile_cc_recipients'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='gender',
            field=django_enumfield.db.fields.EnumField(default=0, enum=gouda.profiles.models.GenderType, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='referral',
            field=django_enumfield.db.fields.EnumField(default=6, enum=gouda.profiles.models.ReferralType),
        ),
        migrations.AlterField(
            model_name='profile',
            name='status',
            field=django_enumfield.db.fields.EnumField(default=0, enum=gouda.profiles.models.StatusType),
        ),
    ]
