# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0004_remove_profile_type'),
    ]

    operations = [
        migrations.AddField(
            model_name='profile',
            name='cc_recipients',
            field=django.contrib.postgres.fields.ArrayField(default=[], size=None, base_field=models.EmailField(max_length=254), blank=True),
        ),
    ]
