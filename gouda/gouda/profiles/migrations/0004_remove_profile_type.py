# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20151022_0348'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='type',
        ),
    ]
