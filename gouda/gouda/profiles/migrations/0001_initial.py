# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.profiles.models
import django_countries.fields
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('body', models.TextField()),
                ('is_read', models.BooleanField(default=False)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Profile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('gender', models.IntegerField(default=0)),
                ('date_of_birth', models.DateField()),
                ('image', easy_thumbnails.fields.ThumbnailerImageField(blank=True, upload_to=b'profile_images/', validators=[gouda.profiles.models.validate_image])),
                ('locality', models.CharField(default=b'', max_length=256)),
                ('region', models.CharField(default=b'', max_length=256)),
                ('country', django_countries.fields.CountryField(max_length=2)),
                ('status', models.IntegerField(default=0)),
                ('referral', models.IntegerField(default=6)),
                ('type', models.IntegerField(default=0)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ProfileShippingAddress',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('is_primary', models.BooleanField(default=False)),
                ('address', models.ForeignKey(to='addresses.Address')),
                ('profile', models.ForeignKey(to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='profile',
            name='shipping_addresses',
            field=models.ManyToManyField(to='addresses.Address', through='profiles.ProfileShippingAddress'),
        ),
    ]
