# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.profiles.models
import django_countries.fields
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20151003_2123'),
    ]

    operations = [
        migrations.AlterField(
            model_name='profile',
            name='country',
            field=django_countries.fields.CountryField(default=b'US', max_length=2),
        ),
        migrations.AlterField(
            model_name='profile',
            name='date_of_birth',
            field=models.DateField(null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='gender',
            field=models.IntegerField(default=0, null=True),
        ),
        migrations.AlterField(
            model_name='profile',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(blank=True, null=True, upload_to=b'profile_images/', validators=[gouda.profiles.models.validate_image]),
        ),
    ]
