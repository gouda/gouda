from __future__ import absolute_import

from rest_framework import serializers
from gouda.users.serializers import UserListSerializer
from gouda.profiles.models import Profile
from gouda.profiles.models import Message
from gouda.profiles.models import Thread
from django.conf import settings

STATIC_URL = getattr(settings, 'STATIC_URL')


class ProfileListSerializer(serializers.ModelSerializer):
    # url = serializers.HyperlinkedIdentityField(view_name='api:profile_detail')
    user = UserListSerializer(required=True)
    image = serializers.ImageField()

    def to_representation(self, instance):
        ret = super(ProfileListSerializer, self).to_representation(instance)
        if not ret.get('image'):
            ret['image'] = '%s%s' % (STATIC_URL, 'profiles/images/anonymous_profile.png')
        return ret

    class Meta:
        model = Profile
        exclude = ('date_modified', 'date_created', 'gender', 'date_of_birth', 'status', 'country')


class ProfileIDSerializer(serializers.ModelSerializer):
    def to_internal_value(self, data):
        queryset = Profile.objects.filter(**data)
        if not queryset.exists() or not queryset.count() == 1:
            raise serializers.ValidationError('Profile not found')
        return queryset[0]

    class Meta:
        model = Profile
        fields = ('id', )


class MessageSerializer(serializers.ModelSerializer):
    receiver = ProfileIDSerializer()

    def create(self, validated_data):
        receiver = validated_data.pop('receiver')
        sender = self.context.get('request').user.profile
        thread = Thread.objects.create()
        body = validated_data.pop('body')
        return Message.objects.create(
            body=body,
            receiver=receiver,
            sender=sender,
            thread=thread
        )

    class Meta:
        model = Message
        depth = 10
        fields = ['body', 'receiver']
