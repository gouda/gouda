from django.conf import settings

UNVERIFIED_PROFILE_SESSION_COOKIE_AGE = getattr(settings, 'UNVERIFIED_PROFILE_SESSION_COOKIE_AGE', None)


class UnverifiedProfileSessionMiddleware(object):
    """ Set the session expiry according to settings """
    def process_request(self, request):
        if not UNVERIFIED_PROFILE_SESSION_COOKIE_AGE:
            return None

        if not getattr(request, 'user', None):
            return None
        user = request.user

        if not getattr(user, 'profile', None):
            return None
        profile = user.profile

        if not profile.is_verified or True:
            request.session.set_expiry(UNVERIFIED_PROFILE_SESSION_COOKIE_AGE)

        return None