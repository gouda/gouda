{% autoescape off %}Hey {{ user.first_name }}!

We've received a request to reset your password. If you didn't make the request, you can ignore this email. Otherwise, you can reset your password using this link:
{% block reset_link %}
{{ protocol }}://{{ domain }}{% url 'profiles:password_reset_confirm' uidb64=uid token=token %}
{% endblock %}
{{ email_closing }}
{% endautoescape %}