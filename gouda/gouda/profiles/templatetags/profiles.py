from django import template

register = template.Library()


@register.assignment_tag(takes_context=True)
def is_read(context, message):
    profile = context.get('request').user.profile
    return profile.id == message.receiver.id and message.is_read
