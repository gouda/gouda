from __future__ import absolute_import

import stripe
from django.core.management.base import BaseCommand
from gouda.users.models import User
from gouda.profiles.models import Profile
from gouda.profiles.models import StatusType
from gouda.profiles.models import GenderType
from fabric.operations import prompt
from dateutil.parser import parse


def validate_date(text):
    return parse(text)


class Command(BaseCommand):
    def handle(self, *args, **options):
        profile_data = dict()
        profile_data['email_address'] = prompt('Email address:')
        profile_data['user'] = User.objects.get(email_address=profile_data['email_address'])

        profile_data['gender'] = prompt('Gender:', validate=r'male|female|other')
        for key, value in GenderType.labels.iteritems():
            if value.lower() == profile_data['gender']:
                profile_data['gender'] = key
                break

        profile_data['date_of_birth'] = prompt('DOB:', validate=validate_date)
        profile_data['country'] = prompt('Country:')
        profile_data['status'] = StatusType.ACTIVE

        profile_data.pop('email_address')
        Profile.objects.create(**profile_data)
