from django.contrib.sites.models import Site
from django.db.models.signals import post_save
from django.dispatch import receiver
from easy_thumbnails.signals import saved_file
from easy_thumbnails.signal_handlers import generate_aliases
from gouda.profiles.models import Profile
from gouda.profiles.models import Message
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template import loader
from django.core.mail import send_mail
from django.conf import settings
from gouda.profiles.tokens import VerifyEmailAddressTokenGenerator
from gouda.context_processors import constants
import gouda.postmaster.models
import string

IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')


@receiver(post_save, sender=Message)
def handle_message_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    # Build context
    if not IS_TEST_MODE:
        current_site = Site.objects.get_current()
        context = {
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
        }
    else:
        context = {
            'domain': 'testserver.com',
            'site_name': 'testserver',
            'protocol': 'http',
        }
    context['message'] = instance
    context.update(constants(None))

    # Render
    subject = loader.render_to_string('profiles/emails/message_notification_subject.txt', context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string('profiles/emails/message_notification_body.txt', context)

    # Send
    send_mail(subject=subject, message=body, from_email=DEFAULT_FROM_EMAIL, recipient_list=[instance.receiver.email_address])


@receiver(post_save, sender=Profile)
def handle_profile_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    # Build context
    user = instance.user
    token_generator = VerifyEmailAddressTokenGenerator()
    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'uid': urlsafe_base64_encode(force_bytes(user.pk)),
        'user': user,
        'token': token_generator.make_token(user),
    })
    context['profile'] = instance
    context.update(constants(None))

    # Render
    templates = [
        ('profiles/emails/verify_email_address_subject.txt', 'profiles/emails/verify_email_address_body.txt', DEFAULT_FROM_EMAIL, [instance.email_address]),
        ('profiles/emails/profile_notification_subject.txt', 'profiles/emails/profile_notification_body.txt', SERVER_EMAIL, STAFF_EMAILS),
    ]
    for subject_template, body_template, from_email, recipients in templates:
        subject = loader.render_to_string(subject_template, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(body_template, context)
        send_mail(subject=subject, message=body, from_email=from_email, recipient_list=recipients)

    # Add to postmaster
    user, is_created = gouda.postmaster.models.User.objects.get_or_create(profile=instance, defaults={})


# For thumbnails...?
saved_file.connect(generate_aliases)
