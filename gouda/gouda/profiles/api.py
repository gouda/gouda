import logging

from rest_framework import viewsets
from rest_framework import mixins
from rest_framework import authentication
from rest_framework import permissions
from gouda.profiles.models import Message
from gouda.profiles.serializers import MessageSerializer

logger = logging.getLogger(__name__)


class MessageViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = Message.objects.all()
    serializer_class = MessageSerializer
    authentication_classes = [authentication.SessionAuthentication]
    permission_classes = [permissions.IsAuthenticated]
