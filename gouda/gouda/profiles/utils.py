from __future__ import absolute_import

import base64
from datetime import date

import os
from django.core import mail
from django.core.cache import cache
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.db import transaction
from django.db.utils import IntegrityError
from gouda.profiles.models import GenderType
from gouda.profiles.models import Message
from gouda.profiles.models import Profile
from gouda.profiles.models import ReferralType
from gouda.profiles.models import StatusType as ProfileStatusType
from gouda.profiles.models import Thread
from gouda.users.models import User
from rest_framework import status
from django.conf import settings


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), 'fixtures')
PEOPLE = [
    {
        'full_name': 'Sterling Archer',
        'gender': GenderType.MALE
    },
    {
        'full_name': 'Algernop Krieger',
        'gender': GenderType.MALE
    },
    {
        'full_name': 'Cheryl Tunt',
        'gender': GenderType.FEMALE
    },
    {
        'full_name': 'Cyril Figgis',
        'gender': GenderType.MALE,
    },
    {
        'full_name': 'Lana Kane',
        'gender': GenderType.FEMALE,
    },
    {
        'full_name': 'Malory Archer',
        'gender': GenderType.FEMALE,
    },
    {
        'full_name': 'Pam Poovey',
        'gender': GenderType.FEMALE,
    },
    {
        'full_name': 'Brett Bunsen',
        'gender': GenderType.MALE,
    },
]


class ProfileCreateViewMixin(object):
    def create_profile(self, form_data):
        url = reverse('profiles:create')
        response = self.app.get(url)
        form = response.form
        form['full_name'] = form_data['full_name']
        form['email_address'] = form_data['email_address']
        form['password'] = form_data['password']
        form['month'] = form_data['month']
        form['day'] = form_data['day']
        form['year'] = form_data['year']
        form['gender'] = form_data['gender']
        form['referral'] = form_data['referral']
        form['locality'] = form_data['locality']
        form['region'] = form_data['region']
        form['country'] = form_data['country']
        response = form.submit()
        return response


class ProfileDataMixin(object):
    archer_profile_form_data = {
        'full_name': 'Sterling Archer',
        'email_address': 'success+sterlingarcher@simulator.amazonses.com',
        'password': 'dangerzone123DANGERZONE!@#',
        'gender': GenderType.MALE,
        'month': 8,
        'day': 23,
        'year': 1986,
        'locality': 'New York City',
        'region': 'NY',
        'country': 'US',
        'referral': ReferralType.INSTAGRAM
    }
    lana_profile_form_data = {
        'full_name': 'Lana Kane',
        'email_address': 'success+lanakane@simulator.amazonses.com',
        'password': 'ARCHER!!!!!',
        'gender': GenderType.FEMALE,
        'month': 11,
        'day': 4,
        'year': 1991,
        'country': 'US',
        'locality': 'New York City',
        'region': 'NY',
        'referral': ReferralType.FACEBOOK
    }

    @staticmethod
    def get_user_data(form_data):
        form_data = form_data.copy()
        keys = list(form_data.keys())
        for key in keys:
            if key not in ['email_address', 'password', 'full_name']:
                form_data.pop(key, None)
        return form_data

    @staticmethod
    def get_profile_data(form_data):
        form_data = form_data.copy()
        form_data['date_of_birth'] = date(
            year=form_data.pop('year'),
            month=form_data.pop('month'),
            day=form_data.pop('day')
        )
        keys = list(form_data.keys())
        for key in keys:
            if key not in ['date_of_birth', 'image']:
                form_data.pop(key, None)
        return form_data


class ProfileImageUploadMixin(object):
    def encode_image(self, image_name):
        image_path = self.get_image_path(image_name)
        root, ext = os.path.splitext(image_path)
        ext = ext.strip('.')
        with open(image_path, 'rb') as image_file:
            encoded_text = '%s' % (base64.b64encode(image_file.read()))
        self.assertIsNotNone(encoded_text)
        return encoded_text

    def get_image_path(self, image_name):
        image_path = os.path.join(os.path.dirname(__file__), 'fixtures/%s' % image_name)
        return image_path


class SetUpProfilesMixin(object):
    def setUp(self):
        tear_down_profiles()
        user_data = self.get_user_data(self.archer_profile_form_data)
        self.archer_user = User.objects.create_user(**user_data)

        user_data = self.get_user_data(self.lana_profile_form_data)
        self.lana_user = User.objects.create_user(**user_data)

        profile_data = self.get_profile_data(self.archer_profile_form_data)
        self.archer_profile = Profile.objects.create(user=self.archer_user, **profile_data)

        profile_data = self.get_profile_data(self.lana_profile_form_data)
        self.lana_profile = Profile.objects.create(user=self.lana_user, **profile_data)

        mail.outbox = []


class PasswordResetMixin(object):
    def password_reset(self, email_address):
        url = reverse('profiles:password_reset')
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        form['email_address'] = email_address
        response = form.submit()
        return response


def get_user_data(person):
    full_name = person['full_name']
    prefix = full_name.replace(' ', '').lower()
    password = prefix + '123'
    email_address = 'success+%s@simulator.amazonses.com' % prefix
    user_data = locals()
    user_data.pop('person', None)
    user_data.pop('prefix', None)
    return user_data


def get_profile_image(full_name):
    image_name = full_name.replace(' ', '_').lower()
    profile_images_path = os.path.join(FIXTURES_DIR, 'profile_images/')
    for file in os.listdir(profile_images_path):
        image_file_path = os.path.join(profile_images_path, file)
        if not os.path.isfile(image_file_path):
            continue
        if image_name in image_file_path:
            return ContentFile(open(image_file_path).read(), name=os.path.basename(image_file_path))
    return None


def get_profile_data(person, is_superuser=False):
    user_data = get_user_data(person)

    # Create user
    try:
        with transaction.atomic():
            user = User.objects.create_user(**user_data)
    except IntegrityError:
        user = User.objects.get(email_address=user_data.get('email_address'))
    user.is_superuser = is_superuser
    user.is_staff = is_superuser
    user.save()
    gender = person['gender']
    date_of_birth = date(year=1970, month=1, day=1)
    image = get_profile_image(person['full_name'])
    locality = 'New York City'
    region = 'NY'
    country = 'US'
    status = ProfileStatusType.ACTIVE
    profile_data = locals()
    profile_data.pop('person', None)
    profile_data.pop('user_data', None)
    profile_data.pop('is_superuser', None)
    return profile_data


def create_profiles(num_profiles=len(PEOPLE), is_superuser=False):
    profiles = list()
    num_profiles = min(num_profiles, len(PEOPLE))
    for person in PEOPLE[:num_profiles]:
        profile_data = get_profile_data(person, is_superuser)
        user = profile_data.pop('user')
        profile, is_created = Profile.objects.get_or_create(user=user, defaults=profile_data)
        profiles.append(profile)
    mail.outbox = []
    return profiles


def tear_down_profiles():
    User.objects.all().delete()
    Profile.objects.all().delete()
    Message.objects.all().delete()
    Thread.objects.all().delete()
    cache.clear()


