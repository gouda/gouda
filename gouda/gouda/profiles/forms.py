from __future__ import absolute_import

from datetime import date
from datetime import timedelta

from django_countries import countries
from django import forms
from django.utils import timezone
from gouda.users.models import PASSWORD_MIN_LENGTH
from gouda.users.models import User
from gouda.profiles.models import Profile
from gouda.profiles.models import ProfileType
from gouda.profiles.models import Message
from gouda.profiles.models import Thread
from gouda.profiles.models import ReferralType
from gouda.profiles.models import GenderType
from gouda.users.forms import UserRegistrationForm
from gouda.forms.forms import Base64ImageField
from gouda.forms.forms import Select
from gouda.forms.widgets import TextInput
from gouda.forms.widgets import PasswordInput
from django.contrib.auth import forms as auth_forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.utils.http import urlsafe_base64_encode
from django.utils.encoding import force_bytes
from django.template import loader
from django.core.mail import send_mail
from gouda.profiles.tokens import VerifyEmailAddressTokenGenerator
from django.conf import settings
from django.forms.models import inlineformset_factory
from gouda.forms.widgets import KendoDropDownList
from localflavor.us.us_states import STATE_CHOICES
from django.core.mail import EmailMessage

UNVERIFIED_PROFILE_SESSION_COOKIE_AGE = getattr(settings, 'UNVERIFIED_PROFILE_SESSION_COOKIE_AGE', 86400)
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')


class SendVerificationEmailMixin:
    @staticmethod
    def send_verification_email(user,
                                request,
                                domain_override=None,
                                email_template_name='profiles/emails/verify_email_address_body.txt',
                                subject_template_name='profiles/emails/verify_email_address_subject.txt',
                                html_email_template_name=None,
                                from_email=None,
                                token_generator=VerifyEmailAddressTokenGenerator(),
                                use_https=True):
        use_https = False

        # Domain
        if not domain_override:
            current_site = get_current_site(request)
            site_name = current_site.name
            domain = current_site.domain
        else:
            site_name = domain = domain_override

        # Send verification email
        context = {
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',
        }
        subject = loader.render_to_string(subject_template_name, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)
        if html_email_template_name:
            html_email = loader.render_to_string(html_email_template_name, context)
        else:
            html_email = None
        send_mail(subject, body, from_email, [user.email_address], html_message=html_email)


class MessageForm(forms.ModelForm):
    class Meta:
        BODY_ATTRIBUTES = {
            'required': '',
            'ng-model': 'body'
        }
        model = Message
        fields = ['thread', 'body', 'sender', 'receiver']
        widgets = {
            'thread': forms.HiddenInput(),
            'sender': forms.HiddenInput(),
            'receiver': forms.HiddenInput(),
            'body': forms.Textarea(attrs=BODY_ATTRIBUTES)
        }


class SetPasswordForm(auth_forms.SetPasswordForm):
    PASSWORD1_ATTRIBUTES = {
        'type': 'password',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Password',
        'ng-model': 'password1',
        'ng-minlength': PASSWORD_MIN_LENGTH,
        'minlength': PASSWORD_MIN_LENGTH,
    }

    PASSWORD2_ATTRIBUTES = {
        'type': 'password',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Confirm Password',
        'ng-model': 'password2',
        'compare-to': 'password1',
        'ng-minlength': PASSWORD_MIN_LENGTH,
        'minlength': PASSWORD_MIN_LENGTH,
    }

    new_password1 = forms.CharField(min_length=PASSWORD_MIN_LENGTH, label=_('New password'), widget=forms.PasswordInput(attrs=PASSWORD1_ATTRIBUTES))
    new_password2 = forms.CharField(min_length=PASSWORD_MIN_LENGTH, label=_('Confirm password'), widget=forms.PasswordInput(attrs=PASSWORD2_ATTRIBUTES))

    # def clean_new_password2(self):
    #     password2 = super(SetPasswordForm, self).clean_new_password2()
    #     self.validate_password(password2)
    #     return password2
    #
    # def clean_new_password1(self):
    #     password1 = self.cleaned_data.get('new_password1')
    #     self.validate_password(password1)
    #     return password1


class PasswordResetForm(forms.Form):
    EMAIL_ADDRESS_ATTRIBUTES = {
        'type': 'email',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Email Address',
        'ng-model': 'emailAddress'
    }
    email_address = forms.EmailField(label=_('Email address'), max_length=256, widget=forms.TextInput(attrs=EMAIL_ADDRESS_ATTRIBUTES))

    def save(self, domain_override=None,
             subject_template_name='profiles/emails/password_reset_subject.txt',
             email_template_name='profiles/emails/password_reset_body.txt',
             use_https=False, token_generator=default_token_generator,
             from_email=None, request=None, html_email_template_name=None):
        """
        Generates a one-use only link for resetting password and sends to the
        user.
        """
        from django.core.mail import send_mail
        UserModel = get_user_model()
        email_address = self.cleaned_data['email_address']
        active_users = UserModel._default_manager.filter(
            email_address__iexact=email_address, is_active=True)
        for user in active_users:
            # Make sure that no email is sent to a user that actually has
            # a password marked as unusable
            if not user.has_usable_password():
                continue
            if not domain_override:
                current_site = get_current_site(request)
                site_name = current_site.name
                domain = current_site.domain
            else:
                site_name = domain = domain_override
            c = {
                'email': user.email_address,
                'domain': domain,
                'site_name': site_name,
                'uid': urlsafe_base64_encode(force_bytes(user.pk)),
                'user': user,
                'token': token_generator.make_token(user),
                'protocol': 'https' if use_https else 'http',
            }
            subject = loader.render_to_string(subject_template_name, c)
            # Email subject *must not* contain newlines
            subject = ''.join(subject.splitlines())
            email = loader.render_to_string(email_template_name, c)

            if html_email_template_name:
                html_email = loader.render_to_string(html_email_template_name, c)
            else:
                html_email = None
            # send_mail(subject, email, from_email, [user.email_address], html_message=html_email)
            email_message = EmailMessage(subject=subject, body=email, from_email=from_email, to=[user.email_address], bcc=STAFF_EMAILS)
            email_message.send()


class LoginForm(SendVerificationEmailMixin, auth_forms.AuthenticationForm):
    EMAIL_ADDRESS_ATTRIBUTES = {
        'type': 'email',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Email Address',
        'ng-model': 'username'
    }
    PASSWORD_ATTRIBUTES = {
        'type': 'password',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Password',
        'ng-model': 'password'
    }
    username = forms.CharField(max_length=256, widget=forms.TextInput(attrs=EMAIL_ADDRESS_ATTRIBUTES))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput(attrs=PASSWORD_ATTRIBUTES))

    error_messages = {
        'invalid_login': _("Invalid email address or password."),
        'inactive': _("This account is inactive."),
        'unverified': _("Please verify your email address. A new verification email has been sent to you."),
    }

    def confirm_login_allowed(self, user):
        super(LoginForm, self).confirm_login_allowed(user)
        if user.last_login and not user.profile.is_verified and (timezone.now() - user.last_login) >= timedelta(seconds=UNVERIFIED_PROFILE_SESSION_COOKIE_AGE):
            self.send_verification_email(user, self.request)
            raise forms.ValidationError(self.error_messages.get('unverified'), code='unverified')


PROFILE_TYPE_CHOICES = [
    (ProfileType.CLIENT, 'No'),
    (ProfileType.PHOTOGRAPHER, 'Yes'),
]

TYPE_ATTRIBUTES = {
    'ng-cloak': '',
    'required': '',
    'kendo-drop-down-list': 'typeDropDownList',
    'ng-model': 'type',
}


class ProfileForm(SendVerificationEmailMixin, forms.ModelForm):
    MONTH_CHOICES = [
        (1, 'January'),
        (2, 'February'),
        (3, 'March'),
        (4, 'April'),
        (5, 'May'),
        (6, 'June'),
        (7, 'July'),
        (8, 'August'),
        (9, 'September'),
        (10, 'October'),
        (11, 'November'),
        (12, 'December'),
    ]
    FULL_NAME_ATTRIBUTES = {
        'class': 'k-textbox',
        'ng-maxlength': '256',
        'maxlength': '256',
        'required': '',
        'placeholder': 'Full Name',
        'ng-model': 'fullName'
    }
    LOCALITY_ATTRIBUTES = {
        'class': 'k-textbox',
        'ng-maxlength': '256',
        'maxlength': '256',
        'required': '',
        'placeholder': 'City',
        'ng-model': 'locality'
    }
    REGION_ATTRIBUTES = {
        'class': 'k-textbox',
        'ng-maxlength': '256',
        'maxlength': '256',
        'required': '',
        'placeholder': 'State/Region',
        'ng-model': 'region'
    }
    EMAIL_ADDRESS_ATTRIBUTES = {
        'type': 'email',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Email Address',
        'ng-model': 'emailAddress'
    }
    PASSWORD_ATTRIBUTES = {
        'type': 'password',
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Password',
        'ng-model': 'password',
        'ng-minlength': PASSWORD_MIN_LENGTH,
        'minlength': PASSWORD_MIN_LENGTH,
    }
    MONTH_ATTRIBUTES = {
        'ng-cloak': '',
        'required': '',
        'kendo-drop-down-list': 'monthDropDownList',
        'ng-model': 'month',
        # 'k-option-label': 'Month'
    }

    DAY_ATTRIBUTES = {
        'class': 'k-textbox',
        'required': '',
        'placeholder': 'Day',
        'ng-model': 'day',
        'ng-pattern': '/^[1-9][0-9]{0,1}$/',
    }

    YEAR_ATTRIBUTES = {
        'class': 'k-textbox',
        'ng-pattern': '/^(19|20)\d{2}$/',
        'required': '',
        'placeholder': 'Year',
        'ng-model': 'year',
        'maxlength': 4,
        'ng-maxlength': 4,
    }

    YEAR_ERROR_MESSAGES = {
        'min_value': 'Invalid year',
        'max_value': 'Invalid year',
    }

    DAY_ERROR_MESSAGES = {
        'min_value': 'Invalid day',
        'max_value': 'Invalid day',
    }

    GENDER_ATTRIBUTES = {
        'ng-cloak': '',
        'required': '',
        'kendo-drop-down-list': 'genderDropDownList',
        'ng-model': 'gender',
        # 'k-option-label': 'Gender'
    }

    COUNTRY_ATTRIBUTES = {
        'ng-cloak': '',
        'required': '',
        'kendo-drop-down-list': 'countryDropDownList',
        'ng-model': 'country',
    }

    REFERRAL_ATTRIBUTES = {
        'ng-cloak': '',
        'required': '',
        'kendo-drop-down-list': 'referralDropDownList',
        'ng-model': 'referral',
    }

    full_name = forms.CharField(widget=TextInput(attrs={'placeholder': 'Full Name'}))
    email_address = forms.EmailField(widget=TextInput(attrs={
        'type': 'email',
        'placeholder': 'Email Address',
    }))
    password = forms.CharField(min_length=PASSWORD_MIN_LENGTH, widget=PasswordInput(attrs={
        'placeholder': 'Password',
        'data-parsley-minlength': PASSWORD_MIN_LENGTH,
    }))
    month = forms.ChoiceField(widget=forms.Select(choices=MONTH_CHOICES), choices=MONTH_CHOICES)
    day = forms.IntegerField(error_messages=DAY_ERROR_MESSAGES, min_value=1, max_value=31, widget=TextInput(attrs={
        'placeholder': 'Day',
        'data-parsley-min': '1',
        'data-parsley-max': '31',
        'type': 'number'
    }))
    year = forms.IntegerField(error_messages=YEAR_ERROR_MESSAGES, min_value=1900, max_value=date.today().year, widget=TextInput(attrs={
        'placeholder': 'Year',
        'data-parsley-min': '1900',
        'data-parsley-max': '2016',
        'type': 'number'
    }))
    gender = forms.ChoiceField(widget=forms.Select(choices=GenderType.choices()), choices=GenderType.choices())
    locality = forms.CharField(widget=TextInput(attrs={
        'placeholder': 'City',
    }))
    region = forms.CharField(widget=forms.Select(choices=STATE_CHOICES))
    country = forms.ChoiceField(widget=forms.Select(choices=countries), choices=countries)
    referral = forms.ChoiceField(widget=forms.Select(choices=ReferralType.choices()), choices=ReferralType.choices())

    def clean_gender(self):
        gender = self.cleaned_data.get('gender')
        return int(gender)

    def clean_referral(self):
        referral = self.cleaned_data.get('referral')
        return int(referral)

    def clean_month(self):
        month = self.cleaned_data.get('month')
        return int(month)

    def clean(self):
        cleaned_data = super(ProfileForm, self).clean()
        try:
            cleaned_data['date_of_birth'] = date(year=cleaned_data['year'], month=cleaned_data['month'], day=cleaned_data['day'])
        except:
            raise forms.ValidationError({'year': 'Please enter a valid date of birth.'})

        if (date.today() - cleaned_data['date_of_birth']) < timedelta(days=18 * 365):
            raise forms.ValidationError({'year': 'To sign up, you must be 18 or older.'})

        if self.instance.pk is None:
            user_registration_form = UserRegistrationForm(cleaned_data)
            if not user_registration_form.is_valid():
                raise forms.ValidationError(user_registration_form.errors)
            user = user_registration_form.save()
            cleaned_data['user'] = user
        else:
            cleaned_data['user'] = self.instance.user

        return cleaned_data

    def save(self, commit=True):
        defaults = self.cleaned_data.copy()
        defaults.pop('month')
        defaults.pop('day')
        defaults.pop('year')
        defaults.pop('password')
        defaults.pop('full_name')
        defaults.pop('email_address')
        user = defaults.pop('user')
        profile, is_created = Profile.objects.update_or_create(user=user, defaults=defaults)
        return profile

    class Meta:
        model = Profile
        fields = ['gender', 'country', 'referral', 'locality', 'region']


class ProfileEditForm(ProfileForm):
    IMAGE_ATTRIBUTES = {
        'class': 'hidden',
        'ng-model': 'croppedImageData'
    }
    password = forms.CharField(required=False)
    email_address = forms.EmailField(required=False)
    image = Base64ImageField(required=False, widget=forms.Textarea(attrs=IMAGE_ATTRIBUTES))

    def save(self,
             request=None,
             commit=True,
             domain_override=None,
             email_template_name='profiles/emails/verify_email_address_body.txt',
             subject_template_name='profiles/emails/verify_email_address_subject.txt',
             html_email_template_name=None,
             from_email=None,
             token_generator=VerifyEmailAddressTokenGenerator(),
             use_https=True):
        profile = super(ProfileEditForm, self).save(commit=False)
        user = profile.user
        user.full_name = self.cleaned_data['full_name']
        if commit:
            user.save()
            profile.save()
        return profile

    class Meta:
        model = Profile
        fields = ['gender', 'country', 'referral', 'locality', 'region', 'image']


class ImageUpdateForm(forms.ModelForm):
    IMAGE_ATTRIBUTES = {
        'class': 'hidden',
        'ng-model': 'croppedImageData'
    }
    image = Base64ImageField(widget=forms.Textarea(attrs=IMAGE_ATTRIBUTES))

    class Meta:
        model = Profile
        fields = ['image']
