var module = angular.module('gouda.profiles.directives', []);

module.directive('input', ['$parse', function ($parse) {
  return {
    restrict: 'E',
    require: '?ngModel',
    link: function (scope, element, attrs) {
      if (attrs.ngModel && attrs.value) {
          if (attrs.type && (attrs.type === 'number')) {
              $parse(attrs.ngModel).assign(scope, parseInt(attrs.value));
          } else {
              $parse(attrs.ngModel).assign(scope, attrs.value);
          }
      }
    }
  };
}]);

module.directive('textarea', ['$parse', function ($parse) {
  return {
    restrict: 'E',
    require: '?ngModel',
    link: function (scope, element, attrs) {
      if (attrs.ngModel) {
        $parse(attrs.ngModel).assign(scope, element.val());
      }
    }
  };
}]);

module.directive('select', ['$parse', function ($parse) {
  return {
    restrict: 'E',
    require: '?ngModel',
    link: function (scope, element, attrs) {
      if (attrs.ngModel && attrs.value) {
        $parse(attrs.ngModel).assign(scope, attrs.value);
      }
    }
  };
}]);

module.directive('compareTo', function () {
  return {
    require: "ngModel",
    scope: {
      otherModelValue: "=compareTo"
    },
    link: function(scope, element, attributes, ngModel) {

      ngModel.$validators.compareTo = function(modelValue) {
        return modelValue == scope.otherModelValue;
      };

      scope.$watch("otherModelValue", function() {
        ngModel.$validate();
      });
    }
  };
});

module.factory('DropzoneAPI', function(){
    return {
        dropzone: undefined,
        listeners: [],
        addListener: function(listener) {
            this.listeners.push(listener);
        },
        addedfile: function(file) {
            angular.forEach(this.listeners, function(listener, key){
                listener.addedfile(file);
            });
        },
        removedfile: function(file) {
            angular.forEach(this.listeners, function(listener, key){
                listener.removedfile(file);
            });
        }
    }
});

module.directive('mjDropzone', ['DropzoneAPI', function(DropzoneAPI){
    function link(scope, element, attrs, ctrl) {
        scope.maxFiles = 20;
        var addPhotoButton = element.find('.add-photo-button')[0];
        var previewsContainer = element.find('.dropzone-previews')[0];
        var previewTemplate = element.find('.preview-template')[0];

        scope.getNumValidFiles = function() {
            var i;
            var count = 0;
            for (i = 0; i < scope.files.length; i++) {
                if (scope.files[i].isValid) {
                    count++;
                }
            }
            return count;
        };

        scope.isMaxFilesExceeded = function() {
            return scope.getNumValidFiles() >= (scope.maxFiles + 1);
        };
        scope.dropzone = element[0].dropzone;

        //scope.init = function() {
        //    console.log('Running init()');
        //    var dropzone = scope.dropzone;
        //    console.log(dropzone);
        //    angular.forEach(scope.files, function(file, key) {
        //        if (!file.isValid) {
        //            return;
        //        }
        //        file.wasBootstrapped = true;
        //        dropzone.addFile(file);
        //    });
        //};

        element.dropzone({
            init: function() {
                var dropzone = this;
                scope.dropzone = dropzone;
                DropzoneAPI.dropzone = dropzone;
                angular.forEach(scope.files, function(file, key) {
                    if (!file.isValid) {
                        return;
                    }
                    file.wasBootstrapped = true;
                    dropzone.addFile(file);
                });
            },
            url: '/',
            autoProcessQueue: false,
            clickable: addPhotoButton,
            previewsContainer: previewsContainer,
            maxFilesize: 5,
            maxFiles: scope.maxFiles,
            acceptedFiles: '.jpg,.jpeg,.png',
            thumbnailWidth: 500,
            thumbnailHeight: 500,
            previewTemplate: previewTemplate.innerHTML,
            accept: function(file, done) {
                if (scope.isMaxFilesExceeded()) {
                    done('You can not upload any more files.');
                } else {
                    done();
                }
            },
            addedfile: function(file) {
                if (file.wasBootstrapped) {
                    $('.dropzone-previews').append(file.previewElement);
                    return;
                }
                var dropzone = this;
                //file.previewElement = Dropzone.createElement(this.options.previewTemplate);
                file.isValid = true;
                file.id = (new Date()).getTime();
                file.previewElement = $($.parseHTML(this.options.previewTemplate));
                scope.files.push(file);

                function attachFile(file) {
                    // Now attach this new element some where in your page
                    $('.dropzone-previews').append(file.previewElement);

                    if (scope.getNumValidFiles() >= dropzone.options.maxFiles) {
                        dropzone.disable();
                    } else {
                        dropzone.enable();
                    }
                    if (!scope.isMaxFilesExceeded()) {
                        DropzoneAPI.addedfile(file);
                    }
                    _.defer(function(){
                        scope.$apply();
                    });
                }

                // Read the data into base64
                var fileReader = new FileReader();
                fileReader.onload = function(e) {
                    var dataURL = fileReader.result;
                    file.dataURL = dataURL;
                    attachFile(file);
                };

                if (file.hasOwnProperty('image')) {
                    attachFile(file);
                } else {
                    fileReader.readAsDataURL(file);
                }
            },
            removedfile: function(file) {
                var i;
                var dropzone = this;
                file.previewElement.remove();
                for (i = 0; i < scope.files.length; i++) {
                    if (file.id == scope.files[i].id) {
                        scope.files.splice(i, 1);
                        break;
                    }
                }
                if (scope.getNumValidFiles() >= dropzone.options.maxFiles) {
                    dropzone.disable();
                } else {
                    dropzone.enable();
                }
                _.defer(function(){
                    scope.$apply();
                });

                if (!scope.isMaxFilesExceeded()) {
                    DropzoneAPI.removedfile(file);
                }
            },
            complete: function(file) {
            },
            error: function(file, errorMessage, xhr) {
                if (errorMessage == 'Upload canceled.') {
                    return;
                }
                file.isValid = false;
                file.dataURL = null;
                file.previewElement.addClass('ng-invalid');
                var error = file.previewElement.find('.dz-error');
                var title = error.find('.title');
                title.html(errorMessage);
                _.defer(function(){
                    scope.$apply();
                });
            },
            maxfilesreached: function(file) {

            },
            maxfilesexceeded: function(file) {

            },
            thumbnail: function(file, dataUrl) {
                var dropzone = scope.dropzone;
                var removeButton = file.previewElement.find('.dz-remove');
                removeButton.click(function(){
                    if (scope.files.length < 2) {
                        return;
                    }
                    dropzone.removeFile(file);
                });
                var imageContainer = file.previewElement.find('.dz-image');
                imageContainer.css('background-image', 'url(' + dataUrl + ')');
                imageContainer.css('background-size', 'cover');
                imageContainer.css('background-position', 'center center');
                imageContainer.css('width', '100%');
                imageContainer.css('height', '100%');
                _.defer(function(){
                    scope.$apply();
                });
            }
        });
    }

    return {
        link: link,
        templateUrl: 'profiles/dropzone.html',
        scope: {
            files: '='
        }
    }
}]);
