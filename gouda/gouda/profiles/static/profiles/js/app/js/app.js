var ProfileApp = angular.module('gouda.profiles.apps.ProfileApp', [
        'ngMaterial',
        'ngSanitize',
        'ngAnimate',
        'ngMask',
        'kendo.directives',
        'ngImgCrop',
        'uiGmapgoogle-maps',
        'gouda.profiles.controllers',
        'gouda.profiles.directives',
        'gouda.profiles.templates',
        'gouda.listings.services',
        'gouda.directives'
    ]
);

ProfileApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);
