'use strict';

/* Controllers */

var profileControllers = angular.module('gouda.profiles.controllers', []);

profileControllers.factory('bookingFormService', function() {
    return {
        booking: undefined,
    }
});

profileControllers.controller('MessageCreateFormController', ['$scope',
    function($scope) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('ProfileDashboardController', ['$scope', '$window',
    function($scope, $window) {
        //angular.element($window).bind('scroll', function(){
        //    //console.log($scope.isSidebarFixed);
        //});
        //$scope.fixSidebar = function() {
        //    $scope.isSidebarFixed = $window.pageYOffset >= 50;
        //    if(!$scope.$$phase) {
        //        $scope.$apply();
        //    }
        //};
        //angular.element($window).bind('scroll', $scope.fixSidebar);
        //$scope.fixSidebar();
        //$scope.isFormValid = function() {
        //    return $scope.form.$valid;
        //};
    }
]);

profileControllers.controller('ProfileLoginFormController', ['$scope',
    function($scope) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('ProfileCreateFormController', ['$scope',
    function($scope) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        $scope.country = 'US';
    }
]);

profileControllers.controller('BraintreeMerchantAccountUpdateFormController', ['$scope', '$attrs', function($scope, $attrs) {
    $scope.individualMonth = $attrs.individualMonth;
    $scope.individualRegion = $attrs.individualRegion;
    $scope.status = $attrs.status;
    console.log($scope);
    console.log($attrs);
    $scope.isFormValid = function() {
        return $scope.form.$valid;
    };
}]);

profileControllers.controller('RentalAddressUpdateFormController', ['$scope', '$attrs', 'uiGmapGoogleMapApi',
    function($scope, $attrs, uiGmapGoogleMapApi) {
        $scope.rawCountry = $attrs.country;
        $scope.country = $attrs.country;
        $scope.rawStreetAddress = $attrs.streetAddress;
        $scope.rawSubPremise = $attrs.subPremise;
        $scope.rawLocality = $attrs.locality;
        $scope.rawRegion = $attrs.region;
        $scope.rawPostalCode = $attrs.postalCode;
        $scope.doShowMap = false;

        // Country type
        var countryDataSource = [
            {
                "label": "Afghanistan",
                "value": "AF"
            },
            {
                "label": "Albania",
                "value": "AL"
            },
            {
                "label": "Algeria",
                "value": "DZ"
            },
            {
                "label": "American Samoa",
                "value": "AS"
            },
            {
                "label": "Andorra",
                "value": "AD"
            },
            {
                "label": "Angola",
                "value": "AO"
            },
            {
                "label": "Anguilla",
                "value": "AI"
            },
            {
                "label": "Antarctica",
                "value": "AQ"
            },
            {
                "label": "Antigua and Barbuda",
                "value": "AG"
            },
            {
                "label": "Argentina",
                "value": "AR"
            },
            {
                "label": "Armenia",
                "value": "AM"
            },
            {
                "label": "Aruba",
                "value": "AW"
            },
            {
                "label": "Australia",
                "value": "AU"
            },
            {
                "label": "Austria",
                "value": "AT"
            },
            {
                "label": "Azerbaijan",
                "value": "AZ"
            },
            {
                "label": "Bahamas",
                "value": "BS"
            },
            {
                "label": "Bahrain",
                "value": "BH"
            },
            {
                "label": "Bangladesh",
                "value": "BD"
            },
            {
                "label": "Barbados",
                "value": "BB"
            },
            {
                "label": "Belarus",
                "value": "BY"
            },
            {
                "label": "Belgium",
                "value": "BE"
            },
            {
                "label": "Belize",
                "value": "BZ"
            },
            {
                "label": "Benin",
                "value": "BJ"
            },
            {
                "label": "Bermuda",
                "value": "BM"
            },
            {
                "label": "Bhutan",
                "value": "BT"
            },
            {
                "label": "Bolivia",
                "value": "BO"
            },
            {
                "label": "Bonaire",
                "value": "BQ"
            },
            {
                "label": "Bosnia and Herzegovina",
                "value": "BA"
            },
            {
                "label": "Botswana",
                "value": "BW"
            },
            {
                "label": "Bouvet Island",
                "value": "BV"
            },
            {
                "label": "Brazil",
                "value": "BR"
            },
            {
                "label": "British Indian Ocean Territory",
                "value": "IO"
            },
            {
                "label": "Brunei Darussalam",
                "value": "BN"
            },
            {
                "label": "Bulgaria",
                "value": "BG"
            },
            {
                "label": "Burkina Faso",
                "value": "BF"
            },
            {
                "label": "Burundi",
                "value": "BI"
            },
            {
                "label": "Cabo Verde",
                "value": "CV"
            },
            {
                "label": "Cambodia",
                "value": "KH"
            },
            {
                "label": "Cameroon",
                "value": "CM"
            },
            {
                "label": "Canada",
                "value": "CA"
            },
            {
                "label": "Cayman Islands",
                "value": "KY"
            },
            {
                "label": "Central African Republic",
                "value": "CF"
            },
            {
                "label": "Chad",
                "value": "TD"
            },
            {
                "label": "Chile",
                "value": "CL"
            },
            {
                "label": "China",
                "value": "CN"
            },
            {
                "label": "Christmas Island",
                "value": "CX"
            },
            {
                "label": "Cocos (Keeling) Islands",
                "value": "CC"
            },
            {
                "label": "Colombia",
                "value": "CO"
            },
            {
                "label": "Comoros",
                "value": "KM"
            },
            {
                "label": "Congo",
                "value": "CG"
            },
            //{
            //    "label": "Congo (the Democratic Republic of the)",
            //    "value": "CD"
            //},
            {
                "label": "Cook Islands",
                "value": "CK"
            },
            {
                "label": "Costa Rica",
                "value": "CR"
            },
            {
                "label": "Croatia",
                "value": "HR"
            },
            {
                "label": "Cuba",
                "value": "CU"
            },
            {
                "label": "Cura\u00e7ao",
                "value": "CW"
            },
            {
                "label": "Cyprus",
                "value": "CY"
            },
            {
                "label": "Czech Republic",
                "value": "CZ"
            },
            {
                "label": "C\u00f4te d'Ivoire",
                "value": "CI"
            },
            {
                "label": "Denmark",
                "value": "DK"
            },
            {
                "label": "Djibouti",
                "value": "DJ"
            },
            {
                "label": "Dominica",
                "value": "DM"
            },
            {
                "label": "Dominican Republic",
                "value": "DO"
            },
            {
                "label": "Ecuador",
                "value": "EC"
            },
            {
                "label": "Egypt",
                "value": "EG"
            },
            {
                "label": "El Salvador",
                "value": "SV"
            },
            {
                "label": "Equatorial Guinea",
                "value": "GQ"
            },
            {
                "label": "Eritrea",
                "value": "ER"
            },
            {
                "label": "Estonia",
                "value": "EE"
            },
            {
                "label": "Ethiopia",
                "value": "ET"
            },
            {
                "label": "Falkland Islands",
                "value": "FK"
            },
            {
                "label": "Faroe Islands",
                "value": "FO"
            },
            {
                "label": "Fiji",
                "value": "FJ"
            },
            {
                "label": "Finland",
                "value": "FI"
            },
            {
                "label": "France",
                "value": "FR"
            },
            {
                "label": "French Guiana",
                "value": "GF"
            },
            {
                "label": "French Polynesia",
                "value": "PF"
            },
            {
                "label": "French Southern Territories",
                "value": "TF"
            },
            {
                "label": "Gabon",
                "value": "GA"
            },
            {
                "label": "Gambia",
                "value": "GM"
            },
            {
                "label": "Georgia",
                "value": "GE"
            },
            {
                "label": "Germany",
                "value": "DE"
            },
            {
                "label": "Ghana",
                "value": "GH"
            },
            {
                "label": "Gibraltar",
                "value": "GI"
            },
            {
                "label": "Greece",
                "value": "GR"
            },
            {
                "label": "Greenland",
                "value": "GL"
            },
            {
                "label": "Grenada",
                "value": "GD"
            },
            {
                "label": "Guadeloupe",
                "value": "GP"
            },
            {
                "label": "Guam",
                "value": "GU"
            },
            {
                "label": "Guatemala",
                "value": "GT"
            },
            {
                "label": "Guernsey",
                "value": "GG"
            },
            {
                "label": "Guinea",
                "value": "GN"
            },
            {
                "label": "Guinea-Bissau",
                "value": "GW"
            },
            {
                "label": "Guyana",
                "value": "GY"
            },
            {
                "label": "Haiti",
                "value": "HT"
            },
            {
                "label": "Heard Island and McDonald Islands",
                "value": "HM"
            },
            {
                "label": "Holy See",
                "value": "VA"
            },
            {
                "label": "Honduras",
                "value": "HN"
            },
            {
                "label": "Hong Kong",
                "value": "HK"
            },
            {
                "label": "Hungary",
                "value": "HU"
            },
            {
                "label": "Iceland",
                "value": "IS"
            },
            {
                "label": "India",
                "value": "IN"
            },
            {
                "label": "Indonesia",
                "value": "ID"
            },
            {
                "label": "Iran",
                "value": "IR"
            },
            {
                "label": "Iraq",
                "value": "IQ"
            },
            {
                "label": "Ireland",
                "value": "IE"
            },
            {
                "label": "Isle of Man",
                "value": "IM"
            },
            {
                "label": "Israel",
                "value": "IL"
            },
            {
                "label": "Italy",
                "value": "IT"
            },
            {
                "label": "Jamaica",
                "value": "JM"
            },
            {
                "label": "Japan",
                "value": "JP"
            },
            {
                "label": "Jersey",
                "value": "JE"
            },
            {
                "label": "Jordan",
                "value": "JO"
            },
            {
                "label": "Kazakhstan",
                "value": "KZ"
            },
            {
                "label": "Kenya",
                "value": "KE"
            },
            {
                "label": "Kiribati",
                "value": "KI"
            },
            {
                "label": "Kuwait",
                "value": "KW"
            },
            {
                "label": "Kyrgyzstan",
                "value": "KG"
            },
            {
                "label": "Laos",
                "value": "LA"
            },
            {
                "label": "Latvia",
                "value": "LV"
            },
            {
                "label": "Lebanon",
                "value": "LB"
            },
            {
                "label": "Lesotho",
                "value": "LS"
            },
            {
                "label": "Liberia",
                "value": "LR"
            },
            {
                "label": "Libya",
                "value": "LY"
            },
            {
                "label": "Liechtenstein",
                "value": "LI"
            },
            {
                "label": "Lithuania",
                "value": "LT"
            },
            {
                "label": "Luxembourg",
                "value": "LU"
            },
            {
                "label": "Macao",
                "value": "MO"
            },
            {
                "label": "Macedonia",
                "value": "MK"
            },
            {
                "label": "Madagascar",
                "value": "MG"
            },
            {
                "label": "Malawi",
                "value": "MW"
            },
            {
                "label": "Malaysia",
                "value": "MY"
            },
            {
                "label": "Maldives",
                "value": "MV"
            },
            {
                "label": "Mali",
                "value": "ML"
            },
            {
                "label": "Malta",
                "value": "MT"
            },
            {
                "label": "Marshall Islands",
                "value": "MH"
            },
            {
                "label": "Martinique",
                "value": "MQ"
            },
            {
                "label": "Mauritania",
                "value": "MR"
            },
            {
                "label": "Mauritius",
                "value": "MU"
            },
            {
                "label": "Mayotte",
                "value": "YT"
            },
            {
                "label": "Mexico",
                "value": "MX"
            },
            {
                "label": "Micronesia",
                "value": "FM"
            },
            {
                "label": "Moldova",
                "value": "MD"
            },
            {
                "label": "Monaco",
                "value": "MC"
            },
            {
                "label": "Mongolia",
                "value": "MN"
            },
            {
                "label": "Montenegro",
                "value": "ME"
            },
            {
                "label": "Montserrat",
                "value": "MS"
            },
            {
                "label": "Morocco",
                "value": "MA"
            },
            {
                "label": "Mozambique",
                "value": "MZ"
            },
            {
                "label": "Myanmar",
                "value": "MM"
            },
            {
                "label": "Namibia",
                "value": "NA"
            },
            {
                "label": "Nauru",
                "value": "NR"
            },
            {
                "label": "Nepal",
                "value": "NP"
            },
            {
                "label": "Netherlands",
                "value": "NL"
            },
            {
                "label": "New Caledonia",
                "value": "NC"
            },
            {
                "label": "New Zealand",
                "value": "NZ"
            },
            {
                "label": "Nicaragua",
                "value": "NI"
            },
            {
                "label": "Niger",
                "value": "NE"
            },
            {
                "label": "Nigeria",
                "value": "NG"
            },
            {
                "label": "Niue",
                "value": "NU"
            },
            {
                "label": "Norfolk Island",
                "value": "NF"
            },
            {
                "label": "Northern Mariana Islands",
                "value": "MP"
            },
            {
                "label": "North Korea",
                "value": "KP"
            },
            {
                "label": "Norway",
                "value": "NO"
            },
            {
                "label": "Oman",
                "value": "OM"
            },
            {
                "label": "Pakistan",
                "value": "PK"
            },
            {
                "label": "Palau",
                "value": "PW"
            },
            {
                "label": "Palestine",
                "value": "PS"
            },
            {
                "label": "Panama",
                "value": "PA"
            },
            {
                "label": "Papua New Guinea",
                "value": "PG"
            },
            {
                "label": "Paraguay",
                "value": "PY"
            },
            {
                "label": "Peru",
                "value": "PE"
            },
            {
                "label": "Philippines",
                "value": "PH"
            },
            {
                "label": "Pitcairn",
                "value": "PN"
            },
            {
                "label": "Poland",
                "value": "PL"
            },
            {
                "label": "Portugal",
                "value": "PT"
            },
            {
                "label": "Puerto Rico",
                "value": "PR"
            },
            {
                "label": "Qatar",
                "value": "QA"
            },
            {
                "label": "Romania",
                "value": "RO"
            },
            {
                "label": "Russia",
                "value": "RU"
            },
            {
                "label": "Rwanda",
                "value": "RW"
            },
            {
                "label": "R\u00e9union",
                "value": "RE"
            },
            {
                "label": "Saint Barth\u00e9lemy",
                "value": "BL"
            },
            {
                "label": "Saint Helena",
                "value": "SH"
            },
            {
                "label": "Saint Kitts and Nevis",
                "value": "KN"
            },
            {
                "label": "Saint Lucia",
                "value": "LC"
            },
            {
                "label": "Saint Martin",
                "value": "MF"
            },
            {
                "label": "Saint Pierre and Miquelon",
                "value": "PM"
            },
            {
                "label": "Saint Vincent and the Grenadines",
                "value": "VC"
            },
            {
                "label": "Samoa",
                "value": "WS"
            },
            {
                "label": "San Marino",
                "value": "SM"
            },
            {
                "label": "Sao Tome and Principe",
                "value": "ST"
            },
            {
                "label": "Saudi Arabia",
                "value": "SA"
            },
            {
                "label": "Senegal",
                "value": "SN"
            },
            {
                "label": "Serbia",
                "value": "RS"
            },
            {
                "label": "Seychelles",
                "value": "SC"
            },
            {
                "label": "Sierra Leone",
                "value": "SL"
            },
            {
                "label": "Singapore",
                "value": "SG"
            },
            {
                "label": "Sint Maarten",
                "value": "SX"
            },
            {
                "label": "Slovakia",
                "value": "SK"
            },
            {
                "label": "Slovenia",
                "value": "SI"
            },
            {
                "label": "Solomon Islands",
                "value": "SB"
            },
            {
                "label": "Somalia",
                "value": "SO"
            },
            {
                "label": "South Africa",
                "value": "ZA"
            },
            {
                "label": "South Georgia",
                "value": "GS"
            },
            {
                "label": "South Korea",
                "value": "KR"
            },
            {
                "label": "South Sudan",
                "value": "SS"
            },
            {
                "label": "Spain",
                "value": "ES"
            },
            {
                "label": "Sri Lanka",
                "value": "LK"
            },
            {
                "label": "Sudan",
                "value": "SD"
            },
            {
                "label": "Suriname",
                "value": "SR"
            },
            {
                "label": "Svalbard",
                "value": "SJ"
            },
            {
                "label": "Swaziland",
                "value": "SZ"
            },
            {
                "label": "Sweden",
                "value": "SE"
            },
            {
                "label": "Switzerland",
                "value": "CH"
            },
            {
                "label": "Syria",
                "value": "SY"
            },
            {
                "label": "Taiwan",
                "value": "TW"
            },
            {
                "label": "Tajikistan",
                "value": "TJ"
            },
            {
                "label": "Tanzania",
                "value": "TZ"
            },
            {
                "label": "Thailand",
                "value": "TH"
            },
            {
                "label": "Timor-Leste",
                "value": "TL"
            },
            {
                "label": "Togo",
                "value": "TG"
            },
            {
                "label": "Tokelau",
                "value": "TK"
            },
            {
                "label": "Tonga",
                "value": "TO"
            },
            {
                "label": "Trinidad and Tobago",
                "value": "TT"
            },
            {
                "label": "Tunisia",
                "value": "TN"
            },
            {
                "label": "Turkey",
                "value": "TR"
            },
            {
                "label": "Turkmenistan",
                "value": "TM"
            },
            {
                "label": "Turks and Caicos Islands",
                "value": "TC"
            },
            {
                "label": "Tuvalu",
                "value": "TV"
            },
            {
                "label": "Uganda",
                "value": "UG"
            },
            {
                "label": "Ukraine",
                "value": "UA"
            },
            {
                "label": "United Arab Emirates",
                "value": "AE"
            },
            {
                "label": "United Kingdom",
                "value": "GB"
            },
            {
                "label": "United States Minor Outlying Islands",
                "value": "UM"
            },
            {
                "label": "United States",
                "value": "US"
            },
            {
                "label": "Uruguay",
                "value": "UY"
            },
            {
                "label": "Uzbekistan",
                "value": "UZ"
            },
            {
                "label": "Vanuatu",
                "value": "VU"
            },
            {
                "label": "Venezuela",
                "value": "VE"
            },
            {
                "label": "Vietnam",
                "value": "VN"
            },
            {
                "label": "Virgin Islands (British)",
                "value": "VG"
            },
            {
                "label": "Virgin Islands (U.S.)",
                "value": "VI"
            },
            {
                "label": "Wallis and Futuna",
                "value": "WF"
            },
            {
                "label": "Western Sahara",
                "value": "EH"
            },
            {
                "label": "Yemen",
                "value": "YE"
            },
            {
                "label": "Zambia",
                "value": "ZM"
            },
            {
                "label": "Zimbabwe",
                "value": "ZW"
            },
            {
                "label": "\u00c5land Islands",
                "value": "AX"
            }
        ];
        $scope.countryDropDownListOptions = {
            dataSource: countryDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };
        $scope.getCountryLabel = function() {
            if (angular.isDefined($scope.countryDropDownList)) {
                return countryDataSource[$scope.countryDropDownList.selectedIndex].label;
            } else {
                return '';
            }
        };

        $scope.onVerifyButtonClick = function() {
            $scope.updateRaw();
            $scope.doShowMap = true;
            $scope.geocode($scope.raw);
        };

        // Update raw
        $scope.updateRaw = function() {
            var raw = '';
            var optionalProperties = [
                'rawStreetAddress',
                'rawSubPremise',
                'rawLocality',
                'rawRegion',
                'rawPostalCode'
            ];
            angular.forEach(optionalProperties, function(property, key){
                if ($scope[property] && $scope[property].trim()) {
                    raw += $scope[property].trim() + ',';
                }
            });
            raw += $scope.rawCountry;
            $scope.raw = raw;
        };

        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        $scope.isRawFormValid = function() {
            return $scope.rawForm.$valid;
        };

        // Map settings
        $scope.didFailToGeocode = false;
        $scope.map = {
            center: {
                latitude: 0,
                longitude: 0
            },
            options: {
                scrollwheel: false
            },
            zoom: 15,
            control: {},
            events: {
                drag: function(map, eventName, args) {
                    if (!$scope.didFailToGeocode) {
                        return;
                    }
                    var center = map.getCenter();
                    $scope.marker.coords.latitude = center.lat();
                    $scope.marker.coords.longitude = center.lng();
                    $scope.location = 'POINT(' + center.lng() + ' ' + center.lat() + ')';
                },
                dragend: function(map, eventName, args) {
                    if (!$scope.didFailToGeocode) {
                        return;
                    }
                    $scope.map.control.refresh();
                    //var center = map.getCenter();
                    //$scope.marker.coords.latitude = center.lat();
                    //$scope.marker.coords.longitude = center.lng();
                }
            }
        };

        // Marker settings
        $scope.marker = {
            id: 0,
            coords: {
                latitude: 0,
                longitude: 0
            }
        };

        // Third address line...?
        $scope.getThirdAddressLine = function() {
            var addressComponents = [];
            angular.forEach(['locality', 'region', 'postalCode'], function(property, key){
                if ($scope[property] && $scope[property].trim()) {
                    addressComponents.push($scope[property].trim());
                }
            });
            var addressLine = undefined;
            angular.forEach(addressComponents, function(addressComponent, key){
                if (!addressLine) {
                    addressLine = addressComponent;
                } else {
                    addressLine += ', ' + addressComponent;
                }
            });
            return addressLine;
        };

        // Get address component
        function getAddressComponent(type, addressComponents) {
            for (var i = 0; i < addressComponents.length; i++) {
                var addressComponent = addressComponents[i];
                var types = addressComponent.types;
                for (var j = 0; j < types.length; j++) {
                    if (type == types[j]) {
                        return addressComponent;
                    }
                }
            }
            return {
                long_name: '',
                short_name: ''
            };
        }

        // Map API
        uiGmapGoogleMapApi.then(function(maps) {
            var geocoder = new maps.Geocoder();
            $scope.geocode = function(address) {
                $scope.isGeocoding = true;
                geocoder.geocode({address: address}, function(results, status) {
                    var latitude;
                    var longitude;
                    var result;
                    if (status == maps.GeocoderStatus.OK && !results[0].partial_match) {
                        result = results[0];
                        var geometry = result.geometry;
                        latitude = geometry.location.lat();
                        longitude = geometry.location.lng();
                        var addressComponents = result.address_components;

                        // Grab some data
                        $scope.formatted = result.formatted_address;
                        $scope.placeID = result.place_id;

                        // Street
                        var streetNumber = getAddressComponent('street_number', addressComponents).long_name;
                        var route = getAddressComponent('route', addressComponents).long_name;
                        $scope.streetAddress = streetNumber + ' ' + route;
                        $scope.subPremise = getAddressComponent('subpremise', addressComponents).long_name;
                        $scope.postalCode = getAddressComponent('postal_code', addressComponents).long_name;

                        // Country
                        $scope.country = $scope.rawCountry;

                        // Region
                        $scope.region = getAddressComponent('administrative_area_level_1', addressComponents).long_name;
                        $scope.regionCode = getAddressComponent('administrative_area_level_1', addressComponents).short_name;

                        // Locality
                        $scope.locality = getAddressComponent('locality', addressComponents).long_name;

                        // Update address with lat/lon
                        $scope.location = 'POINT(' + longitude + ' ' + latitude + ')';

                        // Update map
                        $scope.marker.coords.latitude = latitude;
                        $scope.marker.coords.longitude = longitude;
                        $scope.map.center.latitude = latitude;
                        $scope.map.center.longitude = longitude;

                        // NO FAIL
                        $scope.didFailToGeocode = false;
                    } else {
                        $scope.didFailToGeocode = true;
                        result = results[0];

                        // Use whatever the user entered...
                        $scope.streetAddress = $scope.rawStreetAddress;
                        $scope.subPremise = $scope.rawSubPremise;
                        $scope.postalCode = $scope.rawPostalCode;
                        $scope.locality = $scope.rawLocality;
                        $scope.region = $scope.rawRegion;

                        if (result && result.partial_match) {
                            latitude = result.geometry.location.lat();
                            longitude = result.geometry.location.lng();

                            // Update address with lat/lon
                            $scope.location = 'POINT(' + longitude + ' ' + latitude + ')'

                            // Update map
                            $scope.marker.coords.latitude = latitude;
                            $scope.marker.coords.longitude = longitude;
                            $scope.map.center.latitude = latitude;
                            $scope.map.center.longitude = longitude;
                            $scope.map.zoom = 15;
                        } else {
                            latitude = 0;
                            longitude = 0;
                            $scope.map.zoom = 3;
                            $scope.map.center.latitude = latitude;
                            $scope.map.center.longitude = longitude;
                            $scope.marker.coords.latitude = latitude;
                            $scope.marker.coords.longitude = longitude;
                            $scope.location = 'POINT(' + longitude + ' ' + latitude + ')'
                        }
                    }
                    $scope.isGeocoding = false;
                    $scope.map.control.refresh();
                    $scope.$apply();
                });
            };
        });
    }
]);

profileControllers.controller('RentalBasicsUpdateFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.numBedrooms = $attrs.numBedrooms;
        $scope.numBeds = $attrs.numBeds;
        $scope.numBathrooms = $attrs.numBathrooms;
        $scope.buildingType = $attrs.buildingType;
        $scope.roomType = $attrs.roomType;
        $scope.maxGuests = $attrs.maxGuests;
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('RentalDescriptionUpdateFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('RentalImageUpdateFormController', ['$scope', '$attrs', 'ImageResource', 'DropzoneAPI',
    function($scope, $attrs, ImageResource, DropzoneAPI) {
        $scope.rentalId = $attrs.id;
        $scope.files = [];
        $scope.didInitialize = false;
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        // Dropzone API listener
        $scope.dropzoneListener = {
            addedfile: function(file) {
                if (!$scope.didInitialize) {
                    return;
                }

                var imageResource = new ImageResource();
                imageResource.image = file.dataURL.split(',')[1];
                imageResource.rental = {
                    id: $scope.rentalId
                };
                imageResource.isPrimary = false;
                imageResource.$save(function(object){
                    file.image = object;
                }, function(httpResponse){
                });
            },
            removedfile: function(file) {
                if (!$scope.didInitialize) {
                    return;
                }
                var imageResource = new ImageResource();
                imageResource.id = file.image.id;
                imageResource.$delete(function(){
                });
            }
        };
        DropzoneAPI.addListener($scope.dropzoneListener);

        $scope.query = function(params) {
            $scope.imageResourcePromise = ImageResource.get(params, function(imageResource) {
                // Dump imagery into files...?
                angular.forEach(imageResource.results, function(value, key){
                    var file = {};
                    file.image = value;
                    file.type = 'image';
                    file.name = file.image.image;
                    file.dataURL = file.image.image;
                    file.isValid = true;
                    DropzoneAPI.dropzone.addFile(file);
                    DropzoneAPI.dropzone.options.thumbnail(file, file.dataURL);
                });
                $scope.didInitialize = true;

            });
        };

        $scope.query({
            rental: $scope.rentalId
        });
    }
]);

profileControllers.controller('BookingStatusFormController', ['$scope', '$attrs', '$timeout', 'bookingFormService',
    function($scope, $attrs, $timeout, bookingFormService) {
        $scope.booking = angular.fromJson($attrs.booking);
        $scope.status = $scope.booking.status;
        $scope.setStatus = function(status) {
            $scope.status = status;
            bookingFormService.booking = $scope.booking;
            bookingFormService.booking.acceptText = (status == 0) ? 'accept':'decline';
            //$timeout(function(){
            //    angular.element('.formset')[0].submit();
            //    console.log(angular.element('.formset'));
            //    console.log("SUBMIT!!!");
            //}, 10);
            //$scope.$parent.form.submit();
            //
        }
    }
]);

profileControllers.controller('BookingStatusFormsetController', ['$scope', '$attrs', 'bookingFormService',
    function($scope, $attrs, bookingFormService) {
        $scope.getBooking = function() {
            return bookingFormService.booking;
        };
        $scope.submit = function() {
            angular.element('.formset')[0].submit();
        };
    }
]);

profileControllers.controller('RentalDetailsUpdateFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('RentalPricingUpdateFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.currency = $attrs.currency;

        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        // Currency type
        var currencyDataSource = [
            {
                label: 'AED',
                value: '0',
                symbol: 'د.إ'
            },
            {
                label: 'ARS',
                value: '1',
                symbol: '$'
            },
            {
                label: 'AUD',
                value: '2',
                symbol: '$'
            },
            {
                label: 'BRL',
                value: '3',
                symbol: 'R$'
            },
            {
                label: 'CAD',
                value: '4',
                symbol: '$'
            },
            {
                label: 'CHF',
                value: '5',
                symbol: 'CHF'
            },
            {
                label: 'CNY',
                value: '6',
                symbol: '¥'
            },
            {
                label: 'CRC',
                value: '7',
                symbol: '₡'
            },
            {
                label: 'CZK',
                value: '8',
                symbol: 'Kč'
            },
            {
                label: 'DKK',
                value: '9',
                symbol: 'kr'
            },
            {
                label: 'EUR',
                value: '10',
                symbol: '€'
            },
            {
                label: 'GBP',
                value: '11',
                symbol: '£'
            },
            {
                label: 'HKD',
                value: '12',
                symbol: '$'
            },
            {
                label: 'HRK',
                value: '13',
                symbol: 'kn'
            },
            {
                label: 'HUF',
                value: '14',
                symbol: 'Ft'
            },
            {
                label: 'IDR',
                value: '15',
                symbol: 'Rp'
            },
            {
                label: 'ILS',
                value: '16',
                symbol: '₪'
            },
            {
                label: 'INR',
                value: '17',
                symbol: '₹'
            },
            {
                label: 'JPY',
                value: '18',
                symbol: '¥'
            },
            {
                label: 'KRW',
                value: '19',
                symbol: '₩'
            },
            {
                label: 'MAD',
                value: '20',
                symbol: 'MAD'
            },
            {
                label: 'MXN',
                value: '21',
                symbol: '$'
            },
            {
                label: 'MYR',
                value: '22',
                symbol: 'RM'
            },
            {
                label: 'NOK',
                value: '23',
                symbol: 'kr'
            },
            {
                label: 'NZD',
                value: '24',
                symbol: '$'
            },
            {
                label: 'PEN',
                value: '25',
                symbol: 'S/.'
            },
            {
                label: 'PHP',
                value: '26',
                symbol: '₱'
            },
            {
                label: 'PLN',
                value: '27',
                symbol: 'zł'
            },
            {
                label: 'RON',
                value: '28',
                symbol: 'lei'
            },
            {
                label: 'RUB',
                value: '29',
                symbol: 'р'
            },
            {
                label: 'SEK',
                value: '30',
                symbol: 'kr'
            },
            {
                label: 'SGD',
                value: '31',
                symbol: '$'
            },
            {
                label: 'THB',
                value: '32',
                symbol: '฿'
            },
            {
                label: 'TRY',
                value: '33',
                symbol: 'TL'
            },
            {
                label: 'TWD',
                value: '34',
                symbol: '$'
            },
            {
                label: 'USD',
                value: '35',
                symbol: '$'
            },
            {
                label: 'VND',
                value: '36',
                symbol: '₫'
            },
            {
                label: 'ZAR',
                value: '37',
                symbol: '₫'
            }
        ];
        $scope.getCurrencySymbol = function(idx) {
            return currencyDataSource[idx].symbol;
        };
    }
]);

profileControllers.controller('RentalAmenitiesUpdateFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        if ($attrs.amenities != '') {
            $scope.amenity_values = JSON.parse($attrs.amenities);
        } else {
            $scope.amenity_values = [];
        }
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        $scope.$watch('amenity_values', function(newValue){
            $scope.amenities = '';
            angular.forEach(newValue, function(value, key){
                $scope.amenities += value + ',';
            });
            $scope.amenities = $scope.amenities.replace(/(^,)|(,$)/g, "");
        }, true);

        $scope.commonAmenities = [
            {
                label: 'Essentials',
                value: 0,
                tooltip: 'Towels, bed sheets, soap, and toilet paper'
            },
            {
                label: 'TV',
                value: 1
            },
            {
                label: 'Cable TV',
                value: 2
            },
            {
                label: 'Air Conditioning',
                value: 3
            },
            {
                label: 'Heating',
                value: 4,
                tooltip: 'Central heating or heater at your pad'
            },
            {
                label: 'Kitchen',
                value: 5,
                tooltip: 'Space where guests can cook their own meals'
            },
            {
                label: 'Internet',
                value: 6,
                tooltip: 'Internet (wired or wireless)'
            },
            {
                label: 'Wireless Internet',
                value: 7,
                tooltip: 'Continuous access to WiFi'
            }
        ];

        $scope.additionalAmenities = [
            {
                label: 'Hot Tub',
                value: 8
            },
            {
                label: 'Washer',
                value: 9,
                tooltip: 'In the building, free, or for a fee'
            },
            {
                label: 'Pool',
                value: 10,
                tooltip: 'Private or shared'
            },
            {
                label: 'Dryer',
                value: 11,
                tooltip: 'In the building, free, or for a fee'
            },
            {
                label: 'Breakfast',
                value: 12,
                tooltip: 'Breakfast is provided'
            },
            {
                label: 'Free Parking on Premises',
                value: 13
            },
            {
                label: 'Gym',
                value: 14
            },
            {
                label: 'Elevator in Building',
                value: 15
            },
            {
                label: 'Indoor Fireplace',
                value: 16
            },
            {
                label: 'Buzzer/Wireless Intercom',
                value: 17
            },
            {
                label: 'Doorman',
                value: 18
            },
            {
                label: 'Shampoo',
                value: 19
            }
        ];

        $scope.specialFeatures = [
            {
                label: 'Family/Kid Friendly',
                value: 20,
                tooltip: 'The property is suitable for hosting families with children'
            },
            {
                label: 'Smoking Allowed',
                value: 21
            },
            {
                label: 'Suitable for Events',
                value: 22,
                tooltip: 'The listing can accommodate a gathering of 25 or more attendees'
            },
            {
                label: 'Pets Allowed',
                value: 23
            },
            {
                label: 'Pets live on this property',
                value: 24
            },
            {
                label: 'Wheelchair Accessible',
                value: 25
            }
        ];

        $scope.homeSafety = [
            {
                label: 'Smoke Detector',
                value: 26,
                tooltip: 'There is a functioning smoke detector in the listing'
            },
            {
                label: 'Carbon Monoxide Detector',
                value: 27,
                tooltip: 'There is a functioning carbon monoxide detector in the listing'
            },
            {
                label: 'First Aid Kit',
                value: 28
            },
            {
                label: 'Safety Card',
                value: 29,
                tooltip: 'Posted emergency information and resources'
            },
            {
                label: 'Fire Extinguisher',
                value: 30
            }
        ];

        $scope.toggleAmenity = function(amenity) {
            var idx = $scope.amenity_values.indexOf(amenity.value);
            if (idx > -1) {
                $scope.amenity_values.splice(idx, 1);
            } else {
                $scope.amenity_values.push(amenity.value);
            }
        };

        $scope.containsAmenity = function (amenity) {
            return $scope.amenity_values.indexOf(amenity.value) > -1;
        };
    }
]);


//profileControllers.controller('RentalEditFormController', ['$scope', '$attrs',
//    function($scope, $attrs) {
//        console.log($attrs.amenities);
//        $scope.amenity_values = JSON.parse($attrs.amenities);
//        $scope.numBedrooms = $attrs.numBedrooms;
//        $scope.numBeds = $attrs.numBeds;
//        $scope.numBathrooms = $attrs.numBathrooms;
//        $scope.buildingType = $attrs.buildingType;
//        $scope.roomType = $attrs.roomType;
//        $scope.maxGuests = $attrs.maxGuests;
//        $scope.isFormValid = function() {
//            return $scope.form.$valid;
//        };
//
//        $scope.commonAmenities = [
//            {
//                label: 'Essentials',
//                value: 0,
//                tooltip: 'Towels, bed sheets, soap, and toilet paper'
//            },
//            {
//                label: 'TV',
//                value: 1
//            },
//            {
//                label: 'Cable TV',
//                value: 2
//            },
//            {
//                label: 'Air Conditioning',
//                value: 3
//            },
//            {
//                label: 'Heating',
//                value: 4,
//                tooltip: 'Central heating or heater at your pad'
//            },
//            {
//                label: 'Kitchen',
//                value: 5,
//                tooltip: 'Space where guests can cook their own meals'
//            },
//            {
//                label: 'Internet',
//                value: 6,
//                tooltip: 'Internet (wired or wireless)'
//            },
//            {
//                label: 'Wireless Internet',
//                value: 7,
//                tooltip: 'Continuous access to WiFi'
//            }
//        ];
//
//        $scope.additionalAmenities = [
//            {
//                label: 'Hot Tub',
//                value: 8
//            },
//            {
//                label: 'Washer',
//                value: 9,
//                tooltip: 'In the building, free, or for a fee'
//            },
//            {
//                label: 'Pool',
//                value: 10,
//                tooltip: 'Private or shared'
//            },
//            {
//                label: 'Dryer',
//                value: 11,
//                tooltip: 'In the building, free, or for a fee'
//            },
//            {
//                label: 'Breakfast',
//                value: 12,
//                tooltip: 'Breakfast is provided'
//            },
//            {
//                label: 'Free Parking on Premises',
//                value: 13
//            },
//            {
//                label: 'Gym',
//                value: 14
//            },
//            {
//                label: 'Elevator in Building',
//                value: 15
//            },
//            {
//                label: 'Indoor Fireplace',
//                value: 16
//            },
//            {
//                label: 'Buzzer/Wireless Intercom',
//                value: 17
//            },
//            {
//                label: 'Doorman',
//                value: 18
//            },
//            {
//                label: 'Shampoo',
//                value: 19
//            }
//        ];
//
//        $scope.specialFeatures = [
//            {
//                label: 'Family/Kid Friendly',
//                value: 20,
//                tooltip: 'The property is suitable for hosting families with children'
//            },
//            {
//                label: 'Smoking Allowed',
//                value: 21
//            },
//            {
//                label: 'Suitable for Events',
//                value: 22,
//                tooltip: 'The listing can accommodate a gathering of 25 or more attendees'
//            },
//            {
//                label: 'Pets Allowed',
//                value: 23
//            },
//            {
//                label: 'Pets live on this property',
//                value: 24
//            },
//            {
//                label: 'Wheelchair Accessible',
//                value: 25
//            }
//        ];
//
//        $scope.homeSafety = [
//            {
//                label: 'Smoke Detector',
//                value: 26,
//                tooltip: 'There is a functioning smoke detector in the listing'
//            },
//            {
//                label: 'Carbon Monoxide Detector',
//                value: 27,
//                tooltip: 'There is a functioning carbon monoxide detector in the listing'
//            },
//            {
//                label: 'First Aid Kit',
//                value: 28
//            },
//            {
//                label: 'Safety Card',
//                value: 29,
//                tooltip: 'Posted emergency information and resources'
//            },
//            {
//                label: 'Fire Extinguisher',
//                value: 30
//            }
//        ];
//
//        $scope.toggleAmenity = function(amenity) {
//            var idx = $scope.amenity_values.indexOf(amenity.value);
//            if (idx > -1) {
//                $scope.amenity_values.splice(idx, 1);
//            } else {
//                $scope.amenity_values.push(amenity.value);
//            }
//        };
//
//        $scope.containsAmenity = function (amenity) {
//            return $scope.amenity_values.indexOf(amenity.value) > -1;
//        };
//    }
//]);

profileControllers.controller('ProfileEditFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.gender = $attrs.profileGender;
        $scope.month = $attrs.profileMonth;
        $scope.country = $attrs.profileCountry;
        $scope.referral = $attrs.profileReferral;
        $scope.type = $attrs.profileType;

        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('PasswordResetFormController', ['$scope',
    function($scope) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('SetPasswordFormController', ['$scope',
    function($scope) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

profileControllers.controller('ProfileImageUploadController', ['$scope',
    function($scope) {
        $scope.image = undefined;
        $scope.croppedImage = undefined;
        $scope.croppedImageData = undefined;
        $scope.didUploadImage = false;

        $scope.$watch('croppedImage', function(newValue){
            if (!newValue) {
                return;
            }
            var toks = newValue.split(',');
            if (toks.length < 2) {
                return;
            }
            $scope.croppedImageData = toks[1];
        });

        $scope.isFormValid = function() {
            return $scope.didUploadImage;
        };

        $scope.onFileInputClick = function() {
            angular.element(document.querySelector('#file-input')).click();
        };

        function onFileReaderLoad() {
            return function (event) {
                $scope.$apply(function ($scope) {
                    $scope.croppedImage = null;
                    $scope.croppedImageData = null;
                    $scope.image = event.target.result;
                    $scope.didUploadImage = true;
                });
            };
        }
        angular.element(document.querySelector('#file-input')).on('change', function(event) {
            var file = event.currentTarget.files[0];
            var fileReader = new FileReader();
            fileReader.onload = onFileReaderLoad();
            fileReader.readAsDataURL(file);
        });

        var cropArea = document.querySelector('#crop-area');
        cropArea.addEventListener('dragover', function(event) {
            event.stopPropagation();
            event.preventDefault();
            event.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
        }, false);
        cropArea.addEventListener('drop', function(event) {
            event.stopPropagation();
            event.preventDefault();
            var files = event.dataTransfer.files; // FileList object.
            if (!files || files.length == 0) {
                return;
            }
            var file = files[0];
            var fileReader = new FileReader();
            fileReader.onload = onFileReaderLoad();
            fileReader.readAsDataURL(file);
        }, false);
    }
]);
