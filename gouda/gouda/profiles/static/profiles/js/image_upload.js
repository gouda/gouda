$( document ).ready(function() {
    $("#image-cropper").cropit({
        onImageLoading: function() {
            $(".image-select-controls").css("display", "none");
            $(".image-loading").css("visibility", "visible");
            $("#cropit-image-preview").css("background-image", "none");
        },
        onImageLoaded: function() {
            $(".image-select-controls").css("display", "none");
            $(".image-loading").css("display", "none");
            $(".btn-reselect-image").css("display", "inherit");
            $("#cropit-image-preview").removeClass("image-select").unbind("click");
        }
    });
    $(".image-select").click(function() {
        $(".cropit-image-input").click();
        return false;
    });
    $("form").submit(function() {
        var image = $("#image-cropper").cropit("export", {type: "image/png"});
        if (image != null) {
            $("#id_image").val(image);
            return true;
        } else {
            return false;
        }

    });
    $('#cropit-image-input').attr("accept", "image/jpg, image/png");
});
