exports.config = {
    allScriptsTimeout: 30000,

    specs: [
        'e2e/signup.js',
        'e2e/scenarios.js',
    ],

    capabilities: {
        'browserName': 'chrome'
    },

    chromeOnly: true,

    baseUrl: 'http://localhost:8000/',

    framework: 'jasmine',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    }
};