from django.conf.urls import url
from django.contrib.auth.views import logout
from django.contrib.auth.views import password_reset_complete
from django.contrib.auth.views import password_reset_done
from django.core.urlresolvers import reverse_lazy
from django.views.decorators.cache import cache_page, never_cache

from gouda.profiles.forms import LoginForm
from gouda.profiles.forms import PasswordResetForm
from gouda.profiles.forms import SetPasswordForm
from gouda.profiles.views import AccountEditView
from gouda.profiles.views import BookingAlbumDownloadView
from gouda.profiles.views import BookingCsvDownloadView
from gouda.profiles.views import BookingDetailView
from gouda.profiles.views import BookingListView
from gouda.profiles.views import BookingRequestDetailView
from gouda.profiles.views import BookingRequestListView
from gouda.profiles.views import BraintreeMerchantAccountUpdateView
from gouda.profiles.views import CustomerUpdateView
from gouda.profiles.views import ImageUpdateView
from gouda.profiles.views import InboxView
from gouda.profiles.views import PayoutListView
from gouda.profiles.views import ProfileCreateView
from gouda.profiles.views import ProfileUpdateView
from gouda.profiles.views import ShootDetailView
from gouda.profiles.views import ShootListView
from gouda.profiles.views import ThreadView
from gouda.profiles.views import VerifyEmailAddressView
from gouda.profiles.views import password_reset_confirm
from gouda.users.views import login
from gouda.users.views import password_reset

urls = list()
urls.append(url(
    r'^signup/$',
    never_cache(ProfileCreateView.as_view()),
    name='create')
)
urls.append(url(
    r'^password-reset/$',
    password_reset,
    {
        'password_reset_form': PasswordResetForm,
        'template_name': 'profiles/password_reset_form.html',
        'email_template_name': 'profiles/emails/password_reset_body.txt',
        'subject_template_name': 'profiles/emails/password_reset_subject.txt',
        'post_reset_redirect': reverse_lazy('profiles:password_reset_done')
    },
    name='password_reset'
))
urls.append(url(
    r'^password-reset/done/$',
    password_reset_done,
    {
        'template_name': 'profiles/password_reset_done.html'
    },
    name='password_reset_done'
))
urls.append(url(
    r'^verify/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    VerifyEmailAddressView.as_view(),
    name='verify_email_address'
))

urls.append(url(
    r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
    password_reset_confirm,
    {
        'set_password_form': SetPasswordForm,
        'template_name': 'profiles/password_reset_confirm.html',
        'post_reset_redirect': reverse_lazy('index')
    },
    name='password_reset_confirm'
))
urls.append(url(
    r'^reset/done/$',
    password_reset_complete,
    {
        'template_name': 'profiles/password_reset_complete.html'
    },
    name='password_reset_complete'
))
urls.append(url(
    r'^login/$',
    login,
    {
        'template_name': 'profiles/login.html',
        'authentication_form': LoginForm
    },
    name='login'
))
urls.append(url(
    r'^logout/$',
    logout,
    {'next_page': reverse_lazy('index')},
    name='logout'
))
urls.append(url(
    r'^(?P<pk>[0-9]+)/upload/$',
    never_cache(ImageUpdateView.as_view()),
    name='image_update'
))
urls.append(url(
    r'^inbox/$',
    never_cache(InboxView.as_view()),
    name='inbox'
))
urls.append(url(
    r'^inbox/(?P<pk>[0-9]+)/$',
    never_cache(ThreadView.as_view()),
    name='thread'
))
urls.append(url(
    r'^manage-account/payout/$',
    never_cache(BraintreeMerchantAccountUpdateView.as_view()),
    name='braintree_merchant_account_update'
))
urls.append(url(
    r'^manage-account/payout/$',
    never_cache(BraintreeMerchantAccountUpdateView.as_view()),
    name='account_update'
))
urls.append(url(
    r'^edit/$',
    never_cache(ProfileUpdateView.as_view()),
    name='update_profile'
))
urls.append(url(
    r'^account/$',
    never_cache(AccountEditView.as_view()),
    name='edit_account'
))
urls.append(url(
    r'^bookings/$',
    never_cache(BookingListView.as_view()),
    name='booking_list'
))
urls.append(url(
    r'^bookings/(?P<pk>[0-9]+)$',
    never_cache(BookingDetailView.as_view()),
    name='booking_detail'
))
urls.append(url(
    r'^bookings/download/(?P<pk>[0-9]+)$',
    never_cache(BookingAlbumDownloadView.as_view()),
    name='booking_album_download'
))
urls.append(url(
    r'^bookings/csv/$',
    never_cache(BookingCsvDownloadView.as_view()),
    name='booking_csv_download'
))
urls.append(url(
    r'^payouts/$',
    never_cache(PayoutListView.as_view()),
    name='payout_list'
))
urls.append(url(
    r'^shoots/$',
    never_cache(ShootListView.as_view()),
    name='shoot_list'
))
urls.append(url(
    r'^shoots/(?P<pk>[0-9]+)$',
    never_cache(ShootDetailView.as_view()),
    name='shoot_detail'
))
urls.append(url(
    r'^requests/(?P<pk>[0-9]+)$',
    never_cache(BookingRequestDetailView.as_view()),
    name='booking_request_detail'
))
urls.append(url(
    r'^requests/$',
    never_cache(BookingRequestListView.as_view()),
    name='booking_request_list'
))
urls.append(url(
    r'^payment/$',
    never_cache(CustomerUpdateView.as_view()),
    name='customer_update'
))

urlpatterns = urls

