from __future__ import absolute_import

from datetime import date
from datetime import timedelta

import re
from django.core import mail
from django import test
from gouda.profiles.forms import MessageForm
from gouda.profiles.forms import ProfileForm
from gouda.profiles.forms import ProfileEditForm
from gouda.profiles.forms import ImageUpdateForm
from gouda.profiles.forms import PasswordResetForm
from gouda.profiles.forms import SetPasswordForm
from django.core.urlresolvers import reverse
from gouda.profiles.models import Profile
from gouda.profiles.models import ProfileType
from gouda.profiles.models import ReferralType
from gouda.profiles.models import GenderType
from gouda.profiles.models import Thread
from gouda.profiles.models import Message
from gouda.users.models import User
from gouda.profiles.utils import ProfileDataMixin
from gouda.profiles.utils import ProfileImageUploadMixin
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from django.conf import settings
from unittest import skip

STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')


class MessageCreateFormTestCase(test.TestCase):
    def setUp(self):
        super(MessageCreateFormTestCase, self).setUp()
        create_profiles(2)
        self.thread = Thread.objects.create()
        self.sender = Profile.objects.all()[0]
        self.receiver = Profile.objects.all()[1]
        self.initial = {
            'sender': self.sender.id,
            'receiver': self.receiver.id,
            'thread': self.thread.id
        }
        self.data = {
            'body': 'LANNNNAAAAAAA!!!!!!!!',
            'sender': self.sender.id,
            'receiver': self.receiver.id,
            'thread': self.thread.id
        }
        mail.outbox = []

    def tearDown(self):
        super(MessageCreateFormTestCase, self).tearDown()
        tear_down_profiles()

    def test_init(self):
        MessageForm()

    def test_create_message(self):
        self.assertTrue(Message.objects.all().count() == 0)
        message_create_form = MessageForm(data=self.data, initial=self.initial)
        self.assertTrue(message_create_form.is_valid())
        message_create_form.save()
        self.assertTrue(Message.objects.all().count() == 1)

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1, 'Failed to receive email about message being sent')

    def test_create_empty_message(self):
        self.assertTrue(Message.objects.all().count() == 0)
        data = self.data.copy()
        data['body'] = ''
        message_create_form = MessageForm(data=data, initial=self.initial)
        self.assertFalse(message_create_form.is_valid())
        self.assertTrue(Message.objects.all().count() == 0)


class ProfileEditFormTestCase(ProfileImageUploadMixin, ProfileDataMixin, test.TestCase):
    def setUp(self):
        super(ProfileEditFormTestCase, self).setUp()
        self.profile = create_profiles(1)[0]
        self.initial = {
            'day': int(self.profile.date_of_birth.strftime('%d')),
            'month': int(self.profile.date_of_birth.strftime('%m')),
            'year': int(self.profile.date_of_birth.strftime('%Y')),

            'full_name': self.profile.full_name,
            'gender': self.profile.gender,

            'referral': self.profile.referral,

            'locality': self.profile.locality,
            'region': self.profile.region,
            'country': self.profile.country
        }

    def tearDown(self):
        super(ProfileEditFormTestCase, self).tearDown()
        tear_down_profiles()

    def test_edit_date_of_birth(self):
        data = self.initial.copy()
        data['day'] = 2
        data['month'] = 2
        data['year'] = 1972
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(int(profile.date_of_birth.strftime('%d')), data.get('day'))
        self.assertEqual(int(profile.date_of_birth.strftime('%m')), data.get('month'))
        self.assertEqual(int(profile.date_of_birth.strftime('%Y')), data.get('year'))

    def test_edit_full_name(self):
        data = self.initial.copy()
        data['full_name'] = 'Duchess'
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(profile.full_name, data.get('full_name'))

    def test_edit_referral(self):
        data = self.initial.copy()
        data['referral'] = ReferralType.OTHER
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(profile.referral, data.get('referral'))

    def test_edit_gender(self):
        data = self.initial.copy()
        data['gender'] = GenderType.OTHER
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        profile_edit_form.is_valid()
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(profile.gender, data.get('gender'))

    def test_edit_locality(self):
        data = self.initial.copy()
        data['locality'] = 'Bakersfield'
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(profile.locality, data.get('locality'))

    def test_edit_region(self):
        data = self.initial.copy()
        data['region'] = 'California'
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(profile.region, data.get('region'))
        self.assertNotEqual(profile.region, self.initial.get('region'))

    def test_edit_country(self):
        data = self.initial.copy()
        data['country'] = 'JM'
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertEqual(profile.country, data.get('country'))

    def test_edit_image(self):
        profile_image = self.profile.image
        data = self.initial.copy()
        data['image'] = self.encode_image('archer.jpg')
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertTrue(profile_edit_form.is_valid())
        profile = profile_edit_form.save()
        self.assertNotEqual(profile.image, profile_image)

    def test_user_is_too_young(self):
        data = self.initial.copy()
        data['day'] = int(date.today().strftime('%d'))
        data['month'] = int(date.today().strftime('%m'))
        data['year'] = int(date.today().strftime('%Y'))
        profile_edit_form = ProfileEditForm(data=data, initial=self.initial, instance=self.profile)
        self.assertFalse(profile_edit_form.is_valid())


class ProfileFormTestCase(ProfileDataMixin, test.TestCase):
    def setUp(self):
        self.request_factory = test.RequestFactory()
        self.request = self.request_factory.get(reverse('profiles:create'))

    def test_init(self):
        ProfileForm()

    # def test_verification_email_gets_sent(self):
    #     create_form = ProfileForm(self.archer_profile_form_data)
    #     create_form.save()
    #
    #     # Make sure email gets sent
    #     self.assertEqual(len(mail.outbox), 1, 'Failed to receive email with verification link')
    #
    #     # Check sender
    #     self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')
    #
    #     # # Check for reset link
    #     # match_object = re.compile(r"(https://[\S]+)").search(mail.outbox[0].body)
    #     # url = match_object.groups(1)[0]
    #     # self.assertIsNotNone(url, 'Failed to find reset link in email')

    def test_valid_data(self):
        create_form = ProfileForm(self.archer_profile_form_data)
        self.assertTrue(create_form.is_valid())
        profile = create_form.save()
        user = profile.user
        self.assertEqual(user.full_name, self.archer_profile_form_data['full_name'])
        self.assertEqual(user.email_address, self.archer_profile_form_data['email_address'])
        self.assertNotEqual(user.password, self.archer_profile_form_data['password'])
        self.assertEqual(profile.gender, self.archer_profile_form_data['gender'])
        self.assertEqual(profile.country, self.archer_profile_form_data['country'])
        self.assertEqual(profile.referral, self.archer_profile_form_data['referral'])
        self.assertEqual(profile.locality, self.archer_profile_form_data['locality'])
        self.assertEqual(profile.region, self.archer_profile_form_data['region'])
        date_of_birth = date(self.archer_profile_form_data['year'], self.archer_profile_form_data['month'], self.archer_profile_form_data['day'])
        self.assertEqual(profile.date_of_birth, date_of_birth)

    def test_blank_data(self):
        create_form = ProfileForm({})
        self.assertFalse(create_form.is_valid())
        self.assertIn('full_name', create_form.errors)
        self.assertIn('email_address', create_form.errors)
        self.assertIn('password', create_form.errors)
        self.assertIn('month', create_form.errors)
        self.assertIn('day', create_form.errors)
        self.assertIn('year', create_form.errors)
        self.assertIn('gender', create_form.errors)
        self.assertIn('locality', create_form.errors)
        self.assertIn('region', create_form.errors)
        self.assertIn('country', create_form.errors)
        self.assertIn('referral', create_form.errors)

    def test_blank_gender(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['gender'] = ''
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_invalid_gender(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['gender'] = 'phrasing'
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_blank_date_of_birth(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['month'] = ''
        profile_form_data['day'] = None
        profile_form_data['year'] = None
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_invalid_month(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['month'] = 13
        profile_form_data['day'] = 30
        profile_form_data['year'] = 1986
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_invalid_day(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['month'] = 2
        profile_form_data['day'] = 30
        profile_form_data['year'] = 1986
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_invalid_year(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['month'] = 8
        profile_form_data['day'] = 23
        profile_form_data['year'] = (date.today() + timedelta(days=365)).year
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_too_young(self):
        date_of_birth = date.today() - timedelta(days=365 * 17)
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['month'] = date_of_birth.month
        profile_form_data['day'] = date_of_birth.day
        profile_form_data['year'] = date_of_birth.year
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_blank_password(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['password'] = ''
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_blank_country(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['country'] = None
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_blank_referral(self):
        profile_form_data = self.archer_profile_form_data.copy()
        profile_form_data['referral'] = None
        create_form = ProfileForm(profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_duplicate_email_address(self):
        create_form = ProfileForm(self.archer_profile_form_data)
        self.assertTrue(create_form.is_valid())
        archer_profile = create_form.save()

        lana_profile_form_data = self.lana_profile_form_data.copy()
        lana_profile_form_data['email_address'] = self.archer_profile_form_data['email_address']
        create_form = ProfileForm(lana_profile_form_data)
        self.assertFalse(create_form.is_valid())

    def test_long_full_name(self):
        archer_profile_form_data = self.archer_profile_form_data.copy()
        archer_profile_form_data['full_name'] = 'ARCHER'.join(['STERLING' for i in range(100000)])
        create_form = ProfileForm(archer_profile_form_data)
        self.assertFalse(create_form.is_valid())


class ImageUpdateFormTestCase(ProfileDataMixin, ProfileImageUploadMixin, test.TestCase):
    def setUp(self):
        super(ImageUpdateFormTestCase, self).setUp()
        user_data = self.get_user_data(self.archer_profile_form_data)
        self.archer_user = User.objects.create_user(**user_data)

        profile_data = self.get_profile_data(self.archer_profile_form_data)
        self.archer_profile = Profile.objects.create(user=self.archer_user, **profile_data)

    def tearDown(self):
        super(ImageUpdateFormTestCase, self).tearDown()
        tear_down_profiles()

    def test_init(self):
        self.assertIsNotNone(ImageUpdateForm(instance=self.archer_profile))

    def test_jpeg_image(self):
        self.assertFalse(bool(self.archer_profile.image))
        profile_image_update_form = ImageUpdateForm({'image': self.encode_image('archer.jpg')}, instance=self.archer_profile)
        self.assertTrue(profile_image_update_form.is_valid())
        profile = profile_image_update_form.save()
        self.assertTrue(bool(profile.image))

    def test_png_image(self):
        self.assertFalse(bool(self.archer_profile.image))
        profile_image_update_form = ImageUpdateForm({'image': self.encode_image('archer.png')}, instance=self.archer_profile)
        profile_image_update_form.is_valid()
        self.assertTrue(profile_image_update_form.is_valid())
        profile = profile_image_update_form.save()
        self.assertTrue(bool(profile.image))

    def test_gif_image(self):
        self.assertFalse(bool(self.archer_profile.image))
        profile_image_update_form = ImageUpdateForm({'image': self.encode_image('archer.gif')}, instance=self.archer_profile)
        self.assertFalse(profile_image_update_form.is_valid())

    @skip('Need to get a larger image')
    def test_large_image(self):
        profile_image_update_form = ImageUpdateForm({'image': self.encode_image('archer_large.png')}, instance=self.archer_profile)
        self.assertFalse(profile_image_update_form.is_valid())


class PasswordResetFormTestCase(ProfileDataMixin, test.TestCase):
    def setUp(self):
        user_data = self.get_user_data(self.archer_profile_form_data)
        self.archer_user = User.objects.create_user(**user_data)

        profile_data = self.get_profile_data(self.archer_profile_form_data)
        self.archer_profile = Profile.objects.create(user=self.archer_user, **profile_data)
        mail.outbox = []

    def tearDown(self):
        super(PasswordResetFormTestCase, self).tearDown()
        tear_down_profiles()

    def test_init(self):
        PasswordResetForm()

    def test_password_reset(self):
        password_reset_form = PasswordResetForm({'email_address': self.archer_profile_form_data['email_address']})

        # Check form
        self.assertTrue(password_reset_form.is_valid())

        # Make sure email gets sent
        password_reset_form.save(domain_override='www.shotzu.com', use_https=True)
        self.assertEqual(len(mail.outbox), 1, 'Failed to receive email with password reset link')

        # Check sender
        self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')

        # Check bcc
        self.assertEqual(mail.outbox[0].bcc, STAFF_EMAILS)

        # # Check for reset link
        # match_object = re.compile(r"(http://[\S]+)").search(mail.outbox[0].body)
        # url = match_object.groups(1)[0]
        # self.assertIsNotNone(url, 'Failed to find reset link in email')

    def test_password_reset_with_nonexistent_user(self):
        password_reset_form = PasswordResetForm({'email_address': 'krieger123@gmail.com'})
        self.assertTrue(password_reset_form.is_valid())

        password_reset_form.save()
        self.assertEqual(len(mail.outbox), 0)

    # def test_password_reset_when_user_has_no_password(self):
    #     self.archer_user.password = ''
    #     self.archer_user.save()
    #     password_reset_form = PasswordResetForm({'email_address': self.archer_profile_form_data['email_address']})
    #
    #     # Check form
    #     self.assertTrue(password_reset_form.is_valid())
    #
    #     # Make sure email gets sent
    #     password_reset_form.save(domain_override='www.shotzu.com', use_https=True)
    #     self.assertEqual(len(mail.outbox), 1, 'Failed to receive email with password reset link')
    #
    #     # Check sender
    #     self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')


class SetPasswordFormTestCase(ProfileDataMixin, test.TestCase):
    def setUp(self):
        user_data = self.get_user_data(self.archer_profile_form_data)
        self.archer_user = User.objects.create_user(**user_data)

        profile_data = self.get_profile_data(self.archer_profile_form_data)
        self.archer_profile = Profile.objects.create(user=self.archer_user, **profile_data)

    def test_init(self):
        SetPasswordForm(self.archer_user)

    def test_set_password(self):
        set_password_form = SetPasswordForm(self.archer_user, {
            'new_password1': self.archer_profile_form_data['password'],
            'new_password2': self.archer_profile_form_data['password']
        })
        self.assertTrue(set_password_form.is_valid())

    def test_set_invalid_password(self):
        set_password_form = SetPasswordForm(self.archer_user, {
            'new_password1': 'LANA!',
            'new_password2': 'LANA!'
        })
        self.assertFalse(set_password_form.is_valid())
