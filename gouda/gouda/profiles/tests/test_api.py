from __future__ import absolute_import

from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from gouda.profiles.models import Profile
from gouda.profiles.models import Message
from gouda.profiles.models import Thread
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from unittest import skip


@skip('Not needed...')
class MessageCreateAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(MessageCreateAPITestCase, cls).setUpClass()
        create_profiles()

    @classmethod
    def tearDownClass(cls):
        super(MessageCreateAPITestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.profile = Profile.objects.get(user__email_address='success+sterlingarcher@simulator.amazonses.com')
        self.client.login(username='success+sterlingarcher@simulator.amazonses.com', password='sterlingarcher123')
        self.message_data = {
            'body': 'LANA!!!!!!',
            'receiver': {
                'id': Profile.objects.get(user__email_address='success+lanakane@simulator.amazonses.com').pk
            }
        }
        self.assertEqual(Thread.objects.all().count(), 0)
        self.assertEqual(Message.objects.all().count(), 0)

    def test_create_message(self):
        url = reverse('api:message_list')
        message_data = self.message_data.copy()
        response = self.client.post(url, message_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Thread.objects.all().count(), 1)
        self.assertEqual(Message.objects.all().count(), 1)

    def test_create_message_for_invalid_profile(self):
        url = reverse('api:message_list')
        message_data = self.message_data.copy()
        message_data['receiver'] = {
            'id': 999999999
        }
        response = self.client.post(url, message_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(Thread.objects.all().count(), 0)
        self.assertEqual(Message.objects.all().count(), 0)

    def test_create_message_if_logged_out(self):
        self.client.logout()
        url = reverse('api:message_list')
        message_data = self.message_data.copy()
        response = self.client.post(url, message_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        self.assertEqual(Thread.objects.all().count(), 0)
        self.assertEqual(Message.objects.all().count(), 0)
