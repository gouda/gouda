from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import random
from datetime import date
from datetime import datetime
from datetime import time
from datetime import timedelta

import pytz
import re
import unittest
from braintree.test.nonces import Nonces
from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.sites.models import Site
from django.core import mail
from django.core.urlresolvers import reverse
from django.utils import timezone
from django_webtest import WebTest
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.utils import braintree
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import Photographer
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_food_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers
from gouda.postmaster.models import Thread
from gouda.profiles.models import GenderType
from gouda.profiles.models import Message
from gouda.profiles.models import Profile
from gouda.profiles.utils import PasswordResetMixin
from gouda.profiles.utils import ProfileCreateViewMixin
from gouda.profiles.utils import ProfileDataMixin
from gouda.profiles.utils import ProfileImageUploadMixin
from gouda.profiles.utils import SetUpProfilesMixin
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from gouda.urlresolvers import reverse_with_query
from gouda.users.models import LoginInfo
from gouda.users.models import User
from psycopg2.extras import DateTimeTZRange
from rest_framework import status

MAX_LOGIN_INFOS = getattr(settings, 'MAX_LOGIN_INFOS')


class CustomerCreateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(CustomerCreateViewTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(CustomerCreateViewTestCase, cls).tearDownClass()
        tear_down_profiles()

    @property
    def view_name(self):
        return 'profiles:customer_update'

    def setUp(self):
        self.profile = random.choice(Profile.objects.all())
        self.data = {
            'payment_method_nonce': Nonces.Transactable,
        }

    def test_page_exists(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/customer_update.html')

    def test_create_customer(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, url)


class CustomerUpdateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(CustomerUpdateViewTestCase, cls).setUpClass()
        create_photographers(1, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(CustomerUpdateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.profile = random.choice(Profile.objects.all())
        self.data = {
            'payment_method_nonce': Nonces.Transactable,
        }

    def test_page_exists(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/customer_update.html')

    def test_update_customer(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, url)

    def test_invalid_card(self):
        data = {
            'payment_method_nonce': Nonces.Consumed,
        }
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()

    @property
    def view_name(self):
        return 'profiles:customer_update'


class BookingRequestListViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingRequestListViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(10)
        create_profiles(10)

        photographer = Photographer.objects.all()[0]
        for booking in Booking.objects.all():
            BookingRequest.objects.create(**{
                'photographer': photographer,
                'booking': booking
            })

    @classmethod
    def tearDownClass(cls):
        super(BookingRequestListViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.profile = BookingRequest.objects.all()[0].photographer.profile
        for booking_request in BookingRequest.objects.all():
            booking_request.photographer = self.profile.photographer
            booking_request.save()

    def test_page_exists(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/booking_request_list.html')

    def test_login_required(self):
        url = reverse(self.view_name)
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_photographer_required(self):
        profile = Profile.objects.filter(photographer__isnull=True)[0]
        url = reverse(self.view_name)
        response = self.app.get(url, user=profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        profile.delete()

    def test_never_cache_headers(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def order_by(self, ordering_field):
        is_descending = '-' in ordering_field
        url = reverse_with_query(self.view_name, {'o': ordering_field})
        ordering_field = ordering_field.replace('-', '')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['booking_requests']), 1)
        last_booking_request = None
        for booking_request in response.context['booking_requests']:
            if last_booking_request:
                assertion = self.assertGreaterEqual if is_descending else self.assertLessEqual
                if ordering_field == 'locality':
                    assertion(last_booking_request.booking.address.normalized_locality, booking_request.booking.address.normalized_locality)
                elif ordering_field == 'region':
                    assertion(last_booking_request.booking.address.normalized_region, booking_request.booking.address.normalized_region)
                elif ordering_field == 'display_name':
                    assertion(last_booking_request.booking.display_name, booking_request.booking.display_name)
                elif ordering_field == 'time_range':
                    assertion(last_booking_request.booking.time_range, booking_request.booking.time_range)
                else:
                    assertion(getattr(last_booking_request, ordering_field), getattr(booking_request, ordering_field))
            last_booking_request = booking_request

    def test_order_by_status(self):
        self.order_by('status')

    def test_order_by_status_descending(self):
        self.order_by('-status')

    def test_order_by_locality(self):
        self.order_by('locality')

    def test_order_by_locality_descending(self):
        self.order_by('-locality')

    def test_order_by_region(self):
        self.order_by('region')

    def test_order_by_region_descending(self):
        self.order_by('-region')

    def test_order_by_display_name(self):
        self.order_by('display_name')

    def test_order_by_display_name_descending(self):
        self.order_by('-display_name')

    def test_order_by_time_range(self):
        self.order_by('time_range')

    def test_order_by_time_range_descending(self):
        self.order_by('-time_range')

    def test_filter_by_region(self):
        region = BookingRequest.objects.filter(photographer__profile=self.profile)[0].booking.address.normalized_region
        url = reverse_with_query('profiles:booking_request_list', {'region': region})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['booking_requests']), 0)
        for booking_request in response.context['booking_requests']:
            self.assertEqual(booking_request.booking.address.normalized_region, region)

    def test_filter_by_locality(self):
        locality = BookingRequest.objects.filter(photographer__profile=self.profile)[0].booking.address.normalized_locality
        url = reverse_with_query('profiles:booking_request_list', {'locality': locality})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['booking_requests']), 0)
        for booking_request in response.context['booking_requests']:
            self.assertEqual(booking_request.booking.address.normalized_locality, locality)

    def test_filter_by_status(self):
        request_status = BookingRequest.objects.filter(photographer__profile=self.profile)[0].status
        url = reverse_with_query('profiles:booking_request_list', {'status': request_status})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['booking_requests']), 0)
        for booking_request in response.context['booking_requests']:
            self.assertEqual(booking_request.status, request_status)

    @property
    def view_name(self):
        return 'profiles:booking_request_list'


class BookingRequestDetailViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingRequestDetailViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(1, do_send_client_confirmation=False, do_assign_photographers=False)

        for photographer in Photographer.objects.all():
            photographer.address.region = 'MD'
            photographer.address.save()
        photographer = Photographer.objects.all()[0]
        photographer.address.region = 'TX'
        photographer.address.save()
        photographer.save()

        for booking in Booking.objects.all():
            booking.address.region = 'TX'
            booking.address.save()

        BookingRequest.objects.create(**{
            'photographer': Photographer.objects.all()[0],
            'booking': Booking.objects.all()[0],
        })

    @classmethod
    def tearDownClass(cls):
        super(BookingRequestDetailViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BookingRequestDetailViewTestCase, self).setUp()
        self.booking_request = BookingRequest.objects.all()[0]
        self.user = self.booking_request.photographer.profile.user

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/booking_request_detail.html')

    def test_invalid_access(self):
        user = None
        for photographer in Photographer.objects.all():
            if photographer != self.booking_request.photographer:
                user = photographer.profile.user
                break
        url = self.get_url()
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'profiles:booking_request_detail'

    def get_kwargs(self):
        return {
            'pk': self.booking_request.id
        }


class BookingAlbumDownloadViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingAlbumDownloadViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(1, do_create_images=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumDownloadViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.user = self.booking.client.user

    def test_download_zip_file(self):
        url = reverse('profiles:booking_album_download', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.user.email_address)
        booking = Booking.objects.get(id=self.booking.id)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertTrue(booking.media_file.url in response.headers['location'])
        # self.assertIn('Content-Disposition', response.headers)
        # self.assertIn('attachment', response.headers.get('Content-Disposition'))
        # self.assertIn('application/zip', response.headers.get('Content-Type'))

    def test_unauthorized_download(self):
        url = reverse('profiles:booking_album_download', kwargs={'pk': self.booking.id})
        user = self.booking.photographer.profile.user
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        url = reverse('profiles:booking_album_download', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        url = reverse('profiles:booking_album_download', kwargs={'pk': self.booking.id})
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))


class BookingCsvDownloadViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingCsvDownloadViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(5, do_create_images=False)

    @classmethod
    def tearDownClass(cls):
        super(BookingCsvDownloadViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.user = self.booking.client.user

    def test_download_csv_file(self):
        url = reverse('profiles:booking_csv_download')
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('Content-Disposition', response.headers)
        self.assertIn('attachment', response.headers.get('Content-Disposition'))
        self.assertIn('text/csv', response.headers.get('Content-Type'))

    def test_never_cache_headers(self):
        url = reverse('profiles:booking_csv_download')
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        url = reverse('profiles:booking_csv_download')
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))


class BookingDetailViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingDetailViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(10)

    @classmethod
    def tearDownClass(cls):
        super(BookingDetailViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.profile = self.booking.client

    def test_page_exists(self):
        url = reverse('profiles:booking_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/booking_detail.html')

    def test_login_required(self):
        url = reverse('profiles:booking_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse('profiles:booking_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_access_unauthorized_booking(self):
        url = reverse('profiles:booking_detail', kwargs={'pk': self.booking.id})
        profile = None
        for profile in Profile.objects.all():
            if profile.id != self.profile.id:
                break
        response = self.app.get(url, user=profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_staff_access(self):
        url = reverse('profiles:booking_detail', kwargs={'pk': self.booking.id})
        profile = None
        for profile in Profile.objects.all():
            if profile.id != self.profile.id:
                break
        profile.user.is_staff = True
        profile.user.save()
        response = self.app.get(url, user=profile.email_address, expect_errors=False)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        profile.user.is_staff = False
        profile.user.save()


class BookingListViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingListViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_food_bookings(10)

    @classmethod
    def tearDownClass(cls):
        super(BookingListViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.profile = Booking.objects.all()[0].client
        for booking in Booking.objects.all():
            booking.client = self.profile
            booking.save()

    def test_page_exists(self):
        url = reverse('profiles:booking_list')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/booking_list.html')

    def test_login_required(self):
        url = reverse('profiles:booking_list')
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse('profiles:booking_list')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def order_by(self, ordering_field):
        is_descending = '-' in ordering_field
        url = reverse_with_query('profiles:booking_list', {'o': ordering_field})
        ordering_field = ordering_field.replace('-', '')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(response.context['bookings'].count(), 1)
        last_booking = None
        for booking in response.context['bookings']:
            if last_booking:
                assertion = self.assertGreaterEqual if is_descending else self.assertLessEqual
                if ordering_field == 'photographer':
                    assertion(last_booking.photographer.full_name, booking.photographer.full_name)
                elif ordering_field == 'locality':
                    assertion(last_booking.address.normalized_locality.lower(), booking.address.normalized_locality.lower())
                elif ordering_field == 'region':
                    assertion(last_booking.address.normalized_region.lower(), booking.address.normalized_region.lower())
                elif ordering_field == 'display_name':
                    assertion(last_booking.display_name.replace(' ', ''), booking.display_name.replace(' ', ''))
                else:
                    assertion(getattr(last_booking, ordering_field), getattr(booking, ordering_field))
            last_booking = booking

    def test_order_by_photographer(self):
        self.order_by('photographer')

    def test_order_by_photographer_descending(self):
        self.order_by('-photographer')

    def test_order_by_time_range(self):
        self.order_by('time_range')

    def test_order_by_time_range_descending(self):
        self.order_by('-time_range')

    def test_order_by_locality(self):
        self.order_by('locality')

    def test_order_by_locality_descending(self):
        self.order_by('-locality')

    def test_order_by_region(self):
        self.order_by('region')

    def test_order_by_region_descending(self):
        self.order_by('-region')

    def test_order_by_display_name(self):
        self.order_by('display_name')

    def test_order_by_display_name_descending(self):
        self.order_by('-display_name')

    def test_filter_by_region(self):
        region = Booking.objects.filter(client=self.profile)[0].address.normalized_region
        url = reverse_with_query('profiles:booking_list', {'region': region})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['bookings']), 0)
        for booking in response.context['bookings']:
            self.assertEqual(booking.address.normalized_region.lower(), region.lower())

    def test_filter_by_locality(self):
        locality = Booking.objects.filter(client=self.profile)[0].address.normalized_locality
        url = reverse_with_query('profiles:booking_list', {'locality': locality})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['bookings']), 0)
        for booking in response.context['bookings']:
            self.assertEqual(booking.address.normalized_locality.lower(), locality.lower())

    def test_form_exists(self):
        url = reverse('profiles:booking_list')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_filter_by_restaurant_name_query(self):
        query = FoodBooking.objects.filter(client=self.profile)[0].restaurant_name
        url = reverse_with_query('profiles:booking_list', {'query': query})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['bookings']), 0)
        for booking in response.context['bookings']:
            self.assertTrue(query in booking.get_display_name())

    def test_filter_by_locality_query(self):
        query = FoodBooking.objects.filter(client=self.profile)[0].address.normalized_locality
        url = reverse_with_query('profiles:booking_list', {'query': query})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['bookings']), 0)
        for booking in response.context['bookings']:
            self.assertTrue(query.lower() in booking.address.normalized_locality.lower())


class PayoutListViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(PayoutListViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(10)
        for booking in Booking.objects.all():
            booking.num_shots = random.randint(12, 100)
            booking.did_photographer_approve = True
            booking.did_staff_approve = True
            booking.did_client_approve = True
            booking.photographer_approval_date = timezone.now()
            booking.staff_approval_date = timezone.now()
            booking.client_approval_date = timezone.now()
            booking.save()

    @classmethod
    def tearDownClass(cls):
        super(PayoutListViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.profile = Booking.objects.all()[0].photographer.profile
        for booking in Booking.objects.all():
            booking.photographer = self.profile.photographer
            booking.save()
        self.assertGreater(Booking.objects.filter(photographer=self.profile.photographer).count(), 0)

    def test_page_exists(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/payout_list.html')

    def test_login_required(self):
        url = reverse(self.view_name)
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse(self.view_name)
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def order_by(self, ordering_field):
        is_descending = '-' in ordering_field
        url = reverse_with_query(self.view_name, {'o': ordering_field})
        ordering_field = ordering_field.replace('-', '')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(response.context['bookings'].count(), 1)
        last_booking = None
        for booking in response.context['bookings']:
            if last_booking:
                assertion = self.assertGreaterEqual if is_descending else self.assertLessEqual
                if ordering_field == 'date':
                    assertion(last_booking.client_approval_date, booking.client_approval_date)
                elif ordering_field == 'amount':
                    assertion(last_booking.service_fee_amount, booking.service_fee_amount)
                elif ordering_field == 'status':
                    assertion(last_booking.transaction_status, booking.transaction_status)
                else:
                    assertion(getattr(last_booking, ordering_field), getattr(booking, ordering_field))
            last_booking = booking

    @property
    def view_name(self):
        return 'profiles:payout_list'

    def test_order_by_date(self):
        self.order_by('date')

    def test_order_by_date_descending(self):
        self.order_by('-date')

    def test_order_by_amount(self):
        self.order_by('amount')

    def test_order_by_amount_descending(self):
        self.order_by('-amount')

    def test_order_by_status(self):
        self.order_by('status')

    def test_order_by_status_descending(self):
        self.order_by('-status')


class ShootListViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ShootListViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(10)

    @classmethod
    def tearDownClass(cls):
        super(ShootListViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.profile = Booking.objects.all()[0].photographer.profile
        for booking in Booking.objects.all():
            booking.photographer = self.profile.photographer
            booking.save()
        b = Booking.objects.all().order_by('date_created')[0]
        b.display_name = 'doloribus molestiae cumque aliquam mollitia et odi'
        b.save()

        b = Booking.objects.all().order_by('date_created')[1]
        b.display_name = 'dolor sapiente aperiam officia obcaecati totam'
        b.save()

    def test_page_exists(self):
        url = reverse('profiles:shoot_list')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/shoot_list.html')

    def test_login_required(self):
        url = reverse('profiles:shoot_list')
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse('profiles:shoot_list')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def order_by(self, ordering_field):
        is_descending = '-' in ordering_field
        url = reverse_with_query('profiles:shoot_list', {'o': ordering_field})
        ordering_field = ordering_field.replace('-', '')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(response.context['bookings'].count(), 1)
        last_booking = None
        # print('\n'.join([b.display_name for b in Booking.objects.filter(photographer=self.profile.photographer).order_by('display_name')]))
        for booking in response.context['bookings']:
            if last_booking:
                assertion = self.assertGreaterEqual if is_descending else self.assertLessEqual
                if ordering_field == 'photographer':
                    assertion(last_booking.photographer.full_name, booking.photographer.full_name)
                elif ordering_field == 'locality':
                    assertion(last_booking.address.normalized_locality, booking.address.normalized_locality)
                elif ordering_field == 'region':
                    assertion(last_booking.address.normalized_region, booking.address.normalized_region)
                elif ordering_field == 'display_name':
                    assertion(last_booking.display_name.replace(' ', ''), booking.display_name.replace(' ', ''))
                else:
                    assertion(getattr(last_booking, ordering_field), getattr(booking, ordering_field))
            last_booking = booking

    def test_order_by_photographer(self):
        self.order_by('photographer')

    def test_order_by_photographer_descending(self):
        self.order_by('-photographer')

    def test_order_by_time_range(self):
        self.order_by('time_range')

    def test_order_by_time_range_descending(self):
        self.order_by('-time_range')

    def test_order_by_locality(self):
        self.order_by('locality')

    def test_order_by_locality_descending(self):
        self.order_by('-locality')

    def test_order_by_region(self):
        self.order_by('region')

    def test_order_by_region_descending(self):
        self.order_by('-region')

    def test_order_by_display_name(self):
        self.order_by('display_name')

    def test_order_by_display_name_descending(self):
        self.order_by('-display_name')

    def test_filter_by_region(self):
        region = Booking.objects.filter(photographer__profile=self.profile)[0].address.normalized_region
        url = reverse_with_query('profiles:shoot_list', {'region': region})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['bookings']), 0)
        for booking in response.context['bookings']:
            self.assertEqual(booking.address.normalized_region, region)

    def test_filter_by_locality(self):
        locality = Booking.objects.filter(photographer__profile=self.profile)[0].address.normalized_locality
        url = reverse_with_query('profiles:shoot_list', {'locality': locality})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.context['bookings']), 0)
        for booking in response.context['bookings']:
            self.assertEqual(booking.address.normalized_locality, locality)


class ShootDetailViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ShootDetailViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(10)

    @classmethod
    def tearDownClass(cls):
        super(ShootDetailViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        time_zone = 'US/Central'
        self.booking = Booking.objects.all()[0]
        self.profile = self.booking.photographer.profile
        self.booking.address.time_zone = time_zone
        self.booking.contact_name = 'Sterling Archer'
        self.booking.contact_email_address = 'sterlingarcher@gmail.com'
        self.booking.contact_phone_number = '5555555555'
        self.booking.save()
        self.booking.address.save()

        time_zone = 'US/Central'
        self.time_range = DateTimeTZRange(
            lower=pytz.timezone(time_zone).localize(datetime.today().replace(hour=9, microsecond=0)),
            upper=pytz.timezone(time_zone).localize(datetime.today().replace(hour=12, microsecond=0)),
        )
        self.data = {
            'date': date(self.time_range.lower.year, self.time_range.lower.month, self.time_range.lower.day),
            'start_time': time(
                self.time_range.lower.hour,
                self.time_range.lower.minute,
                self.time_range.lower.second,
                self.time_range.lower.microsecond
            ),
            'stop_time': time(
                self.time_range.upper.hour,
                self.time_range.upper.minute,
                self.time_range.upper.second,
                self.time_range.upper.microsecond
            ),
        }

    def test_page_exists(self):
        url = reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/shoot_detail.html')

    def test_login_required(self):
        url = reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_access_unauthorized_booking(self):
        url = reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id})
        profile = None
        for profile in Profile.objects.all():
            if profile.id != self.profile.id:
                break
        response = self.app.get(url, user=profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_form_exists(self):
        url = reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_reschedule_booking(self):
        url = reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id})
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Fill out the form
        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id}))

        # Check data
        booking = Booking.objects.get(id=self.booking.id)
        self.assertIsNotNone(booking.time_range)
        self.assertEqual(booking.time_range.lower, self.time_range.lower)
        self.assertEqual(booking.time_range.upper, self.time_range.upper)


@unittest.skip('No longer exists')
class InboxViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(InboxViewTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(InboxViewTestCase, cls).tearDownClass()
        Profile.objects.all().delete()

    def setUp(self):
        self.profile = Profile.objects.all()[0]

    def tearDown(self):
        Profile.objects.all().delete()

    def test_page_exists(self):
        url = reverse('profiles:inbox')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/inbox.html')

    def test_login_required(self):
        url = reverse('profiles:inbox')
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse('profiles:inbox')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


@unittest.skip('No longer exists')
class ThreadViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ThreadViewTestCase, cls).setUpClass()
        create_profiles(3)

    @classmethod
    def tearDownClass(cls):
        super(ThreadViewTestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.profiles = Profile.objects.all()
        self.thread = Thread.objects.create()
        message = Message.objects.create(
            body='LANA!!!!!!!',
            thread=self.thread,
            sender=self.profiles[0],
            receiver=self.profiles[1]
        )

    def test_invalid_access(self):
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[2].email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_page_exists(self):
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[0].email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/thread.html')

    def test_login_required(self):
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_form_exists(self):
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[0].email_address)
        self.assertNotEqual(len(response.forms), 0)

    def test_create_message(self):
        num_messages = Message.objects.all().count()
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[0].email_address)
        form = response.form
        form['body'] = 'LANNNAA!!!'
        response = form.submit()
        self.assertRedirects(response, url)
        self.assertEqual(Message.objects.all().count(), num_messages + 1)

    def test_create_blank_message(self):
        num_messages = Message.objects.all().count()
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[0].email_address)
        form = response.form
        form['body'] = ''
        response = form.submit()
        self.assertFormError(response, 'form', 'body', ['This field is required.'])
        self.assertEqual(num_messages, Message.objects.all().count())

    def test_messages_get_read(self):
        self.assertEqual(Message.objects.all().filter(is_read=True).count(), 0)
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[1].email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Message.objects.all().filter(is_read=True).count(), 1)

    def test_never_cache_headers(self):
        url = reverse('profiles:thread', kwargs={'pk': self.thread.id})
        response = self.app.get(url, user=self.profiles[0].email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


class BraintreeMerchantAccoutUpdateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BraintreeMerchantAccoutUpdateViewTestCase, cls).setUpClass()
        create_profiles()

    @classmethod
    def tearDownClass(cls):
        super(BraintreeMerchantAccoutUpdateViewTestCase, cls).tearDownClass()
        tear_down_profiles()

    def tearDown(self):
        BraintreeMerchantAccount.objects.all().delete()

    def setUp(self):
        self.user = Profile.objects.all()[0].user
        date_of_birth = date.today() - timedelta(days=365 * 28)
        self.data = {
            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'CA',
            'individual_street_address': '1504 Parkpath Way',
            'individual_month': int(date_of_birth.strftime('%m')),
            'individual_day': int(date_of_birth.strftime('%d')),
            'individual_year': int(date_of_birth.strftime('%Y')),
            'individual_email': self.user.email_address,
            'individual_first_name': self.user.first_name,
            'individual_last_name': self.user.last_name,
            'individual_phone': '4437421240',

            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        }

        for profile in Profile.objects.all():
            if profile.user == self.user:
                continue

    def submit_form(self, data, do_expect_errors=False):
        url = reverse('profiles:braintree_merchant_account_update')
        response = self.app.get(url, user=self.user.email_address)
        form = response.forms[0]

        for key in data.keys():
            form[key] = data.get(key)
        response = form.submit(expect_errors=True)

        if do_expect_errors:
            self.assertGreater(len(response.context['form'].errors), 0)
            self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
        else:
            self.assertRedirects(response, url)
            self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)
            braintree_merchant_account = BraintreeMerchantAccount.objects.all()[0]
            self.assertIsNotNone(braintree.MerchantAccount.find(braintree_merchant_account.braintree_id))

    def test_page_exists(self):
        url = reverse('profiles:braintree_merchant_account_update')
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/manage_account/braintree_merchant_account_update.html')
        self.assertIsNotNone(response.forms[0])

    def test_create_braintree_merchant_account(self):
        self.submit_form(self.data.copy())

    def test_update_braintree_merchant_account(self):
        self.submit_form(self.data.copy())
        data = self.data.copy()
        data.update({
            'funding_account_number': '1123581322',
            'funding_routing_number': '071000013',
        })
        self.submit_form(data)

    def test_update_braintree_merchant_account_with_invalid_data(self):
        self.submit_form(self.data.copy())
        data = self.data.copy()
        data.update({
            # 'funding_account_number': '123',
            'funding_routing_number': '123',
        })
        self.submit_form(data, do_expect_errors=True)

    # def test_update_braintree_merchant_account_with_invalid_data(self):


    # def test_invalid_access(self):
    #     user = User.objects.exclude(pk=self.user.pk)[0]
    #     url = reverse('profiles:braintree_merchant_account_update')
    #     response = self.app.get(url, user=user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class ProfileUpdateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ProfileUpdateViewTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(ProfileUpdateViewTestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.profile = Profile.objects.all()[0]

    def test_page_exists(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/update_profile.html')

    def test_login_required(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_form_exists(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertNotEqual(len(response.forms), 0)

    def test_edit_full_name(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        form['full_name'] = 'Duchess'
        response = form.submit()
        self.assertRedirects(response, url)

        profile = Profile.objects.get(pk=self.profile.pk)
        self.assertEqual(profile.full_name, 'Duchess')

    def test_edit_gender(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        form['gender'] = GenderType.OTHER
        response = form.submit()
        self.assertRedirects(response, url)

        self.assertEqual(Profile.objects.all().count(), 1)
        profile = Profile.objects.get(pk=self.profile.pk)
        self.assertEqual(profile.gender, GenderType.OTHER)

    def test_edit_locality(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        form['locality'] = 'Funky Town'
        response = form.submit()
        self.assertRedirects(response, url)
        profile = Profile.objects.get(pk=self.profile.pk)
        self.assertEqual(profile.locality, 'Funky Town')

    def test_edit_without_date_of_birth(self):
        date_of_birth = self.profile.date_of_birth
        self.profile.date_of_birth = None
        self.profile.save()
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/update_profile.html')
        self.profile.date_of_birth = date_of_birth
        self.profile.save()

    def test_never_cache_headers(self):
        url = reverse('profiles:update_profile')
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


class EditAccountViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(EditAccountViewTestCase, cls).setUpClass()
        create_profiles(1)

    @classmethod
    def tearDownClass(cls):
        super(EditAccountViewTestCase, cls).tearDownClass()
        tear_down_profiles()

    def setUp(self):
        self.profile = Profile.objects.all()[0]

    def test_page_exists(self):
        url = reverse('profiles:edit_account')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'profiles/edit_account.html')

    def test_login_required(self):
        url = reverse('profiles:edit_account')
        response = self.app.get(url)
        self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))

    def test_never_cache_headers(self):
        url = reverse('profiles:edit_account')
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


# class DashboardViewTestCase(SetUpProfilesMixin, ProfileDataMixin, WebTest):
#     def test_page_exists(self):
#         url = reverse('profiles:dashboard', kwargs={'pk': self.archer_profile.pk})
#         response = self.app.get(url, user=self.archer_profile.user.email_address)
#         self.assertEqual(response.status_code, status.HTTP_200_OK)
#
#     def test_login_required(self):
#         url = reverse('profiles:dashboard', kwargs={'pk': self.archer_profile.pk})
#         response = self.app.get(url)
#         self.assertRedirects(response, '%s?next=%s' % (reverse('profiles:login'), url))
#
#     def test_invalid_access_by_other_user(self):
#         url = reverse('profiles:dashboard', kwargs={'pk': self.archer_profile.pk})
#         response = self.app.get(url, user=self.lana_profile.user.email_address, expect_errors=True)
#         self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PasswordResetViewTestCase(PasswordResetMixin, SetUpProfilesMixin, ProfileDataMixin, WebTest):
    def test_page_exists(self):
        url = reverse('profiles:password_reset')
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_form_exists(self):
        url = reverse('profiles:password_reset')
        response = self.app.get(url)
        self.assertEqual(len(response.forms), 1)

    def test_password_reset(self):
        self.authenticate_user('Failed to authenticate user before password reset')

        response = self.password_reset(self.archer_profile_form_data['email_address'])
        self.assertRedirects(response, reverse('profiles:password_reset_done'))

        self.authenticate_user('Failed to authenticate user after password reset')

    def authenticate_user(self, message):
        user = authenticate(username=self.archer_profile_form_data['email_address'],
                            password=self.archer_profile_form_data['password'])
        self.assertIsNotNone(user, message)

    def test_password_with_nonexistent_user(self):
        response = self.password_reset('krieger123@gmail.com')
        self.assertRedirects(response, reverse('profiles:password_reset_done'))

    def test_cache_headers(self):
        url = reverse('profiles:password_reset')
        response = self.app.get(url)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=86400', response.headers['Cache-Control'])


class PasswordResetConfirmViewTestCase(PasswordResetMixin, SetUpProfilesMixin, ProfileDataMixin, WebTest):
    def setUp(self):
        super(PasswordResetConfirmViewTestCase, self).setUp()

    def test_password_reset_email(self):
        # Reset my password
        self.password_reset(self.archer_profile_form_data['email_address'])

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1, 'Failed to receive email with password reset link')

        # Check sender
        self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')

        # Check for reset link
        url = self.get_password_reset_confirm_url(mail.outbox[0])
        self.assertIsNotNone(url, 'Failed to find reset link in email')

    def get_password_reset_confirm_url(self, email_message):
        match_object = re.compile(r"(http[s]*://[\S]+)").search(email_message.body)
        url = match_object.groups(1)[0]
        return url

    def test_password_reset(self):
        # Reset my password
        self.password_reset(self.archer_profile_form_data['email_address'])

        # Get password reset link
        url = self.get_password_reset_confirm_url(mail.outbox[0])

        # Go to password reset confirm page
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        new_password = 'dangerzoner123!'
        form['new_password1'] = new_password
        form['new_password2'] = new_password
        response = form.submit()
        self.assertRedirects(response, reverse('index'), host='%s:80' % Site.objects.get_current().domain)

        user = authenticate(username=self.archer_profile_form_data['email_address'], password=new_password)
        self.assertIsNotNone(user, 'Failed to authenticate user after changing password')

        # # Link should be broken now
        # response = self.client.get(url)
        # self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        # Reset my password
        self.password_reset(self.archer_profile_form_data['email_address'])

        # Get password reset link
        url = self.get_password_reset_confirm_url(mail.outbox[0])

        # Go to password reset confirm page
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


class ImageUpdateViewTestCase(ProfileDataMixin, SetUpProfilesMixin, ProfileImageUploadMixin, WebTest):
    def upload_image(self, profile, image_name, is_valid=True):
        profile = Profile.objects.get(pk=profile.pk)
        self.assertFalse(bool(profile.image))
        url = reverse('profiles:image_update', kwargs={'pk': profile.pk})
        response = self.app.get(url, user=profile.user.email_address)

        form = response.form
        form['image'] = self.encode_image(image_name)
        response = form.submit()

        if is_valid:
            self.assertRedirects(response, reverse('index'))
            profile = Profile.objects.get(pk=profile.pk)
            self.assertTrue(bool(profile.image))

        return response

    def test_jpeg_image(self):
        self.upload_image(self.archer_profile, 'archer.jpg')

    def test_png_image(self):
        self.upload_image(self.archer_profile, 'archer.png')

    def test_gif_image(self):
        response = self.upload_image(self.archer_profile, 'archer.gif', False)
        self.assertFormError(response, 'form', 'image', ['Upload a valid image. Images must be a PNG or JPG file that is less than %d MB.' % settings.MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES])

    @unittest.skip('Need larger image...')
    def test_large_image(self):
        response = self.upload_image(self.archer_profile, 'archer_large.png', False)
        self.assertFormError(response, 'form', 'image', ['Maximum image file size is 5 MB'])

    def test_page_exists(self):
        url = reverse('profiles:image_update', kwargs={'pk': self.archer_profile.pk})
        response = self.app.get(url, user=self.archer_user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_unauthorized_upload(self):
        # Archer should NOT be able to upload a photo for Lana
        # self.assertTrue(self.client.login(username=self.archer_profile_form_data['email_address'], password=self.archer_profile_form_data['password']))
        url = reverse('profiles:image_update', kwargs={'pk': self.lana_profile.pk})
        response = self.app.get(url, user=self.archer_profile_form_data['email_address'], expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_form_exists(self):
        url = reverse('profiles:image_update', kwargs={'pk': self.archer_profile.pk})
        response = self.app.get(url, user=self.archer_user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_login_required(self):
        url = reverse('profiles:image_update', kwargs={'pk': self.archer_profile.pk})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

    def test_never_cache_headers(self):
        url = reverse('profiles:image_update', kwargs={'pk': self.archer_profile.pk})
        response = self.app.get(url, user=self.archer_user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


class LoginViewTestCase(ProfileDataMixin, WebTest):
    def tearDown(self):
        tear_down_profiles()

    def setUp(self):
        tear_down_profiles()
        LoginInfo.objects.all().delete()
        user_data = self.get_user_data(self.archer_profile_form_data)
        self.archer_user = User.objects.create_user(**user_data)
        profile_data = self.get_profile_data(self.archer_profile_form_data)
        self.profile = Profile.objects.create(user=self.archer_user, **profile_data)
        mail.outbox = []

    def test_page_exists(self):
        url = reverse('profiles:login')
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_form_exists(self):
        url = reverse('profiles:login')
        response = self.app.get(url)
        self.assertEqual(len(response.forms), 1)

    def test_login_info_gets_created(self):
        self.assertEqual(LoginInfo.objects.all().count(), 0)
        user_data = self.get_user_data(self.archer_profile_form_data)

        url = reverse('profiles:login')
        response = self.app.get(url)
        form = response.form
        form['username'] = user_data['email_address']
        form['password'] = user_data['password']
        response = form.submit()

        self.assertEqual(LoginInfo.objects.all().count(), 1)

    # def test_num_login_infos_is_limited(self):
    #     self.assertEqual(LoginInfo.objects.all().count(), 0)
    #     user_data = self.get_user_data(self.archer_profile_form_data)
    #
    #     for i in range(MAX_LOGIN_INFOS * 2):
    #         url = reverse('profiles:login')
    #         response = self.app.get(url)
    #         form = response.form
    #         form['username'] = user_data['email_address']
    #         form['password'] = user_data['password']
    #         response = form.submit()
    #
    #         url = reverse('profiles:logout')
    #         response = self.app.get(url)
    #
    #     self.assertEqual(LoginInfo.objects.all().count(), MAX_LOGIN_INFOS)

    def test_login(self):
        user_data = self.get_user_data(self.archer_profile_form_data)

        url = reverse('profiles:login')
        response = self.app.get(url)
        form = response.form
        form['username'] = user_data['email_address']
        form['password'] = user_data['password']
        response = form.submit()

        self.assertIn('sessionid', response.headers['Set-Cookie'])

        url = reverse('profiles:image_update', kwargs={'pk': self.profile.pk})
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_login_after_verification_timeout(self):
        user_data = self.get_user_data(self.archer_profile_form_data)
        user = User.objects.get(email_address=user_data['email_address'])
        profile = user.profile
        self.assertFalse(profile.is_verified)
        user.last_login = timezone.now() - timedelta(seconds=getattr(settings, 'UNVERIFIED_PROFILE_SESSION_COOKIE_AGE'))
        user.save()

        # When attempting to login the user should be told to verify their email...
        url = reverse('profiles:login')
        response = self.app.get(url)
        form = response.form
        form['username'] = user_data['email_address']
        form['password'] = user_data['password']
        response = form.submit()
        self.assertFormError(response, 'form', None, ['Please verify your email address. A new verification email has been sent to you.'])
        self.assertNotEqual(response.status_code, status.HTTP_302_FOUND)

        # Also a new verification link should be sent out...
        self.assertEqual(len(mail.outbox), 1, 'Failed to receive email with verification link')

        # Check sender
        self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')

        # # Check for reset link
        # match_object = re.compile(r"(https://[\S]+)").search(mail.outbox[0].body)
        # url = match_object.groups(1)[0]
        # self.assertIsNotNone(url, 'Failed to find reset link in email')

    def test_never_cache_headers(self):
        url = reverse('profiles:login')
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])


class VerifyEmailAddressViewTestCase(ProfileDataMixin, ProfileCreateViewMixin, WebTest):
    # @classmethod
    # def setUpClass(cls):
    #     super(VerifyEmailAddressViewTestCase, cls).setUpClass()
    #     tear_down_profiles()

    def tearDown(self):
        tear_down_profiles()

    def get_verify_email_address_url(self, email_message):
        match_object = re.compile(r"(http[s]*://[\S]+)").search(email_message.body)
        url = match_object.groups(1)[0]
        return url

    def test_verify_email_address(self):
        response = self.create_profile(self.archer_profile_form_data)
        self.assertTrue(Profile.objects.filter(user__email_address=self.archer_profile_form_data.get('email_address')).exists())
        self.assertFalse(self.is_profile_verified(self.archer_profile_form_data), 'Profile should not be verified')

        # Get verify email address link
        url = self.get_verify_email_address_url(mail.outbox[0])

        # Visit url
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, reverse('index'))
        self.assertTrue(self.is_profile_verified(self.archer_profile_form_data), 'Failed to verify profile after clicking verification link')

        # URL should be busted now
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Is the profile pending...?
        self.assertTrue(self.get_user(self.archer_profile_form_data).profile.is_active)

    def test_success_message(self):
        response = self.create_profile(self.archer_profile_form_data)
        self.assertTrue(Profile.objects.filter(user__email_address=self.archer_profile_form_data.get('email_address')).exists())
        self.assertFalse(self.is_profile_verified(self.archer_profile_form_data), 'Profile should not be verified')

        # Get verify email address link
        url = self.get_verify_email_address_url(mail.outbox[0])

        # Visit url
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)
        self.assertRedirects(response, reverse('index'))
        response = response.follow()
        self.assertTrue(self.is_profile_verified(self.archer_profile_form_data), 'Failed to verify profile after clicking verification link')

        # Look for message
        self.assertIn('messages', response.context)
        self.assertIsNotNone(response.context['messages'])
        self.assertIn('Your email address has been verified.', response)

    def is_profile_verified(self, form_data):
        user = self.get_user(form_data)
        return user.profile.is_verified

    def get_user(self, form_data):
        user_data = self.get_user_data(form_data)
        user = User.objects.get(email_address=user_data['email_address'])
        return user


class ProfileCreateViewTestCase(ProfileDataMixin, ProfileCreateViewMixin, WebTest):
    def setUp(self):
        tear_down_profiles()

    def test_page_exists(self):
        url = reverse('profiles:create')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_form_exists(self):
        url = reverse('profiles:create')
        response = self.app.get(url)
        self.assertEqual(len(response.forms), 1)

    def test_blank_data(self):
        url = reverse('profiles:create')
        response = self.client.post(url, {})
        for key in self.archer_profile_form_data.keys():
            self.assertFormError(response, 'form', key, 'This field is required.')

    def test_form_success(self):
        response = self.create_profile(self.archer_profile_form_data)
        self.assertIn('sessionid', response.headers['Set-Cookie'])
        user = User.objects.get(full_name=self.archer_profile_form_data['full_name'], email_address=self.archer_profile_form_data['email_address'])
        self.assertIsNotNone(user)
        profile = Profile.objects.get(user=user)
        self.assertIsNotNone(profile)
        self.assertRedirects(response, reverse('profiles:image_update', kwargs={'pk': profile.pk}))

    def test_never_cache_headers(self):
        url = reverse('profiles:login')
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])
