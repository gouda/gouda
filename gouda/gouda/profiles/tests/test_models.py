from __future__ import absolute_import

from django import test
from gouda.profiles.models import Profile
from gouda.profiles.models import Thread
from gouda.profiles.models import Message
from gouda.users.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core import mail
from gouda.profiles.utils import ProfileDataMixin
from gouda.profiles.utils import ProfileImageUploadMixin
from gouda.profiles.utils import create_profiles
import re
from django.conf import settings
import gouda.postmaster.models

SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')


class MessageTestCase(test.TestCase):
    def setUp(self):
        create_profiles(2)
        mail.outbox = []

    def test_init(self):
        Message()

    def test_create_message(self):
        profiles = Profile.objects.all()
        thread = Thread.objects.create()
        message = Message.objects.create(
            body='Hello World!',
            thread=thread,
            sender=profiles[0],
            receiver=profiles[1]
        )
        self.assertIsNotNone(message)

    def test_notification_email_for_message(self):
        profiles = Profile.objects.all()
        thread = Thread.objects.create()
        message = Message.objects.create(
            body='Hello World!\nLANA!\nLANA!\n\nLAAAANAAAAAAAA!!!!!!!!!!!!!\n\ndanger zone.',
            thread=thread,
            sender=profiles[0],
            receiver=profiles[1]
        )

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1)

        # Check sender
        self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')

        # Check for user message
        self.assertTrue(re.search(message.body, mail.outbox[0].body), 'Failed to find actual message in email')

    def tearDown(self):
        Profile.objects.all().delete()
        Thread.objects.all().delete()
        Message.objects.all().delete()


class ProfileTestCase(ProfileDataMixin, ProfileImageUploadMixin, test.TestCase):
    def setUp(self):
        user_data = self.get_user_data(self.archer_profile_form_data)
        self.user = User.objects.create_user(**user_data)

    def tearDown(self):
        mail.outbox = []
        gouda.postmaster.models.User.objects.all().delete()

    def test_init(self):
        Profile()

    def test_thumbnail_generation(self):
        image_path = self.get_image_path('archer.png')
        profile_data = self.get_profile_data(self.archer_profile_form_data)
        profile_data['image'] = SimpleUploadedFile('archer.png', file(image_path).read())
        profile_data['user'] = self.user
        profile = Profile.objects.create(**profile_data)
        aliases = [
            '160x160',
            '80x80',
            '35x35',
        ]
        for alias in aliases:
            self.assertIsNotNone(profile.image[alias].url)

    def test_create_profile_with_required_fields(self):
        profile = Profile.objects.create(user=self.user)
        self.assertIsNotNone(profile)

    def test_notification_email(self):
        profile = Profile.objects.create(user=self.user)
        self.assertIsNotNone(profile)

        server_outbox = [m for m in mail.outbox if 'A new profile was created by %s' % profile.full_name == m.subject]
        self.assertEqual(len(server_outbox), 1)
        server_mail = server_outbox[0]
        self.assertEqual(SERVER_EMAIL, server_mail.from_email)
        self.assertEqual(STAFF_EMAILS, server_mail.recipients())

    def test_profile_verification_email(self):
        profile = Profile.objects.create(user=self.user)
        self.assertIsNotNone(profile)

        outbox = [m for m in mail.outbox if 'Confirm your email address' == m.subject]
        self.assertEqual(len(outbox), 1)
        verify_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, verify_mail.from_email)
        self.assertEqual(len(verify_mail.recipients()), 1)
        self.assertEqual(profile.email_address, verify_mail.recipients()[0])

    def test_postmaster_gets_created(self):
        profile = Profile.objects.create(user=self.user)
        self.assertTrue(gouda.postmaster.models.User.objects.filter(profile=profile).exists())

