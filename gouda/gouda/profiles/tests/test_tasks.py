from __future__ import absolute_import

from datetime import timedelta

from django import test
from gouda.profiles.models import Profile
from gouda.profiles.models import StatusType
from gouda.users.models import User
from django.core.cache import cache
from gouda.profiles.utils import create_profiles
from freezegun import freeze_time
from gouda.profiles.tasks import send_profile_approved_emails
from django.conf import settings
from django.core import mail
from unittest import skip

PROFILE_APPROVED_EMAIL_DELAY = getattr(settings, 'PROFILE_APPROVED_EMAIL_DELAY_SECONDS')


def setUpModule():
    User.objects.all().delete()
    Profile.objects.all().delete()
    create_profiles(1)
    Profile.objects.all().update(status=StatusType.PENDING)
    cache.clear()


def tearDownModule():
    User.objects.all().delete()
    Profile.objects.all().delete()
    cache.clear()


@skip('No longer used')
class SendProfileApprovedEmailTestCase(test.TestCase):
    def setUp(self):
        self.profile = Profile.objects.all()[0]

    def test_send_email(self):
        future_time = self.profile.date_created + timedelta(seconds=PROFILE_APPROVED_EMAIL_DELAY)
        with freeze_time(future_time):
            self.assertEqual(Profile.objects.all()[0].status, StatusType.PENDING)
            send_profile_approved_emails.apply()
            self.assertEqual(len(mail.outbox), 1)
            self.assertEqual(Profile.objects.all()[0].status, StatusType.ACTIVE)

    def test_send_email_with_invalid_time(self):
        past_time = self.profile.date_created + timedelta(seconds=PROFILE_APPROVED_EMAIL_DELAY - 1)
        with freeze_time(past_time):
            self.assertEqual(Profile.objects.all()[0].status, StatusType.PENDING)
            send_profile_approved_emails.apply()
            self.assertEqual(len(mail.outbox), 0)
            self.assertEqual(Profile.objects.all()[0].status, StatusType.PENDING)
