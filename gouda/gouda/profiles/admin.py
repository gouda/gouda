from __future__ import absolute_import

from django.contrib import admin
from gouda.profiles.models import Profile
from gouda.postmaster.models import User as PostmasterUser
import string


class ProfileAdmin(admin.ModelAdmin):
    list_display = (
        'full_name',
        'gender',
        'date_of_birth',
        'referral',
        'last_city',
        'last_region',
        'last_country',
        'last_ip_address',
        'date_created',
    )
    ordering = ()
    actions = ['add_to_postmaster']

    def last_city(self, obj):
        last_login_info = obj.last_login_info
        return last_login_info.city if last_login_info and last_login_info.city else obj.locality
    last_city.short_description = 'City'

    def last_region(self, obj):
        last_login_info = obj.last_login_info
        return last_login_info.region if last_login_info and last_login_info.region else obj.region
    last_region.short_description = 'Region'

    def last_country(self, obj):
        last_login_info = obj.last_login_info
        return last_login_info.country_code if last_login_info and last_login_info.country_code else obj.country
    last_country.short_description = 'Country'

    def last_ip_address(self, obj):
        last_login_info = obj.last_login_info
        return last_login_info.ip_address if last_login_info else None
    last_ip_address.short_description = 'IP Address'

    def add_to_postmaster(self, request, queryset):
        num_added = 0
        for profile in queryset:
            user, is_created = PostmasterUser.objects.get_or_create(profile=profile)
            num_added = num_added + 1 if is_created else num_added
        self.message_user(request, '%d profiles were added to the postmaster.' % num_added)



admin.site.register(Profile, ProfileAdmin)
