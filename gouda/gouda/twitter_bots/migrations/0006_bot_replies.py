# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0005_auto_20151121_0237'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='replies',
            field=django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(max_length=100), size=None),
        ),
    ]
