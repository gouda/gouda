# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Reply',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('tweet_id', models.CharField(max_length=100)),
                ('content', models.TextField()),
                ('bot', models.ForeignKey(to='twitter_bots.Bot')),
                ('user', models.ForeignKey(to='twitter_bots.User')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
