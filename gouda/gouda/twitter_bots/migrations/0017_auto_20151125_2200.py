# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0016_auto_20151124_2330'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='is_follow_enabled',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bot',
            name='is_reply_enabled',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='bot',
            name='replies',
            field=django.contrib.postgres.fields.ArrayField(default=[], size=None, base_field=models.CharField(max_length=100), blank=True),
        ),
        migrations.AlterField(
            model_name='bot',
            name='screen_names',
            field=django.contrib.postgres.fields.ArrayField(default=[], size=None, base_field=models.CharField(max_length=100), blank=True),
        ),
        migrations.AlterField(
            model_name='bot',
            name='search_terms',
            field=django.contrib.postgres.fields.ArrayField(default=[], size=None, base_field=models.CharField(max_length=100), blank=True),
        ),
    ]
