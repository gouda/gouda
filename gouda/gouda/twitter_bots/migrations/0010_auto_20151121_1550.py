# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('twitter_bots', '0009_bot_reply_timeout'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bot',
            name='password',
        ),
        migrations.RemoveField(
            model_name='bot',
            name='username',
        ),
        migrations.AlterField(
            model_name='bot',
            name='reply_timeout',
            field=models.IntegerField(default=172800, verbose_name=b'Reply timeout (s)'),
        ),
    ]
