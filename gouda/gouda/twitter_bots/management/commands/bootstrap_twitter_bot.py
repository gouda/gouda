from __future__ import absolute_import

from django.core.management.base import BaseCommand
from gouda.twitter_bots.models import Bot


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('screen_name')

    def handle(self, *args, **options):
        bot = Bot.objects.get(
            screen_name=options.get('screen_name'),
        )
        bot.bootstrap_follows()
