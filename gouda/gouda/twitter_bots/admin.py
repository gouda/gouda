from __future__ import absolute_import

from django.contrib import admin
from gouda.twitter_bots.models import Bot


@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    list_display = (
        'screen_name',
        'speed',
        'is_follow_enabled',
        'is_reply_enabled'
    )
    fieldsets = (
        ('Login Credentials', {
            'fields': (
                'screen_name',
                'password',
            )
        }),
        ('API Credentials', {
            'fields': (
                'consumer_key',
                'consumer_secret',
                'access_token',
                'access_token_secret'
            )
        }),
        ('Configuration', {
            'fields': (
                'is_enabled',
                'are_email_notifications_enabled',
                'speed',
            )
        }),
        ('Reply', {
            'fields': (
                'is_reply_enabled',
                'search_terms',
                'replies',
                'reply_timeout'
            )
        }),
        ('Follow', {
            'fields': (
                'is_follow_enabled',
                'screen_names',
                'follow_source',
                'unfollow_timeout'
            )
        }),
        ('Geocode', {
            'fields': (
                'latitude',
                'longitude',
                'radius',
            )
        }),
    )
