class TwitterBotCacheKeys(object):
    prefix = 'gouda.twitter_bots.cache_keys.'

    @staticmethod
    def get_key(suffix):
        return '%s%s' % (TwitterBotCacheKeys.prefix, suffix)
