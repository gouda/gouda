from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


from seacucumber.backend import SESBackend


class PostmatesBackend(SESBackend):
    def send_messages(self, email_messages):
        for email_message in email_messages:
            email_message.to = ['an.hoang@postmates.com' if r == 'images@postmates.com' else r for r in email_message.to]
        return super(PostmatesBackend, self).send_messages(email_messages)
