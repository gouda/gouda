# -*- coding: utf-8 -*-

from django_enumfield import enum


class MonthType(enum.Enum):
    JANUARY = 1
    FEBRUARY = 2
    MARCH = 3
    APRIL = 4
    MAY = 5
    JUNE = 6
    JULY = 7
    AUGUST = 8
    SEPTEMBER = 9
    OCTOBER = 10
    NOVEMBER = 11
    DECEMBER = 12

    labels = {
        JANUARY: 'January',
        FEBRUARY: 'February',
        MARCH: 'March',
        APRIL: 'April',
        MAY: 'May',
        JUNE: 'June',
        JULY: 'July',
        AUGUST: 'August',
        SEPTEMBER: 'September',
        OCTOBER: 'October',
        NOVEMBER: 'November',
        DECEMBER: 'December',
    }


class EscrowStatusType(enum.Enum):
    HOLD_PENDING = 0
    HELD = 1
    RELEASE_PENDING = 2
    RELEASED = 3
    REFUNDED = 4

    labels = {
        HOLD_PENDING: 'hold_pending',
        HELD: 'held',
        RELEASE_PENDING: 'release_pending',
        RELEASED: 'released',
        REFUNDED: 'refunded',
    }


class CurrencyType(enum.Enum):
    AED = 0
    ARS = 1
    AUD = 2
    BRL = 3
    CAD = 4
    CHF = 5
    CNY = 6
    CRC = 7
    CZK = 8
    DKK = 9
    EUR = 10
    GBP = 11
    HKD = 12
    HRK = 13
    HUF = 14
    IDR = 15
    ILS = 16
    INR = 17
    JPY = 18
    KRW = 19
    MAD = 20
    MXN = 21
    MYR = 22
    NOK = 23
    NZD = 24
    PEN = 25
    PHP = 26
    PLN = 27
    RON = 28
    RUB = 29
    SEK = 30
    SGD = 31
    THB = 32
    TRY = 33
    TWD = 34
    USD = 35
    VND = 36
    ZAR = 37

    symbols = {
        AED: u'د.إ',
        ARS: u'$',
        AUD: u'$',
        BRL: u'R$',
        CAD: u'$',
        CHF: u'CHF',
        CNY: u'¥',
        CRC: u'₡',
        CZK: u'Kč',
        DKK: u'kr',
        EUR: u'€',
        GBP: u'£',
        HKD: u'HK$',
        HRK: u'kn',
        HUF: u'Ft',
        IDR: u'Rp',
        ILS: u'₪',
        INR: u'₹',
        JPY: u'¥',
        KRW: u'₩',
        MAD: u'MAD',
        MXN: u'$',
        MYR: u'RM',
        NOK: u'kr',
        NZD: u'$',
        PEN: u'S/.',
        PHP: u'₱',
        PLN: u'zł',
        RON: u'lei',
        RUB: u'руб',
        SEK: u'kr',
        SGD: u'$',
        THB: u'฿',
        TRY: u'TRY',
        TWD: u'NT$',
        USD: u'$',
        VND: u'₫',
        ZAR: u'R'
    }