# -*- coding: utf-8 -*-

from __future__ import absolute_import, unicode_literals

from drf_haystack import filters


class HaystackFilter(filters.HaystackFilter):
    filter_aliases = None

    def get_request_filters(self, request):
        request_filters = super(HaystackFilter, self).get_request_filters(request)
        keys = request_filters.keys()
        if self.filter_aliases:
            for key in keys:
                if key in self.filter_aliases:
                    request_filters[self.filter_aliases.get(key)] = request_filters.pop(key)[0]
        return request_filters
