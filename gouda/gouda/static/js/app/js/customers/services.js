var customerServices = angular.module('gouda.customers.services', ['ngResource', 'gouda.services']);

customerServices.factory('StripeCouponResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/stripe_coupon/:id',
            {},
            {
                verify: {
                    method: 'GET',
                    url: '/api/stripe_coupon/:id/verify'
                }
            },
            {stripTrailingSlashes: false});
    }]
);

customerServices.factory('CouponResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/customers/coupon/:id',
            {},
            {
                verify: {
                    method: 'GET',
                    url: '/api/customers/coupon/:code/verify'
                }
            },
            {stripTrailingSlashes: false});
    }]
);

customerServices.factory('BraintreeClientTokenResource', ['$resource', 'constants',
    function($resource, constants) {
        return $resource(
            constants.braintreeClientTokenUrl,
            {},
            {},
            {stripTrailingSlashes: false});
    }]
);

customerServices.factory('StripeCustomerResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/stripe_customer/',
            {},
            {},
            {stripTrailingSlashes: false});
    }]
);