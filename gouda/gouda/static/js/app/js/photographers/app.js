var PhotographerApp = angular.module('gouda.photographers.apps.PhotographerApp', [
        'angularPayments',
        'ngMaterial',
        'ngSanitize',
        'ngAnimate',
        'ngTouch',
        'slick',
        'uiGmapgoogle-maps',
        'kendo.directives',
        'gouda.customers.services',
        'gouda.customers.controllers',
        'gouda.photographers.services',
        'gouda.photographers.filters',
        'gouda.photographers.controllers',
        'gouda.directives',
        'gouda.templates'
    ]
);

var PhotographerSearchResultsApp = angular.module('gouda.photographers.apps.PhotographerSearchResultsApp', [
        'ngMaterial',
        'ngSanitize',
        'ngAnimate',
        'ngTouch',
        'slick',
        'ui.router',
        'uiGmapgoogle-maps',
        'kendo.directives',
        'sprintf',
        'gouda.photographers.services',
        'gouda.photographers.filters',
        'gouda.photographers.controllers',
        'gouda.directives',
        'gouda.templates'
    ]
);

PhotographerApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

// Search results
PhotographerSearchResultsApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

PhotographerSearchResultsApp.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise(function($injector, $location) {
            return '/?query=drones&minPrice=0&offset=0';
        });

        $stateProvider.state('searchResults', {
            url: '/?query&condition&minPrice&maxPrice&offset',
            templateUrl: 'listings/search_results.html',
            controller: 'ListingSearchResultsController'
        });
    }
]);
