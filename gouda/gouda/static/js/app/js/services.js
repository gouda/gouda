var module = angular.module('gouda.services', []);

module.constant('constants', {
  braintreeClientTokenUrl: '/api/braintree/clienttoken/'
});
