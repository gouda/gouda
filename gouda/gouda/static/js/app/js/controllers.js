'use strict';

/* Controllers */

var customerControllers = angular.module('gouda.customers.controllers', []);

customerControllers.factory('checkoutFormService', function() {
    return {
        coupon: undefined,
        isCouponValid: function() {
            return angular.isDefined(this.coupon) && this.coupon.valid;
        }
    }
});


customerControllers.controller('StripeCheckoutFormController', ['$scope', '$attrs', '$interval', 'StripeCustomerResource', 'checkoutFormService',
    function($scope, $attrs, $interval, StripeCustomerResource, checkoutFormService) {
        $scope.coupon = undefined;
        $scope.didSubmit = false;
        $scope.didFail = false;
        $scope._isUploading = false;
        $scope.isUploading = function() {
            return $scope._isUploading;
        };
        $scope.isVerifyingCoupon = function() {
            return angular.isDefined(checkoutFormService.couponResourcePromise);
        };

        // Button
        $scope.isPurchaseButtonDisabled = function() {
            return $scope.isUploading() || !$scope.isFormValid() || $scope.isVerifyingCoupon();
        };

        // Form validation
        $scope.isFormValid = function() {
            return $scope.stripeForm.$valid;
        };

        // Handle discounts
        $scope.$watch(function(){return checkoutFormService;}, function(newVal){
            $scope.coupon = checkoutFormService.coupon;
        });
        $scope.isCouponValid = function() {
            return checkoutFormService.isCouponValid();
        };
        $scope.plan = angular.extend({}, angular.fromJson($attrs.plan));
        $scope.plan.getDollarAmount = function() {
            return parseFloat(this.amount) / 100.0;
        };
        $scope.getDiscountTotal = function() {
            var discountTotal = $scope.plan.getDollarAmount();
            if ($scope.isCouponValid()) {
                discountTotal -= discountTotal * checkoutFormService.coupon.percentOff / 100;
            }
            return discountTotal;
        };

        // Submit
        $scope.afterStripeSubmit = function(status, response) {
            function success(stripeCustomer, responseHeaders) {
                var location = responseHeaders('Location');
                $scope._isUploading = false;
                window.location = location;
            }
            function error(response) {
                $scope.didFail = true;
                $scope._isUploading = false;
            }
            if (status == 200) {
                var token = response;

                token.plan_id = $scope.plan.stripeId;
                token.coupon = checkoutFormService.coupon;
                if (token.coupon) {
                    token.coupon.$promise = undefined;
                    token.coupon.$$state = undefined;
                    token.coupon.$resolved = undefined;
                }

                // POST
                StripeCustomerResource.save(token, success, error);
            } else {
                error(null);
            }
        };
        $scope.submit = function() {
            $scope._isUploading = true;
            $scope.didSubmit = true;
            $scope.didFail = false;
            function clickStripeFormSubmitButton() {
                angular.element('#stripe-form-submit-button').trigger('click');
            }
            $interval(clickStripeFormSubmitButton, 0, 1, false);
        };
    }
]);

customerControllers.controller('StripeCouponFormController', ['$scope', 'checkoutFormService', 'StripeCouponResource',
    function($scope, checkoutFormService, StripeCouponResource) {
        $scope.isLoading = function() {
            return angular.isDefined(checkoutFormService.couponResourcePromise);
        };
        $scope.didSubmit = false;
        $scope.isFormValid = function() {
            return $scope.couponForm.$valid;
        };
        $scope.doShowMessage = function() {
            return $scope.didSubmit && !$scope.isLoading();
        };
        $scope.isCouponValid = function() {
            return checkoutFormService.isCouponValid();
        };
        $scope.getCoupon = function () {
            return checkoutFormService.coupon;
        };
        $scope.submit = function() {
            $scope.didSubmit = true;
            function success(coupon, responseHeaders) {
                checkoutFormService.couponResourcePromise = undefined;
                checkoutFormService.coupon = coupon;
            }
            function error(response) {
                checkoutFormService.couponResourcePromise = undefined;
                checkoutFormService.coupon = undefined;
            }
            checkoutFormService.couponResourcePromise = StripeCouponResource.verify({id: $scope.couponCode}, success, error);
        }
    }
]);
