import React from 'react';
import Axios from 'axios';
import ReactCssTransitionGroup from 'react-addons-css-transition-group';

class Image extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: props.image.comment,
            isCommentFormVisible: false
        };
        this.onDeleteImage = this.onDeleteImage.bind(this);
        this.onApproveImage = this.onApproveImage.bind(this);
        this.onRejectImage = this.onRejectImage.bind(this);
        this.onCommentChange = this.onCommentChange.bind(this);
        this.onSubmitComment = this.onSubmitComment.bind(this);
    }
    onCommentChange(element) {
        let state = Object.assign({}, this.state);
        state.comment = element.target.value;
        this.setState(state);
    }
    onSubmitComment(element) {
        let state = Object.assign({}, this.state);
        state.isCommentFormVisible = false;
        this.setState(state);
        this.props.onSubmitComment(this.props.image, this.state.comment);
    }
    onCloseCommentForm(element) {
        let state = Object.assign({}, this.state);
        state.isCommentFormVisible = false;
        this.setState(state);
    }
    onApproveImage(element) {
        this.props.onApproveImage(this.props.image);
    }
    onRejectImage(element) {
        this.props.onRejectImage(this.props.image);
    }
    onDeleteImage(element) {
        this.props.onDeleteImage(this.props.image);
    }
    onClickCommentButton(element) {
        let state = Object.assign({}, this.state);
        state.isCommentFormVisible = true;
        this.setState(state);
    }
    render() {
        let style = {
            backgroundImage: `url(${this.props.image.image.thumbnailUrl})`
        };
        if (this.props.image.isUploading) {
            return (
                <div className="col-sm-12 col-md-6 col-lg-4 image-col">
                    <div className="spinner">
                        <div className="circle-1"></div>
                        <div className="circle-2"></div>
                    </div>
                </div>
            );
        } else {
            let clientCommentForm = (
                <div className="comment">
                    <textarea value={this.state.comment} onChange={this.onCommentChange.bind(this)}/>
                    <button className="k-primary k-button transparent submit-button" onClick={this.onSubmitComment.bind(this)}>Submit</button>
                </div>
            );
            let photographerCommentForm = (
                <div className="comment">
                    <textarea readOnly={true} value={this.state.comment}/>
                    <button className="k-primary k-button transparent submit-button" onClick={this.onCloseCommentForm.bind(this)}>Close</button>
                </div>
            );
            let clientToolbar =  (
                <div className="toolbar">
                    <div className="icon-container">
                        <a href={this.props.image.image.url} target="_blank"><span className="fa fa-search icon"/></a>
                        <span className={this.props.image.didClientApprove ? "fa fa-thumbs-up icon green":"fa fa-thumbs-up icon"} onClick={this.onApproveImage.bind(this)}/>
                        <span className={this.props.image.didClientReject ? "fa fa-thumbs-down icon red":"fa fa-thumbs-down icon"} onClick={this.onRejectImage.bind(this)}/>
                        <span className={this.state.comment == '' ? "fa fa-comment-o icon":"fa fa-comment icon"} onClick={this.onClickCommentButton.bind(this)}/>
                    </div>
                </div>
            );
            let photographerToolbar =  (
                <div className="toolbar">
                    <div className="icon-container">
                        <a href={this.props.image.image.url} target="_blank"><span className="fa fa-search icon"/></a>
                        <span className={this.state.comment == '' ? "fa fa-comment-o icon":"fa fa-comment icon"} onClick={this.onClickCommentButton.bind(this)}/>
                        <span className="fa fa-trash icon" onClick={this.onDeleteImage.bind(this)}/>
                    </div>
                </div>
            );

            let toolbar = this.props.isClientMode ? clientToolbar : photographerToolbar;
            let commentForm = this.props.isClientMode ? clientCommentForm : photographerCommentForm;
            return (
                <div className="col-sm-12 col-md-6 col-lg-4 image-col">
                    <div className="image" style={style}>
                        {this.state.isCommentFormVisible ? commentForm : toolbar}
                    </div>
                </div>
            );
        }
    }
}

class ImageLoader extends React.Component {
    constructor(props) {
        super(props);
        this.state = {images: [], doShowSpinner: false};
        this.onSubmitComment = this.onSubmitComment.bind(this);
        this.onDeleteImage = this.onDeleteImage.bind(this);
        this.onApproveImage = this.onApproveImage.bind(this);
        this.onRejectImage = this.onRejectImage.bind(this);
    }
    componentDidMount() {
        this.getImages();
    }
    getImages() {
        let component = this;
        Axios.get(this.props.endpoint)
            .then(function (response) {
                component.setState({images: response.data.results});
            })
            .catch(function (response) {
                //console.log(response);
            });
    }
    loadImage(response) {
        let state = Object.assign({}, this.state);
        console.log(response);

        for (let i = 0; i < state.images.length; i++) {
            if (response.data.id == state.images[i].id) {
                state.images.splice(i, 1);
                break;
            }
        }
        state.images.push(response.data);
        this.setState(state);
    }
    setDoShowSpinner(doShowSpinner) {
        let state = Object.assign({}, this.state);
        state.doShowSpinner = doShowSpinner;
        this.setState(state);
    }
    onSubmitComment(image, comment) {
        let imageLoader = this;

        Axios.patch(image.url, {
                comment: comment,
            }, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function (response) {
                console.log(response);
                //console.log(imageLoader);
                imageLoader.getImages();
            })
            .catch(function (response) {
                //console.log(response);
            });
    }
    onApproveImage(image) {
        let imageLoader = this;

        Axios.patch(image.url, {
                didClientApprove: !image.didClientApprove,
                didClientReject: false
            }, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function (response) {
                //console.log(imageLoader);
                imageLoader.loadImage(response);
            })
            .catch(function (response) {
                //console.log(response);
            });
    }
    onRejectImage(image) {
        let imageLoader = this;

        Axios.patch(image.url, {
                didClientApprove: false,
                didClientReject: !image.didClientReject
            }, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function (response) {
                //console.log(imageLoader);
                imageLoader.loadImage(response);
            })
            .catch(function (response) {
                //console.log(response);
            });
    }
    onDeleteImage(image) {
        //console.log('Delete!!!');
        //console.log(image);
        // Find and delete
        let imageLoader = this;
        let images = Object.assign([], imageLoader.state.images);
        for (let i = 0; i < images.length; i++) {
            if (images[i].url == image.url) {
                images.splice(i, 1);
                break;
            }

        }
        imageLoader.setState({images: images});


        Axios.delete(image.url, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function (response) {
                //console.log(response);
                //console.log(imageLoader);
                imageLoader.getImages();
            })
            .catch(function (response) {
                //console.log(response);
            });
    }
    onFilesAdded(element) {
        //console.log(element.target.files);
        let imageLoader = this;
        let fileReader = new FileReader();
        let files = element.target.files;
        let i = 0;
        if (files.length <= 0) {
            return;
        }

        function processFile() {
            i += 1;
            if (i < files.length) {
                imageLoader.setDoShowSpinner(true);
                fileReader.readAsDataURL(files[i]);
            } else {
                imageLoader.setDoShowSpinner(false);
            }
        }

        fileReader.onload = function(upload) {
            // Post imageData
            let imageData = upload.target.result.split(',')[1].trim();
            Axios.post(imageLoader.props.endpoint, {
                    image: imageData,
                    name: files[i].name.replace(/\.[^/.]+$/, "")
                }, {
                    xsrfCookieName: 'csrftoken',
                    xsrfHeaderName: 'X-CSRFToken'
                })
                .then(function (response) {
                    console.log(response);
                    imageLoader.loadImage(response);
                    processFile();
                })
                .catch(function (response) {
                    processFile();
                });
        };
        fileReader.readAsDataURL(files[i]);
        imageLoader.setDoShowSpinner(true);
    }
    onUploadButtonClicked(element) {
        this.fileInput.click();
    }
    render() {
        let imageLoader = this;
        function imageComparator(a, b) {
            if (a.dateCreated > b.dateCreated) {
                return -1;
            } else if (a.dateCreated < b.dateCreated) {
                return 1;
            } else {
                return 0;
            }
        }
        let imageNodes = this.state.images.sort(imageComparator).map(function(image) {
            return (
                <Image key={image.id} isClientMode={imageLoader.props.isClientMode} onDeleteImage={imageLoader.onDeleteImage} onApproveImage={imageLoader.onApproveImage} onRejectImage={imageLoader.onRejectImage} onSubmitComment={imageLoader.onSubmitComment} image={image}/>
            );
        });
        let spinnerNode = (
            <div className="col-xs-12 col-lg-6">
                <div id="rectangle-spinner" className="rectangle-spinner">
                    <div className="rect1"></div>
                    <div className="rect2"></div>
                    <div className="rect3"></div>
                    <div className="rect4"></div>
                    <div className="rect5"></div>
                </div>
            </div>
        );
        let numImagesNode = (
                <div className="col-xs-12 col-lg-6">
                    <h4 className="text-right">{this.state.images.length} images</h4>
                </div>
        );
        let uploadButton = (
            <div className="row">
                <div className="col-xs-12 col-lg-6">
                    <input className="hidden" type="file" ref={(c) => this.fileInput = c}  accept="image/*" multiple onChange={this.onFilesAdded.bind(this)} />
                    <button onClick={this.onUploadButtonClicked.bind(this)} type="button" className="k-primary k-button"><span className="fa fa-upload"/> Upload photos</button>
                </div>
                {this.state.doShowSpinner ? spinnerNode:numImagesNode}
            </div>
        );
        return (
            <div className="image-loader">
                {this.props.isClientMode ? null:uploadButton}
                <div className="row images">
                    <ReactCssTransitionGroup transitionName="image-transition" transitionEnterTimeout={500} transitionLeaveTimeout={250}>
                        {imageNodes}
                    </ReactCssTransitionGroup>
                </div>
            </div>
        );
    }
}

ImageLoader.propTypes = {
    endpoint: React.PropTypes.string.isRequired
};

export default ImageLoader;