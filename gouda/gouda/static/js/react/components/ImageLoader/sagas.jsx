import Axios from 'axios';
import Thumbor from 'thumbor';
import UUID from 'uuid';
import _ from 'lodash';
import { takeLatest, takeEvery } from 'redux-saga';
import { eventChannel, END } from 'redux-saga';
import { call, put, select, cancel, take, actionChannel} from 'redux-saga/effects';
import { getAlbums, getThumborSettings } from './selectors.jsx';
import { getSpinnerIds } from './selectors.jsx';
import { albumComparator, dateCreatedComparator } from './utils.jsx';
import {
    addSpinnerId,
    deleteSpinnerId,

    deleteImageSuccess,
    deleteImageFailure,

    retrieveAlbumsSuccess,
    retrieveAlbumsFailure,

    createAlbumSuccess,
    createAlbumFailure,

    createSmartCroppedAlbumSuccess,
    createSmartCroppedAlbumFailure,

    deleteAlbumSuccess,
    deleteAlbumFailure,

    initializeSuccess,
    initializeFailure,

    createImageRequest,
    createImageSuccess,
    createImageFailure,

    uploadFileRequest,
    uploadFileSuccess,
    uploadFileFailure,

    commentImageSuccess,
    commentImageFailure,

    approveImageSuccess,
    approveImageFailure,

    rejectImageSuccess,
    rejectImageFailure,

    selectAlbumById,

    INITIALIZE_REQUEST,

    UPLOAD_FILE_REQUEST,

    REJECT_IMAGE_REQUEST,
    REJECT_IMAGE_SUCCESS,
    REJECT_IMAGE_FAILURE,
    APPROVE_IMAGE_REQUEST,
    APPROVE_IMAGE_SUCCESS,
    APPROVE_IMAGE_FAILURE,
    COMMENT_IMAGE_REQUEST,
    COMMENT_IMAGE_SUCCESS,
    COMMENT_IMAGE_FAILURE,
    DELETE_IMAGE_REQUEST,
    DELETE_IMAGE_SUCCESS,
    DELETE_IMAGE_FAILURE,
    CREATE_IMAGE_REQUEST,
    CREATE_IMAGE_SUCCESS,
    CREATE_IMAGE_FAILURE,
    CREATE_SMART_CROPPED_IMAGE_REQUEST,
    CREATE_SMART_CROPPED_IMAGE_SUCCESS,
    CREATE_SMART_CROPPED_IMAGE_FAILURE,
    RETRIEVE_IMAGES_REQUEST,
    RETRIEVE_IMAGES_SUCCESS,
    RETRIEVE_IMAGES_FAILURE,
    RETRIEVE_ALBUMS_REQUEST,
    RETRIEVE_ALBUMS_SUCCESS,
    RETRIEVE_ALBUMS_FAILURE,
    RETRIEVE_ALBUM_REQUEST,
    RETRIEVE_ALBUM_SUCCESS,
    RETRIEVE_ALBUM_FAILURE,
    CREATE_ALBUM_REQUEST,
    CREATE_ALBUM_SUCCESS,
    CREATE_ALBUM_FAILURE,
    CREATE_SMART_CROPPED_ALBUM_REQUEST,
    DELETE_ALBUM_REQUEST,
    DELETE_ALBUM_SUCCESS,
    DELETE_ALBUM_FAILURE,
    SELECT_ALBUM_BY_ID,
    SHOW_SPINNER} from './actions.jsx';
import * as actions from './actions.jsx';

function readFileAsDataUrl(file) {
    return eventChannel(function (emitter) {
        let fileReader = new FileReader();
        fileReader.onload = function(event) {
            emitter(event.target.result);
            emitter(END);
        };
        fileReader.readAsDataURL(file);
        return function() {};
    });
}

function* selectLatestAlbum() {
    const albums = yield select(getAlbums);
    if (!_.isNil(albums) && albums.size > 0) {
        let latestAlbum = albums.sort(dateCreatedComparator).get(-1);
        if (!_.isNil(latestAlbum)) {
            yield put(selectAlbumById(latestAlbum.id));
        }
    }
}

function* updateAlbum(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));
    try {
        let url = action.payload.url;
        const response = yield call(Axios.patch, url, action.payload.data, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });

        // Success!
        yield put(actions.updateAlbumSuccess(response.data));
    } catch (error) {
        yield put(actions.updateAlbumFailure(error));
    }
    yield put(deleteSpinnerId(id));
}

function* deleteAlbum(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));
    try {
        let url = action.payload.album.url;
        const response = yield call(Axios.delete, url, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });

        // Success!
        yield put(deleteAlbumSuccess(url));

        // Select latest album
        yield call(selectLatestAlbum);
    } catch (error) {
        yield put(deleteAlbumFailure(error));
    }
    yield put(deleteSpinnerId(id));
}

function* createAlbum(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));
    try {
        let url = action.payload.url;
        let payload = {
            name: action.payload.album.name,
            width: action.payload.album.width,
            height: action.payload.album.height
        };
        const response = yield call(Axios.post, url, payload, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });

        // Success!
        yield put(createAlbumSuccess(response.data));

        // Select latest album
        yield call(selectLatestAlbum);
    } catch (error) {
        yield put(createAlbumFailure(error));
    }
    yield put(deleteSpinnerId(id));
}

function* createSmartCroppedAlbum(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));
    try {
        let url = action.payload.url;
        let payload = {
            name: action.payload.album.name,
            width: action.payload.album.width,
            height: action.payload.album.height
        };
        let response = yield call(Axios.post, url, payload, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });
        const smartCroppedAlbum = response.data;

        // Success!
        yield put(createSmartCroppedAlbumSuccess(smartCroppedAlbum));

        // Select latest album
        yield call(selectLatestAlbum);

        const thumborSettings = yield select(getThumborSettings);
        let thumbor  = new Thumbor(thumborSettings.key, thumborSettings.url);
        for (let i = 0; i < action.payload.sourceAlbum.images.size; i += 1) {
            let sourceImage = action.payload.sourceAlbum.images.get(i);
            let smartCroppedImageUrl = thumbor.setImagePath(sourceImage.image.url).resize(
                action.payload.album.width,
                action.payload.album.height).smartCrop(true).buildUrl();
            console.log(smartCroppedImageUrl);
            response = yield call(Axios.get, smartCroppedImageUrl, {
                responseType: 'blob'
            });
            let channel = yield call(readFileAsDataUrl, response.data);
            let dataUrl = yield take(channel);
            let data = dataUrl.split(',')[1].trim();
            yield call(createImage, createImageRequest(smartCroppedAlbum, sourceImage.name, data));
        }
    } catch (error) {
        yield put(createSmartCroppedAlbumFailure(error));
    }
    yield put(deleteSpinnerId(id));
}

function* retrieveAlbums(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));
    try {
        const response = yield call(Axios.get, action.payload.url);
        yield put(retrieveAlbumsSuccess(response.data.results));
    } catch (error) {
        yield put(retrieveAlbumsFailure(error));
    }
    yield put(deleteSpinnerId(id));
}

function* initialize(action) {
    try {
        // Retrieve albums from server
        yield call(retrieveAlbums, action);

        // Sort albums and select one
        const albums = yield select(getAlbums);
        if (!_.isNil(albums) && albums.size > 0) {
            yield put(selectAlbumById(albums.sort(albumComparator).get(0).id));
        }

        // Success!
        yield put(initializeSuccess());
    } catch (error) {
        yield put(initializeFailure());
    }
}

function* commentImage(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));

    let url = action.payload.image.url;
    let payload = {
        comment: action.payload.image.comment
    };
    try {
        const response = yield call(Axios.patch, url, payload, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });
        yield put(commentImageSuccess(response.data));
    } catch (error) {
        yield put(commentImageFailure(error));
    }

    yield put(deleteSpinnerId(id));
}

function* approveImage(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));

    let url = action.payload.image.url;
    let payload = {
        didClientApprove: action.payload.image.didClientApprove,
        didClientReject: false
    };
    try {
        const response = yield call(Axios.patch, url, payload, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });
        yield put(approveImageSuccess(response.data));
    } catch (error) {
        yield put(approveImageFailure(error));
    }

    yield put(deleteSpinnerId(id));
}

function* rejectImage(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));

    let url = action.payload.image.url;
    let payload = {
        didClientApprove: false,
        didClientReject: action.payload.image.didClientReject
    };
    try {
        const response = yield call(Axios.patch, url, payload, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });
        yield put(rejectImageSuccess(response.data));
    } catch (error) {
        yield put(rejectImageFailure(error));
    }

    yield put(deleteSpinnerId(id));
}


function* createImage(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));

    try {
        let url = action.payload.album.url;
        let payload = {
            images: action.payload.album.images
        };
        const response = yield call(Axios.patch, url, payload, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });
        yield put(createImageSuccess(response.data));
    } catch (error) {
        yield put(createImageFailure(error));
    }

    yield put(deleteSpinnerId(id));
}

function* deleteImage(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));

    try {
        let url = action.payload.image.url;
        const response = yield call(Axios.delete, url, {
            xsrfCookieName: 'csrftoken',
            xsrfHeaderName: 'X-CSRFToken'
        });

        // Success!
        yield put(deleteImageSuccess(url));
    } catch (error) {
        yield put(deleteImageFailure(error));
    }

    yield put(deleteSpinnerId(id));
}


function* uploadFile(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));

    try {
        let file = action.payload.file;
        let name = file.name.replace(/\.[^/.]+$/, "");
        let channel = yield call(readFileAsDataUrl, file);
        let dataUrl = yield take(channel);
        let data = dataUrl.split(',')[1].trim();
        yield call(createImage, createImageRequest(action.payload.album, name, data));
        yield put(uploadFileSuccess());
    } catch (error) {
        yield put(uploadFileFailure());
    }

    yield put(deleteSpinnerId(id));
}

export function* retrieveImageMetadata(action) {
    const id = UUID.v1();
    yield put(addSpinnerId(id));
    try {
        const response = yield call(Axios.get, action.payload.url);
        yield put(actions.retrieveImageMetadataSuccess(action.payload.id, response.data.thumbor));
    } catch (error) {
        yield put(actions.retrieveImageMetadataFailure(action.payload.id, error));
    }
    yield put(deleteSpinnerId(id));
}

export function* watchUploadFileRequest() {
    let channel = yield actionChannel(UPLOAD_FILE_REQUEST);
    while (true) {
        let action = yield take(channel);
        yield call(uploadFile, action);
    }
}

export function* watchCreateImageRequest() {
    let channel = yield actionChannel(CREATE_IMAGE_REQUEST);
    while (true) {
        let action = yield take(channel);
        yield call(createImage, action);
    }
}

export function* watchCreateAlbumRequest() {
    yield* takeEvery(CREATE_ALBUM_REQUEST, createAlbum);
}

export function* watchCreateSmartCroppedAlbumRequest() {
    yield* takeEvery(CREATE_SMART_CROPPED_ALBUM_REQUEST, createSmartCroppedAlbum);
}

export function* watchDeleteAlbumRequest() {
    yield* takeEvery(DELETE_ALBUM_REQUEST, deleteAlbum);
}

export function* watchRetrieveAlbumsRequest() {
    yield* takeLatest(RETRIEVE_ALBUMS_REQUEST, retrieveAlbums);
}

export function* watchInitializeRequest() {
    yield* takeLatest(INITIALIZE_REQUEST, initialize);
}

export function* watchDeleteImageRequest() {
    yield* takeEvery(DELETE_IMAGE_REQUEST, deleteImage);
}

export function* watchCommentImageRequest() {
    yield* takeEvery(COMMENT_IMAGE_REQUEST, commentImage);
}

export function* watchApproveImageRequest() {
    yield* takeEvery(APPROVE_IMAGE_REQUEST, approveImage);
}

export function* watchRejectImageRequest() {
    yield* takeEvery(REJECT_IMAGE_REQUEST, rejectImage);
}

export function* watchRetrieveImageMetadataRequest() {
    yield* takeEvery(actions.RETRIEVE_IMAGE_METADATA_REQUEST, retrieveImageMetadata);
}

export function* watchUpdateAlbumRequest() {
    yield* takeEvery(actions.UPDATE_ALBUM_REQUEST, updateAlbum);
}
