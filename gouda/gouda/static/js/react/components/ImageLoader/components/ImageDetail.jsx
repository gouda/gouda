import React from 'react';
import ReactCssTransitionAlbum from 'react-addons-css-transition-group';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Image from './Image.jsx';
import _ from 'lodash';

class CommentForm extends React.Component {
    static propTypes = {
        image: React.PropTypes.object,
        commentImage: React.PropTypes.func.isRequired,
        isClientMode: React.PropTypes.bool.isRequired,
    };

    constructor(props) {
        super(props);
        this.state = {
            comment: props.image.comment
        }
    }
    componentWillReceiveProps(nextProps) {
        this.setState({
            comment: nextProps.image.comment
        });
    }
    onCommentChange(element) {
        let state = Object.assign({}, this.state);
        state.comment = element.target.value;
        this.setState(state);
    }
    onSubmitComment(element) {
        let state = Object.assign({}, this.state);
        this.setState(state);
        this.props.commentImage(this.props.image.url, this.state.comment);
    }
    render() {
        return (
            <div className="row comment-form">
                <div className="col-xs-12">
                    <label>Comment</label>
                    <textarea readOnly={!this.props.isClientMode} className="k-textbox" value={this.state.comment} onChange={this.onCommentChange.bind(this)}/>
                </div>
                {this.props.isClientMode ? (
                    <div className="col-xs-offset-7 col-xs-5">
                        <button className="k-primary k-button blue" onClick={this.onSubmitComment.bind(this)}>Submit</button>
                    </div>
                ) : null}
            </div>
        )
    }
}

class ImageDetail extends React.Component {
    static propTypes = {
        image: React.PropTypes.object,
        metadata: React.PropTypes.object,
        isClientMode: React.PropTypes.bool.isRequired,
        placeholderUrl: React.PropTypes.string.isRequired,
        doShow: React.PropTypes.bool.isRequired,
        doShowSpinner: React.PropTypes.bool.isRequired,

        index: React.PropTypes.number,
        numImages: React.PropTypes.number,

        selectNextImage: React.PropTypes.func.isRequired,
        selectPreviousImage: React.PropTypes.func.isRequired,
        hideImageDetail: React.PropTypes.func.isRequired,

        addSpinnerId: React.PropTypes.func.isRequired,
        deleteSpinnerId: React.PropTypes.func.isRequired,

        deleteImage: React.PropTypes.func.isRequired,
        approveImage: React.PropTypes.func.isRequired,
        rejectImage: React.PropTypes.func.isRequired,
        commentImage: React.PropTypes.func.isRequired,
        retrieveImageMetadata: React.PropTypes.func.isRequired,
    };

    constructor(props) {
        super(props);
        this.refs = {};
        this._show.bind(this);
        this._hide.bind(this);
        this.onSelectNextImage.bind(this);
        this.onSelectPreviousImage.bind(this);
        this.onKeyDown = this.onKeyDown.bind(this);
    }
    componentDidMount() {
        let component = this;
        $(this.modal).on('hidden.bs.modal', function () {
            component.props.hideImageDetail();
            $(document).off('keydown', component.onKeyDown);
        });
        $(this.modal).on('shown.bs.modal', function () {
            $(document).keydown(component.onKeyDown);
        });
    }
    componentWillReceiveProps(nextProps) {
        if (!_.isEqual(this.props.image, nextProps.image) && !_.isNil(nextProps.image)) {
            nextProps.retrieveImageMetadata(nextProps.image.id, nextProps.image.image.metadataUrl);
        }
    }
    componentDidUpdate(prevProps, prevState) {
        if (this.props.doShow) {
            this._show();
        } else {
            this._hide();
        }
    }

    _show() {
        $(this.modal).modal('show');
    }
    _hide() {
        $(this.modal).modal('hide');
    }
    onSelectNextImage(element) {
        this.props.selectNextImage();
    }
    onSelectPreviousImage(element) {
        this.props.selectPreviousImage();
    }
    onApproveImage(element) {
        this.props.approveImage(this.props.image.url, !this.props.image.didClientApprove);
    }
    onRejectImage(element) {
        this.props.rejectImage(this.props.image.url, !this.props.image.didClientReject);
    }
    onDeleteImage(element) {
        this.props.deleteImage(this.props.image.url);
    }
    onKeyDown(event) {
        switch(event.keyCode) {
            case 39: // Right arrow
                this.props.selectNextImage();
                break;
            case 37: // Left arrow
                this.props.selectPreviousImage();
                break;
            case 27: // Esc
                this._hide();
                break;
        }
    }
    //componentDidMount() {
    //    $('#image-detail-modal').modal('show');
    //    console.log('SHOW!!?!?!?');
    //}
    render() {
        let style = {
            backgroundImage: _.isNil(this.props.image) ? null : `url(${this.props.image.image.url})`
        };
        let clientToolbar = _.isNil(this.props.image) ? null : (
            <div className="row">
                <div className="col-xs-12 toolbar">
                    <span className="link" onClick={this.onApproveImage.bind(this)}><span className={this.props.image.didClientApprove ? "fa fa-thumbs-up icon green":"fa fa-thumbs-up icon"}/> Approve</span>
                    <span className="link" onClick={this.onRejectImage.bind(this)}><span className={this.props.image.didClientReject ? "fa fa-thumbs-down icon red":"fa fa-thumbs-down icon"}/> Reject</span>
                </div>
            </div>
        );
        let photographerToolbar = _.isNil(this.props.image) ? null : (
            <div className="row">
                <div className="col-xs-12 toolbar">
                    <p>
                        <span className="link"><span className={this.props.image.didClientApprove ? "fa fa-thumbs-up icon green":"fa fa-thumbs-up icon"}/></span>
                        <span className="link"><span className={this.props.image.didClientReject ? "fa fa-thumbs-down icon red":"fa fa-thumbs-down icon"}/></span>
                    </p>
                </div>
            </div>
        );
        let spinner = this.props.doShowSpinner ? (
            <div className="col-xs-3">
                <div className="rectangle-spinner">
                    <div className="rect1"></div>
                    <div className="rect2"></div>
                    <div className="rect3"></div>
                    <div className="rect4"></div>
                    <div className="rect5"></div>
                </div>
            </div>
        ) : null;
        let toolbar = this.props.isClientMode ? clientToolbar : photographerToolbar;
        let modalContent = _.isNil(this.props.image) ? null : (
            <div className="modal-content">
                <div className="modal-body">
                    <div className="row">
                        <div className="col-xs-9 img-col" style={style}>
                            <div className="chevron-container">
                                <span className="fa fa-chevron-left" onClick={this.onSelectPreviousImage.bind(this)}/>
                                <span className="fa fa-chevron-right pull-right" onClick={this.onSelectNextImage.bind(this)}/>
                            </div>
                            <div className="index-container">
                                <span>{this.props.index + 1} of {this.props.numImages}</span>
                            </div>
                        </div>
                        <div className="col-xs-3 sidebar-col">
                            <div className="row">
                                <div className="col-xs-9">
                                    <p className="image-name">{this.props.image.name}</p>
                                </div>
                                {spinner}
                            </div>
                            <div className="row">
                                <div className="col-xs-12">
                                    {_.isNil(this.props.metadata) ? null:(<p>{this.props.metadata.source.width} x {this.props.metadata.source.height}</p>)}
                                </div>
                            </div>
                            {toolbar}
                            <hr/>
                            <CommentForm image={this.props.image} commentImage={this.props.commentImage} isClientMode={this.props.isClientMode} />
                        </div>
                    </div>
                </div>
            </div>
        );
        return (
            <div id="image-detail-modal" className="modal fade image-detail-modal" role="dialog" ref={function(c) {this.modal = c}.bind(this)}>
                <div className="modal-dialog">
                    {modalContent}
                </div>
            </div>
        );
    }
}

export default ImageDetail;