import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Immutable from 'immutable';

class DownloadButton extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        let spinnerNode = (
            <div className="col-xs-12 col-lg-6">
                <div id="rectangle-spinner" className="rectangle-spinner">
                    <div className="rect1"></div>
                    <div className="rect2"></div>
                    <div className="rect3"></div>
                    <div className="rect4"></div>
                    <div className="rect5"></div>
                </div>
            </div>
        );
        let numImagesNode = (
            <div className="col-xs-12 col-lg-6">
                <h4 className="text-right">{this.props.images.size} photos</h4>
            </div>
        );

        return (
            <div className="row">
                <div className="col-xs-12 col-lg-6">
                    <a href={this.props.downloadUrl}><button type="button" className="k-primary k-button blue"><span className="fa fa-download"></span> Download</button></a>
                </div>
                {this.props.doShowSpinner ? spinnerNode:numImagesNode}
            </div>
        );
    }
}

DownloadButton.propTypes = {
    images: ImmutablePropTypes.list.isRequired,
    doShowSpinner: React.PropTypes.bool.isRequired,
    downloadUrl: React.PropTypes.string.isRequired
};

export default DownloadButton;