import React from 'react';
import ReactCssTransitionAlbum from 'react-addons-css-transition-group';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Image from './Image.jsx';

class ImageList extends React.Component {
    static propTypes = {
        images: ImmutablePropTypes.list.isRequired,
        isClientMode: React.PropTypes.bool.isRequired,
        placeholderUrl: React.PropTypes.string.isRequired,

        limit: React.PropTypes.number.isRequired,
        offset: React.PropTypes.number.isRequired,
        incrementOffset: React.PropTypes.func.isRequired,
        decrementOffset: React.PropTypes.func.isRequired,

        addSpinnerId: React.PropTypes.func.isRequired,
        deleteSpinnerId: React.PropTypes.func.isRequired,

        showImageDetail: React.PropTypes.func.isRequired,
        deleteImage: React.PropTypes.func.isRequired,
        approveImage: React.PropTypes.func.isRequired,
        rejectImage: React.PropTypes.func.isRequired,
        commentImage: React.PropTypes.func.isRequired
    };
    static defaultProps = {
        limit: 16
    };

    onNextPage(element) {
        this.props.incrementOffset(this.props.limit);
    }
    onPreviousPage(element) {
        this.props.decrementOffset(this.props.limit);
    }
    render() {
        let limit = this.props.limit;
        let offset = this.props.offset;
        let imageList = this;
        function imageComparator(a, b) {
            if (a.dateCreated > b.dateCreated) {
                return -1;
            } else if (a.dateCreated < b.dateCreated) {
                return 1;
            } else {
                return 0;
            }
        }
        let images = this.props.images.sort(imageComparator).slice(
            offset,
            offset + limit
        );
        let imageNodes = images.map(function(image) {
            return (
                <Image
                    key={image.id}
                    isClientMode={imageList.props.isClientMode}
                    showImageDetail={imageList.props.showImageDetail}
                    deleteImage={imageList.props.deleteImage}
                    approveImage={imageList.props.approveImage}
                    rejectImage={imageList.props.rejectImage}
                    commentImage={imageList.props.commentImage}
                    placeholderUrl={imageList.props.placeholderUrl}
                    addSpinnerId={imageList.props.addSpinnerId}
                    deleteSpinnerId={imageList.props.deleteSpinnerId}
                    image={image}/>
            );
        });
        let previousButton = (offset - limit >= 0) ? (
            <li>
                <a href="#image-loader-card" className="previous fa fa-chevron-left" ariaLabel="Previous" onClick={this.onPreviousPage.bind(this)}></a>
            </li>
        ) : null;
        let nextButton = ((limit + offset) < this.props.images.size) ? (
            <li>
                <a href="#image-loader-card" className="next fa fa-chevron-right" ariaLabel="Next" onClick={this.onNextPage.bind(this)}></a>
            </li>
        ) : null;
        return (
            <div className="row images">
                <ReactCssTransitionAlbum transitionName="image-transition" transitionEnterTimeout={500} transitionLeaveTimeout={250}>
                    {imageNodes}
                </ReactCssTransitionAlbum>
                <div className="col-xs-12">
                    <div className="pagination-container">
                        <ul className="pagination">
                            {previousButton}
                            {nextButton}
                        </ul>
                        <p className="pagination-info regular-font">
                            {offset + 1}-{Math.min(this.props.images.size, offset + limit)} of {this.props.images.size}
                        </p>
                    </div>
                </div>
            </div>
        );
    }
}

export default ImageList;