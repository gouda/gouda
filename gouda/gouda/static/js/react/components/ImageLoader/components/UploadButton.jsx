import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Immutable from 'immutable';
import _ from 'lodash';

class UploadButton extends React.Component {
    constructor(props) {
        super(props);
    }

    onUploadButtonClicked(element) {
        this.fileInput.click();
    }

    onFilesAdded(element) {
        let files = element.target.files;
        for(let i = 0; i < files.length; i++) {
            this.props.uploadFile(this.props.album, files[i]);
        }
    }

    render() {
        let spinnerNode = (
            <div className="col-xs-12 col-lg-6">
                <div id="rectangle-spinner" className="rectangle-spinner">
                    <div className="rect1"></div>
                    <div className="rect2"></div>
                    <div className="rect3"></div>
                    <div className="rect4"></div>
                    <div className="rect5"></div>
                </div>
            </div>
        );
        let numImagesNode = (
            <div className="col-xs-12 col-lg-6">
                <h4 className="text-right">{!_.isNil(this.props.album) ? this.props.album.images.size:0} photos</h4>
            </div>
        );

        return (
            <div className="row">
                <div className="col-xs-12 col-lg-6">
                    <input className="hidden" type="file" ref={(c) => this.fileInput = c}  accept="image/*" multiple onChange={this.onFilesAdded.bind(this)} />
                    <button disabled={!this.props.isEnabled ? "disabled" : false} onClick={this.onUploadButtonClicked.bind(this)} type="button" className="k-primary k-button blue"><span className="fa fa-upload"/> Upload photos</button>
                </div>
                {this.props.doShowSpinner ? spinnerNode:numImagesNode}
            </div>
        );
    }
}

UploadButton.propTypes = {
    album: React.PropTypes.object,
    uploadFile: React.PropTypes.func.isRequired,
    createImage: React.PropTypes.func.isRequired,
    isEnabled: React.PropTypes.bool.isRequired,
    doShowSpinner: React.PropTypes.bool.isRequired
};

export default UploadButton;