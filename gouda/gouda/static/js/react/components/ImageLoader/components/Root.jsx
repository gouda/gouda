import React from 'react';
import { connect } from 'react-redux';
import ImageListContainer from '../containers/ImageListContainer.jsx';
import ImageDetailContainer from '../containers/ImageDetailContainer.jsx';
import UploadButtonContainer from '../containers/UploadButtonContainer.jsx';
import DownloadButtonContainer from '../containers/DownloadButtonContainer.jsx';
import AlbumSelectContainer from '../containers/AlbumSelectContainer.jsx';
import { initializeRequest } from '../actions.jsx'
import { retrieveAlbumsRequest } from '../actions.jsx'
import { selectAlbumById } from '../actions.jsx'
import ImmutablePropTypes from 'react-immutable-proptypes';
import { albumComparator } from '../utils.jsx';


class Root extends React.Component {
    componentDidMount() {
        let component = this;
        let promise = this.props.dispatch(initializeRequest(this.props.endpoint));
        //promise.then(function() {
        //    if (component.props.albums.size > 0) {
        //        component.props.dispatch(selectAlbumById(component.props.albums.sort(albumComparator).get(0).id));
        //    }
        //});
    }
    render() {
        return (
            <div className="image-loader">
                <AlbumSelectContainer />
                <hr/>
                {this.props.isClientMode ? (<DownloadButtonContainer />) : (<UploadButtonContainer />)}
                <hr/>
                <ImageDetailContainer />
                <ImageListContainer />
            </div>
        );
    }
}

const mapStateToProps = function(state) {
    return {
        endpoint: state.endpoint,
        isClientMode: state.isClientMode,
        albums: state.albums
    }
};

Root.propTypes = {
    endpoint: React.PropTypes.string.isRequired,
    dispatch: React.PropTypes.func.isRequired,
    albums: ImmutablePropTypes.list.isRequired
};

export default connect(mapStateToProps)(Root)
