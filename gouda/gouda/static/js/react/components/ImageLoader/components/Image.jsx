import React from 'react';
import Axios from 'axios';
import _ from 'lodash';
import UUID from 'uuid';

class Image extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: props.image.comment,
            isCommentFormVisible: false,
            metadata: undefined,
            style: {
                opacity: 0
            }
        };
        this.onDeleteImage = this.onDeleteImage.bind(this);
        this.onApproveImage = this.onApproveImage.bind(this);
        this.onRejectImage = this.onRejectImage.bind(this);
        this.onCommentChange = this.onCommentChange.bind(this);
        this.onSubmitComment = this.onSubmitComment.bind(this);
    }
    componentDidMount() {
        let component = this;
        const id = UUID.v1();
        this.props.addSpinnerId(id);
        function hideSpinner() {
            component.props.deleteSpinnerId(id);
        }
        Axios.get(this.props.image.image.metadataUrl)
            .then(function (response) {
                // We found valid metadata...?!?!
                let state = Object.assign({}, component.state);
                state.metadata = response.data.thumbor;

                Axios.get(component.props.image.image.thumbnailUrl, {
                        responseType: 'blob'
                    })
                    .then(function (response) {
                        let fileReader = new FileReader();
                        fileReader.onload = function (event) {
                            let dataUri = event.target.result;
                            state.style = {
                                backgroundImage: `url(${dataUri})`,
                                opacity: 1.0
                            };
                            component.setState(state);
                            hideSpinner();
                        };
                        fileReader.readAsDataURL(response.data);
                    })
                    .catch(function (response) {hideSpinner();});

            })
            .catch(function (response) {
                // Use placeholder when nothing is found...
                let state = Object.assign({}, component.state);
                state.style = {
                    backgroundImage: `url(${component.props.placeholderUrl})`
                };
                component.setState(state);
                hideSpinner();
            });
    }
    onCommentChange(element) {
        let state = Object.assign({}, this.state);
        state.comment = element.target.value;
        this.setState(state);
    }
    onSubmitComment(element) {
        let state = Object.assign({}, this.state);
        state.isCommentFormVisible = false;
        this.setState(state);
        this.props.commentImage(this.props.image.url, this.state.comment);
    }
    onCloseCommentForm(element) {
        let state = Object.assign({}, this.state);
        state.isCommentFormVisible = false;
        this.setState(state);
    }
    onApproveImage(element) {
        this.props.approveImage(this.props.image.url, !this.props.image.didClientApprove);
    }
    onRejectImage(element) {
        this.props.rejectImage(this.props.image.url, !this.props.image.didClientReject);
    }
    onDeleteImage(element) {
        this.props.deleteImage(this.props.image.url);
    }
    onShowImageDetail(element) {
        this.props.showImageDetail(this.props.image.id);
    }
    onClickCommentButton(element) {
        let state = Object.assign({}, this.state);
        state.isCommentFormVisible = true;
        this.setState(state);
    }
    render() {
        //let style = {
        //    backgroundImage: `url(${this.props.image.image.thumbnailUrl})`
        //};

        let clientCommentForm = (
            <div className="comment">
                <textarea value={this.state.comment} onChange={this.onCommentChange.bind(this)}/>
                <button className="k-primary k-button transparent submit-button" onClick={this.onSubmitComment.bind(this)}>Submit</button>
            </div>
        );
        let photographerCommentForm = (
            <div className="comment">
                <textarea readOnly={true} value={this.state.comment}/>
                <button className="k-primary k-button transparent submit-button" onClick={this.onCloseCommentForm.bind(this)}>Close</button>
            </div>
        );
        let clientToolbar =  (
            <div className="toolbar">
                <div className="icon-container">
                    <span className="fa fa-search icon" onClick={this.onShowImageDetail.bind(this)}/>
                    <span className={this.props.image.didClientApprove ? "fa fa-thumbs-up icon green":"fa fa-thumbs-up icon"} onClick={this.onApproveImage.bind(this)}/>
                    <span className={this.props.image.didClientReject ? "fa fa-thumbs-down icon red":"fa fa-thumbs-down icon"} onClick={this.onRejectImage.bind(this)}/>
                    <span className={this.state.comment == '' ? "fa fa-comment-o icon":"fa fa-comment icon"} onClick={this.onClickCommentButton.bind(this)}/>
                </div>
            </div>
        );
        let photographerToolbar =  (
            <div className="toolbar">
                <div className="icon-container">
                    <span className="fa fa-search icon" onClick={this.onShowImageDetail.bind(this)}/>
                    <span className={this.state.comment == '' ? "fa fa-comment-o icon":"fa fa-comment icon"} onClick={this.onClickCommentButton.bind(this)}/>
                    <span className="fa fa-trash icon" onClick={this.onDeleteImage.bind(this)}/>
                </div>
            </div>
        );

        let toolbar = this.props.isClientMode ? clientToolbar : photographerToolbar;
        let commentForm = this.props.isClientMode ? clientCommentForm : photographerCommentForm;
        return (
            <div className="col-xs-6 col-sm-3 image-col">
                <div className="image" style={this.state.style}>
                    {this.state.isCommentFormVisible ? commentForm : toolbar}
                </div>
                <div className="metadata">
                    <p className="text-center text-muted name">{this.props.image.name}</p>
                    {_.isNil(this.state.metadata) ? null:(<p className="text-center">{this.state.metadata.source.width} x {this.state.metadata.source.height}</p>)}
                </div>
            </div>
        );
    }
}

Image.propTypes = {
    image: React.PropTypes.object.isRequired,
    placeholderUrl: React.PropTypes.string.isRequired,
    isClientMode: React.PropTypes.bool.isRequired,
    
    showImageDetail: React.PropTypes.func.isRequired,
    deleteImage: React.PropTypes.func.isRequired,
    approveImage: React.PropTypes.func.isRequired,
    rejectImage: React.PropTypes.func.isRequired,
    commentImage: React.PropTypes.func.isRequired,
    
    addSpinnerId: React.PropTypes.func.isRequired,
    deleteSpinnerId: React.PropTypes.func.isRequired,
};

export default Image;