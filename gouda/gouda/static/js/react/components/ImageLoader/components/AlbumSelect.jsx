import React from 'react';
import ImmutablePropTypes from 'react-immutable-proptypes';
import Immutable from 'immutable';
import _ from 'lodash';
import { albumComparator } from '../utils.jsx';
import UUID from 'uuid';


class UpdateAlbumModal extends React.Component {
    static propTypes = {
        getSelectedAlbum: React.PropTypes.func.isRequired,
        updateAlbum: React.PropTypes.func.isRequired
    };
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            isNameValid: true
        }
    }
    componentWillReceiveProps(nextProps) {
        let album = nextProps.getSelectedAlbum();
        this.setState({
            name: _.isNil(album) ? '':album.name,
            isNameValid: true
        });
    }
    resetState() {
        let album = this.props.getSelectedAlbum();
        this.setState({
            name: _.isNil(album) ? '':album.name,
            isNameValid: true
        });
    }
    onNameChange(element) {
        let state = Object.assign({}, this.state);
        state.name = element.target.value;
        this.setState(state);
    }
    onEditAlbum(element) {
        // Validate form
        let album = this.props.getSelectedAlbum();
        let state = Object.assign({}, this.state);
        state.isNameValid = _.isString(state.name) && state.name != '';
        this.setState(state);
        if (!state.isNameValid) {
            return;
        }
        $('#update-album-modal').modal('toggle');
        this.props.updateAlbum(album.url, {name: state.name});
        this.resetState();
    }
    render() {
        return (
            <div id="update-album-modal" className="modal fade" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal"><span className="fa fa-times"></span></button>
                            <h4 className="modal-title">Edit this album</h4>
                        </div>
                        <form role="form" name="form">
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <label>Name</label>
                                        <input maxLength="250" value={this.state.name} onChange={this.onNameChange.bind(this)} className={"k-textbox" + (this.state.isNameValid ? "":" parsley-error")}/>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-xs-12 col-md-offset-8 col-md-4">
                                        <button onClick={this.onEditAlbum.bind(this)} type="button" className="k-primary k-button green">
                                            Edit album
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

class CreateAlbumModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            width: '',
            height: '',

            isNameValid: true,
            isWidthValid: true,
            isHeightValid: true,

            doUseSmartCropping: false,
            sourceAlbumId: undefined
        };
        this.resetState.bind(this);
    }

    resetState() {
        let state = Object.assign({}, this.state);
        state.name = '';
        state.width = '';
        state.height = '';
        state.isNameValid = true;
        state.isWidthValid = true;
        state.isHeightValid = true;
        state.doUseSmartCropping = false;
        state.sourceAlbumId = undefined;
        this.setState(state);
    }

    componentWillReceiveProps(nextProps) {
        // Update source album id...?
        let sourceAlbum = nextProps.albums.sort(albumComparator).get(0);
        if (!_.isNil(sourceAlbum)) {
            let state = Object.assign({}, this.state);
            state.sourceAlbumId = sourceAlbum.id;
            this.setState(state);
        }
    }

    componentDidUpdate() {
        let component = this;
        let dataSource = this.props.albums.sort(albumComparator).map(function(album) {
            return {id: album.id, name: album.name}
        }).toJS();
        var albumDropDownList = $('#create-album-drop-down-list');
        albumDropDownList.kendoDropDownList({
            dataTextField: 'name',
            dataValueField: 'id',
            dataSource: dataSource,
            change: function() {
                let value = this.value();
                let state = Object.assign({}, component.state);
                state.sourceAlbumId = _.toInteger(value);
                component.setState(state);
            },
        });
        var dropdownlist = albumDropDownList.data("kendoDropDownList");
    }

    onNameChange(element) {
        let state = Object.assign({}, this.state);
        state.name = element.target.value;
        this.setState(state);
    }

    onWidthChange(element) {
        let state = Object.assign({}, this.state);
        state.width = element.target.value;
        this.setState(state);
    }

    onHeightChange(element) {
        let state = Object.assign({}, this.state);
        state.height = element.target.value;
        this.setState(state);
    }

    onDoUseSmartCroppingChange(element) {
        let state = Object.assign({}, this.state);
        state.doUseSmartCropping = element.target.checked;
        this.setState(state);
    }

    onAddAlbum(element) {
        let component = this;
        // Validate form
        let state = Object.assign({}, this.state);
        state.width = parseInt(state.width);
        state.height = parseInt(state.height);
        state.isNameValid = _.isString(state.name) && state.name != '';
        state.isWidthValid = _.isInteger(state.width) && state.width > 0 && state.width <= 5000;
        state.isHeightValid = _.isInteger(state.height) && state.height > 0 && state.height <= 5000;
        this.setState(state);
        if (!state.isNameValid || !state.isWidthValid || !state.isHeightValid) {
            return;
        }
        $('#create-album-modal').modal('toggle');

        if (state.doUseSmartCropping && !_.isNil(state.sourceAlbumId)) {
            this.props.createSmartCroppedAlbum(
                this.props.endpoint,
                state.name,
                state.width,
                state.height,
                this.props.getAlbumById(this.state.sourceAlbumId)
            );
        } else {
            this.props.createAlbum(this.props.endpoint, state.name, state.width, state.height);
        }
        this.resetState();
    }

    render() {
        return (
            <div id="create-album-modal" className="modal fade" role="dialog">
                <div className="modal-dialog">
                    <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal"><span className="fa fa-times"></span></button>
                            <h4 className="modal-title">Create an album</h4>
                        </div>
                        <form role="form" name="form">
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <label>Name</label>
                                        <input maxLength="250" value={this.state.name} onChange={this.onNameChange.bind(this)} className={"k-textbox" + (this.state.isNameValid ? "":" parsley-error")}/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12 col-md-6">
                                        <label>Image Width (px)</label>
                                        <input value={this.state.width} onChange={this.onWidthChange.bind(this)} type="number" min="1" max="5000" className={"k-textbox" + (this.state.isWidthValid ? "":" parsley-error")}/>
                                    </div>
                                    <div className="col-xs-12 col-md-6">
                                        <label>Image Height (px)</label>
                                        <input value={this.state.height} onChange={this.onHeightChange.bind(this)} type="number" min="1" max="5000" className={"k-textbox" + (this.state.isHeightValid ? "":" parsley-error")}/>
                                    </div>
                                </div>
                                <div className="row">
                                    <div className="col-xs-12">
                                        <div className="checkbox">
                                            <label><input type="checkbox" checked={this.state.doUseSmartCropping} onClick={this.onDoUseSmartCroppingChange.bind(this)}/> Use smart cropping (beta)</label>
                                        </div>
                                    </div>
                                </div>
                                {this.state.doUseSmartCropping ? (
                                    <div className="row">
                                        <div className="col-xs-12">
                                            <label>Source Folder</label>
                                            <input id="create-album-drop-down-list"/>
                                        </div>
                                    </div>
                                ) : null}
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-xs-12 col-md-offset-8 col-md-4">
                                        <button onClick={this.onAddAlbum.bind(this)} type="button" className="k-primary k-button green">
                                            Add album
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        );
    }
}

class AlbumSelect extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            width: null,
            height: null,

            isNameValid: true,
            isWidthValid: true,
            isHeightValid: true
        }
    }

    componentDidMount() {
    }

    componentDidUpdate() {
        let component = this;
        let dataSource = this.props.albums.sort(albumComparator).map(function(album) {
            return {id: album.id, name: album.name}
        }).toJS();
        var albumDropDownList = $('#album-drop-down-list');
        albumDropDownList.kendoDropDownList({
            dataTextField: 'name',
            dataValueField: 'id',
            dataSource: dataSource,
            change: function() {
                let value = this.value();
                component.props.selectAlbumById(_.toInteger(value));
            }
        });
        var dropdownlist = albumDropDownList.data("kendoDropDownList");
        dropdownlist.value(this.props.selectedAlbumId);
        dropdownlist.enable(!component.props.doShowSpinner);
    }

    onDeleteAlbum(element) {
        $('#delete-album-modal').modal('toggle');
        this.props.deleteAlbum(this.props.getSelectedAlbum().url);
    }

    render() {
        //let optionNodes = this.props.albums.sort(albumComparator).map(function(album) {
        //    return (
        //        <option key={album.id} value={album.id}>{ album.name }</option>
        //    );
        //});
        let selectedAlbum = this.props.getSelectedAlbum();

        let clientWidget = (
            <div className="row album-select">
                <div className="col-xs-12">
                    <input id="album-drop-down-list"/>
                </div>
            </div>
        );
        let photographerWidget = (
            <div className="row album-select">
                <div className="col-xs-12 col-md-5 col-lg-6">
                    <input id="album-drop-down-list"/>
                </div>
                <div className="col-xs-12 col-md-4 col-lg-4">
                    <button disabled={this.props.doShowSpinner ? "disabled" : false} type="button" className="k-primary k-button green create-button" data-toggle="modal" data-target="#create-album-modal">
                        <span className="fa fa-picture-o"/> Create album
                    </button>
                </div>
                <div className="col-xs-12 col-md-2 col-lg-1">
                    <button disabled={this.props.doShowSpinner ? "disabled" : false} type="button" className="k-primary k-button orange edit-button" data-toggle="modal" data-target="#update-album-modal">
                        <span className="fa fa-pencil"/>
                    </button>
                </div>
                <div className="col-xs-12 col-md-2 col-lg-1">
                    <button disabled={this.props.doShowSpinner ? "disabled" : false} type="button" className="k-primary k-button red delete-button" data-toggle="modal" data-target="#delete-album-modal">
                        <span className="fa fa-trash-o"/>
                    </button>
                </div>
            </div>
        );

        return (
            <div>
                <div id="delete-album-modal" className="modal fade" role="dialog">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal"><span className="fa fa-times"></span></button>
                                <h4 className="modal-title">Delete <span className="regular-font">{_.isNil(selectedAlbum) ? '':selectedAlbum.name }?</span></h4>
                            </div>
                            <div className="modal-body">
                                <div className="row">
                                    <div className="col-xs-12">
                                        <p>This action will permanently delete <span className="regular-font">{_.isNil(selectedAlbum) ? '':selectedAlbum.images.length}</span> photos. Are you sure you want to do this?</p>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer">
                                <div className="row">
                                    <div className="col-xs-12 col-md-offset-6 col-md-3">
                                        <button type="button" className="k-primary k-button white" data-toggle="modal" data-target="#delete-album-modal">
                                            Cancel
                                        </button>
                                    </div>
                                    <div className="col-xs-12 col-md-3">
                                        <button onClick={this.onDeleteAlbum.bind(this)} type="button" className="k-primary k-button red">
                                            Delete
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
                <CreateAlbumModal
                    albums={this.props.albums}
                    selectedAlbumId={this.props.selectedAlbumId}
                    endpoint={this.props.endpoint}
                    createAlbum={this.props.createAlbum}
                    createSmartCroppedAlbum={this.props.createSmartCroppedAlbum}
                    getSelectedAlbum={this.props.getSelectedAlbum}
                    getAlbumById={this.props.getAlbumById}
                    addSpinnerId={this.props.addSpinnerId}
                    deleteSpinnerId={this.props.deleteSpinnerId}
                    retrieveAlbum={this.props.retrieveAlbum}
                />
                <UpdateAlbumModal
                    getSelectedAlbum={this.props.getSelectedAlbum}
                    updateAlbum={this.props.updateAlbum}
                />
                {this.props.isClientMode ? clientWidget:photographerWidget}
            </div>
        );
    }
}

CreateAlbumModal.propTypes = {
    endpoint: React.PropTypes.string.isRequired,
    selectedAlbumId: React.PropTypes.number,

    albums: ImmutablePropTypes.list.isRequired,

    createAlbum: React.PropTypes.func.isRequired,
    createSmartCroppedAlbum: React.PropTypes.func.isRequired,
    getSelectedAlbum: React.PropTypes.func.isRequired,
    getAlbumById: React.PropTypes.func.isRequired,
    addSpinnerId: React.PropTypes.func.isRequired,
    deleteSpinnerId: React.PropTypes.func.isRequired,
    retrieveAlbum: React.PropTypes.func.isRequired
};

AlbumSelect.propTypes = {
    albums: ImmutablePropTypes.list.isRequired,

    selectedAlbumId: React.PropTypes.number,
    endpoint: React.PropTypes.string.isRequired,
    doShowSpinner: React.PropTypes.bool.isRequired,
    isClientMode: React.PropTypes.bool.isRequired,

    createAlbum: React.PropTypes.func.isRequired,
    deleteAlbum: React.PropTypes.func.isRequired,
    selectAlbumById: React.PropTypes.func.isRequired,
    getSelectedAlbum: React.PropTypes.func.isRequired,
    createSmartCroppedAlbum: React.PropTypes.func.isRequired,
    addSpinnerId: React.PropTypes.func.isRequired,
    deleteSpinnerId: React.PropTypes.func.isRequired,
    getAlbumById: React.PropTypes.func.isRequired,
    retrieveAlbum: React.PropTypes.func.isRequired,
    updateAlbum: React.PropTypes.func.isRequired
};

export default AlbumSelect;