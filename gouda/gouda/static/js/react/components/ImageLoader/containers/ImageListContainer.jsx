import { connect } from 'react-redux';
import Immutable from 'immutable';
import { deleteImageRequest, approveImageRequest, rejectImageRequest, commentImageRequest, getSelectedAlbum } from '../actions.jsx';
import { addSpinnerId, deleteSpinnerId } from '../actions.jsx';
import * as actions from '../actions.jsx';
import ImageList from '../components/ImageList.jsx';
import _ from 'lodash';

const mapStateToProps = function(state) {
    let images = Immutable.List([]);
    let selectedAlbum = getSelectedAlbum()(null, function(){return state;});
    if (!_.isNil(selectedAlbum)) {
        images = Immutable.List(selectedAlbum.images);
    }
    return {
        images: images,
        isClientMode: state.isClientMode,
        placeholderUrl: state.placeholderUrl,
        offset: state.listOffset
    }
};

const mapDispatchToProps = function(dispatch) {
    return {
        showImageDetail: function(id) {
            dispatch(actions.showImageDetail());
            dispatch(actions.selectImageById(id));
        },
        incrementOffset: function(amount) {
            dispatch(actions.incrementListOffset(amount));
        },
        decrementOffset: function(amount) {
            dispatch(actions.decrementListOffset(amount));
        },
        deleteImage: function(url) {
            dispatch(deleteImageRequest(url));
        },
        approveImage: function(url, didClientApprove) {
            dispatch(approveImageRequest(url, didClientApprove));
        },
        rejectImage: function(url, didClientReject) {
            dispatch(rejectImageRequest(url, didClientReject));
        },
        commentImage: function(url, comment) {
            dispatch(commentImageRequest(url, comment));
        },
        addSpinnerId: function(id) {
            return dispatch(addSpinnerId(id));
        },
        deleteSpinnerId: function(id) {
            return dispatch(deleteSpinnerId(id));
        }
    }
};

const ImageListContainer = connect(mapStateToProps, mapDispatchToProps)(ImageList);

export default ImageListContainer
