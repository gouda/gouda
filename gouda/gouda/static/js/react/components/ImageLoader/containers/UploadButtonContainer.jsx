import { connect } from 'react-redux';
import { createImageRequest } from '../actions.jsx';
import { uploadFileRequest } from '../actions.jsx';
import { showSpinner } from '../actions.jsx';
import { getSelectedAlbum } from '../actions.jsx';
import UploadButton from '../components/UploadButton.jsx';
import Immutable from 'immutable';
import * as actions from '../actions.jsx';

const mapStateToProps = function(state) {
    let selectedAlbum = getSelectedAlbum()(null, function(){return state;});
    if (!_.isNil(selectedAlbum)) {
        selectedAlbum.images = Immutable.List(selectedAlbum.images);
    }
    let doShowSpinner = state.spinnerIds.size > 0;
    return {
        isEnabled: !_.isNil(selectedAlbum) && !doShowSpinner,
        album: selectedAlbum,
        isClientMode: state.isClientMode,
        doShowSpinner: doShowSpinner
    }
};

const mapDispatchToProps = function(dispatch) {
    return {
        uploadFile: function(album, file) {
            dispatch(actions.resetListOffset());
            return dispatch(uploadFileRequest(album, file));
        },
        createImage: function(album, name, data) {
            return dispatch(createImageRequest(album, name, data));
        }
    }
};

const UploadButtonContainer = connect(mapStateToProps, mapDispatchToProps)(UploadButton);

export default UploadButtonContainer
