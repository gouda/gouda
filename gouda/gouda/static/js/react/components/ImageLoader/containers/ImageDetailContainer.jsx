import { connect } from 'react-redux';
import Immutable from 'immutable';
import * as actions from '../actions.jsx';
import ImageDetail from '../components/ImageDetail.jsx';
import _ from 'lodash';

const mapStateToProps = function(state) {
    let numImages = undefined;
    let images = Immutable.List([]);
    let image = undefined;
    let selectedAlbum = actions.getSelectedAlbum()(null, function(){return state;});
    if (!_.isNil(selectedAlbum)) {
        images = Immutable.List(selectedAlbum.images);
        numImages = images.size;
    }
    let index = undefined;
    if (!_.isNil(state.imageDetailState.selectedImageId)) {
        index = images.findIndex(function(image){
            return image.id == state.imageDetailState.selectedImageId;
        });
        if (index >= 0) {
            image = images.get(index);
        }
    }

    // Metadata
    let metadata = undefined;
    if (!_.isNil(image)) {
        let imageMetadataRequest = state.imageMetadataRequests.get(image.id);
        if (!_.isNil(imageMetadataRequest)) {
            if (imageMetadataRequest.isSuccess) {
                metadata = imageMetadataRequest.payload;
            }
        }
    }
    return {
        index: index,
        numImages: numImages,
        image: image,
        metadata: metadata,
        images: images,
        isClientMode: state.isClientMode,
        placeholderUrl: state.placeholderUrl,
        doShow: state.imageDetailState.doShow,
        doShowSpinner: state.spinnerIds.size > 0
    }
};

const mapDispatchToProps = function(dispatch) {
    return {
        dispatch: dispatch,
        deleteImage: function(url) {
            dispatch(actions.deleteImageRequest(url));
        },
        approveImage: function(url, didClientApprove) {
            dispatch(actions.approveImageRequest(url, didClientApprove));
        },
        rejectImage: function(url, didClientReject) {
            dispatch(actions.rejectImageRequest(url, didClientReject));
        },
        commentImage: function(url, comment) {
            dispatch(actions.commentImageRequest(url, comment));
        },

        addSpinnerId: function(id) {
            return dispatch(addSpinnerId(id));
        },
        deleteSpinnerId: function(id) {
            return dispatch(deleteSpinnerId(id));
        },
        hideImageDetail: function() {
            return dispatch(actions.hideImageDetail());
        },
        retrieveImageMetadata: function(id, url) {
            return dispatch(actions.retrieveImageMetadataRequest(id, url));
        }
    }
};

function mergeProps(stateProps, dispatchProps, ownProps) {
    const { image, images } = stateProps;
    const { dispatch } = dispatchProps;
    return {
        ...ownProps,
        ...stateProps,
        ...dispatchProps,
        selectNextImage: function() {
            if (!_.isNil(image)) {
                let index = images.findIndex(function(i){
                    return image.id == i.id;
                });
                index = index + 1;
                if (index >= images.count()) {
                    index = 0;
                }
                return dispatch(actions.selectImageById(images.get(index).id));
            }
            return null;
        },
        selectPreviousImage: function() {
            if (!_.isNil(image)) {
                let index = images.findIndex(function(i){
                    return image.id == i.id;
                });
                index = index - 1;
                if (index < 0) {
                    index = images.count() - 1;
                }
                return dispatch(actions.selectImageById(images.get(index).id));
            }
            return null;
        }
    };
}



const ImageDetailContainer = connect(mapStateToProps, mapDispatchToProps, mergeProps)(ImageDetail);

export default ImageDetailContainer
