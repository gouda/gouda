import { connect } from 'react-redux';
import { createImage } from '../actions.jsx';
import { getSelectedAlbum } from '../actions.jsx';
import DownloadButton from '../components/DownloadButton.jsx';
import Immutable from 'immutable';

const mapStateToProps = function(state) {
    let images = Immutable.List([]);
    let selectedAlbum = getSelectedAlbum()(null, function(){return state;});
    if (!_.isNil(selectedAlbum)) {
        images = Immutable.List(selectedAlbum.images);
    }
    let doShowSpinner = state.spinnerIds.size > 0;

    return {
        images: images,
        isClientMode: state.isClientMode,
        doShowSpinner: doShowSpinner,
        downloadUrl: state.downloadUrl
    }
};

const DownloadButtonContainer = connect(mapStateToProps)(DownloadButton);

export default DownloadButtonContainer
