import { connect } from 'react-redux';
import { addSpinnerId } from '../actions.jsx';
import { deleteSpinnerId } from '../actions.jsx';
import { selectAlbumById } from '../actions.jsx';
import { deleteAlbumRequest } from '../actions.jsx';
import { createAlbumRequest } from '../actions.jsx';
import { createSmartCroppedAlbumRequest } from '../actions.jsx';
import { selectLatestAlbum } from '../actions.jsx';
import { getSelectedAlbum } from '../actions.jsx';
import { createSmartCroppedImage } from '../actions.jsx';
import { getAlbumById } from '../actions.jsx';
import { retrieveAlbum } from '../actions.jsx';
import AlbumSelect from '../components/AlbumSelect.jsx';
import * as actions from '../actions.jsx';
import Immutable from 'immutable';

const mapStateToProps = function(state) {
    let doShowSpinner = state.spinnerIds.size > 0;
    return {
        albums: state.albums,
        selectedAlbumId: state.selectedAlbumId,
        endpoint: state.endpoint,
        doShowSpinner: doShowSpinner,
        isClientMode: state.isClientMode
    }
};

const mapDispatchToProps = function(dispatch) {
    return {
        createSmartCroppedImage: function(image_url, name, width, height, album) {
            return dispatch(createSmartCroppedImage(image_url, name, width, height, album));
        },
        getSelectedAlbum: function() {
            return dispatch(getSelectedAlbum());
        },
        selectAlbumById: function(id) {
            return dispatch(selectAlbumById(id));
        },
        createAlbum: function(url, name, width, height) {
            return dispatch(createAlbumRequest(url, name, width, height));
        },
        createSmartCroppedAlbum: function(url, name, width, height, sourceAlbum) {
            return dispatch(createSmartCroppedAlbumRequest(url, name, width, height, sourceAlbum));
        },
        deleteAlbum: function(url) {
            return dispatch(deleteAlbumRequest(url));
        },
        getAlbumById: function(id) {
            return dispatch(getAlbumById(id));
        },
        addSpinnerId: function(id) {
            return dispatch(addSpinnerId(id));
        },
        deleteSpinnerId: function(id) {
            return dispatch(deleteSpinnerId(id));
        },
        retrieveAlbum: function(endpoint) {
            return dispatch(retrieveAlbum(endpoint));
        },
        updateAlbum: function(endpoint, data) {
            return dispatch(actions.updateAlbumRequest(endpoint, data));
        }
    }
};

const AlbumSelectContainer = connect(mapStateToProps, mapDispatchToProps)(AlbumSelect);

export default AlbumSelectContainer
