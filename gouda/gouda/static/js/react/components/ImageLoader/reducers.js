import Immutable from 'immutable';
import { combineReducers } from 'redux';
import * as actions from './actions.jsx';
import {
    ADD_SPINNER_ID,
    DELETE_SPINNER_ID,
    REJECT_IMAGE_REQUEST,
    REJECT_IMAGE_SUCCESS, 
    REJECT_IMAGE_FAILURE, 
    APPROVE_IMAGE_REQUEST, 
    APPROVE_IMAGE_SUCCESS, 
    APPROVE_IMAGE_FAILURE, 
    COMMENT_IMAGE_REQUEST, 
    COMMENT_IMAGE_SUCCESS, 
    COMMENT_IMAGE_FAILURE, 
    DELETE_IMAGE_REQUEST, 
    DELETE_IMAGE_SUCCESS, 
    DELETE_IMAGE_FAILURE, 
    CREATE_IMAGE_REQUEST, 
    CREATE_IMAGE_SUCCESS, 
    CREATE_IMAGE_FAILURE, 
    CREATE_SMART_CROPPED_IMAGE_REQUEST, 
    CREATE_SMART_CROPPED_IMAGE_SUCCESS, 
    CREATE_SMART_CROPPED_IMAGE_FAILURE, 
    RETRIEVE_IMAGES_REQUEST, 
    RETRIEVE_IMAGES_SUCCESS, 
    RETRIEVE_IMAGES_FAILURE, 
    RETRIEVE_ALBUMS_REQUEST,
    RETRIEVE_ALBUMS_SUCCESS,
    RETRIEVE_ALBUMS_FAILURE,
    RETRIEVE_ALBUM_REQUEST,
    RETRIEVE_ALBUM_SUCCESS,
    RETRIEVE_ALBUM_FAILURE,
    CREATE_ALBUM_REQUEST,
    CREATE_ALBUM_SUCCESS,
    CREATE_SMART_CROPPED_ALBUM_SUCCESS,
    CREATE_ALBUM_FAILURE,
    DELETE_ALBUM_REQUEST,
    DELETE_ALBUM_SUCCESS,
    DELETE_ALBUM_FAILURE,
    SELECT_ALBUM_BY_ID,
    SHOW_SPINNER} from './actions.jsx';

export function thumborSettings(state = {}, action) {
    return state;
}

export function doShowImageDetail(state = false, action) {
    switch (action.type) {
        case actions.SHOW_IMAGE_DETAIL:
            return true;
        case actions.HIDE_IMAGE_DETAIL:
            return false;
        default:
            return state;
    }
}

export function selectedImageId(state = null, action) {
    switch (action.type) {
        case actions.SELECT_IMAGE_BY_ID:
            return action.payload.id;
        default:
            return state;
    }
}

export const imageDetailState = combineReducers({
    doShow: doShowImageDetail,
    selectedImageId: selectedImageId
});

export function listOffset(state = 0, action) {
    switch (action.type) {
        case actions.INCREMENT_LIST_OFFSET:
            return state + action.payload.amount;
        case actions.DECREMENT_LIST_OFFSET:
            return Math.max(state - action.payload.amount, 0);
        case actions.RESET_LIST_OFFSET:
            return 0;
        case actions.SELECT_ALBUM_BY_ID:
            return 0;
        default:
            return state;
    }
}

function selectedAlbumId(state = null, action) {
    switch (action.type) {
        case SELECT_ALBUM_BY_ID:
            return action.payload.selectedAlbumId;
        default:
            return state;
    }
}

function albumFormModal(state = '', action) {
    return state;
}

function isClientMode(state = false, action) {
    return state;
}

function endpoint(state = '', action) {
    return state;
}

function placeholderUrl(state = '', action) {
    return state;
}

function downloadUrl(state = '', action) {
    return state;
}

function doShowSpinner(state = false, action) {
    switch (action.type) {
        //case CREATE_IMAGE_REQUEST:
        //    return action.payload.doShowSpinner;
        //case CREATE_IMAGE_SUCCESS:
        //    return action.payload.doShowSpinner;
        //case CREATE_IMAGE_FAILURE:
        //    return action.payload.doShowSpinner;

        //case CREATE_SMART_CROPPED_IMAGE_REQUEST:
        //case CREATE_SMART_CROPPED_IMAGE_SUCCESS:
        //case CREATE_SMART_CROPPED_IMAGE_FAILURE:

        //case CREATE_ALBUM_REQUEST:
        //case CREATE_ALBUM_SUCCESS:
        //case CREATE_ALBUM_FAILURE:
        //
        //case DELETE_IMAGE_REQUEST:
        //case DELETE_IMAGE_SUCCESS:
        //case DELETE_IMAGE_FAILURE:
        //
        //case DELETE_IMAGE_REQUEST:
        //case DELETE_IMAGE_SUCCESS:
        //case DELETE_IMAGE_FAILURE:
        //
        //case RETRIEVE_IMAGES_REQUEST:
        //case RETRIEVE_IMAGES_SUCCESS:
        //case RETRIEVE_IMAGES_FAILURE:
        //
        //case RETRIEVE_ALBUMS_REQUEST:
        //case RETRIEVE_ALBUMS_SUCCESS:
        //case RETRIEVE_ALBUMS_FAILURE:
        //
        //case APPROVE_IMAGE_REQUEST:
        //case APPROVE_IMAGE_SUCCESS:
        //case APPROVE_IMAGE_FAILURE:
        //
        //case REJECT_IMAGE_REQUEST:
        //case REJECT_IMAGE_SUCCESS:
        //case REJECT_IMAGE_FAILURE:
        //
        //case COMMENT_IMAGE_REQUEST:
        //case COMMENT_IMAGE_SUCCESS:
        //case COMMENT_IMAGE_FAILURE:
        //    return action.payload.doShowSpinner;

        case SHOW_SPINNER:
            return action.payload.doShowSpinner;
        default:
            return state
    }
}

function albums(state = Immutable.List([]), action) {
    let indexToRemove;
    let newState;
    switch (action.type) {
        case RETRIEVE_ALBUMS_SUCCESS:
            return Immutable.List(action.payload.albums);
        case CREATE_SMART_CROPPED_ALBUM_SUCCESS:
        case CREATE_ALBUM_SUCCESS:
            return state.push(action.payload.album);
        case DELETE_ALBUM_SUCCESS:
            newState = state;
            indexToRemove = newState.findIndex(function(album){
                return album.url == action.payload.album.url;
            });
            if (indexToRemove >= 0) {
                newState = newState.delete(indexToRemove);
            }
            return newState;

        case actions.UPDATE_ALBUM_SUCCESS:
        case RETRIEVE_ALBUM_SUCCESS:
        case CREATE_SMART_CROPPED_IMAGE_SUCCESS:
        case CREATE_IMAGE_SUCCESS:
            newState = state;
            indexToRemove = newState.findIndex(function(album){
                return album.url == action.payload.album.url;
            });
            if (indexToRemove >= 0) {
                newState = newState.delete(indexToRemove);
            }
            return newState.push(action.payload.album);

        case COMMENT_IMAGE_SUCCESS:
        case APPROVE_IMAGE_SUCCESS:
        case REJECT_IMAGE_SUCCESS:
            newState = state;
            for (let album of newState) {
                let images = Immutable.List(album.images);
                indexToRemove = images.findIndex(function(image) {
                    return image.url == action.payload.image.url;
                });
                if (indexToRemove >= 0) {
                    images = images.delete(indexToRemove);
                }
                album.images = images.push(action.payload.image);
            }
            return newState;
        case DELETE_IMAGE_SUCCESS:
            newState = state;
            for (let album of newState) {
                let images = Immutable.List(album.images);
                indexToRemove = images.findIndex(function(image) {
                    return image.url == action.payload.image.url;
                });
                if (indexToRemove >= 0) {
                    images = images.delete(indexToRemove);
                }
                album.images = images;
            }
            return newState;
        default:
            return state;

    }
}

function spinnerIds(state = Immutable.Set([]), action) {
    switch (action.type) {
        case ADD_SPINNER_ID:
            return state.add(action.payload.id);
        case DELETE_SPINNER_ID:
            return state.delete(action.payload.id);
        default:
            return state;
    }
}

function images(state = Immutable.List([]), action) {
    let indexToRemove;
    switch (action.type) {
        case RETRIEVE_IMAGES_SUCCESS:
            return Immutable.List(action.payload.images);
        case CREATE_IMAGE_SUCCESS:
            return state.push(action.payload.image);
        case DELETE_IMAGE_SUCCESS:
            indexToRemove = state.findIndex(function(image) {
                return image.url == action.payload.image.url;
            });
            if (indexToRemove >= 0) {
                return state.delete(indexToRemove);
            } else {
                return state;
            }
            return state;
        case COMMENT_IMAGE_SUCCESS:
        case APPROVE_IMAGE_SUCCESS:
        case REJECT_IMAGE_SUCCESS:
            indexToRemove = state.findIndex(function(image) {
                return image.url == action.payload.image.url;
            });
            let newState;
            if (indexToRemove >= 0) {
                newState = state.delete(indexToRemove);
            } else {
                newState = state;
            }
            return newState.push(action.payload.image);

        default:
            return state;

    }
}

export function imageMetadataRequests(state = Immutable.Map({}), action) {
    switch (action.type) {
        case actions.RETRIEVE_IMAGE_METADATA_SUCCESS:
            return state.set(action.payload.id, {
                isSuccess: true,
                payload: action.payload.metadata
            });
        case actions.RETRIEVE_IMAGE_METADATA_FAILURE:
            return state.set(action.payload.id, {
                isSuccess: false,
                payload: action.payload.error
            });
        default:
            return state;
    }
}

const rootReducer = combineReducers({
    spinnerIds,
    endpoint,
    isClientMode,
    downloadUrl,
    placeholderUrl,
    imageMetadataRequests,

    albums,
    selectedAlbumId,

    listOffset,

    thumborSettings,

    imageDetailState
});

export default rootReducer;