import 'babel-polyfill';
import { createStore as reduxCreateStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import createLogger from 'redux-logger';
import rootReducer from './reducers';
import createSagaMiddleware from 'redux-saga'
import { watchCreateSmartCroppedAlbumRequest } from './sagas.jsx'
import { watchRejectImageRequest } from './sagas.jsx'
import { watchCommentImageRequest } from './sagas.jsx'
import { watchApproveImageRequest } from './sagas.jsx'
import { watchDeleteImageRequest } from './sagas.jsx'
import { watchUploadFileRequest } from './sagas.jsx'
import { watchCreateImageRequest } from './sagas.jsx'
import { watchRetrieveAlbumsRequest } from './sagas.jsx'
import { watchCreateAlbumRequest } from './sagas.jsx'
import { watchDeleteAlbumRequest } from './sagas.jsx'
import { watchInitializeRequest } from './sagas.jsx'
import * as sagas from './sagas.jsx'

const loggerMiddleware = createLogger();
const sagaMiddleware = createSagaMiddleware();

export function createStore(initialState) {
    const store = reduxCreateStore(
        rootReducer,
        initialState,
        applyMiddleware(
            sagaMiddleware,
            thunkMiddleware,
            loggerMiddleware
        )
    );
    sagaMiddleware.run(watchRejectImageRequest);
    sagaMiddleware.run(watchCommentImageRequest);
    sagaMiddleware.run(watchApproveImageRequest);
    sagaMiddleware.run(watchUploadFileRequest);
    sagaMiddleware.run(watchDeleteImageRequest);
    sagaMiddleware.run(watchCreateImageRequest);
    sagaMiddleware.run(watchRetrieveAlbumsRequest);
    sagaMiddleware.run(watchCreateAlbumRequest);
    sagaMiddleware.run(watchCreateSmartCroppedAlbumRequest);
    sagaMiddleware.run(watchDeleteAlbumRequest);
    sagaMiddleware.run(watchInitializeRequest);
    sagaMiddleware.run(sagas.watchRetrieveImageMetadataRequest);
    sagaMiddleware.run(sagas.watchUpdateAlbumRequest);
    return store;
}