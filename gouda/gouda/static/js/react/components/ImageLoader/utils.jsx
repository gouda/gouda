export function albumComparator(a, b) {
    if (a.name.includes('Original')) {
        return -1;
    } else if (b.name.includes('Original')) {
        return 1;
    } else if (a.name > b.name) {
        return 1;
    } else if (a.name < b.name) {
        return -1;
    } else {
        return 0;
    }
}

export function dateCreatedComparator(a, b) {
    if (a.dateCreated > b.dateCreated) {
        return 1;
    } else if (a.dateCreated < b.dateCreated) {
        return -1;
    } else {
        return 0;
    }
}
