import Axios from 'axios';
import _ from 'lodash';

import { dateCreatedComparator } from './utils.jsx';

export const ADD_SPINNER_ID = 'ADD_SPINNER_ID';
export const DELETE_SPINNER_ID = 'DELETE_SPINNER_ID';

export const UPLOAD_FILE_REQUEST = 'UPLOAD_FILE_REQUEST';
export const UPLOAD_FILE_SUCCESS = 'UPLOAD_FILES_SUCCESS';
export const UPLOAD_FILE_FAILURE = 'UPLOAD_FILES_FAILURE';

export const INITIALIZE_REQUEST = 'INITIALIZE_REQUEST';
export const INITIALIZE_SUCCESS = 'INITIALIZE_SUCCESS';
export const INITIALIZE_FAILURE = 'INITIALIZE_FAILURE';

export const RETRIEVE_ALBUMS_REQUEST = 'RETRIEVE_ALBUMS_REQUEST';
export const RETRIEVE_ALBUMS_SUCCESS = 'RETRIEVE_ALBUMS_SUCCESS';
export const RETRIEVE_ALBUMS_FAILURE = 'RETRIEVE_ALBUMS_FAILURE';

export const RETRIEVE_ALBUM_REQUEST = 'RETRIEVE_ALBUM_REQUEST';
export const RETRIEVE_ALBUM_SUCCESS = 'RETRIEVE_ALBUM_SUCCESS';
export const RETRIEVE_ALBUM_FAILURE = 'RETRIEVE_ALBUM_FAILURE';

export const CREATE_ALBUM_REQUEST = 'CREATE_ALBUM_REQUEST';
export const CREATE_ALBUM_SUCCESS = 'CREATE_ALBUM_SUCCESS';
export const CREATE_ALBUM_FAILURE = 'CREATE_ALBUM_FAILURE';

export const CREATE_SMART_CROPPED_ALBUM_REQUEST = 'CREATE_SMART_CROPPED_ALBUM_REQUEST';
export const CREATE_SMART_CROPPED_ALBUM_SUCCESS = 'CREATE_SMART_CROPPED_ALBUM_SUCCESS';
export const CREATE_SMART_CROPPED_ALBUM_FAILURE = 'CREATE_SMART_CROPPED_ALBUM_FAILURE';

export const DELETE_ALBUM_REQUEST = 'DELETE_ALBUM_REQUEST';
export const DELETE_ALBUM_SUCCESS = 'DELETE_ALBUM_SUCCESS';
export const DELETE_ALBUM_FAILURE = 'DELETE_ALBUM_FAILURE';

export const RETRIEVE_IMAGES_REQUEST = 'RETRIEVE_IMAGES_REQUEST';
export const RETRIEVE_IMAGES_SUCCESS = 'RETRIEVE_IMAGES_SUCCESS';
export const RETRIEVE_IMAGES_FAILURE = 'RETRIEVE_IMAGES_FAILURE';

export const CREATE_SMART_CROPPED_IMAGE_REQUEST = 'CREATE_SMART_CROPPED_IMAGE_REQUEST';
export const CREATE_SMART_CROPPED_IMAGE_SUCCESS = 'CREATE_SMART_CROPPED_IMAGE_SUCCESS';
export const CREATE_SMART_CROPPED_IMAGE_FAILURE = 'CREATE_SMART_CROPPED_IMAGE_FAILURE';

export const CREATE_IMAGE_REQUEST = 'CREATE_IMAGE_REQUEST';
export const CREATE_IMAGE_SUCCESS = 'CREATE_IMAGE_SUCCESS';
export const CREATE_IMAGE_FAILURE = 'CREATE_IMAGE_FAILURE';

export const DELETE_IMAGE_REQUEST = 'DELETE_IMAGE_REQUEST';
export const DELETE_IMAGE_SUCCESS = 'DELETE_IMAGE_SUCCESS';
export const DELETE_IMAGE_FAILURE = 'DELETE_IMAGE_FAILURE';

export const COMMENT_IMAGE_REQUEST = 'COMMENT_IMAGE_REQUEST';
export const COMMENT_IMAGE_SUCCESS = 'COMMENT_IMAGE_SUCCESS';
export const COMMENT_IMAGE_FAILURE = 'COMMENT_IMAGE_FAILURE';

export const APPROVE_IMAGE_REQUEST = 'APPROVE_IMAGE_REQUEST';
export const APPROVE_IMAGE_SUCCESS = 'APPROVE_IMAGE_SUCCESS';
export const APPROVE_IMAGE_FAILURE = 'APPROVE_IMAGE_FAILURE';

export const REJECT_IMAGE_REQUEST = 'REJECT_IMAGE_REQUEST';
export const REJECT_IMAGE_SUCCESS = 'REJECT_IMAGE_SUCCESS';
export const REJECT_IMAGE_FAILURE = 'REJECT_IMAGE_FAILURE';

export const SELECT_ALBUM_BY_ID = 'SELECT_ALBUM_BY_ID';

export const SHOW_SPINNER = 'SHOW_SPINNER';

export const INCREMENT_LIST_OFFSET = 'INCREMENT_LIST_OFFSET';
export const DECREMENT_LIST_OFFSET = 'DECREMENT_LIST_OFFSET';
export const RESET_LIST_OFFSET = 'RESET_LIST_OFFSET';

export const SHOW_IMAGE_DETAIL = 'SHOW_IMAGE_DETAIL';
export const HIDE_IMAGE_DETAIL = 'HIDE_IMAGE_DETAIL';

export const SELECT_IMAGE_BY_ID = 'SELECT_IMAGE_BY_ID';

export const RETRIEVE_IMAGE_METADATA_REQUEST = 'RETRIEVE_IMAGE_METADATA_REQUEST';
export const RETRIEVE_IMAGE_METADATA_SUCCESS = 'RETRIEVE_IMAGE_METADATA_SUCCESS';
export const RETRIEVE_IMAGE_METADATA_FAILURE = 'RETRIEVE_IMAGE_METADATA_FAILURE';

export const UPDATE_ALBUM_REQUEST = 'UPDATE_ALBUM_REQUEST';
export const UPDATE_ALBUM_SUCCESS = 'UPDATE_ALBUM_SUCCESS';
export const UPDATE_ALBUM_FAILURE = 'UPDATE_ALBUM_FAILURE';

export function updateAlbumRequest(url, data) {
    return {
        type: UPDATE_ALBUM_REQUEST,
        payload: {
            url: url,
            data: data
        }
    }
}

export function updateAlbumSuccess(album) {
    return {
        type: UPDATE_ALBUM_SUCCESS,
        payload: {
            album: album
        }
    }
}

export function updateAlbumFailure(response) {
    return {
        type: UPDATE_ALBUM_FAILURE,
        payload: {
            error: response
        }
    }
}

export function retrieveImageMetadataRequest(id, url) {
    return {
        type: RETRIEVE_IMAGE_METADATA_REQUEST,
        payload: {
            id: id,
            url: url
        }
    }
}

export function retrieveImageMetadataSuccess(id, metadata) {
    return {
        type: RETRIEVE_IMAGE_METADATA_SUCCESS,
        payload: {
            id: id,
            metadata: metadata
        }
    }
}

export function retrieveImageMetadataFailure(id, response) {
    return {
        type: RETRIEVE_IMAGE_METADATA_FAILURE,
        payload: {
            id: id,
            error: response
        }
    }
}

// export function uploadFileSuccess() {
//     return {
//         type: UPLOAD_FILE_SUCCESS,
//         payload: {
//         }
//     }
// }
//
// export function uploadFileFailure() {
//     return {
//         type: UPLOAD_FILE_FAILURE,
//         payload: {
//         }
//     }
// }

export function selectImageById(id) {
    return {
        type: SELECT_IMAGE_BY_ID,
        payload: {
            id: id
        }
    }
}

export function showImageDetail() {
    return {
        type: SHOW_IMAGE_DETAIL
    }
}

export function hideImageDetail() {
    return {
        type: HIDE_IMAGE_DETAIL
    }
}

export function incrementListOffset(amount) {
    return {
        type: INCREMENT_LIST_OFFSET,
        payload: {
            amount: amount
        }
    }
}

export function decrementListOffset(amount) {
    return {
        type: DECREMENT_LIST_OFFSET,
        payload: {
            amount: amount
        }
    }
}

export function resetListOffset() {
    return {
        type: RESET_LIST_OFFSET
    }
}

export function addSpinnerId(id) {
    return {
        type: ADD_SPINNER_ID,
        payload: {
            id: id
        }
    }
}

export function deleteSpinnerId(id) {
    return {
        type: DELETE_SPINNER_ID,
        payload: {
            id: id
        }
    }
}

export function selectAlbumById(id) {
    return {
        type: SELECT_ALBUM_BY_ID,
        payload: {
            selectedAlbumId: id
        }
    }
}

export function uploadFileRequest(album, file) {
    return {
        type: UPLOAD_FILE_REQUEST,
        payload: {
            album: album,
            file: file
        }
    }
}

export function uploadFileSuccess() {
    return {
        type: UPLOAD_FILE_SUCCESS,
        payload: {
        }
    }
}

export function uploadFileFailure() {
    return {
        type: UPLOAD_FILE_FAILURE,
        payload: {
        }
    }
}

function createSmartCroppedImageRequest() {
    return {
        type: CREATE_SMART_CROPPED_IMAGE_REQUEST,
        payload: {
        }
    }
}

function createSmartCroppedImageSuccess(album) {
    return {
        type: CREATE_SMART_CROPPED_IMAGE_SUCCESS,
        payload: {
            album: album
        }
    }
}

function createSmartCroppedImageFailure(response) {
    return {
        type: CREATE_SMART_CROPPED_IMAGE_FAILURE,
        payload: {
            error: response
        }
    }
}

export function createSmartCroppedAlbumRequest(url, name, width, height, sourceAlbum) {
    return {
        type: CREATE_SMART_CROPPED_ALBUM_REQUEST,
        payload: {
            url: url,
            sourceAlbum: sourceAlbum,
            album: {
                name: name,
                width: width,
                height: height,
            }
        }
    }
}

export function createSmartCroppedAlbumSuccess(album) {
    return {
        type: CREATE_SMART_CROPPED_ALBUM_SUCCESS,
        payload: {
            album: album
        }
    }
}

export function createSmartCroppedAlbumFailure(response) {
    return {
        type: CREATE_SMART_CROPPED_ALBUM_FAILURE,
        payload: {
            error: response
        }
    }
}


export function createAlbumRequest(url, name, width, height) {
    return {
        type: CREATE_ALBUM_REQUEST,
        payload: {
            url: url,
            album: {
                name: name,
                width: width,
                height: height
            }
        }
    }
}

export function createAlbumSuccess(album) {
    return {
        type: CREATE_ALBUM_SUCCESS,
        payload: {
            album: album
        }
    }
}

export function createAlbumFailure(response) {
    return {
        type: CREATE_ALBUM_FAILURE,
        payload: {
            error: response
        }
    }
}

export function deleteAlbumRequest(url) {
    return {
        type: DELETE_ALBUM_REQUEST,
        payload: {
            album: {
                url: url
            }
        }
    }
}

export function deleteAlbumSuccess(url) {
    return {
        type: DELETE_ALBUM_SUCCESS,
        payload: {
            album: {
                url: url
            }
        }
    }
}

export function deleteAlbumFailure(response) {
    return {
        type: DELETE_ALBUM_FAILURE,
        payload: {
            error: response
        }
    }
}

function retrieveAlbumRequest() {
    return {
        type: RETRIEVE_ALBUM_REQUEST,
        payload: {
        }
    }
}

function retrieveAlbumSuccess(album) {
    return {
        type: RETRIEVE_ALBUM_SUCCESS,
        payload: {
            album: album
        }
    }
}

function retrieveAlbumFailure(response) {
    return {
        type: RETRIEVE_ALBUM_SUCCESS,
        payload: {
            error: response
        }
    }
}

export function retrieveAlbumsRequest(url) {
    return {
        type: RETRIEVE_ALBUMS_REQUEST,
        payload: {
            url: url
        }
    }
}

export function retrieveAlbumsSuccess(albums) {
    return {
        type: RETRIEVE_ALBUMS_SUCCESS,
        payload: {
            albums: albums
        }
    }
}

export function retrieveAlbumsFailure(response) {
    return {
        type: RETRIEVE_ALBUMS_FAILURE,
        payload: {
            error: response
        }
    }
}

export function initializeRequest(url) {
    return {
        type: INITIALIZE_REQUEST,
        payload: {
            url: url
        }
    }
}

export function initializeSuccess() {
    return {
        type: INITIALIZE_SUCCESS,
        payload: {
        }
    }
}

export function initializeFailure() {
    return {
        type: INITIALIZE_FAILURE,
        payload: {
        }
    }
}

function retrieveImagesRequest() {
    return {
        type: RETRIEVE_IMAGES_REQUEST,
        payload: {
        }
    }
}

function retrieveImagesSuccess(images) {
    return {
        type: RETRIEVE_IMAGES_SUCCESS,
        payload: {
            images: images
        }
    }
}

function retrieveImagesFailure(response) {
    return {
        type: RETRIEVE_IMAGES_SUCCESS,
        payload: {
            error: response
        }
    }
}

export function createImageRequest(album, name, data) {
    return {
        type: CREATE_IMAGE_REQUEST,
        payload: {
            album: {
                url: album.url,
                images: [{
                    booking: album.booking,
                    name: name,
                    image: data
                }]
            }
        }
    }
}

export function createImageSuccess(album) {
    return {
        type: CREATE_IMAGE_SUCCESS,
        payload: {
            album: album
        }
    }
}

export function createImageFailure(response) {
    return {
        type: CREATE_IMAGE_FAILURE,
        payload: {
            error: response
        }
    }
}

export function commentImageRequest(url, comment) {
    return {
        type: COMMENT_IMAGE_REQUEST,
        payload: {
            image: {
                url: url,
                comment: comment
            }
        }
    }
}

export function commentImageSuccess(image) {
    return {
        type: COMMENT_IMAGE_SUCCESS,
        payload: {
            image: image
        }
    }
}

export function commentImageFailure(response) {
    return {
        type: COMMENT_IMAGE_FAILURE,
        payload: {
            error: response
        }
    }
}

export function approveImageRequest(url, didClientApprove) {
    return {
        type: APPROVE_IMAGE_REQUEST,
        payload: {
            image: {
                url: url,
                didClientApprove: didClientApprove
            }
        }
    }
}

export function approveImageSuccess(image) {
    return {
        type: APPROVE_IMAGE_SUCCESS,
        payload: {
            image: image
        }
    }
}

export function approveImageFailure(response) {
    return {
        type: APPROVE_IMAGE_FAILURE,
        payload: {
            error: response
        }
    }
}

export function rejectImageRequest(url, didClientReject) {
    return {
        type: REJECT_IMAGE_REQUEST,
        payload: {
            image: {
                url: url,
                didClientReject: didClientReject
            }
        }
    }
}

export function rejectImageSuccess(image) {
    return {
        type: REJECT_IMAGE_SUCCESS,
        payload: {
            image: image
        }
    }
}

export function rejectImageFailure(response) {
    return {
        type: REJECT_IMAGE_FAILURE,
        payload: {
            error: response
        }
    }
}

export function deleteImageRequest(url) {
    return {
        type: DELETE_IMAGE_REQUEST,
        payload: {
            image: {
                url: url
            }
        }
    }
}

export function deleteImageSuccess(url) {
    return {
        type: DELETE_IMAGE_SUCCESS,
        payload: {
            image: {
                url: url
            }
        }
    }
}

export function deleteImageFailure(response) {
    return {
        type: DELETE_IMAGE_FAILURE,
        payload: {
            error: response
        }
    }
}

function get(url, onRequest, onSuccess, onFailure) {
    return function(dispatch) {
        dispatch(onRequest()); // Notify store a request is beginning
        return Axios.get(url, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function(response) {
                if (_.has(response.data, 'results')) {
                    dispatch(onSuccess(response.data.results));
                } else {
                    dispatch(onSuccess(response.data));
                }
            })
            .catch(function(response){
                console.log(response);
                dispatch(onFailure(response));
            });
    }
}

function patch(url, data, onRequest, onSuccess, onFailure) {
    return function(dispatch) {
        dispatch(onRequest()); // Notify store a request is beginning
        return Axios.patch(url, data, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function(response) {
                console.log(response);
                dispatch(onSuccess(response.data));
            })
            .catch(function(response){
                console.log(response);
                dispatch(onFailure(response));
            });
    }
}

function post(url, data, onRequest, onSuccess, onFailure) {
    return function(dispatch, getState) {
        dispatch(onRequest()); // Notify store a request is beginning
        return Axios.post(url, data, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function(response) {
                dispatch(onSuccess(response.data));
            })
            .catch(function(response){
                dispatch(onFailure(response));
            });
    }
}

function delete_(url, onRequest, onSuccess, onFailure) {
    return function(dispatch, getState) {
        dispatch(onRequest()); // Notify store a request is beginning
        return Axios.delete(url, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function(response) {
                dispatch(onSuccess(response.data));
            })
            .catch(function(response){
                dispatch(onFailure(response));
            });
    }
}

export function commentImage(url, comment) {
    let data = {
        comment: comment
    };
    return patch(url, data, commentImageRequest, commentImageSuccess, commentImageFailure)
}

export function approveImage(url, didClientApprove) {
    let data = {
        didClientApprove: didClientApprove,
        didClientReject: false
    };
    return patch(url, data, approveImageRequest, approveImageSuccess, approveImageFailure)
}

export function rejectImage(url, didClientReject) {
    let data = {
        didClientApprove: false,
        didClientReject: didClientReject
    };
    return patch(url, data, rejectImageRequest, rejectImageSuccess, rejectImageFailure)
}

export function deleteImage(url) {
    return function(dispatch) {
        dispatch(deleteImageRequest()); // Notify store a request is beginning
        return Axios.delete(url, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function(response) {
                dispatch(deleteImageSuccess(response.config.url));
            })
            .catch(function(response){
                dispatch(deleteImageFailure(response));
            });
    }
}

export function createImage(name, data) {
    return function(dispatch, getState) {
        let selectedAlbum = getState().albums.find(function(album) {
            return album.id == getState().selectedAlbumId;
        });
        let payload = {
            images: [
                {
                    booking: selectedAlbum.booking,
                    name: name,
                    image: data
                }
            ]
        };
        //console.log(payload);
        return patch(selectedAlbum.url, payload, createImageRequest, createImageSuccess, createImageFailure)(dispatch);
    }
}

export function retrieveImages(endpoint) {
    return get(endpoint, retrieveImagesRequest, retrieveImagesSuccess, retrieveImagesFailure);
}

export function retrieveAlbums(endpoint) {
    return get(endpoint, retrieveAlbumsRequest, retrieveAlbumsSuccess, retrieveAlbumsFailure)
}

export function selectLatestAlbum() {
    return function(dispatch, getState) {
        let latestAlbum = null;
        if (getState().albums) {
            latestAlbum = getState().albums.sort(dateCreatedComparator).get(-1);
            if (!_.isNil(latestAlbum)) {
                dispatch(selectAlbumById(latestAlbum.id))
            }
        }
    }
}

export function getSelectedAlbum() {
    return function(dispatch, getState) {
        let selectedAlbum = null;
        if (getState().selectedAlbumId) {
            selectedAlbum = getState().albums.find(function(album) {
                return album.id == getState().selectedAlbumId;
            });
        }
        return selectedAlbum;
    }
}

export function getAlbumById(id) {
    return function(dispatch, getState) {
        let album = getState().albums.find(function(album) {
            return album.id == id;
        });
        return album;
    }
}

export function createAlbum(url, name, width, height) {
    let payload = {
        name: name,
        width: width,
        height: height
    };
    return post(url, payload, createAlbumRequest, createAlbumSuccess, createAlbumFailure);
}

export function deleteAlbum(url) {
    return function(dispatch, getState) {
        dispatch(deleteAlbumRequest()); // Notify store a request is beginning
        return Axios.delete(url, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function(response) {
                dispatch(deleteAlbumSuccess(url));
            })
            .catch(function(response){
                dispatch(deleteAlbumFailure(response));
            });
    };
}

export function createSmartCroppedImage(image_url, name, width, height, album) {
    let payload = {
        images: [
            {
                image_url: image_url,
                album: album.id,
                name: name,
                width: width,
                height: height
            }
        ]
    };
    return patch(
        album.url + 'smart_crop/',
        payload,
        createSmartCroppedImageRequest,
        createSmartCroppedImageSuccess,
        createSmartCroppedImageFailure
    );
}

export function retrieveAlbum(endpoint) {
    return get(endpoint, retrieveAlbumRequest, retrieveAlbumSuccess, retrieveAlbumFailure)
}
