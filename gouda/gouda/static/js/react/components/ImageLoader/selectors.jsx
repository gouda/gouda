export function getAlbums(state) {
    return state.albums;
}

export function getSpinnerIds(state) {
    return state.spinnerIds;
}

export function getThumborSettings(state) {
    return state.thumborSettings;
}
