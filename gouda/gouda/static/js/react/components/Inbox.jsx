import React from 'react';
import Axios from 'axios';
import ReactCssTransitionGroup from 'react-addons-css-transition-group';
import Moment from 'moment';
import SanitizeHtml from 'sanitize-html';

class Message extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        let dateCreated = moment(this.props.message.dateCreated).fromNow();
        let sanitizedBody = SanitizeHtml(
            this.props.message.body,
            {
                allowedTags: [ 'b', 'i', 'em', 'strong']
            }
        );
        return (
            <div className="row message">
                <div className="hidden-xs col-sm-1 col-lg-1 col-image">
                    <img className="img-responsive sender-image" src={this.props.message.sender.thumbnailUrl}/>
                </div>
                <div className="col-xs-8 col-lg-9">
                    <h4 className="regular-font sender">{this.props.message.sender.firstName}</h4>
                    <p className="body" dangerouslySetInnerHTML={{__html: sanitizedBody.replace(/[\r\n]/g, "<br />")}}/>
                    <p>{this.props.user.id != this.props.message.sender.id ? (<a href="#ez-inbox-form">Reply</a>) : null}</p>
                </div>
                <div className="col-xs-2 col-lg-2">
                    <p className="text-right text-muted">{dateCreated}</p>
                </div>
            </div>
        )
    }
}

class Inbox extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            body: ''
        };
    }
    componentDidMount() {
        this.getMessages();
    }
    getMessages() {
        let component = this;
        Axios.get(this.props.endpoint)
            .then(function (response) {
                let state = Object.assign({}, component.state);
                state.messages = response.data.results;
                component.setState(state);
                console.log(state);
            })
            .catch(function (response) {
                //console.log(response);
            });
    }
    onBodyChange(element) {
        let state = Object.assign({}, this.state);
        state.body = element.target.value;
        this.setState(state);
    }
    onReplyButtonClicked(element) {
        let state = Object.assign({}, this.state);
        console.log('Click!');

        // Calculate new id....
        let id = 0;
        let isIdUnique = false;
        while (!isIdUnique) {
            id = Math.random() * 1000000;
            isIdUnique = true;
            for (let m of state.messages) {
                if (m.id == id) {
                    isIdUnique = false;
                    break;
                }
            }
        }

        //// Place message into view optimistically
        let message = {
            id: id,
            sender: this.props.user,
            body: state.body,
            dateCreated: moment().toISOString()
        };
        //state.messages = state.messages.concat([message]);
        state.body = '';
        this.setState(state);

        // Write new message to server
        let inbox = this;
        delete message.id;
        Axios.post(this.props.endpoint, message, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function (response) {
                console.log(response);
                let state = Object.assign({}, inbox.state);
                state.messages = state.messages.concat([response.data]);
                inbox.setState(state);
                inbox.getMessages(); // lazy...
            })
            .catch(function (response) {
                console.log(response);
            });
    }
    render() {
        let inbox = this;
        let messageNodes = this.state.messages.map(function(message, i, messages) {
            let divider = <hr/>;
            return (
                <div key={message.id}>
                    <Message key={message.id} message={message} user={inbox.props.user}/>
                    {i < (messages.length - 1) ? divider : null}
                </div>
            );
        });
        return (
            <div className="ez-inbox">
                <ReactCssTransitionGroup transitionName="message-transition" transitionEnterTimeout={250} transitionLeaveTimeout={1}>
                    {messageNodes}
                </ReactCssTransitionGroup>
                <div id="ez-inbox-form" className="message-form">
                    <hr/>
                    <div className="row">
                        <div className="col-xs-12">
                            <textarea className="k-textbox" value={this.state.body} onChange={this.onBodyChange.bind(this)}/>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-xs-12">
                            <button onClick={this.onReplyButtonClicked.bind(this)} type="button" className="k-primary k-button">Reply</button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

//Inbox.propTypes = {
//    endpoint: React.PropTypes.string.isRequired
//};

export default Inbox;