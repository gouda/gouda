import React from 'react';
import ReactCssTransitionGroup from 'react-addons-css-transition-group';
import Axios from 'axios';

class Chip extends React.Component {
    onDeleteChip(element) {
        this.props.onDeleteChip(this.props.chip);
    }
    render() {
        return (
            <div className="ez-chip">
                <span className="text">{this.props.chip.text}</span>
                {(this.props.readonly) ? null : (<span className="fa fa-times" onClick={this.onDeleteChip.bind(this)}/>)}
            </div>
        );
    }
}

class Chips extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            currentChip: {
                id: 0,
                text: ''
            },
            placeHolder: 'Type in the name of the item and press Enter',
            chips: []
        };
        this.addChip = this.addChip.bind(this);
        this.removeChip = this.removeChip.bind(this);
    }
    componentDidMount() {
        if (('endpoint' in this.props) && (this.props.endpoint)) {
            this.getChips();
        }
    }
    processResponse(response) {
        let state = Object.assign({}, this.state);
        state.chips = [];
        for (let i = 0; i < response.data.items.length; i++) {
            state.chips.push({
                id: i,
                text: response.data.items[i]
            });
        }
        state.currentChip = {
            id: response.data.items.length,
            text: ''
        };
        this.setState(state);
    }
    getChips() {
        let component = this;
        Axios.get(this.props.endpoint)
            .then(function (response) {
                component.processResponse(response);
            })
            .catch(function (response) {
                console.log(response);
            });
    }
    saveChips(chips) {
        let items = [];
        let component = this;
        for (let c of chips) {
            items.push(c.text);
        }
        Axios.patch(this.props.endpoint, {
                items: items,
            }, {
                xsrfCookieName: 'csrftoken',
                xsrfHeaderName: 'X-CSRFToken'
            })
            .then(function (response) {
                component.processResponse(response);
            })
            .catch(function (response) {
                console.log(response);
            });
    }
    onChipsClick(event) {
        if (this._input) {
            this._input.focus();
        }
    }
    onCurrentChipChange(event) {
        let state = Object.assign({}, this.state);
        state.currentChip.text = event.target.value;
        this.setState(state);
    }
    onCurrentChipKeyPress(event) {
        let isNumber = event.charCode >= 48 && event.charCode <= 57;
        let isUppercaseAlpha = event.charCode >= 65 && event.charCode <= 90;
        let isLowercaseAlpha = event.charCode >= 97 && event.charCode <= 122;
        let isBackspace = event.charCode == 8;
        let isSpace = event.charCode == 32;
        let isHyphen = event.charCode == 45;
        let isComma = event.charCode == 44;
        let isParentheses = event.charCode == 40 || event.charCode == 41;
        let isColon = event.charCode == 58;
        let isQuotation = event.charCode == 34 || event.charCode == 39;
        let isValidKeyCode = isNumber || isUppercaseAlpha || isLowercaseAlpha || isBackspace || isSpace || isHyphen || isParentheses || isColon || isQuotation;
        if (event.key == 'Enter') {
            if (this.state.currentChip.text && this.state.currentChip.text != '') {
                this.addChip(this.state.currentChip);
            }
            event.preventDefault();
        } else if (!isValidKeyCode) {
            event.preventDefault();
        }
    }
    addChip(chip) {
        let state = Object.assign({}, this.state);
        state.chips.push(chip);
        state.currentChip = {
            id: chip.id + 1,
            text: ''
        };
        //state.placeHolder = '';
        this.setState(state); // Optimistic...?

        this.saveChips(state.chips);

        // Update form element
        $(this.props.formElement).val(this.getValue());
    }
    removeChip(chip) {
        let state = Object.assign({}, this.state);
        state.chips.map(function(c, index) {
            if (c.id == chip.id) {
                state.chips.splice(index, 1);
            }
        });
        //if (state.chips.length <= 0) {
        //    state.placeHolder = 'Type in the name of the item and press Enter';
        //} else {
        //    state.placeHolder = '';
        //}
        this.setState(state);
        this.saveChips(state.chips);

    }
    getValue() {
        let value = '';
        for (let i = 0; i < this.state.chips.length; i++) {
            value += this.state.chips[i].text;
            if (i < this.state.chips.length - 1) {
                value += ',';
            }
        }
        return value;
    }
    render() {
        let chipsComponent = this;
        let chipNodes = this.state.chips.map(function(chip) {
            return (
                <Chip key={chip.id} chip={chip} readonly={chipsComponent.props.readonly} onDeleteChip={chipsComponent.removeChip}/>
            );
        });
        let input = (
            <input
                className="k-textbox"
                type="text"
                ref={(c) => this._input = c}
                onChange={this.onCurrentChipChange.bind(this)}
                onKeyPress={this.onCurrentChipKeyPress.bind(this)}
                value={this.state.currentChip.text}
                placeholder={this.state.placeHolder}
            />
        );
        return (
            <div className="ez-chips" onClick={this.onChipsClick.bind(this)}>
                {this.props.readonly ? null : input}
                    <ReactCssTransitionGroup transitionName="ez-chip-transition" transitionEnterTimeout={125} transitionLeaveTimeout={75}>
                        {chipNodes}
                    </ReactCssTransitionGroup>
            </div>
        );
    }
}
Chips.defaultProps = { readonly: false };

export default Chips;