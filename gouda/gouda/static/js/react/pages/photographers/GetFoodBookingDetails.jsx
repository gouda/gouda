import React from 'react';
import ReactDOM from 'react-dom';
import Moment from 'moment';

import Chips from '../../components/Chips.jsx';

$(document).ready(function() {
    function onChange() {
        var startTime = startTimePicker.value();

        if (startTime) {
            startTime = new Date(startTime);

            stopTimePicker.max(startTime);

            startTime.setMinutes(startTime.getMinutes() + this.options.interval);

            stopTimePicker.min(startTime);
            stopTimePicker.value(startTime);
        }
    }
    $("#id_service_type").kendoDropDownList({
        enable: false
    });

    var datePicker = $("#id_date").kendoDatePicker({
        min: Moment().add(1, 'day').toDate()
    }).data("kendoDatePicker");

    var startTimePicker = $("#id_start_time").kendoTimePicker({
        change: onChange
    }).data("kendoTimePicker");

    var stopTimePicker = $("#id_stop_time").kendoTimePicker({
    }).data("kendoTimePicker");
});

window.GetFoodBookingDetails = {
    init(opts) {
        ReactDOM.render(
            <Chips formElement={opts.itemsFormElement}/>,
            document.querySelector(opts.el)
        );
    }
};