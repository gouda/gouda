//import Axios from 'axios';
import Moment from 'moment';

$(document).ready(function() {
    //let geocoder = new google.maps.Geocoder();
    //geocoder.geocode({ 'address': window.ScheduleBooking.address }, function(results, status) {
    //    if (status == google.maps.GeocoderStatus.OK) {
    //        let latitude = results[0].geometry.location.lat();
    //        let longitude = results[0].geometry.location.lng();
    //        $("#id_latitude").val(latitude);
    //        $("#id_longitude").val(longitude);
    //        let url = `https://maps.googleapis.com/maps/api/timezone/json?location=${latitude},${longitude}&timestamp=${(new Date()).getTime() / 1000}&key=${window.ScheduleBooking.GoogleApiKey}`;
    //        console.log(url);
    //        Axios.get(url)
    //            .then(function(response){
    //                $("#id_time_zone").val(response.data.timeZoneId);
    //                console.log(response)
    //            })
    //            .catch(function(response){});
    //        console.log(results[0]);
    //    }
    //});
    function onChange() {
        var startTime = startTimePicker.value();

        if (startTime) {
            startTime = new Date(startTime);
            endTimePicker.min(startTime);
            endTimePicker.value(Moment(startTime).add(3, 'hour').toDate());
            endTimePicker.max("20:00");
        }
    }
    var datePicker = $("#id_date").kendoDatePicker({
        min: new Date((new Date()).getTime() + 10 * 24 * 60 * 60 * 1000),
        disableDates: window.ScheduleBooking.invalidDates
    }).data("kendoDatePicker");
    var startTimePicker = $("#id_start_time").kendoTimePicker({
        change: onChange,
        min: "08:00",
        max: "17:00"
    }).data("kendoTimePicker");
    var endTimePicker = $("#id_stop_time").kendoTimePicker({
        min: "11:00",
        max: "20:00"
    }).data("kendoTimePicker");
});
