//import Axios from 'axios';
import Moment from 'moment';

$(document).ready(function() {
    function onChange() {
        var startTime = startTimePicker.value();

        if (startTime) {
            startTime = new Date(startTime);
            endTimePicker.min(startTime);
            endTimePicker.value(Moment(startTime).add(3, 'hour').toDate());
            endTimePicker.max("20:00");
        }
    }
    $("#id_region").kendoDropDownList({
    });

    var datePicker = $("#id_date").kendoDatePicker({
        min: new Date((new Date()).getTime() + 10 * 24 * 60 * 60 * 1000),
        disableDates: window.CreateFoodBooking.invalidDates
    }).data("kendoDatePicker");
    var startTimePicker = $("#id_start_time").kendoTimePicker({
        change: onChange,
        min: "08:00",
        max: "17:00"
    }).data("kendoTimePicker");
    var endTimePicker = $("#id_stop_time").kendoTimePicker({
        min: "11:00",
        max: "20:00"
    }).data("kendoTimePicker");
});
