import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import _ from 'lodash';
import Moment from 'moment';

import Inbox from '../../components/Inbox.jsx';
import Chips from '../../components/Chips.jsx';

import { createStore } from '../../components/ImageLoader/stores.jsx';
import { default as ImageLoader } from '../../components/ImageLoader/components/Root.jsx' ;

window.ShootDetail = {
    init(opts) {
        let imageLoaderStore = createStore({
            endpoint: opts.imageLoader.endpoint,
            isClientMode: false,
            placeholderUrl: opts.imageLoader.placeholderUrl,
            thumborSettings: {
                key: opts.imageLoader.thumborSettings.key,
                url: opts.imageLoader.thumborSettings.url
            }
        });

        //ReactDOM.render(
        //    <ImageLoader endpoint={opts.imageLoader.endpoint} isClientMode={false}/>,
        //    document.querySelector(opts.imageLoader.el)
        //);
        ReactDOM.render(
            <Inbox endpoint={opts.inbox.endpoint} user={opts.inbox.user}/>,
            document.querySelector(opts.inbox.el)
        );
        ReactDOM.render(
            <Provider store={imageLoaderStore}>
                <ImageLoader/>
            </Provider>,
            document.querySelector(opts.imageLoader.el)
        )
        if (!_.isNil(opts.chips)) {
            ReactDOM.render(
                <Chips endpoint={opts.chips.endpoint} readonly={opts.chips.readonly}/>,
                document.querySelector(opts.chips.el)
            );
        }
    }
};

$(document).ready(function() {
    function onChange() {
        var startTime = startTimePicker.value();

        if (startTime) {
            startTime = new Date(startTime);

            stopTimePicker.max(startTime);

            startTime.setMinutes(startTime.getMinutes() + this.options.interval);

            stopTimePicker.min(startTime);
            stopTimePicker.value(startTime);
        }
    }
    $("#id_service_type").kendoDropDownList({
        enable: false
    });

    var datePicker = $("#id_date").kendoDatePicker({
        min: Moment().add(1, 'day').toDate()
    }).data("kendoDatePicker");

    var startTimePicker = $("#id_start_time").kendoTimePicker({
        change: onChange
    }).data("kendoTimePicker");

    var stopTimePicker = $("#id_stop_time").kendoTimePicker({
    }).data("kendoTimePicker");
});

//console.log(store.getState());
//
//// Every time the state changes, log it
//// Note that subscribe() returns a function for unregistering the listener
//let unsubscribe = store.subscribe(function() {
//    console.log(store.getState());
//});
//
//// Dispatch some actions
//store.dispatch(showSpinner(true));
