$(document).ready(function() {
    $(window).scroll(function() {
        if ($(window).scrollTop() > 14 ) {
            $('#navbar').addClass('shadow');
            $('#dashboard-sidebar').addClass('fixed');
            let dashboardContent = $('#dashboard-content');
            dashboardContent.addClass('col-xs-offset-3');
            dashboardContent.addClass('col-lg-offset-2');
        } else {
            $('#navbar').removeClass('shadow');
            $('#dashboard-sidebar').removeClass('fixed');
            let dashboardContent = $('#dashboard-content');
            dashboardContent.removeClass('col-xs-offset-3');
            dashboardContent.removeClass('col-lg-offset-2');
        }
    });
});
