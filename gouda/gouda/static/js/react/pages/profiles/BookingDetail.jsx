import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import _ from 'lodash';

import Inbox from '../../components/Inbox.jsx';
import Chips from '../../components/Chips.jsx';

import { createStore } from '../../components/ImageLoader/stores.jsx';
import { default as ImageLoader } from '../../components/ImageLoader/components/Root.jsx' ;

window.BookingDetail = {
    init(opts) {
        let imageLoaderStore = createStore({
            endpoint: opts.imageLoader.endpoint,
            isClientMode: true,
            downloadUrl: opts.imageLoader.downloadUrl,
            placeholderUrl: opts.imageLoader.placeholderUrl
        });
        ReactDOM.render(
            <Provider store={imageLoaderStore}>
                <ImageLoader/>
            </Provider>,
            document.querySelector(opts.imageLoader.el)
        );
        ReactDOM.render(
            <Inbox endpoint={opts.inbox.endpoint} user={opts.inbox.user}/>,
            document.querySelector(opts.inbox.el)
        );
        if (!_.isNil(opts.chips)) {
            ReactDOM.render(
                <Chips endpoint={opts.chips.endpoint} readonly={opts.chips.readonly}/>,
                document.querySelector(opts.chips.el)
            );
        }

    }
};