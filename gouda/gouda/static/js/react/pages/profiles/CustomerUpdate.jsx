import Axios from 'axios';

$(document).ready(function() {
    $('#id_card_number').formatter({
        'pattern': '{{9999}} {{9999}} {{9999}} {{9999}}'
    });

    $('#id_expiration_date').formatter({
        'pattern': '{{99}}/{{99}}'
    });

    $('#id_security_code').formatter({
        'pattern': '{{9999}}'
    });

    var didGetPaymentMethodNonce = false;
    $('#customer-update-form').submit(function(event) {
        if (didGetPaymentMethodNonce) {
            $('#id_card_number').prop('disabled', true);
            $('#id_expiration_date').prop('disabled', true);
            $('#id_security_code').prop('disabled', true);
            return;
        }
        $('#rectangle-spinner').removeClass('hidden');
        // Get client token from server to initialize Braintree client
        Axios.get('/api/braintree/clienttoken/')
            .then(function(response) {
                let braintreeClient = new braintree.api.Client({clientToken: response.data.clientToken});
                braintreeClient.tokenizeCard({
                    number: $('#id_card_number').val(),
                    expirationDate: $('#id_expiration_date').val(),
                    cvv: $('#id_security_code').val()
                }, function (error, paymentMethodNonce) {
                    if (error) {
                        $('#error-message').removeClass('hidden');
                        $('#rectangle-spinner').addClass('hidden');
                    } else {
                        didGetPaymentMethodNonce = true;
                        $('#id_payment_method_nonce').val(paymentMethodNonce);
                        $('#customer-update-form').submit();
                    }
                });
            })
            .catch(function(response) {
                $('#error-message').removeClass('hidden');
                $('#rectangle-spinner').addClass('hidden');
            });
        event.preventDefault();
    })
});