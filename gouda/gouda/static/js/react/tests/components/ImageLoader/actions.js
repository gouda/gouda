/**
 * Created by chukwuemeka on 6/27/16.
 */

import Test from 'tape';
import * as actions from '../../../components/ImageLoader/actions';

Test('Increment list offset', function(t) {
    let amount = 20;
    t.deepEqual(actions.incrementListOffset(amount), {
        type: actions.INCREMENT_LIST_OFFSET,
        payload: {
            amount: amount
        }
    });
    t.end();
});

Test('Decrement list offset', function(t) {
    let amount = 20;
    t.deepEqual(actions.decrementListOffset(amount), {
        type: actions.DECREMENT_LIST_OFFSET,
        payload: {
            amount: amount
        }
    });
    t.end();
});

Test('Reset list offset', function(t) {
    t.deepEqual(actions.resetListOffset(), {
        type: actions.RESET_LIST_OFFSET
    });
    t.end();
});

Test('Show image detail', function(t) {
    t.deepEqual(actions.showImageDetail(), {
        type: actions.SHOW_IMAGE_DETAIL
    });
    t.end();
});

Test('Hide image detail', function(t) {
    t.deepEqual(actions.hideImageDetail(), {
        type: actions.HIDE_IMAGE_DETAIL
    });
    t.end();
});

Test('Select image by id', function(t) {
    let id = 1;
    t.deepEqual(actions.selectImageById(id), {
        type: actions.SELECT_IMAGE_BY_ID,
        payload: {
            id: id
        }
    });
    t.end();
});

Test('Retrieve image metadata request', function(t) {
    let url = 'url';
    let id = 1;
    t.deepEqual(actions.retrieveImageMetadataRequest(id, url), {
        type: actions.RETRIEVE_IMAGE_METADATA_REQUEST,
        payload: {
            id: id,
            url: url
        }
    });
    t.end();
});

Test('Retrieve image metadata success', function(t) {
    let id = 1;
    let metadata = {};
    t.deepEqual(actions.retrieveImageMetadataSuccess(id, metadata), {
        type: actions.RETRIEVE_IMAGE_METADATA_SUCCESS,
        payload: {
            id: id,
            metadata: metadata
        }
    });
    t.end();
});

Test('Retrieve image metadata failure', function(t) {
    let id = 1;
    let error = {};
    t.deepEqual(actions.retrieveImageMetadataFailure(id, error), {
        type: actions.RETRIEVE_IMAGE_METADATA_FAILURE,
        payload: {
            id: id,
            error: error
        }
    });
    t.end();
});

Test('Update album request', function(t) {
    let url = 'url';
    let data = {name: 'fsdafs'};
    t.deepEqual(actions.updateAlbumRequest(url, data), {
        type: actions.UPDATE_ALBUM_REQUEST,
        payload: {
            url: url,
            data: data
        }
    });
    t.end();
});

Test('Update album success', function(t) {
    let album = {};
    t.deepEqual(actions.updateAlbumSuccess(album), {
        type: actions.UPDATE_ALBUM_SUCCESS,
        payload: {
            album: album
        }
    });
    t.end();
});

Test('Update album failure', function(t) {
    let error = {};
    t.deepEqual(actions.updateAlbumFailure(error), {
        type: actions.UPDATE_ALBUM_FAILURE,
        payload: {
            error: error
        }
    });
    t.end();
});

Test('Create smart cropped album request', function(t) {
    let url = 'url';
    let name = 'fdafsfas';
    let width = 500;
    let height = 500;
    let sourceAlbum = {
        images: [],
        name: 'fdfasfdsfas'
    };
    t.deepEqual(actions.createSmartCroppedAlbumRequest(url, name, width, height, sourceAlbum), {
        type: actions.CREATE_SMART_CROPPED_ALBUM_REQUEST,
        payload: {
            url: url,
            sourceAlbum: sourceAlbum,
            album: {
                name: name,
                width: width,
                height: height,
            }
        }
    });
    t.end();
});

Test('Create smart cropped album success', function(t) {
    let album = {};
    t.deepEqual(actions.createSmartCroppedAlbumSuccess(album), {
        type: actions.CREATE_SMART_CROPPED_ALBUM_SUCCESS,
        payload: {
            album: album
        }
    });
    t.end();
});

Test('Create smart cropped album failure', function(t) {
    let error = {};
    t.deepEqual(actions.createSmartCroppedAlbumFailure(error), {
        type: actions.CREATE_SMART_CROPPED_ALBUM_FAILURE,
        payload: {
            error: error
        }
    });
    t.end();
});
