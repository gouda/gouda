/**
 * Created by chukwuemeka on 6/27/16.
 */

import Immutable from 'immutable';
import Test from 'tape';
import * as actions from '../../../components/ImageLoader/actions';
import * as reducers from '../../../components/ImageLoader/reducers';

Test('Initial state for list offset', function(t) {
    t.deepEqual(reducers.listOffset(undefined, {}), 0);
    t.end();
});

Test('Increment list offset', function(t) {
    let amount = 20;
    let action = actions.incrementListOffset(amount);
    t.deepEqual(reducers.listOffset(undefined, action), amount);
    t.end();
});

Test('Decrement list offset', function(t) {
    let amount = 20;
    let action = actions.decrementListOffset(amount);
    t.deepEqual(reducers.listOffset(40, action), amount);
    t.end();
});

Test('Decrement list offset past zero', function(t) {
    let amount = 20;
    let action = actions.decrementListOffset(amount);
    t.deepEqual(reducers.listOffset(0, action), 0);
    t.end();
});

Test('Reset list offset', function(t) {
    let action = actions.resetListOffset();
    t.deepEqual(reducers.listOffset(0, action), 0);
    t.end();
});

Test('Reset list offset on album change', function(t) {
    let action = actions.selectAlbumById(1);
    t.deepEqual(reducers.listOffset(20, action), 0);
    t.end();
});

Test('Initial state for showing image detail', function(t) {
    t.deepEqual(reducers.doShowImageDetail(undefined, {}), false);
    t.end();
});

Test('Show image detail', function(t) {
    let action = actions.showImageDetail();
    t.deepEqual(reducers.doShowImageDetail(undefined, action), true);
    t.end();
});

Test('Hide image detail', function(t) {
    let action = actions.hideImageDetail();
    t.deepEqual(reducers.doShowImageDetail(undefined, action), false);
    t.end();
});

Test('Select image by id', function(t) {
    let id = 1;
    let action = actions.selectImageById(id);
    t.deepEqual(reducers.selectedImageId(undefined, action), id);
    t.end();
});

Test('Initial state for selected image id', function(t) {
    t.deepEqual(reducers.selectedImageId(undefined, {}), null);
    t.end();
});

Test('Initial state for image metadata requests', function(t) {
    t.deepEqual(reducers.imageMetadataRequests(undefined, {}), Immutable.Map({}));
    t.end();
});

Test('Successful image metadata request', function(t) {
    let id = 1;
    let metadata = {};
    let action = actions.retrieveImageMetadataSuccess(id, metadata);
    let expectedValue = Immutable.Map({});
    expectedValue = expectedValue.set(action.payload.id, {
        isSuccess: true,
        payload: action.payload.metadata
    });
    t.deepEqual(reducers.imageMetadataRequests(undefined, action), Immutable.Map(expectedValue));
    t.end();
});

Test('Failed image metadata request', function(t) {
    let id = 1;
    let error = {};
    let action = actions.retrieveImageMetadataFailure(id, error);
    let expectedValue = Immutable.Map({});
    expectedValue = expectedValue.set(action.payload.id, {
        isSuccess: false,
        payload: action.payload.error
    });
    t.deepEqual(reducers.imageMetadataRequests(undefined, action), Immutable.Map(expectedValue));
    t.end();
});

//Test('incrementOffset', function(t) {
//  let amount = 20;
//  let action = actions.incrementOffset(amount);
//  t.deepEqual(, {
//    type: actions.INCREMENT_OFFSET,
//    payload: {
//      amount: amount
//    }
//  });
//  t.end();
//});
//
//Test('decrementOffset', function(t) {
//  let amount = 20;
//  t.deepEqual(actions.decrementOffset(amount), {
//    type: actions.DECREMENT_OFFSET,
//    payload: {
//      amount: amount
//    }
//  });
//  t.end();
//});
//
//Test('resetOffset', function(t) {
//  t.deepEqual(actions.resetOffset(), {
//    type: actions.RESET_OFFSET
//  });
//  t.end();
//});
