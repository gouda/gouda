from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals
import sys
import os
import braintree
from django.core.urlresolvers import reverse_lazy
from psycopg2.extras import NumericRange
from datetime import timedelta
import ast
import imp
from celery.schedules import crontab

APP_NAME = 'gouda'
PROJECT_DIR = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
BOWER_COMPONENTS_DIR = os.path.join(PROJECT_DIR, 'bower_components')
BASE_DIR = os.path.join(PROJECT_DIR, APP_NAME)
APP_DIR = os.path.join(BASE_DIR, APP_NAME)
RUN_DIR = '/var/run/%s/' % APP_NAME
if not os.path.exists(RUN_DIR):
    os.makedirs(RUN_DIR)


IS_TEST_MODE = len(sys.argv) > 1 and sys.argv[1] == 'test'
IS_PRODUCTION = ast.literal_eval(os.environ['IS_PRODUCTION_ENVIRONMENT'])
IS_COLLECT_STATICS_ENVIRONMENT = len(sys.argv) > 1 and sys.argv[1] == 'collectstatic'  # ast.literal_eval(os.environ.get('IS_COLLECT_STATICS_ENVIRONMENT', 'False'))

# IS_LOCAL = 'chukwuemeka' in socket.gethostname().lower() or 'new-host' in socket.gethostname()
ADMINS = (
    ('Chukwuemeka Ezekwe', 'cue0083@gmail.com'),
    ('Chukwuemeka Ezekwe', 'emeka@shotzu.com'),
)
if not IS_PRODUCTION:
    STAFF = (
        ('Chukwuemeka Ezekwe', 'cue0083@gmail.com'),
        ('Chukwuemeka Ezekwe', 'emeka@shotzu.com'),
    )
else:
    STAFF = (
        ('Chukwuemeka Ezekwe', 'cue0083@gmail.com'),
        ('Chukwuemeka Ezekwe', 'emeka@shotzu.com'),
        ('Julian Everly', 'julian@julianeverly.com'),
        ('Julian Everly', 'julian@shotzu.com'),
        ('Susie Jerez', 'susie@shotzu.com'),
        # ('Leigh Gordon', 'leighgordon49@gmail.com'),
    )

# Which site...?
SITE_ID = 1
SITE_DOMAIN = os.environ['SITE_DOMAIN']  # 'localhost:5031'
SITE_NAME = os.environ['SITE_NAME']  # 'SHOTZU'
SITE_DISPLAY_NAME = os.environ['SITE_DISPLAY_NAME']  # 'SHOTZU'

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%yb^e0oqkl#176&70x%%i#n)(0m$d$g-gr2yx(dvr0+2i_uw&e'

# SECURITY WARNING: don't run with debug turned on in production!
# DEBUG = IS_LOCAL or os.environ['DEBUG'] == 'True'
DEBUG = ast.literal_eval(os.environ.get('DEBUG', 'False').capitalize())
# IS_PRODUCTION = not DEBUG and not IS_LOCAL

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.template.context_processors.request',
                'django.contrib.messages.context_processors.messages',
                'social.apps.django_app.context_processors.backends',
                'social.apps.django_app.context_processors.login_redirect',
                'gouda.context_processors.api_keys',
                'gouda.context_processors.constants',
            ],
            'debug': DEBUG
        },
        'DIRS': [
            os.path.join(APP_DIR, 'templates/'),
        ]
    },
]

INTERNAL_IPS = (
    'localhost',
    'localhost:4577',
    '127.0.0.1'
)

ALLOWED_HOSTS = [
    'shotzu.com',
    '192.241.247.136',
    'www.shotzu.com'
]

AUTH_USER_MODEL = 'users.User'

# Application definition

INSTALLED_APPS = [
    # 'grappelli',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.gis',
    'django.contrib.sites',
    'social.apps.django_app.default',
    'kombu.transport.django',
    'djcelery',
    'storages',
    'seacucumber',
    'easy_thumbnails',
    'django_extensions',
    'django_countries',
    'compressor',
    'admin_honeypot',
    'haystack',
    'hijack',
    'compat',
    'hijack_admin',
    'backups',
    # 'easy_timezones',
    'gouda.templatetags',
    'gouda.postmaster',
    'gouda.users',
    'gouda.profiles',
    'gouda.favorites',
    'gouda.customers',
    'gouda.listings',
    'gouda.addresses',
    'gouda.photographers',
    'gouda.instagram_bots',
    'gouda.twitter_bots',
    'gouda.utils',
    'gouda.anansi',
    'gouda.homeaway_bots',
]
# INSTALLED_APPS.append('django.contrib.sites')
#
# if not IS_TEST_MODE:
#     INSTALLED_APPS.append('django.contrib.sites')

MIDDLEWARE_CLASSES = (
    'django.middleware.cache.UpdateCacheMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'easy_timezones.middleware.EasyTimezoneMiddleware',
    'gouda.profiles.middleware.UnverifiedProfileSessionMiddleware',

    # 'django.middleware.cache.FetchFromCacheMiddleware',
)

ROOT_URLCONF = 'gouda.urls'

WSGI_APPLICATION = 'gouda.wsgi.application'

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'en-us'

USE_I18N = True

USE_L10N = True

# Time
USE_TZ = True
TIME_INPUT_FORMATS = (
    '%H:%M:%S',     # '14:30:59'
    '%H:%M:%S.%f',  # '14:30:59.000200'
    '%H:%M',        # '14:30'
    '%I:%M %p',
    '%I:%M%p'
)
TIME_ZONE = 'UTC'

# GeoIP
GEOIP_PATH = '/usr/share/GeoIP/'
# GEOIP_LIBRARY_PATH = '/usr/lib/x86_64-linux-gnu/libGeoIP.so'
# GEOIP_DATABASE = os.path.join(GEOIP_PATH, 'GeoLiteCity.dat')
# GEOIPV6_DATABASE = os.path.join(GEOIP_PATH, 'GeoIPv6.dat')

# Email
SERVER_EMAIL = 'Bot <bot@shotzu.com>'
DEFAULT_FROM_EMAIL = 'The SHOTZU Crew <hello@shotzu.com>'
SUPPORT_EMAIL = 'support@shotzu.com'
EMAIL_CLOSING = 'Peace,\nThe %s Crew' % SITE_DISPLAY_NAME
STAFF_EMAILS = [staff_email[1] for staff_email in STAFF]
EMAIL_BACKEND = os.environ['EMAIL_BACKEND']

# Media files
MIN_RENTAL_IMAGES = 1
MAX_RENTAL_IMAGES = 20
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = 50.0
# if IS_LOCAL:
#     MEDIA_URL = '/media/'
#     MEDIA_ROOT = os.path.join(BASE_DIR, 'media/')
#     DEFAULT_FILE_STORAGE = 'gouda.storages.FileSystemStorage'
# else:
#     DEFAULT_FILE_STORAGE = 'gouda.storages.S3MediaStorage'
#     MEDIAFILES_LOCATION = 'media'
#     MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
# THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE

# BACKUPS
BACKUPS_BACKUP_DIRECTORY = '/backups'
BACKUPS_BACKUP_CLEANUP_LIMIT = 9000


# Logging
def get_logging(logs_root):
    logging = {
        'version': 1,
        'disable_existing_loggers': False,
        'formatters': {
            'verbose': {
                'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
            },
            'standard': {
                'format': '%(levelname)s %(message)s'
            },
            'basic': {
                'format': '%(message)s'
            },
        },
        'filters': {
            'require_debug_false': {
                '()': 'django.utils.log.RequireDebugFalse',
            }
        },
        'handlers': {
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'INFO' if not IS_TEST_MODE else 'CRITICAL',
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.FileHandler',
                'filename': os.path.join(logs_root, 'django.log'),
            },
            'mail_admins': {
                'level': 'ERROR',
                'filters': ['require_debug_false'],
                'class': 'django.utils.log.AdminEmailHandler',
                'include_html': True
            },
            'rotating_file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': os.path.join(logs_root, 'django.log'),
                'maxBytes': 1024 * 1024 * 10,  # 10MB
                'backupCount': 10,
                'formatter': 'verbose',
            },
        },
        'loggers': {
            'stripe': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'WARNING',
                'propagate': False
            },
            'django.request': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'WARNING',
                'propagate': False
            },
            'django.security': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'WARNING',
                'propagate': False
            },
            'urllib3': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'ERROR',
                'propagate': False
            },
            'requests': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'ERROR',
                'propagate': False
            },
            'elasticsearch.trace': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'gouda.backups.tasks': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'gouda.photographers.tasks': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'gouda.addresses.tasks': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'gouda.profiles.tasks': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            'celery.task': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
            '': {
                'handlers': ['console', 'rotating_file', 'mail_admins'],
                'level': 'INFO',
                'propagate': False,
            },
        },
    }

    return logging
LOGS_ROOT = '/var/log/django'
if not os.path.exists(LOGS_ROOT):
    os.makedirs(LOGS_ROOT)
LOGGING = get_logging(LOGS_ROOT)

# Misc
LOGOUT_URL = reverse_lazy('index')
LOGIN_REDIRECT_URL = reverse_lazy('index')
LOGIN_URL = reverse_lazy('profiles:login')
LOGIN_ERROR_URL = reverse_lazy('index')
VERIFY_EMAIL_ADDRESS_URL_TIMEOUT_DAYS = sys.maxint
UNVERIFIED_PROFILE_SESSION_COOKIE_AGE = 86400
AUTHENTICATION_BACKENDS = [
    'gouda.users.backends.VerificationEmailTokenBackend',
    'django.contrib.auth.backends.ModelBackend',
    'social.backends.facebook.FacebookOAuth2',
]

# Django REST Framework
REST_FRAMEWORK = {
    # 'DEFAULT_AUTHENTICATION_CLASSES': (
    #     'rest_framework.authentication.SessionAuthentication',
    # ),
    #
    # 'DEFAULT_PERMISSION_CLASSES': (
    #     'rest_framework.permissions.IsAuthenticated',
    # ),
    'DEFAULT_FILTER_BACKENDS': ('rest_framework.filters.DjangoFilterBackend',),

    'DEFAULT_PAGINATION_CLASS': 'gouda.pagination.LimitOffsetPagination',

    'DEFAULT_RENDERER_CLASSES': (
        'djangorestframework_camel_case.render.CamelCaseJSONRenderer',
    ),

    'DEFAULT_PARSER_CLASSES': (
        'djangorestframework_camel_case.parser.CamelCaseJSONParser',
    ),
}

# Thumbnails
THUMBNAIL_ALIASES = {
    'profiles.Profile.image': {
        '160x160': {'size': (160, 160), 'crop': True},
        '80x80': {'size': (80, 80), 'crop': True},
        '35x35': {'size': (35, 35), 'crop': True},
    },
    'photographers.BookingImage.image': {
        '500x0': {'size': (500, 0), 'crop': True},
    },
}
THUMBNAIL_PRESERVE_EXTENSIONS = ('jpg',)

# Social media
INSTAGRAM_CLIENT_ID = 'a5a4afe245164f3fbf86166239b4d4fc'
INSTAGRAM_CLIENT_SECRET = 'c5c5197e356f4e5f86e93b523cfe66ed'
INSTAGRAM_REDIRECT_URI = 'https://www.google.com'
INSTAGRAM_SCOPE = ['basic', 'comments', 'relationships', 'likes']
FACEBOOK_PUBLIC_KEY = os.environ['FACEBOOK_PUBLIC_KEY']
FACEBOOK_SECRET_KEY = os.environ['FACEBOOK_SECRET_KEY']
FACEBOOK_APP_ID = FACEBOOK_PUBLIC_KEY
SOCIAL_AUTH_LOGIN_REDIRECT_URL = LOGIN_REDIRECT_URL
SOCIAL_AUTH_FACEBOOK_KEY = FACEBOOK_PUBLIC_KEY
SOCIAL_AUTH_FACEBOOK_SECRET = FACEBOOK_SECRET_KEY
SOCIAL_AUTH_FACEBOOK_SCOPE = ['email', 'user_birthday', 'public_profile', 'user_hometown', 'user_location']
USER_FIELDS = ['full_name', 'email_address']
USERNAME_IS_FULL_EMAIL = True
SOCIAL_AUTH_PIPELINE = (
    # Get the information we can about the user and return it in a simple
    # format to create the user instance later. On some cases the details are
    # already part of the auth response from the provider, but sometimes this
    # could hit a provider API.
    'social.pipeline.social_auth.social_details',

    # Get the social uid from whichever service we're authing thru. The uid is
    # the unique identifier of the given user in the provider.
    'social.pipeline.social_auth.social_uid',

    # Verifies that the current auth process is valid within the current
    # project, this is were emails and domains whitelists are applied (if
    # defined).
    'social.pipeline.social_auth.auth_allowed',

    # Checks if the current social-account is already associated in the site.
    'social.pipeline.social_auth.social_user',

    # Make up a username for this person, appends a random string at the end if
    # there's any collision.
    # 'social.pipeline.user.get_username',

    # Send a validation email to the user to verify its email address.
    # Disabled by default.
    # 'social.pipeline.mail.mail_validation',

    # Associates the current social details with another user account with
    # a similar email address. Disabled by default.
    # 'social.pipeline.social_auth.associate_by_email',

    # Create a user account if we haven't found one yet.
    # 'social.pipeline.user.create_user',
    'gouda.social.pipeline.create_user',

    # Create the record that associated the social account with this user.
    'social.pipeline.social_auth.associate_user',

    # Populate the extra_data field in the social record with the values
    # specified by settings (and the default ones like access_token, etc).
    'social.pipeline.social_auth.load_extra_data',

    # Update the user record with any changed info from the auth service.
    # 'social.pipeline.user.user_details',

    # Create a user profile
    'gouda.social.pipeline.create_profile'
)

# Braintree
BRAINTREE_MERCHANT_ACCOUNT_ID = os.environ['BRAINTREE_MERCHANT_ACCOUNT_ID']
BRAINTREE_MERCHANT_ID = os.environ['BRAINTREE_MERCHANT_ID']
BRAINTREE_PUBLIC_KEY = os.environ['BRAINTREE_PUBLIC_KEY']
BRAINTREE_PRIVATE_KEY = os.environ['BRAINTREE_PRIVATE_KEY']
BRAINTREE_ENVIRONMENT = eval(os.environ['BRAINTREE_ENVIRONMENT'])
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = int(os.environ['BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS'])

# # Stripe
STRIPE_SECRET_KEY = 'sk_test_WxmKBXQijNw9IfzWZPk5H2yw'
STRIPE_PUBLIC_KEY = 'pk_test_4p8flX8l034kwFhUFeSlI2XF'
MONTHLY_PLAN_ID = 'com.theislandcrew.gouda.plans.monthly'
YEARLY_PLAN_ID = 'com.theislandcrew.gouda.plans.yearly'

# AWS
AWS_S3_SECURE_URLS = True
AWS_HEADERS = {
    'Cache-Control': 'max-age=86400',
}
AWS_CLOUDFRONT_DOMAIN = os.environ['AWS_CLOUDFRONT_DOMAIN']
AWS_STORAGE_BUCKET_NAME = os.environ['AWS_STORAGE_BUCKET_NAME']
AWS_ACCESS_KEY_ID = os.environ['AWS_ACCESS_KEY_ID']
AWS_SECRET_ACCESS_KEY = os.environ['AWS_SECRET_ACCESS_KEY']
AWS_AUTO_CREATE_BUCKET = True
if ast.literal_eval(os.environ['IS_AWS_CLOUDFRONT_ENABLED']):
    AWS_S3_CUSTOM_DOMAIN = AWS_CLOUDFRONT_DOMAIN
    S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN
else:
    AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
    S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN

# Media
DO_USE_MEDIA_S3_STORAGE = ast.literal_eval(os.environ['IS_AWS_S3_MEDIA_STORAGE_ENABLED'])
DO_USE_MEDIA_S3_STORAGE = not IS_TEST_MODE and DO_USE_MEDIA_S3_STORAGE
if DO_USE_MEDIA_S3_STORAGE:
    DEFAULT_FILE_STORAGE = 'gouda.storages.S3MediaStorage'
    MEDIAFILES_LOCATION = 'media'
    MEDIA_URL = '%s/%s/' % (S3_URL, MEDIAFILES_LOCATION)
    THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE
else:
    MEDIA_URL = '/media/'
    MEDIA_ROOT = os.path.join(RUN_DIR, 'media')
    DEFAULT_FILE_STORAGE = 'gouda.storages.FileSystemStorage'
    THUMBNAIL_DEFAULT_STORAGE = DEFAULT_FILE_STORAGE

# Static
STATICFILES_VERSION = os.environ['STATICFILES_VERSION']
STATICFILES_LOCATION = 'static/%s' % STATICFILES_VERSION
STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    # other finders..
    'compressor.finders.CompressorFinder',
)
if IS_COLLECT_STATICS_ENVIRONMENT:
    STATICFILES_DIRS = (
        os.path.join(APP_DIR, 'static/'),
        ('bower_components/kendo-ui/styles/Material', os.path.join(BOWER_COMPONENTS_DIR, 'kendo-ui/styles/Material/')),
        ('bower_components/kendo-ui/styles/images', os.path.join(BOWER_COMPONENTS_DIR, 'kendo-ui/styles/images/')),
        ('bower_components/kendo-ui/styles/fonts', os.path.join(BOWER_COMPONENTS_DIR, 'kendo-ui/styles/fonts/')),
        ('bower_components/bootstrap/dist/fonts', os.path.join(BOWER_COMPONENTS_DIR, 'bootstrap/dist/fonts/')),
        ('bower_components/font-awesome/fonts', os.path.join(BOWER_COMPONENTS_DIR, 'font-awesome/fonts/')),
    )
else:
    STATICFILES_DIRS = (
        os.path.join(APP_DIR, 'static/'),
        ('bower_components', BOWER_COMPONENTS_DIR),
    )
do_use_static_s3_storage = ast.literal_eval(os.environ['IS_AWS_S3_STATIC_STORAGE_ENABLED'])
if do_use_static_s3_storage:
    STATIC_ROOT = os.path.join(BASE_DIR, 'static')
    if not os.path.exists(STATIC_ROOT):
        os.makedirs(STATIC_ROOT)
    STATICFILES_STORAGE = 'gouda.storages.CachedS3BotoStaticStorage'
    STATIC_URL = '%s/%s/' % (S3_URL, STATICFILES_LOCATION)
else:
    STATIC_URL = '/static/'
    STATICFILES_STORAGE = 'django.contrib.staticfiles.storage.StaticFilesStorage'
    STATIC_ROOT = os.path.join(RUN_DIR, 'static')
    if not os.path.exists(STATIC_ROOT):
        os.makedirs(STATIC_ROOT)

# Compress
COMPRESS_OFFLINE = os.environ['COMPRESS_OFFLINE']
COMPRESS_ENABLED = ast.literal_eval(os.environ['COMPRESS_ENABLED'])
COMPRESS_CSS_FILTERS = ['compressor.filters.css_default.CssAbsoluteFilter', 'compressor.filters.yui.YUICSSFilter']
COMPRESS_YUI_CSS_ARGUMENTS = '--verbose'
COMPRESS_YUI_BINARY = 'java -jar %s' % (os.path.join(PROJECT_DIR, 'deploy/compose/services/worker/yui-compressor/yuicompressor.jar'))
# COMPRESS_JS_FILTERS = ['compressor.filters.closure.ClosureCompilerFilter']
COMPRESS_JS_FILTERS = ['compressor.filters.jsmin.JSMinFilter']
COMPRESS_CLOSURE_COMPILER_BINARY = 'java -jar %s' % (os.path.join(PROJECT_DIR, 'deploy/compose/services/worker/closure-compiler/compiler.jar'))
COMPRESS_CLOSURE_COMPILER_ARGUMENTS = '--compilation_level SIMPLE_OPTIMIZATIONS --language_in ECMASCRIPT5'
# COMPRESS_CLOSURE_COMPILER_ARGUMENTS = '--compilation_level WHITESPACE_ONLY --language_in ECMASCRIPT5'
COMPRESS_URL = STATIC_URL
COMPRESS_ROOT = STATIC_ROOT
COMPRESS_STORAGE = 'gouda.storages.CachedS3BotoStaticGzipStorage'

# Profiles
PROFILE_APPROVED_EMAIL_DELAY_SECONDS = 86400
MAX_LOGIN_INFOS = 10

# Grappelli
GRAPPELLI_ADMIN_TITLE = 'gouda-web'

# Money
AMOUNT_WITHHELD_PERCENTAGE = 15.0
PHOTOGRAPHER_PRICE = 500.0
PHOTOGRAPHER_AMOUNT_WITHHELD = PHOTOGRAPHER_PRICE * AMOUNT_WITHHELD_PERCENTAGE / 100
DEFAULT_PHOTOGRAPHER_ID = 0
POSTMATES_BOOKING_CHARGE_DELAY = timedelta(days=10)
CLIENT_APPROVAL_TIMEOUT = timedelta(days=2)
MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY = 3
MIN_TIME_BETWEEN_BOOKINGS = timedelta(minutes=150)
MAX_BOOKINGS_PER_PHOTOGRAPHER = 20

# Payscale
# i. Shoot with 25 or fewer menu items - $150/ $50 Com
# ii. Shoot with 26-37 menu items - $ 225/$75 Com
# iii. Shoot with 38-49 menu items - $ 300/$100 Comm
# iv. Shoot with 50 menu items - $450/$250 Comm
# v.  Shoot with 51-62 menu items - $450/$200
# vi. Shoot with 62 or more menu items - $450/$150
PAY_SCALE = [
    {
        'shot_range': NumericRange(lower=1, upper=25, bounds='[]'),
        'amount': 135,
        'service_fee_amount': 35,
    },
    {
        'shot_range': NumericRange(lower=26, upper=37, bounds='[]'),
        'amount': 205,
        'service_fee_amount': 55,
    },
    {
        'shot_range': NumericRange(lower=38, upper=49, bounds='[]'),
        'amount': 270,
        'service_fee_amount': 70,
    },
    {
        'shot_range': NumericRange(lower=50, upper=61, bounds='[]'),
        'amount': 340,
        'service_fee_amount': 90,
    },
    {
        'shot_range': NumericRange(lower=62, upper=None, bounds='[]'),
        'amount': 405,
        'service_fee_amount': 105,
    },
]

# Sessions
SESSION_ENGINE = 'django.contrib.sessions.backends.cached_db'
SESSION_SAVE_EVERY_REQUEST = True
SESSION_COOKIE_SECURE = not DEBUG

# SCRAPY
os.environ['SCRAPY_SETTINGS_MODULE'] = 'gouda.anansi.settings'
# os.environ['http_proxy'] = "http://%s:%s" % (os.environ['TOR_PORT_8118_TCP_ADDR'], os.environ['TOR_PORT_8118_TCP_PORT'])


# IMAP
POSTMASTER_EMAIL_DOMAIN = 'shotzu.com'
POSTMASTER_IMAP_HOST = 'mail.shotzu.com'
POSTMASTER_IMAP_USERNAME = 'bot@shotzu.com'
POSTMASTER_IMAP_PASSWORD = 'password1234'
POSTMASTER_IMAP_USE_SSL = False
POSTMASTER_FROM_EMAIL = DEFAULT_FROM_EMAIL
POSTMASTER_BCC = STAFF_EMAILS

MESSAGE_STORAGE = 'django.contrib.messages.storage.session.SessionStorage'

# GOOGLE
GOOGLE_API_KEY = 'AIzaSyBDy8tNmqVgHIU7yloC72NOOTPltAUZ5eY'

# Database
DATABASES = {
    'default': {
        'ENGINE': 'django.contrib.gis.db.backends.postgis',
        'NAME': os.environ['POSTGRES_DB'],
        'USER': os.environ['POSTGRES_USER'],
        'PASSWORD': os.environ['POSTGRES_PASSWORD'],
        'HOST': os.environ['POSTGRES_HOST'],
        'PORT': os.environ['POSTGRES_PORT'],
    }
}

# Cache
CACHE_BACKEND = os.environ['CACHE_BACKEND'] if not IS_TEST_MODE else 'django.core.cache.backends.locmem.LocMemCache'
url = 'redis://:%s@%s:%s' % (
    os.environ['REDIS_PASSWORD'],
    os.environ['REDIS_HOST'],
    os.environ['REDIS_PORT']
)
CACHES = {
    'default': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/1' % (url, ),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'twitter_bots': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/2' % (url, ),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    },
    'instagram_bots': {
        'BACKEND': CACHE_BACKEND,
        'LOCATION': '%s/2' % (url, ),
        'OPTIONS': {
            'CLIENT_CLASS': 'django_redis.client.DefaultClient',
        }
    }
}

# Celery
CELERY_ALWAYS_EAGER = IS_TEST_MODE
CELERY_RESULT_BACKEND = 'djcelery.backends.database:DatabaseBackend'
BROKER_URL = 'amqp://admin:%s@%s:%s//' % (
    os.environ['RABBITMQ_PASSWORD'], os.environ['RABBITMQ_HOST'],
    os.environ['RABBITMQ_PORT']
)
if ast.literal_eval(os.environ.get('IS_CELERYBEAT_SCHEDULE_ENABLED', 'False')):
    CELERYBEAT_SCHEDULE = {
        'send_postmates_notification': {
            'task': 'gouda.photographers.tasks.send_postmates_notification',
            # 'schedule': timedelta(seconds=60 * 15),
            'schedule': crontab(minute=0, hour=1),
        },
        'send_photographer_completion': {
            'task': 'gouda.photographers.tasks.send_photographer_completion',
            'schedule': timedelta(seconds=60 * 60),
        },
        'backup_database': {
            'task': 'backups.tasks.BackupDatabaseTask',
            'schedule': timedelta(seconds=60 * 60),
        },
        # 'process_photographer_approved_bookings': {
        #     'task': 'gouda.photographers.tasks.process_photographer_approved_bookings',
        #     'schedule': timedelta(seconds=60 * 60),
        # },
        # 'process_client_approved_bookings': {
        #     'task': 'gouda.photographers.tasks.process_client_approved_bookings',
        #     'schedule': timedelta(seconds=60 * 60),
        # },
        'update_braintree_transactions': {
            'task': 'gouda.photographers.tasks.UpdateBookingsTask',
            'schedule': timedelta(seconds=60 * 60),
        },
        'homeaway_send_messages': {
            'task': 'gouda.homeaway_bots.tasks.run_bots',
            'schedule': timedelta(seconds=60 * 30)
        },
        # 'process_client_approved_bookings': {
        #     'task': 'gouda.photographers.tasks.process_client_approved_bookings',
        #     'schedule': timedelta(seconds=60),
        # },
        # 'process_mailbox': {
        #     'task': 'gouda.postmaster.tasks.process_mailbox',
        #     'schedule': timedelta(seconds=10),
        # },
        # 'backup_database': {
        #     'task': 'gouda.backups.tasks.backup_database',
        #     'schedule': timedelta(seconds=60 * 60),
        # },
        # 'instagram_follow': {
        #     'task': 'gouda.instagram_bots.tasks.run_follow_bots',
        #     'schedule': crontab(minute=0)
        # },
        # 'instagram_unfollow': {
        #     'task': 'gouda.instagram_bots.tasks.run_unfollow_bots',
        #     'schedule': crontab(minute=30)
        # },
        # 'twitter_reply': {
        #     'task': 'gouda.twitter_bots.tasks.run_reply_bots',
        #     'schedule': crontab(minute=40)
        # },
        # 'twitter_follow': {
        #     'task': 'gouda.twitter_bots.tasks.run_follow_bots',
        #     'schedule': crontab(minute=0)
        # },
        # 'twitter_unfollow': {
        #     'task': 'gouda.twitter_bots.tasks.run_unfollow_bots',
        #     'schedule': crontab(minute=20)
        # },
        # 'send_profile_approved_emails': {
        #     'task': 'gouda.profiles.tasks.send_profile_approved_emails',
        #     'schedule': timedelta(seconds=24 * 60 * 60),
        #     # 'schedule': timedelta(seconds=60 * 60),
        # },
        # 'update_booking_transactions': {
        #     'task': 'gouda.rentals.tasks.update_booking_transactions',
        #     'schedule': timedelta(seconds=60 * 60),
        # }
    }

# Haystack
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.simple_backend.SimpleEngine',
    },
    # 'default': {
    #     'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
    #     'URL': 'http://%s:%d/' % (os.environ['ELASTICSEARCH_PORT_9200_TCP_ADDR'], int(os.environ['ELASTICSEARCH_PORT_9200_TCP_PORT'])),
    #     'INDEX_NAME': 'test' if IS_TEST_MODE else 'haystack',
    # },
}

# Thumbor
# THUMBOR_SERVER = 'http://%s:%s' % (os.environ['HAPROXY_PORT_8888_TCP_ADDR'], os.environ['HAPROXY_PORT_8888_TCP_PORT'])
THUMBOR_SERVER = os.environ['THUMBOR_SERVER']
THUMBOR_MEDIA_URL = MEDIA_URL
THUMBOR_SECURITY_KEY = os.environ['THUMBOR_SECURITY_KEY']

# Hijack
HIJACK_USE_BOOTSTRAP = True
HIJACK_REGISTER_ADMIN = False
HIJACK_ALLOW_GET_REQUESTS = True

# SSL
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
