# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0017_bot_is_logging_enabled'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bot',
            old_name='is_logging_enabled',
            new_name='are_email_notifications_enabled',
        ),
    ]
