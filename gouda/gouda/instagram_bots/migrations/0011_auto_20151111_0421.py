# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0010_bot_speed'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bot',
            old_name='name',
            new_name='username',
        ),
    ]
