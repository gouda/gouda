# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0019_auto_20151123_2218'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='speed',
            field=models.IntegerField(default=2, verbose_name=b'activity speed'),
        ),
        migrations.AlterField(
            model_name='bot',
            name='unfollow_timeout',
            field=models.IntegerField(default=129600, verbose_name=b'Unfollow timeout (s)'),
        ),
    ]
