# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0005_auto_20151110_2351'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='usernames',
            field=django.contrib.postgres.fields.ArrayField(default=[], base_field=models.CharField(max_length=100), size=None),
        ),
    ]
