# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0004_auto_20151110_2334'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bot',
            old_name='username',
            new_name='name',
        ),
    ]
