# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0009_auto_20151111_0101'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='speed',
            field=models.IntegerField(default=0, verbose_name=b'activity speed'),
        ),
    ]
