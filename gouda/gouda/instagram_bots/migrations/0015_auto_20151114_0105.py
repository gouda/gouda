# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0014_bot_unfollow_timeout'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='unfollow_timeout',
            field=models.IntegerField(default=0, verbose_name=b'Unfollow timeout (s)'),
        ),
    ]
