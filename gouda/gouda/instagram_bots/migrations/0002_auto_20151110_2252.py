# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bot',
            name='password',
        ),
        migrations.AddField(
            model_name='bot',
            name='access_token',
            field=models.CharField(default='', max_length=100),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='bot',
            name='username',
            field=models.CharField(unique=True, max_length=100),
        ),
    ]
