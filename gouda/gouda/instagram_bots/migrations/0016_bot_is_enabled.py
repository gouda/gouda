# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0015_auto_20151114_0105'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='is_enabled',
            field=models.BooleanField(default=True),
        ),
    ]
