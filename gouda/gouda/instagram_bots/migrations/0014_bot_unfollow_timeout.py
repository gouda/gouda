# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0013_bot_do_ignore_private_users'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='unfollow_timeout',
            field=models.IntegerField(default=0),
        ),
    ]
