# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0018_auto_20151116_2251'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bot',
            name='access_token',
        ),
        migrations.AddField(
            model_name='bot',
            name='client_id',
            field=models.CharField(default=b'a5a4afe245164f3fbf86166239b4d4fc', max_length=100),
        ),
        migrations.AddField(
            model_name='bot',
            name='client_secret',
            field=models.CharField(default=b'c5c5197e356f4e5f86e93b523cfe66ed', max_length=100),
        ),
    ]
