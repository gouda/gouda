# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0012_bot_password'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='do_ignore_private_users',
            field=models.BooleanField(default=True),
        ),
    ]
