# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0003_auto_20151110_2330'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='access_token',
            field=models.CharField(max_length=100),
        ),
    ]
