# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0002_auto_20151110_2252'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='access_token',
            field=models.CharField(max_length=500),
        ),
    ]
