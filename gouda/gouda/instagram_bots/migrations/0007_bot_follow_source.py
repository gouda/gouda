# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0006_bot_usernames'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='follow_source',
            field=models.IntegerField(default=0),
        ),
    ]
