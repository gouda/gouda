# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0016_bot_is_enabled'),
    ]

    operations = [
        migrations.AddField(
            model_name='bot',
            name='is_logging_enabled',
            field=models.BooleanField(default=True),
        ),
    ]
