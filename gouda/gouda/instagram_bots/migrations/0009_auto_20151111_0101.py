# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('instagram_bots', '0008_auto_20151111_0049'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bot',
            name='follow_source',
            field=models.IntegerField(default=0, verbose_name=b'follow source'),
        ),
    ]
