from __future__ import absolute_import

from django.core.management.base import BaseCommand
from gouda.instagram_bots.models import Bot
from gouda.instagram_bots.models import SpeedType
from gouda.instagram_bots.models import FollowSourceType


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('username')

    def handle(self, *args, **options):
        bot = Bot.objects.get(
            username=options.get('username'),
        )
        bot.bootstrap_follows()
