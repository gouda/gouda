from __future__ import absolute_import

from django.core.management.base import BaseCommand
from gouda.instagram_bots.models import Bot
from gouda.instagram_bots.models import SpeedType
from gouda.instagram_bots.models import FollowSourceType


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('username')
        parser.add_argument('password')
        parser.add_argument('-u', dest='usernames', action='append')

    def handle(self, *args, **options):
        bot = Bot.objects.create(
            username=options.get('username'),
            password=options.get('password'),
            speed=SpeedType.FAST,
            follow_source=FollowSourceType.FOLLOWINGS,
            unfollow_timeout=129600,
            usernames=options.get('usernames'),
            do_ignore_private_users=False
        )
        bot.bootstrap_follows()
