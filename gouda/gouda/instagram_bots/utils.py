from instagram.client import InstagramAPI
from django.conf import settings

INSTAGRAM_CLIENT_ID = getattr(settings, 'INSTAGRAM_CLIENT_ID')
INSTAGRAM_CLIENT_SECRET = getattr(settings, 'INSTAGRAM_CLIENT_SECRET')
INSTAGRAM_REDIRECT_URI = getattr(settings, 'INSTAGRAM_REDIRECT_URI')
INSTAGRAM_SCOPE = getattr(settings, 'INSTAGRAM_SCOPE')


def connect_to_instagram(client_id=INSTAGRAM_CLIENT_ID, client_secret=INSTAGRAM_CLIENT_SECRET):
    if not hasattr(connect_to_instagram, 'api'):
        num_retries = 10
        for i in range(num_retries):
            try:
                connect_to_instagram.api = InstagramAPI(
                    # access_token=self.access_token,
                    client_id=client_id,
                    client_secret=client_secret,
                    redirect_uri=INSTAGRAM_REDIRECT_URI,
                )
            except Exception as e:
                if i >= num_retries - 1:
                    raise e
    return connect_to_instagram.api
