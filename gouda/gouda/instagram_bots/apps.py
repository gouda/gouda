from __future__ import absolute_import

from django.apps import AppConfig


class InstagramBotsAppConfig(AppConfig):
    name = 'gouda.instagram_bots'
    verbose_name = 'Instagram Bots'
