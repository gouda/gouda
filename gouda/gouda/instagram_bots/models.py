from __future__ import absolute_import

import os
from selenium.webdriver.phantomjs.webdriver import WebDriver
from selenium.webdriver.support.select import Select
from selenium.webdriver.common.action_chains import ActionChains
from django.db import models
from django.conf import settings
from fabric.operations import prompt
from django.contrib.postgres.fields import ArrayField
from django_enumfield import enum
import re
import logging
import random
import requests
import time
from urlparse import urljoin
from django.utils import timezone
from datetime import timedelta
from django.core.mail import mail_admins
from gouda.instagram_bots.utils import connect_to_instagram
from gouda.instagram_bots.utils import INSTAGRAM_SCOPE
from gouda.instagram_bots.utils import INSTAGRAM_CLIENT_ID
from gouda.instagram_bots.utils import INSTAGRAM_CLIENT_SECRET
from celery.utils.log import get_task_logger
from instagram.client import InstagramAPI
from django.core.cache import caches
import hashlib
import simplejson
from instagram.models import User as InstagramUser

cache = caches['instagram_bots']
logger = get_task_logger(__name__)

INSTAGRAM_BASE_URL = 'https://instagram.com/'
INSTAGRAM_REDIRECT_URI = getattr(settings, 'INSTAGRAM_REDIRECT_URI')


class FollowSourceType(enum.Enum):
    FOLLOWERS = 0
    FOLLOWINGS = 1

    labels = {
        FOLLOWERS: 'Followers of usernames',
        FOLLOWINGS: 'Followings of usernames',
    }


class SpeedType(enum.Enum):
    SLOW = 0
    NORMAL = 1
    FAST = 2

    labels = {
        SLOW: 'Slow',
        NORMAL: 'Normal',
        FAST: 'Fast',
    }

    # Per hour
    follow = {
        SLOW: 14,
        NORMAL: 20,
        FAST: 26,
    }
    unfollow = {
        SLOW: 10,
        NORMAL: 15,
        FAST: 20,
    }


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class User(BaseModel):
    username = models.CharField(max_length=100)
    user_id = models.CharField(max_length=100)


class Follow(BaseModel):
    bot = models.ForeignKey('Bot')
    user = models.ForeignKey(User)
    is_unfollow = models.BooleanField(default=False)


class Bot(BaseModel):
    username = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    client_id = models.CharField(max_length=100, default=INSTAGRAM_CLIENT_ID)
    client_secret = models.CharField(max_length=100, default=INSTAGRAM_CLIENT_SECRET)
    speed = enum.EnumField(SpeedType, blank=False, default=SpeedType.FAST, verbose_name='activity speed')

    is_enabled = models.BooleanField(default=True)
    are_email_notifications_enabled = models.BooleanField(default=True)

    usernames = ArrayField(models.CharField(max_length=100), default=list())
    follow_source = enum.EnumField(FollowSourceType, blank=False, default=FollowSourceType.FOLLOWERS, verbose_name='follow source')
    do_ignore_private_users = models.BooleanField(default=True)
    unfollow_timeout = models.IntegerField(default=129600, verbose_name='Unfollow timeout (s)')

    _api = None

    def clean(self):
        # Clean usernames
        usernames = list()
        for username in self.usernames:
            for t in re.split(r'[\s,]', username):
                if t:
                    usernames.append(t.strip())
        self.usernames = usernames

    @property
    def api(self):
        if not self._api:
            num_retries = 10
            for i in range(num_retries):
                try:
                    self._api = InstagramAPI(
                        # access_token=self.access_token,
                        client_id=self.client_id,
                        client_secret=self.client_secret,
                        redirect_uri=INSTAGRAM_REDIRECT_URI,
                    )
                except Exception as e:
                    if i >= num_retries - 1:
                        raise e
        return self._api

    @staticmethod
    def get_access_token():
        instagram_api = connect_to_instagram()
        redirect_uri = instagram_api.get_authorize_login_url(scope=INSTAGRAM_SCOPE)
        
        print 'Visit this page and authorize access in your browser: %s' % redirect_uri

        code = prompt('Paste in code in query string after redirect: ')
        access_token = instagram_api.exchange_code_for_access_token(code)
        print access_token
        return access_token[0]

    def is_under_follow_speed_limit(self, num_follows):
        # cutoff_time = timezone.now() - timedelta(hours=1)
        # cutoff_time = cutoff_time + timedelta(minutes=7)
        # Follow.objects.filter(bot=self, is_unfollow=False, date_created__gte=cutoff_time).count()
        return num_follows < SpeedType.follow.get(self.speed)

    def is_under_unfollow_speed_limit(self, num_unfollows):
        return num_unfollows < SpeedType.unfollow.get(self.speed)

    def generate_follows(self, usernames, follow_source, timeout=timedelta(seconds=86400)):
        api = self.api
        usernames = list(usernames)
        random.shuffle(usernames)
        for username in usernames:
            users = api.user_search(q=username)
            user_id = None
            for u in users:
                if u.username == username:
                    user_id = u.id
                    break
            if not user_id:
                logger.error('Failed to find ID for %s' % username)
                continue
            logger.info('Processing %ss for %s...' % (Bot.get_follow_source_label(follow_source), username))

            # Get follows
            follow_api = api.user_follows if follow_source == FollowSourceType.FOLLOWERS else api.user_followed_by
            is_first_pass = True
            next_url = None
            while next_url or is_first_pass:
                cache_key = hashlib.sha224('%s.%s.%s' % ('user_follows' if follow_source == FollowSourceType.FOLLOWERS else 'user_followed_by', username, next_url if next_url else '')).hexdigest()
                logger.info('Cache Key: %s' % cache_key)
                # Try the cache
                instagram_users = cache.get(cache_key, None)
                if instagram_users:
                    data = simplejson.loads(instagram_users)
                    instagram_users = [InstagramUser.object_from_dictionary(u) for u in data.get('users')]
                    next_url = data.get('next_url')
                else:
                    # Make the API Request
                    instagram_users, next_url = follow_api(user_id) if is_first_pass else follow_api(with_next_url=next_url)

                    # Cache it
                    data = dict()
                    data['users'] = [{'full_name': f.full_name, 'id': f.id, 'username': f.username} for f in instagram_users]
                    data['next_url'] = next_url
                    cache.set(cache_key, simplejson.dumps(data), timeout=timeout.total_seconds())

                # Check for private users and yield
                is_first_pass = False
                for f in instagram_users:
                    if self.do_ignore_private_users:
                        try:
                            api.user_recent_media(user_id=f.id, count=10)
                        except Exception as e:
                            logger.info('Skipping private user (%s)' % f.username)
                            continue
                    yield f
            return

    def bootstrap_follows(self):
        # Get list of followers (people I'm following)
        follows = list()
        for instagram_follow in self.generate_follows([self.username], FollowSourceType.FOLLOWERS, timeout=timedelta(minutes=5)):
            if instagram_follow.username == self.username:
                continue
            user, is_created = User.objects.get_or_create(username=instagram_follow.username, user_id=instagram_follow.id)
            follow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            follows.append(follow)
        return follows

    def auto_unfollow(self, is_test_mode):
        browser = self._login()

        # Get list of followers (people I'm following)
        follows = list()
        for instagram_follow in self.generate_follows([self.username], FollowSourceType.FOLLOWERS, timeout=timedelta(minutes=5)):
            if instagram_follow.username == self.username:
                continue
            user, is_created = User.objects.get_or_create(username=instagram_follow.username, user_id=instagram_follow.id)
            follow, is_created = Follow.objects.get_or_create(user=user, bot=self, is_unfollow=False)
            if is_created:
                logger.warn('Unlogged follow found (%d, %s)!' % (follow.id, follow.user.username))

            # Determine if we should unfollow based on timeout
            unfollow_timeout = timedelta(seconds=self.unfollow_timeout)
            if timezone.now() - unfollow_timeout < follow.date_created:
                logger.info('Skipping %s due to timeout' % follow.user.username)
                continue
            follows.append(follow)

        # Get list of followings (people who follow me)
        followings = list()
        for instagram_follow in self.generate_follows([self.username], FollowSourceType.FOLLOWINGS, timeout=timedelta(minutes=5)):
            followings.append(instagram_follow)

        # Start unfollowing people who don't follow back...
        unfollows = list()
        for follow in follows:
            user = follow.user
            # See if user is following this account
            if [f for f in followings if f.username == follow.user.username]:
                logger.info('%s is already following %s' % (user.username, self.username))
                continue

            # Load user's page
            logger.info('Loading page for %s' % follow.user.username)
            if not is_test_mode:
                browser.get(urljoin(INSTAGRAM_BASE_URL, follow.user.username))
                time.sleep(5)
                logger.info('Loaded %s' % browser.current_url)

                # Click unfollow...
                unfollow_button = browser.find_element_by_css_selector('.-cx-PRIVATE-FollowButton__button')
                if unfollow_button.text == 'FOLLOWING':
                    logger.info('Clicking <%s>' % unfollow_button.text)
                    unfollow_button.click()
                else:
                    logger.error('We are not following %s (<%s>)' % (follow.user.username, unfollow_button.text))

            unfollow = Follow.objects.create(bot=self, user=user, is_unfollow=True)
            unfollows.append(unfollow)
            logger.info('Unfollowing %s' % unfollow.user.username)

            if not self.is_under_unfollow_speed_limit(len(unfollows)):
                break

            if not is_test_mode:
                sleep_duration = random.randrange(10, 20)
                logger.info('Sleeping for %d seconds to look more human' % sleep_duration)
                time.sleep(sleep_duration)
        browser.quit()
        return unfollows

    def auto_follow(self, is_test_mode):
        browser = self._login()

        # Visit each follow's page
        follows = list()
        for instagram_follow in self.generate_follows(self.usernames, self.follow_source):
            if instagram_follow.username == self.username:
                continue

            if Follow.objects.filter(user__user_id=instagram_follow.id).exists():  # Make sure this follow is unique
                logger.info('Ignoring %s' % instagram_follow.username)
                continue

            # Load user's page
            is_new_follow = True
            is_follow_requested = False
            try:
                logger.info('Loading page for %s' % instagram_follow.username)
                if not is_test_mode:
                    browser.get(urljoin(INSTAGRAM_BASE_URL, instagram_follow.username))
                    time.sleep(5)
                    logger.info('Loaded %s' % browser.current_url)

                    # Click follow...
                    follow_button = browser.find_element_by_css_selector('.-cx-PRIVATE-FollowButton__button')
                    is_new_follow = follow_button.text == 'FOLLOW'
                    is_follow_requested = follow_button.text == 'REQUESTED'
                    if is_new_follow:
                        logger.info('Clicking <%s>' % follow_button.text)
                        follow_button.click()
                    elif is_follow_requested:
                        logger.info('Already requested a follow for %s' % instagram_follow.username)
                    else:
                        logger.warning('We are already following %s (%s)' % (instagram_follow.username, follow_button.text))
                        continue
            except:
                logger.warning('Failed to process page for %s' % instagram_follow.username)
                continue

            user, is_created = User.objects.get_or_create(username=instagram_follow.username, user_id=instagram_follow.id)
            follow, is_created = Follow.objects.get_or_create(bot=self, user=user, is_unfollow=False)
            if is_new_follow:
                follows.append(follow)
            logger.info('Following %s' % follow.user.username)

            if not self.is_under_follow_speed_limit(len(follows)):
                break

            if not is_test_mode:
                sleep_duration = random.randrange(10, 20)
                logger.info('Sleeping for %d seconds to look more human' % sleep_duration)
                if is_new_follow:
                    time.sleep(sleep_duration)

        browser.quit()
        logger.info('Followed %d users!' % len(follows))
        return follows

    def _login(self):
        # Create driver
        # service_args = [
        #     '--proxy=%s:%s' % (os.environ['TOR_PORT_8118_TCP_ADDR'], os.environ['TOR_PORT_8118_TCP_PORT']),
        #     '--proxy-type=socks5'
        # ]
        # logger.info(service_args)
        browser = WebDriver()
        num_retries = 5
        for i in range(num_retries):
            browser.get(INSTAGRAM_BASE_URL)
            time.sleep(5)
            logger.info('Loading %s...' % browser.current_url)
            if INSTAGRAM_BASE_URL in browser.current_url:
                break
            logger.info('Failed to load %s! Retrying...' % INSTAGRAM_BASE_URL)

        # Login to account
        username_field = browser.find_element_by_xpath(
            '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[1]/input')
        password_field = browser.find_element_by_xpath(
            '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/div[2]/input')
        submit_button = browser.find_element_by_xpath(
            '//*[@id="react-root"]/section/main/article/div[2]/div[1]/div/form/button')
        username_field.send_keys(self.username)
        password_field.send_keys(self.password)
        submit_button.click()
        time.sleep(5)
        logger.info('Loaded %s' % browser.current_url)
        return browser

    @staticmethod
    def get_follow_source_label(follow_source):
        return 'following' if follow_source == FollowSourceType.FOLLOWINGS else 'follower'

    @property
    def follow_source_label(self):
        return 'following' if self.follow_source == FollowSourceType.FOLLOWINGS else 'follower'
