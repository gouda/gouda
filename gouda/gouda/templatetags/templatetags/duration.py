from django import template

register = template.Library()

@register.filter
def duration(delta, format):
    if not delta:
        return ''
    if format == 'days':
        return '%d' % delta.days
    return ''
