from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from collections import OrderedDict
from django.utils.http import urlencode
from django import template

register = template.Library()

@register.simple_tag
def query_params(request, field, value):
    params = request.GET.copy()
    params[field] = value
    return urlencode(OrderedDict(sorted(params.items())))


@register.simple_tag
def url_ordering(request, field, value, direction=''):
    dict_ = request.GET.copy()

    if field == 'o' and field in dict_.keys():
        if dict_[field].startswith('-') and dict_[field].lstrip('-') == value:
            dict_[field] = value
        elif dict_[field].lstrip('-') == value:
            dict_[field] = "-" + value
        else:
            dict_[field] = direction + value
    else:
        dict_[field] = direction + value

    return urlencode(OrderedDict(sorted(dict_.items())))

@register.simple_tag
def url_ordering_icon(request, field, value):
    params = request.GET.copy()

    if field == 'o' and field in params.keys():
        if params.get(field).lstrip('-') == value:
            if params.get(field).startswith('-'):
                return 'fa fa-sort-desc'
            else:
                return 'fa fa-sort-asc'
    return ''
