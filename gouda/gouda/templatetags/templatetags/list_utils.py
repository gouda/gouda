"""
Template tags for working with lists.

You'll use these in templates thusly::

    {% load listutil %}
    {% for sublist in mylist|parition:"3" %}
        {% for item in mylist %}
            do something with {{ item }}
        {% endfor %}
    {% endfor %}
"""

from django import template

register = template.Library()

@register.filter
def partition(collection, num_partitions):
    """
    Break a list into ``n`` pieces. The last list may be larger than the rest if
    the list doesn't break cleanly. That is::

        >>> l = range(10)

        >>> partition(l, 2)
        [[0, 1, 2, 3, 4], [5, 6, 7, 8, 9]]

        >>> partition(l, 3)
        [[0, 1, 2], [3, 4, 5], [6, 7, 8, 9]]

        >>> partition(l, 4)
        [[0, 1], [2, 3], [4, 5], [6, 7, 8, 9]]

        >>> partition(l, 5)
        [[0, 1], [2, 3], [4, 5], [6, 7], [8, 9]]

    """
    try:
        num_partitions = int(num_partitions)
        collection = list(collection)
    except (ValueError, TypeError):
        return [collection]
    p = len(collection) / num_partitions
    return [collection[p*i:p*(i+1)] for i in range(num_partitions - 1)] + [collection[p*(i+1):]]

@register.filter
def partition_horizontal(collection, num_partitions):
    """
    Break a list into ``n`` peices, but "horizontally." That is, 
    ``partition_horizontal(range(10), 3)`` gives::
    
        [[1, 2, 3],
         [4, 5, 6],
         [7, 8, 9],
         [10]]
        
    Clear as mud?
    """
    try:
        num_partitions = int(num_partitions)
        collection = list(collection)
    except (ValueError, TypeError):
        return [collection]
    newlists = [list() for i in range(num_partitions)]
    for i, val in enumerate(collection):
        newlists[i%num_partitions].append(val)
    return newlists