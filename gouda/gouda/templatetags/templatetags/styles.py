from django import template
from django.core.urlresolvers import reverse
from django.core.urlresolvers import NoReverseMatch

register = template.Library()

@register.simple_tag()
def is_active(request, urls, active_class=None, inactive_class=None):
    for url in urls.split():
        try:
            if request.path in reverse(url):
                return active_class if active_class else 'active'
        except NoReverseMatch:
            if url in request.path:
                return active_class if active_class else 'active'
    return inactive_class if inactive_class else ''


@register.simple_tag()
def checkout_progress_class(request, viewname, order=None):
    url = reverse(viewname)
    shipping_address_create_url = reverse('listings:shipping_address_create')
    customer_create_url = reverse('listings:customer_create')
    review_order_url = reverse('listings:review_order')
    if request.path in shipping_address_create_url:
        if url in shipping_address_create_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle-o'
    elif request.path in customer_create_url:
        if url in customer_create_url:
            return 'fa-dot-circle-o'
        elif url in shipping_address_create_url:
            return 'fa-circle'
        else:
            return 'fa-circle-o'
    elif request.path in review_order_url:
        if url in review_order_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle'
    return 'fa-circle-o'
