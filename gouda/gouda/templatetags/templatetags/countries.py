from django import template

register = template.Library()

@register.filter
def region(region_code):
    region_names = {
        'CO': 'Colorado',
        'AK': 'Alaska',
        'MD': 'Maryland',
        'WA': 'Washington',
        'TX': 'Texas'
    }
    return region_names.get(region_code, region_code)

@register.filter
def country(country_code):
    country_names = {
        'US': 'United States',
    }
    return country_names.get(country_code, country_code)
