from django import template

register = template.Library()

@register.filter
def divide(x, y):
    return x / y if y != 0 else 0

@register.filter
def multiply(x, y):
    return x * y
