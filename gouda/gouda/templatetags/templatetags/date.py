import math
from datetime import timedelta
from django.utils import timezone

from django import template

register = template.Library()

@register.filter
def prettifydate(date):
    if not date:
        return ''
    now_time = timezone.now()
    delta = (now_time - date)
    if delta < timedelta(seconds=86400):
        if delta.seconds >= 3600:
            hours = int(delta.seconds / 3600.0)
            return '%d hour%s ago' % (hours, 's' if hours != 1 else '')
        elif delta.seconds >= 60:
            minutes = int(delta.seconds / 60.0)
            return '%d minute%s ago' % (minutes, 's' if minutes != 1 else '')
        else:
            return '%d second%s ago' % (delta.seconds, 's' if delta.seconds != 1 else '')
    elif now_time.year == date.year:
        day = date.strftime('%d').lstrip('0')
        month = date.strftime('%b').lstrip('0')
        return '%s %s' % (month, day)
    else:
        day = date.strftime('%d').lstrip('0')
        month = date.strftime('%m').lstrip('0')
        year = date.strftime('%y')
        return '%s/%s/%s' % (month, day, year)
