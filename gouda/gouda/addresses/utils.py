from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import csv
import random

import linecache
import os
from gouda.addresses.models import Address


FIXTURES_DIR = os.path.join(os.path.dirname(__file__), 'fixtures')


def get_street_name():
    return linecache.getline(os.path.join(FIXTURES_DIR, 'street_names.txt'), random.randrange(0, 91670)).decode('utf-8')


def get_street_suffix():
    return linecache.getline(os.path.join(FIXTURES_DIR, 'street_suffixes.txt'), random.randrange(0, 50)).decode('utf-8')


def get_street_address():
    return ('%d %s %s' % (random.randrange(1000, 10000), get_street_name().strip(), get_street_suffix().strip())).strip()


def get_locality():
    line = linecache.getline(os.path.join(FIXTURES_DIR, 'locations.txt'), random.randrange(1, 39104))
    csv_reader = csv.reader([line], delimiter=str(','))
    values = csv_reader.next()
    return (' '.join([w.capitalize() for w in values[10].split(' ')])).strip()


def get_region():
    line = linecache.getline(os.path.join(FIXTURES_DIR, 'locations.txt'), random.randrange(1, 39104))
    csv_reader = csv.reader([line], delimiter=str(','))
    values = csv_reader.next()
    return values[11].upper().strip()


def create_address():
    address = Address.objects.create(**{
        'full_name': '',
        'street_address': get_street_address(),
        'locality': get_locality(),
        'region': get_region(),
        'postal_code': random.randrange(10000, 99999),
        'country': 'US'
    })
    return address
