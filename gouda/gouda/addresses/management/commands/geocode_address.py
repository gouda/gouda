from __future__ import absolute_import

from django.core.management.base import BaseCommand
from gouda.addresses.models import Address


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('id', type=int)

    def handle(self, *args, **options):
        address = Address.objects.get(id=options.get('id'))
        geocode_result = address.geocode()
        print(geocode_result)
