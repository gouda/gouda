from __future__ import absolute_import

import random

from gouda.users.models import User
from gouda.customers.models import StripeCustomer
from gouda.customers.models import StripePlan
from django.core.management.base import BaseCommand
from django.conf import settings
import stripe
import os


class Command(BaseCommand):
    def handle(self, *args, **options):
        states = list()
        with open(os.path.join(os.path.dirname(__file__), 'us_states.txt')) as f:
            for line in f.readlines():
                state = line.strip()
                states.append(state)

        states.sort()
        javascript = list()
        for state in states:
            javascript.append('{label:\'%s\',value:\'%s\'}' % (state, state))
        print ',\n'.join(javascript)

