# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0004_address_time_zone'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='formatted',
            field=models.CharField(default=b'', max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='address',
            name='place_id',
            field=models.CharField(max_length=500, blank=True),
        ),
    ]
