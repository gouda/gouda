# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0005_auto_20160517_2101'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='normalized_locality',
            field=models.CharField(default=b'', max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='address',
            name='normalized_postal_code',
            field=models.CharField(default=b'', max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='address',
            name='normalized_region',
            field=models.CharField(default=b'', max_length=500, blank=True),
        ),
        migrations.AddField(
            model_name='address',
            name='normalized_street_address',
            field=models.CharField(default=b'', max_length=500, blank=True),
        ),
    ]
