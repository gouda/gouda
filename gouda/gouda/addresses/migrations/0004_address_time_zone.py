# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import timezone_field.fields


class Migration(migrations.Migration):

    dependencies = [
        ('addresses', '0003_address_point'),
    ]

    operations = [
        migrations.AddField(
            model_name='address',
            name='time_zone',
            field=timezone_field.fields.TimeZoneField(blank=True, null=True),
        ),
    ]
