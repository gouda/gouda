# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Address',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('street_address', models.CharField(max_length=500)),
                ('sub_premise', models.CharField(max_length=500, blank=True)),
                ('postal_code', models.CharField(max_length=20, blank=True)),
                ('locality', models.CharField(max_length=500)),
                ('region', models.CharField(max_length=500)),
                ('country', django_countries.fields.CountryField(max_length=2)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
