from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
from django.contrib.gis.db import models

from django.contrib import admin
from gouda.addresses.models import Address
from gouda.forms.widgets import OpenLayersWidget

logger = logging.getLogger(__name__)

# admin.site.disable_action('delete_selected')


@admin.register(Address)
class Address(admin.ModelAdmin):
    date_hierarchy = 'date_created'
    list_display = (
        'street_address',
        'locality',
        'region',
        'country',
        'point',
        'time_zone',
        'date_created'
    )
    formfield_overrides = {
        models.PointField: {'widget': OpenLayersWidget(attrs={'display_raw': True})}
    }
