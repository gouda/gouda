from gouda.addresses.models import Address
from gouda.addresses.forms import AddressForm
from django.test import TestCase


class AddressFormTestCase(TestCase):
    data = {
        'full_name': 'Chukwuemeka Ezekwe',
        'street_address': '5719 Old Buggy Court',
        'postal_code': '21045',
        'locality': 'Columbia',
        'region': 'Maryland',
        'country': 'US'
    }

    def tearDown(self):
        Address.objects.all().delete()

    def test_init(self):
        AddressForm()

    def test_create_address(self):
        self.assertEqual(Address.objects.all().count(), 0)
        address_form = AddressForm(self.data)
        self.assertTrue(address_form.is_valid())
        address = address_form.save()
        self.assertIsNotNone(address)
        self.assertEqual(Address.objects.all().count(), 1)

        for k in self.data.keys():
            self.assertEqual(self.data.get(k), getattr(address, k))
