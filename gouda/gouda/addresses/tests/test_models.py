from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django import test
from gouda.addresses.models import Address


class AddressTestCase(test.TestCase):
    def setUp(self):
        self.data = {
            'full_name': 'Chukwuemeka Ezekwe',
            'street_address': '27 Treasure Cove Drive',
            'postal_code': '77381',
            'locality': 'Spring',
            'region': 'TX',
            'country': 'US'
        }

    def test_init(self):
        Address()

    def test_create_address(self):
        address = Address.objects.create(**self.data)
        self.assertIsNotNone(address)

    # def test_geocoding(self):
    #     address = Address.objects.create(**self.data)
    #     self.assertIsNotNone(address)
    #     self.assertIsNotNone(address.latitude)
    #     self.assertIsNotNone(address.longitude)
