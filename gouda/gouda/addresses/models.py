from django.conf import settings
from django.contrib.gis.db import models
from django_countries.fields import CountryField
from timezone_field import TimeZoneField
import googlemaps
from django.core.cache import cache
import hashlib

GOOGLE_API_KEY = getattr(settings, 'GOOGLE_API_KEY')


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Address(BaseModel):
    full_name = models.CharField(max_length=500)
    street_address = models.CharField(max_length=500)
    sub_premise = models.CharField(max_length=500, blank=True)
    postal_code = models.CharField(max_length=20, blank=True)
    locality = models.CharField(max_length=500)
    region = models.CharField(max_length=500)
    country = CountryField(default='US')

    formatted = models.CharField(max_length=500, blank=True, default='')
    place_id = models.CharField(max_length=500, blank=True)
    point = models.PointField(srid=4326, blank=True, null=True)
    time_zone = TimeZoneField(null=True, blank=True)

    normalized_street_address = models.CharField(max_length=500, default='', blank=True)
    normalized_locality = models.CharField(max_length=500, default='', blank=True)
    normalized_region = models.CharField(max_length=500, default='', blank=True)
    normalized_postal_code = models.CharField(max_length=500, default='', blank=True)

    objects = models.GeoManager()

    def __str__(self):
        address_components = [
            self.street_address,
            self.sub_premise,
            self.locality,
            self.region,
            self.postal_code,
            self.country.__str__(),
        ]
        return ', '.join([c for c in address_components if c]).replace('\n', '') if not self.formatted else self.formatted

    def geocode(self):
        google_maps_client = googlemaps.Client(key=GOOGLE_API_KEY)

        key = 'addresses:address:%s:geocode' % hashlib.sha512(self.__str__()).hexdigest()
        geocode_result = cache.get(key, None)
        if not geocode_result:
            geocode_result = google_maps_client.geocode('%s' % self)
            cache.set(key, geocode_result, 86400)
            assert len(geocode_result) > 0, 'No results found when geocoding'

        key = 'addresses:address:%s:timezone' % hashlib.sha512(self.__str__()).hexdigest()
        timezone_result = cache.get(key, None)
        if not timezone_result:
            timezone_result = google_maps_client.timezone(geocode_result[0].get('geometry').get('location'))
            cache.set(key, timezone_result, 86400)

        if len(geocode_result) > 0:
            # Get lat/lon
            self.point = 'POINT(%f %f)' % (
                geocode_result[0].get('geometry').get('location').get('lng'),
                geocode_result[0].get('geometry').get('location').get('lat'),
            )

            # Get place_id
            self.place_id = geocode_result[0].get('place_id')

            # Get formatted
            self.formatted = geocode_result[0].get('formatted_address')

            # Get normalized components
            self.normalized_street_address = Address.get_normalized_street_address(self.formatted)
            self.normalized_locality = Address.get_normalized_locality(self.formatted)
            self.normalized_region = Address.get_normalized_region(self.formatted)
            self.normalized_postal_code = Address.get_normalized_postal_code(self.formatted)

        # Get timezone
        self.time_zone = timezone_result.get('timeZoneId')

        # Save!
        self.save()

        return geocode_result

    @property
    def formatted_locality(self):
        if self.formatted:
            return Address.get_normalized_locality(self.formatted)
        return self.locality

    @property
    def formatted_region(self):
        if self.formatted:
            return Address.get_normalized_region(self.formatted)
        return self.region

    @property
    def formatted_postal_code(self):
        if self.formatted:
            return Address.get_normalized_postal_code(self.formatted)
        return self.postal_code

    @property
    def formatted_street_address(self):
        if self.formatted:
            return Address.get_normalized_street_address(self.formatted)
        return self.street_address

    @staticmethod
    def get_normalized_street_address(formatted):
        toks = [t.strip() for t in formatted.split(',')]
        if len(toks) == 4:
            return toks[0]
        elif len(toks) == 5:
            return toks[1]
        return ''

    @staticmethod
    def get_normalized_locality(formatted):
        toks = [t.strip() for t in formatted.split(',')]
        if len(toks) == 4:
            return toks[1]
        elif len(toks) == 5:
            return toks[2]
        return ''

    @staticmethod
    def get_normalized_region(formatted):
        toks = [t.strip() for t in formatted.split(',')]
        if len(toks) == 4:
            return toks[2].split(' ')[0]
        elif len(toks) == 5:
            return toks[3].split(' ')[0]
        return ''

    @staticmethod
    def get_normalized_postal_code(formatted):
        toks = [t.strip() for t in formatted.split(',')]
        if len(toks) == 4:
            return toks[2].split(' ')[1]
        elif len(toks) == 5:
            return toks[3].split(' ')[1]
        return ''

    @property
    def latitude(self):
        if self.point:
            return self.point.y
        # google_maps_client = googlemaps.Client(key=GOOGLE_API_KEY)
        # geocode_result = google_maps_client.geocode('%s' % self)
        # assert len(geocode_result) > 0, 'No results found when geocoding'

    @property
    def longitude(self):
        if self.point:
            return self.point.x
