from __future__ import absolute_import

from django import forms
from django_countries.fields import countries
from gouda.addresses.models import Address
from gouda.forms.widgets import TextInput


class AddressForm(forms.ModelForm):
    class Meta:
        model = Address
        exclude = ['date_modified', 'date_created']
        widgets = {
            'full_name': TextInput(),
            'street_address': TextInput(),
            'sub_premise': TextInput(is_required=False),
            'postal_code': TextInput(is_required=False),
            'locality': TextInput('locality'),
            'region': TextInput('region'),
            'country': forms.Select(choices=countries),
        }
