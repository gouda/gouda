from __future__ import absolute_import
from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import hashlib
from datetime import datetime

import os
import re
from django.core.management.base import BaseCommand
from django.conf import settings

APP_DIR = getattr(settings, 'APP_DIR')


class Command(BaseCommand):
    def handle(self, *args, **options):
        version = hashlib.md5(datetime.utcnow().isoformat()).hexdigest()

        lines = list()
        settings_file_path = os.path.join(APP_DIR, 'settings/constants.py')
        with open(settings_file_path) as settings_file:
            for line in settings_file:
                lines.append(re.sub(r'^STATICFILES_VERSION\s+=.*$', 'STATICFILES_VERSION = \'%s\'' % version, line))
        if lines:
            with open(settings_file_path, 'w') as settings_file:
                settings_file.writelines(lines)
        self.stdout.write(version)
