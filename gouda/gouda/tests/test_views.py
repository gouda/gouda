from __future__ import absolute_import

from django.core.urlresolvers import reverse
from rest_framework import status
from django_webtest import WebTest


class PrivacyPolicyViewTestCase(WebTest):
    def test_page_exists(self):
        url = reverse('privacy_policy')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'privacy_policy.html')

    def test_page_is_cached(self):
        url = reverse('privacy_policy')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['cache-control'], 'max-age=86400')


class TermsOfServiceViewTestCase(WebTest):
    def test_page_exists(self):
        url = reverse('terms_of_service')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'terms_of_service.html')

    def test_page_is_cached(self):
        url = reverse('terms_of_service')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['cache-control'], 'max-age=86400')


class AboutUsViewTestCase(WebTest):
    def test_page_exists(self):
        url = reverse('about_us')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'about_us.html')

    def test_page_is_cached(self):
        url = reverse('about_us')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response['cache-control'], 'max-age=86400')
