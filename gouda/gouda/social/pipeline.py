import logging
import facebook
from gouda.profiles.models import Profile
from gouda.profiles.models import GenderType
from dateutil.parser import parse as parse_date
from django.conf import settings
from django.forms import ImageField
import uuid
from django.core.files.base import ContentFile
from django_countries import countries
from gouda.users.models import User

FACEBOOK_PUBLIC_KEY = getattr(settings, 'FACEBOOK_PUBLIC_KEY')
USER_FIELDS = ['username', 'email']
logger = logging.getLogger(__name__)


def create_user(strategy, details, user=None, *args, **kwargs):
    # logger.info('USER is %s', user)
    if user:
        return {'is_new': False}

    fields = dict((name, kwargs.get(name) or details.get(name))
                  for name in strategy.setting('USER_FIELDS', USER_FIELDS))

    # Make a password
    fields['password'] = uuid.uuid4().hex

    # Get full name
    fields['full_name'] = details.get('fullname', None)

    # Get email address
    fields['email_address'] = details.get('email', None)

    # logger.info(fields)
    # logger.info(strategy.setting('USER_FIELDS', USER_FIELDS))
    # logger.info(details)
    # logger.info(strategy)
    # logger.info(kwargs)
    if not fields:
        return

    user = None
    try:
        user = User.objects.get(email_address=fields.get('email_address'))
    except User.DoesNotExist:
        pass

    return {
        'is_new': user is None,
        'user': strategy.create_user(**fields) if user is None else user
    }


def create_profile(backend, details, response, uid, user=None, *args, **kwargs):
    logger.info(details)
    logger.info(response)
    if backend.name != 'facebook':
        return

    # Get the birthday
    date_of_birth = response.get('birthday', None)
    logger.info('DOB: %s' % date_of_birth)
    if date_of_birth:
        date_of_birth = parse_date(date_of_birth)

    # Get the gender
    gender = response.get('gender', None)
    if gender:
        gender = gender.lower()
        gender_types = [item[1].lower() for item in GenderType.labels.items()]
        if gender not in gender_types:
            gender = 'other'
        gender = GenderType.get(gender.upper()).value

    # Get location
    locality = ''
    region = ''
    country = ''
    try:
        location = response.get('location', None).get('name', None)
    except:
        location = None
    if location:
        toks = location.split(',')
        toks = [tok.strip() for tok in toks]
        locality = toks[0]
        for c in countries:
            if c[1].lower() == toks[1].lower():
                country = c[0]
                break
        if not country:
            region = toks[1]

    # Update or create a profile
    profile, is_new_profile = Profile.objects.update_or_create(user=user, defaults={
        'date_of_birth': date_of_birth,
        'gender': gender,
        'locality': locality,
        'region': region,
        'country': country
    })
    logger.info('Saved profile (%s)' % profile)

    if is_new_profile:
        # Get a profile picture
        graph = facebook.GraphAPI(response.get('access_token'))
        image = graph.get_connections(uid, 'picture', type='large')
        image_data = image.get('data', None)
        if image_data:
            file_name = uuid.uuid1().hex
            profile.image.save(file_name, ContentFile(image_data, name=file_name))
            profile.save()

    return {
        'is_new_profile': is_new_profile,
        'profile': profile
    }
