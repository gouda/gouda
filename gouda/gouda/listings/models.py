# -*- coding: utf-8 -*-

from __future__ import absolute_import

import logging
from decimal import Decimal

from django.db import models
from gouda.models import EscrowStatusType, CurrencyType
from gouda.profiles.models import Profile
from easy_thumbnails.fields import ThumbnailerImageField
from django.contrib.postgres.fields import ArrayField
from django_enumfield import enum
from django.conf import settings
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.contrib.sessions.models import Session
from gouda.listings.managers import ListingTransactionManager

logger = logging.getLogger(__name__)


MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')
AMOUNT_WITHHELD_PERCENTAGE = getattr(settings, 'AMOUNT_WITHHELD_PERCENTAGE')


def validate_image(obj):
    if obj.file.size > MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES * 1024 * 1024:
        raise ValidationError('Maximum image file size is %d MB' % MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES)


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class ListingStatusType(enum.Enum):
    ACTIVE = 0
    INACTIVE = 1

    labels = {
        ACTIVE: 'Active',
        INACTIVE: 'Inactive',
    }


class ListingConditionType(enum.Enum):
    NEW = 0
    USED = 1
    REFURBISHED = 2

    labels = {
        NEW: 'New',
        USED: 'Used',
        REFURBISHED: 'Refurbished',
    }


class ListingTransactionStatusType(enum.Enum):
    AUTHORIZATION_EXPIRED = 0
    AUTHORIZED = 1
    AUTHORIZING = 2
    SETTLEMENT_PENDING = 3
    SETTLEMENT_CONFIRMED = 4
    SETTLEMENT_DECLINED = 5
    FAILED = 6
    GATEWAY_REJECTED = 7
    PROCESSOR_DECLINED = 8
    SETTLED = 9
    SETTLING = 10
    SUBMITTED_FOR_SETTLEMENT = 11
    VOIDED = 12
    LOCKED = 13  # Our flag...

    labels = {
        AUTHORIZATION_EXPIRED: 'authorization_expired',
        AUTHORIZED: 'authorized',
        AUTHORIZING: 'authorizing',
        SETTLEMENT_PENDING: 'settlement_pending',
        SETTLEMENT_CONFIRMED: 'settlement_confirmed',
        SETTLEMENT_DECLINED: 'settlement_declined',
        FAILED: 'failed',
        GATEWAY_REJECTED: 'gateway_rejected',
        PROCESSOR_DECLINED: 'processor_declined',
        SETTLED: 'settled',
        SETTLING: 'settling',
        SUBMITTED_FOR_SETTLEMENT: 'submitted_for_settlement',
        VOIDED: 'voided',
        LOCKED: 'processing',  # Our flag...
    }


class Listing(BaseModel):
    profile = models.ForeignKey(Profile)
    title = models.CharField(max_length=140, blank=False)

    # Description
    description = models.TextField()
    shipping_policy = models.TextField()
    refund_policy = models.TextField()
    tags = ArrayField(base_field=models.CharField(max_length=32), blank=True)

    # Pricing
    currency = enum.EnumField(CurrencyType, blank=False, default=CurrencyType.USD)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    quantity = models.IntegerField(null=True, blank=True)

    # Status
    status = enum.EnumField(ListingStatusType, blank=False, default=ListingStatusType.ACTIVE)

    # Condition
    condition = enum.EnumField(ListingConditionType, blank=False, default=ListingConditionType.USED)

    # def get_cost(self, quantity):
    #     return (self.price * quantity).quantize(Decimal(10) ** -2)
    #
    # def get_amount_withheld(self, quantity):
    #     return (self.get_cost(quantity) * Decimal(AMOUNT_WITHHELD_PERCENTAGE / 100)).quantize(Decimal(10) ** -2)

    @property
    def currency_symbol(self):
        return CurrencyType.symbols.get(self.currency)

    @property
    def primary_image(self):
        queryset = ListingImage.objects.filter(listing=self, is_primary=True)
        return queryset[0] if queryset.exists() else None

    @property
    def condition_label(self):
        return ListingConditionType.label(self.condition)

    @property
    def is_active(self):
        return self.status == ListingStatusType.ACTIVE

    @property
    def url(self):
        return reverse('listings:detail', kwargs={'pk': self.id})

    @property
    def rating(self):
        reviews = self.reviews.all()
        num_reviews = reviews.count()
        if num_reviews == 0:
            return 0
        sum = 0
        for review in reviews:
            sum += review.rating
        return float(sum) / num_reviews

    @property
    def absolute_url(self):
        return 'https://%s%s' % (Site.objects.get_current().domain, self.url)


class ListingSearchQuery(BaseModel):
    query = models.CharField(max_length=500, blank=False, unique=True)


class ListingImage(BaseModel):
    listing = models.ForeignKey(Listing, related_name='images')
    image = ThumbnailerImageField(max_length=500, upload_to='listing_images/', validators=[validate_image])
    is_primary = models.BooleanField(default=False)
    caption = models.TextField(default='', blank=True)


class Cart(BaseModel):
    session = models.ForeignKey(Session)
    items = models.ManyToManyField(Listing, through='CartItem')

    @property
    def currency_symbol(self):
        return '$'

    @property
    def subtotal_price(self):
        subtotal_price = 0
        for listing in self.items.all():
            subtotal_price += listing.price
        return subtotal_price

    def has_inventory(self, listing, quantity):
        return self.get_quantity(listing) + quantity <= listing.quantity

    def get_quantity(self, listing):
        try:
            cart_item = CartItem.objects.get(cart=self, listing=listing)
        except CartItem.DoesNotExist:
            cart_item = None
        return cart_item.quantity if cart_item else 0


class CartItem(BaseModel):
    cart = models.ForeignKey('Cart')
    listing = models.ForeignKey('Listing')
    quantity = models.IntegerField(default=0)


class Review(BaseModel):
    listing = models.ForeignKey(Listing, related_name='reviews')
    profile = models.ForeignKey(Profile, related_name='listing_reviews')
    title = models.CharField(max_length=100)
    content = models.TextField()
    rating = models.IntegerField()


class ShareMessage(BaseModel):
    body = models.TextField()
    sender = models.ForeignKey(Profile, related_name='share_messages', null=True)
    rental = models.ForeignKey(Listing, related_name='share_messages')
    receiver = models.EmailField()


class ListingTransaction(BaseModel):
    braintree_id = models.CharField(blank=False, max_length=100, unique=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    service_fee_amount = models.DecimalField(max_digits=10, decimal_places=2, verbose_name='Amount withheld')
    escrow_status = enum.EnumField(EscrowStatusType, null=True, blank=True)
    status = enum.EnumField(ListingTransactionStatusType, blank=False)

    buyer = models.ForeignKey(Profile, related_name='listing_transaction_buyer')
    seller = models.ForeignKey(Profile, related_name='listing_transaction_seller')
    notes = models.TextField()

    listings = models.ManyToManyField(Listing, through='ListingTransactionItem')

    objects = ListingTransactionManager()


class ListingTransactionItem(BaseModel):
    listing_transaction = models.ForeignKey('ListingTransaction')
    listing = models.ForeignKey('Listing')
    quantity = models.IntegerField()

    @property
    def cost(self):
        return (self.listing.price * self.quantity).quantize(Decimal(10) ** -2)

    @property
    def amount_withheld(self):
        return (self.cost * Decimal(AMOUNT_WITHHELD_PERCENTAGE / 100)).quantize(Decimal(10) ** -2)


from gouda.listings.signals import *
