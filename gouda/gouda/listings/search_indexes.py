import datetime
from haystack import indexes
from gouda.listings.models import Listing
from gouda.listings.models import ListingSearchQuery
from django.conf import settings
from django.core.urlresolvers import reverse

STATIC_URL = getattr(settings, 'STATIC_URL')


class ListingSearchQueryIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    query = indexes.CharField(model_attr='query')
    content_auto = indexes.EdgeNgramField(model_attr='query')

    def get_model(self):
        return ListingSearchQuery

    def get_updated_field(self):
        return 'date_modified'


class ListingIndex(indexes.ModelSearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    image = indexes.CharField()
    profile_image = indexes.CharField()
    price = indexes.FloatField()
    condition_label = indexes.CharField()
    url = indexes.CharField()

    def prepare_url(self, obj):
        return obj.url

    def prepare_profile_image(self, obj):
        return obj.profile.image.url if obj.profile.image else '%s%s' % (STATIC_URL, 'profiles/images/anonymous_profile.png')

    def prepare_condition_label(self, obj):
        return obj.condition_label

    def prepare_image(self, obj):
        return obj.primary_image.image.url

    def prepare_price(self, obj):
        return float(obj.price)

    def get_updated_field(self):
        return 'date_modified'

    class Meta:
        model = Listing
        fields = [
            'title',
            'currency',
            'status',
            'condition'
        ]
