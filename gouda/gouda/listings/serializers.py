from __future__ import absolute_import

from gouda.serializers import HaystackSerializer
from gouda.listings.search_indexes import ListingIndex
from rest_framework import serializers
from gouda.profiles.serializers import ProfileListSerializer
from gouda.listings.models import Listing
from gouda.listings.models import Review


class ListingIDSerializer(serializers.ModelSerializer):
    def to_internal_value(self, data):
        queryset = Listing.objects.filter(**data)
        if not queryset.exists() or not queryset.count() == 1:
            raise serializers.ValidationError('Listing not found')
        return queryset[0]

    class Meta:
        model = Listing
        fields = ['id']


class ReviewListSerializer(serializers.ModelSerializer):
    profile = ProfileListSerializer(required=True)
    listing = ListingIDSerializer(required=True)

    class Meta:
        model = Review
        fields = ['id',
                  'date_created',
                  'title',
                  'content',
                  'listing',
                  'profile',
                  'rating']


class ListingSerializer(HaystackSerializer):
    class Meta:
        index_classes = [ListingIndex]
        fields = [
            'text',
            'title',
            'currency',
            'price',
            'image',
            'profile_image',
            'status',
            'condition',
            'condition_label',
            'url'
        ]
        ignore_fields = ['text']
        field_aliases = {
            'query': 'text',
        }
