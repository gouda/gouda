from __future__ import absolute_import

from datetime import date
from datetime import timedelta

from django.core.management.base import BaseCommand
from gouda.listings.utils import create_listings
from gouda.users.models import User
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from django.conf import settings
from braintree.test.nonces import Nonces
import random

IS_PRODUCTION = getattr(settings, 'IS_PRODUCTION', False)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('num_listings', type=int)

    def handle(self, *args, **options):
        assert not IS_PRODUCTION

        create_listings(
            options.get('num_listings'),
            num_images=lambda: random.randint(1, 10),
            num_reviews=lambda: random.randint(0, 20),
            quantity=lambda: random.choice([1, 1, 1, random.randrange(5, 100)])
        )

        # for user in User.objects.all():
        #     BraintreeCustomer.objects.update_or_create_braintree_customer(user, Nonces.Transactable)
        #
        #     individual = {
        #         'locality': 'Bakersfield',
        #         'postal_code': '93311',
        #         'region': 'CA',
        #         'street_address': '1504 Parkpath Way',
        #         'date_of_birth': date.today() - timedelta(days=365 * 28),
        #         'email': user.email_address,
        #         'first_name': user.first_name,
        #         'last_name': user.last_name,
        #         'phone': '4437421240'
        #     }
        #     funding = {
        #         'email': user.email_address,
        #         'mobile_phone': '5555555555',
        #         'account_number': '1123581321',
        #         'routing_number': '071101307',
        #     }
        #     braintree_merchant_account, is_created = BraintreeMerchantAccount.objects.update_or_create_braintree_merchant_account(
        #         user=user,
        #         tos_accepted=True,
        #         individual=individual,
        #         funding=funding
        #     )
        #     braintree_merchant_account.status = BraintreeMerchantAccountStatusType.ACTIVE
        #     braintree_merchant_account.save()
        #
        # for booking in Booking.objects.all():
        #     try:
        #         BookingTransaction.objects.create_booking_transaction(booking, BookingTransactionType.LISTING, is_test_mode=True)
        #         if booking.do_charge_security_deposit:
        #             BookingTransaction.objects.create_booking_transaction(booking, BookingTransactionType.SECURITY_DEPOSIT, is_test_mode=True)
        #     except:
        #         continue
        #
