from easy_thumbnails.signals import saved_file
from easy_thumbnails.signal_handlers import generate_aliases
from django.conf import settings
from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from gouda.listings.models import Listing
from gouda.listings.models import ListingSearchQuery
from gouda.listings.models import ShareMessage
from gouda.listings.models import CartItem
from django.dispatch import receiver
from django.contrib.sites.models import Site
from django.template import loader
from django.core.mail import send_mail
from django.core.cache import cache
from django.core.cache.utils import make_template_fragment_key

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')

# For thumbnails
saved_file.connect(generate_aliases)


@receiver(post_save, sender=Listing)
def handle_listing_post_save(sender, instance, created, **kwargs):
    # Remove template fragment
    cache.delete(make_template_fragment_key('listing_detail', [instance.id]))

    for tag in instance.tags:
        ListingSearchQuery.objects.get_or_create(query=tag)


@receiver(pre_save, sender=Listing)
def handle_listing_pre_save(sender, instance, **kwargs):
    instance.tags = list(set(instance.tags))


@receiver(post_save, sender=ShareMessage)
def handle_share_message_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    # Build context
    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
    })
    # else:
    #     context = {
    #         'domain': 'testserver.com',
    #         'site_name': 'testserver',
    #         'protocol': 'http',
    #     }
    context['share_message'] = instance

    # Render
    subject = loader.render_to_string('listings/emails/share_message_subject.txt', context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string('listings/emails/share_message_body.txt', context)

    # Send
    send_mail(subject=subject, message=body, from_email=DEFAULT_FROM_EMAIL, recipient_list=[instance.receiver])
