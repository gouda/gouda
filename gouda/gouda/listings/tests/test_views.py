from __future__ import absolute_import

from datetime import date
from datetime import timedelta

from django.core.urlresolvers import reverse
from rest_framework import status
from django_webtest import WebTest
from gouda.users.models import User
from gouda.profiles.models import Profile
from gouda.addresses.models import Address
from gouda.listings.models import Listing
from gouda.listings.models import Cart
from gouda.listings.models import CartItem
from gouda.listings.models import ListingStatusType
from gouda.listings.utils import create_listings
from gouda.listings.utils import tear_down_listings
from gouda.customers.utils import tear_down_customers
from django.core.cache import cache
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccount
from braintree.test.nonces import Nonces
from gouda.urlresolvers import reverse_with_query
from gouda.customers.utils import braintree
from unittest import skip


class BaseTestCases(object):
    class BaseListingViewTestCase(WebTest):
        view_name = None
        template_name = None
        do_never_cache = True
        user = None

        def get_url(self):
            raise NotImplementedError()

        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.BaseListingViewTestCase, cls).setUpClass()
            create_listings(1)
            # for user in User.objects.all():
            #     BraintreeCustomer.objects.update_or_create_braintree_customer(user, Nonces.Transactable)
            # user = Listing.objects.all()[0].profile.user
            # individual = {
            #     'locality': 'Bakersfield',
            #     'postal_code': '93311',
            #     'region': 'CA',
            #     'street_address': '1504 Parkpath Way',
            #     'date_of_birth': date.today() - timedelta(days=365 * 28),
            #     'email': user.email_address,
            #     'first_name': user.first_name,
            #     'last_name': user.last_name,
            #     'phone': '4437421240'
            # }
            # funding = {
            #     'email': user.email_address,
            #     'mobile_phone': '5555555555',
            #     'account_number': '1123581321',
            #     'routing_number': '071101307',
            # }
            # BraintreeMerchantAccount.objects.update_or_create_braintree_merchant_account(
            #     user=user,
            #     tos_accepted=True,
            #     individual=individual,
            #     funding=funding
            # )
            # cache.clear()

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.BaseListingViewTestCase, cls).tearDownClass()
            tear_down_listings()

        def test_never_cache_headers(self):
            if not self.do_never_cache:
                return
            url = self.get_url()
            response = self.app.get(url, user=self.user.email_address if self.user else None)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            # Check caching
            self.assertIn('Last-Modified', response.headers)
            self.assertIn('Expires', response.headers)
            self.assertIn('Cache-Control', response.headers)
            self.assertIn('max-age=0', response.headers['Cache-Control'])


@skip('')
class BraintreeCustomerCreateViewTestCase(BaseTestCases.BaseListingViewTestCase):
    def setUp(self):
        self.user = User.objects.all()[0]
        self.data = {
            'user': self.user.pk,
            'payment_method_nonce': Nonces.Transactable,
            'cardholder_name': self.user.full_name,
            'full_name': self.user.full_name,
            'street_address': '1504 Parkpath Way',
            'sub_premise': 'Apt #7',
            'postal_code': '93311',
            'locality': 'Bakersfield',
            'region': 'California',
            'country': 'US'
        }
        self.assertEqual(BraintreeCustomer.objects.all().count(), 0)

    def tearDown(self):
        tear_down_customers()

    def get_url(self):
        return reverse('listings:customer_create')

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'listings/checkout/customer_create.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertIsNotNone(response.form)

    def test_create_customer(self):
        profile = self.user.profile
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('listings:review_order'))
        self.assertEqual(BraintreeCustomer.objects.all().count(), 1)
        braintree_customer = BraintreeCustomer.objects.all()[0]
        customer = braintree.Customer.find(braintree_customer.braintree_id)
        self.assertIsNotNone(customer)
        self.assertEqual(len(customer.credit_cards), 1)

    def test_blank_data(self):
        profile = self.user.profile
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        for k in self.data.keys():
            if k in ['postal_code', 'sub_premise', 'country', 'user']:
                continue
            self.assertFormError(response, 'form', k, 'This field is required.')


@skip('')
class ShippingAddressCreateViewTestCase(BaseTestCases.BaseListingViewTestCase):
    data = {
        'full_name': 'Chukwuemeka Ezekwe',
        'street_address': '5719 Old Buggy Court',
        'postal_code': '21045',
        'locality': 'Columbia',
        'region': 'Maryland',
        'country': 'US'
    }

    def setUp(self):
        super(ShippingAddressCreateViewTestCase, self).setUp()
        self.user = User.objects.all()[0]

    def tearDown(self):
        super(ShippingAddressCreateViewTestCase, self).tearDown()
        Address.objects.all().delete()

    def get_url(self):
        return reverse('listings:shipping_address_create')

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'listings/checkout/shipping_address_create.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_create_shipping_address(self):
        profile = self.user.profile
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('listings:customer_create'))
        self.assertEqual(profile.shipping_addresses.all().count(), 1)
        shipping_address = profile.shipping_addresses.all()[0]
        for k in self.data.keys():
            self.assertEqual(self.data.get(k), getattr(shipping_address, k))

    def test_prevent_duplicate_shipping_address(self):
        profile = self.user.profile
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        for i in range(5):
            url = self.get_url()
            response = self.app.get(url, user=self.user.email_address)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            form = response.form
            for k in self.data.keys():
                form[k] = self.data.get(k)
            response = form.submit()
            self.assertRedirects(response, reverse('listings:customer_create'))

        self.assertEqual(profile.shipping_addresses.all().count(), 1)
        shipping_address = profile.shipping_addresses.all()[0]
        for k in self.data.keys():
            self.assertEqual(self.data.get(k), getattr(shipping_address, k))

    def test_prevent_multiple_primary_shipping_address(self):
        profile = self.user.profile
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        for i in range(5):
            data = self.data.copy()
            data['street_address'] = i
            url = self.get_url()
            response = self.app.get(url, user=self.user.email_address)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            form = response.form
            for k in data.keys():
                form[k] = data.get(k)
            response = form.submit()
            self.assertRedirects(response, reverse('listings:customer_create'))
        self.assertEqual(profile.shipping_addresses.all().count(), 5)
        self.assertEqual(Address.objects.filter(profileshippingaddress__profile=profile.pk, profileshippingaddress__is_primary=True).count(), 1)

    def test_blank_data(self):
        profile = self.user.profile
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(profile.shipping_addresses.all().count(), 0)
        for k in self.data.keys():
            if k in ['postal_code', 'sub_premise']:
                continue
            self.assertFormError(response, 'form', k, 'This field is required.')


@skip('')
class ListingSearchViewTestCase(BaseTestCases.BaseListingViewTestCase):
    form_data = {
        'query': 'Drone',
    }

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertIsNotNone(response.form)

    # def test_blank_data(self):
    #     url = self.get_url()
    #     response = self.app.get(url)
    #     form = response.form
    #     form['query'] = None
    #     response = form.submit()
    #     print response.context
    #     for key in self.form_data.keys():
    #         self.assertFormError(response, 'form', key, 'This field is required.')

    def get_url(self):
        return reverse('listings:search')


@skip('')
class ListingSearchResultsViewTestCase(BaseTestCases.BaseListingViewTestCase):
    def test_page_exists(self):
        url = reverse('listings:search_results')
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def get_url(self):
        return reverse('listings:search_results')


@skip('')
class ListingDetailViewTestCase(BaseTestCases.BaseListingViewTestCase):
    @classmethod
    def setUpClass(cls):
        super(ListingDetailViewTestCase, cls).setUpClass()
        create_listings(1)
        listing = Listing.objects.all()[1]
        listing.status = ListingStatusType.INACTIVE
        listing.save()

    @classmethod
    def tearDownClass(cls):
        super(ListingDetailViewTestCase, cls).tearDownClass()
        tear_down_listings()

    def setUp(self):
        super(ListingDetailViewTestCase, self).setUp()
        self.listing = Listing.objects.filter(status=ListingStatusType.ACTIVE)[0]
        self.profile = Profile.objects.all()[0]
        self.profile.user.is_staff = False
        self.profile.user.save()
        self.assertEqual(Cart.objects.all().count(), 0)
        self.assertEqual(CartItem.objects.all().count(), 0)

    def tearDown(self):
        Cart.objects.all().delete()
        CartItem.objects.all().delete()

    def get_url(self):
        return reverse('listings:detail', kwargs={'pk': self.listing.id})

    def test_correct_template_is_used(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'listings/listing_detail.html')

    def test_access_unviewable_listing(self):
        listing = Listing.objects.filter(status=ListingStatusType.INACTIVE)[0]
        url = reverse('listings:detail', kwargs={'pk': listing.id})
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_access_unviewable_listing_as_staff(self):
        listing = Listing.objects.filter(status=ListingStatusType.INACTIVE)[0]
        user = self.profile.user
        user.is_staff = True
        user.save()
        url = reverse('listings:detail', kwargs={'pk': listing.id})
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_never_cache_headers(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def add_to_cart(self, form_id, quantity=1, do_expect_errors=False):
        url = reverse('listings:detail', kwargs={'pk': self.listing.id})
        response = self.app.get(url)
        form = response.forms[form_id]
        form['quantity'] = quantity
        response = form.submit()
        # self.assertTemplateUsed(response, 'listings/cart_update.html')
        if do_expect_errors:
            self.assertFormError(response, 'form', '', 'Invalid quantity')
        else:
            self.assertRedirects(response, reverse('listings:cart_update'))
            self.assertEqual(Cart.objects.all().count(), 1)
            cart = Cart.objects.all()[0]
            self.assertEqual(cart.items.all().count(), 1)

    def test_add_to_cart_with_inline_form(self):
        self.add_to_cart('inline-add-to-cart-form')

    def test_add_to_cart(self):
        self.add_to_cart('add-to-cart-form')

    def test_add_to_cart_with_invalid_quantity(self):
        self.add_to_cart('add-to-cart-form', 9000, do_expect_errors=True)


@skip('')
class CartUpdateViewTestCase(BaseTestCases.BaseListingViewTestCase):
    @classmethod
    def setUpClass(cls):
        super(CartUpdateViewTestCase, cls).setUpClass()
        create_listings(5, num_profiles=10)
        for listing in Listing.objects.all():
            listing.quantity = 9000
            listing.save()

    @classmethod
    def tearDownClass(cls):
        super(CartUpdateViewTestCase, cls).tearDownClass()
        tear_down_listings()

    def add_to_cart(self, listing, quantity=1):
        num_cart_items = CartItem.objects.all().count()
        url = listing.url
        response = self.app.get(url)
        form = response.forms['add-to-cart-form']
        form['quantity'] = quantity
        response = form.submit()
        self.assertRedirects(response, reverse('listings:cart_update'))
        self.assertEqual(Cart.objects.all().count(), 1)
        self.assertEqual(CartItem.objects.all().count(), num_cart_items + 1)

    def tearDown(self):
        super(CartUpdateViewTestCase, self).tearDown()
        Cart.objects.all().delete()
        CartItem.objects.all().delete()

    def setUp(self):
        super(CartUpdateViewTestCase, self).setUp()
        for listing in Listing.objects.all():
            self.add_to_cart(listing)
        self.cart = Cart.objects.all()[0]

    def get_url(self):
        return reverse('listings:cart_update')

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.form)
        self.assertTemplateUsed(response, 'listings/cart_update.html')

    def test_update_cart(self):
        quantity = 10
        url = self.get_url()
        response = self.app.get(url)
        form = response.form
        form['cartitem_set-0-quantity'] = quantity
        response = form.submit()
        cart_item = CartItem.objects.get(id=form.get('cartitem_set-0-id').value)
        self.assertRedirects(response, reverse('listings:cart_update'))
        self.assertEqual(cart_item.quantity, quantity)

    def test_update_cart_with_zero_quantity(self):
        num_items = Cart.objects.get(id=self.cart.id).items.all().count()
        quantity = 0
        url = self.get_url()
        response = self.app.get(url)
        form = response.form
        form['cartitem_set-0-quantity'] = quantity
        cart_item_id = form.get('cartitem_set-0-id').value
        response = form.submit()
        self.assertRedirects(response, reverse('listings:cart_update'))
        self.assertFalse(CartItem.objects.filter(id=cart_item_id).exists())
        self.assertEqual(Cart.objects.get(id=self.cart.id).items.all().count(), num_items - 1)

    def test_update_cart_with_invalid_quantity(self):
        quantity = 9000 * 10
        url = self.get_url()
        response = self.app.get(url)
        form = response.form
        form['cartitem_set-0-quantity'] = quantity
        response = form.submit()
        cart_item = CartItem.objects.get(id=form.get('cartitem_set-0-id').value)
        self.assertEqual(cart_item.quantity, 1)
        self.assertTrue(bool(response.context['formset'].forms[0].errors.as_data()))
