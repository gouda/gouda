from __future__ import absolute_import

import sys
from gouda.listings.utils import create_listings
from gouda.listings.utils import tear_down_listings
from gouda.urlresolvers import reverse_with_query
from django.core.management import call_command
from django.core.urlresolvers import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from gouda.listings.models import Listing
from gouda.listings.models import ListingConditionType
from haystack.query import SearchQuerySet
from unittest import skip


@skip('')
class ReviewRetrievalAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ReviewRetrievalAPITestCase, cls).setUpClass()
        create_listings(20, num_reviews=20)

    @classmethod
    def tearDownClass(cls):
        super(ReviewRetrievalAPITestCase, cls).tearDownClass()
        tear_down_listings()

    def test_review_list(self):
        url = reverse('api_listings:review_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check data
        reviews = response.data['results']
        self.assertGreater(len(reviews), 0)
        self.assertLessEqual(len(reviews), 20)
        for review in reviews:
            self.assertIn('listing', review)
            self.assertIn('id', review.get('listing'))
            self.assertIn('profile', review)
            self.assertIn('image', review.get('profile'))
            self.assertIn('user', review.get('profile'))
            self.assertIn('fullName', review.get('profile').get('user'))
            self.assertIn('title', review)
            self.assertIn('content', review)
            self.assertIn('rating', review)
            self.assertIn('id', review)
            self.assertIn('dateCreated', review)

    def test_review_list_caching(self):
        url = reverse('api_listings:review_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response)
        self.assertIn('Expires', response)
        self.assertIn('Cache-Control', response)
        self.assertIn('max-age=3600', response['Cache-Control'])

    def test_filter_reviews_by_listing_id(self):
        listing = Listing.objects.all()[0]
        url = reverse_with_query('api_listings:review_list', {'listing': listing.id})
        response = self.client.get(url)
        reviews = response.data['results']
        self.assertGreater(len(reviews), 0)
        for review in reviews:
            self.assertEqual(listing.id, review.get('listing').get('id'))

    def test_review_list_pagination(self):
        listing = Listing.objects.all()[0]
        url = reverse_with_query('api_listings:review_list', {'limit': 5, 'offset': 1, 'listing': listing.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        self.assertIn('count', response.data)
        self.assertIn('next', response.data)
        self.assertIn('previous', response.data)
        self.assertIn('results', response.data)
        self.assertIsNotNone(response.data.get('next'))
        self.assertIsNotNone(response.data.get('previous'))


@skip('')
class ListingRetrievalAPITestCase(APITestCase):
    @classmethod
    def setUpClass(cls):
        super(ListingRetrievalAPITestCase, cls).setUpClass()
        create_listings(20, num_images=1)
        call_command('rebuild_index', interactive=False)

    @classmethod
    def tearDownClass(cls):
        super(ListingRetrievalAPITestCase, cls).tearDownClass()
        tear_down_listings()

    def test_listing_list_caching(self):
        url = reverse('api_listings:listing_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response)
        self.assertIn('Expires', response)
        self.assertIn('Cache-Control', response)
        self.assertIn('max-age=3600', response['Cache-Control'])

    def test_listing_list(self):
        url = reverse('api_listings:listing_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check data
        listings = response.data['results']
        self.assertGreater(len(listings), 0)
        for listing in listings:
            self.assertNotIn('text', listing)
            self.assertIn('image', listing)
            self.assertIn('url', listing)
            self.assertIn('profileImage', listing)
            self.assertIn('price', listing)
            self.assertIn('title', listing)
            self.assertIn('currency', listing)
            self.assertIn('status', listing)
            self.assertIn('condition', listing)
            self.assertIn('conditionLabel', listing)

    def test_filter_by_query(self):
        title = Listing.objects.all()[0].title
        url = reverse_with_query('api_listings:listing_list', {'query': title})
        response = self.client.get(url)
        listings = response.data.get('results')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(listings), 0)
        listing_titles = [listing.get('title') for listing in listings]

        search_results = SearchQuerySet().filter(content=title)
        search_result_titles = [search_result.title for search_result in search_results]

        self.assertEqual(listing_titles, search_result_titles)
        # for listing in listings:
        #     print listing
        #     # self.assertEqual(listing.get('title'), country)

    def test_filter_by_price(self):
        min_price = Listing.objects.all().order_by('price')[0].price
        max_price = Listing.objects.all().order_by('-price')[0].price
        min_price += 1
        max_price -= 1
        url = reverse_with_query('api_listings:listing_list', {'maxPrice': max_price, 'minPrice': min_price})
        response = self.client.get(url)
        listings = response.data.get('results')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(listings), 0)
        for listing in listings:
            self.assertGreaterEqual(listing.get('price'), min_price)
            self.assertLessEqual(listing.get('price'), max_price)

    def test_filter_by_condition(self):
        condition = ListingConditionType.NEW
        url = reverse_with_query('api_listings:listing_list', {'condition': condition})
        response = self.client.get(url)
        listings = response.data.get('results')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(listings), 0)
        for listing in listings:
            self.assertEqual(int(listing.get('condition')), condition)

    def test_listing_list_pagination(self):
        url = reverse_with_query('api_listings:listing_list', {'limit': 5, 'offset': 1})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('count', response.data)
        self.assertIn('next', response.data)
        self.assertIn('previous', response.data)
        self.assertIn('results', response.data)
        self.assertIsNotNone(response.data['next'])
        self.assertIsNotNone(response.data['previous'])
        self.assertGreater(len(response.data['results']), 0)
