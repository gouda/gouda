from gouda.listings.forms import ListingSearchForm
from gouda.listings.forms import AddToCartForm
from gouda.listings.forms import CartItemForm
from gouda.listings.forms import CartItemFormSet
from gouda.listings.models import Listing
from gouda.listings.models import ListingTransaction
from gouda.listings.models import Cart
from gouda.listings.models import CartItem
from django.test import TestCase
from gouda.listings.utils import create_listings
from gouda.listings.utils import tear_down_listings
from gouda.profiles.models import Profile
from gouda.listings.forms import ListingTransactionForm
from importlib import import_module
from django.conf import settings
from django.contrib.sessions.models import Session
from unittest import skip
SessionStore = import_module(settings.SESSION_ENGINE).SessionStore


@skip('')
class ListingTransactionFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ListingTransactionFormTestCase, cls).setUpClass()
        create_listings(1, num_profiles=2, quantity=10, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ListingTransactionFormTestCase, cls).tearDownClass()
        tear_down_listings()

    def setUp(self):
        super(ListingTransactionFormTestCase, self).setUp()
        # Create session
        session = SessionStore()
        session.save()
        self.session = Session.objects.get(session_key=session.session_key)
        self.listing = Listing.objects.all()[0]
        self.profile = Profile.objects.exclude(id=self.listing.profile.id)[0]
        cart, is_created = Cart.objects.get_or_create(session=self.session)
        cart_item, is_created = CartItem.objects.get_or_create(
            cart=cart,
            listing=self.listing,
            quantity=1
        )
        self.data = {
            'buyer': self.profile.id,
            'seller': self.listing.profile.id,
            'notes': 'Hello World!',
            'cart_items': [cart_item.id for cart_item in [cart_item]]
        }

    def tearDown(self):
        ListingTransaction.objects.all().delete()
        CartItem.objects.all().delete()

    def test_init(self):
        ListingTransactionForm()

    def test_invalid_quantity(self):
        self.assertEqual(ListingTransaction.objects.all().count(), 0)
        data = self.data.copy()
        CartItem.objects.all().update(quantity=9000)
        listing_transaction_form = ListingTransactionForm(data)
        self.assertFalse(listing_transaction_form.is_valid())

    def test_create_listing_transaction(self):
        self.assertEqual(ListingTransaction.objects.all().count(), 0)
        listing_transaction_form = ListingTransactionForm(self.data)
        self.assertTrue(listing_transaction_form.is_valid(), msg='%s' % listing_transaction_form.errors.as_data())
        listing_transaction = listing_transaction_form.save()
        self.assertIsNotNone(listing_transaction)
        self.assertEqual(ListingTransaction.objects.all().count(), 1)


@skip('')
class ListingSearchFormTestCase(TestCase):
    form_data = {
        'query': 'Drone',
    }

    def test_init(self):
        ListingSearchForm()

    def test_valid_data(self):
        listing_search_form = ListingSearchForm(self.form_data)
        listing_search_form.is_valid()
        self.assertTrue(listing_search_form.is_valid())

    def test_blank_data(self):
        listing_search_form = ListingSearchForm({})
        self.assertFalse(listing_search_form.is_valid())
        self.assertIn('query', listing_search_form.errors)


@skip('')
class CartItemFormSetTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(CartItemFormSetTestCase, cls).setUpClass()
        create_listings(10, num_profiles=10, quantity=9000)

    @classmethod
    def tearDownClass(cls):
        super(CartItemFormSetTestCase, cls).tearDownClass()
        tear_down_listings()

    def setUp(self):
        # Create session
        session = SessionStore()
        session.save()
        self.session = Session.objects.get(session_key=session.session_key)

        # Create cart
        self.cart, is_created = Cart.objects.get_or_create(session=self.session)

        # Add stuff to the cart
        for listing in Listing.objects.all():
            CartItem.objects.create(
                listing=listing,
                cart=self.cart,
                quantity=1
            )
        self.assertEqual(Cart.objects.all().count(), 1)

    def tearDown(self):
        Cart.objects.all().delete()
        CartItem.objects.all().delete()

    def test_init(self):
        CartItemFormSet()

    def test_formset_is_rightish(self):
        queryset = CartItem.objects.filter(cart=self.cart)
        formset = CartItemFormSet(instance=self.cart)
        self.assertEqual(len(formset.forms), queryset.count())


@skip('')
class CartItemFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(CartItemFormTestCase, cls).setUpClass()
        create_listings(10, num_profiles=10, quantity=9000)

    @classmethod
    def tearDownClass(cls):
        super(CartItemFormTestCase, cls).tearDownClass()
        tear_down_listings()

    def setUp(self):
        # Create session
        session = SessionStore()
        session.save()
        self.session = Session.objects.get(session_key=session.session_key)

        # Create cart
        self.cart, is_created = Cart.objects.get_or_create(session=self.session)

        # Add stuff to the cart
        for listing in Listing.objects.all():
            CartItem.objects.create(
                listing=listing,
                cart=self.cart,
                quantity=1
            )
        self.assertEqual(Cart.objects.all().count(), 1)

    def tearDown(self):
        Cart.objects.all().delete()
        CartItem.objects.all().delete()

    def test_init(self):
        CartItemForm()

    def test_update_quantity(self):
        num_cart_items = CartItem.objects.all().count()
        cart_item = CartItem.objects.all()[0]
        data = {
            'quantity': 5,
            'cart': cart_item.cart.id,
            'listing': cart_item.listing.id
        }
        cart_item_form = CartItemForm(data=data, instance=cart_item)
        self.assertTrue(cart_item_form.is_valid())
        cart_item = cart_item_form.save()
        self.assertIsNotNone(cart_item)
        self.assertEqual(cart_item.quantity, data.get('quantity'))
        self.assertEqual(CartItem.objects.all().count(), num_cart_items)

    def test_update_with_invalid_quantity(self):
        num_cart_items = CartItem.objects.all().count()
        cart_item = CartItem.objects.all()[0]
        data = {
            'quantity': cart_item.listing.quantity * 2,
            'cart': cart_item.cart.id,
            'listing': cart_item.listing.id
        }
        cart_item_form = CartItemForm(data=data, instance=cart_item)
        self.assertFalse(cart_item_form.is_valid())
        self.assertEqual(CartItem.objects.all().count(), num_cart_items)

    def test_set_quantity_to_zero(self):
        num_cart_items = CartItem.objects.all().count()
        cart_item = CartItem.objects.all()[0]
        data = {
            'quantity': 0,
            'cart': cart_item.cart.id,
            'listing': cart_item.listing.id
        }
        cart_item_form = CartItemForm(data=data, instance=cart_item)
        self.assertTrue(cart_item_form.is_valid())
        cart_item = cart_item_form.save()
        self.assertIsNotNone(cart_item)
        self.assertEqual(CartItem.objects.all().count(), num_cart_items - 1)


@skip('')
class AddToCartFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(AddToCartFormTestCase, cls).setUpClass()
        create_listings(1, num_profiles=10)

    @classmethod
    def tearDownClass(cls):
        super(AddToCartFormTestCase, cls).tearDownClass()
        tear_down_listings()

    def setUp(self):
        self.listing = Listing.objects.all()[0]
        self.listing.quantity = 20
        self.listing.save()

        # Create session
        session = SessionStore()
        session.save()
        self.session = Session.objects.get(session_key=session.session_key)

        self.data = {
            'session': self.session.session_key,
            'listing': self.listing.id,
            'quantity': 1
        }
        self.assertEqual(Cart.objects.all().count(), 0)

    def tearDown(self):
        Cart.objects.all().delete()
        CartItem.objects.all().delete()

    def test_init(self):
        AddToCartForm()

    def test_add_to_cart(self):
        add_to_cart_form = AddToCartForm(self.data)
        self.assertTrue(add_to_cart_form.is_valid())
        cart = add_to_cart_form.save()
        self.assertEqual(Cart.objects.all().count(), 1)
        self.assertEqual(cart.items.all().count(), 1)

    def test_multiple_add_to_cart(self):
        cart = None
        for i in range(5):
            data = self.data.copy()
            data['quantity'] = 1
            add_to_cart_form = AddToCartForm(data)
            self.assertTrue(add_to_cart_form.is_valid())
            cart = add_to_cart_form.save()

        self.assertIsNotNone(cart)
        self.assertEqual(Cart.objects.all().count(), 1)
        self.assertEqual(cart.items.all().count(), 1)
        self.assertEqual(CartItem.objects.filter(cart__id=cart.id, listing__id=self.listing.id, quantity=5).count(), 1)

    def test_invalid_quantity(self):
        data = self.data.copy()
        data['quantity'] = 9000
        add_to_cart_form = AddToCartForm(data)
        self.assertFalse(add_to_cart_form.is_valid())

    def test_lack_of_inventory(self):
        for i in range(self.listing.quantity * 2):
            data = self.data.copy()
            data['quantity'] = 1
            add_to_cart_form = AddToCartForm(data)
            if i < self.listing.quantity:
                self.assertTrue(add_to_cart_form.is_valid())
                add_to_cart_form.save()
            else:
                self.assertFalse(add_to_cart_form.is_valid())
