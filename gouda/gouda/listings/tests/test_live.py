# import time
#
# from django.contrib.staticfiles.testing import StaticLiveServerTestCase
# from selenium.webdriver.firefox.webdriver import WebDriver
# from selenium.webdriver.support.select import Select
# from selenium.webdriver.common.action_chains import ActionChains
# from django.core.urlresolvers import reverse
# from gouda.listings.utils import create_listings
# from gouda.users.models import User
# from gouda.profiles.models import Profile
# from gouda.addresses.models import Address
# from gouda.listings.models import Listing
# from gouda.listings.models import Cart
# from gouda.listings.models import CartItem
# from gouda.listings.models import ListingStatusType
# from gouda.listings.utils import create_listings
# from gouda.listings.utils import tear_down_listings
# from gouda.customers.utils import tear_down_customers
# from django.core.cache import cache
# from gouda.customers.models import BraintreeCustomer
# from gouda.customers.models import BraintreeMerchantAccount
# from braintree.test.nonces import Nonces
# from gouda.urlresolvers import reverse_with_query
# from gouda.customers.utils import braintree
#
#
# class BraintreeCustomerCreateTestCase(StaticLiveServerTestCase):
#     @classmethod
#     def setUpClass(cls):
#         super(BraintreeCustomerCreateTestCase, cls).setUpClass()
#         cls.selenium = WebDriver()
#         create_listings(5)
#
#     @classmethod
#     def tearDownClass(cls):
#         super(BraintreeCustomerCreateTestCase, cls).tearDownClass()
#         cls.selenium.quit()
#
#     def setUp(self):
#         # self.login()
#         for listing in Listing.objects.all():
#             self.add_to_cart(listing)
#         self.profile = Profile.objects.get(user__email_address='success+sterlingarcher@simulator.amazonses.com')
#         shipping_address = Address.objects.create(
#             full_name=self.profile.full_name,
#             street_address='5719 Old Buggy Court',
#             postal_code='21045',
#             locality='Columbia',
#             region='Maryland',
#             country='US'
#         )
#         self.profile.shipping_addresses.add(shipping_address)
#
#     def tearDown(self):
#         super(BraintreeCustomerCreateTestCase, self).tearDown()
#         Cart.objects.all().delete()
#         CartItem.objects.all().delete()
#         Address.objects.all().delete()
#
#     def add_to_cart(self, listing):
#         url = self.reverse('listings:detail', kwargs={'pk': listing.pk})
#         print url
#         self.selenium.get(url)
#         time.sleep(5)
#         self.selenium.save_screenshot('screenie.png')
#         print self.selenium.page_source.encode("utf-8")
#         time.sleep(5)
#         self.selenium.find_element_by_xpath('//button[@id="add-to-cart-button"]').click()
#         time.sleep(0.5)
#
#     def login(self):
#         self.selenium.get(self.reverse('profiles:login'))
#         self.selenium.save_screenshot('screenie1.png')
#         username_input = self.selenium.find_element_by_name('username')
#         username_input.send_keys('success+sterlingarcher@simulator.amazonses.com')
#         password_input = self.selenium.find_element_by_name('password')
#         password_input.send_keys('sterlingarcher123')
#         self.selenium.find_element_by_xpath('//button[@id="login-button"]').click()
#         time.sleep(0.5)
#
#     def reverse(self, viewname, kwargs=None):
#         return '%s%s' % (self.live_server_url, reverse(viewname, kwargs=kwargs))
#
#     def test_create_customer(self):
#         self.selenium.get(self.reverse('listings:customer_create'))
#         time.sleep(500)
