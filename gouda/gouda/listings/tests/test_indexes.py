from __future__ import absolute_import

from django.test import TestCase
from gouda.listings.models import Listing
from gouda.listings.utils import create_listings
from gouda.listings.utils import tear_down_listings
from django.core.management import call_command
from haystack.query import SearchQuerySet
from haystack.inputs import AutoQuery, Exact, Clean
from unittest import skip


class BaseTestCases(object):
    class BaseListingIndexTestCase(TestCase):
        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.BaseListingIndexTestCase, cls).setUpClass()
            create_listings(20, num_images=1)
            call_command('rebuild_index', interactive=False)

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.BaseListingIndexTestCase, cls).tearDownClass()
            tear_down_listings()


@skip('')
class ListingIndexTestCase(BaseTestCases.BaseListingIndexTestCase):
    def test_query_by_tag(self):
        tag = Listing.objects.all()[0].tags[0]
        listings = Listing.objects.filter(tags__contains=[tag])
        search_results = SearchQuerySet().filter(content=tag)
        self.assertGreater(listings.count(), 0)
        self.assertGreater(search_results.count(), 0)
        self.assertEqual(listings.count(), search_results.count())

        listing_pks = set([listing.pk for listing in listings])
        search_result_pks = set([int(search_result.pk) for search_result in search_results])
        self.assertEqual(listing_pks, search_result_pks)


@skip('')
class ListingSearchQueryIndexTestCase(BaseTestCases.BaseListingIndexTestCase):
    def test_autocomplete(self):
        tag = Listing.objects.all()[0].tags[0]
        query = tag[:3]
        search_results = SearchQuerySet().autocomplete(content_auto=query)

        for search_result in search_results:
            self.assertGreater(search_result.score, 1.0)
