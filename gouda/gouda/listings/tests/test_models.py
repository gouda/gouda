from __future__ import absolute_import

from django.conf import settings
from django.test import TestCase
from gouda.listings.models import Listing
from gouda.profiles.models import Profile
from gouda.listings.models import ShareMessage
from gouda.listings.models import ListingSearchQuery
from gouda.listings.models import ListingImage
from gouda.listings.models import Review
from gouda.listings.models import Cart
from gouda.listings.models import ListingTransaction
from gouda.listings.models import ListingTransactionItem
from gouda.listings.models import ListingTransactionStatusType
from gouda.models import EscrowStatusType
from gouda.listings.utils import create_listings
from gouda.listings.utils import tear_down_listings
from django.core import mail
import re
from gouda.customers.utils import braintree
import random
from decimal import *
from unittest import skip

AMOUNT_WITHHELD_PERCENTAGE = getattr(settings, 'AMOUNT_WITHHELD_PERCENTAGE')


@skip('')
class ListingTransactionTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ListingTransactionTestCase, cls).setUpClass()
        create_listings(1, num_profiles=2, quantity=10, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ListingTransactionTestCase, cls).tearDownClass()
        tear_down_listings()

    def setUp(self):
        super(ListingTransactionTestCase, self).setUp()
        self.listing = Listing.objects.all()[0]
        self.profile = Profile.objects.exclude(id=self.listing.profile.id)[0]

    def tearDown(self):
        ListingTransaction.objects.all().delete()

    def test_init(self):
        ListingTransaction()

    def create_listing_transaction(self, listing, quantity, do_force_duplicate_check=False):
        listing_transaction_item = ListingTransactionItem(listing=listing, quantity=quantity)
        listing_transaction = ListingTransaction.objects.create_listing_transaction(
            listing_transaction_items=[listing_transaction_item],
            buyer=self.profile,
            seller=listing.profile,
            notes='Hello World!',
            do_force_duplicate_check=do_force_duplicate_check
        )
        self.assertIsNotNone(listing_transaction)
        return listing_transaction, listing_transaction_item

    def test_create_listing_transaction(self):
        quantity = random.randrange(1, self.listing.quantity)
        listing_transaction, listing_transaction_item = self.create_listing_transaction(self.listing, quantity)

        # Verify data in DB
        self.assertEqual(listing_transaction.amount, listing_transaction_item.cost)
        self.assertEqual(listing_transaction.service_fee_amount, listing_transaction_item.amount_withheld)
        self.assertEqual(listing_transaction.status, ListingTransactionStatusType.AUTHORIZED)
        self.assertEqual(listing_transaction.escrow_status, EscrowStatusType.HOLD_PENDING)

        # Verify data in Braintree
        transaction = braintree.Transaction.find(listing_transaction.braintree_id)
        self.assertEqual(Decimal(transaction.amount), listing_transaction_item.cost)
        self.assertEqual(Decimal(transaction.service_fee_amount), listing_transaction_item.amount_withheld)
        self.assertEqual(
            transaction.status,
            ListingTransactionStatusType.labels.get(
                ListingTransactionStatusType.AUTHORIZED
            )
        )
        self.assertEqual(
            transaction.escrow_status,
            EscrowStatusType.labels.get(
                EscrowStatusType.HOLD_PENDING
            )
        )

    def test_check_for_duplicate_transactions(self):
        quantity = 1
        self.create_listing_transaction(self.listing, quantity)
        self.assertRaises(AssertionError, self.create_listing_transaction, listing=self.listing, quantity=quantity, do_force_duplicate_check=True)

    def test_invalid_quantity(self):
        quantity = self.listing.quantity * 2
        self.assertRaises(AssertionError, self.create_listing_transaction, listing=self.listing, quantity=quantity)


@skip('')
class ListingTestCase(TestCase):
    def setUp(self):
        listings = create_listings(1, num_reviews=10)
        self.listing = listings[0]

    def tearDown(self):
        tear_down_listings()

    def test_init(self):
        Listing()

    def test_listing_query_creation(self):
        self.assertEqual(ListingSearchQuery.objects.all().count(), len(self.listing.tags))

    def test_rating(self):
        listing = Listing.objects.all()[0]
        sum = 0
        for review in listing.reviews.all():
            sum += review.rating
        self.assertEqual(float(sum) / listing.reviews.all().count(), listing.rating)


@skip('')
class ListingSearchQueryTestCase(TestCase):
    def setUp(self):
        listings = create_listings(1)
        self.listing = listings[0]

    def tearDown(self):
        tear_down_listings()

    def test_init(self):
        ListingSearchQuery()


@skip('')
class ListingImageTestCase(TestCase):
    def setUp(self):
        listings = create_listings(1, num_images=5)
        self.listing = listings[0]

    def test_init(self):
        ListingImage()

    def tearDown(self):
        tear_down_listings()

    def test_thumbnail_generation(self):
        aliases = [
            '480x260',
            '0x120',
            '1024x0'
        ]
        for image in self.listing.images.all():
            for alias in aliases:
                self.assertIsNotNone(image.image[alias].url)


@skip('')
class ReviewTestCase(TestCase):
    def test_init(self):
        Review()


@skip('')
class CartTestCase(TestCase):
    def test_init(self):
        Cart()


@skip('')
class ShareMessageTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ShareMessageTestCase, cls).setUpClass()
        create_listings(1)

    def setUp(self):
        self.listing = Listing.objects.all()[0]

    def test_init(self):
        ShareMessage()

    def create_share_message(self):
        profile = Profile.objects.all()[0]
        listing = Listing.objects.all()[0]
        share_message = ShareMessage.objects.create(
            body='Hello World!',
            sender=profile,
            rental=listing,
            receiver='success+cyrilfiggis@simulator.amazonses.com'
        )
        return share_message

    def test_create_share_message(self):
        share_message = self.create_share_message()
        self.assertIsNotNone(share_message)

    # def test_create_share_message_anonymously(self):
    #     share_message = ShareMessage.objects.create(
    #         body='Hello World!',
    #         sender=None,
    #         rental=self.listing,
    #         receiver='success+cyrilfiggis@simulator.amazonses.com'
    #     )
    #     self.assertIsNotNone(share_message)

    def test_sending_share_message(self):
        share_message = self.create_share_message()

        # Make sure email gets sent
        self.assertEqual(len(mail.outbox), 1, 'Failed to send email on behalf of user')

        # Check sender
        self.assertEqual(mail.outbox[0].from_email, 'The SHOTZU Crew <hello@shotzu.com>')

        # Check for user message
        self.assertTrue(re.search(share_message.body, mail.outbox[0].body), 'Failed to find actual message in email')
