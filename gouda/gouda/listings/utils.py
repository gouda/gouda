from __future__ import absolute_import

import random
from datetime import date
from datetime import timedelta

from django.core.files.base import ContentFile
from gouda.users.models import User
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.models import CurrencyType
from gouda.listings.models import Listing
from gouda.listings.models import Cart
from gouda.listings.models import CartItem
from gouda.listings.models import Review
from gouda.listings.models import ListingSearchQuery
from gouda.listings.models import ListingImage
from gouda.listings.models import ListingConditionType
from gouda.listings.models import ListingTransaction
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from gouda.favorites.utils import tear_down_favorites
from gouda.customers.utils import create_customers
from gouda.utils import get_lorem_text, is_int, get_title
import os
from braintree.test.nonces import Nonces

FIXTURES_DIR = os.path.join(os.path.dirname(__file__), 'fixtures')
TITLES = [
    'Syma X8C 2.4Ghz 6-Axis Gyro RC Quadcopter Drone UAV RTF UFO 2MP HD Camera Black',
    'ATTOP YD-928 2.4Ghz 4CH 6-Axis GYRO 3D Mini RC Quadcopter Helicopter UFO Drone',
    'Headless Mode JJRC H8D 5.8G FPV Real-time RC Quadcopter Drone with 2MP HD Camera',
    'NEW USA Eachine H8 Mini RC Quadcopter Drone Headless Mode 2.4G 4CH 6 Axis RTF',
    'RC Mini Nano Hexacopter Quadcopter Drone MJX X901 X900 4Ch 6Axis RTF UFO White',
    'Syma X5SW Explorers2 FPV 2.4Ghz UFO RC Drone Quadcopter 2MP Wifi Camera Black',
    'SYMA X8C 2.4G 4CH 6-Axis Gyro Quadcopter RTF Drone+2.0MP HD Camera Orange 0B9A',
    'JJRC H8D 5.8G 4CH FPV Headless Mode RC Helicopter Quadcopter Drone W/HD Camera',
    'JJRC H9D 2.4G 4CH 6 Axis Gyro RC Quadcopter Drone Helicopter with 0.3MP Camera',
    'JJRC JJ 810 Mini RC Quadcopter blue 4 Channel 6 Axis Gyro Aerocraft LED Drone',
    '4 in 1 Drones 668-Q5 RC Quadcopter 4CH 2.4GHz Remote Control Drone Red Color',
    'XIRO Xplorer Professional Quadcopter RC Drone With Remote Controllor OE',
    'Zero Explorer Xplorer FPV 5.8G RC Quadcopter Drone Easy To Use',
    ' New 3DR Solo Drone'
]


def get_listing_image():
    listing_images_path = os.path.join(FIXTURES_DIR, 'listing_images/')
    files = os.listdir(listing_images_path)
    files = [f for f in files if 'DS_Store' not in f]
    image_file_path = os.path.join(listing_images_path, random.choice(files))
    return ContentFile(open(image_file_path).read(), name=os.path.basename(image_file_path))


def create_listings(num_listings, num_profiles=None, num_images=0, num_reviews=0, quantity=1, do_create_marketplace=False):
    profiles = create_profiles(num_profiles if num_profiles else num_listings)
    listings = list()
    tags = [
        'white',
        'black',
        'gray',
        'lightgray',
        'blue',
        'navy',
        'cyan',
        'green',
        'red',
        'pink',
        'magenta',
        'tan',
        'yellow',
        'brown',
        'gold',
        'orange',
    ]

    for i in range(num_listings):
        condition = random.choice([
            ListingConditionType.NEW,
            ListingConditionType.USED,
            ListingConditionType.REFURBISHED
        ])
        quantity = quantity if is_int(quantity) else quantity()
        listing = Listing.objects.create(
            profile=random.choice(profiles),
            title=get_title(5, 30),
            description=get_lorem_text(random.randrange(1, 3), 'b'),
            shipping_policy=get_lorem_text(random.randrange(1, 2), 'b'),
            refund_policy=get_lorem_text(random.randrange(1, 2), 'b'),
            tags=[random.choice(tags) for i in range(3)],
            currency=CurrencyType.USD,
            price=round(random.uniform(50, 1500)),
            quantity=quantity,
            condition=condition
        )
        listings.append(listing)

    if num_images:
        for listing in listings:
            for i in range(num_images if is_int(num_images) else num_images()):
                image = get_listing_image()
                is_primary = i == 0
                caption = get_lorem_text(random.randrange(0, 25), 'w')
                image = ListingImage.objects.create(image=image, is_primary=is_primary, listing=listing, caption=caption)
                assert(image is not None)

    if num_reviews:
        for listing in listings:
            for i in range(num_reviews if is_int(num_reviews) else num_reviews()):
                review = Review.objects.create(
                    listing=listing,
                    profile=random.choice(profiles),
                    title=get_title(5, 30),
                    content=get_lorem_text(random.randrange(1, 3), 'b'),
                    rating=max(1, min(5, int(random.gauss(4, 2))))
                )

    if do_create_marketplace:
        create_customers()

    return listings


def tear_down_listings():
    Listing.objects.all().delete()
    ListingSearchQuery.objects.all().delete()
    ListingImage.objects.all().delete()
    Cart.objects.all().delete()
    CartItem.objects.all().delete()
    ListingTransaction.objects.all().delete()
    tear_down_profiles()
    tear_down_favorites()
