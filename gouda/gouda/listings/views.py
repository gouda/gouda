from __future__ import absolute_import

import logging

from gouda.listings.models import Listing
from gouda.listings.models import ListingStatusType
from gouda.listings.forms import ListingSearchForm
from gouda.listings.forms import ListingSearchResultsForm
from gouda.listings.forms import AddToCartForm
from gouda.listings.forms import CartItemFormSet
from django.views.generic.edit import FormMixin
from django.views.generic.edit import FormView
from django.views.generic.edit import CreateView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.base import TemplateView
from django.core.urlresolvers import reverse_lazy
from gouda.views import LoginRequiredMixin
from gouda.views import NeverCacheMixin
from gouda.views import InlineModelFormSetUpdateView
from gouda.profiles.models import ProfileShippingAddress
from gouda.favorites.models import Favorite
from django.core.exceptions import PermissionDenied
from gouda.addresses.forms import AddressForm
from gouda.customers.forms import BraintreeCustomerForm
from gouda.listings.models import Cart
from django.contrib.sessions.models import Session
from django.http import HttpResponseRedirect

logger = logging.getLogger(__name__)


# Create your views here.
class ListingSearchFormView(NeverCacheMixin, FormView):
    template_name = 'listings/search.html'
    success_url = reverse_lazy('listings:search_results')
    form_class = ListingSearchForm

    def get_context_data(self, **kwargs):
        context_data = super(ListingSearchFormView, self).get_context_data(**kwargs)

        # Get latest rentals
        context_data['latest_listings'] = Listing.objects.filter(status=ListingStatusType.ACTIVE).order_by('-date_created')[:12]

        return context_data


class ListingSearchResultsView(NeverCacheMixin, FormView):
    template_name = 'listings/search_results.html'
    success_url = reverse_lazy('listings:search_results')
    form_class = ListingSearchResultsForm


class ShippingAddressCreateView(NeverCacheMixin, LoginRequiredMixin, CreateView):
    template_name = 'listings/checkout/shipping_address_create.html'
    form_class = AddressForm
    success_url = reverse_lazy('listings:customer_create')

    def form_valid(self, form):
        address = form.save(commit=False)
        profile = self.request.user.profile
        ProfileShippingAddress.objects.filter(profile=self.request.user.profile).update(is_primary=False)

        queryset = profile.shipping_addresses.filter(
            full_name=address.full_name,
            street_address=address.street_address,
            sub_premise=address.sub_premise,
            postal_code=address.postal_code,
            locality=address.locality,
            region=address.region,
            country=address.country
        )
        if not queryset.exists():
            address.save()
            ProfileShippingAddress.objects.create(
                profile=self.request.user.profile,
                address=address,
                is_primary=True
            )
            self.object = address
        else:
            self.object = queryset[0]

        return HttpResponseRedirect(self.get_success_url())


class BraintreeCustomerCreateView(NeverCacheMixin, LoginRequiredMixin, FormView):
    template_name = 'listings/checkout/customer_create.html'
    form_class = BraintreeCustomerForm
    success_url = reverse_lazy('listings:review_order')

    def get_context_data(self, **kwargs):
        context_data = super(BraintreeCustomerCreateView, self).get_context_data(**kwargs)
        profile = self.request.user.profile
        context_data['shipping_address'] = profile.latest_shipping_address
        return context_data

    def form_valid(self, form):
        response = super(BraintreeCustomerCreateView, self).form_valid(form)
        form.save()
        return response

    def get_initial(self):
        initial = super(BraintreeCustomerCreateView, self).get_initial()
        initial['user'] = self.request.user.pk
        return initial


class ReviewOrderView(NeverCacheMixin, LoginRequiredMixin, TemplateView):
    template_name = 'listings/checkout/review_order.html'


class CartUpdateView(NeverCacheMixin, InlineModelFormSetUpdateView):
    template_name = 'listings/cart_update.html'
    success_url = reverse_lazy('listings:cart_update')
    formset_class = CartItemFormSet
    context_object_name = 'cart'

    def get_object(self, queryset=None):
        session_key = self.request.session.session_key
        session = Session.objects.get(session_key=session_key)
        cart, is_created = Cart.objects.get_or_create(session=session)
        return cart


class ListingDetailView(NeverCacheMixin, SingleObjectMixin, FormView):
    model = Listing
    context_object_name = 'listing'
    form_class = AddToCartForm
    success_url = reverse_lazy('listings:cart_update')
    template_name = 'listings/listing_detail.html'

    def get_initial(self):
        initial = super(ListingDetailView, self).get_initial()
        initial['quantity'] = 1
        initial['listing'] = self.object.id
        if not self.request.session.session_key:
            self.request.session.save()
        initial['session'] = self.request.session.session_key
        return initial

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()
        response = super(ListingDetailView, self).get(request, *args, **kwargs)
        if self.object.is_active:
            return response
        elif not request.user.is_anonymous() and (request.user.is_staff or self.object.profile == request.user.profile):
            return response
        raise PermissionDenied()

    def post(self, request, *args, **kwargs):
        self.object = self.get_object()
        return super(ListingDetailView, self).post(request, args, kwargs)

    def form_valid(self, form):
        form.save()
        return super(ListingDetailView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context_data = super(ListingDetailView, self).get_context_data(**kwargs)

        # # Request form
        # context_data['form'] = self.get_form()

        # Is favorite
        try:
            favorite = Favorite.objects.get_for_profile(self.request.user.profile, app_label='listings', model_name='Listing').filter(object_id=self.object.id)[0]
            context_data['favorite'] = favorite
        except:
            context_data['favorite'] = None
        return context_data
