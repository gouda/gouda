{% autoescape off %}{{ share_message.sender.full_name }} has shared a listing with you! See below:

{{ share_message.listing.absolute_url }}

Message from {{ share_message.sender.full_name }}:

{{ share_message.body }}


{{ email_closing }}
{% endautoescape %}