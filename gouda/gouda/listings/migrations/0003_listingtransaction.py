# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20151003_2123'),
        ('listings', '0002_auto_20151003_2123'),
    ]

    operations = [
        migrations.CreateModel(
            name='ListingTransaction',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('braintree_id', models.CharField(unique=True, max_length=100)),
                ('amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('service_fee_amount', models.DecimalField(verbose_name=b'Amount withheld', max_digits=10, decimal_places=2)),
                ('escrow_status', models.IntegerField(default=0, null=True, blank=True)),
                ('status', models.IntegerField(default=0)),
                ('buyer', models.ForeignKey(related_name='listing_transaction_buyer', to='profiles.Profile')),
                ('seller', models.ForeignKey(related_name='listing_transaction_seller', to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
