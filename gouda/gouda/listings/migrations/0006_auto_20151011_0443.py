# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0005_listingtransaction_notes'),
    ]

    operations = [
        migrations.CreateModel(
            name='ListingTransactionItem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('quantity', models.IntegerField()),
                ('listing', models.ForeignKey(to='listings.Listing')),
                ('listing_transaction', models.ForeignKey(to='listings.ListingTransaction')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='listingtransaction',
            name='listings',
            field=models.ManyToManyField(to='listings.Listing', through='listings.ListingTransactionItem'),
        ),
    ]
