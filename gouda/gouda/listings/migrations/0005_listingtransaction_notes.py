# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0004_listingtransaction_quantity'),
    ]

    operations = [
        migrations.AddField(
            model_name='listingtransaction',
            name='notes',
            field=models.TextField(default=''),
            preserve_default=False,
        ),
    ]
