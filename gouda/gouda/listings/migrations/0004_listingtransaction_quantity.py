# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('listings', '0003_listingtransaction'),
    ]

    operations = [
        migrations.AddField(
            model_name='listingtransaction',
            name='quantity',
            field=models.IntegerField(default=1),
            preserve_default=False,
        ),
    ]
