# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('sessions', '0001_initial'),
        ('listings', '0001_initial'),
        ('profiles', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='sharemessage',
            name='sender',
            field=models.ForeignKey(related_name='share_messages', to='profiles.Profile', null=True),
        ),
        migrations.AddField(
            model_name='review',
            name='listing',
            field=models.ForeignKey(related_name='reviews', to='listings.Listing'),
        ),
        migrations.AddField(
            model_name='review',
            name='profile',
            field=models.ForeignKey(related_name='listing_reviews', to='profiles.Profile'),
        ),
        migrations.AddField(
            model_name='listingimage',
            name='listing',
            field=models.ForeignKey(related_name='images', to='listings.Listing'),
        ),
        migrations.AddField(
            model_name='listing',
            name='profile',
            field=models.ForeignKey(to='profiles.Profile'),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='cart',
            field=models.ForeignKey(to='listings.Cart'),
        ),
        migrations.AddField(
            model_name='cartitem',
            name='listing',
            field=models.ForeignKey(to='listings.Listing'),
        ),
        migrations.AddField(
            model_name='cart',
            name='items',
            field=models.ManyToManyField(to='listings.Listing', through='listings.CartItem'),
        ),
        migrations.AddField(
            model_name='cart',
            name='session',
            field=models.ForeignKey(to='sessions.Session'),
        ),
    ]
