from __future__ import absolute_import

from django import forms
from django.forms.models import inlineformset_factory
from gouda.listings.models import ListingConditionType
from gouda.profiles.models import Profile
from gouda.listings.models import Cart
from gouda.listings.models import CartItem
from gouda.listings.models import Listing
from gouda.listings.models import ListingTransaction
from gouda.listings.models import ListingTransactionItem
from gouda.forms.widgets import KendoTextInput
from gouda.forms.widgets import KendoTextarea
from gouda.forms.widgets import KendoDropDownList
from django.contrib.sessions.models import Session


CONDITION_CHOICES = ListingConditionType.choices()
max_enum_value = 0
for choice in CONDITION_CHOICES:
    max_enum_value = max(max_enum_value, choice[0])
CONDITION_CHOICES.append((max_enum_value + 1, 'All'))
CONDITION_CHOICES.sort(key=lambda choice: choice[1], reverse=True)


class ListingTransactionForm(forms.Form):
    cart_items = forms.ModelMultipleChoiceField(queryset=CartItem.objects.all(), widget=forms.HiddenInput())
    buyer = forms.ModelChoiceField(queryset=Profile.objects.all(), widget=forms.HiddenInput())
    seller = forms.ModelChoiceField(queryset=Profile.objects.all(), widget=forms.HiddenInput())
    notes = forms.CharField(widget=KendoTextarea('notes'))

    def clean(self):
        cleaned_data = super(ListingTransactionForm, self).clean()
        cart_items = cleaned_data.get('cart_items')
        for cart_item in cart_items:
            if cart_item.quantity > cart_item.listing.quantity:
                raise forms.ValidationError('Invalid quantity')
        return cleaned_data

    def save(self):
        cart_items = self.cleaned_data.get('cart_items')
        listing_transaction_items = list()
        for cart_item in cart_items:
            listing_transaction_item = ListingTransactionItem(listing=cart_item.listing, quantity=cart_item.quantity)
            listing_transaction_items.append(listing_transaction_item)

        listing_transaction = ListingTransaction.objects.create_listing_transaction(
            listing_transaction_items=listing_transaction_items,
            buyer=self.cleaned_data.get('buyer'),
            seller=self.cleaned_data.get('seller'),
            notes=self.cleaned_data.get('notes')
        )
        return listing_transaction


class ListingSearchForm(forms.Form):
    query = forms.CharField(widget=KendoTextInput('query', attrs={
        'kendo-auto-complete': 'queryAutoComplete',
        'k-data-source': 'queries',
    }))


class ListingSearchResultsForm(forms.Form):
    query = forms.CharField(widget=KendoTextInput('query', attrs={
        'kendo-auto-complete': 'queryAutoComplete',
        'k-data-source': 'queries',
        'k-ng-disabled': '!areControlsEnabled()',
    }))
    condition = forms.IntegerField(widget=KendoDropDownList('condition', choices=CONDITION_CHOICES, attrs={
        'k-ng-disabled': '!areControlsEnabled()',
        # 'k-options': 'conditionDropDownListOptions'
    }))


def validate_quantity(listing, quantity, session):
    # Make sure quantity requested is not ridiculous
    if quantity > listing.quantity:
        return False

    # Make sure quantity lines up with inventory
    try:
        cart = Cart.objects.get(session=session)
    except Cart.DoesNotExist:
        cart = None
    if cart and not cart.has_inventory(listing, quantity):
        if listing.quantity > 1:
            return False
    return True


class AddToCartForm(forms.Form):
    quantity = forms.IntegerField(widget=KendoTextInput('quantity', attrs={'ng-min': 1, 'type': 'number'}))
    session = forms.ModelChoiceField(queryset=Session.objects.all(), widget=forms.HiddenInput())
    listing = forms.ModelChoiceField(queryset=Listing.objects.all(), widget=forms.HiddenInput())

    def clean(self):
        cleaned_data = super(AddToCartForm, self).clean()
        listing = cleaned_data.get('listing')
        quantity = cleaned_data.get('quantity')
        session = cleaned_data.get('session')

        # Make sure quantity requested is not ridiculous
        if not validate_quantity(listing, quantity, session):
            raise forms.ValidationError('Invalid quantity')

        return cleaned_data

    def save(self):
        session = self.cleaned_data.get('session')
        listing = self.cleaned_data.get('listing')
        quantity = self.cleaned_data.get('quantity')

        # Create a cart (or not)
        cart, is_created = Cart.objects.get_or_create(session=session)

        # Create a cart item
        cart_item, is_created = CartItem.objects.get_or_create(
            cart=cart,
            listing=listing
        )
        cart_item.quantity = max(1, min(listing.quantity, cart_item.quantity + quantity))

        cart_item.save()

        return cart


class CartItemForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super(CartItemForm, self).clean()
        listing = cleaned_data.get('listing')
        quantity = cleaned_data.get('quantity')
        cart = cleaned_data.get('cart')

        # Make sure quantity requested is not ridiculous
        if not validate_quantity(listing, quantity, cart.session):
            raise forms.ValidationError('Invalid quantity')

        return cleaned_data

    def save(self, commit=True):
        cart_item = super(CartItemForm, self).save(commit)
        if cart_item.quantity <= 0:
            cart_item.delete()
        return cart_item

    class Meta:
        model = CartItem
        fields = ['quantity', 'listing', 'cart']
        widgets = {
            'quantity': forms.TextInput(attrs={'class': 'k-textbox', 'ng-min': 0, 'type': 'number'}),
            'cart': forms.HiddenInput(),
            'listing': forms.HiddenInput()
        }


CartItemFormSet = inlineformset_factory(Cart, CartItem, form=CartItemForm, extra=0)
