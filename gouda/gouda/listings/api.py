from __future__ import absolute_import

from drf_haystack.viewsets import HaystackViewSet
from gouda.listings.serializers import ListingSerializer
from gouda.listings.serializers import ReviewListSerializer
from gouda.api import HaystackFilter
from rest_framework import viewsets
from rest_framework import mixins

import django_filters
from gouda.listings.models import Listing
from gouda.listings.models import Review


class ReviewFilterSet(django_filters.FilterSet):
    listing = django_filters.ModelChoiceFilter(name='listing', queryset=Listing.objects.all())

    class Meta:
        model = Review
        fields = ['listing']


class ListingFilter(HaystackFilter):
    filter_aliases = {
        'minPrice': 'price__gte',
        'maxPrice': 'price__lte'
    }


class ListingViewSet(HaystackViewSet):
    index_models = [Listing]
    serializer_class = ListingSerializer
    filter_backends = [ListingFilter]


class ReviewViewSet(mixins.ListModelMixin, viewsets.GenericViewSet):
    queryset = Review.objects.all()
    serializer_class = ReviewListSerializer
    filter_class = ReviewFilterSet
    ordering_fields = ('date_created',)

