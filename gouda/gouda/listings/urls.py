from django.conf.urls import url
from gouda.listings.views import ListingSearchFormView
from gouda.listings.views import ListingSearchResultsView
from gouda.listings.views import ListingDetailView
from gouda.listings.views import CartUpdateView
from gouda.listings.views import ShippingAddressCreateView
from gouda.listings.views import BraintreeCustomerCreateView
from gouda.listings.views import ReviewOrderView

urlpatterns = [
    url(r'^search/$', ListingSearchFormView.as_view(), name='search'),
    url(r'^search/results/$', ListingSearchResultsView.as_view(), name='search_results'),
    url(r'^(?P<pk>\d+)/$', ListingDetailView.as_view(), name='detail'),
    url(r'^cart/$', CartUpdateView.as_view(), name='cart_update'),
    url(r'^checkout/shipping-address/$', ShippingAddressCreateView.as_view(), name='shipping_address_create'),
    url(r'^checkout/payment/$', BraintreeCustomerCreateView.as_view(), name='customer_create'),
    url(r'^checkout/review/$', ReviewOrderView.as_view(), name='review_order'),
]
