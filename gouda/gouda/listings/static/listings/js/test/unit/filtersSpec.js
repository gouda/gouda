'use strict';

describe('Rental Filters', function(){
    beforeEach(angular.mock.module('gouda.listings.filters'));

    describe('firstName', function(){
        it('should convert full name to first name', inject(function(firstNameFilter){
            expect(firstNameFilter('Sterling Archer')).toBe('Sterling');
        }));
        it('should not freak out if there is only one name', inject(function(firstNameFilter){
            expect(firstNameFilter('Sterling')).toBe('Sterling');
        }));
    });

    describe('unabbreviate', function(){
        it('should convert abbreviated state to full state name', inject(function(unabbreviateFilter){
            expect(unabbreviateFilter('CO')).toBe('Colorado');
            expect(unabbreviateFilter('WA')).toBe('Washington');
        }));
    });

    describe('thumbnail', function(){
        it('should convert an image url to its thumbnail version', inject(function(thumbnailFilter){
            var imageUrl = '/media/rental_images/Interior-Design-Ideas-For-Bedroom-30_rszJuJi.jpg.480x260_q85_crop.png';
            expect(thumbnailFilter(imageUrl, '480x260')).toBe(imageUrl + '.480x260_q85_crop.png');
            imageUrl = '/media/rental_images/Interior-Design-Ideas-For-Bedroom-30_rszJuJi.jpg.480x260_q85_crop.jpg';
            expect(thumbnailFilter(imageUrl, '480x260')).toBe(imageUrl + '.480x260_q85_crop.jpg');
        }));
    });

    describe('paragraphs', function(){
        it('should turn blocks of text to paragraph blocks', inject(function(paragraphsFilter){
            var text = '1\n\n2\n\n3';
            expect(paragraphsFilter(text)).toBe('<p>1</p><p>2</p><p>3</p>');
        }));
        it('should not freak out if there are no newlines', inject(function(paragraphsFilter){
            var text = '1';
            expect(paragraphsFilter(text)).toBe('<p>1</p>');
        }));
        it('should not freak out if there are newlines at the end', inject(function(paragraphsFilter){
            var text = '1\n\n\n\n';
            expect(paragraphsFilter(text)).toBe('<p>1</p>');
        }));
        it('should not freak out if there are newlines in the middle', inject(function(paragraphsFilter){
            var text = '1\n\n\n\n2\n\n';
            expect(paragraphsFilter(text)).toBe('<p>1</p><p>2</p>');
        }));
    });
});