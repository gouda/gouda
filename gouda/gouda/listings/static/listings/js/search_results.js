$( document ).ready(function() {
    // Check out date
    var check_out_date_input = $('#id_check_out_date').pickadate({
        format: 'm/d/yyyy',
        container: '#input-container--check-out-date',
        min: Date.now(),
        onSet: function(context) {
            //$('#id_check_out_date').parsley().validate();
        }
    });

    // Check in date
    var check_in_date_input = $('#id_check_in_date').pickadate({
        format: 'm/d/yyyy',
        container: '#input-container--check-in-date',
        min: Date.now(),
        onSet: function(context) {
            var check_in_date = this.get('select');
            console.log('Just set stuff:', check_in_date);
            var check_out_date = check_out_date_input.pickadate('picker').get('select');
            if (check_out_date) {
                if (check_in_date['pick'] > check_out_date['pick']) {
                    check_out_date_input.pickadate('picker').clear();
                    check_out_date_input.pickadate('picker').set('min', check_in_date);
                }
            } else {
                check_out_date_input.pickadate('picker').set('min', check_in_date);
            }
            //$('#id_check_in_date').parsley().validate();
        }
    });
});