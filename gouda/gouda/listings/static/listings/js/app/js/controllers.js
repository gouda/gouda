'use strict';

var US_REGIONS = [
    {label: 'Alabama', value: 'Alabama'},
    {label: 'Alaska', value: 'Alaska'},
    {label: 'American Samoa', value: 'American Samoa'},
    {label: 'Arizona', value: 'Arizona'},
    {label: 'Arkansas', value: 'Arkansas'},
    {label: 'California', value: 'California'},
    {label: 'Colorado', value: 'Colorado'},
    {label: 'Connecticut', value: 'Connecticut'},
    {label: 'Delaware', value: 'Delaware'},
    {label: 'District of Columbia', value: 'District of Columbia'},
    {label: 'Florida', value: 'Florida'},
    {label: 'Georgia', value: 'Georgia'},
    {label: 'Guam', value: 'Guam'},
    {label: 'Hawaii', value: 'Hawaii'},
    {label: 'Idaho', value: 'Idaho'},
    {label: 'Illinois', value: 'Illinois'},
    {label: 'Indiana', value: 'Indiana'},
    {label: 'Iowa', value: 'Iowa'},
    {label: 'Kansas', value: 'Kansas'},
    {label: 'Kentucky', value: 'Kentucky'},
    {label: 'Louisiana', value: 'Louisiana'},
    {label: 'Maine', value: 'Maine'},
    {label: 'Maryland', value: 'Maryland'},
    {label: 'Massachusetts', value: 'Massachusetts'},
    {label: 'Michigan', value: 'Michigan'},
    {label: 'Minnesota', value: 'Minnesota'},
    {label: 'Mississippi', value: 'Mississippi'},
    {label: 'Missouri', value: 'Missouri'},
    {label: 'Montana', value: 'Montana'},
    {label: 'Nebraska', value: 'Nebraska'},
    {label: 'Nevada', value: 'Nevada'},
    {label: 'New Hampshire', value: 'New Hampshire'},
    {label: 'New Jersey', value: 'New Jersey'},
    {label: 'New Mexico', value: 'New Mexico'},
    {label: 'New York', value: 'New York'},
    {label: 'North Carolina', value: 'North Carolina'},
    {label: 'North Dakota', value: 'North Dakota'},
    {label: 'Northern Mariana Islands', value: 'Northern Mariana Islands'},
    {label: 'Ohio', value: 'Ohio'},
    {label: 'Oklahoma', value: 'Oklahoma'},
    {label: 'Oregon', value: 'Oregon'},
    {label: 'Pennsylvania', value: 'Pennsylvania'},
    {label: 'Puerto Rico', value: 'Puerto Rico'},
    {label: 'Rhode Island', value: 'Rhode Island'},
    {label: 'South Carolina', value: 'South Carolina'},
    {label: 'South Dakota', value: 'South Dakota'},
    {label: 'Tennessee', value: 'Tennessee'},
    {label: 'Texas', value: 'Texas'},
    {label: 'U.S. Virgin Islands', value: 'U.S. Virgin Islands'},
    {label: 'Utah', value: 'Utah'},
    {label: 'Vermont', value: 'Vermont'},
    {label: 'Virginia', value: 'Virginia'},
    {label: 'Washington', value: 'Washington'},
    {label: 'West Virginia', value: 'West Virginia'},
    {label: 'Wisconsin', value: 'Wisconsin'},
    {label: 'Wyoming', value: 'Wyoming'}
];

/* Controllers */
var LOCATION_DROP_DOWN_LIST_OPTIONS = {
    dataSource: [
        {
            label: 'Colorado',
            value: 'CO'
        },
        {
            label: 'Jamaica',
            value: 'JM'
        },
        {
            label: 'Washington D.C.',
            value: 'DC'
        },
        {
            label: 'All Locations',
            value: 'ALL'
        }
    ],
    dataTextField: 'label',
    dataValueField: 'value',
    valueTemplate: '<span class="fa fa-map-marker fa-1x kendo-input-addon"></span><span>{{dataItem.label}}</span>'
};

var CONDITION_DROP_DOWN_LIST_OPTIONS = {
    dataTextField: 'label',
    dataValueField: 'value',
    valueTemplate: '<span class="fa fa-map-marker fa-1x kendo-input-addon"></span><span>{{dataItem.label}}</span>'
};

var MIN_GUESTS_DROP_DOWN_LIST_OPTIONS = {
    dataSource: [
        {
            label: '1 Guest',
            value: '1'
        },
        {
            label: '2 Guests',
            value: '2'
        },
        {
            label: '3 Guests',
            value: '3'
        },
        {
            label: '4 Guests',
            value: '4'
        },
        {
            label: '5 Guests',
            value: '5'
        },
        {
            label: '6 Guests',
            value: '6'
        },
        {
            label: '7 Guests',
            value: '7'
        },
        {
            label: '8 Guests',
            value: '8'
        },
        {
            label: '9 Guests',
            value: '9'
        },
        {
            label: '10+ Guests',
            value: '10'
        }
    ],
    dataTextField: 'label',
    dataValueField: 'value',
    valueTemplate: '<span class="fa fa-users fa-1x kendo-input-addon"></span><span>{{dataItem.label}}</span>'
};


var listingControllers = angular.module('gouda.listings.controllers', []);


listingControllers.factory('listingSearchResultsService', function() {
    return {
        listings: [],
        lastParams: undefined
    }
});

listingControllers.factory('listingDetailService', function() {
    return {
        checkInDate: moment().format('L'),
        checkOutDate: moment().add(1, 'days').format('L'),
        numGuests: 1
    }
});


listingControllers.factory('bookingCreateFormService', function() {
    return {
        submitStripeTokenForm: undefined,
        isStripeFormValid: undefined,
        processStripeToken: undefined,
    }
});



listingControllers.factory('listingCreateFormService', function() {
    return {
        listing: {
            numBedrooms: '1',
            numBeds: '1',
            numBathrooms: '1',
            buildingType: '1',
            roomType: '0',
            maxGuests: '1',

            title: undefined,
            summary: undefined,

            space: undefined,
            guestAccess: undefined,
            interactionWithGuests: undefined,
            otherThingsToNote: undefined,
            houseRules: undefined,
            neighborhoodOverview: undefined,
            neighborhoodTravel: undefined,

            amenities: [],

            currency: '35',
            nightlyPrice: undefined,
            weeklyPrice: undefined,
            monthlyPrice: undefined,
            securityDeposit: undefined,
            cleaningFee: undefined,
            extraPeopleFee: undefined,
            minimumStay: undefined,

            rawAddress: undefined,
            formattedAddress: '',
            streetAddress: undefined,
            subPremise: undefined,
            placeID: undefined,
            postalCode: undefined,
            location: {
                latitude: 0,
                longitude: 0
            },
            locality: undefined,
            region: undefined,
            regionCode: undefined,
            country: 'US'
        },
        rawStreetAddress: null,
        rawSubPremise: null,
        rawLocality: null,
        rawRegion: null,
        rawPostalCode: null,
        files: [],
        didSubmit: false
    }
});

function bindToListingCreateFormService(scope, listingCreateFormService) {
    // These are my fields
    var fields = [];
    for (var propertyName in listingCreateFormService.listing) {
        if (propertyName == 'files') {
            continue;
        }
        if (listingCreateFormService.listing.hasOwnProperty(propertyName)) {
            fields.push(propertyName);
            scope[propertyName] = listingCreateFormService.listing[propertyName];
        }
    }

    var rawAddressFields = [
        'rawStreetAddress',
        'rawSubPremise',
        'rawLocality',
        'rawRegion',
        'rawPostalCode'
    ];
    angular.forEach(rawAddressFields, function(propertyName, key){
        if (listingCreateFormService.hasOwnProperty(propertyName)) {
            scope[propertyName] = listingCreateFormService[propertyName];
        }
    });

    // Watch the birdy...
    //scope.$watch(function(){ return listingCreateFormService}, function(newVal){
    //    angular.forEach(fields, function(field, key){
    //        scope[field] = newVal[field];
    //    });
    //}, true);
    angular.forEach(fields, function(field, key){
        if (Object.prototype.toString.call(scope[field]) == '[object Array]') {
            scope.$watchCollection(field, function(newVal){
                listingCreateFormService.listing[field] = newVal;
            });
        } else {
            scope.$watch(field, function(newVal) {
                if (newVal === '') {
                    listingCreateFormService.listing[field] = undefined;
                } else {
                    listingCreateFormService.listing[field] = newVal;
                }
            });
        }
    }, true);

    angular.forEach(rawAddressFields, function(field, key){
       scope.$watch(field, function(newVal) {
           if (newVal === '') {
               listingCreateFormService[field] = undefined;
           } else {
               listingCreateFormService[field] = newVal;
           }
       });
    });
}

listingControllers.controller('ListingLocationFormController', ['$scope', '$log', 'uiGmapGoogleMapApi', 'listingCreateFormService',
    function($scope, $log, uiGmapGoogleMapApi, listingCreateFormService) {
        bindToListingCreateFormService($scope, listingCreateFormService);
        console.log(listingCreateFormService);

        // Make it dirty =D
        $scope.$watch('form.$error', function(newVal){
            if (listingCreateFormService.didSubmit) {
                angular.forEach($scope.form.$error.required, function(field) {
                    field.$setDirty();
                });
            }
        }, true);

        // CLICK (Verify)
        $scope.onVerifyButtonClick = function() {
            $scope.updateRaw();
            $scope.setDoShowMap(true);
            $scope.geocode($scope.rawAddress);
        };

        // CLICK (edit)
        $scope.onEditAddressButtonClick = function() {
            $scope.setDoShowMap(false);
        };

        // Update raw
        $scope.updateRaw = function() {
            var raw = '';
            var optionalProperties = ['rawStreetAddress', 'rawSubPremise', 'rawLocality', 'rawRegion', 'rawPostalCode'];

            angular.forEach(optionalProperties, function(property, key){
                if ($scope[property] && $scope[property].trim()) {
                    raw += $scope[property].trim() + ',';
                }
            });
            raw += $scope.country;
            $scope.rawAddress = raw;
        };

        // Country type
        var countryDataSource = [
            {
                "label": "Afghanistan",
                "value": "AF"
            },
            {
                "label": "Albania",
                "value": "AL"
            },
            {
                "label": "Algeria",
                "value": "DZ"
            },
            {
                "label": "American Samoa",
                "value": "AS"
            },
            {
                "label": "Andorra",
                "value": "AD"
            },
            {
                "label": "Angola",
                "value": "AO"
            },
            {
                "label": "Anguilla",
                "value": "AI"
            },
            {
                "label": "Antarctica",
                "value": "AQ"
            },
            {
                "label": "Antigua and Barbuda",
                "value": "AG"
            },
            {
                "label": "Argentina",
                "value": "AR"
            },
            {
                "label": "Armenia",
                "value": "AM"
            },
            {
                "label": "Aruba",
                "value": "AW"
            },
            {
                "label": "Australia",
                "value": "AU"
            },
            {
                "label": "Austria",
                "value": "AT"
            },
            {
                "label": "Azerbaijan",
                "value": "AZ"
            },
            {
                "label": "Bahamas",
                "value": "BS"
            },
            {
                "label": "Bahrain",
                "value": "BH"
            },
            {
                "label": "Bangladesh",
                "value": "BD"
            },
            {
                "label": "Barbados",
                "value": "BB"
            },
            {
                "label": "Belarus",
                "value": "BY"
            },
            {
                "label": "Belgium",
                "value": "BE"
            },
            {
                "label": "Belize",
                "value": "BZ"
            },
            {
                "label": "Benin",
                "value": "BJ"
            },
            {
                "label": "Bermuda",
                "value": "BM"
            },
            {
                "label": "Bhutan",
                "value": "BT"
            },
            {
                "label": "Bolivia",
                "value": "BO"
            },
            {
                "label": "Bonaire",
                "value": "BQ"
            },
            {
                "label": "Bosnia and Herzegovina",
                "value": "BA"
            },
            {
                "label": "Botswana",
                "value": "BW"
            },
            {
                "label": "Bouvet Island",
                "value": "BV"
            },
            {
                "label": "Brazil",
                "value": "BR"
            },
            {
                "label": "British Indian Ocean Territory",
                "value": "IO"
            },
            {
                "label": "Brunei Darussalam",
                "value": "BN"
            },
            {
                "label": "Bulgaria",
                "value": "BG"
            },
            {
                "label": "Burkina Faso",
                "value": "BF"
            },
            {
                "label": "Burundi",
                "value": "BI"
            },
            {
                "label": "Cabo Verde",
                "value": "CV"
            },
            {
                "label": "Cambodia",
                "value": "KH"
            },
            {
                "label": "Cameroon",
                "value": "CM"
            },
            {
                "label": "Canada",
                "value": "CA"
            },
            {
                "label": "Cayman Islands",
                "value": "KY"
            },
            {
                "label": "Central African Republic",
                "value": "CF"
            },
            {
                "label": "Chad",
                "value": "TD"
            },
            {
                "label": "Chile",
                "value": "CL"
            },
            {
                "label": "China",
                "value": "CN"
            },
            {
                "label": "Christmas Island",
                "value": "CX"
            },
            {
                "label": "Cocos (Keeling) Islands",
                "value": "CC"
            },
            {
                "label": "Colombia",
                "value": "CO"
            },
            {
                "label": "Comoros",
                "value": "KM"
            },
            {
                "label": "Congo",
                "value": "CG"
            },
            //{
            //    "label": "Congo (the Democratic Republic of the)",
            //    "value": "CD"
            //},
            {
                "label": "Cook Islands",
                "value": "CK"
            },
            {
                "label": "Costa Rica",
                "value": "CR"
            },
            {
                "label": "Croatia",
                "value": "HR"
            },
            {
                "label": "Cuba",
                "value": "CU"
            },
            {
                "label": "Cura\u00e7ao",
                "value": "CW"
            },
            {
                "label": "Cyprus",
                "value": "CY"
            },
            {
                "label": "Czech Republic",
                "value": "CZ"
            },
            {
                "label": "C\u00f4te d'Ivoire",
                "value": "CI"
            },
            {
                "label": "Denmark",
                "value": "DK"
            },
            {
                "label": "Djibouti",
                "value": "DJ"
            },
            {
                "label": "Dominica",
                "value": "DM"
            },
            {
                "label": "Dominican Republic",
                "value": "DO"
            },
            {
                "label": "Ecuador",
                "value": "EC"
            },
            {
                "label": "Egypt",
                "value": "EG"
            },
            {
                "label": "El Salvador",
                "value": "SV"
            },
            {
                "label": "Equatorial Guinea",
                "value": "GQ"
            },
            {
                "label": "Eritrea",
                "value": "ER"
            },
            {
                "label": "Estonia",
                "value": "EE"
            },
            {
                "label": "Ethiopia",
                "value": "ET"
            },
            {
                "label": "Falkland Islands",
                "value": "FK"
            },
            {
                "label": "Faroe Islands",
                "value": "FO"
            },
            {
                "label": "Fiji",
                "value": "FJ"
            },
            {
                "label": "Finland",
                "value": "FI"
            },
            {
                "label": "France",
                "value": "FR"
            },
            {
                "label": "French Guiana",
                "value": "GF"
            },
            {
                "label": "French Polynesia",
                "value": "PF"
            },
            {
                "label": "French Southern Territories",
                "value": "TF"
            },
            {
                "label": "Gabon",
                "value": "GA"
            },
            {
                "label": "Gambia",
                "value": "GM"
            },
            {
                "label": "Georgia",
                "value": "GE"
            },
            {
                "label": "Germany",
                "value": "DE"
            },
            {
                "label": "Ghana",
                "value": "GH"
            },
            {
                "label": "Gibraltar",
                "value": "GI"
            },
            {
                "label": "Greece",
                "value": "GR"
            },
            {
                "label": "Greenland",
                "value": "GL"
            },
            {
                "label": "Grenada",
                "value": "GD"
            },
            {
                "label": "Guadeloupe",
                "value": "GP"
            },
            {
                "label": "Guam",
                "value": "GU"
            },
            {
                "label": "Guatemala",
                "value": "GT"
            },
            {
                "label": "Guernsey",
                "value": "GG"
            },
            {
                "label": "Guinea",
                "value": "GN"
            },
            {
                "label": "Guinea-Bissau",
                "value": "GW"
            },
            {
                "label": "Guyana",
                "value": "GY"
            },
            {
                "label": "Haiti",
                "value": "HT"
            },
            {
                "label": "Heard Island and McDonald Islands",
                "value": "HM"
            },
            {
                "label": "Holy See",
                "value": "VA"
            },
            {
                "label": "Honduras",
                "value": "HN"
            },
            {
                "label": "Hong Kong",
                "value": "HK"
            },
            {
                "label": "Hungary",
                "value": "HU"
            },
            {
                "label": "Iceland",
                "value": "IS"
            },
            {
                "label": "India",
                "value": "IN"
            },
            {
                "label": "Indonesia",
                "value": "ID"
            },
            {
                "label": "Iran",
                "value": "IR"
            },
            {
                "label": "Iraq",
                "value": "IQ"
            },
            {
                "label": "Ireland",
                "value": "IE"
            },
            {
                "label": "Isle of Man",
                "value": "IM"
            },
            {
                "label": "Israel",
                "value": "IL"
            },
            {
                "label": "Italy",
                "value": "IT"
            },
            {
                "label": "Jamaica",
                "value": "JM"
            },
            {
                "label": "Japan",
                "value": "JP"
            },
            {
                "label": "Jersey",
                "value": "JE"
            },
            {
                "label": "Jordan",
                "value": "JO"
            },
            {
                "label": "Kazakhstan",
                "value": "KZ"
            },
            {
                "label": "Kenya",
                "value": "KE"
            },
            {
                "label": "Kiribati",
                "value": "KI"
            },
            {
                "label": "Kuwait",
                "value": "KW"
            },
            {
                "label": "Kyrgyzstan",
                "value": "KG"
            },
            {
                "label": "Laos",
                "value": "LA"
            },
            {
                "label": "Latvia",
                "value": "LV"
            },
            {
                "label": "Lebanon",
                "value": "LB"
            },
            {
                "label": "Lesotho",
                "value": "LS"
            },
            {
                "label": "Liberia",
                "value": "LR"
            },
            {
                "label": "Libya",
                "value": "LY"
            },
            {
                "label": "Liechtenstein",
                "value": "LI"
            },
            {
                "label": "Lithuania",
                "value": "LT"
            },
            {
                "label": "Luxembourg",
                "value": "LU"
            },
            {
                "label": "Macao",
                "value": "MO"
            },
            {
                "label": "Macedonia",
                "value": "MK"
            },
            {
                "label": "Madagascar",
                "value": "MG"
            },
            {
                "label": "Malawi",
                "value": "MW"
            },
            {
                "label": "Malaysia",
                "value": "MY"
            },
            {
                "label": "Maldives",
                "value": "MV"
            },
            {
                "label": "Mali",
                "value": "ML"
            },
            {
                "label": "Malta",
                "value": "MT"
            },
            {
                "label": "Marshall Islands",
                "value": "MH"
            },
            {
                "label": "Martinique",
                "value": "MQ"
            },
            {
                "label": "Mauritania",
                "value": "MR"
            },
            {
                "label": "Mauritius",
                "value": "MU"
            },
            {
                "label": "Mayotte",
                "value": "YT"
            },
            {
                "label": "Mexico",
                "value": "MX"
            },
            {
                "label": "Micronesia",
                "value": "FM"
            },
            {
                "label": "Moldova",
                "value": "MD"
            },
            {
                "label": "Monaco",
                "value": "MC"
            },
            {
                "label": "Mongolia",
                "value": "MN"
            },
            {
                "label": "Montenegro",
                "value": "ME"
            },
            {
                "label": "Montserrat",
                "value": "MS"
            },
            {
                "label": "Morocco",
                "value": "MA"
            },
            {
                "label": "Mozambique",
                "value": "MZ"
            },
            {
                "label": "Myanmar",
                "value": "MM"
            },
            {
                "label": "Namibia",
                "value": "NA"
            },
            {
                "label": "Nauru",
                "value": "NR"
            },
            {
                "label": "Nepal",
                "value": "NP"
            },
            {
                "label": "Netherlands",
                "value": "NL"
            },
            {
                "label": "New Caledonia",
                "value": "NC"
            },
            {
                "label": "New Zealand",
                "value": "NZ"
            },
            {
                "label": "Nicaragua",
                "value": "NI"
            },
            {
                "label": "Niger",
                "value": "NE"
            },
            {
                "label": "Nigeria",
                "value": "NG"
            },
            {
                "label": "Niue",
                "value": "NU"
            },
            {
                "label": "Norfolk Island",
                "value": "NF"
            },
            {
                "label": "Northern Mariana Islands",
                "value": "MP"
            },
            {
                "label": "North Korea",
                "value": "KP"
            },
            {
                "label": "Norway",
                "value": "NO"
            },
            {
                "label": "Oman",
                "value": "OM"
            },
            {
                "label": "Pakistan",
                "value": "PK"
            },
            {
                "label": "Palau",
                "value": "PW"
            },
            {
                "label": "Palestine",
                "value": "PS"
            },
            {
                "label": "Panama",
                "value": "PA"
            },
            {
                "label": "Papua New Guinea",
                "value": "PG"
            },
            {
                "label": "Paraguay",
                "value": "PY"
            },
            {
                "label": "Peru",
                "value": "PE"
            },
            {
                "label": "Philippines",
                "value": "PH"
            },
            {
                "label": "Pitcairn",
                "value": "PN"
            },
            {
                "label": "Poland",
                "value": "PL"
            },
            {
                "label": "Portugal",
                "value": "PT"
            },
            {
                "label": "Puerto Rico",
                "value": "PR"
            },
            {
                "label": "Qatar",
                "value": "QA"
            },
            {
                "label": "Romania",
                "value": "RO"
            },
            {
                "label": "Russia",
                "value": "RU"
            },
            {
                "label": "Rwanda",
                "value": "RW"
            },
            {
                "label": "R\u00e9union",
                "value": "RE"
            },
            {
                "label": "Saint Barth\u00e9lemy",
                "value": "BL"
            },
            {
                "label": "Saint Helena",
                "value": "SH"
            },
            {
                "label": "Saint Kitts and Nevis",
                "value": "KN"
            },
            {
                "label": "Saint Lucia",
                "value": "LC"
            },
            {
                "label": "Saint Martin",
                "value": "MF"
            },
            {
                "label": "Saint Pierre and Miquelon",
                "value": "PM"
            },
            {
                "label": "Saint Vincent and the Grenadines",
                "value": "VC"
            },
            {
                "label": "Samoa",
                "value": "WS"
            },
            {
                "label": "San Marino",
                "value": "SM"
            },
            {
                "label": "Sao Tome and Principe",
                "value": "ST"
            },
            {
                "label": "Saudi Arabia",
                "value": "SA"
            },
            {
                "label": "Senegal",
                "value": "SN"
            },
            {
                "label": "Serbia",
                "value": "RS"
            },
            {
                "label": "Seychelles",
                "value": "SC"
            },
            {
                "label": "Sierra Leone",
                "value": "SL"
            },
            {
                "label": "Singapore",
                "value": "SG"
            },
            {
                "label": "Sint Maarten",
                "value": "SX"
            },
            {
                "label": "Slovakia",
                "value": "SK"
            },
            {
                "label": "Slovenia",
                "value": "SI"
            },
            {
                "label": "Solomon Islands",
                "value": "SB"
            },
            {
                "label": "Somalia",
                "value": "SO"
            },
            {
                "label": "South Africa",
                "value": "ZA"
            },
            {
                "label": "South Georgia",
                "value": "GS"
            },
            {
                "label": "South Korea",
                "value": "KR"
            },
            {
                "label": "South Sudan",
                "value": "SS"
            },
            {
                "label": "Spain",
                "value": "ES"
            },
            {
                "label": "Sri Lanka",
                "value": "LK"
            },
            {
                "label": "Sudan",
                "value": "SD"
            },
            {
                "label": "Suriname",
                "value": "SR"
            },
            {
                "label": "Svalbard",
                "value": "SJ"
            },
            {
                "label": "Swaziland",
                "value": "SZ"
            },
            {
                "label": "Sweden",
                "value": "SE"
            },
            {
                "label": "Switzerland",
                "value": "CH"
            },
            {
                "label": "Syria",
                "value": "SY"
            },
            {
                "label": "Taiwan",
                "value": "TW"
            },
            {
                "label": "Tajikistan",
                "value": "TJ"
            },
            {
                "label": "Tanzania",
                "value": "TZ"
            },
            {
                "label": "Thailand",
                "value": "TH"
            },
            {
                "label": "Timor-Leste",
                "value": "TL"
            },
            {
                "label": "Togo",
                "value": "TG"
            },
            {
                "label": "Tokelau",
                "value": "TK"
            },
            {
                "label": "Tonga",
                "value": "TO"
            },
            {
                "label": "Trinidad and Tobago",
                "value": "TT"
            },
            {
                "label": "Tunisia",
                "value": "TN"
            },
            {
                "label": "Turkey",
                "value": "TR"
            },
            {
                "label": "Turkmenistan",
                "value": "TM"
            },
            {
                "label": "Turks and Caicos Islands",
                "value": "TC"
            },
            {
                "label": "Tuvalu",
                "value": "TV"
            },
            {
                "label": "Uganda",
                "value": "UG"
            },
            {
                "label": "Ukraine",
                "value": "UA"
            },
            {
                "label": "United Arab Emirates",
                "value": "AE"
            },
            {
                "label": "United Kingdom",
                "value": "GB"
            },
            {
                "label": "United States Minor Outlying Islands",
                "value": "UM"
            },
            {
                "label": "United States",
                "value": "US"
            },
            {
                "label": "Uruguay",
                "value": "UY"
            },
            {
                "label": "Uzbekistan",
                "value": "UZ"
            },
            {
                "label": "Vanuatu",
                "value": "VU"
            },
            {
                "label": "Venezuela",
                "value": "VE"
            },
            {
                "label": "Vietnam",
                "value": "VN"
            },
            {
                "label": "Virgin Islands (British)",
                "value": "VG"
            },
            {
                "label": "Virgin Islands (U.S.)",
                "value": "VI"
            },
            {
                "label": "Wallis and Futuna",
                "value": "WF"
            },
            {
                "label": "Western Sahara",
                "value": "EH"
            },
            {
                "label": "Yemen",
                "value": "YE"
            },
            {
                "label": "Zambia",
                "value": "ZM"
            },
            {
                "label": "Zimbabwe",
                "value": "ZW"
            },
            {
                "label": "\u00c5land Islands",
                "value": "AX"
            }
        ];
        $scope.countryDropDownListOptions = {
            dataSource: countryDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };
        $scope.getCountryLabel = function() {
            if (angular.isDefined($scope.countryDropDownList)) {
                return countryDataSource[$scope.countryDropDownList.selectedIndex].label;
            } else {
                return '';
            }
        };

        // Map settings
        $scope.didFailToGeocode = false;
        $scope.map = {
            center: {
                latitude: 0,
                longitude: 0
            },
            options: {
                scrollwheel: false
            },
            zoom: 15,
            control: {},
            events: {
                drag: function(map, eventName, args) {
                    if (!$scope.didFailToGeocode) {
                        return;
                    }
                    var center = map.getCenter();
                    $scope.marker.coords.latitude = center.lat();
                    $scope.marker.coords.longitude = center.lng();
                    $scope.location.latitude = center.lat();
                    $scope.location.longitude = center.lng();
                },
                dragend: function(map, eventName, args) {
                    if (!$scope.didFailToGeocode) {
                        return;
                    }
                    $scope.map.control.refresh();
                    //var center = map.getCenter();
                    //$scope.marker.coords.latitude = center.lat();
                    //$scope.marker.coords.longitude = center.lng();
                }
            }
        };

        // Marker settings
        $scope.marker = {
            id: 0,
            coords: {
                latitude: 0,
                longitude: 0
            }
        };

        // Watch form validity
        $scope.isFormValid = false;
        $scope.$watch('form.$valid', function(newValue){
            $scope.isFormValid = newValue;
        });

        // Do show map
        $scope.doShowMap = false;
        $scope.setDoShowMap = function(doShow) {
            $scope.doShowMap = doShow;
        };

        $scope.getThirdAddressLine = function() {
            var addressComponents = [];
            angular.forEach(['locality', 'region', 'postalCode'], function(property, key){
                if ($scope[property] && $scope[property].trim()) {
                    addressComponents.push($scope[property].trim());
                }
            });
            var addressLine = undefined;
            angular.forEach(addressComponents, function(addressComponent, key){
                if (!addressLine) {
                    addressLine = addressComponent;
                } else {
                    addressLine += ', ' + addressComponent;
                }
            });
            return addressLine;
        };

        function getAddressComponent(type, addressComponents) {
            for (var i = 0; i < addressComponents.length; i++) {
                var addressComponent = addressComponents[i];
                var types = addressComponent.types;
                for (var j = 0; j < types.length; j++) {
                    if (type == types[j]) {
                        return addressComponent;
                    }
                }
            }
            return {
                long_name: '',
                short_name: ''
            };
        }

        // Map API
        uiGmapGoogleMapApi.then(function(maps) {
            var geocoder = new maps.Geocoder();
            $scope.geocode = function(address) {
                $scope.isGeocoding = true;
                geocoder.geocode({address: address}, function(results, status) {
                    var latitude;
                    var longitude;
                    var result;
                    if (status == maps.GeocoderStatus.OK && !results[0].partial_match) {
                        result = results[0];
                        var geometry = result.geometry;
                        latitude = geometry.location.lat();
                        longitude = geometry.location.lng();
                        var addressComponents = result.address_components;

                        // Grab some data
                        $scope.formattedAddress = result.formatted_address;
                        $scope.placeID = result.place_id;

                        // Street
                        var streetNumber = getAddressComponent('street_number', addressComponents).long_name;
                        var route = getAddressComponent('route', addressComponents).long_name;
                        $scope.streetAddress = streetNumber + ' ' + route;
                        $scope.subPremise = getAddressComponent('subpremise', addressComponents).long_name;
                        $scope.postalCode = getAddressComponent('postal_code', addressComponents).long_name;

                        // Region
                        $scope.region = getAddressComponent('administrative_area_level_1', addressComponents).long_name;
                        $scope.regionCode = getAddressComponent('administrative_area_level_1', addressComponents).short_name;

                        // Locality
                        $scope.locality = getAddressComponent('locality', addressComponents).long_name;

                        // Update address with lat/lon
                        $scope.location.latitude = latitude;
                        $scope.location.longitude = longitude;

                        // Update map
                        $scope.marker.coords.latitude = latitude;
                        $scope.marker.coords.longitude = longitude;
                        $scope.map.center.latitude = latitude;
                        $scope.map.center.longitude = longitude;

                        // NO FAIL
                        $scope.didFailToGeocode = false;
                    } else {
                        $scope.didFailToGeocode = true;
                        result = results[0];

                        // Use whatever the user entered...
                        $scope.streetAddress = $scope.rawStreetAddress;
                        $scope.subPremise = $scope.rawSubPremise;
                        $scope.postalCode = $scope.rawPostalCode;
                        $scope.locality = $scope.rawLocality;
                        $scope.region = $scope.rawRegion;

                        if (result && result.partial_match) {
                            latitude = result.geometry.location.lat();
                            longitude = result.geometry.location.lng();

                            // Update address with lat/lon
                            $scope.location.latitude = latitude;
                            $scope.location.longitude = longitude;

                            // Update map
                            $scope.marker.coords.latitude = latitude;
                            $scope.marker.coords.longitude = longitude;
                            $scope.map.center.latitude = latitude;
                            $scope.map.center.longitude = longitude;
                            $scope.map.zoom = 15;
                        } else {
                            latitude = 0;
                            longitude = 0;
                            $scope.map.zoom = 3;
                            $scope.map.center.latitude = latitude;
                            $scope.map.center.longitude = longitude;
                            $scope.marker.coords.latitude = latitude;
                            $scope.marker.coords.longitude = longitude;
                            $scope.location.latitude = latitude;
                            $scope.location.longitude = longitude;
                        }
                    }
                    $scope.isGeocoding = false;
                    $scope.map.control.refresh();
                    $scope.$apply();
                });
            };
        });
    }
]);

listingControllers.controller('ListingPhotosFormController', ['$scope', 'listingCreateFormService',
    function($scope, listingCreateFormService) {
        //bindToListingCreateFormService($scope, listingCreateFormService);

        $scope.files = listingCreateFormService.files;
        $scope.onNextButtonClick = function() {
            listingCreateFormService.files = $scope.files;
            //var listingImages = [];
            //angular.forEach($scope.files, function(file, key){
            //    listingImages.push({
            //        image: file.dataURL.split(',')[1],
            //        isPrimary: false,
            //        caption: ''
            //    });
            //});
            //listingImages[0].isPrimary = true;
            //listingCreateFormService.listing.images = listingImages;
        };

        $scope.isFormValid = function() {
            return $scope.files && $scope.files.length >= 1;
        }
    }
]);

listingControllers.controller('ListingPricingFormController', ['$scope', 'listingCreateFormService',
    function($scope, listingCreateFormService) {
        bindToListingCreateFormService($scope, listingCreateFormService);

        $scope.currency = 35;
        $scope.$watch('currency', function(newVal){
            listingCreateFormService.listing.currency = newVal;
        });

        // Currency type
        var currencyDataSource = [
            {
                label: 'AED',
                value: '0',
                symbol: 'د.إ'
            },
            {
                label: 'ARS',
                value: '1',
                symbol: '$'
            },
            {
                label: 'AUD',
                value: '2',
                symbol: '$'
            },
            {
                label: 'BRL',
                value: '3',
                symbol: 'R$'
            },
            {
                label: 'CAD',
                value: '4',
                symbol: '$'
            },
            {
                label: 'CHF',
                value: '5',
                symbol: 'CHF'
            },
            {
                label: 'CNY',
                value: '6',
                symbol: '¥'
            },
            {
                label: 'CRC',
                value: '7',
                symbol: '₡'
            },
            {
                label: 'CZK',
                value: '8',
                symbol: 'Kč'
            },
            {
                label: 'DKK',
                value: '9',
                symbol: 'kr'
            },
            {
                label: 'EUR',
                value: '10',
                symbol: '€'
            },
            {
                label: 'GBP',
                value: '11',
                symbol: '£'
            },
            {
                label: 'HKD',
                value: '12',
                symbol: '$'
            },
            {
                label: 'HRK',
                value: '13',
                symbol: 'kn'
            },
            {
                label: 'HUF',
                value: '14',
                symbol: 'Ft'
            },
            {
                label: 'IDR',
                value: '15',
                symbol: 'Rp'
            },
            {
                label: 'ILS',
                value: '16',
                symbol: '₪'
            },
            {
                label: 'INR',
                value: '17',
                symbol: '₹'
            },
            {
                label: 'JPY',
                value: '18',
                symbol: '¥'
            },
            {
                label: 'KRW',
                value: '19',
                symbol: '₩'
            },
            {
                label: 'MAD',
                value: '20',
                symbol: 'MAD'
            },
            {
                label: 'MXN',
                value: '21',
                symbol: '$'
            },
            {
                label: 'MYR',
                value: '22',
                symbol: 'RM'
            },
            {
                label: 'NOK',
                value: '23',
                symbol: 'kr'
            },
            {
                label: 'NZD',
                value: '24',
                symbol: '$'
            },
            {
                label: 'PEN',
                value: '25',
                symbol: 'S/.'
            },
            {
                label: 'PHP',
                value: '26',
                symbol: '₱'
            },
            {
                label: 'PLN',
                value: '27',
                symbol: 'zł'
            },
            {
                label: 'RON',
                value: '28',
                symbol: 'lei'
            },
            {
                label: 'RUB',
                value: '29',
                symbol: 'р'
            },
            {
                label: 'SEK',
                value: '30',
                symbol: 'kr'
            },
            {
                label: 'SGD',
                value: '31',
                symbol: '$'
            },
            {
                label: 'THB',
                value: '32',
                symbol: '฿'
            },
            {
                label: 'TRY',
                value: '33',
                symbol: 'TL'
            },
            {
                label: 'TWD',
                value: '34',
                symbol: '$'
            },
            {
                label: 'USD',
                value: '35',
                symbol: '$'
            },
            {
                label: 'VND',
                value: '36',
                symbol: '₫'
            },
            {
                label: 'ZAR',
                value: '37',
                symbol: '₫'
            }
        ];
        $scope.getCurrencySymbol = function(idx) {
            return currencyDataSource[idx].symbol;
        };
        $scope.currencyDropDownListOptions = {
            dataSource: currencyDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };


        // Watch form validity
        $scope.isFormValid = false;
        $scope.$watch('form.$valid', function(newValue){
            $scope.isFormValid = newValue;
        });

    }
]);

listingControllers.controller('ListingAmenitiesFormController', ['$scope', 'listingCreateFormService',
    function($scope, listingCreateFormService) {
        bindToListingCreateFormService($scope, listingCreateFormService);

        $scope.commonAmenities = [
            {
                label: 'Essentials',
                value: 0,
                tooltip: 'Towels, bed sheets, soap, and toilet paper'
            },
            {
                label: 'TV',
                value: 1
            },
            {
                label: 'Cable TV',
                value: 2
            },
            {
                label: 'Air Conditioning',
                value: 3
            },
            {
                label: 'Heating',
                value: 4,
                tooltip: 'Central heating or heater at your pad'
            },
            {
                label: 'Kitchen',
                value: 5,
                tooltip: 'Space where guests can cook their own meals'
            },
            {
                label: 'Internet',
                value: 6,
                tooltip: 'Internet (wired or wireless)'
            },
            {
                label: 'Wireless Internet',
                value: 7,
                tooltip: 'Continuous access to WiFi'
            }
        ];

        $scope.additionalAmenities = [
            {
                label: 'Hot Tub',
                value: 8
            },
            {
                label: 'Washer',
                value: 9,
                tooltip: 'In the building, free, or for a fee'
            },
            {
                label: 'Pool',
                value: 10,
                tooltip: 'Private or shared'
            },
            {
                label: 'Dryer',
                value: 11,
                tooltip: 'In the building, free, or for a fee'
            },
            {
                label: 'Breakfast',
                value: 12,
                tooltip: 'Breakfast is provided'
            },
            {
                label: 'Free Parking on Premises',
                value: 13
            },
            {
                label: 'Gym',
                value: 14
            },
            {
                label: 'Elevator in Building',
                value: 15
            },
            {
                label: 'Indoor Fireplace',
                value: 16
            },
            {
                label: 'Buzzer/Wireless Intercom',
                value: 17
            },
            {
                label: 'Doorman',
                value: 18
            },
            {
                label: 'Shampoo',
                value: 19
            }
        ];

        $scope.specialFeatures = [
            {
                label: 'Family/Kid Friendly',
                value: 20,
                tooltip: 'The property is suitable for hosting families with children'
            },
            {
                label: 'Smoking Allowed',
                value: 21
            },
            {
                label: 'Suitable for Events',
                value: 22,
                tooltip: 'The listing can accommodate a gathering of 25 or more attendees'
            },
            {
                label: 'Pets Allowed',
                value: 23
            },
            {
                label: 'Pets live on this property',
                value: 24
            },
            {
                label: 'Wheelchair Accessible',
                value: 25
            }
        ];

        $scope.homeSafety = [
            {
                label: 'Smoke Detector',
                value: 26,
                tooltip: 'There is a functioning smoke detector in the listing'
            },
            {
                label: 'Carbon Monoxide Detector',
                value: 27,
                tooltip: 'There is a functioning carbon monoxide detector in the listing'
            },
            {
                label: 'First Aid Kit',
                value: 28
            },
            {
                label: 'Safety Card',
                value: 29,
                tooltip: 'Posted emergency information and resources'
            },
            {
                label: 'Fire Extinguisher',
                value: 30
            }
        ];

        $scope.toggleAmenity = function(amenity) {
            var idx = $scope.amenities.indexOf(amenity.value);
            if (idx > -1) {
                $scope.amenities.splice(idx, 1);
            } else {
                $scope.amenities.push(amenity.value);
            }
        };

        $scope.containsAmenity = function (amenity) {
            return $scope.amenities.indexOf(amenity.value) > -1;
        };

        // Watch form validity
        $scope.isFormValid = false;
        $scope.$watch('form.$valid', function(newValue){
            $scope.isFormValid = newValue;
        });

    }
]);

listingControllers.controller('ListingDetailsFormController', ['$scope', 'listingCreateFormService',
    function($scope, listingCreateFormService) {
        bindToListingCreateFormService($scope, listingCreateFormService);

        // Watch form validity
        $scope.isFormValid = false;
        $scope.$watch('form.$valid', function(newValue){
            $scope.isFormValid = newValue;
        });

    }
]);

listingControllers.controller('ListingDescriptionFormController', ['$scope', 'listingCreateFormService',
    function($scope, listingCreateFormService) {
        bindToListingCreateFormService($scope, listingCreateFormService);

        // Watch form validity
        $scope.isFormValid = false;
        $scope.$watch('form.$valid', function(newValue){
            $scope.isFormValid = newValue;
        });

        // Make it dirty =D
        $scope.$watch('form.$error', function(newVal){
            if (listingCreateFormService.didSubmit) {
                angular.forEach($scope.form.$error.required, function(field) {
                    field.$setDirty();
                });
            }
        }, true);

    }
]);

listingControllers.controller('ListingBasicsFormController', ['$scope', 'listingCreateFormService',
    function($scope, listingCreateFormService) {
        var i = 0;
        // Bedrooms
        var numBedroomsDataSource = [];
        for (i = 0; i < 20; i++) {
            numBedroomsDataSource.push({
                label: String(i + 1),
                value: i + 1
            });
        }
        $scope.numBedroomsDropDownListOptions = {
            dataSource: numBedroomsDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        // Beds
        var numBedsDataSource = [];
        for (i = 0; i < 20; i++) {
            numBedsDataSource.push({
                label: String(i + 1),
                value: i + 1
            });
        }
        $scope.numBedsDropDownListOptions = {
            dataSource: numBedsDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        // Bathrooms
        var numBathroomsDataSource = [];
        for (i = 0; i < 10; i+=0.5) {
            numBathroomsDataSource.push({
                label: String(i + 0.5),
                value: i + 0.5
            });
        }
        $scope.numBathroomsDropDownListOptions = {
            dataSource: numBathroomsDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        // Building type
        var buildingTypeDataSource = [
            {
                label: 'Apartment',
                value: 0
            },
            {
                label: 'House',
                value: 1
            },
            {
                label: 'Bed and Breakfast',
                value: 2
            },
            {
                label: 'Loft',
                value: 3
            },
            {
                label: 'Townhouse',
                value: 4
            },
            {
                label: 'Condominium',
                value: 5
            },
            {
                label: 'Bungalow',
                value: 6
            },
            {
                label: 'Cabin',
                value: 7
            },
            {
                label: 'Villa',
                value: 8
            },
            {
                label: 'Castle',
                value: 9
            },
            {
                label: 'Dorm',
                value: 10
            },
            {
                label: 'Treehouse',
                value: 11
            },
            {
                label: 'Boat',
                value: 12
            },
            {
                label: 'Plane',
                value: 13
            },
            {
                label: 'Camper/RV',
                value: 14
            },
            {
                label: 'Igloo',
                value: 15
            },
            {
                label: 'Lighthouse',
                value: 16
            },
            {
                label: 'Yurt',
                value: 17
            },
            {
                label: 'Tipi',
                value: 18
            },
            {
                label: 'Cave',
                value: 19
            },
            {
                label: 'Island',
                value: 20
            },
            {
                label: 'Chalet',
                value: 21
            },
            {
                label: 'Earth House',
                value: 22
            },
            {
                label: 'Hut',
                value: 23
            },
            {
                label: 'Train',
                value: 24
            },
            {
                label: 'Tent',
                value: 25
            },
            {
                label: 'Other',
                value: 9999999
            }
        ];
        $scope.buildingTypeDropDownListOptions = {
            dataSource: buildingTypeDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        // Room type
        var roomTypeDataSource = [
            {
                label: 'Entire home',
                value: 0
            },
            {
                label: 'Private home',
                value: 1
            },
            {
                label: 'Shared room',
                value: 2
            }
        ];
        $scope.roomTypeDropDownListOptions = {
            dataSource: roomTypeDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        // Max guests
        var maxGuestsDataSource = [];
        for (i = 0; i < 20; i++) {
            maxGuestsDataSource.push({
                label: String(i + 1),
                value: i + 1
            });
        }
        $scope.maxGuestsDropDownListOptions = {
            dataSource: maxGuestsDataSource,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        bindToListingCreateFormService($scope, listingCreateFormService);
        //// These are my fields
        //var fields = [];
        //for (var propertyName in listingCreateFormService) {
        //    if (listingCreateFormService.hasOwnProperty(propertyName)) {
        //        fields.push(propertyName);
        //        $scope[propertyName] = listingCreateFormService[propertyName];
        //    }
        //}
        //
        //// Watch the birdy...
        //$scope.$watch(function(){ return listingCreateFormService}, function(newVal){
        //    angular.forEach(fields, function(field, key){
        //        $scope[field] = newVal[field];
        //    });
        //}, true);
        //angular.forEach(fields, function(field, key){
        //    $scope.$watch(field, function(newVal){
        //        listingCreateFormService[field] = newVal;
        //    });
        //});
    }
]);

listingControllers.controller('ListingCreateController', ['$scope', '$state', '$anchorScroll', '$location', 'ListingResource', 'listingCreateFormService',
    function($scope, $state, $anchorScroll, $location, ListingResource, listingCreateFormService) {
        $scope.isUploading = false;
        $scope.didUpload = false;
        $scope.didFail = false;
        $scope.getCurrentState = function() {
            return $state.current;
        };

        $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
            if ($scope.didUpload) {
                event.preventDefault();

            } else {
                $anchorScroll(0);
            }
        });

        function validateListing(listing) {
            // Validate description
            if (!listing.title || !listing.summary) {
                $state.go('createListing.description');
                return false;
            }

            // Validate address
            if (!listing.rawAddress ||
                !listing.country) {
                $state.go('createListing.location');
                return false;
            }

            // Validate images
            if (listing.images.length <= 0) {
                $state.go('createListing.photos');
                return false;
            }
            return true;
        }

        bindToListingCreateFormService($scope, listingCreateFormService);

        $scope.onSubmit = function() {
            listingCreateFormService.didSubmit = true;
            $scope.isUploading = true;
            $scope.didFail = false;
            var listing = new ListingResource();

            // Pull fields out of service
            for (var k in listingCreateFormService.listing) {
                if (k == 'weeklyPrice' ||
                    k == 'monthlyPrice' ||
                    k == 'securityDeposit' ||
                    k == 'cleaningFee' ||
                    k == 'extraPeopleFee') {
                    if (!isFinite(listingCreateFormService.listing[k]) || (listingCreateFormService.listing[k] === '')) {
                        continue;
                    }
                }
                if (listingCreateFormService.listing[k] === undefined) {
                    continue;
                }
                if (k == 'minimumStay') {
                    listing[k] = listingCreateFormService.listing[k] + ' 00:00:00.000000';
                } else {
                    listing[k] = listingCreateFormService.listing[k];
                }
            }

            // Prep listing images
            var images = [];
            angular.forEach(listingCreateFormService.files, function(file, key){
                images.push({
                    image: file.dataURL.split(',')[1],
                    isPrimary: false,
                    caption: ''
                });
            });
            if (images.length > 0) {
                images[0].isPrimary = true;
            }
            listing.images = images;

            // FINAL VALIDATION!!!!
            if (!validateListing(listing)) {
                $scope.isUploading = false;
                return;
            }

            // POST
            function success(listingObjectResource, responseHeaders) {
                $scope.didUpload = true;
                var url = responseHeaders('Location');
                window.location = url;
            }
            function error(listingObjectResource) {
                $scope.didUpload = false;
                $scope.didFail = true;
                $scope.isUploading = false;
            }
            console.log(listing);
            listing.$save(success, error);
        };

        $scope.getListingCreateFormService = function() {
            return listingCreateFormService;
        };

        //// Watch the birdy...
        //var fields = [];
        //for (var propertyName in listingCreateFormService) {
        //    if (listingCreateFormService.hasOwnProperty(propertyName)) {
        //        fields.push(propertyName);
        //    }
        //}
        //$scope.$watch(function(){ return listingCreateFormService}, function(newVal){
        //    angular.forEach(fields, function(field, key){
        //        $scope[field] = newVal[field];
        //        console.log($scope);
        //    });
        //}, true);
        //angular.forEach(fields, function(field, key){
        //    $scope.$watch(field, function(newVal){
        //        listingCreateFormService[field] = newVal;
        //    });
        //});
    }
]);

listingControllers.controller('ReviewListController', ['$scope', '$attrs', 'ReviewResource',
    function($scope, $attrs, ReviewResource) {
        $scope.reviewResource = null;
        $scope.limit = 10;
        $scope.offset = 0;
        $scope.listingId = $attrs.listingId;

        $scope.query = function(doResetOffset) {
            $scope.offset = (doResetOffset) ? 0:$scope.offset;
            var params = {
                listing: $scope.listingId,
                limit: $scope.limit,
                offset:$scope.offset
            };

            $scope.reviewResourcePromise = ReviewResource.get(params, function(reviewResource){
                $scope.reviewResource = reviewResource;
                $scope.reviewResourcePromise = null;
            });
        };

        // If the offset changes
        $scope.$watch('offset', function(newValue, oldValue) {
            $scope.query(false);
        });

        // Spinner
        $scope.isLoading = function() {
            return angular.isDefined($scope.reviewResourcePromise) && $scope.reviewResourcePromise != null;
        };

    }
]);



listingControllers.controller('StripeTokenFormController', ['$scope', '$timeout', 'bookingCreateFormService',
    function($scope, $timeout, bookingCreateFormService) {
        $scope.handleStripeToken = function(status, token) {
            if (status == 200) {
                bookingCreateFormService.processStripeToken(token.id);
            } else {
                bookingCreateFormService.processStripeToken(null);
            }
        };

        bookingCreateFormService.submitStripeTokenForm = function() {
            $timeout(function() {
                angular.element('#stripe-form-submit').click();
            }, 10);
        };

        bookingCreateFormService.isStripeFormValid = function() {
            return $scope.stripeForm.$valid
        };
    }
]);

listingControllers.controller('BookingCreateFormController', ['$scope', '$timeout', 'BraintreeClientTokenResource',
    function($scope, $timeout, BraintreeClientTokenResource) {
        $scope.didFail = false;
        $scope.paymentMethodNonce = undefined;
        
        // Watch form validity
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        // Submit
        $scope.submit = function(event) {
            if ($scope.paymentMethodNonce) {
                return;
            }
            $scope.isUploading = true;
            event.preventDefault();

            $scope.braintreeClient.tokenizeCard({
                number: $scope.cardNumber,
                expirationDate: $scope.expirationDate,
                cvv: $scope.securityCode
            }, function (error, paymentMethodNonce) {
                if (error) {
                    $timeout(function () {
                        $scope.didFail = true;
                        $scope.isUploading = false;
                    });
                } else {
                    $timeout(function () {
                        $scope.paymentMethodNonce = paymentMethodNonce;
                        angular.element('#booking-form').submit();
                    });
                }
                //console.log(error);
                //console.log(paymentMethodNonce);
            });
        };

        // Fetch client token for Braintree
        function success(response, responseHeaders) {
            $scope.braintreeClient = new braintree.api.Client({clientToken: response.clientToken});
        }
        function error(response) {
            $scope.didFail = true;
        }
        var clientTokenPromise = BraintreeClientTokenResource.get({}, success, error);
    }
]);

listingControllers.controller('AddToCartFormController', ['$scope', 'listingDetailService',
    function($scope, listingDetailService) {
        $scope.quantity = 1;

        // Watch form validity
        $scope.isFormValid = function() {
            return ($scope.form) ? $scope.form.$valid:false;
        };
    }
]);

listingControllers.controller('FavoriteController', ['$scope', '$attrs', 'FavoriteResource',
    function($scope, $attrs, FavoriteResource) {
        // Listing
        var favoriteId = ($attrs.favoriteId) ? parseInt($attrs.favoriteId):-1;
        $scope.listing = {
            isFavorite: favoriteId >= 0,
            id: $attrs.listingId
        };

        // Favorites
        $scope.favoriteResource = {
            id: favoriteId
        };

        $scope.toggleFavorite = function() {
            $scope.listing.isFavorite = !angular.isDefined($scope.listing.isFavorite) || !$scope.listing.isFavorite;
            var favoriteResource = new FavoriteResource();
            if ($scope.listing.isFavorite) {
                favoriteResource.objectId = $scope.listing.id;
                favoriteResource.appLabel = 'listings';
                favoriteResource.modelName = 'Listing';
                favoriteResource.$save(function(favoriteObjectResource){
                    $scope.favoriteResource = favoriteObjectResource;
                });
            } else {
                favoriteResource.id = $scope.favoriteResource.id;
                favoriteResource.$delete(function(){
                    $scope.favoriteResource = {};
                });
            }
        };
    }
]);

listingControllers.controller('ListingDetailController', ['$scope', '$attrs', 'listingDetailService', 'FavoriteResource',
    function($scope, $attrs, listingDetailService, FavoriteResource) {
        $scope.scrollTo = function(container, anchor) {
            var element = angular.element(anchor);
            angular.element(container).scrollTop(element.offset().top - 150);
        };
        $scope.scrollTo('body', '#content');

        // Listing
        var favoriteId = parseInt($attrs.favoriteId);
        $scope.listing = {
            isFavorite: favoriteId >= 0,
            id: $attrs.listingId
        };

        // Favorites
        $scope.favoriteResource = {
            id: favoriteId
        };
        $scope.toggleFavorite = function() {
            $scope.listing.isFavorite = !angular.isDefined($scope.listing.isFavorite) || !$scope.listing.isFavorite;
            var favoriteResource = new FavoriteResource();
            if ($scope.listing.isFavorite) {
                favoriteResource.objectId = $scope.listing.id;
                favoriteResource.appLabel = 'listings';
                favoriteResource.modelName = 'Listing';
                favoriteResource.$save(function(favoriteObjectResource){
                    $scope.favoriteResource = favoriteObjectResource;
                });
            } else {
                favoriteResource.id = $scope.favoriteResource.id;
                favoriteResource.$delete(function(){
                    $scope.favoriteResource = {};
                });
            }
        };
    }
]);

listingControllers.controller('ListingSearchResultsController', ['$scope', '$window', '$interval', '$state', 'listingSearchResultsService', 'ListingResource', 'FavoriteResource',
    function($scope, $window, $interval, $state, listingSearchResultsService, ListingResource, FavoriteResource) {
        $scope.listingResource = null;
        $scope.limit = 21;
        $scope.offset = 0;
        $scope.didInitialize = false;

        // The watch
        $scope.$watch('offset', function(newValue){
            $state.go('searchResults', {offset: newValue});
        });

        // Use $state to query server
        var queryPromise;
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            if (!$scope.didInitialize) {
                if (toParams.offset) {
                    $scope.offset = toParams.offset;
                }
                $scope.didInitialize = true;
            }
            if (angular.isDefined(queryPromise)) {
                $interval.cancel(queryPromise);
                listingSearchResultsService.lastParams = undefined;
            }
            queryPromise = $interval(function() {
                var params = angular.extend({}, $state.params);
                if (!angular.isDefined(params.offset)) {
                    params.offset = 0;
                }
                params.limit = $scope.limit;

                $scope.query(params);

                // Scroll
                var element = angular.element('html');
                angular.element('html, body, .search-container').scrollTop(0);
            }, 250, 1);
        });

        $scope.query = function(params) {
            listingSearchResultsService.listings = [];
            if (angular.isDefined(params.maxPrice) && params.maxPrice >= 1000) {
                params.maxPrice = undefined;
            }
            if (angular.isDefined(params.condition) && params.condition > 2) {
                params.condition = undefined;
            }
            params.listingId = '';

            if (angular.isDefined(listingSearchResultsService.lastParams) && angular.equals(listingSearchResultsService.lastParams, params)) {
                return;
            }

            $scope.listingResourcePromise = ListingResource.get(params, function(listingResource){
                $scope.listingResource = listingResource;
                $scope.listingResourcePromise = null;
                listingSearchResultsService.listings = listingResource.results;
                $scope.refreshFavorites();
                listingSearchResultsService.lastParams = params;
            });

            if (!angular.isObject($scope.favoriteResourcePromise) && !angular.isObject($scope.favoriteResource)) {
                $scope.queryFavorites();
            }
        };

        // Spinner
        $scope.isLoading = function() {
            return angular.isDefined($scope.listingResourcePromise) && $scope.listingResourcePromise != null;
        };

        // Favorites
        $scope.favoriteResource = null;
        $scope.refreshFavorites = function() {
            if (!angular.isObject($scope.listingResource) || !angular.isObject($scope.favoriteResource)) {
                return;
            }
            angular.forEach($scope.listingResource.results, function(listing, key) {
                angular.forEach($scope.favoriteResource, function(favorite, key){
                    if (favorite.objectId == listing.id) {
                        listing.isFavorite = true;
                    }
                });
            });
        };
        $scope.queryFavorites = function() {
            $scope.favoriteResourcePromise = FavoriteResource.query({favoriteId:''}, function(favoriteResource){
                $scope.favoriteResource = favoriteResource;
                $scope.favoriteResourcePromise = null;
                $scope.refreshFavorites();
            });
        };
        $scope.toggleFavorite = function(listing) {
            listing.isFavorite = !angular.isDefined(listing.isFavorite) || !listing.isFavorite;
            if (listing.isFavorite) {
                var favorite = new FavoriteResource();
                favorite.objectId = listing.id;
                favorite.appLabel = 'listings';
                favorite.modelName = 'Listing';
                favorite.$save(function(favoriteObjectResource){
                    if (!angular.isArray($scope.favoriteResource)) {
                        $scope.favoriteResource = [];
                    }
                    $scope.favoriteResource.push(favoriteObjectResource);
                });
            } else {
                angular.forEach($scope.favoriteResource, function(favorite, key){
                    if (favorite.objectId != listing.id) {
                        return;
                    }
                    favorite.$delete(function(){
                        $scope.favoriteResource.splice(key, 1);
                    });
                });
            }
        };
    }
]);

listingControllers.controller('ListingSearchResultsFormController', ['$scope', '$window', '$interval', '$state',
    function($scope, $window, $interval, $state) {
        $scope.didInitialize = false;

        // Query
        var queryPromise;
        $scope.query = '';
        $scope.$watch('query', function(newVal) {
            if (angular.isDefined(queryPromise)) {
                $interval.cancel(queryPromise);
            }
            queryPromise = $interval(function(){
                if (!$scope.didInitialize) {
                    return;
                }
                $state.go('searchResults', {
                    query: newVal,
                    offset: 0
                });
            }, 250, 1);
        });

        // Condition
        $scope.condition = 3;
        $scope.conditionDropDownListOptions = CONDITION_DROP_DOWN_LIST_OPTIONS;
        $scope.$watch('condition', function(newVal){
            if (!$scope.didInitialize) {
                return;
            }
            $state.go('searchResults', {
                condition: newVal,
                offset: 0
            });
        });

        // Price Range
        $scope.price = [0, 1000];
        $scope.priceRangeSliderOptions = {
            min: 0,
            max: 1000,
            tooltip: {
                enabled: false,
                template: '$#= selectionStart # - $#= selectionEnd #'
            }
        };

        $scope.getMinPrice = function() {
            return $scope.price[0];
        };

        $scope.getMaxPrice = function() {
            return $scope.price[1];
        };

        var priceQueryPromise;
        $scope.onPriceRangeSliderSlide = function(kendoEvent) {
            $scope.price = kendoEvent.value;
            if (angular.isDefined(priceQueryPromise)) {
                $interval.cancel(priceQueryPromise);
            }
            priceQueryPromise = $interval(function(){
                if (!$scope.didInitialize) {
                    return;
                }
                $state.go('searchResults', {
                    minPrice: $scope.price[0],
                    maxPrice: $scope.price[1],
                    offset: 0
                });
            }, 250, 1);
        };

        $(window).resize(function() {  // For redrawing the slider when the window is resized
            if (angular.isDefined($scope.priceRangeSlider)) {
                $scope.priceRangeSlider.resize();
            }
        });

        // Disable controls
        $scope.areControlsEnabled = function() {
            return true;
        };

        // Use $state to initialize controls
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            if ($scope.didInitialize) {
                return;
            }
            if (angular.isDefined(toParams.query)) {
                $scope.query = toParams.query;
            }
            if (angular.isDefined(toParams.condition)) {
                $scope.condition = toParams.condition;
            }
            if (angular.isDefined(toParams.minPrice)) {
                $scope.price[0] = toParams.minPrice;
            }
            if (angular.isDefined(toParams.maxPrice)) {
                $scope.price[1] = toParams.maxPrice;
            }
            $scope.didInitialize = true;
        });
    }
]);

listingControllers.controller('ListingSearchQueryFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        console.log($scope.queryAutoComplete);

        $scope.queries = [
            'drone',
            'red drones',
            'green drones',
            'blue drones'
        ];

        $scope.didInitialize = false;
        $scope.onSubmit = function() {
            var url = sprintf(
                '%s#/?query=%s',
                $attrs.jwAction,
                encodeURIComponent($scope.query));
            window.location = url;
        };
    }
]);

listingControllers.controller('MessageCreateFormController', ['$scope', '$attrs', 'MessageResource',
    function($scope, $attrs, MessageResource) {
        $scope.onSubmit = function() {
            var messageResource = new MessageResource();
            messageResource.body = $scope.body;
            messageResource.receiver = {
                id: $attrs.receiverId
            };
            messageResource.$save(function(messageObjectResource){
            });
        };
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

listingControllers.controller('ShippingAddressFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.country = 'US';
        $scope.region = '';
        $scope.fullName = $attrs.fullName;
        $scope.$watch('country', function(newVal){
            $scope.region = '';
        });
        $scope.doShowRegionDropdown = function() {
            return $scope.country == 'US';
        };
        //console.log($attrs);
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        $scope.regionDropDownListOptions = {
            dataSource: US_REGIONS,
            dataTextField: 'label',
            dataValueField: 'value'
        };
    }
]);

listingControllers.controller('CustomerCreateFormController', ['$scope', '$attrs', '$timeout', 'BraintreeClientTokenResource',
    function($scope, $attrs, $timeout, BraintreeClientTokenResource) {
        $scope.doDisableAddressForm = true;
        $scope.country = $attrs.country;
        $scope.region = $attrs.region;
        $scope.fullName = $attrs.fullName;
        $scope.streetAddress = $attrs.streetAddress;
        $scope.subPremise = $attrs.subPremise;
        $scope.postalCode = $attrs.postalCode;
        $scope.locality = $attrs.locality;
        $scope.didFail = false;
        $scope.paymentMethodNonce = undefined;

        $scope.$watch('country', function(newVal){
            $scope.region = '';
        });
        $scope.doShowRegionDropdown = function() {
            return $scope.country == 'US';
        };
        //console.log($attrs);
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        $scope.regionDropDownListOptions = {
            dataSource: US_REGIONS,
            dataTextField: 'label',
            dataValueField: 'value'
        };

        // Submit
        $scope.submit = function(event) {
            if ($scope.paymentMethodNonce) {
                return;
            }
            $scope.isUploading = true;
            event.preventDefault();

            $scope.braintreeClient.tokenizeCard({
                number: $scope.cardNumber,
                expirationDate: $scope.expirationDate,
                cvv: $scope.securityCode
            }, function (error, paymentMethodNonce) {
                if (error) {
                    $timeout(function () {
                        $scope.didFail = true;
                        $scope.isUploading = false;
                    });
                } else {
                    $timeout(function () {
                        $scope.paymentMethodNonce = paymentMethodNonce;
                        angular.element('#customer-create-form').submit();
                    });
                }
                //console.log(error);
                //console.log(paymentMethodNonce);
            });
        };

        // Fetch client token for Braintree
        function success(response, responseHeaders) {
            $scope.braintreeClient = new braintree.api.Client({clientToken: response.clientToken});
        }
        function error(response) {
            $scope.didFail = true;
        }
        var clientTokenPromise = BraintreeClientTokenResource.get({}, success, error);
    }
]);

listingControllers.controller('ShareMessageCreateFormController', ['$scope', '$attrs', 'ShareMessageResource',
    function($scope, $attrs, ShareMessageResource) {
        $scope.body = 'Check out this cool listing on ISLAND!';
        $scope.onSubmit = function() {
            var shareMessageResource = new ShareMessageResource();
            shareMessageResource.body = $scope.body;
            shareMessageResource.receiver = $scope.receiver;
            shareMessageResource.listing = {
                id: $attrs.listingId
            };
            shareMessageResource.$save(function(shareMessageObjectResource){
            });
        };
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);
