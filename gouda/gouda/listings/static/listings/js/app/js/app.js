var ListingApp = angular.module('gouda.listings.apps.ListingApp', [
        'angularPayments',
        'ngMaterial',
        'ngSanitize',
        'ngAnimate',
        'ngTouch',
        'slick',
        'uiGmapgoogle-maps',
        'kendo.directives',
        'gouda.customers.services',
        'gouda.listings.services',
        'gouda.listings.filters',
        'gouda.listings.controllers',
        'gouda.listings.directives',
        'gouda.listings.templates',
        'gouda.directives'
    ]
);

var ListingSearchResultsApp = angular.module('gouda.listings.apps.ListingSearchResultsApp', [
        'ngMaterial',
        'ngSanitize',
        'ngAnimate',
        'ngTouch',
        'slick',
        'ui.router',
        'uiGmapgoogle-maps',
        'kendo.directives',
        'sprintf',
        'gouda.listings.services',
        'gouda.listings.filters',
        'gouda.listings.controllers',
        'gouda.listings.directives',
        'gouda.listings.templates',
        'gouda.directives'
    ]
);

var ListingCreateApp = angular.module('gouda.listings.apps.ListingCreateApp', [
        'ngMaterial',
        'ngAnimate',
        'kendo.directives',
        'ui.router',
        'uiGmapgoogle-maps',
        'gouda.listings.services',
        'gouda.listings.filters',
        'gouda.listings.controllers',
        'gouda.listings.directives',
        'gouda.listings.templates',
        'gouda.directives'
    ]
);

ListingApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

// Creation form
ListingCreateApp.config(['uiGmapGoogleMapApiProvider', function(uiGmapGoogleMapApiProvider) {
    uiGmapGoogleMapApiProvider.configure({
        key: 'AIzaSyC1gVRbCwI42ov-U1bAS4Za1ZSyNKHX9Mg'
    });
}]);

ListingCreateApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

ListingCreateApp.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise('/form/basics');

        $stateProvider.state('createListing', {
            abstract: true,
            url: '/form',
            template: '<div style="position: relative;"><div ui-view></div></div>'
        });
        $stateProvider.state('createListing.basics', {
            url: '/basics',
            templateUrl: 'listings/basics.html',
            controller: 'ListingBasicsFormController'
        });
        $stateProvider.state('createListing.description', {
            url: '/description',
            templateUrl: 'listings/description.html',
            controller: 'ListingDescriptionFormController'
        });
        $stateProvider.state('createListing.details', {
            url: '/details',
            templateUrl: 'listings/details.html',
            controller: 'ListingDetailsFormController'
        });
        $stateProvider.state('createListing.location', {
            url: '/location',
            templateUrl: 'listings/location.html',
            controller: 'ListingLocationFormController'
        });
        $stateProvider.state('createListing.amenities', {
            url: '/amenities',
            templateUrl: 'listings/amenities.html',
            controller: 'ListingAmenitiesFormController'
        });
        $stateProvider.state('createListing.photos', {
            url: '/photos',
            templateUrl: 'listings/photos.html',
            controller: 'ListingPhotosFormController'
        });
        $stateProvider.state('createListing.pricing', {
            url: '/pricing',
            templateUrl: 'listings/pricing.html',
            controller: 'ListingPricingFormController'
        });
    }
]);

// Search results
ListingSearchResultsApp.config(['$httpProvider', function($httpProvider){
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
}]);

ListingSearchResultsApp.config(['$stateProvider', '$urlRouterProvider',
    function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise(function($injector, $location) {
            return '/?query=drones&minPrice=0&offset=0';
        });

        $stateProvider.state('searchResults', {
            url: '/?query&condition&minPrice&maxPrice&offset',
            templateUrl: 'listings/search_results.html',
            controller: 'ListingSearchResultsController'
        });
    }
]);
