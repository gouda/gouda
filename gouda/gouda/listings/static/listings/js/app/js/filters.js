'use strict';

var module = angular.module('gouda.listings.filters', ['ngSanitize']);

module.filter('firstName', function() {
    return function(input) {
        var names = input.split(' ');
        if (names.length > 1) {
            return names[0];
        } else {
            return input;
        }
    };
});

module.filter('unabbreviate', function() {
    var regions = {
        'CO': 'Colorado',
        'WA': 'Washington',
        'AK': 'Alaska'
    };
    return function(input) {
        return regions[input];
    };
});

module.filter('thumbnail', function() {
    return function(input, alias) {
        var ext = input.substring(input.lastIndexOf("."), input.length);
        if (ext == '.jpeg') {
            ext = '.jpg';
        }
        return input + '.' + alias + '_q85_crop' + ext;
    };
});

module.filter('paragraphs', ['$sanitize', function($sanitize) {
   return function(input) {
       var paragraphs = '';
       var blocks = input.trim().split(/\n\n|\r\r/);
       angular.forEach(blocks, function(block, key) {
           if (block.length > 0) {
               paragraphs = paragraphs + '<p>' + block + '</p>';
           }
       });
       return paragraphs;
   }
}]);