var listingServices = angular.module('gouda.listings.services', ['ngResource']);

listingServices.factory('ListingResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/listings/listing/:listingId',
            {},
            {},
            {stripTrailingSlashes: false});
    }]
);

listingServices.factory('MessageResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/message/:messageId',
            {},
            {},
            {stripTrailingSlashes: false});
    }]
);

listingServices.factory('ShareMessageResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/share_message/:shareMessageId',
            {},
            {},
            {stripTrailingSlashes: false});
    }]
);

listingServices.factory('ReviewResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/listings/review/',
            {},
            {},
            {stripTrailingSlashes: false});
    }]
);

listingServices.factory('FavoriteResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/favorites/favorite/:favoriteId',
            {
                favoriteId: '@id'
            },
            {
                query: {
                    method: 'GET',
                    isArray: true,
                    transformResponse: function (data) {
                        var jsonData = angular.fromJson(data);
                        return jsonData.results;
                    }
                }
            },
            {
                stripTrailingSlashes: false
            }
        );
    }]
);

listingServices.factory('ImageResource', ['$resource',
    function($resource) {
        return $resource(
            '/api/image/:id',
            {
                id: '@id'
            },
            {},
            {stripTrailingSlashes: false});
    }]
);
