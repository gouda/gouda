$( document ).ready(function() {
    $.localScroll();

    // Check out date
    var check_out_date_input = $('#id_check_out_date').pickadate({
        format: 'm/d/yyyy',
        container: '.form-group--check-out-date',
        min: Date.now(),
        onSet: function(context) {
            $('#id_check_out_date').parsley().validate();
        }
    });

    // Check in date
    var check_in_date_input = $('#id_check_in_date').pickadate({
        format: 'm/d/yyyy',
        container: '.form-group--check-in-date',
        min: Date.now(),
        onSet: function(context) {
            var check_in_date = this.get('select');
            console.log('Just set stuff:', check_in_date);
            var check_out_date = check_out_date_input.pickadate('picker').get('select');
            if (check_out_date) {
                if (check_in_date['pick'] > check_out_date['pick']) {
                    check_out_date_input.pickadate('picker').clear();
                    check_out_date_input.pickadate('picker').set('min', check_in_date);
                }
            } else {
                check_out_date_input.pickadate('picker').set('min', check_in_date);
            }
            $('#id_check_in_date').parsley().validate();
        }
    });
    var picker = check_in_date_input.pickadate('picker')
});

$(window).scroll(function() {
    var navbar = $(".navbar");
    var navbarLogo = $("#navbar-logo");
    var navbarLogoText = $("#navbar-logo-text");
    var scrollOffset = $(this).height() * 0.015;
    if ($(this).scrollTop() >= scrollOffset) {
        navbar.removeClass("navbar--before").addClass("navbar--after");
        navbarLogo.removeClass("navbar-logo--before").addClass("navbar-logo--after");
        navbarLogoText.removeClass("navbar-logo-text--before").addClass("navbar-logo-text--after");
    } else {
        navbar.removeClass("navbar--after").addClass("navbar--before");
        navbarLogo.removeClass("navbar-logo--after").addClass("navbar-logo--before");
        navbarLogoText.removeClass("navbar-logo-text--after").addClass("navbar-logo-text--before");
    }
});
