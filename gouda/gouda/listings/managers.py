from __future__ import absolute_import

from django.db import models
import stripe
from gouda.customers.utils import braintree
from gouda.customers.models import BraintreeCustomer
import logging
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
from django.conf import settings

BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')

logger = logging.getLogger(__name__)


class ListingTransactionManager(models.Manager):
    def create_listing_transaction(self, listing_transaction_items, seller, buyer, notes, do_force_duplicate_check=False, is_test_mode=None):
        if is_test_mode is None:
            is_test_mode = IS_TEST_MODE

        for listing_transaction_item in listing_transaction_items:
            try:
                assert listing_transaction_item.quantity <= listing_transaction_item.listing.quantity
            except AssertionError:
                logger.exception('Insufficient quantity of %s in stock (%d > %d)' % (listing_transaction_item.listing.title, listing_transaction_item.quantity, listing_transaction_item.listing.quantity))
                raise

        from gouda.listings.models import ListingTransaction
        from gouda.listings.models import ListingTransactionStatusType
        from gouda.models import EscrowStatusType

        customer_id = buyer.braintree_customer.braintree_id
        merchant_account_id = seller.braintree_merchant_account.braintree_id
        amount = sum([listing_transaction_item.cost for listing_transaction_item in listing_transaction_items])
        service_fee_amount = sum([listing_transaction_item.amount_withheld for listing_transaction_item in listing_transaction_items])

        result = braintree.Transaction.sale({
            'customer_id': customer_id,
            'merchant_account_id': merchant_account_id,
            'amount': amount,
            'service_fee_amount': service_fee_amount,
            'options': {
                'hold_in_escrow': True
            }
        })
        try:
            assert result.is_success
        except AssertionError:
            print result.message
            logger.exception('Failed to create a listing transaction (%s)' % result.message)
            raise

        # Check for duplicates
        if not is_test_mode or do_force_duplicate_check:
            min_time = timezone.now() - timedelta(seconds=BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS)
            try:
                assert not ListingTransaction.objects.filter(date_created__gt=min_time, amount=amount, service_fee_amount=service_fee_amount, buyer=buyer, seller=seller).exists()
            except AssertionError:
                logger.exception('Failed to issue a duplicate transaction!')
                raise

        listing_transaction = ListingTransaction.objects.create(
            braintree_id=result.transaction.id,
            amount=amount,
            service_fee_amount=service_fee_amount,
            status=ListingTransactionStatusType.get(result.transaction.status).value,
            escrow_status=EscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None,
            buyer=buyer,
            seller=seller,
            notes=notes
        )

        for listing_transaction_item in listing_transaction_items:
            listing_transaction_item.listing_transaction = listing_transaction
            listing_transaction_item.save()

        return listing_transaction
