from __future__ import absolute_import

from django.core.management.base import BaseCommand
from gouda.homeaway_bots.models import Bot


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('username')

    def handle(self, *args, **options):
        bot = Bot.objects.get(
            username=options.get('username'),
        )
        bot.send_messages()
