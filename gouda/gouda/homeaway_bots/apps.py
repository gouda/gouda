from __future__ import unicode_literals

from django.apps import AppConfig


class HomeawayBotsConfig(AppConfig):
    name = 'homeaway_bots'
