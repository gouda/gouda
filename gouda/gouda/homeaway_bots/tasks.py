from __future__ import absolute_import

import logging

from gouda.celeryapp import app
from gouda.homeaway_bots.models import Bot
from django.core import management

logger = logging.getLogger(__name__)


@app.task
def run_bot(bot_id):
    bot = Bot.objects.get(id=bot_id)
    management.call_command('homeaway_bot_send_messages', bot.username)


@app.task
def run_bots():
    for bot in Bot.objects.filter(is_enabled=True):
        run_bot.delay(bot.id)
