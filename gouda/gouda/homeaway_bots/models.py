from __future__ import unicode_literals

from django.db import models
from selenium.webdriver.common.keys import Keys
from selenium.webdriver import Remote
from selenium.webdriver.phantomjs.webdriver import WebDriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import logging
import os
import time
from django.contrib.postgres.fields import ArrayField
import re
from django.conf import settings
from django.utils import timezone
from datetime import timedelta
import random

DEBUG = getattr(settings, 'DEBUG')
PAGE_LOAD_TIMEOUT = 3

logger = logging.getLogger(__name__)


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Proxy(BaseModel):
    ip_address = models.GenericIPAddressField()
    port = models.IntegerField()

    class Meta:
        verbose_name_plural = "proxies"


class Bot(BaseModel):
    username = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    first_name = models.CharField(max_length=100, default='')
    last_name = models.CharField(max_length=100, default='')

    urls = ArrayField(models.URLField(max_length=100), default=list())
    message_template = models.TextField(default='')
    num_messages_per_hour = models.IntegerField(default=30)

    is_enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.username

    def send_messages(self):
        num_messages_created = 0

        # Decide whether or not to continue
        if Message.objects.filter(
                date_created__gte=timezone.now() - timedelta(hours=1)).count() >= self.num_messages_per_hour:
            return num_messages_created

        browser = self._login()

        for url in self.urls:
            for page_number in range(1, 20):
                # Load the page
                page_url = '%s/page:%d' % (url, page_number)
                logger.info('Loading %s...' % page_url)
                browser.get(page_url)
                time.sleep(PAGE_LOAD_TIMEOUT)

                # Process rental links
                rental_links = [str(a.get_attribute('href')) for a in browser.find_elements_by_xpath('//a')]
                rental_links = set([l for l in rental_links if re.search('vacation-rental/p\d+', l)])
                for rental_link in rental_links:
                    # Decide whether or not to continue
                    if Message.objects.filter(date_created__gte=timezone.now() - timedelta(hours=1)).count() >= self.num_messages_per_hour:
                        return num_messages_created

                    # Don't be pesky
                    if Message.objects.filter(rental_url=rental_link, bot=self).exists():
                        continue

                    # Process link
                    browser = self._process_rental(browser, rental_link)
                    num_messages_created += 1

    def _process_rental(self, browser, url):
        logger.info('Loading %s' % url)
        browser.get(url)
        time.sleep(PAGE_LOAD_TIMEOUT)
        if DEBUG:
            browser.save_screenshot('/opt/gouda/screenshots/rental_page.png')

        # Click send message
        send_message_button = browser.find_element_by_class_name('inquiry-button-sticky-hide')
        send_message_button.send_keys(Keys.TAB)
        send_message_button.click()
        time.sleep(1)
        logger.info('Clicked "Send Message"...')
        if DEBUG:
            browser.save_screenshot('/opt/gouda/screenshots/clicked_send_message.png')

        # Fill out form
        num_adults_field = browser.find_element_by_css_selector('#propertyInquiryForm > div.modal-body.js-inquiryForm.book-from-inquiry-modal > div.row > div.inquiry-form-section.clearfix > div:nth-child(2) > div > form > div > div:nth-child(1) > div > div > div > input')
        message_field = browser.find_element_by_css_selector('#modal-comments')
        num_adults_field.clear()
        num_adults_field.send_keys(1)
        message_field.clear()
        message_field.send_keys(self.message_template)
        logger.info('Filled out message form...')
        if DEBUG:
            browser.save_screenshot('/opt/gouda/screenshots/message_form.png')

        # Submit form
        submit_button = browser.find_element_by_css_selector('#propertyInquiryForm > div.modal-footer > button.btn.btn-primary.js-submitInquiry')
        submit_button.click()
        time.sleep(PAGE_LOAD_TIMEOUT)
        if DEBUG:
            browser.save_screenshot('/opt/gouda/screenshots/submitted.png')

        # Some bookkeeping...
        Message.objects.create(bot=self, text=self.message_template, rental_url=url)

        return browser

    def _login(self):
        #browser = Remote(command_executor='http://%s:%d/wd/hub' % (SELENIUM_HOST, SELENIUM_PORT), desired_capabilities=DesiredCapabilities.CHROME)
        proxies = Proxy.objects.all()
        if not proxies:
            logger.critical('No proxies were found!')
            return None
        proxy = random.choice(Proxy.objects.all())
        if not proxy:
            logger.critical('No proxy was found!')
            return None
        service_args = [
            '--proxy=http://%s:%d' % (proxy.ip_address, proxy.port),
            '--proxy-type=http'
        ]
        browser = WebDriver(service_args=service_args)
        browser.set_page_load_timeout(30)
        logger.info('Loading login page...')
        browser.get('https://cas.homeaway.com/auth/traveler/login?service=https%3A%2F%2Fwww.homeaway.com%2Fuser%2Fsso%2Fauth%3Flt%3Dtraveler%26context%3Ddef%26service%3D%252F&kmsi=true')

        # Login to account
        username_field = browser.find_element_by_xpath('//*[@id="username"]')
        password_field = browser.find_element_by_xpath('//*[@id="password"]')
        submit_button = browser.find_element_by_xpath('//*[@id="form-submit"]')
        username_field.send_keys(self.username)
        password_field.send_keys(self.password)
        submit_button.click()
        time.sleep(PAGE_LOAD_TIMEOUT)
        logger.info('Loaded %s' % browser.current_url)
        if DEBUG:
            browser.save_screenshot('/opt/gouda/screenshots/logged_in.png')
        return browser


class Message(BaseModel):
    text = models.TextField(default='')
    rental_url = models.URLField()
    bot = models.ForeignKey(Bot, related_name='messages')
