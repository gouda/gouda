from django.contrib import admin
from gouda.homeaway_bots.models import Bot
from gouda.homeaway_bots.models import Proxy
from gouda.homeaway_bots.models import Message


@admin.register(Proxy)
class ProxyAdmin(admin.ModelAdmin):
    list_display = (
        'ip_address',
        'port'
    )

    fieldsets = (
        ('Configuration', {
            'fields': (
                'ip_address',
                'port',
            )
        }),
    )


@admin.register(Bot)
class BotAdmin(admin.ModelAdmin):
    list_display = (
        'username',
    )
    # readonly_fields = (
    #     'client_id',
    #     'client_secret',
    # )

    fieldsets = (
        ('Login Credentials', {
            'fields': (
                'username',
                'password',
            )
        }),
        ('Personal Information', {
            'fields': (
                'first_name',
                'last_name',
            )
        }),
        ('Configuration', {
            'fields': (
                'is_enabled',
                'num_messages_per_hour',
                'urls',
                'message_template'
            )
        }),
    )


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'bot',
        'rental_url',
        'date_created',
    )

    fieldsets = (
        ('', {
            'fields': (
                'bot',
                'rental_url',
                'text',
            )
        }),
    )
