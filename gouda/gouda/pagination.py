from rest_framework import pagination


class LargeLimitOffsetPagination(pagination.LimitOffsetPagination):
    default_limit = 100
    max_limit = 200


class LimitOffsetPagination(pagination.LimitOffsetPagination):
    default_limit = 20
    max_limit = 40
