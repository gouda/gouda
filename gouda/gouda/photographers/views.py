from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from dateutil.parser import parse as parse_date
from django.conf import settings
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.exceptions import SuspiciousOperation
from django.http import Http404
from django.core.urlresolvers import reverse
from django.core.urlresolvers import reverse_lazy
from django.utils import timezone
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView
from django.views.generic.edit import FormView
from gouda.postmaster.models import Thread
from gouda.addresses.models import Address
from gouda.customers.models import BraintreeMerchantAccountStatusType
from gouda.customers.models import BraintreeCustomer
from gouda.customers.utils import braintree
from gouda.customers.views import BraintreeCustomerCreateView
from gouda.photographers.forms import CreateFoodBookingForm
from gouda.photographers.forms import BulkBookingForm
from gouda.photographers.forms import BookingAddressForm
from gouda.photographers.forms import BookingDetailsForm
from gouda.photographers.forms import BookingForm
from gouda.photographers.forms import StaffApprovalForm
from gouda.photographers.forms import BraintreeCustomerForm
from gouda.photographers.forms import FoodBookingAddressForm
from gouda.photographers.forms import FoodBookingDetailsForm
from gouda.photographers.forms import FoodBookingForm
from gouda.photographers.forms import ScheduleBookingForm
from gouda.photographers.forms import PhotographerForm
from gouda.photographers.forms import PhotographerRequestForm
from gouda.photographers.forms import PhotographerSearchForm
from gouda.photographers.forms import ClientApprovalForm
from gouda.photographers.forms import ClientRejectionForm
from gouda.photographers.forms import PhotographerApprovalForm
from gouda.photographers.forms import PhotographerSearchResultsForm
from gouda.photographers.forms import ReviewForm
from gouda.photographers.models import BookingCheckoutURL
from gouda.photographers.models import Booking
from gouda.photographers.models import BulkBooking
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerStatusType
from gouda.photographers.models import ReviewURL
from gouda.photographers.models import TimePeriodType
from gouda.photographers.models import ServiceType
from gouda.photographers.keys import BookingSessionKeys
from gouda.photographers.keys import FoodBookingSessionKeys
from gouda.photographers.keys import BulkBookingSessionKeys
from gouda.views import StaffLoginRequiredMixin
from gouda.views import LoginRequiredMixin
from gouda.views import NeverCacheMixin
from gouda.customers.views import BraintreeMerchantAccountRequiredMixin
from gouda.customers.models import BraintreeMerchantAccount
from datetime import time
from datetime import date
from datetime import timedelta
from django.views.generic import DetailView
from django.views.generic.detail import SingleObjectMixin
from django.views.generic.base import RedirectView
from psycopg2.extras import DateTimeTZRange
from gouda.customers.forms import BraintreeMerchantAccountVerificationForm
from django.core.exceptions import ObjectDoesNotExist
from django.core import signing

logger = logging.getLogger(__name__)
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')
IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')
DEFAULT_PHOTOGRAPHER_ID = getattr(settings, 'DEFAULT_PHOTOGRAPHER_ID')
PHOTOGRAPHER_PRICE = getattr(settings, 'PHOTOGRAPHER_PRICE')
PAY_SCALE = getattr(settings, 'PAY_SCALE')


class CreateFoodBookingFormView(NeverCacheMixin, LoginRequiredMixin, CreateView):
    template_name = 'photographers/create_food_booking.html'
    form_class = CreateFoodBookingForm
    success_url = reverse_lazy('index')

    def get_form_kwargs(self):
        form_kwargs = super(CreateFoodBookingFormView, self).get_form_kwargs()
        form_kwargs['client'] = self.request.user.profile
        return form_kwargs

    def get_initial(self):
        initial = super(CreateFoodBookingFormView, self).get_initial()
        initial['date'] = (date.today() + timedelta(days=14)).strftime('%m/%d/%Y')
        initial['start_time'] = '8:00 AM'
        initial['stop_time'] = '11:00 AM'
        return initial

    def form_valid(self, form):
        response = super(CreateFoodBookingFormView, self).form_valid(form)
        messages.info(self.request, 'Your shoot has been scheduled.')
        return response


class ScheduleBookingFormView(NeverCacheMixin, UpdateView):
    template_name = 'photographers/schedule_booking.html'
    form_class = ScheduleBookingForm
    queryset = Booking.objects.all()
    success_url = reverse_lazy('index')

    def get_context_data(self, **kwargs):
        booking = self.get_object()
        context_data = super(ScheduleBookingFormView, self).get_context_data(**kwargs)

        if not booking.address.point:
            return context_data

        context_data['invalid_dates'] = list()
        for i in range(60):
            time_range = DateTimeTZRange(lower=timezone.now() + timedelta(days=i), upper=timezone.now() + timedelta(days=i + 1))
            if not Booking.is_appointment_available(time_range, booking.address.point.y, booking.address.point.x):
                context_data['invalid_dates'].append('new Date(%d, %d, %d)' % (time_range.lower.year, time_range.lower.month - 1, time_range.lower.day))
        if context_data['invalid_dates']:
            context_data['invalid_dates'] = ', '.join(context_data['invalid_dates'])
        else:
            context_data['invalid_dates'] = ''

        return context_data

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        if not Booking.is_token_valid(kwargs.get('token'), booking.pk):
            raise SuspiciousOperation('Invalid token')
        return super(ScheduleBookingFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        booking = self.get_object()
        if not Booking.is_token_valid(kwargs.get('token'), booking.pk):
            raise SuspiciousOperation('Invalid token')
        return super(ScheduleBookingFormView, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        response = super(ScheduleBookingFormView, self).form_valid(form)
        messages.info(self.request, 'Your shoot has been scheduled.')
        return response

    def get_initial(self):
        initial = super(ScheduleBookingFormView, self).get_initial()
        initial['date'] = (date.today() + timedelta(days=14)).strftime('%m/%d/%Y')
        initial['start_time'] = '8:00 AM'
        initial['stop_time'] = '11:00 AM'
        return initial


class PhotographerApprovalFormView(NeverCacheMixin, LoginRequiredMixin, UpdateView):
    template_name = 'photographers/photographer_approval.html'
    form_class = PhotographerApprovalForm

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        if booking.did_photographer_approve or booking.did_client_approve:
            raise SuspiciousOperation('Photographer cannot approve shoot twice')
        return super(PhotographerApprovalFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        booking = self.get_object()
        if booking.did_photographer_approve or booking.did_client_approve:
            raise SuspiciousOperation('Photographer cannot approve shoot twice')
        return super(PhotographerApprovalFormView, self).post(request, *args, **kwargs)

    def get_initial(self):
        initial = super(PhotographerApprovalFormView, self).get_initial()
        initial['did_photographer_approve'] = True
        initial['num_shots'] = 25
        return initial

    def get_success_url(self):
        booking = self.get_object()
        return reverse('profiles:shoot_detail', kwargs={'pk': booking.id})

    def get_queryset(self):
        return Booking.objects.filter(photographer__profile=self.request.user.profile)

    def form_valid(self, form):
        response = super(PhotographerApprovalFormView, self).form_valid(form)
        messages.info(self.request, 'Your shoot has been finalized.')
        return response


class StaffApprovalFormView(NeverCacheMixin, StaffLoginRequiredMixin, UpdateView):
    template_name = 'photographers/staff_approval.html'
    form_class = StaffApprovalForm

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        try:
            booking.on_staff_approval(do_commit=False)
        except AssertionError as a:
            raise SuspiciousOperation(a.message)
        return super(StaffApprovalFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        booking = self.get_object()
        try:
            booking.on_staff_approval(do_commit=False)
        except AssertionError as a:
            raise SuspiciousOperation(a.message)
        return super(StaffApprovalFormView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(StaffApprovalFormView, self).get_context_data(**kwargs)

        # Get pay scale
        booking = self.get_object()
        pay_scale = None
        for p in PAY_SCALE:
            if booking.num_shots in p.get('shot_range'):
                pay_scale = p
                break
        assert pay_scale, 'Failed to find a pay scale for %d shots' % booking.num_shots

        context_data.update({
            'total_amount': pay_scale.get('amount'),
            'service_fee_amount': pay_scale.get('service_fee_amount'),
        })

        return context_data

    def get_initial(self):
        initial = super(StaffApprovalFormView, self).get_initial()
        initial['did_staff_approve'] = True
        return initial

    def get_success_url(self):
        booking = self.get_object()
        return reverse('profiles:booking_detail', kwargs={'pk': booking.id})

    def get_queryset(self):
        return Booking.objects.all()

    def form_valid(self, form):
        response = super(StaffApprovalFormView, self).form_valid(form)
        messages.info(self.request, 'You have approved this booking. The client will be contacted.')
        return response


class ClientRejectionFormView(NeverCacheMixin, LoginRequiredMixin, UpdateView):
    template_name = 'photographers/client_rejection.html'
    form_class = ClientRejectionForm

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        try:
            booking.on_client_rejection(do_commit=False)
        except AssertionError as a:
            raise SuspiciousOperation(a.message)
        return super(ClientRejectionFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        booking = self.get_object()
        try:
            booking.on_client_rejection(do_commit=False)
        except AssertionError as a:
            raise SuspiciousOperation(a.message)
        return super(ClientRejectionFormView, self).post(request, *args, **kwargs)

    def get_initial(self):
        initial = super(ClientRejectionFormView, self).get_initial()
        initial['did_client_reject'] = True
        return initial

    def get_success_url(self):
        booking = self.get_object()
        return reverse('profiles:booking_detail', kwargs={'pk': booking.id})

    def get_queryset(self):
        return Booking.objects.filter(client=self.request.user.profile)

    def form_valid(self, form):
        response = super(ClientRejectionFormView, self).form_valid(form)
        messages.info(self.request, 'You have rejected the images for this booking. We will be in touch shortly to help resolve any issues.')
        return response


class ClientApprovalFormView(NeverCacheMixin, LoginRequiredMixin, UpdateView):
    template_name = 'photographers/client_approval.html'
    form_class = ClientApprovalForm

    def get(self, request, *args, **kwargs):
        booking = self.get_object()
        try:
            booking.on_client_approval(do_commit=False)
        except AssertionError as a:
            raise SuspiciousOperation(a.message)
        return super(ClientApprovalFormView, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        booking = self.get_object()
        try:
            booking.on_client_approval(do_commit=False)
        except AssertionError as a:
            raise SuspiciousOperation(a.message)
        return super(ClientApprovalFormView, self).post(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(ClientApprovalFormView, self).get_context_data(**kwargs)

        # Get pay scale
        booking = self.get_object()
        pay_scale = None
        for p in PAY_SCALE:
            if booking.num_shots in p.get('shot_range'):
                pay_scale = p
                break
        assert pay_scale, 'Failed to find a pay scale for %d shots' % booking.num_shots

        try:
            braintree_customer = BraintreeCustomer.objects.get(user=self.request.user)
        except ObjectDoesNotExist:
            braintree_customer = None

        context_data.update({
            'credit_card': braintree_customer.default_credit_card if braintree_customer else None,
            'total_amount': pay_scale.get('amount'),
        })

        return context_data

    def get_initial(self):
        initial = super(ClientApprovalFormView, self).get_initial()
        initial['did_client_approve'] = True
        return initial

    def get_success_url(self):
        booking = self.get_object()
        return reverse('profiles:booking_detail', kwargs={'pk': booking.id})

    def get_queryset(self):
        return Booking.objects.filter(client=self.request.user.profile)

    def form_valid(self, form):
        response = super(ClientApprovalFormView, self).form_valid(form)
        messages.info(self.request, 'Your booking has been finalized.')
        return response


class PhotographerSearchFormView(NeverCacheMixin, FormView):
    template_name = 'photographers/search.html'
    form_class = PhotographerSearchForm

    def get(self, request, *args, **kwargs):
        # messages.info(request, 'Your email address has been verified.')
        return super(PhotographerSearchFormView, self).get(request, *args, **kwargs)

    def get_initial(self):
        initial = super(PhotographerSearchFormView, self).get_initial()
        initial['region'] = 'DC'
        return initial

    def form_valid(self, form):
        self.request.session[BookingSessionKeys.region] = form.cleaned_data.get('region')
        self.request.session[BookingSessionKeys.service_type] = form.cleaned_data.get('service_type')
        self.request.session[BookingSessionKeys.date] = form.cleaned_data.get('date').isoformat()

        if form.cleaned_data.get('time_period') == TimePeriodType.NOW:
            self.request.session[BookingSessionKeys.start_time] = time(8, 0, 0).isoformat()
            self.request.session[BookingSessionKeys.end_time] = time(20, 0, 0).isoformat()
        elif form.cleaned_data.get('time_period') == TimePeriodType.MORNING:
            self.request.session[BookingSessionKeys.start_time] = time(8, 0, 0).isoformat()
            self.request.session[BookingSessionKeys.end_time] = time(12, 0, 0).isoformat()
        elif form.cleaned_data.get('time_period') == TimePeriodType.AFTERNOON:
            self.request.session[BookingSessionKeys.start_time] = time(12, 0, 0).isoformat()
            self.request.session[BookingSessionKeys.end_time] = time(16, 0, 0).isoformat()
        elif form.cleaned_data.get('time_period') == TimePeriodType.EVENING:
            self.request.session[BookingSessionKeys.start_time] = time(16, 0, 0).isoformat()
            self.request.session[BookingSessionKeys.end_time] = time(20, 0, 0).isoformat()
        elif form.cleaned_data.get('time_period') == TimePeriodType.ANYTIME:
            self.request.session[BookingSessionKeys.start_time] = time(8, 0, 0).isoformat()
            self.request.session[BookingSessionKeys.end_time] = time(20, 0, 0).isoformat()

        return super(PhotographerSearchFormView, self).form_valid(form)

    def get_success_url(self):
        if self.request.session.get(BookingSessionKeys.service_type, None) == ServiceType.FOOD:
            return reverse_lazy('photographers:get_food_booking_details')
        else:
            return reverse_lazy('photographers:create_photographer_request')

    def get_context_data(self, **kwargs):
        context_data = super(PhotographerSearchFormView, self).get_context_data(**kwargs)

        # Get latest rentals
        context_data['featured_photographers'] = Photographer.objects.filter(status=PhotographerStatusType.ACTIVE).order_by('-date_created')[:12]

        return context_data


class PhotographerCreateView(NeverCacheMixin, LoginRequiredMixin, FormView):
    template_name = 'photographers/create_photographer.html'
    success_url = reverse_lazy('photographers:search')
    form_class = PhotographerForm
    merchant_account = None

    def post(self, request, *args, **kwargs):
        user = self.request.user
        if hasattr(user, 'braintree_merchant_account'):
            raise PermissionDenied()
        return super(PhotographerCreateView, self).post(request, *args, **kwargs)

    def get_merchant_account(self):
        user = self.request.user
        if hasattr(user, 'braintree_merchant_account') and not self.merchant_account:
            self.merchant_account = braintree.MerchantAccount.find(user.braintree_merchant_account.braintree_id)
        return self.merchant_account

    def get_initial(self):
        initial = super(PhotographerCreateView, self).get_initial()
        user = self.request.user
        initial['user'] = user.pk

        # Get merchant data
        merchant_account = self.get_merchant_account()
        if merchant_account:
            individual = merchant_account.individual_details.__dict__.copy()
            funding = merchant_account.funding_details.__dict__.copy()
            braintree_merchant_account_data = {
                'individual': individual,
                'funding': funding,
            }
            if merchant_account.business_details:
                braintree_merchant_account_data.update({
                    'business': merchant_account.business_details.__dict__.copy()
                })
            for prefix, data in braintree_merchant_account_data.iteritems():
                if prefix in ['individual', 'business']:
                    data.update(data.get('address_details').__dict__.copy())
                    data.pop('_setattrs', None)
                    data.pop('address_details', None)
                    data.pop('address', None)
                    if 'date_of_birth' in data:
                        date_of_birth = parse_date(data.pop('date_of_birth'))
                        data.update({
                            'month': int(date_of_birth.strftime('%m')),
                            'day': int(date_of_birth.strftime('%d')),
                            'year': int(date_of_birth.strftime('%Y'))
                        })
                data.pop('_setattrs', None)
                for key in data.keys():
                    initial['%s_%s' % (prefix, key)] = data.get(key)
            initial['funding_account_number'] = '************%s' % initial.get('funding_account_number_last_4')
            initial['status'] = BraintreeMerchantAccountStatusType.get(user.braintree_merchant_account.status)
        else:
            profile = self.request.user.profile
            initial['individual_email'] = profile.email_address
            initial['individual_locality'] = profile.locality
            initial['individual_region'] = profile.region
            initial['individual_first_name'] = profile.first_name
            initial['individual_last_name'] = profile.last_name
            if profile.date_of_birth:
                initial['individual_month'] = profile.date_of_birth.month - 1
                initial['individual_day'] = profile.date_of_birth.day
                initial['individual_year'] = profile.date_of_birth.year
        return initial

    def form_valid(self, form):
        response = super(PhotographerCreateView, self).form_valid(form)
        photographer = form.save()
        messages.info(self.request, 'Thanks for applying! One of our team members will reach out to you shortly.')
        return response

    def get_form_kwargs(self):
        kwargs = super(PhotographerCreateView, self).get_form_kwargs()

        # Make form readonly if merchant account exists
        merchant_account = self.get_merchant_account()
        kwargs['is_readonly'] = merchant_account is not None

        # Plug in user info server side...
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['funding_email'] = data.get('individual_email')
            data['funding_mobile_phone'] = data.get('individual_phone')
            data['user'] = self.request.user.pk
            kwargs['data'] = data
        return kwargs


class PhotographerSearchResultsView(NeverCacheMixin, FormView):
    template_name = 'photographers/search_results.html'
    success_url = reverse_lazy('photographers:search_results')
    form_class = PhotographerSearchResultsForm


class PhotographerRequestCreateView(NeverCacheMixin, CreateView):
    template_name = 'photographers/create_photographer_request.html'
    success_url = reverse_lazy('photographers:search')
    form_class = PhotographerRequestForm

    def get_form_kwargs(self):
        form_kwargs = super(PhotographerRequestCreateView, self).get_form_kwargs()
        form_kwargs['is_on_demand'] = self.is_on_demand()
        return form_kwargs

    def get_initial(self):
        initial = super(PhotographerRequestCreateView, self).get_initial()
        initial['full_name'] = self.request.user.full_name if self.request.user.is_authenticated() else None
        initial['email_address'] = self.request.user.email_address if self.request.user.is_authenticated() else None
        initial['service_type'] = self.request.GET.get('service_type', None)
        initial['region'] = self.request.GET.get('region', None)
        initial['date'] = self.request.GET.get('date', None)
        initial['time_period'] = self.request.GET.get('time_period', None)
        return initial

    def get_context_data(self, **kwargs):
        context_data = super(PhotographerRequestCreateView, self).get_context_data(**kwargs)
        context_data['is_on_demand'] = self.is_on_demand()

        return context_data

    def is_on_demand(self):
        service_type = self.request.GET.get('service_type', None)
        return int(service_type) == ServiceType.ON_DEMAND if service_type else False

    def get(self, request, *args, **kwargs):
        return super(PhotographerRequestCreateView, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        response = super(PhotographerRequestCreateView, self).form_valid(form)
        messages.info(self.request, 'Your request has been submitted.')
        return response


class ReviewCreateView(NeverCacheMixin, LoginRequiredMixin, FormView):
    template_name = 'photographers/create_review.html'
    success_url = reverse_lazy('photographers:search')
    form_class = ReviewForm
    review_url = None

    def get_review_url(self, request, *args, **kwargs):
        try:
            self.review_url = ReviewURL.objects.get(key=kwargs.get('key'))
        except ReviewURL.DoesNotExist:
            raise PermissionDenied()
        if self.review_url.has_been_used:
            raise PermissionDenied()
        if not request.user.is_authenticated:
            raise PermissionDenied()
        if request.user.profile.pk != self.review_url.client.pk:
            raise PermissionDenied()
        return self.review_url

    def post(self, request, *args, **kwargs):
        self.get_review_url(request, *args, **kwargs)
        return super(ReviewCreateView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.get_review_url(request, *args, **kwargs)
        return super(ReviewCreateView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(ReviewCreateView, self).get_context_data(**kwargs)
        context_data['photographer'] = self.review_url.booking.photographer
        return context_data

    def get_form_kwargs(self):
        form_kwargs = super(ReviewCreateView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = form_kwargs.get('data').copy()
            data['reviewer'] = self.request.user.profile.pk
            data['booking'] = self.review_url.booking.id
            form_kwargs['data'] = data
        return form_kwargs

    def form_valid(self, form):
        review = form.save()
        self.review_url.date_used = timezone.now()
        self.review_url.save()
        return super(ReviewCreateView, self).form_valid(form)


class BookingCheckoutURLMixin(NeverCacheMixin, LoginRequiredMixin):
    booking_checkout_url = None

    def get_booking_checkout_url(self, request, *args, **kwargs):
        try:
            self.booking_checkout_url = BookingCheckoutURL.objects.get(key=kwargs.get('booking_checkout_url'))
        except BookingCheckoutURL.DoesNotExist:
            raise PermissionDenied()
        if self.booking_checkout_url.has_been_used:
            raise PermissionDenied()
        if not request.user.is_authenticated:
            raise PermissionDenied()
        if request.user.profile.pk != self.booking_checkout_url.customer.pk:
            raise PermissionDenied()


class CustomerCreateView(BookingCheckoutURLMixin, BraintreeCustomerCreateView):
    template_name = 'photographers/checkout/create_customer.html'
    form_class = BraintreeCustomerForm

    def get_form_kwargs(self):
        kwargs = super(CustomerCreateView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['user'] = self.request.user.pk
            kwargs['data'] = data
        return kwargs

    def get_booking_checkout_url(self, request, *args, **kwargs):
        self.booking_checkout_url = None
        if 'booking_checkout_url' in kwargs:
            try:
                self.booking_checkout_url = BookingCheckoutURL.objects.get(key=kwargs.get('booking_checkout_url'))
            except BookingCheckoutURL.DoesNotExist:
                raise PermissionDenied()
            if self.booking_checkout_url.has_been_used:
                raise PermissionDenied()
            if not request.user.is_authenticated:
                raise PermissionDenied()
            if request.user.profile.pk != self.booking_checkout_url.customer.pk:
                raise PermissionDenied()

    def get_success_url(self):
        if self.booking_checkout_url:
            return reverse('photographers:create_booking', kwargs={'booking_checkout_url': self.booking_checkout_url.key})
        elif self.request.session.get(BulkBookingSessionKeys.bulk_booking_id, None):
            return reverse('photographers:create_bulk_booking')
        elif self.request.session.get(BookingSessionKeys.service_type, None) == ServiceType.FOOD:
            return reverse('photographers:create_food_booking')
        raise Http404

    def post(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        return super(CustomerCreateView, self).post(request, *args, **kwargs)

    def is_session_valid(self):
        is_bulk_booking = BulkBookingSessionKeys.bulk_booking_id in self.request.session
        is_address_valid = BookingSessionKeys.address_id in self.request.session
        is_service_type_valid = BookingSessionKeys.service_type in self.request.session
        return is_bulk_booking or is_address_valid or is_service_type_valid

    def get(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        if not self.is_session_valid():
            raise PermissionDenied()
        return super(CustomerCreateView, self).get(request, *args, **kwargs)

    def get_initial(self):
        initial = super(CustomerCreateView, self).get_initial()
        initial['user'] = self.request.user.pk
        return initial

    def form_valid(self, form):
        response = super(CustomerCreateView, self).form_valid(form)
        customer = braintree.Customer.find(self.braintree_customer.braintree_id)
        for credit_card in customer.credit_cards:
            if credit_card.default:
                self.request.session[BookingSessionKeys.credit_card] = {
                    'last_4': credit_card.last_4,
                    'card_type': credit_card.card_type,
                    'expiration_month': credit_card.expiration_month,
                    'expiration_year': credit_card.expiration_year
                }
                break
        return response

    def get_context_data(self, **kwargs):
        context_data = super(CustomerCreateView, self).get_context_data(**kwargs)
        if self.booking_checkout_url:
            context_data['booking_checkout_url'] = self.booking_checkout_url
            context_data['base_template'] = 'photographers/checkout/base.html'
            context_data['price'] = self.booking_checkout_url.price
        elif self.request.session.get(BulkBookingSessionKeys.bulk_booking_id, None):
            bulk_booking = BulkBooking.objects.get(id=self.request.session.get(BulkBookingSessionKeys.bulk_booking_id))
            context_data['base_template'] = 'photographers/checkout/bulk_base.html'
            context_data['price'] = bulk_booking.get_total_cost_from_file()
        elif self.request.session.get(BookingSessionKeys.service_type, None) == ServiceType.FOOD:
            context_data['base_template'] = 'photographers/checkout/food_base.html'
            context_data['price'] = ServiceType.prices.get(ServiceType.FOOD)
        return context_data


class BookingDetailsFormView(BookingCheckoutURLMixin, FormView):
    template_name = 'photographers/checkout/get_booking_details.html'
    form_class = BookingDetailsForm

    def get_success_url(self):
        return reverse('photographers:create_booking_address', kwargs={'booking_checkout_url': self.booking_checkout_url.key})

    def post(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        return super(BookingDetailsFormView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        return super(BookingDetailsFormView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context_data = super(BookingDetailsFormView, self).get_context_data(**kwargs)
        context_data['booking_checkout_url'] = self.booking_checkout_url
        return context_data

    def form_valid(self, form):
        self.request.session[BookingSessionKeys.display_name] = form.cleaned_data.get('display_name')
        self.request.session[BookingSessionKeys.date] = form.cleaned_data.get('date').isoformat()
        self.request.session[BookingSessionKeys.service_type] = form.cleaned_data.get('service_type')
        self.request.session[BookingSessionKeys.start_time] = form.cleaned_data.get('start_time').isoformat()
        self.request.session[BookingSessionKeys.end_time] = form.cleaned_data.get('end_time').isoformat()
        self.request.session[BookingSessionKeys.phone_number] = form.cleaned_data.get('phone_number')
        return super(BookingDetailsFormView, self).form_valid(form)


class BookingAddressCreateView(BookingCheckoutURLMixin, CreateView):
    template_name = 'photographers/checkout/create_booking_address.html'
    form_class = BookingAddressForm

    def post(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        return super(BookingAddressCreateView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        return super(BookingAddressCreateView, self).get(request, *args, **kwargs)

    def get_success_url(self):
        return reverse('photographers:create_customer', kwargs={'booking_checkout_url': self.booking_checkout_url.key})

    def get_initial(self):
        initial = super(BookingAddressCreateView, self).get_initial()
        initial['country'] = 'US'
        return initial

    def get_context_data(self, **kwargs):
        context_data = super(BookingAddressCreateView, self).get_context_data(**kwargs)
        context_data['region'] = self.request.session.get(BookingSessionKeys.region, None)
        context_data['booking_checkout_url'] = self.booking_checkout_url
        return context_data

    def form_valid(self, form):
        response = super(BookingAddressCreateView, self).form_valid(form)
        self.request.session[BookingSessionKeys.address_id] = self.object.id
        return response


class BookingCreateView(BookingCheckoutURLMixin, CreateView):
    template_name = 'photographers/checkout/create_booking.html'
    form_class = BookingForm
    success_url = reverse_lazy('photographers:search')

    def post(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        return super(BookingCreateView, self).post(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        self.get_booking_checkout_url(request, *args, **kwargs)
        if BookingSessionKeys.phone_number not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.service_type not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.start_time not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.end_time not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.date not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.address_id not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.credit_card not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.display_name not in self.request.session:
            raise PermissionDenied()
        try:
            request.user.braintree_customer
        except:
            raise PermissionDenied()
        return super(BookingCreateView, self).get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(BookingCreateView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['thread'] = Thread.objects.create().id
            data['client'] = self.request.user.profile.pk
            data['display_name'] = self.request.session[BookingSessionKeys.display_name]
            data['address'] = self.request.session[BookingSessionKeys.address_id]
            data['date'] = parse_date(self.request.session[BookingSessionKeys.date])
            data['service_type'] = self.request.session[BookingSessionKeys.service_type]
            data['start_time'] = self.request.session[BookingSessionKeys.start_time]
            data['end_time'] = self.request.session[BookingSessionKeys.end_time]
            data['phone_number'] = self.request.session[BookingSessionKeys.phone_number]
            kwargs['data'] = data
        kwargs['amount'] = self.booking_checkout_url.price
        kwargs['amount_withheld'] = self.booking_checkout_url.amount_withheld
        kwargs['coupon_id'] = self.request.session.get(BookingSessionKeys.coupon_id, None)
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super(BookingCreateView, self).get_context_data(**kwargs)
        context_data['total_cost'] = self.booking_checkout_url.price
        context_data['address'] = Address.objects.get(id=self.request.session[BookingSessionKeys.address_id])
        context_data['phone_number'] = self.request.session[BookingSessionKeys.phone_number]
        context_data['date'] = parse_date(self.request.session[BookingSessionKeys.date])
        context_data['service_type'] = ServiceType.label(int(self.request.session[BookingSessionKeys.service_type]))
        context_data['start_time'] = parse_date(self.request.session[BookingSessionKeys.start_time])
        context_data['end_time'] = parse_date(self.request.session[BookingSessionKeys.end_time])
        context_data['credit_card'] = self.request.session[BookingSessionKeys.credit_card]
        context_data['line_item'] = self.booking_checkout_url.line_item
        context_data['booking_checkout_url'] = self.booking_checkout_url
        return context_data

    def form_valid(self, form):
        response = super(BookingCreateView, self).form_valid(form)
        BookingSessionKeys.flush(self.request.session)
        messages.info(self.request, 'Your booking has been submitted.')
        self.booking_checkout_url.has_been_used = True
        self.booking_checkout_url.date_used = timezone.now()
        self.booking_checkout_url.save()
        return response


class FoodBookingDetailsFormView(LoginRequiredMixin, FormView):
    template_name = 'photographers/checkout/get_food_booking_details.html'
    form_class = FoodBookingDetailsForm

    def get_success_url(self):
        return reverse('photographers:create_food_booking_address')

    def get_form_kwargs(self):
        kwargs = super(FoodBookingDetailsFormView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['service_type'] = ServiceType.FOOD
            kwargs['data'] = data
        return kwargs

    def get_initial(self):
        initial = super(FoodBookingDetailsFormView, self).get_initial()
        initial['service_type'] = ServiceType.FOOD
        initial['contact_name'] = self.request.user.profile.full_name
        if BookingSessionKeys.date in self.request.session:
            initial['date'] = parse_date(self.request.session.get(BookingSessionKeys.date)).strftime('%m/%d/%Y')
        if BookingSessionKeys.start_time in self.request.session:
            initial['start_time'] = parse_date(self.request.session.get(BookingSessionKeys.start_time)).strftime('%I:%M %p').lstrip('0')
        if BookingSessionKeys.end_time in self.request.session:
            initial['end_time'] = parse_date(self.request.session.get(BookingSessionKeys.end_time)).strftime('%I:%M %p').lstrip('0')
        return initial

    def form_valid(self, form):
        self.request.session[BookingSessionKeys.display_name] = form.cleaned_data.get('display_name')
        self.request.session[BookingSessionKeys.date] = form.cleaned_data.get('date').isoformat()
        self.request.session[BookingSessionKeys.service_type] = form.cleaned_data.get('service_type')
        self.request.session[BookingSessionKeys.start_time] = form.cleaned_data.get('start_time').isoformat()
        self.request.session[BookingSessionKeys.end_time] = form.cleaned_data.get('end_time').isoformat()
        self.request.session[BookingSessionKeys.phone_number] = form.cleaned_data.get('contact_phone_number')

        self.request.session[FoodBookingSessionKeys.contact_name] = form.cleaned_data.get('contact_name')
        self.request.session[FoodBookingSessionKeys.contact_phone_number] = form.cleaned_data.get('contact_phone_number')
        self.request.session[FoodBookingSessionKeys.is_full_menu_shoot] = form.cleaned_data.get('is_full_menu_shoot')
        self.request.session[FoodBookingSessionKeys.is_confirmed_with_restaurant] = form.cleaned_data.get('is_confirmed_with_restaurant')
        self.request.session[FoodBookingSessionKeys.items] = form.cleaned_data.get('items')

        return super(FoodBookingDetailsFormView, self).form_valid(form)


class FoodBookingAddressCreateView(LoginRequiredMixin, CreateView):
    template_name = 'photographers/checkout/create_food_booking_address.html'
    form_class = FoodBookingAddressForm

    def get_success_url(self):
        return reverse('photographers:create_food_customer')

    def get_initial(self):
        initial = super(FoodBookingAddressCreateView, self).get_initial()
        initial['country'] = 'US'
        if BookingSessionKeys.region in self.request.session:
            initial['region'] = self.request.session.get(BookingSessionKeys.region)
        return initial

    def get_context_data(self, **kwargs):
        context_data = super(FoodBookingAddressCreateView, self).get_context_data(**kwargs)
        context_data['region'] = self.request.session.get(BookingSessionKeys.region, None)
        return context_data

    def form_valid(self, form):
        response = super(FoodBookingAddressCreateView, self).form_valid(form)
        self.request.session[FoodBookingSessionKeys.restaurant_name] = form.cleaned_data.get('restaurant_name')
        self.request.session[BookingSessionKeys.address_id] = self.object.id
        return response


class FoodBookingCreateView(LoginRequiredMixin, CreateView):
    template_name = 'photographers/checkout/create_food_booking.html'
    form_class = FoodBookingForm
    success_url = reverse_lazy('index')
    price = ServiceType.prices.get(ServiceType.FOOD)
    amount_withheld = ServiceType.amounts_withheld.get(ServiceType.FOOD)
    line_item = ServiceType.line_items.get(ServiceType.FOOD)

    def get(self, request, *args, **kwargs):
        if BookingSessionKeys.phone_number not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.service_type not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.start_time not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.end_time not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.date not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.address_id not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.credit_card not in self.request.session:
            raise PermissionDenied()
        if BookingSessionKeys.display_name not in self.request.session:
            raise PermissionDenied()
        if FoodBookingSessionKeys.contact_name not in self.request.session:
            raise PermissionDenied()
        if FoodBookingSessionKeys.contact_phone_number not in self.request.session:
            raise PermissionDenied()
        if FoodBookingSessionKeys.is_full_menu_shoot not in self.request.session:
            raise PermissionDenied()
        if FoodBookingSessionKeys.is_confirmed_with_restaurant not in self.request.session:
            raise PermissionDenied()
        if FoodBookingSessionKeys.items not in self.request.session:
            raise PermissionDenied()
        if FoodBookingSessionKeys.restaurant_name not in self.request.session:
            raise PermissionDenied()
        try:
            request.user.braintree_customer
        except:
            raise PermissionDenied()
        return super(FoodBookingCreateView, self).get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(FoodBookingCreateView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['thread'] = Thread.objects.create().id
            data['client'] = self.request.user.profile.pk
            data['display_name'] = self.request.session[BookingSessionKeys.display_name]
            data['address'] = self.request.session[BookingSessionKeys.address_id]
            data['date'] = parse_date(self.request.session[BookingSessionKeys.date])
            data['service_type'] = self.request.session[BookingSessionKeys.service_type]
            data['start_time'] = self.request.session[BookingSessionKeys.start_time]
            data['end_time'] = self.request.session[BookingSessionKeys.end_time]
            data['phone_number'] = self.request.session[BookingSessionKeys.phone_number]

            data['restaurant_name'] = self.request.session[FoodBookingSessionKeys.restaurant_name]
            data['contact_name'] = self.request.session[FoodBookingSessionKeys.contact_name]
            data['contact_phone_number'] = self.request.session[FoodBookingSessionKeys.contact_phone_number]
            data['is_full_menu_shoot'] = self.request.session[FoodBookingSessionKeys.is_full_menu_shoot]
            data['is_confirmed_with_restaurant'] = self.request.session[FoodBookingSessionKeys.is_confirmed_with_restaurant]
            data['items'] = self.request.session[FoodBookingSessionKeys.items]
            kwargs['data'] = data
        kwargs['amount'] = self.price
        kwargs['amount_withheld'] = self.amount_withheld
        kwargs['coupon_id'] = self.request.session.get(BookingSessionKeys.coupon_id, None)
        return kwargs

    def get_context_data(self, **kwargs):
        context_data = super(FoodBookingCreateView, self).get_context_data(**kwargs)
        context_data['total_cost'] = self.price
        context_data['address'] = Address.objects.get(id=self.request.session[BookingSessionKeys.address_id])
        context_data['contact_name'] = self.request.session[FoodBookingSessionKeys.contact_name]
        context_data['contact_phone_number'] = self.request.session[FoodBookingSessionKeys.contact_phone_number]
        context_data['date'] = parse_date(self.request.session[BookingSessionKeys.date])
        context_data['service_type'] = ServiceType.label(int(self.request.session[BookingSessionKeys.service_type]))
        context_data['start_time'] = parse_date(self.request.session[BookingSessionKeys.start_time])
        context_data['end_time'] = parse_date(self.request.session[BookingSessionKeys.end_time])
        context_data['credit_card'] = self.request.session[BookingSessionKeys.credit_card]
        context_data['line_item'] = self.line_item
        context_data['restaurant_name'] = self.request.session[FoodBookingSessionKeys.restaurant_name]
        return context_data

    def form_valid(self, form):
        response = super(FoodBookingCreateView, self).form_valid(form)
        messages.info(self.request, 'Your booking has been submitted.')
        BookingSessionKeys.flush(self.request.session)
        FoodBookingSessionKeys.flush(self.request.session)
        return response


class BulkBookingFileFormView(LoginRequiredMixin, CreateView):
    template_name = 'photographers/checkout/get_bulk_booking_file.html'
    form_class = BulkBookingForm
    success_url = reverse_lazy('photographers:create_bulk_customer')

    def form_valid(self, form):
        response = super(BulkBookingFileFormView, self).form_valid(form)
        bulk_booking = self.object
        self.request.session[BulkBookingSessionKeys.bulk_booking_id] = bulk_booking.id
        return response


class BulkBookingCreateView(LoginRequiredMixin, UpdateView):
    template_name = 'photographers/checkout/create_bulk_booking.html'
    form_class = BulkBookingForm
    success_url = reverse_lazy('index')

    def get(self, request, *args, **kwargs):
        if BulkBookingSessionKeys.bulk_booking_id not in self.request.session:
            raise PermissionDenied()
        try:
            request.user.braintree_customer
        except:
            raise PermissionDenied()
        return super(BulkBookingCreateView, self).get(request, *args, **kwargs)

    def get_form_kwargs(self):
        kwargs = super(BulkBookingCreateView, self).get_form_kwargs()
        if self.request.method in ('POST', 'PUT'):
            data = kwargs.get('data').copy()
            data['client'] = self.request.user.profile.pk
            kwargs['data'] = data
        return kwargs

    def get_context_data(self, **kwargs):
        bulk_booking = self.get_object()
        context_data = super(BulkBookingCreateView, self).get_context_data(**kwargs)
        context_data['bookings'] = bulk_booking.get_booking_data_from_file()
        context_data['total_cost'] = bulk_booking.get_total_cost_from_file()
        context_data['credit_card'] = self.request.session[BookingSessionKeys.credit_card]
        return context_data

    def form_valid(self, form):
        response = super(BulkBookingCreateView, self).form_valid(form)
        bulk_booking = self.object
        bulk_booking.create_bookings_from_file()
        messages.info(self.request, 'Your bookings have been submitted.')
        BookingSessionKeys.flush(self.request.session)
        FoodBookingSessionKeys.flush(self.request.session)
        BulkBookingSessionKeys.flush(self.request.session)
        return response

    def get_object(self, queryset=None):
        return BulkBooking.objects.get(id=self.request.session.get(BulkBookingSessionKeys.bulk_booking_id))


class BookingRequestDetailView(LoginRequiredMixin, DetailView):
    template_name = 'photographers/booking_request_detail.html'
    context_object_name = 'booking_request'
    model = BookingRequest

    def get_queryset(self):
        return BookingRequest.objects.filter(photographer__profile=self.request.user.profile)


class AcceptBookingRequestView(LoginRequiredMixin, SingleObjectMixin, RedirectView):
    permanent = False
    model = BookingRequest

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        # Was this url generated by the server...?
        if not BookingRequest.is_url_token_valid(kwargs.get('token'), self.object.pk):
            raise PermissionDenied()

        # Accept!
        if not self.object.can_photographer_accept_booking:
            if self.object.photographer.status == PhotographerStatusType.PENDING:
                messages.error(self.request, 'Sorry, while your account is under review we cannot allow you to accept this shoot.')
            elif self.object.booking.photographer is not None:
                messages.error(self.request, 'Sorry, it looks like another photographer has accepted the shoot.')
            else:
                messages.error(self.request, 'Sorry, based on your current schedule we cannot allow you to accept this shoot.')
        else:
            if self.object.booking.photographer is None:
                try:
                    self.object.accept_booking()
                except AssertionError:
                    raise SuspiciousOperation()
                messages.info(self.request, 'You have accepted the booking.')
            elif self.object.booking.photographer.profile == self.request.user.profile:  # User is trying to accept twice...
                raise SuspiciousOperation()
            else:
                messages.error(self.request, 'Sorry, it looks like another photographer has accepted the shoot.')

        return super(AcceptBookingRequestView, self).get(request, args, kwargs)

    def get_redirect_url(self, *args, **kwargs):
        if self.object.booking.photographer and self.object.booking.photographer.profile == self.request.user.profile:
            return reverse('profiles:shoot_detail', kwargs={'pk': self.object.booking.id})
        return reverse('profiles:shoot_list')

    def get_queryset(self):
        return BookingRequest.objects.filter(photographer__profile=self.request.user.profile)


class RejectBookingRequestView(LoginRequiredMixin, SingleObjectMixin, RedirectView):
    permanent = False
    model = BookingRequest

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        # Was this url generated by the server...?
        if not BookingRequest.is_url_token_valid(kwargs.get('token'), self.object.pk):
            raise PermissionDenied()

        # Accept!
        try:
            self.object.reject_booking()
        except AssertionError:
            raise SuspiciousOperation()

        messages.info(self.request, 'You have rejected the booking.')
        return super(RejectBookingRequestView, self).get(request, args, kwargs)

    def get_redirect_url(self, *args, **kwargs):
        return reverse('profiles:shoot_list')

    def get_queryset(self):
        return BookingRequest.objects.filter(photographer__profile=self.request.user.profile)


class PhotographerVerificationFormView(BraintreeMerchantAccountRequiredMixin, UpdateView):
    template_name = 'photographers/braintree_merchant_account_verification.html'
    form_class = BraintreeMerchantAccountVerificationForm
    success_url = reverse_lazy('index')

    def get_object(self, queryset=None):
        queryset = BraintreeMerchantAccount.objects.filter(user=self.request.user)
        if not queryset.exists():
            raise SuspiciousOperation('User does not have a Braintree merchant account')
        return queryset[0]

    def form_valid(self, form):
        messages.info(self.request, 'Your information has been submitted to Braintree.')
        return super(PhotographerVerificationFormView, self).form_valid(form)


class PhotographerCompletionView(LoginRequiredMixin, RedirectView):
    url = reverse_lazy('index')

    def get(self, request, *args, **kwargs):
        # Is the token valid
        try:
            payload = signing.loads(kwargs.get('token'))
        except signing.BadSignature:
            raise SuspiciousOperation()

        # Is this a valid booking?
        try:
            booking = Booking.objects.get(id=payload.get('booking'))
        except ObjectDoesNotExist:
            raise SuspiciousOperation()

        # Check ownership
        if self.request.user.profile != booking.photographer.profile:
            raise SuspiciousOperation()

        # Complete the shoot
        if booking.did_photographer_complete:
            raise SuspiciousOperation()
        booking.on_photographer_completion()

        # Let em know
        messages.info(self.request, 'Your photoshoot has been marked as completed!')

        return super(PhotographerCompletionView, self).get(request, args, kwargs)
