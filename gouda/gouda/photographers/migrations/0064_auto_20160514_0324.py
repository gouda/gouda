# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0063_bookingsurcharge'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingsurcharge',
            name='booking',
            field=models.ForeignKey(related_name='surcharges', to='photographers.Booking'),
        ),
    ]
