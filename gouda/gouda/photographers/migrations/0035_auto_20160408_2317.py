# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0034_foodbooking_restaurant_name'),
    ]

    operations = [
        migrations.AddField(
            model_name='foodbooking',
            name='contact_name',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='foodbooking',
            name='is_confirmed_with_restaurant',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='foodbooking',
            name='is_full_menu_shoot',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='foodbooking',
            name='items',
            field=django.contrib.postgres.fields.ArrayField(default=[], size=None, base_field=models.CharField(max_length=200), blank=True),
            preserve_default=False,
        ),
    ]
