# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0013_auto_20151103_0553'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingcheckouturl',
            name='has_been_used',
            field=models.BooleanField(default=False),
        ),
    ]
