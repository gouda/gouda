# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0007_auto_20151018_1449'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='braintree_id',
            field=models.CharField(max_length=100),
        ),
        migrations.AlterField(
            model_name='booking',
            name='escrow_status',
            field=models.IntegerField(default=9000, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='transaction_status',
            field=models.IntegerField(default=9000),
        ),
    ]
