# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0028_reviewurl'),
    ]

    operations = [
        migrations.AddField(
            model_name='reviewurl',
            name='download_url',
            field=models.URLField(blank=True),
        ),
        migrations.AddField(
            model_name='reviewurl',
            name='survey_url',
            field=models.URLField(blank=True),
        ),
    ]
