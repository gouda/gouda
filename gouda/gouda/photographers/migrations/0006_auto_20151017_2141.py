# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0005_auto_20151017_0145'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='photographer',
            name='description',
        ),
        migrations.RemoveField(
            model_name='photographer',
            name='num_flight_hours',
        ),
    ]
