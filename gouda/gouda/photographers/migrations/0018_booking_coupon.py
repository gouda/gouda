# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('customers', '0005_coupon'),
        ('photographers', '0017_bookingcheckouturl_amount_withheld'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='coupon',
            field=models.ForeignKey(to='customers.Coupon', null=True),
        ),
    ]
