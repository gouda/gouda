# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0059_auto_20160512_2258'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='did_client_approve',
            field=models.BooleanField(default=False),
        ),
    ]
