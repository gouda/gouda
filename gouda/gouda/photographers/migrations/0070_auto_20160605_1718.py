# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0069_auto_20160529_1959'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='client_rejection_date',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='did_client_reject',
            field=models.BooleanField(default=False),
        ),
    ]
