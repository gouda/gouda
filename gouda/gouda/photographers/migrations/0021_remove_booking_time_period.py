# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0020_auto_20151109_2218'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='time_period',
        ),
    ]
