# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0012_auto_20151102_2144'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='bookingcheckouturl',
            name='has_been_used',
        ),
        migrations.AddField(
            model_name='bookingcheckouturl',
            name='date_used',
            field=models.DateTimeField(null=True),
        ),
    ]
