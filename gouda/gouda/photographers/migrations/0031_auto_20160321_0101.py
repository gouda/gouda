# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0030_booking_display_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='display_name',
            field=models.CharField(default=b'', max_length=50),
        ),
    ]
