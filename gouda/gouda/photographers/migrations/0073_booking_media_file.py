# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0072_booking_staff_approval_date'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='media_file',
            field=models.FileField(null=True, upload_to='photographers/booking/media_files', blank=True),
        ),
    ]
