# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0011_auto_20151102_2143'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingcheckouturl',
            name='customer',
            field=models.ForeignKey(related_name='booking_checkout_urls', to='profiles.Profile'),
        ),
    ]
