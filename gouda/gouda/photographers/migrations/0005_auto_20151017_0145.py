# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0004_auto_20151016_2357'),
    ]

    operations = [
        migrations.RenameField(
            model_name='booking',
            old_name='status',
            new_name='transaction_status',
        ),
        migrations.AddField(
            model_name='booking',
            name='currency',
            field=models.IntegerField(default=35),
        ),
    ]
