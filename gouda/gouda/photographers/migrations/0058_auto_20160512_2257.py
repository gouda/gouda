# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0057_auto_20160511_0238'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='foodbooking',
            name='contact_email_address',
        ),
        migrations.RemoveField(
            model_name='foodbooking',
            name='contact_name',
        ),
        migrations.RemoveField(
            model_name='foodbooking',
            name='contact_phone_number',
        ),
    ]
