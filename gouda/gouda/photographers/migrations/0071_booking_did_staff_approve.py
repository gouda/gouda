# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0070_auto_20160605_1718'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='did_staff_approve',
            field=models.BooleanField(default=False),
        ),
    ]
