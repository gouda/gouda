# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0026_auto_20151118_0151'),
    ]

    operations = [
        migrations.RenameField(
            model_name='review',
            old_name='profile',
            new_name='reviewer',
        ),
    ]
