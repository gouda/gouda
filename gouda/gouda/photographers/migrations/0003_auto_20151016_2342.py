# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0002_booking'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='amount',
            field=models.DecimalField(default=1, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='booking',
            name='braintree_id',
            field=models.CharField(default='1', unique=True, max_length=100),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='booking',
            name='escrow_status',
            field=models.IntegerField(default=0, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='service_fee_amount',
            field=models.DecimalField(default=1, verbose_name=b'Amount withheld', max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='booking',
            name='status',
            field=models.IntegerField(default=0),
        ),
    ]
