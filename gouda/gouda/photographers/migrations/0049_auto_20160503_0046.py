# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0048_booking_do_hold_in_escrow'),
    ]

    operations = [
        migrations.AlterField(
            model_name='foodbooking',
            name='contact_phone_number',
            field=models.CharField(default='', max_length=200, blank=True),
        ),
    ]
