# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.storages


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0074_auto_20160608_0208'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='media_file',
            field=models.FileField(storage=gouda.storages.S3NamedMediaStorage(), null=True, upload_to='photographers/booking/media_files', blank=True),
        ),
    ]
