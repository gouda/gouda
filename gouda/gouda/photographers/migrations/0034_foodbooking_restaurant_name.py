# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0033_foodbooking'),
    ]

    operations = [
        migrations.AddField(
            model_name='foodbooking',
            name='restaurant_name',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
