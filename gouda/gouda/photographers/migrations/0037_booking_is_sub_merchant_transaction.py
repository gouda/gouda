# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0036_foodbooking_contact_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='is_sub_merchant_transaction',
            field=models.BooleanField(default=True),
        ),
    ]
