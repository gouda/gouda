# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.ranges


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0043_foodbooking_contact_email_address'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='time_range',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(null=True),
        ),
    ]
