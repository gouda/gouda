# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0003_auto_20151016_2342'),
    ]

    operations = [
        migrations.RenameField(
            model_name='booking',
            old_name='profile',
            new_name='client',
        ),
    ]
