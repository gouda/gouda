# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0044_auto_20160417_0224'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingimage',
            name='display_name',
            field=models.CharField(default='', max_length=500),
        ),
    ]
