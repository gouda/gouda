# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0015_booking_phone_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='photographerrequest',
            name='date',
            field=models.DateField(default=datetime.datetime(2015, 11, 3, 22, 42, 59, 223580, tzinfo=utc)),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='photographerrequest',
            name='time_period',
            field=models.IntegerField(default=0),
        ),
    ]
