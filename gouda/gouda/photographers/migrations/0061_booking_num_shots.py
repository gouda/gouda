# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0060_booking_did_client_approve'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='num_shots',
            field=models.IntegerField(default=None, null=True),
        ),
    ]
