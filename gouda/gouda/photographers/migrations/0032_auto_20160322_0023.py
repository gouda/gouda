# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.photographers.models
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0031_auto_20160321_0101'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingimage',
            name='image',
            field=easy_thumbnails.fields.ThumbnailerImageField(upload_to=b'booking_images/', validators=[gouda.photographers.models.validate_image]),
        ),
    ]
