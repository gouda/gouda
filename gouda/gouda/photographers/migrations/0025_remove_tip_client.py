# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0024_auto_20151117_2310'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='tip',
            name='client',
        ),
    ]
