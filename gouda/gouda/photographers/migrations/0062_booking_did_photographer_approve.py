# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0061_booking_num_shots'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='did_photographer_approve',
            field=models.BooleanField(default=False),
        ),
    ]
