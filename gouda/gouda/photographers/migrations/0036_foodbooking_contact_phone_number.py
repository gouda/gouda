# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0035_auto_20160408_2317'),
    ]

    operations = [
        migrations.AddField(
            model_name='foodbooking',
            name='contact_phone_number',
            field=models.CharField(default='', max_length=200),
            preserve_default=False,
        ),
    ]
