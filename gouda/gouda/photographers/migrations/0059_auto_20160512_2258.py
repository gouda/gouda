# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0058_auto_20160512_2257'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='contact_email_address',
            field=models.EmailField(default='', max_length=200, blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='contact_name',
            field=models.CharField(default='', max_length=200, blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='contact_phone_number',
            field=models.CharField(default='', max_length=200, blank=True),
        ),
    ]
