# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0075_auto_20160608_0211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingalbum',
            name='images',
            field=models.ManyToManyField(related_name='albums', to='photographers.BookingImage', blank=True),
        ),
    ]
