# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.storages
import gouda.photographers.models
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0021_remove_booking_time_period'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('image', easy_thumbnails.fields.ThumbnailerImageField(storage=gouda.storages.S3MediaStorage(acl=b'private'), max_length=500, upload_to=b'booking_images/', validators=[gouda.photographers.models.validate_image])),
                ('booking', models.ForeignKey(related_name='images', to='photographers.Booking')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
