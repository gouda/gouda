# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('postmaster', '0002_auto_20160510_2206'),
        ('photographers', '0054_auto_20160508_2135'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='thread',
            field=models.ForeignKey(to='postmaster.Thread', null=True),
        ),
    ]
