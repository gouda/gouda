# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0037_booking_is_sub_merchant_transaction'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='do_send_photographer_notification',
            field=models.BooleanField(default=True),
        ),
    ]
