# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0010_createbookingurl'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='CreateBookingURL',
            new_name='BookingCheckoutURL',
        ),
    ]
