# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0045_bookingimage_display_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bookingimage',
            name='display_name',
            field=models.CharField(max_length=500),
        ),
    ]
