# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0047_auto_20160428_1748'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='do_hold_in_escrow',
            field=models.BooleanField(default=True),
        ),
    ]
