# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import gouda.photographers.models
import easy_thumbnails.fields


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20151003_2123'),
        ('addresses', '0002_address_full_name'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photographer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('description', models.TextField()),
                ('phone_number', models.CharField(max_length=50)),
                ('num_flight_hours', models.IntegerField()),
                ('status', models.IntegerField(default=2)),
                ('address', models.ForeignKey(to='addresses.Address')),
                ('profile', models.OneToOneField(to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PhotographerImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('image', easy_thumbnails.fields.ThumbnailerImageField(max_length=500, upload_to=b'listing_images/', validators=[gouda.photographers.models.validate_image])),
                ('is_primary', models.BooleanField(default=False)),
                ('caption', models.TextField(default=b'', blank=True)),
                ('photographer', models.ForeignKey(related_name='images', to='photographers.Photographer')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='PhotographerRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('full_name', models.CharField(max_length=500)),
                ('email_address', models.EmailField(max_length=254)),
                ('service_type', models.IntegerField(default=0)),
                ('locality', models.CharField(max_length=500)),
                ('region', models.CharField(max_length=500)),
                ('postal_code', models.CharField(max_length=500)),
                ('message', models.TextField(blank=True)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Review',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('title', models.CharField(max_length=100)),
                ('content', models.TextField()),
                ('rating', models.IntegerField()),
                ('photographer', models.ForeignKey(related_name='reviews', to='photographers.Photographer')),
                ('profile', models.ForeignKey(related_name='photographer_reviews', to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('type', models.IntegerField(default=0)),
                ('currency', models.IntegerField(default=35)),
                ('hourly_price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('photographer', models.ForeignKey(related_name='services', to='photographers.Photographer')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
