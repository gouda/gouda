# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0042_auto_20160416_1804'),
    ]

    operations = [
        migrations.AddField(
            model_name='foodbooking',
            name='contact_email_address',
            field=models.EmailField(default='', max_length=200, blank=True),
        ),
    ]
