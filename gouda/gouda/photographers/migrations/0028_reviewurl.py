# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20151022_0348'),
        ('photographers', '0027_auto_20151118_2314'),
    ]

    operations = [
        migrations.CreateModel(
            name='ReviewURL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('key', models.CharField(unique=True, max_length=10)),
                ('date_used', models.DateTimeField(null=True)),
                ('booking', models.ForeignKey(related_name='review_urls', to='photographers.Booking')),
                ('client', models.ForeignKey(related_name='review_urls', to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
