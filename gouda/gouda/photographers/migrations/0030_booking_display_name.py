# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0029_auto_20151119_2213'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='display_name',
            field=models.CharField(default=b'', max_length=200),
        ),
    ]
