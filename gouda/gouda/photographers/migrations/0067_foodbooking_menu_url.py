# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0066_auto_20160521_0034'),
    ]

    operations = [
        migrations.AddField(
            model_name='foodbooking',
            name='menu_url',
            field=models.URLField(default='', blank=True),
        ),
    ]
