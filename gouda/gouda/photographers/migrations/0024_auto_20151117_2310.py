# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20151022_0348'),
        ('photographers', '0023_tip'),
    ]

    operations = [
        migrations.AddField(
            model_name='tip',
            name='booking',
            field=models.ForeignKey(default=None, to='photographers.Booking'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tip',
            name='client',
            field=models.ForeignKey(default=None, to='profiles.Profile'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='tip',
            name='message',
            field=models.TextField(blank=True),
        ),
    ]
