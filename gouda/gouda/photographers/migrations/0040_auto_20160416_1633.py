# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20151022_0348'),
        ('photographers', '0039_remove_booking_do_send_photographer_notification'),
    ]

    operations = [
        migrations.CreateModel(
            name='BulkBooking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('file', models.FileField(upload_to=b'')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='booking',
            name='do_send_client_confirmation',
            field=models.BooleanField(default=True),
        ),
        migrations.AddField(
            model_name='bulkbooking',
            name='bookings',
            field=models.ManyToManyField(related_name='bulk_bookings', to='photographers.Booking'),
        ),
        migrations.AddField(
            model_name='bulkbooking',
            name='client',
            field=models.ForeignKey(to='profiles.Profile'),
        ),
    ]
