# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0064_auto_20160514_0324'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='num_shots',
            field=models.IntegerField(default=None, null=True, blank=True),
        ),
    ]
