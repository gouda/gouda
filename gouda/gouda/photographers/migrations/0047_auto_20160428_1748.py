# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0046_auto_20160427_1959'),
    ]

    operations = [
        migrations.RenameField(
            model_name='bookingimage',
            old_name='display_name',
            new_name='name',
        ),
    ]
