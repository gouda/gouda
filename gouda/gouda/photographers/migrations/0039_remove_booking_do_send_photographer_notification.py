# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0038_booking_do_send_photographer_notification'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='do_send_photographer_notification',
        ),
    ]
