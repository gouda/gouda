# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0068_bookingalbum'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='client_approval_date',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='photographer_approval_date',
            field=models.DateTimeField(default=None, null=True, blank=True),
        ),
    ]
