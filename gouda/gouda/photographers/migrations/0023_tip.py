# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0022_bookingimage'),
    ]

    operations = [
        migrations.CreateModel(
            name='Tip',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('braintree_id', models.CharField(max_length=100)),
                ('amount', models.DecimalField(max_digits=10, decimal_places=2)),
                ('service_fee_amount', models.DecimalField(null=True, verbose_name=b'Amount withheld', max_digits=10, decimal_places=2)),
                ('transaction_status', models.IntegerField(default=9000)),
                ('escrow_status', models.IntegerField(default=9000, null=True, blank=True)),
                ('currency', models.IntegerField(default=35)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
