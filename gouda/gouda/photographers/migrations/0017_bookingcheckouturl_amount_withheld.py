# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0016_auto_20151103_2242'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingcheckouturl',
            name='amount_withheld',
            field=models.DecimalField(default=0, max_digits=10, decimal_places=2),
            preserve_default=False,
        ),
    ]
