# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0040_auto_20160416_1633'),
    ]

    operations = [
        migrations.AddField(
            model_name='bulkbooking',
            name='message',
            field=models.TextField(blank=True),
        ),
    ]
