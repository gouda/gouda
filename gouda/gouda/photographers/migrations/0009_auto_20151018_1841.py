# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0008_auto_20151018_1530'),
    ]

    operations = [
        migrations.AddField(
            model_name='photographerrequest',
            name='phone_number',
            field=models.CharField(default='1', max_length=500),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='photographerrequest',
            name='locality',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AlterField(
            model_name='photographerrequest',
            name='postal_code',
            field=models.CharField(max_length=500, blank=True),
        ),
        migrations.AlterField(
            model_name='photographerrequest',
            name='region',
            field=models.CharField(max_length=500, blank=True),
        ),
    ]
