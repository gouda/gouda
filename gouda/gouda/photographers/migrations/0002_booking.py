# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0002_auto_20151003_2123'),
        ('addresses', '0002_address_full_name'),
        ('photographers', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Booking',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('date', models.DateField()),
                ('service_type', models.IntegerField(default=0)),
                ('time_period', models.IntegerField(default=0)),
                ('message', models.TextField(blank=True)),
                ('address', models.ForeignKey(to='addresses.Address')),
                ('photographer', models.ForeignKey(to='photographers.Photographer')),
                ('profile', models.ForeignKey(to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
