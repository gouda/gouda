# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0041_bulkbooking_message'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bulkbooking',
            name='client',
            field=models.ForeignKey(to='profiles.Profile', null=True),
        ),
        migrations.AlterField(
            model_name='bulkbooking',
            name='file',
            field=models.FileField(upload_to='booking_files/'),
        ),
    ]
