# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.ranges
from psycopg2.extras import DateTimeTZRange
from django.utils import timezone

class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0019_auto_20151107_2302'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='booking',
            name='date',
        ),
        migrations.AddField(
            model_name='booking',
            name='time_range',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(default=DateTimeTZRange(lower=timezone.now(), upper=timezone.now())),
            preserve_default=False,
        ),
    ]
