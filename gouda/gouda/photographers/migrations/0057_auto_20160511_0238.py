# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0056_auto_20160511_0236'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='thread',
            field=models.ForeignKey(to='postmaster.Thread'),
        ),
    ]
