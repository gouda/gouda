# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0006_auto_20151017_2141'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='photographer',
            field=models.ForeignKey(to='photographers.Photographer', null=True),
        ),
    ]
