# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.postgres.fields.ranges


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0065_auto_20160514_1352'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='client_approval_date',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AddField(
            model_name='booking',
            name='photographer_approval_date',
            field=models.DateField(default=None, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='photographer',
            field=models.ForeignKey(blank=True, to='photographers.Photographer', null=True),
        ),
        migrations.AlterField(
            model_name='booking',
            name='time_range',
            field=django.contrib.postgres.fields.ranges.DateTimeRangeField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='bookingimage',
            name='comment',
            field=models.TextField(default='', blank=True),
        ),
    ]
