# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0051_auto_20160505_0114'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='available_times',
            field=models.TextField(default=''),
        ),
    ]
