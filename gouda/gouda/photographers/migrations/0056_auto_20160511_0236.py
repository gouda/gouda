# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


def create_threads(apps, schema_editor):
    Booking = apps.get_model("photographers", "Booking")
    Thread = apps.get_model("postmaster", "Thread")
    for booking in Booking.objects.all():
        if not booking.thread:
            booking.thread = Thread.objects.create()
            booking.save()


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0055_booking_thread'),
        ('postmaster', '0002_auto_20160510_2206'),
    ]

    operations = [
        migrations.RunPython(
            create_threads
        ),
    ]
