# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0053_auto_20160508_1602'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingimage',
            name='comment',
            field=models.TextField(default=''),
        ),
        migrations.AddField(
            model_name='bookingimage',
            name='did_client_approve',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bookingimage',
            name='did_client_reject',
            field=models.BooleanField(default=False),
        ),
    ]
