# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0025_remove_tip_client'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='review',
            name='photographer',
        ),
        migrations.AddField(
            model_name='review',
            name='booking',
            field=models.ForeignKey(related_name='reviews', default=None, to='photographers.Booking'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='review',
            name='profile',
            field=models.ForeignKey(related_name='booking_reviews', to='profiles.Profile'),
        ),
    ]
