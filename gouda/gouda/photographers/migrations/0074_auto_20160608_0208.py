# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0073_booking_media_file'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='media_file',
            field=models.FileField(storage='gouda.storages.S3NamedMediaStorage', null=True, upload_to='photographers/booking/media_files', blank=True),
        ),
    ]
