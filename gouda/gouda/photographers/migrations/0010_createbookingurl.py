# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profiles', '0003_auto_20151022_0348'),
        ('photographers', '0009_auto_20151018_1841'),
    ]

    operations = [
        migrations.CreateModel(
            name='CreateBookingURL',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('key', models.CharField(unique=True, max_length=10)),
                ('line_item', models.CharField(max_length=500)),
                ('price', models.DecimalField(max_digits=10, decimal_places=2)),
                ('currency', models.IntegerField(default=35)),
                ('has_been_used', models.BooleanField(default=False)),
                ('customer', models.ForeignKey(related_name='create_booking_urls', to='profiles.Profile')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
