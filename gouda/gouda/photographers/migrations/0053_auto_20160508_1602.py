# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0052_booking_available_times'),
    ]

    operations = [
        migrations.RenameField(
            model_name='booking',
            old_name='available_times',
            new_name='available_dates',
        ),
    ]
