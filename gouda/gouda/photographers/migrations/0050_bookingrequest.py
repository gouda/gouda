# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0049_auto_20160503_0046'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('booking', models.ForeignKey(to='photographers.Booking', null=True)),
                ('photographer', models.ForeignKey(to='photographers.Photographer', null=True)),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
