# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0018_booking_coupon'),
    ]

    operations = [
        migrations.AlterField(
            model_name='booking',
            name='service_fee_amount',
            field=models.DecimalField(null=True, verbose_name=b'Amount withheld', max_digits=10, decimal_places=2),
        ),
    ]
