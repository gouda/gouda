# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0067_foodbooking_menu_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='BookingAlbum',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(max_length=500)),
                ('width', models.IntegerField(null=True, blank=True)),
                ('height', models.IntegerField(null=True, blank=True)),
                ('booking', models.ForeignKey(related_name='albums', to='photographers.Booking')),
                ('images', models.ManyToManyField(related_name='albums', to='photographers.BookingImage')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
