# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0050_bookingrequest'),
    ]

    operations = [
        migrations.AddField(
            model_name='bookingrequest',
            name='did_accept_booking',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='bookingrequest',
            name='did_reject_booking',
            field=models.BooleanField(default=False),
        ),
    ]
