# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0032_auto_20160322_0023'),
    ]

    operations = [
        migrations.CreateModel(
            name='FoodBooking',
            fields=[
                ('booking_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to='photographers.Booking')),
            ],
            options={
                'abstract': False,
            },
            bases=('photographers.booking',),
        ),
    ]
