# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('photographers', '0014_bookingcheckouturl_has_been_used'),
    ]

    operations = [
        migrations.AddField(
            model_name='booking',
            name='phone_number',
            field=models.CharField(default='', max_length=500),
            preserve_default=False,
        ),
    ]
