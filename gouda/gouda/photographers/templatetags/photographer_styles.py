from django import template
from django.core.urlresolvers import reverse
from django.core.urlresolvers import NoReverseMatch

register = template.Library()


@register.simple_tag()
def photographer_bulk_checkout_progress_class(request, viewname):
    url = reverse(viewname)
    get_bulk_booking_file_url = reverse('photographers:get_bulk_booking_file')
    create_customer_url = reverse('photographers:create_bulk_customer')
    create_booking_url = reverse('photographers:create_bulk_booking')
    # create_booking_url = '#'
    if request.path in get_bulk_booking_file_url:
        if url in get_bulk_booking_file_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle-o'
    elif request.path in create_customer_url:
        if url in create_customer_url:
            return 'fa-dot-circle-o'
        elif url in get_bulk_booking_file_url:
            return 'fa-circle'
        else:
            return 'fa-circle-o'
    elif request.path in create_booking_url:
        if url in create_booking_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle'
    return 'fa-circle-o'


@register.simple_tag()
def photographer_food_checkout_progress_class(request, viewname):
    url = reverse(viewname)
    get_booking_details_url = reverse('photographers:get_food_booking_details')
    create_booking_address_url = reverse('photographers:create_food_booking_address')
    create_customer_url = reverse('photographers:create_food_customer')
    create_booking_url = reverse('photographers:create_food_booking')
    if request.path in get_booking_details_url:
        if url in get_booking_details_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle-o'
    elif request.path in create_booking_address_url:
        if url in create_booking_address_url:
            return 'fa-dot-circle-o'
        elif url in get_booking_details_url:
            return 'fa-circle'
        else:
            return 'fa-circle-o'
    elif request.path in create_customer_url:
        if url in create_customer_url:
            return 'fa-dot-circle-o'
        elif url in create_booking_address_url:
            return 'fa-circle'
        elif url in get_booking_details_url:
            return 'fa-circle'
        else:
            return 'fa-circle-o'
    elif request.path in create_booking_url:
        if url in create_booking_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle'
    return 'fa-circle-o'


@register.simple_tag()
def photographer_checkout_progress_class(request, viewname, booking_checkout_url):
    url = reverse(viewname, kwargs={'booking_checkout_url': booking_checkout_url})
    get_booking_details_url = reverse('photographers:get_booking_details', kwargs={'booking_checkout_url': booking_checkout_url})
    create_booking_address_url = reverse('photographers:create_booking_address', kwargs={'booking_checkout_url': booking_checkout_url})
    create_customer_url = reverse('photographers:create_customer', kwargs={'booking_checkout_url': booking_checkout_url})
    create_booking_url = reverse('photographers:create_booking', kwargs={'booking_checkout_url': booking_checkout_url})
    if request.path in get_booking_details_url:
        if url in get_booking_details_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle-o'
    elif request.path in create_booking_address_url:
        if url in create_booking_address_url:
            return 'fa-dot-circle-o'
        elif url in get_booking_details_url:
            return 'fa-circle'
        else:
            return 'fa-circle-o'
    elif request.path in create_customer_url:
        if url in create_customer_url:
            return 'fa-dot-circle-o'
        elif url in create_booking_address_url:
            return 'fa-circle'
        elif url in get_booking_details_url:
            return 'fa-circle'
        else:
            return 'fa-circle-o'
    elif request.path in create_booking_url:
        if url in create_booking_url:
            return 'fa-dot-circle-o'
        else:
            return 'fa-circle'
    return 'fa-circle-o'
