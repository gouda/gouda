from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import random
import string
from datetime import timedelta
from decimal import *

from django.conf import settings
from django.contrib.sites.models import Site
from django.core.mail import EmailMessage
from django.core.mail import send_mail
from django.db.models.signals import post_save
from django.db.models.signals import pre_save
from django.dispatch import receiver
from django.template import loader
from django.utils import timezone
from easy_thumbnails.signal_handlers import generate_aliases
from easy_thumbnails.signals import saved_file
from gouda.context_processors import constants
from gouda.customers.models import BraintreeEscrowStatusType
from gouda.customers.models import BraintreeTransactionStatusType
from gouda.customers.utils import braintree
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingAlbum
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import BookingCheckoutURL
from gouda.photographers.models import BookingImage
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import ReviewURL
from gouda.photographers.models import Tip
from gouda.postmaster.models import User as PostmasterUser
from gouda.postmaster.models import Thread

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')
IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
logger = logging.getLogger(__name__)


@receiver(post_save, sender=PhotographerRequest)
def handle_photographer_request_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    photographer_request = instance
    if not created:
        return

    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'photographer_request': photographer_request
    })
    context.update(constants(None))

    # Render and send notification email
    subject = loader.render_to_string('photographers/emails/photographer_request_notification_subject.txt', context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string('photographers/emails/photographer_request_notification_body.txt', context)
    send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)

    # Render and send confirmation email
    subject = loader.render_to_string('photographers/emails/photographer_request_confirmation_subject.txt', context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string('photographers/emails/photographer_request_confirmation_body.txt', context)
    send_mail(subject=subject, message=body, from_email=DEFAULT_FROM_EMAIL, recipient_list=[photographer_request.email_address])


@receiver(pre_save, sender=Tip)
def handle_tip_pre_save(instance, **kwargs):
    tip = instance

    if tip.braintree_id != '':
        return

    is_test_mode = None
    do_force_duplicate_check = True
    if is_test_mode is None:
        is_test_mode = IS_TEST_MODE

    # Check for duplicates
    if not IS_TEST_MODE or do_force_duplicate_check:
        min_time = timezone.now() - timedelta(seconds=BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS)
        try:
            assert not Tip.objects.filter(date_created__gt=min_time, amount=tip.amount, service_fee_amount=tip.service_fee_amount, booking=tip.booking).exclude(braintree_id='').exists()
        except AssertionError:
            logger.exception('Failed to issue a duplicate transaction!')
            raise

    # Get that dollar...
    result = braintree.Transaction.sale({
        'customer_id': tip.client.braintree_customer.braintree_id,
        'merchant_account_id': tip.booking.photographer.braintree_merchant_account.braintree_id,
        'amount': Decimal(tip.amount),
        'service_fee_amount': Decimal(tip.service_fee_amount),
        'options': {
            'hold_in_escrow': False,
            'submit_for_settlement': True,
        }
    })
    try:
        assert result.is_success
    except AssertionError:
        logger.exception('Failed to create a tip (%s)' % result.message)
        raise
    tip.braintree_id = result.transaction.id
    tip.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
    tip.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None


@receiver(post_save, sender=Tip)
def handle_tip_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    tip = instance
    if not created:
        return

    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'tip': tip
    })
    context.update(constants(None))

    # Render
    templates = [
        ('photographers/emails/tip/confirmation_subject.txt', 'photographers/emails/tip/confirmation_body.txt', DEFAULT_FROM_EMAIL, [tip.client.email_address]),
        ('photographers/emails/tip/photographer_notification_subject.txt', 'photographers/emails/tip/photographer_notification_body.txt', DEFAULT_FROM_EMAIL, [tip.photographer.email_address]),
        ('photographers/emails/tip/staff_notification_subject.txt', 'photographers/emails/tip/staff_notification_body.txt', SERVER_EMAIL, STAFF_EMAILS),
    ]
    for subject_template, body_template, from_email, recipients in templates:
        subject = loader.render_to_string(subject_template, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(body_template, context)
        send_mail(subject=subject, message=body, from_email=from_email, recipient_list=recipients)


@receiver(pre_save, sender=Booking)
@receiver(pre_save, sender=FoodBooking)
def handle_booking_pre_save(instance, **kwargs):
    booking = instance

    if booking.braintree_id != '':
        return
    if booking.photographer is None and booking.is_sub_merchant_transaction:
        return

    is_test_mode = None
    do_force_duplicate_check = True
    if is_test_mode is None:
        is_test_mode = IS_TEST_MODE

    # # Check for duplicates
    # if not IS_TEST_MODE or do_force_duplicate_check:
    #     min_time = timezone.now() - timedelta(seconds=BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS)
    #     try:
    #         assert not Booking.objects.filter(date_created__gt=min_time, amount=booking.amount, service_fee_amount=booking.service_fee_amount, client=booking.client).exclude(braintree_id='').exists()
    #     except AssertionError:
    #         logger.exception('Failed to issue a duplicate transaction!')
    #         raise
    #
    # # Get that dollar...
    # if not instance.is_sub_merchant_transaction:
    #     booking.service_fee_amount = None
    #     result = braintree.Transaction.sale({
    #         'customer_id': booking.client.braintree_customer.braintree_id,
    #         'amount': Decimal(booking.amount),
    #         'options': {
    #             'submit_for_settlement': True,
    #         }
    #     })
    # else:
    #     result = braintree.Transaction.sale({
    #         'customer_id': booking.client.braintree_customer.braintree_id,
    #         'merchant_account_id': booking.photographer.braintree_merchant_account.braintree_id,
    #         'amount': Decimal(booking.amount),
    #         'service_fee_amount': Decimal(booking.service_fee_amount),
    #         'options': {
    #             'hold_in_escrow': booking.do_hold_in_escrow,
    #             'submit_for_settlement': True,
    #         }
    #     })
    # try:
    #     assert result.is_success
    # except AssertionError:
    #     logger.exception('Failed to create a booking (%s)' % result.message)
    #     raise
    # booking.braintree_id = result.transaction.id
    # booking.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
    # booking.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None
    #
    #
@receiver(post_save, sender=Booking)
@receiver(post_save, sender=FoodBooking)
def handle_booking_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    booking = instance

    # Handle thread
    booking.thread.users.clear()
    if booking.client:
        booking.thread.users.add(booking.client.postmaster_user)
    if booking.photographer:
        booking.thread.users.add(booking.photographer.profile.postmaster_user)

    if not created:
        return

    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'booking': booking
    })
    context.update(constants(None))

    # Render and send notification email
    subject = loader.render_to_string('photographers/emails/booking_request_notification_subject.txt', context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string('photographers/emails/booking_request_notification_body.txt', context)
    send_mail(subject=subject, message=body, from_email=SERVER_EMAIL, recipient_list=STAFF_EMAILS)

    # Render and send confirmation email
    if booking.do_send_client_confirmation:
        booking.send_client_confirmation()

    # # Create image group
    # BookingAlbum.objects.create(**{
    #     'booking': booking,
    #     'name': 'Original'
    # })


@receiver(pre_save, sender=ReviewURL)
def handle_review_url_pre_save(instance, **kwargs):
    review_url = instance
    if review_url.pk is not None:
        return

    # Create a key
    def create_key():
        choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
        return ''.join([random.choice(choices) for i in range(4)])

    # Keep making keys until one is unique
    key = create_key()
    while ReviewURL.objects.filter(key=key).exists():
        key = create_key()

    review_url.key = key


@receiver(post_save, sender=ReviewURL)
def handle_review_url_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    # Build context
    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'review_url': instance,
    })
    context.update(constants(None))

    # Render
    subject_template = 'photographers/emails/review_url/confirmation_subject.txt' if instance.download_url else 'photographers/emails/review_url/review_subject.txt'
    body_template = 'photographers/emails/review_url/confirmation_body.txt' if instance.download_url else 'photographers/emails/review_url/review_body.txt'
    templates = [
        (subject_template, body_template, DEFAULT_FROM_EMAIL, [instance.client.email_address], STAFF_EMAILS),
    ]
    for subject_template, body_template, from_email, recipients, bcc in templates:
        subject = loader.render_to_string(subject_template, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(body_template, context)
        email_message = EmailMessage(subject=subject, body=body, from_email=from_email, to=recipients, bcc=bcc)
        email_message.send()


@receiver(pre_save, sender=BookingCheckoutURL)
def handle_booking_checkout_url_pre_save(instance, **kwargs):
    booking_checkout_url = instance
    if booking_checkout_url.pk is not None:
        return

    # Create a key
    def create_key():
        choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
        return ''.join([random.choice(choices) for i in range(4)])

    # Keep making keys until one is unique
    key = create_key()
    while BookingCheckoutURL.objects.filter(key=key).exists():
        key = create_key()

    booking_checkout_url.key = key


@receiver(post_save, sender=BookingCheckoutURL)
def handle_booking_checkout_url_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    # Build context
    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'booking_checkout_url': instance,
    })
    context.update(constants(None))

    # Render
    templates = [
        ('photographers/emails/booking_checkout_url/confirmation_subject.txt', 'photographers/emails/booking_checkout_url/confirmation_body.txt', DEFAULT_FROM_EMAIL, [instance.customer.email_address], STAFF_EMAILS),
    ]
    for subject_template, body_template, from_email, recipients, bcc in templates:
        subject = loader.render_to_string(subject_template, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(body_template, context)
        email_message = EmailMessage(subject=subject, body=body, from_email=from_email, to=recipients, bcc=bcc)
        email_message.send()


@receiver(post_save, sender=Photographer)
def handle_photographer_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    # Build context
    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'photographer': instance,
    })
    context.update(constants(None))

    # Render
    templates = [
        ('photographers/emails/photographer_notification_subject.txt', 'photographers/emails/photographer_notification_body.txt', SERVER_EMAIL, STAFF_EMAILS),
    ]
    for subject_template, body_template, from_email, recipients in templates:
        subject = loader.render_to_string(subject_template, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(body_template, context)
        send_mail(subject=subject, message=body, from_email=from_email, recipient_list=recipients)


@receiver(post_save, sender=BookingImage)
def handle_booking_image_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return

    from django.core.cache import cache
    key = 'booking:%d:image_notification' % instance.booking.id
    if cache.get(key, None):
        return
    cache.set(key, True, 3600)

    # Build context
    current_site = Site.objects.get_current()
    context = dict({
        'domain': current_site.domain,
        'site_name': current_site.name,
        'protocol': 'http',
        'booking': instance.booking,
    })
    context.update(constants(None))

    # Render
    templates = [
        ('photographers/emails/booking_image/notification_subject.txt', 'photographers/emails/booking_image/notification_body.txt', DEFAULT_FROM_EMAIL, STAFF_EMAILS, None),
    ]
    for subject_template, body_template, from_email, recipients, bcc in templates:
        subject = loader.render_to_string(subject_template, context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(body_template, context)
        email_message = EmailMessage(subject=subject, body=body, from_email=from_email, to=recipients, bcc=bcc)
        email_message.send()


@receiver(post_save, sender=BookingRequest)
def handle_booking_request_post_save(sender, instance, created, raw, using, update_fields, **kwargs):
    if not created:
        return
    instance.send_photographer_notification()

# For thumbnails...?
saved_file.connect(generate_aliases)
