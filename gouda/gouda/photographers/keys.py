from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals


class BulkBookingSessionKeys(object):
    prefix = 'gouda.photographers.session_keys.bulk_booking'
    bulk_booking_id = '%s.%s' % (prefix, 'bulk_booking_id')

    @staticmethod
    def flush(session):
        session.pop(BulkBookingSessionKeys.bulk_booking_id, None)


class FoodBookingSessionKeys(object):
    prefix = 'gouda.photographers.session_keys.food_booking'
    restaurant_name = '%s.%s' % (prefix, 'restaurant_name')
    contact_name = '%s.%s' % (prefix, 'contact_name')
    contact_phone_number = '%s.%s' % (prefix, 'contact_phone_number')
    is_full_menu_shoot = '%s.%s' % (prefix, 'is_full_menu_shoot')
    is_confirmed_with_restaurant = '%s.%s' % (prefix, 'is_confirmed_with_restaurant')
    items = '%s.%s' % (prefix, 'items')

    @staticmethod
    def flush(session):
        session.pop(FoodBookingSessionKeys.contact_name, None)
        session.pop(FoodBookingSessionKeys.contact_phone_number, None)
        session.pop(FoodBookingSessionKeys.is_full_menu_shoot, None)
        session.pop(FoodBookingSessionKeys.is_confirmed_with_restaurant, None)


class BookingSessionKeys(object):
    prefix = 'gouda.photographers.session_keys.booking'
    display_name = '%s.%s' % (prefix, 'display_name')
    phone_number = '%s.%s' % (prefix, 'phone_number')
    region = '%s.%s' % (prefix, 'region')
    service_type = '%s.%s' % (prefix, 'service_type')
    date = '%s.%s' % (prefix, 'date')
    address_id = '%s.%s' % (prefix, 'address_id')
    credit_card = '%s.%s' % (prefix, 'credit_card')
    start_time = '%s.%s' % (prefix, 'start_time')
    end_time = '%s.%s' % (prefix, 'end_time')
    coupon_id = '%s.%s' % (prefix, 'coupon_id')

    @staticmethod
    def flush(session):
        session.pop(BookingSessionKeys.display_name, None)
        session.pop(BookingSessionKeys.phone_number, None)
        session.pop(BookingSessionKeys.region, None)
        session.pop(BookingSessionKeys.service_type, None)
        session.pop(BookingSessionKeys.date, None)
        session.pop(BookingSessionKeys.address_id, None)
        session.pop(BookingSessionKeys.credit_card, None)
        session.pop(BookingSessionKeys.start_time, None)
        session.pop(BookingSessionKeys.end_time, None)
        session.pop(BookingSessionKeys.coupon_id, None)
