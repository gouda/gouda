# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
from localflavor.us import us_states
from zipfile import ZipFile

from gouda.customers.models import BraintreeTransactionStatusType

import os
from django.conf import settings
from django.contrib.postgres.fields import ArrayField
from django.contrib.postgres.fields import DateTimeRangeField
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.utils.encoding import force_bytes
from django.utils.translation import ugettext_lazy as _
from django_enumfield import enum
from easy_thumbnails.fields import ThumbnailerImageField
from gouda.addresses.models import Address
from gouda.customers.models import BraintreeTransaction
from gouda.customers.models import Coupon
from gouda.models import CurrencyType
from gouda.profiles.models import Profile
from gouda.postmaster.models import Thread
import csv
from django.core.cache import cache
from django.contrib.gis.geos import Point
from django.core import signing
from gouda.photographers import salt
from django.core.cache import cache
from django.contrib.gis.measure import D
from django.contrib.gis.geos import GEOSGeometry
from gouda.customers.models import BraintreeCustomer
from psycopg2.extras import DateTimeTZRange
from django.utils.text import slugify
from django.contrib.gis.db import models
from django.core.files.base import File
from django.core.files.base import ContentFile
from gouda.storages import S3NamedMediaStorage, S3MediaStorage
from dateutil.parser import parse as parse_date
from django.core.exceptions import ObjectDoesNotExist
from django.core.mail import EmailMultiAlternatives

logger = logging.getLogger(__name__)
MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES = getattr(settings, 'MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES')
AMOUNT_WITHHELD_PERCENTAGE = getattr(settings, 'AMOUNT_WITHHELD_PERCENTAGE')
POSTMATES_BOOKING_CHARGE_DELAY = getattr(settings, 'POSTMATES_BOOKING_CHARGE_DELAY')
RUN_DIR = getattr(settings, 'RUN_DIR', '')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
PAY_SCALE = getattr(settings, 'PAY_SCALE')
MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY = getattr(settings, 'MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY')
MIN_TIME_BETWEEN_BOOKINGS = getattr(settings, 'MIN_TIME_BETWEEN_BOOKINGS')
MAX_BOOKINGS_PER_PHOTOGRAPHER = getattr(settings, 'MAX_BOOKINGS_PER_PHOTOGRAPHER')


def validate_image(obj):
    if obj.file.size > MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES * 1024 * 1024:
        raise ValidationError('Maximum image file size is %d MB' % MEDIA_IMAGE_SIZE_LIMIT_IN_MEGABYTES)


class BaseModel(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    date_modified = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class PhotographerStatusType(enum.Enum):
    ACTIVE = 0
    INACTIVE = 1
    PENDING = 2

    labels = {
        ACTIVE: 'Active',
        INACTIVE: 'Inactive',
        PENDING: 'Under Review'
    }


class TimePeriodType(enum.Enum):
    NOW = 0
    ANYTIME = 1
    MORNING = 2
    AFTERNOON = 3
    EVENING = 4
    NIGHT = 5

    labels = {
        NOW: 'Right Now',
        ANYTIME: 'ANYTIME 8am - 12am',
        MORNING: 'MORNING 8am - 12pm',
        AFTERNOON: 'AFTERNOON 12pm - 4pm',
        EVENING: 'EVENING 4pm - 8pm',
        NIGHT: 'NIGHT 8pm - 12am'
    }


class ServiceType(enum.Enum):
    ON_DEMAND = 0
    REAL_ESTATE = 1
    PRIVATE_EVENT = 2
    SURVEILLANCE = 3
    MAPPING = 4
    DISASTER_RELIEF = 5
    CINEMATOGRAPHY = 6
    INSPECTIONS = 7
    SURVEYS = 8
    BUILDER_SERVICES = 9
    LIVE_BROADCASTING = 10
    PHOTOGRAPHY = 11
    FOOD = 12
    FASHION = 13

    labels = {
        ON_DEMAND: 'On Demand',
        REAL_ESTATE: 'Real Estate Photography',
        PRIVATE_EVENT: 'Private Event',
        SURVEILLANCE: 'Surveillance',
        MAPPING: 'Mapping',
        DISASTER_RELIEF: 'Disaster Relief',
        CINEMATOGRAPHY: 'Cinematography',
        INSPECTIONS: 'Inspections',
        SURVEYS: 'Surveys',
        BUILDER_SERVICES: 'Builder Services',
        LIVE_BROADCASTING: 'Live Broadcasting',
        PHOTOGRAPHY: 'Photography',
        FOOD: 'Food Photography',
        FASHION: 'Fashion Photography',
    }

    prices = {
        FOOD: 150
    }

    amounts_withheld = {
        FOOD: 50
    }

    line_items = {
        FOOD: 'Food Photography Services'
    }


class Photographer(BaseModel):
    profile = models.OneToOneField(Profile)
    phone_number = models.CharField(max_length=50, blank=True, default='')
    status = enum.EnumField(PhotographerStatusType, blank=False, default=PhotographerStatusType.PENDING)
    address = models.ForeignKey(Address, null=True, blank=True)

    objects = models.GeoManager()

    def __unicode__(self):
        return self.profile.full_name

    @property
    def full_name(self):
        return self.profile.full_name

    @property
    def first_name(self):
        return self.profile.first_name

    @property
    def email_address(self):
        return self.profile.email_address

    @property
    def braintree_merchant_account(self):
        return self.profile.braintree_merchant_account

    @property
    def rating(self):
        reviews = self.reviews.all()
        num_reviews = reviews.count()
        if num_reviews == 0:
            return 0
        sum = 0
        for review in reviews:
            sum += review.rating
        return float(sum) / num_reviews

    @property
    def is_superuser(self):
        return self.profile.user.is_superuser

    @property
    def postmaster_user(self):
        return self.profile.postmaster_user

    @property
    def display_name(self):
        return self.profile.display_name


class PhotographerImage(BaseModel):
    photographer = models.ForeignKey('Photographer', related_name='images')
    image = ThumbnailerImageField(max_length=500, upload_to='listing_images/', validators=[validate_image])
    is_primary = models.BooleanField(default=False)
    caption = models.TextField(default='', blank=True)


class Service(BaseModel):
    photographer = models.ForeignKey('Photographer', related_name='services')
    type = enum.EnumField(ServiceType, blank=False)
    currency = enum.EnumField(CurrencyType, blank=False, default=CurrencyType.USD)
    hourly_price = models.DecimalField(max_digits=10, decimal_places=2)


class Review(BaseModel):
    booking = models.ForeignKey('Booking', related_name='reviews')
    reviewer = models.ForeignKey(Profile, related_name='booking_reviews')
    title = models.CharField(max_length=100)
    content = models.TextField()
    rating = models.IntegerField()


class ReviewURL(BaseModel):
    key = models.CharField(max_length=10, blank=False, unique=True)
    booking = models.ForeignKey('Booking', related_name='review_urls')
    client = models.ForeignKey(Profile, related_name='review_urls')
    download_url = models.URLField(blank=True)
    survey_url = models.URLField(blank=True)
    date_used = models.DateTimeField(null=True)

    @property
    def url(self):
        site = Site.objects.get_current()
        if self.key:
            return 'https://%s%s' % (site.domain, reverse('photographers:create_review', kwargs={'key': self.key}))
        else:
            return None

    @property
    def has_been_used(self):
        return self.date_used is not None


class PhotographerRequest(BaseModel):
    full_name = models.CharField(max_length=500)
    phone_number = models.CharField(max_length=500)
    email_address = models.EmailField()
    service_type = enum.EnumField(ServiceType, blank=False)
    locality = models.CharField(max_length=500, blank=True)
    region = models.CharField(max_length=500, blank=True)
    postal_code = models.CharField(max_length=500, blank=True)
    message = models.TextField(blank=True)
    date = models.DateField()
    time_period = enum.EnumField(TimePeriodType)

    @property
    def service_type_label(self):
        return ServiceType.label(self.service_type)

    @property
    def time_period_label(self):
        return TimePeriodType.label(self.time_period)

    @property
    def first_name(self):
        toks = self.full_name.split()
        return toks[0]


class BookingCheckoutURL(BaseModel):
    key = models.CharField(max_length=10, blank=False, unique=True)
    line_item = models.CharField(max_length=500)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    amount_withheld = models.DecimalField(max_digits=10, decimal_places=2)
    currency = enum.EnumField(CurrencyType, blank=False, default=CurrencyType.USD)
    date_used = models.DateTimeField(null=True)
    has_been_used = models.BooleanField(default=False)
    customer = models.ForeignKey(Profile, related_name='booking_checkout_urls')

    def clean(self):
        if self.amount_withheld > self.price:
            raise ValidationError(_('Amount withheld cannot be greater than price'))

    @property
    def url(self):
        site = Site.objects.get_current()
        if self.key:
            return 'https://%s%s' % (site.domain, reverse('checkout', kwargs={'booking_checkout_url': self.key}))
        else:
            return None


class BookingRequest(BaseModel):
    photographer = models.ForeignKey('Photographer', null=True)
    booking = models.ForeignKey('Booking', null=True)
    did_accept_booking = models.BooleanField(default=False)
    did_reject_booking = models.BooleanField(default=False)

    def send_client_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking_request': self
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking_request/client_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking_request/client_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.booking.client.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def send_photographer_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking_request': self
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking_request/photographer_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking_request/photographer_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.photographer.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    @property
    def can_photographer_accept_booking(self):
        now_range = DateTimeTZRange(
            lower=timezone.now(),
            upper=timezone.now(),
        )
        if self.booking.photographer is not None:
            return False
        if self.did_accept_booking or self.did_reject_booking:
            return False
        if self.photographer.status != PhotographerStatusType.ACTIVE:
            return False
        # if Booking.objects.filter(photographer=self.photographer, time_range__fully_gt=now_range).count() >= MAX_BOOKINGS_PER_PHOTOGRAPHER:
        #     return False
        # if self.booking.time_range:
        #     # Check max
        #     day_range = DateTimeTZRange(
        #         lower=self.booking.time_range.lower.replace(hour=0, minute=0, second=0),
        #         upper=self.booking.time_range.lower.replace(hour=23, minute=59, second=59)
        #     )
        #     day_bookings = Booking.objects.filter(photographer=self.photographer, time_range__overlap=day_range)
        #     has_too_many_bookings = day_bookings.count() >= MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY
        #     if has_too_many_bookings:
        #         return False
        #
        #     # Check interval
        #     for booking in day_bookings:
        #         if booking.time_range:
        #             if abs((booking.time_range.lower - self.booking.time_range.lower).total_seconds()) < MIN_TIME_BETWEEN_BOOKINGS.total_seconds():
        #                 return False
        return True

    def accept_booking(self):
        assert self.can_photographer_accept_booking

        self.booking.photographer = self.photographer
        self.booking.save()
        # self.booking.do_hold_in_escrow = True
        # self.booking.is_sub_merchant_transaction = True
        # self.booking.save()
        # self.booking.sale(do_hold_in_escrow=True)
        self.did_accept_booking = True
        self.save()
        self.send_client_notification()
        if getattr(self.booking, 'foodbooking', None):
            self.booking.foodbooking.send_contact_notification()

    def reject_booking(self):
        assert(not self.did_accept_booking)
        assert(not self.did_reject_booking)
        self.did_reject_booking = True
        self.save()

    @property
    def url(self):
        site = Site.objects.get_current()
        return 'https://%s%s' % (site.domain, reverse('profiles:booking_request_detail', kwargs={'pk': self.pk}))

    @property
    def reject_booking_url(self):
        return reverse('photographers:reject_booking_request', kwargs={
            'pk': self.id,
            'token': self.url_token
        })

    @property
    def accept_booking_url(self):
        return reverse('photographers:accept_booking_request', kwargs={
            'pk': self.id,
            'token': self.url_token
        })

    @property
    def url_token(self):
        payload = {
            'pk': self.id
        }
        return signing.dumps(payload, salt=salt.BOOKING_REQUEST)

    @staticmethod
    def is_url_token_valid(token, pk):
        try:
            payload = signing.loads(token, salt=salt.BOOKING_REQUEST)
        except signing.BadSignature:
            return False
        return payload.get('pk') == pk

    @property
    def status(self):
        if self.did_reject_booking:
            return 'Rejected'
        elif self.booking.photographer is None:
            return 'Pending'
        elif self.booking.photographer == self.photographer:
            return 'Accepted'
        else:
            return 'Taken'

    @property
    def is_booking_available(self):
        return self.booking.photographer is None


class Booking(BraintreeTransaction):
    time_range = DateTimeRangeField(null=True, blank=True)
    service_type = enum.EnumField(ServiceType)
    message = models.TextField(blank=True)
    phone_number = models.CharField(max_length=500, blank=True)
    display_name = models.CharField(max_length=50, default='')
    is_sub_merchant_transaction = models.BooleanField(default=True)
    do_hold_in_escrow = models.BooleanField(default=True)
    do_send_client_confirmation = models.BooleanField(default=True)
    available_dates = models.TextField(default='')
    did_client_approve = models.BooleanField(default=False)
    did_client_reject = models.BooleanField(default=False)
    did_staff_approve = models.BooleanField(default=False)
    did_photographer_approve = models.BooleanField(default=False)
    did_photographer_complete = models.BooleanField(default=False)
    num_shots = models.IntegerField(null=True, default=None, blank=True)

    client_approval_date = models.DateTimeField(null=True, blank=True, default=None)
    client_rejection_date = models.DateTimeField(null=True, blank=True, default=None)
    staff_approval_date = models.DateTimeField(null=True, blank=True, default=None)
    photographer_approval_date = models.DateTimeField(null=True, blank=True, default=None)
    photographer_completion_date = models.DateTimeField(null=True, blank=True, default=None)
    contact_notification_date = models.DateTimeField(null=True, blank=True, default=None)

    contact_name = models.CharField(max_length=200, default='', blank=True)
    contact_email_address = models.EmailField(max_length=200, default='', blank=True)
    contact_phone_number = models.CharField(max_length=200, default='', blank=True)

    photographer = models.ForeignKey('Photographer', null=True, blank=True)
    client = models.ForeignKey(Profile)
    coupon = models.ForeignKey(Coupon, null=True)
    address = models.ForeignKey(Address)
    thread = models.ForeignKey(Thread)

    media_file = models.FileField(upload_to='photographers/booking/media_files', null=True, blank=True, storage=S3NamedMediaStorage())
    shot_list = models.FileField(upload_to='photographers/booking/shot_lists', null=True, blank=True, storage=S3MediaStorage())

    objects = models.GeoManager()

    @property
    def payout_status(self):
        payout_status = super(Booking, self).payout_status
        return payout_status if self.client_approval_date else '-'

    @property
    def local_start_time(self):
        if self.time_range:
            if self.address.time_zone:
                return self.time_range.lower.astimezone(self.address.time_zone)
            else:
                return self.time_range.lower
        return None

    @property
    def local_stop_time(self):
        if self.time_range:
            if self.address.time_zone:
                return self.time_range.upper.astimezone(self.address.time_zone)
            else:
                return self.time_range.upper
        return None

    @property
    def approved_images(self):
        return BookingImage.objects.filter(booking=self).exclude(did_client_reject=True)

    @property
    def service_type_label(self):
        return ServiceType.label(self.service_type)

    @property
    def time_period_label(self):
        return TimePeriodType.label(self.time_period)

    @property
    def total_amount(self):
        total_amount = self.amount
        for s in self.surcharges.all():
            total_amount += s.amount
        return total_amount

    @property
    def total_service_fee_amount(self):
        total_service_fee_amount = self.service_fee_amount
        for s in self.surcharges.all():
            total_service_fee_amount += s.service_fee_amount
        return total_service_fee_amount

    @property
    def invoice(self):
        return 'Total Cost: $%.2f' % self.amount

    @property
    def token(self):
        payload = {
            'pk': self.id
        }
        return signing.dumps(payload, salt=salt.BOOKING)

    @staticmethod
    def is_appointment_available(time_range, latitude, longitude):
        point = GEOSGeometry('POINT(%lf %lf)' % (longitude, latitude), srid=4326)
        bookings = Booking.objects.filter(time_range__overlap=time_range).filter(address__point__distance_lte=(point, D(mi=200)))
        return bookings.count() < 6

    @staticmethod
    def is_token_valid(token, pk):
        payload = dict()
        try:
            payload = signing.loads(token, salt=salt.BOOKING)
        except signing.BadSignature:
            return False
        return payload.get('pk') == pk

    def sale(self, do_hold_in_escrow=True, do_force_duplicate_check=False):
        assert not self.braintree_id
        assert (self.photographer and self.is_sub_merchant_transaction) or not self.is_sub_merchant_transaction
        
        # Check for duplicates
        if not IS_TEST_MODE or do_force_duplicate_check:
            min_time = timezone.now() - timedelta(seconds=BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS)
            try:
                assert not Booking.objects.filter(date_created__gt=min_time, amount=self.amount, service_fee_amount=self.service_fee_amount, client=self.client).exclude(braintree_id='').exists()
            except AssertionError:
                logger.exception('Failed to issue a duplicate transaction!')
                raise
    
        # Get that dollar...
        result = None
        self.do_hold_in_escrow = do_hold_in_escrow
        if not self.is_sub_merchant_transaction:
            self.service_fee_amount = None
            result = braintree.Transaction.sale({
                'customer_id': self.client.braintree_customer.braintree_id,
                'amount': Decimal(self.amount),
                'options': {
                    'submit_for_settlement': True,
                }
            })
        else:
            result = braintree.Transaction.sale({
                'customer_id': self.client.braintree_customer.braintree_id,
                'merchant_account_id': self.photographer.braintree_merchant_account.braintree_id,
                'amount': Decimal(self.amount),
                'service_fee_amount': Decimal(self.service_fee_amount),
                'options': {
                    'hold_in_escrow': do_hold_in_escrow,
                    'submit_for_settlement': True,
                }
            })
        try:
            assert result.is_success
        except AssertionError:
            logger.exception('Failed to create a transaction (%s)' % result.message)
            raise
        self.braintree_id = result.transaction.id
        self.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
        self.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None
        self.save()

        return result

    def send_date_update_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/date_update_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/date_update_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.contact_email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def send_contact_mistake_notification(self):
        assert self.contact_email_address
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/contact_mistake_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/contact_mistake_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.contact_email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def send_contact_notification(self):
        assert self.contact_email_address
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/contact_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/contact_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.contact_email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def get_display_name(self):
        if getattr(self, 'foodbooking', None):
            return self.foodbooking.get_display_name()
        return self.display_name if self.display_name else 'Photo Shoot in %s' % self.address.locality

    def on_photographer_completion(self):
        self.did_photographer_complete = True
        self.photographer_completion_date = timezone.now()
        self.save()

    def on_photographer_approval(self, do_commit=True, do_check_for_approval=True):
        if do_check_for_approval:
            assert not self.did_photographer_approve, 'Photographer has already approved'
        assert not self.did_staff_approve, 'Staff has already approved'
        assert not self.did_client_approve, 'Client has already approved'

        if not do_commit:
            return

        self.did_client_reject = False
        self.client_rejection_date = None
        self.did_photographer_approve = True
        self.photographer_approval_date = timezone.now()
        self.save()

        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/photographer_approval_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/photographer_approval_body.txt', context)
        # email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.client.email_address], bcc=STAFF_EMAILS)
        email_message = EmailMessage(subject=subject, body=body, from_email=SERVER_EMAIL, to=STAFF_EMAILS)
        email_message.send()

    def on_staff_approval(self, do_commit=True, do_check_for_approval=True):
        if do_check_for_approval:
            assert not self.did_staff_approve, 'Staff has already approved'
        assert self.did_photographer_approve, 'Photographer has not approved'
        assert self.num_shots and self.num_shots > 0, 'Invalid shot count'

        if not do_commit:
            return

        # Set staff approval
        self.did_client_approve = False
        self.did_client_reject = False
        self.client_approval_date = None
        self.client_rejection_date = None
        self.did_staff_approve = True
        self.staff_approval_date = timezone.now()

        # Create media file
        self.create_media_file()

        # Save updated state
        self.save()

        # Save notification
        self.send_staff_approval_notification()

    def send_staff_approval_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))
        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/staff_approval_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/staff_approval_body.txt', context)
        email_message = EmailMessage(
            subject=subject,
            body=body,
            from_email=DEFAULT_FROM_EMAIL,
            to=[self.client.email_address],
            cc=self.client.cc_recipients,
            bcc=STAFF_EMAILS
        )
        email_message.send()

    def on_client_approval(self, do_commit=True, do_check_for_approval=True):
        if do_check_for_approval:
            assert not self.did_client_approve, 'Client has already approved'
        assert not self.did_client_reject, 'Client has already rejected'
        assert self.did_staff_approve, 'Staff has not approved'
        assert self.num_shots and self.num_shots > 0, 'Invalid shot count'

        if not do_commit:
            return

        self.did_client_approve = True
        self.did_client_reject = False
        self.save()

        # Get pay scale
        assert self.num_shots
        pay_scale = None
        for p in PAY_SCALE:
            if self.num_shots in p.get('shot_range'):
                pay_scale = p
                break
        assert pay_scale

        self.amount = pay_scale.get('amount')
        self.service_fee_amount = pay_scale.get('service_fee_amount')
        self.client_approval_date = timezone.now()
        self.save()
        self.send_invoice()

        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/client_approval_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/client_approval_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL,
                                     to=[self.photographer.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def on_client_rejection(self, do_commit=True, do_check_for_rejection=True):
        if do_check_for_rejection:
            assert not self.did_client_reject, 'Client has already rejected'
        assert not self.did_client_approve, 'Client has already approved'
        assert self.did_staff_approve, 'Staff has not approved'

        if not do_commit:
            return

        self.did_staff_approve = False
        self.did_photographer_approve = False
        self.staff_approval_date = None
        self.photographer_approval_date = None
        self.did_client_approve = False
        self.did_client_reject = True
        self.client_rejection_date = timezone.now()
        self.save()

        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/client_rejection_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/client_rejection_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.photographer.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def create_surcharge(self):
        assert self.num_shots
        assert not BookingSurcharge.objects.filter(booking=self).exists()  # There can be only one...for now...

        # Get pay scale
        pay_scale = None
        for p in PAY_SCALE:
            if self.num_shots in p.get('shot_range'):
                pay_scale = p
                break
        assert pay_scale

        # Calculate surcharge amounts
        surcharge_amount = pay_scale.get('amount') - self.amount
        surcharge_service_fee_amount = pay_scale.get('service_fee_amount') - self.service_fee_amount
        if surcharge_amount <= 0:  # Don't charge people $0 dollars...
            return None
        assert surcharge_amount > 0
        assert surcharge_service_fee_amount >= 0

        # Create surcharge
        booking_surcharge = BookingSurcharge.objects.create(**{
            'booking': self,
            'amount': surcharge_amount,
            'service_fee_amount': surcharge_service_fee_amount,
        })
        booking_surcharge.sale()
        return booking_surcharge

    def create_booking_requests(self):
        booking_requests = list()
        if not self.address.point:
            try:
                raise AssertionError('Cannot send booking requests for NULL point')
            except AssertionError:
                logger.exception('Cannot send booking requests for NULL point')
            return booking_requests
        for photographer in Photographer.objects.filter(status=PhotographerStatusType.ACTIVE).filter(address__point__distance_lte=(self.address.point, D(mi=75))):
            data = {
                'photographer': photographer,
                'booking': self
            }
            if not BookingRequest.objects.filter(**data).exists():
                booking_requests.append(BookingRequest.objects.create(**data))
        return booking_requests

    def create_media_file(self):
        # Create name
        choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
        if getattr(self, 'foodbooking', None):
            zip_file_name = '%s-%s' % (self.foodbooking.restaurant_name, ''.join([random.choice(choices) for i in range(4)]))
        else:
            zip_file_name = ''.join([random.choice(choices) for i in range(16)])
        zip_file_name = slugify(zip_file_name)

        zip_file_path = os.path.join(RUN_DIR, zip_file_name + '.zip')
        with ZipFile(zip_file_path, 'w', allowZip64=True) as zip_file:
            for album in self.albums.all():
                names = {}
                for image in album.images.all():
                    name = image.name
                    if name in names:
                        names[name] += 1
                        name = '%s-%d' % (image.name, names[name])
                    else:
                        names[name] = 0
                    zip_file.writestr('%s/%s%s' % (slugify(album.name), slugify(name), os.path.splitext(image.file_name)[1]), force_bytes(image.image.read()))
        self.media_file.save(os.path.basename(zip_file_path), File(open(zip_file_path)), save=True)

        return zip_file_path

    @staticmethod
    def create_csv_file(bookings):
        field_names = (
            'restaurant_name',
            'contact_email_address',
            'contact_phone_number',
            'contact_name',

            'street_address',
            'locality',
            'region',
            'postal_code',

            'photographer',
            'appointment_time',

            'num_images',
            'num_items_shot',

            'did_photographer_approve',
            'did_staff_approve',
            'did_client_approve',
            'did_client_reject',

            'photographer_approval_date',
            'staff_approval_date',
            'client_approval_date',
            'client_rejection_date',

            'amount',
            # 'payment_date',

            'menu_url',
            'shotzu_url'
        )

        # Get file name
        choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
        csv_file_name = ''.join([random.choice(choices) for i in range(16)])
        csv_file_path = os.path.join(RUN_DIR, csv_file_name + '.csv')
        current_site = Site.objects.get_current()

        with open(csv_file_path, 'wb') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=field_names)
            writer.writeheader()
            for booking in bookings:
                if booking.transaction_status == BraintreeTransactionStatusType.SETTLED or \
                                booking.transaction_status == BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT:
                    # transaction = booking.update_braintree_data()
                    amount = '$%.2f' % booking.amount
                    # payment_date = transaction.created_at
                else:
                    amount = ''
                    # payment_date = ''
                try:
                    restaurant_name = booking.foodbooking.restaurant_name
                    menu_url = booking.foodbooking.menu_url if booking.foodbooking.menu_url else ''
                except FoodBooking.DoesNotExist:
                    restaurant_name = ''
                    menu_url = ''
                writer.writerow({
                    'restaurant_name': restaurant_name.encode('utf-8'),
                    'contact_email_address': booking.contact_email_address,
                    'contact_phone_number': booking.contact_phone_number,
                    'contact_name': booking.contact_name,

                    'street_address': booking.address.formatted_street_address,
                    'locality': booking.address.formatted_locality,
                    'region': booking.address.formatted_region,
                    'postal_code': booking.address.formatted_postal_code,

                    'photographer': booking.photographer.display_name if booking.photographer else '',
                    'appointment_time': booking.time_range.lower.isoformat() if booking.time_range else '',

                    'num_images': booking.images.all().count(),
                    'num_items_shot': booking.num_shots,

                    'did_photographer_approve': booking.did_photographer_approve,
                    'did_staff_approve': booking.did_staff_approve,
                    'did_client_approve': booking.did_client_approve,
                    'did_client_reject': booking.did_client_reject,

                    'photographer_approval_date': booking.photographer_approval_date,
                    'staff_approval_date': booking.staff_approval_date,
                    'client_approval_date': booking.client_approval_date,
                    'client_rejection_date': booking.client_rejection_date,

                    'amount': amount,
                    # 'payment_date': payment_date,

                    'shotzu_url': 'https://%s%s' % (current_site.domain, reverse('profiles:booking_detail', kwargs={'pk': booking.id})),
                    'menu_url': menu_url,
                })

        return csv_file_path

    def send_client_confirmation(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self
        })
        context.update(constants(None))
        subject = loader.render_to_string('photographers/emails/booking_request_confirmation_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking_request_confirmation_body.txt', context)
        send_mail(subject=subject, message=body, from_email=DEFAULT_FROM_EMAIL, recipient_list=[self.client.email_address])

    def send_photographer_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.photographer.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def send_invoice(self):
        # Get transaction info
        try:
            braintree_customer = BraintreeCustomer.objects.get(user=self.client.user)
        except ObjectDoesNotExist:
            return

        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
            'total_cost': self.amount,
            'credit_card': braintree_customer.default_credit_card,
            'payment_date': self.client_approval_date + POSTMATES_BOOKING_CHARGE_DELAY
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/invoice_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/invoice_body.txt', context)
        email_message = EmailMessage(
            subject=subject,
            body=body,
            from_email=DEFAULT_FROM_EMAIL,
            to=[self.client.email_address],
            cc=self.client.cc_recipients,
            bcc=STAFF_EMAILS)
        email_message.send()

    def send_receipt(self):
        # Get transaction info
        transaction = braintree.Transaction.find(self.braintree_id)

        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
            'total_cost': self.amount,
            'transaction': transaction,
            'credit_card': transaction.credit_card_details,
            'payment_date': transaction.created_at
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/receipt_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/receipt_body.txt', context)
        email_message = EmailMessage(
            subject=subject,
            body=body,
            from_email=DEFAULT_FROM_EMAIL,
            to=[self.client.email_address],
            cc=self.client.cc_recipients,
            bcc=STAFF_EMAILS
        )
        email_message.send()

    def send_photographer_completion(self):
        from gouda.urlresolvers import get_absolute_url
        url = get_absolute_url('photographers:photographer_completion', kwargs={'token': signing.dumps({
            'booking': self.id
        })})
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
            'url': url,
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking/photographer_completion_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking/photographer_completion_body.txt', context)
        email_message = EmailMessage(
            subject=subject,
            body=body,
            from_email=DEFAULT_FROM_EMAIL,
            to=[self.photographer.email_address],
            bcc=STAFF_EMAILS
        )
        email_message.send()

    def submit_for_settlement(self):
        assert self.num_shots, 'Invalid number of shots'
        assert self.num_shots > 0, 'Invalid number of shots'
        assert self.did_photographer_approve, 'Photographer has not approved'
        assert self.photographer_approval_date, 'Photographer has not approved'
        assert self.did_staff_approve, 'Staff has not approved'
        assert self.staff_approval_date, 'Staff has not approved'
        assert self.did_client_approve, 'Client has not approved'
        assert self.client_approval_date, 'Client has not approved'
        self.sale(do_hold_in_escrow=False)
        self.send_receipt()

    def __unicode__(self):
        return self.get_display_name()
        # return '%s -> %s - %s' % (self.client.full_name, self.photographer.full_name if self.photographer else '?', self.time_range.lower.strftime('%m/%d/%Y'))


class FoodBooking(Booking):
    restaurant_name = models.CharField(max_length=200)
    items = ArrayField(models.CharField(max_length=200), blank=True)
    is_full_menu_shoot = models.BooleanField(default=False)
    is_confirmed_with_restaurant = models.BooleanField(default=False)
    menu_url = models.URLField(default='', blank=True)

    def get_display_name(self):
        return self.display_name if self.display_name else self.restaurant_name

    def send_items_notification(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/food_booking/items_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/food_booking/items_notification_body.txt', context)
        if self.photographer:
            email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.photographer.email_address], bcc=STAFF_EMAILS)
        else:
            email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=STAFF_EMAILS)
        email_message.send()

    def send_contact_notification(self):
        current_site = Site.objects.get_current()
        toks = self.contact_name.split()
        if len(toks) > 0:
            first_name = toks[0]
        else:
            first_name = self.contact_name
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking': self,
            'contact': {
                'first_name': first_name
            }
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/food_booking/merchant_notification_subject.txt',
                                          context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/food_booking/merchant_notification_body.txt',
                                       context)
        email_message = EmailMessage(
            subject=subject,
            body=body,
            from_email=DEFAULT_FROM_EMAIL,
            to=[
                self.client.email_address,
                self.contact_email_address,
                self.photographer.email_address,
                'julian@shotzu.com'
            ],
            bcc=STAFF_EMAILS
        )
        if self.shot_list:
            email_message.attach('shot_list.csv', self.shot_list.read(), 'text/csv')
        email_message.send()
        self.contact_notification_date = timezone.now()
        self.save()
            # def send_photographer_notification(self):
    #     current_site = Site.objects.get_current()
    #     context = dict({
    #         'domain': current_site.domain,
    #         'site_name': current_site.name,
    #         'protocol': 'http',
    #         'booking': self
    #     })
    #     context.update(constants(None))
    #
    #     # Render and send notification email
    #     subject = loader.render_to_string('photographers/emails/booking_notification_subject.txt', context)
    #     subject = ''.join(subject.splitlines())
    #     body = loader.render_to_string('photographers/emails/food_booking_notification_body.txt', context)
    #     email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.photographer.email_address], bcc=STAFF_EMAILS)
    #     email_message.send()


class BulkBooking(BaseModel):
    client = models.ForeignKey(Profile, null=True)
    bookings = models.ManyToManyField(Booking, related_name='bulk_bookings')
    file = models.FileField(upload_to='booking_files/')
    message = models.TextField(blank=True)

    field_names = (
        'menu_url',
        'restaurant_name',
        'contact_email_address',
        'contact_phone_number',
        'contact_full_name',
        'street_address',
        'sub_premise',
        'locality',
        'region',
        'postal_code',
        'available_dates',
    )
    optional_field_names = (
        'contact_phone_number',
        'sub_premise',
        'are_menu_edits_needed',
        'uploaded_menu_url',
        'menu_url',
        'description_of_menu_edits',
    )


    # field_names = (
    #     'restaurant_name',
    #     'contact_email_address',
    #     'contact_phone_number',
    #     'contact_full_name',
    #     'street_address',
    #     'locality',
    #     'region',
    #     'postal_code',
    #     'photographer',
    #     'appointment_time',
    #     'num_images',
    # )
    # optional_field_names = (
    #     'contact_phone_number',
    # )

    def send_client_confirmation(self):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'bulk_booking': self
        })
        context.update(constants(None))
        subject = loader.render_to_string('photographers/emails/booking_request_confirmation_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/bulk_booking_confirmation_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.client.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def get_total_cost_from_file(self):
        bookings = self.get_booking_data_from_file()
        total_cost = 0
        for booking in bookings:
            total_cost += booking.get('amount')
        return total_cost

    def get_booking_data_from_file(self, field_names=None):
        key = None
        if self.id:
            key = 'bulk_booking:%d:file:%s' % (self.id, self.file.name)
            bookings = cache.get(key, None)
            if bookings:
                return bookings
        if field_names:
            fieldnames = field_names
        else:
            fieldnames = BulkBooking.field_names
        reader = csv.DictReader(self.file, delimiter=','.__str__(), fieldnames=fieldnames)
        bookings = list()
        for row in reader:
            if row.get('contact_email_address') == 'Email Address':
                continue
            # booking = {
            #     'num_shots': row.get('num_images', '').strip(),
            #     'appointment_time': row.get('appointment_time', '').strip(),
            #     'service_type': ServiceType.FOOD,
            #     'restaurant_name': row.get('restaurant_name').strip(),
            #     'is_full_menu_shoot': True,
            #     'is_confirmed_with_restaurant': True,
            #     'items': list(),
            #     'amount': PAY_SCALE[0].get('amount'),
            #     'service_fee_amount': PAY_SCALE[0].get('service_fee_amount'),
            #     'contact': {
            #         'email_address': row.get('contact_email_address').strip(),
            #         'full_name': row.get('contact_full_name').strip(),
            #         'phone_number': row.get('contact_phone_number').strip()
            #     },
            #     'address': {
            #         'street_address': row.get('street_address').strip(),
            #         'locality': row.get('locality').strip(),
            #         'region': row.get('region').strip(),
            #         'postal_code': row.get('postal_code').strip(),
            #         'country': 'US',
            #     },
            # }
            booking = {
                'num_shots': row.get('num_images', '').strip(),
                'appointment_time': row.get('appointment_time', '').strip(),
                'menu_url': row.get('menu_url', '').strip(),
                'service_type': ServiceType.FOOD,
                'restaurant_name': row.get('restaurant_name').strip(),
                'is_full_menu_shoot': True,
                'is_confirmed_with_restaurant': True,
                'items': list(),
                'amount': PAY_SCALE[0].get('amount'),
                'service_fee_amount': PAY_SCALE[0].get('service_fee_amount'),
                'contact': {
                    'email_address': row.get('contact_email_address').strip(),
                    'full_name': row.get('contact_full_name').strip(),
                    'phone_number': row.get('contact_phone_number').strip()
                },
                'address': {
                    'street_address': row.get('street_address').strip(),
                    'sub_premise': row.get('sub_premise', '').strip(),
                    'locality': row.get('locality').strip(),
                    'region': row.get('region').strip(),
                    'postal_code': row.get('postal_code').strip(),
                    'country': 'US',
                },
                'available_dates': row.get('available_dates', '').strip(),

            }
            bookings.append(booking)

        if key:
            cache.set(key, bookings, 3600)
        return bookings

    def create_bookings_from_file(self, field_names=None):
        booking_data = self.get_booking_data_from_file(field_names)
        for b in booking_data:
            if b.get('service_type') == ServiceType.FOOD:
                if b.get('appointment_time'):
                    start_time = parse_date(b.get('appointment_time'))
                    time_range = DateTimeTZRange(
                        lower=start_time,
                        upper=start_time + timedelta(seconds=3600)
                    )
                else:
                    time_range = None
                food_booking = FoodBooking.objects.create(**{
                    'thread': Thread.objects.create(),
                    'client': self.client,
                    'message': self.message,
                    'service_type': ServiceType.FOOD,
                    'do_send_client_confirmation': False,
                    'amount': b.get('amount'),
                    'service_fee_amount': b.get('service_fee_amount'),

                    'restaurant_name': b.get('restaurant_name'),
                    'contact_phone_number': b.get('contact').get('phone_number'),
                    'contact_email_address': b.get('contact').get('email_address'),
                    'contact_name': b.get('contact').get('full_name'),
                    'is_full_menu_shoot': b.get('is_full_menu_shoot'),
                    'is_confirmed_with_restaurant': b.get('is_confirmed_with_restaurant'),
                    'items': b.get('items'),

                    'address': Address.objects.create(**{
                        'full_name': b.get('restaurant_name'),
                        'street_address': b.get('address').get('street_address'),
                        'sub_premise': b.get('address').get('sub_premise', ''),
                        'postal_code': b.get('address').get('postal_code'),
                        'locality': b.get('address').get('locality'),
                        'region': b.get('address').get('region'),
                        'country': b.get('address').get('country'),
                    }),
                    'available_dates': b.get('available_dates', '').strip(),
                    'time_range': time_range
                })

                self.bookings.add(food_booking)
        return self.bookings.all()


    @property
    def total_cost(self):
        total_cost = 0
        for booking in self.bookings.all():
            total_cost += booking.amount
        return total_cost

    @property
    def invoice(self):
        return '# of Bookings: %d\nTotal Cost: $%.2f' % (self.bookings.all().count(), self.total_cost)


class BookingAlbum(BaseModel):
    booking = models.ForeignKey('Booking', related_name='albums')
    images = models.ManyToManyField('BookingImage', related_name='albums', blank=True)
    name = models.CharField(max_length=500)
    width = models.IntegerField(null=True, blank=True)
    height = models.IntegerField(null=True, blank=True)


class BookingImage(BaseModel):
    booking = models.ForeignKey('Booking', related_name='images')
    image = ThumbnailerImageField(upload_to='booking_images/', validators=[validate_image])
    name = models.CharField(max_length=500)
    did_client_approve = models.BooleanField(default=False)
    did_client_reject = models.BooleanField(default=False)
    comment = models.TextField(default='', blank=True)

    def send_rejection_notification(self, profile):
        key = 'booking_image:%d:rejection_notification' % self.id
        if cache.get(key, None):
            return
        cache.set(key, True, 86400)

        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking_image': self,
            'booking': self.booking,
            'client': profile,
            'photographer': self.booking.photographer
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking_image/rejection_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking_image/rejection_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.booking.photographer.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    def send_comment_notification(self, profile):
        current_site = Site.objects.get_current()
        context = dict({
            'domain': current_site.domain,
            'site_name': current_site.name,
            'protocol': 'http',
            'booking_image': self,
            'booking': self.booking,
            'client': profile,
            'photographer': self.booking.photographer
        })
        context.update(constants(None))

        # Render and send notification email
        subject = loader.render_to_string('photographers/emails/booking_image/comment_notification_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/booking_image/comment_notification_body.txt', context)
        email_message = EmailMessage(subject=subject, body=body, from_email=DEFAULT_FROM_EMAIL, to=[self.booking.photographer.email_address], bcc=STAFF_EMAILS)
        email_message.send()

    @property
    def file_name(self):
        root, ext = os.path.splitext(self.image.name)
        return '%s%s' % (self.name, ext)


class Tip(BraintreeTransaction):
    booking = models.ForeignKey('Booking')
    message = models.TextField(blank=True)

    @property
    def invoice(self):
        return 'Tip Amount: $%.2f' % self.amount

    @property
    def client(self):
        return self.booking.client

    @property
    def photographer(self):
        return self.booking.photographer


class BookingSurcharge(BraintreeTransaction):
    booking = models.ForeignKey('Booking', related_name='surcharges')

    def sale(self, do_force_duplicate_check=False):
        assert not self.braintree_id
        # Check for duplicates
        if not IS_TEST_MODE or do_force_duplicate_check:
            min_time = timezone.now() - timedelta(seconds=BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS)
            try:
                assert not BookingSurcharge.objects.filter(date_created__gt=min_time, amount=self.amount, service_fee_amount=self.service_fee_amount, booking=self.booking).exclude(braintree_id='').exists()
            except AssertionError:
                logger.exception('Failed to issue a duplicate transaction!')
                raise

        result = braintree.Transaction.sale({
            'customer_id': self.booking.client.braintree_customer.braintree_id,
            'merchant_account_id': self.booking.photographer.braintree_merchant_account.braintree_id,
            'amount': Decimal(self.amount),
            'service_fee_amount': Decimal(self.service_fee_amount),
            'options': {
                'hold_in_escrow': False,
                'submit_for_settlement': True,
            }
        })

        try:
            assert result.is_success, result.message
        except AssertionError:
            logger.exception('Failed to create a booking surcharge (%s)' % result.message)
            raise

        self.braintree_id = result.transaction.id
        self.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
        self.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None
        self.save()


from gouda.photographers.signals import *
