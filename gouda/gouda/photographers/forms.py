from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import csv
import pytz
import logging
from datetime import date
from datetime import datetime
from datetime import timedelta
from datetime import time
from decimal import *
from django_countries.fields import countries

import pytz
from django import forms
from django.conf import settings
from django.forms import ValidationError
from django.forms.utils import ErrorList
from django.utils.translation import ugettext as _
from gouda.addresses.forms import AddressForm
from gouda.addresses.models import Address
from gouda.customers.forms import BraintreeMerchantAccountForm
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import Coupon
from gouda.forms.widgets import CheckboxInput
from gouda.forms.widgets import DateInput
from gouda.forms.widgets import FileInput
from gouda.forms.widgets import KendoDatePicker
from gouda.forms.widgets import KendoDropDownList
from gouda.forms.widgets import KendoTextInput
from gouda.forms.widgets import KendoTextarea
from gouda.forms.widgets import KendoTimePicker
from gouda.forms.widgets import TextInput
from gouda.forms.widgets import Textarea
from gouda.forms.widgets import TimeInput
from gouda.postmaster.models import Thread
from gouda.photographers.models import Booking
from gouda.photographers.models import BulkBooking
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import PhotographerStatusType
from gouda.photographers.models import Review
from gouda.photographers.models import ServiceType
from gouda.photographers.models import TimePeriodType
from gouda.photographers.models import Tip
from gouda.photographers.tasks import on_staff_approval
from gouda.profiles.models import Profile
from gouda.users.models import User
from localflavor.us.us_states import STATE_CHOICES
from psycopg2.extras import DateTimeTZRange
from dateutil.parser import parse as parse_date
from timezone_field import TimeZoneFormField
from pytz import timezone
from django.contrib.postgres.forms import ranges
from django.utils.encoding import force_str, force_text, smart_text
from dateutil.tz import gettz
from pytz import timezone

logger = logging.getLogger(__name__)

PAY_SCALE = getattr(settings, 'PAY_SCALE')
PHOTOGRAPHER_PRICE = getattr(settings, 'PHOTOGRAPHER_PRICE')
PHOTOGRAPHER_AMOUNT_WITHHELD = getattr(settings, 'PHOTOGRAPHER_AMOUNT_WITHHELD')
CONDITION_CHOICES = PhotographerStatusType.choices()
max_enum_value = 0
for choice in CONDITION_CHOICES:
    max_enum_value = max(max_enum_value, choice[0])
CONDITION_CHOICES.append((max_enum_value + 1, 'All'))
CONDITION_CHOICES.sort(key=lambda choice: choice[1], reverse=True)
# STATE_CHOICES = [state_choice for state_choice in STATE_CHOICES if state_choice[0] in ['MD', 'VA', 'DC']]
SERVICE_TYPE_CHOICES = list(ServiceType.choices())
SERVICE_TYPE_CHOICES.insert(1, SERVICE_TYPE_CHOICES.pop(ServiceType.FOOD))
IS_TEST_MODE = getattr(settings, 'IS_TEST_MODE')

TZINFOS = {
    'CST': gettz("US/Central"),
    'CDT': gettz("US/Central"),
    'EDT': gettz("US/Eastern"),
    'EST': gettz("US/Eastern"),
    'MST': gettz("US/Mountain"),
    'MDT': gettz("US/Mountain"),
    'PDT': gettz("US/Pacific"),
    'PST': gettz("US/Pacific"),
}


class DateTimeField(forms.DateTimeField):
    def strptime(self, value, format):
        return parse_date(force_str(value), ignoretz=False, tzinfos=TZINFOS).astimezone(pytz.utc)


class DateTimeRangeField(ranges.DateTimeRangeField):
    base_field = DateTimeField


class ReviewForm(forms.Form):
    booking = forms.ModelChoiceField(queryset=Booking.objects.all())
    reviewer = forms.ModelChoiceField(queryset=Profile.objects.all())
    title = forms.CharField(widget=KendoTextInput('title'))
    content = forms.CharField(widget=KendoTextarea('content'))
    rating = forms.IntegerField(widget=KendoDropDownList('rating', choices=[(i, '%d star%s' % (i, 's' if i > 1 else '')) for i in range(1, 6)], attrs={}))
    amount = forms.IntegerField(required=False, widget=KendoTextInput('amount', is_required=False, attrs={'type': 'number', 'ng-min': 1, 'ng-pattern': '/^\d+$/'}))

    def save(self):
        review_data = self.cleaned_data.copy()
        amount = review_data.pop('amount', None)
        review = Review.objects.create(**review_data)
        if amount:
            tip = Tip.objects.create(**{
                'booking': self.cleaned_data.get('booking'),
                'amount': amount,
                'service_fee_amount': 0
            })
        return review


class TipDetailsForm(forms.Form):
    amount = forms.IntegerField(widget=KendoTextInput('amount', attrs={'type': 'number'}))
    message = forms.CharField(widget=KendoTextarea('message', is_required=False))


class BookingUpdateForm(forms.ModelForm):
    date = forms.DateField(widget=DateInput(attrs={}), required=False)
    start_time = forms.TimeField(widget=TimeInput(attrs={}), required=False)
    stop_time = forms.TimeField(widget=TimeInput(attrs={}), required=False)

    def clean_date(self):
        data = self.cleaned_data['date']
        if data and data < date.today():
            raise forms.ValidationError('Invalid date')
        return data

    # def clean_start_time(self):
    #     start_time = self.cleaned_data['start_time']
    #     if start_time and (start_time < time(8, 0, 0) or start_time > time(13, 0, 0)):
    #         raise forms.ValidationError('Invalid start time')
    #     return start_time
    #
    # def clean_stop_time(self):
    #     stop_time = self.cleaned_data['stop_time']
    #     if stop_time and (stop_time < time(11, 0, 0) or stop_time > time(16, 0, 0)):
    #         raise forms.ValidationError('Invalid end time')
    #     return stop_time

    def clean(self):
        assert self.instance
        cleaned_data = super(BookingUpdateForm, self).clean()

        # Add TZ info
        booking_date = cleaned_data.get('date')
        start_time = cleaned_data.get('start_time')
        stop_time = cleaned_data.get('stop_time')
        time_zone = self.instance.address.time_zone
        if isinstance(time_zone, unicode):
            time_zone = timezone(time_zone)
        if booking_date and start_time and stop_time and time_zone:
            cleaned_data['start_time'] = time_zone.localize(datetime(booking_date.year, booking_date.month, booking_date.day, start_time.hour, start_time.minute, start_time.second))
            cleaned_data['stop_time'] = time_zone.localize(datetime(booking_date.year, booking_date.month, booking_date.day, stop_time.hour, stop_time.minute, stop_time.second))
            if cleaned_data['stop_time'] < cleaned_data['start_time']:
                cleaned_data['stop_time'] = cleaned_data['stop_time'] + timedelta(days=1)

            # Make sure date is valid
            time_range = DateTimeTZRange(lower=self.cleaned_data['start_time'], upper=self.cleaned_data['stop_time'])
            if self.instance.address.point:
                if not Booking.is_appointment_available(time_range, self.instance.address.point.y, self.instance.address.point.x):
                    raise forms.ValidationError('Invalid time range')

        return cleaned_data

    def save(self, commit=True):
        booking = super(BookingUpdateForm, self).save(commit=False)
        start_time = self.cleaned_data.get('start_time')
        stop_time = self.cleaned_data.get('stop_time')
        if start_time and stop_time:
            booking.time_range = DateTimeTZRange(lower=start_time, upper=stop_time)
            booking.save()
        return booking

    class Meta:
        model = Booking
        fields = [
        ]


class ScheduleBookingForm(forms.ModelForm):
    date = forms.DateField(widget=DateInput(attrs={}))
    start_time = forms.TimeField(widget=TimeInput(attrs={}))
    stop_time = forms.TimeField(widget=TimeInput(attrs={}))
    latitude = forms.FloatField(required=False, widget=forms.HiddenInput(attrs={}))
    longitude = forms.FloatField(required=False, widget=forms.HiddenInput(attrs={}))
    time_zone = TimeZoneFormField(required=False, widget=forms.HiddenInput(attrs={}))

    def clean_latitude(self):
        latitude = self.cleaned_data['latitude']
        if latitude:
            if latitude < -90 or latitude > 90:
                raise forms.ValidationError('Invalid latitude (%d)' % latitude)
        return latitude

    def clean_longitude(self):
        longitude = self.cleaned_data['longitude']
        if longitude:
            if longitude < -180 or longitude > 180:
                raise forms.ValidationError('Invalid longitude (%d)' % longitude)
        return longitude

    def clean_date(self):
        data = self.cleaned_data['date']
        if data < date.today():
            raise forms.ValidationError('Invalid date')
        return data

    def clean_start_time(self):
        start_time = self.cleaned_data['start_time']
        if start_time < time(8, 0, 0) or start_time > time(17, 0, 0):
            raise forms.ValidationError('Invalid start time')
        return start_time

    def clean_stop_time(self):
        stop_time = self.cleaned_data['stop_time']
        if stop_time < time(11, 0, 0) or stop_time > time(20, 0, 0):
            raise forms.ValidationError('Invalid end time')
        return stop_time

    def clean(self):
        assert self.instance
        cleaned_data = super(ScheduleBookingForm, self).clean()

        # Add TZ info
        booking_date = cleaned_data.get('date')
        start_time = cleaned_data.get('start_time')
        stop_time = cleaned_data.get('stop_time')
        time_zone = cleaned_data.get('time_zone') or self.instance.address.time_zone
        if not time_zone:
            time_zone = pytz.utc
        if booking_date and start_time and stop_time and time_zone:
            cleaned_data['start_time'] = time_zone.localize(datetime(booking_date.year, booking_date.month, booking_date.day, start_time.hour, start_time.minute, start_time.second))
            cleaned_data['stop_time'] = time_zone.localize(datetime(booking_date.year, booking_date.month, booking_date.day, stop_time.hour, stop_time.minute, stop_time.second))
            if cleaned_data['stop_time'] < cleaned_data['start_time']:
                cleaned_data['stop_time'] = cleaned_data['stop_time'] + timedelta(days=1)

            # Make sure date is valid
            time_range = DateTimeTZRange(lower=self.cleaned_data['start_time'], upper=self.cleaned_data['stop_time'])
            if self.instance.address.point:
                if not Booking.is_appointment_available(time_range, self.instance.address.point.y, self.instance.address.point.x):
                    raise forms.ValidationError('Invalid time range')

        return cleaned_data

    def save(self, commit=True):
        booking = super(ScheduleBookingForm, self).save(commit=False)
        booking.time_range = DateTimeTZRange(lower=self.cleaned_data['start_time'], upper=self.cleaned_data['stop_time'])
        if self.cleaned_data.get('longitude') and self.cleaned_data.get('latitude'):
            booking.address.point = 'POINT(%f %f)' % (self.cleaned_data.get('longitude'), self.cleaned_data.get('latitude'))
        if self.cleaned_data.get('time_zone'):
            booking.address.time_zone = self.cleaned_data.get('time_zone')
        booking.address.save()
        booking.save()
        booking.send_date_update_notification()
        booking.create_booking_requests()
        return booking

    class Meta:
        model = Booking
        fields = [
        ]


class PhotographerApprovalForm(forms.ModelForm):
    def clean_num_shots(self):
        num_shots = self.cleaned_data.get('num_shots')
        if not num_shots or num_shots < 0:
            raise forms.ValidationError('Invalid number of shots. Must be greater than 0.')
        return num_shots

    def clean_did_photographer_approve(self):
        did_photographer_approve = self.cleaned_data.get('did_photographer_approve')
        if not did_photographer_approve:
            raise forms.ValidationError('Invalid value for photographer approval')
        return did_photographer_approve

    def clean(self):
        cleaned_data = super(PhotographerApprovalForm, self).clean()
        if self.instance:
            try:
                self.instance.on_photographer_approval(do_commit=False)
            except AssertionError as e:
                raise forms.ValidationError(e.message)
        # if self.instance and self.instance.did_photographer_approve:
        #     raise forms.ValidationError('Photographer has already approved.')
        # if self.instance and self.instance.did_client_approve:
        #     raise forms.ValidationError('Client has already approved.')
        return cleaned_data

    def save(self, commit=True):
        booking = super(PhotographerApprovalForm, self).save(commit=True)
        booking.on_photographer_approval(do_check_for_approval=False)
        return booking

    class Meta:
        model = Booking
        fields = [
            'did_photographer_approve',
            'num_shots'
        ]
        widgets = {
            'did_photographer_approve': forms.HiddenInput(),
            'num_shots': TextInput(attrs={
                'placeholder': 'How many shots did you do for this shoot?',
                'data-parsley-min': '1',
                'type': 'number'
            })
        }


class StaffApprovalForm(forms.ModelForm):
    def clean_did_staff_approve(self):
        did_staff_approve = self.cleaned_data.get('did_staff_approve')
        if not did_staff_approve:
            raise forms.ValidationError('Invalid value for staff approval')
        return did_staff_approve

    def clean(self):
        cleaned_data = super(StaffApprovalForm, self).clean()
        if self.instance:
            try:
                self.instance.on_staff_approval(do_commit=False)
            except AssertionError as e:
                raise forms.ValidationError(e.message)
            # if not self.instance.did_photographer_approve:
            #     raise forms.ValidationError('Photographer has not approved.')
            # if self.instance.did_staff_approve:
            #     raise forms.ValidationError('Staff has already approved.')
            # if not self.instance.num_shots or self.instance.num_shots < 0:
            #     raise forms.ValidationError('Invalid shot count')
        return cleaned_data

    def save(self, commit=True):
        booking = super(StaffApprovalForm, self).save(commit=False)
        if IS_TEST_MODE:
            on_staff_approval(booking.id)
        else:
            on_staff_approval.delay(booking.id)
        # booking.on_staff_approval(do_check_for_approval=False)
        return booking

    class Meta:
        model = Booking
        fields = [
            'did_staff_approve'
        ]
        widgets = {
            'did_staff_approve': forms.HiddenInput(),
        }


class ClientApprovalForm(forms.ModelForm):
    def clean_did_client_approve(self):
        did_client_approve = self.cleaned_data.get('did_client_approve')
        if not did_client_approve:
            raise forms.ValidationError('Invalid value for client approval')
        return did_client_approve

    def clean(self):
        cleaned_data = super(ClientApprovalForm, self).clean()
        if self.instance:
            try:
                self.instance.on_client_approval(do_commit=False)
            except AssertionError as e:
                raise forms.ValidationError(e.message)
            # if not self.instance.did_staff_approve:
            #     raise forms.ValidationError('Staff has not approved.')
            # if self.instance.did_client_approve:
            #     raise forms.ValidationError('Client has already approved.')
            # if not self.instance.num_shots or self.instance.num_shots < 0:
            #     raise forms.ValidationError('Invalid shot count.')
        return cleaned_data

    def save(self, commit=True):
        booking = super(ClientApprovalForm, self).save(commit=commit)
        booking.on_client_approval(do_check_for_approval=False)
        return booking

    class Meta:
        model = Booking
        fields = [
            'did_client_approve'
        ]
        widgets = {
            'did_client_approve': forms.HiddenInput(),
        }


class ClientRejectionForm(forms.ModelForm):
    def clean_did_client_reject(self):
        did_client_reject = self.cleaned_data.get('did_client_reject')
        if not did_client_reject:
            raise forms.ValidationError('Invalid value for client rejection')
        return did_client_reject

    def clean(self):
        cleaned_data = super(ClientRejectionForm, self).clean()
        if self.instance:
            try:
                self.instance.on_client_rejection(do_commit=False)
            except AssertionError as e:
                raise forms.ValidationError(e.message)
            # if not self.instance.did_staff_approve:
            #     raise forms.ValidationError('Staff has not approved.')
            # if self.instance.did_client_reject:
            #     raise forms.ValidationError('Client has already rejected.')
            # if self.instance.did_client_approve:
            #     raise forms.ValidationError('Client has already approved.')
        return cleaned_data

    def save(self, commit=True):
        booking = super(ClientRejectionForm, self).save(commit=commit)
        booking.on_client_rejection(do_check_for_rejection=False)
        return booking

    class Meta:
        model = Booking
        fields = [
            'did_client_reject'
        ]
        widgets = {
            'did_client_reject': forms.HiddenInput(),
        }


class BookingDetailsForm(forms.Form):
    display_name = forms.CharField(widget=TextInput(attrs={'data-parsley-maxlength': 50}))
    service_type = forms.IntegerField(widget=forms.Select(choices=ServiceType.choices(), attrs={}))
    date = forms.DateField(widget=DateInput(attrs={}))
    start_time = forms.TimeField(widget=TimeInput(attrs={}))
    end_time = forms.TimeField(widget=TimeInput(attrs={}))
    phone_number = forms.CharField(widget=TextInput())

    def clean_date(self):
        data = self.cleaned_data['date']
        if data < date.today():
            raise forms.ValidationError('Invalid date')
        return data


class BookingAddressForm(AddressForm):
    def __init__(self, *args, **kwargs):
        super(BookingAddressForm, self).__init__(*args, **kwargs)
        self.fields.get('country').widget = forms.HiddenInput()
        self.fields.get('region').widget = forms.Select(choices=STATE_CHOICES)
        self.fields.get('postal_code').widget = TextInput()


class BookingForm(forms.ModelForm):
    date = forms.DateField(required=True)
    start_time = forms.TimeField(required=True)
    end_time = forms.TimeField(required=True)

    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, instance=None, amount=PHOTOGRAPHER_PRICE, amount_withheld=PHOTOGRAPHER_AMOUNT_WITHHELD, coupon_id=None):
        self.amount = amount
        self.amount_withheld = amount_withheld
        self.coupon_id = coupon_id
        super(BookingForm, self).__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)

    def clean(self):
        cleaned_data = super(BookingForm, self).clean()

        # Add TZ info
        booking_date = self.cleaned_data['date']
        start_time = self.cleaned_data['start_time']
        end_time = self.cleaned_data['end_time']
        timezone = pytz.timezone('US/Eastern')
        self.cleaned_data['start_time'] = timezone.localize(datetime(booking_date.year, booking_date.month, booking_date.day, start_time.hour, start_time.minute, start_time.second))
        self.cleaned_data['end_time'] = timezone.localize(datetime(booking_date.year, booking_date.month, booking_date.day, end_time.hour, end_time.minute, end_time.second))
        if self.cleaned_data['end_time'] < self.cleaned_data['start_time']:
            self.cleaned_data['end_time'] = self.cleaned_data['end_time'] + timedelta(days=1)
        return cleaned_data

    def save(self, commit=True):
        data = self.cleaned_data.copy()
        data.pop('start_time')
        data.pop('end_time')
        data.pop('date')
        data['coupon'] = None
        if self.coupon_id:
            try:
                data['coupon'] = Coupon.objects.get(id=self.coupon_id)
            except Coupon.DoesNotExist:
                logger.error('Attempted to use nonexistent coupon with ID: %d' % self.coupon_id)
                pass
        data['amount'] = Decimal(self.amount) if not data['coupon'] else Decimal(self.amount - data['coupon'].amount_off)
        data['service_fee_amount'] = Decimal(self.amount_withheld)
        data['is_sub_merchant_transaction'] = True
        data['time_range'] = DateTimeTZRange(lower=self.cleaned_data['start_time'], upper=self.cleaned_data['end_time'])
        booking = Booking.objects.create(**data)
        return booking

    class Meta:
        model = Booking
        exclude = [
            'date_modified',
            'date_created',
            'amount',
            'service_fee_amount',
            'braintree_id',
            'transaction_status',
            'escrow_status',
            'currency',
            'photographer',
            'coupon',
            'time_range',
            'available_dates'
        ]
        widgets = {
            'client': forms.HiddenInput(),
            'address': forms.HiddenInput(),
            'service_type': forms.HiddenInput(),
            'phone_number': forms.HiddenInput(),
            'message': Textarea(is_required=False),
        }


class FoodBookingDetailsForm(forms.Form):
    display_name = forms.CharField(widget=TextInput())
    service_type = forms.IntegerField(widget=forms.Select(choices=ServiceType.choices()))
    date = forms.DateField(widget=DateInput())
    start_time = forms.TimeField(widget=TimeInput())
    end_time = forms.TimeField(widget=TimeInput())
    contact_name = forms.CharField(widget=TextInput())
    contact_phone_number = forms.CharField(widget=TextInput())
    is_full_menu_shoot = forms.BooleanField(required=False, widget=CheckboxInput())
    is_confirmed_with_restaurant = forms.BooleanField(required=False, widget=CheckboxInput())
    items = forms.CharField(required=False, widget=forms.HiddenInput())

    def clean_date(self):
        data = self.cleaned_data['date']
        if data < date.today():
            raise forms.ValidationError('Invalid date')
        return data

        # def __init__(self, *args, **kwargs):
        #     super(FoodBookingDetailsForm, self).__init__(*args, **kwargs)
        #     self.fields.get('display_name').widget = TextInput()
        #     self.fields.get('phone_number').widget = TextInput()
        #     self.fields.get('date').widget = forms.DateInput()
        #     self.fields.get('start_time').widget = forms.TimeInput()
        #     self.fields.get('end_time').widget = forms.TimeInput()
        #     self.fields.get('service_type').widget = forms.Select(choices=ServiceType.choices())


class FoodBookingAddressForm(BookingAddressForm):
    full_name = forms.CharField(required=False)
    restaurant_name = forms.CharField()

    def clean(self):
        cleaned_data = super(FoodBookingAddressForm, self).clean()
        cleaned_data['full_name'] = cleaned_data.get('restaurant_name')
        return cleaned_data

    def __init__(self, *args, **kwargs):
        super(FoodBookingAddressForm, self).__init__(*args, **kwargs)
        self.fields.get('country').widget = forms.HiddenInput()
        self.fields.get('region').widget = forms.Select(choices=STATE_CHOICES)
        self.fields.get('postal_code').widget = TextInput()
        self.fields.get('restaurant_name').widget = TextInput()
        self.fields.get('street_address').widget = TextInput()
        self.fields.get('sub_premise').widget = TextInput(is_required=False)
        self.fields.get('locality').widget = TextInput()


class FoodBookingForm(BookingForm):
    def clean_items(self):
        return [i.strip() for i in self.cleaned_data.get('items')]

    def __init__(self, *args, **kwargs):
        super(FoodBookingForm, self).__init__(*args, **kwargs)
        self.fields.get('message').widget = Textarea(is_required=False)

    def save(self, commit=True):
        data = self.cleaned_data.copy()
        data.pop('start_time')
        data.pop('end_time')
        data.pop('date')
        data['coupon'] = None
        if self.coupon_id:
            try:
                data['coupon'] = Coupon.objects.get(id=self.coupon_id)
            except Coupon.DoesNotExist:
                logger.error('Attempted to use nonexistent coupon with ID: %d' % self.coupon_id)
                pass
        data['amount'] = Decimal(self.amount) if not data['coupon'] else Decimal(self.amount - data['coupon'].amount_off)
        data['service_fee_amount'] = Decimal(self.amount_withheld)
        data['is_sub_merchant_transaction'] = True
        data['time_range'] = DateTimeTZRange(lower=self.cleaned_data['start_time'], upper=self.cleaned_data['end_time'])
        booking = FoodBooking.objects.create(**data)
        return booking

    class Meta:
        model = FoodBooking
        exclude = [
            'date_modified',
            'date_created',
            'amount',
            'service_fee_amount',
            'braintree_id',
            'transaction_status',
            'escrow_status',
            'currency',
            'photographer',
            'coupon',
            'time_range',
            'available_dates'
        ]
        widgets = {
            'client': forms.HiddenInput(),
            'address': forms.HiddenInput(),
            'service_type': forms.HiddenInput(),
            'phone_number': forms.HiddenInput(),
            'message': KendoTextarea('message', is_required=False),
        }


class PhotographerSearchForm(forms.Form):
    region = forms.CharField(widget=forms.Select(choices=STATE_CHOICES))
    service_type = forms.IntegerField(widget=forms.Select(choices=SERVICE_TYPE_CHOICES))
    date = forms.DateField(widget=DateInput())
    time_period = forms.IntegerField(widget=forms.Select(choices=TimePeriodType.choices()))


class PhotographerSearchResultsForm(forms.Form):
    query = forms.CharField(widget=KendoTextInput('query', attrs={
        'kendo-auto-complete': 'queryAutoComplete',
        'k-data-source': 'queries',
        'k-ng-disabled': '!areControlsEnabled()',
    }))
    condition = forms.IntegerField(widget=KendoDropDownList('condition', choices=CONDITION_CHOICES, attrs={
        'k-ng-disabled': '!areControlsEnabled()',
        # 'k-options': 'conditionDropDownListOptions'
    }))


class PhotographerRequestForm(forms.ModelForm):
    def __init__(self, data=None, files=None, auto_id='id_%s', prefix=None,
                 initial=None, error_class=ErrorList, label_suffix=None,
                 empty_permitted=False, instance=None, is_on_demand=False):
        super(PhotographerRequestForm, self).__init__(data, files, auto_id, prefix, initial, error_class, label_suffix, empty_permitted, instance)
        if is_on_demand:
            self.fields['locality'].widget = TextInput(is_required=False)

    class Meta:
        model = PhotographerRequest
        placeholders = {
            'message': 'You can add more information about what makes your space unique.',
        }
        widgets = {
            'full_name': TextInput('fullName'),
            'email_address': TextInput('emailAddress', attrs={'type': 'email'}),
            'phone_number': TextInput('phoneNumber'),
            'service_type': forms.Select(choices=ServiceType.choices()),
            'locality': TextInput('locality'),
            'region': forms.Select(choices=STATE_CHOICES),
            'postal_code': TextInput('postalCode'),
            'message': Textarea(is_required=False),
            'date': DateInput(),
            'time_period': forms.Select(choices=TimePeriodType.choices()),
        }
        exclude = [
            'date_modified',
            'date_created'
        ]


class OnDemandPhotographerRequestForm(PhotographerRequestForm):
    region = forms.CharField(required=False, widget=KendoDropDownList('region', is_required=False, choices=STATE_CHOICES))


class BraintreeCustomerForm(forms.Form):
    user = forms.ModelChoiceField(queryset=User.objects.all(), widget=forms.Select(attrs={'class': 'hidden'}))
    payment_method_nonce = forms.CharField(widget=KendoTextInput('paymentMethodNonce', is_required=False, attrs={'class': 'hidden'}))

    def save(self):
        user = self.cleaned_data.get('user')
        payment_method_nonce = self.cleaned_data.get('payment_method_nonce')

        # Update or create a Braintree customer
        braintree_customer = None
        try:
            braintree_customer, is_created = BraintreeCustomer.objects.update_or_create_braintree_customer(
                user=user,
                payment_method_nonce=payment_method_nonce,
            )
        except AssertionError:
            # booking.delete()
            self.add_error(None, 'Error processing payment details.')
            raise
        return braintree_customer


class PhotographerForm(BraintreeMerchantAccountForm):
    def save(self):
        braintree_merchant_account = super(PhotographerForm, self).save()
        address = Address.objects.create(
            full_name=self.cleaned_data.get('user').full_name,
            street_address=self.cleaned_data.get('individual_street_address'),
            locality=self.cleaned_data.get('individual_locality'),
            region=self.cleaned_data.get('individual_region'),
            postal_code=self.cleaned_data.get('individual_postal_code'),
            country='US',
        )
        photographer, is_created = Photographer.objects.update_or_create(
            profile=self.cleaned_data.get('user').profile,
            defaults={
                'phone_number': self.cleaned_data.get('individual_phone'),
                'address': address,
            }
        )
        return photographer


class BulkBookingForm(forms.ModelForm):
    client = forms.ModelChoiceField(queryset=Profile.objects.all(), required=False)
    bookings = forms.ModelMultipleChoiceField(queryset=Booking.objects.all(), required=False)

    def clean_file(self):
        file_obj = self.cleaned_data.get('file')

        # Is this file even a CSV...?
        fieldnames = BulkBooking.field_names
        reader = csv.DictReader(file_obj, delimiter=','.__str__(), fieldnames=fieldnames)
        try:
            for row in reader:
                # Is this metadata?
                if row.get('contact_email_address') == 'Email Address':
                    continue
                # Check required fields
                for field_name in fieldnames:
                    if field_name not in BulkBooking.optional_field_names and not row.get(field_name):
                        raise ValidationError(_('Unset field: %(field_name)s'), code='invalid', params={'field_name': field_name})
        except ValidationError:
            raise
        except Exception:
            raise ValidationError(_('Failed to read file'), code='invalid', params={})

        return file_obj

    class Meta:
        model = BulkBooking
        exclude = [
            'date_modified',
            'date_created',
        ]
        widgets = {
            'file': FileInput(attrs={'class': 'hidden'}),
            'message': KendoTextarea('message', is_required=False),
        }


class BookingListForm(forms.Form):
    query = forms.CharField(required=False, widget=TextInput(is_required=False, attrs={'placeholder': 'Enter a search term'}))
    region = forms.ChoiceField(choices=list(), required=True)
    locality = forms.ChoiceField(choices=list(), required=True)

    def __init__(self, locality_choices, region_choices, *args, **kwargs):
        super(BookingListForm, self).__init__(*args, **kwargs)
        self.fields['region'].choices = region_choices
        self.fields['locality'].choices = locality_choices


class BookingAdminForm(forms.ModelForm):
    street_address = forms.CharField()
    sub_premise = forms.CharField(required=False)
    postal_code = forms.CharField(required=False)
    locality = forms.CharField()
    region = forms.CharField()
    country = forms.ChoiceField(choices=countries)
    time_range = DateTimeRangeField(required=False)

    def clean(self):
        cleaned_data = super(BookingAdminForm, self).clean()

        # Get address
        address_data = {
            'full_name': cleaned_data.get('client').full_name,
            'street_address': cleaned_data.pop('street_address'),
            'sub_premise': cleaned_data.pop('sub_premise', None),
            'postal_code': cleaned_data.pop('postal_code', None),
            'locality': cleaned_data.pop('locality'),
            'region': cleaned_data.pop('region'),
            'country': cleaned_data.pop('country')
        }
        if self.instance and hasattr(self.instance, 'address'):
            address_form = AddressForm(data=address_data, instance=self.instance.address)
        else:
            address_form = AddressForm(data=address_data)
        if not address_form.is_valid():
            errors = address_form.errors.as_data()
            raise forms.ValidationError(errors)
        cleaned_data['address'] = address_form.save()

        return cleaned_data

    def save(self, commit=True):
        booking = super(BookingAdminForm, self).save(commit=False)

        data = self.cleaned_data.copy()
        booking.thread = Thread.objects.create()
        booking.address = data['address']
        booking.save()
        booking.address.geocode()
        return booking

    class Meta:
        model = Booking
        placeholders = {
            'message': 'You can add more information about what makes your space unique.',
        }
        fields = [
            'client',
            'photographer',
            'amount',
            'service_fee_amount',
            'service_type'
        ]


class FoodBookingAdminForm(forms.ModelForm):
    street_address = forms.CharField()
    sub_premise = forms.CharField(required=False)
    postal_code = forms.CharField(required=False)
    locality = forms.CharField()
    region = forms.CharField()
    country = forms.ChoiceField(choices=countries)
    time_range = DateTimeRangeField(required=False)

    def clean(self):
        cleaned_data = super(FoodBookingAdminForm, self).clean()

        # Get address
        address_data = {
            'full_name': cleaned_data.get('client').full_name,
            'street_address': cleaned_data.pop('street_address'),
            'sub_premise': cleaned_data.pop('sub_premise', None),
            'postal_code': cleaned_data.pop('postal_code', None),
            'locality': cleaned_data.pop('locality'),
            'region': cleaned_data.pop('region'),
            'country': cleaned_data.pop('country')
        }
        if self.instance and hasattr(self.instance, 'address'):
            address_form = AddressForm(data=address_data, instance=self.instance.address)
        else:
            address_form = AddressForm(data=address_data)
        if not address_form.is_valid():
            errors = address_form.errors.as_data()
            raise forms.ValidationError(errors)
        cleaned_data['address'] = address_form.save()

        return cleaned_data

    def save(self, commit=True):
        food_booking = super(FoodBookingAdminForm, self).save(commit=False)
        food_booking.service_type = ServiceType.FOOD

        data = self.cleaned_data.copy()
        food_booking.thread = Thread.objects.create()
        food_booking.address = data['address']
        food_booking.save()
        # data = self.cleaned_data.copy()
        # data['thread'] = Thread.objects.create()
        # food_booking = FoodBooking.objects.create(**data)
        food_booking.address.geocode()
        return food_booking

    class Meta:
        model = FoodBooking
        placeholders = {
            'message': 'You can add more information about what makes your space unique.',
        }
        fields = [
            'client',
            'photographer',
            'restaurant_name',
            'amount',
            'service_fee_amount',
            'items',
            'time_range',
            'contact_name',
            'contact_email_address',
            'contact_phone_number',
            'shot_list'
        ]


class CreateFoodBookingForm(forms.ModelForm):
    date = forms.DateField(widget=DateInput(attrs={}))
    start_time = forms.TimeField(widget=TimeInput(attrs={}))
    stop_time = forms.TimeField(widget=TimeInput(attrs={}))
    street_address = forms.CharField(widget=TextInput())
    sub_premise = forms.CharField(required=False, widget=TextInput(is_required=False))
    postal_code = forms.CharField(required=True, widget=TextInput())
    locality = forms.CharField(widget=TextInput())
    region = forms.CharField(widget=forms.Select(choices=STATE_CHOICES))
    contact_name = forms.CharField(widget=TextInput())
    contact_email_address = forms.EmailField(widget=TextInput(attrs={'type': 'email'}))
    contact_phone_number = forms.CharField(widget=TextInput())

    def __init__(self, client, data=None, *args, **kwargs):
        self.client = client
        super(CreateFoodBookingForm, self).__init__(data=data, *args, **kwargs)

    def clean_latitude(self):
        latitude = self.cleaned_data['latitude']
        if latitude:
            if latitude < -90 or latitude > 90:
                raise forms.ValidationError('Invalid latitude (%d)' % latitude)
        return latitude

    def clean_longitude(self):
        longitude = self.cleaned_data['longitude']
        if longitude:
            if longitude < -180 or longitude > 180:
                raise forms.ValidationError('Invalid longitude (%d)' % longitude)
        return longitude

    def clean_date(self):
        data = self.cleaned_data['date']
        if data < date.today():
            raise forms.ValidationError('Invalid date')
        return data

    def clean_start_time(self):
        start_time = self.cleaned_data['start_time']
        if start_time < time(8, 0, 0) or start_time > time(17, 0, 0):
            raise forms.ValidationError('Invalid start time')
        return start_time

    def clean_stop_time(self):
        stop_time = self.cleaned_data['stop_time']
        if stop_time < time(11, 0, 0) or stop_time > time(20, 0, 0):
            raise forms.ValidationError('Invalid end time')
        return stop_time

    def save(self, commit=True):
        booking = super(CreateFoodBookingForm, self).save(commit=False)
        booking.client = self.client

        # Thread
        booking.thread = Thread.objects.create()
        booking.items = []

        # Money
        booking.amount = PAY_SCALE[0].get('amount')
        booking.service_fee_amount = PAY_SCALE[0].get('service_fee_amount')

        # Address
        address = Address.objects.create(
            street_address=self.cleaned_data.get('street_address'),
            sub_premise=self.cleaned_data.get('sub_premise'),
            postal_code=self.cleaned_data.get('postal_code'),
            locality=self.cleaned_data.get('locality'),
            region=self.cleaned_data.get('region'),
        )
        # if self.cleaned_data.get('longitude') and self.cleaned_data.get('latitude'):
        #     address.point = 'POINT(%f %f)' % (self.cleaned_data.get('longitude'), self.cleaned_data.get('latitude'))
        #     address.save()
        try:
            address.geocode()
        except Exception:
            logger.exception('Failed to geocode address!')

        # Time range
        booking_date = self.cleaned_data.get('date')
        time_zone = address.time_zone
        if not time_zone:
            time_zone = pytz.utc
        else:
            time_zone = timezone(time_zone)
        start_time = time_zone.localize(datetime(
            booking_date.year,
            booking_date.month,
            booking_date.day,
            self.cleaned_data.get('start_time').hour,
            self.cleaned_data.get('start_time').minute,
            self.cleaned_data.get('start_time').second)
        )
        stop_time = time_zone.localize(datetime(
            booking_date.year,
            booking_date.month,
            booking_date.day,
            self.cleaned_data.get('stop_time').hour,
            self.cleaned_data.get('stop_time').minute,
            self.cleaned_data.get('stop_time').second)
        )
        if stop_time < start_time:
            stop_time = stop_time + timedelta(days=1)
        booking.time_range = DateTimeTZRange(lower=start_time, upper=stop_time)

        booking.address = address
        booking.save()
        booking.create_booking_requests()
        return booking

    class Meta:
        model = FoodBooking
        fields = [
            'restaurant_name',
            'contact_name',
            'contact_email_address',
            'contact_phone_number',
            'shot_list'
        ]
        widgets = {
            'shot_list': FileInput(is_required=False),
            'message': KendoTextarea('message', is_required=False),
            'restaurant_name': TextInput()
        }
