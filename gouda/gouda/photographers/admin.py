from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import csv
import datetime
import logging
import os
import random
import string
from wsgiref.util import FileWrapper

from django.conf import settings
from django.contrib import admin
from django.contrib.contenttypes.models import ContentType
from django.contrib.gis.db import models
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _

from gouda.customers.models import BraintreeEscrowStatusType
from gouda.customers.models import BraintreeTransactionStatusType
from gouda.customers.utils import braintree
from gouda.forms.widgets import OpenLayersWidget
from gouda.photographers.forms import FoodBookingAdminForm
from gouda.photographers.forms import BookingAdminForm
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingAlbum
from gouda.photographers.models import BookingCheckoutURL
from gouda.photographers.models import BookingImage
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import PhotographerStatusType
from gouda.photographers.models import Review
from gouda.photographers.models import ReviewURL
from gouda.photographers.models import Tip
from gouda.photographers.tasks import on_staff_approval
from gouda.photographers.tasks import submit_for_settlement
from calendar import Calendar
import json
from dateutil.parser import parse as parse_date
from psycopg2.extras import DateTimeTZRange
from django.core.exceptions import ImproperlyConfigured, ValidationError
from django.core.mail import EmailMultiAlternatives
from django.template import loader
from django.db.models import Sum
from weasyprint import HTML, CSS


RUN_DIR = getattr(settings, 'RUN_DIR', '')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = 30
logger = logging.getLogger(__name__)

# admin.site.disable_action('delete_selected')


def get_admin_url(obj):
    content_type = ContentType.objects.get_for_model(obj.__class__)
    url = reverse('admin:%s_%s_change' % (content_type.app_label, content_type.model), args=[obj.pk])
    return format_html(u'<a href="%s">%s</a>' % (url, obj))


class DateFieldListFilter(admin.DateFieldListFilter):
    def __init__(self, field, request, params, model, model_admin, field_path):
        super(DateFieldListFilter, self).__init__(
            field, request, params, model, model_admin, field_path)

        now = timezone.now()
        # When time zone support is enabled, convert "now" to the user's time
        # zone so Django's definition of "Today" matches what the user expects.
        if timezone.is_aware(now):
            now = timezone.localtime(now)

        if isinstance(field, models.DateTimeField):
            today = now.replace(hour=0, minute=0, second=0, microsecond=0)
        else:       # field is a models.DateField
            today = now.date()
        tomorrow = today + datetime.timedelta(days=1)
        if today.month == 12:
            next_month = today.replace(year=today.year + 1, month=1, day=1)
        else:
            next_month = today.replace(month=today.month + 1, day=1)
        next_year = today.replace(year=today.year + 1, month=1, day=1)

        self.lookup_kwarg_since = '%s__gte' % field_path
        self.lookup_kwarg_until = '%s__lt' % field_path
        self.links = (
            (_('Any date'), {}),
            (_('Today'), {
                self.lookup_kwarg_since: str(today),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('Past minute'), {
                self.lookup_kwarg_since: str(now - datetime.timedelta(minutes=1)),
                self.lookup_kwarg_until: str(now),
            }),
            (_('Past 30 minutes'), {
                self.lookup_kwarg_since: str(now - datetime.timedelta(minutes=30)),
                self.lookup_kwarg_until: str(now),
            }),
            (_('Past hour'), {
                self.lookup_kwarg_since: str(now - datetime.timedelta(hours=1)),
                self.lookup_kwarg_until: str(now),
            }),
            (_('Past 4 hours'), {
                self.lookup_kwarg_since: str(now - datetime.timedelta(hours=4)),
                self.lookup_kwarg_until: str(now),
            }),
            (_('Past 7 days'), {
                self.lookup_kwarg_since: str(today - datetime.timedelta(days=7)),
                self.lookup_kwarg_until: str(tomorrow),
            }),
            (_('This month'), {
                self.lookup_kwarg_since: str(today.replace(day=1)),
                self.lookup_kwarg_until: str(next_month),
            }),
            (_('This year'), {
                self.lookup_kwarg_since: str(today.replace(month=1, day=1)),
                self.lookup_kwarg_until: str(next_year),
            }),
        )


class BookingTimeZoneFilter(admin.SimpleListFilter):
    title = _('time zone')
    parameter_name = 'address__time_zone'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        bookings = model_admin.get_queryset(request)
        time_zones = list()
        for booking in bookings:
            if booking.address.time_zone:
                time_zones.append(booking.address.time_zone)
        choices = list()
        for time_zone in frozenset(time_zones):
            choices.append((time_zone.zone, _(time_zone.zone)))
        return choices

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value():
            return queryset.filter(address__time_zone=self.value())


class WeekFilter(admin.SimpleListFilter):
    title = _('week')
    parameter_name = 'time_range'

    def lookups(self, request, model_admin):
        year = timezone.now().year
        months = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        idx = months.index(timezone.now().month)
        choices = list()
        labels = set()
        for i in range(2):
            month = months[idx]
            calendar = Calendar()
            for week in reversed(calendar.monthdatescalendar(year, month)):
                if week[0] > timezone.now().date():
                    continue
                label = 'Week of %s' % week[0].strftime('%m/%d/%Y')
                code = {
                    'start_date': week[0].isoformat(),
                    'end_date': week[-1].isoformat()
                }
                if label not in labels:
                    choices.append((json.dumps(code), label))
                    labels.add(label)
            idx -= 1
        return choices

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if not self.value():
            return None
        data = json.loads(self.value())
        start_date = parse_date(data['start_date'])
        end_date = parse_date(data['end_date'])
        return queryset.filter(time_range__contained_by=DateTimeTZRange(start_date, end_date))


class TimeRangeFilter(admin.SimpleListFilter):
    title = _('time range')
    parameter_name = 'time_range'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('isnotnull', _('Is Scheduled')),
            ('isnull', _('(None)')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == 'isnotnull':
            return queryset.filter(time_range__isnull=False)
        if self.value() == 'isnull':
            return queryset.filter(time_range__isnull=True)


@admin.register(BookingRequest)
class BookingRequestAdmin(admin.ModelAdmin):
    list_display = (
        'booking',
        'photographer',
        'did_accept_booking',
        'did_reject_booking',
        'date_created',
        'date_modified'
    )
    readonly_fields = (
        'did_accept_booking',
        'did_reject_booking',
    )
    fieldsets = (
        ('Booking', {
            'fields': (
                'booking',
            )
        }),
        ('Photographer', {
            'fields': (
                'photographer',
            )
        }),
        ('Status', {
            'fields': (
                'did_accept_booking',
                'did_reject_booking',
            )
        }),
    )


@admin.register(Tip)
class TipAdmin(admin.ModelAdmin):
    list_display = (
        'client',
        'photographer',
        'booking',
        'amount',
        'service_fee_amount',
        'transaction_status',
        'escrow_status',
        'date_created',
        'date_modified'
    )
    readonly_fields = (
        'client',
        'photographer',
        'booking',
        'amount',
        'service_fee_amount',
        'transaction_status',
        'escrow_status',
        'braintree_id',
    )
    list_filter = ('transaction_status', 'escrow_status')
    ordering = ('date_created',)
    fieldsets = (
        (None, {
            'fields': (
                'client',
                'photographer',
                'booking',
            )
        }),
        ('Braintree', {
            'fields': (
                'braintree_id',
                'amount',
                'service_fee_amount',
                'transaction_status',
                'escrow_status',
            )
        }),
    )

    def photographer(self, obj):
        return obj.booking.photographer
    photographer.admin_order_field = 'booking__photographer'


@admin.register(Review)
class ReviewAdmin(admin.ModelAdmin):
    list_display = (
        'reviewer',
        'photographer',
        'rating',
        'date_created',
    )
    readonly_fields = (
        'reviewer',
        'photographer',
        'title',
        'content',
        'rating',
    )

    fieldsets = (
        (None, {
            'fields': (
                'reviewer',
                'photographer',
            )
        }),
        (None, {
            'fields': (
                'title',
                'content',
                'rating',
            )
        }),
    )

    def photographer(self, obj):
        return obj.booking.photographer
    photographer.admin_order_field = 'booking__photographer'


@admin.register(ReviewURL)
class ReviewURLAdmin(admin.ModelAdmin):
    list_display = (
        'client',
        'booking',
        'url',
        'has_been_used',
        'date_used',
    )
    readonly_fields = (
        'key',
        'url',
        'has_been_used',
        'date_used',
        'date_created',
        'date_modified'
    )

    fieldsets = (
        (None, {
            'fields': (
                'client',
                'booking',
                'url',
                'download_url',
                'survey_url'
                'has_been_used',
                'date_used'
            )
        }),
    )

    add_form_fieldsets = (
        (None, {
            'fields': (
                'client',
                'booking',
                'download_url',
                'survey_url'
            )
        }),
    )

    def get_fieldsets(self, request, obj=None):
        if obj:
            return self.fieldsets
        else:
            return self.add_form_fieldsets


@admin.register(BookingCheckoutURL)
class BookingCheckoutURLAdmin(admin.ModelAdmin):
    exclude = ['key', 'has_been_used', 'url']
    list_display = (
        'customer',
        'line_item',
        'price',
        'amount_withheld',
        'url',
        'has_been_used',
        'date_used',
        'date_created',
        'date_modified'
    )
    raw_id_fields = ('customer',)
    readonly_fields = (
        'key',
        'currency',
        'url',
        'has_been_used',
        'date_used',
        'date_created',
        'date_modified'
    )
    list_filter = ('has_been_used',)

    fieldsets = (
        (None, {
            'fields': (
                'customer',
                'url',
                'has_been_used',
                'date_used'
            )
        }),
        ('Price details', {
            'fields': (
                'price',
                'amount_withheld',
                'line_item',
                'currency',
            )
        }),
    )

    add_form_fieldsets = (
        (None, {
            'fields': (
                'customer',
            )
        }),
        ('Price details', {
            'fields': (
                'price',
                'amount_withheld',
                'line_item',
                'currency',
            )
        }),
    )

    def get_fieldsets(self, request, obj=None):
        if obj:
            return self.fieldsets
        else:
            return self.add_form_fieldsets


class BookingImageInlineAdmin(admin.TabularInline):
    model = BookingImage


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    raw_id_fields = ('client', 'photographer')
    list_display = (
        'client',
        'photographer',
        'date',
        'service_type',
        'amount',
        'service_fee_amount',
        'transaction_status',
        'escrow_status',
        'locality',
        'region',
        'detail_page',
        'id',
        'date_created',
        'date_modified'
    )
    readonly_fields = (
        #'client',
        'coupon',
        # 'amount',
        # 'service_fee_amount',
        'transaction_status',
        'escrow_status',
        'braintree_id',
        'street_address',
        'locality',
        'region',
        'sub_premise',
        'postal_code',
        'time_zone',
        'point',
        'date',
        'time_period',
        # 'service_type',
        'token',
        'available_dates',
        'detail_page'
    )
    list_filter = (
        ('client', admin.RelatedOnlyFieldListFilter),
        # TimeRangeFilter,
        WeekFilter,
        'photographer',
        'service_type',
        'transaction_status',
        'escrow_status',
        'did_client_approve',
        'did_client_reject',
        'did_staff_approve',
        'did_photographer_approve',
        ('date_created', DateFieldListFilter),
        'address__normalized_region',
        BookingTimeZoneFilter,
    )
    ordering = ('-date_created',)
    add_form_fieldsets = (
        (None, {
            'fields': (
                'client',
                'photographer',
            )
        }),
        ('Details', {
            'fields': (
                'service_type',
            )
        }),
        ('Location', {
            'fields': (
                'street_address',
                'locality',
                'region',
                'sub_premise',
                'postal_code',
                'country'
            )
        }),
        ('Braintree', {
            'fields': (
                'amount',
                'service_fee_amount',
            )
        }),
    )
    fieldsets = (
        (None, {
            'fields': (
                'photographer',
                'client',
                'detail_page',
                'token',
            )
        }),
        ('Approval', {
            'fields': (
                'did_client_reject',
                'client_rejection_date',

                'did_client_approve',
                'client_approval_date',

                'did_staff_approve',
                'staff_approval_date',

                'did_photographer_approve',
                'photographer_approval_date',
                'num_shots',

                'did_photographer_complete',
                'photographer_completion_date'
            )
        }),
        ('Download', {
            'fields': (
                'media_file',
            )
        }),
        ('Job details', {
            'fields': (
                'time_range',
                'date',
                'time_period',
                'service_type',
                'available_dates',
                'message',
                'shot_list'
            )
        }),
        ('Contact details', {
            'fields': (
                'contact_name',
                'contact_phone_number',
                'contact_email_address',
            )
        }),
        ('Location', {
            'fields': (
                'address',
                'street_address',
                'locality',
                'region',
                'sub_premise',
                'postal_code',
                'time_zone',
                'point'
            )
        }),
        ('Braintree', {
            'fields': (
                'braintree_id',
                'amount',
                'service_fee_amount',
                'transaction_status',
                'escrow_status',
                'is_sub_merchant_transaction',
                'do_hold_in_escrow'
            )
        }),
    )
    inlines = [BookingImageInlineAdmin]
    exclude = ('images',)
    actions = [
        'release_from_escrow',
        'refund',
        'update',
        'hold_in_escrow',
        'submit_for_settlement',
        # 'create_booking_requests',

        'send_contact_notifications',
        # 'send_contact_mistake_notifications',
        'send_staff_approval_notification',
        'send_photographer_notifications',
        'send_receipts',
        # 'nullify_dates',

        'on_staff_approval',
        'generate_invoice'
    ]
    formfield_overrides = {
        models.PointField: {'widget': OpenLayersWidget(attrs={'display_raw': True})}
    }

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return tuple()
        return self.readonly_fields

    def get_fieldsets(self, request, obj=None):
        if obj:
            return self.fieldsets
        else:
            return self.add_form_fieldsets

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            kwargs['form'] = BookingAdminForm
        return super(BookingAdmin, self).get_form(request, obj, **kwargs)

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == 'photographer':
            kwargs['queryset'] = Photographer.objects.filter(status=PhotographerStatusType.ACTIVE).order_by('profile__user__full_name')
        return super(BookingAdmin, self).formfield_for_foreignkey(db_field, request, **kwargs)

    def nullify_dates(self, request, queryset):
        for booking in queryset:
            booking.time_range = None
            booking.save()
        self.message_user(request, '%d dates were nullified.' % queryset.count())
    nullify_dates.short_description = 'Nullify booking dates'

    def send_contact_mistake_notifications(self, request, queryset):
        for booking in queryset:
            booking.send_contact_mistake_notification()
        self.message_user(request, '%d contact mistake notifications were made.' % queryset.count())
    send_contact_mistake_notifications.short_description = 'Send email asking merchants to reschedule because of high demand'

    def send_contact_notifications(self, request, queryset):
        for booking in queryset:
            booking.send_contact_notification()
        self.message_user(request, '%d contact notifications were made.' % queryset.count())
    send_contact_notifications.short_description = 'Send email asking merchants to schedule a shoot'

    def create_booking_requests(self, request, queryset):
        booking_requests = list()
        for booking in queryset:
            booking_requests.extend(booking.create_booking_requests())
        self.message_user(request, '%d requests were made.' % len(booking_requests))

    def send_photographer_notifications(self, request, queryset):
        bookings = queryset.filter(photographer__isnull=False)
        for booking in bookings:
            booking.send_photographer_notification()
        self.message_user(request, '%d emails were sent out.' % bookings.count())
    send_photographer_notifications.short_description = 'Send photographer notification email'

    def send_receipts(self, request, queryset):
        bookings = queryset.exclude(braintree_id='')
        for booking in bookings:
            booking.send_receipt()
        self.message_user(request, '%d receipts were sent out.' % bookings.count())
    send_receipts.short_description = 'Send receipt'

    def update(self, request, queryset):
        num_updated = 0
        bookings = queryset.exclude(transaction_status=BraintreeTransactionStatusType.LOCKED).exclude(braintree_id='')
        for booking in bookings:
            booking.update_braintree_data()
            # old_transaction_status = booking.transaction_status
            # booking.transaction_status = BraintreeTransactionStatusType.LOCKED
            # booking.save()
            # try:
            #     transaction = braintree.Transaction.find(booking.braintree_id)
            #     assert transaction is not None
            #     logger.info(transaction)
            #     logger.info(transaction.status)
            #     logger.info(transaction.escrow_status)
            #     booking.transaction_status = BraintreeTransactionStatusType.get(transaction.status).value
            #     booking.escrow_status = BraintreeEscrowStatusType.get(transaction.escrow_status).value if transaction.escrow_status else None
            #     booking.save()
            # except:
            #     booking.transaction_status = old_transaction_status
            #     booking.save()
            #     logger.exception('Failed to update transaction (%d, %s)' % (booking.id, booking.braintree_id))
            #     raise
            num_updated += 1
        self.message_user(request, '%d transactions were successfully updated.' % num_updated)
    update.short_description = 'Update selected transactions'

    def submit_for_settlement(self, request, queryset):
        num_submitted_for_settlement = 0
        bookings = queryset.exclude(transaction_status=BraintreeTransactionStatusType.LOCKED)
        for booking in bookings:
            submit_for_settlement.apply_async(
                countdown=num_submitted_for_settlement * (BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS + 5),
                kwargs={'booking_id': booking.id}
            )
            num_submitted_for_settlement += 1
        self.message_user(request, '%d transactions were submitted for settlement.' % (
            num_submitted_for_settlement,
        ))
    submit_for_settlement.short_description = 'Submit selected transactions for settlement'

    def on_staff_approval(self, request, queryset):
        num_approved = 0
        bookings = queryset.filter(did_client_approve=False, did_staff_approve=False, did_photographer_approve=True)
        for booking in bookings:
            try:
                on_staff_approval.delay(booking.id)
                num_approved += 1
            except AssertionError:
                self.message_user(request, '%d bookings were successfully given staff approval.' % (num_approved, ))
                raise
        self.message_user(request, '%d bookings were successfully given staff approval.' % (
            num_approved,
        ))
    on_staff_approval.short_description = 'Give staff approval'

    def send_staff_approval_notification(self, request, queryset):
        num_processed = 0
        bookings = queryset.filter(did_client_approve=False, did_staff_approve=True, did_photographer_approve=True)
        for booking in bookings:
            booking.send_staff_approval_notification()
            num_processed += 1
        self.message_user(request, '%d emails were sent.' % (num_processed,))
    send_staff_approval_notification.short_description = 'Send staff approval notification'

    def hold_in_escrow(self, request, queryset):
        num_held_in_escrow = 0
        bookings = queryset.exclude(transaction_status=BraintreeTransactionStatusType.LOCKED)
        for booking in bookings:
            try:
                booking.sale(do_hold_in_escrow=True)
                num_held_in_escrow += 1
            except AssertionError:
                self.message_user(request, '%d transactions were successfully placed in escrow.' % (num_held_in_escrow, ))
                raise
        self.message_user(request, '%d transactions were successfully placed in escrow.' % (num_held_in_escrow, ))
    hold_in_escrow.short_description = 'Place selected transactions in escrow'

    def refund(self, request, queryset):
        num_refunded = 0
        bookings = queryset.exclude(transaction_status=BraintreeTransactionStatusType.LOCKED).exclude(braintree_id='')
        for booking in bookings:
            old_transaction_status = booking.transaction_status
            booking.transaction_status = BraintreeTransactionStatusType.LOCKED
            booking.save()
            try:
                result = braintree.Transaction.refund(booking.braintree_id)
                assert result.is_success
                logger.info(result)
                logger.info(result.transaction.status)
                logger.info(result.transaction.escrow_status)
                booking.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
                booking.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None
                booking.save()
            except:
                booking.transaction_status = old_transaction_status
                booking.save()
                logger.exception('Failed to issue refund (%d, %s, %s)' % (booking.id, booking.braintree_id, result.message))
                raise
            num_refunded += 1
        self.message_user(request, '%d transactions were successfully refunded.' % num_refunded)
    refund.short_description = 'Refund selected transactions'

    def release_from_escrow(self, request, queryset):
        num_released = 0
        bookings = queryset.exclude(transaction_status=BraintreeTransactionStatusType.LOCKED).exclude(braintree_id='')
        for booking in bookings:
            old_transaction_status = booking.transaction_status
            booking.transaction_status = BraintreeTransactionStatusType.LOCKED
            booking.save()
            try:
                result = braintree.Transaction.release_from_escrow(booking.braintree_id)
                assert result.is_success, result.message
                booking.transaction_status = BraintreeTransactionStatusType.get(result.transaction.status).value
                booking.escrow_status = BraintreeEscrowStatusType.get(result.transaction.escrow_status).value if result.transaction.escrow_status else None
                booking.save()
            except:
                booking.transaction_status = old_transaction_status
                booking.save()
                logger.exception('Failed to release transaction from escrow (%d, %s)' % (booking.id, booking.braintree_id))
                raise
            num_released += 1
        self.message_user(request, '%d transactions were successfully released from escrow.' % num_released)
    release_from_escrow.short_description = 'Release selected transactions from escrow'

    def generate_invoice(self, request, queryset):
        # Send
        context = {
            'bookings': queryset.order_by('-date_created'),
            'date_created': timezone.now(),
            'amount': queryset.aggregate(Sum('amount')).get('amount__sum')
        }
        subject = loader.render_to_string('photographers/emails/invoice_subject.txt', context)
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string('photographers/emails/invoice_body.txt', context)
        html_body = loader.render_to_string('photographers/emails/invoice_body.html', context)
        email_message = EmailMultiAlternatives(
            subject=subject,
            body=body,
            from_email=settings.SERVER_EMAIL,
            to=settings.STAFF_EMAILS,
        )
        email_message.attach('invoice.pdf', HTML(string=html_body).write_pdf(), 'application/pdf')
        email_message.send()
        self.message_user(request, 'Sent invoice for %d bookings' % queryset.count())
    generate_invoice.short_description = 'Generate invoice and mail to admins'

    def date(self, obj):
        if obj.time_range:
            start_time = obj.time_range.lower
            if obj.address.time_zone:
                time_zone = obj.address.time_zone
                time_format = '%m/%d/%Y %Z'
                return '%s' % (
                    start_time.astimezone(time_zone).strftime(time_format),
                )
            else:
                time_format = '%m/%d/%Y'
                return '%s' % (
                    start_time.strftime(time_format),
                )
        else:
            return None
    date.admin_order_field = 'time_range'

    def time_period(self, obj):
        if obj.time_range:
            start_time = obj.time_range.lower
            end_time = obj.time_range.upper
            if obj.address.time_zone:
                time_zone = obj.address.time_zone
                time_format = '%I:%M %p %Z'
                return '%s - %s' % (
                    start_time.astimezone(time_zone).strftime(time_format),
                    end_time.astimezone(time_zone).strftime(time_format),
                )
            else:
                time_format = '%I:%M %p'
                return '%s - %s' % (
                    start_time.strftime(time_format),
                    end_time.strftime(time_format)
                )
        return None
    time_period.admin_order_field = 'time_range'

    def street_address(self, obj):
        return obj.address.formatted_street_address
    street_address.admin_order_field = 'address__street_address'

    def locality(self, obj):
        return obj.address.formatted_locality
    locality.admin_order_field = 'address__locality'

    def region(self, obj):
        return obj.address.formatted_region
    region.admin_order_field = 'address__region'

    def sub_premise(self, obj):
        return obj.address.sub_premise
    sub_premise.admin_order_field = 'address__sub_premise'

    def postal_code(self, obj):
        return obj.address.formatted_postal_code
    postal_code.admin_order_field = 'address__postal_code'

    def time_zone(self, obj):
        return obj.address.time_zone
    postal_code.admin_order_field = 'address__time_zone'

    def point(self, obj):
        return obj.address.point
    postal_code.admin_order_field = 'address__point'

    def detail_page(self, obj):
        return '<a href="%s">View page</a>' % reverse('profiles:booking_detail', kwargs={'pk': obj.id})
    detail_page.allow_tags = True


class FoodBookingAdmin(BookingAdmin):
    list_display = (
        'client',
        'photographer',
        'restaurant_name',
        'date',
        'amount',
        'service_fee_amount',
        'transaction_status',
        'escrow_status',
        'locality',
        'region',
        'detail_page',
        'id',
        'date_created',
        'date_modified'
    )
    raw_id_fields = ('client', 'photographer')
    fieldsets = (
        (None, {
            'fields': (
                'photographer',
                'client',
                'token'
            )
        }),
        ('Approval', {
            'fields': (
                'did_client_reject',
                'client_rejection_date',

                'did_client_approve',
                'client_approval_date',

                'did_staff_approve',
                'staff_approval_date',

                'did_photographer_approve',
                'photographer_approval_date',
                'num_shots',

                'did_photographer_complete',
                'photographer_completion_date',

                'contact_notification_date'
            )
        }),
        ('Download', {
            'fields': (
                'media_file',
            )
        }),
        ('Job details', {
            'fields': (
                'time_range',
                'date',
                'time_period',
                'service_type',
                'message',
                'shot_list'
            )
        }),
        ('Food details', {
            'fields': (
                'menu_url',
                'items',
                'is_full_menu_shoot',
                'is_confirmed_with_restaurant'
            )
        }),
        ('Contact details', {
            'fields': (
                'contact_name',
                'contact_phone_number',
                'contact_email_address',
            )
        }),
        ('Location', {
            'fields': (
                'address',
                'restaurant_name',
                'street_address',
                'locality',
                'region',
                'sub_premise',
                'postal_code',
            )
        }),
        ('Braintree', {
            'fields': (
                'braintree_id',
                'amount',
                'service_fee_amount',
                'transaction_status',
                'escrow_status',
            )
        }),
    )

    add_form_fieldsets = (
        (None, {
            'fields': (
                'client',
                'photographer',
            )
        }),
        ('Food details', {
            'fields': (
                'items',
            )
        }),
        ('Job details', {
            'fields': (
                'time_range',
                'shot_list'
            )
        }),
        ('Location', {
            'fields': (
                'restaurant_name',
                'street_address',
                'locality',
                'region',
                'sub_premise',
                'postal_code',
                'country'
            )
        }),
        ('Contact details', {
            'fields': (
                'contact_name',
                'contact_phone_number',
                'contact_email_address',
            )
        }),
        ('Braintree', {
            'fields': (
                'amount',
                'service_fee_amount',
            )
        }),
    )

    search_fields = (
        'restaurant_name',
    )

    def get_readonly_fields(self, request, obj=None):
        if not obj:
            return tuple()
        return self.readonly_fields

    def get_form(self, request, obj=None, **kwargs):
        if not obj:
            kwargs['form'] = FoodBookingAdminForm
        return super(FoodBookingAdmin, self).get_form(request, obj, **kwargs)

    def get_fieldsets(self, request, obj=None):
        if obj:
            return self.fieldsets
        else:
            return self.add_form_fieldsets

    # def get_actions(self, request):
    #     actions = super(FoodBookingAdmin, self).get_actions(request)
    #     actions.extend([self.get_action(a) for a in [
    #         'send_merchant_notification'
    #     ]])
    #     # actions = list(self.actions)
    #     # actions.extend([
    #     #     'send_merchant_notification'
    #     # ])
    #     return actions

    def __init__(self, model, admin_site):
        # self.readonly_fields += (
        #     'restaurant_name',
        #     'contact_name',
        #     'contact_phone_number',
        #     'is_full_menu_shoot',
        #     'is_confirmed_with_restaurant'
        # )
        self.actions.extend([
            'send_merchant_notification'
        ])
        super(FoodBookingAdmin, self).__init__(model, admin_site)

    def send_merchant_notification(self, request, queryset):
        for booking in queryset:
            booking.send_contact_notification()
        self.message_user(request, '%d merchant notifications were made.' % queryset.count())
    send_merchant_notification.short_description = 'Send email to merchants about upcoming shoot'

admin.site.register(FoodBooking, FoodBookingAdmin)


@admin.register(PhotographerRequest)
class PhotographerRequestAdmin(admin.ModelAdmin):
    list_display = ('email_address', 'full_name', 'phone_number', 'service_type', 'locality', 'region', 'postal_code', 'date', 'time_period', 'date_created')
    list_filter = ('service_type',)
    ordering = ('date_created',)


@admin.register(Photographer)
class PhotographerAdmin(admin.ModelAdmin):
    list_display = (
        'email_address',
        'full_name',
        'phone_number',
        'locality',
        'region',
        'postal_code',
        'status',
        'point',
        'date_created')
    ordering = ('date_created',)
    readonly_fields = (
        'profile',
        'full_name',
        'email_address',
        'street_address',
        'locality',
        'region',
        'postal_code'
    )
    fieldsets = (
        (None, {
            'fields': (
                'profile',
                'status',
                'phone_number',
            )
        }),
        ('Location', {
            'fields': (
                'address',
                'street_address',
                'locality',
                'region',
                'postal_code',
            )
        }),
    )
    actions = [
        'geocode_address',
        'create_csv_file'
    ]
    raw_id_fields = ('address',)

    def create_csv_file(self, request, queryset):
        field_names = (
            'full_name',
            'first_name',
            'last_name',
            'email_address',
            'locality',
            'region'
        )
        choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
        csv_file_name = ''.join([random.choice(choices) for i in range(16)])
        csv_file_path = os.path.join(RUN_DIR, csv_file_name + '.csv')

        with open(csv_file_path, 'wb') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=field_names)
            writer.writeheader()
            for photographer in queryset:
                writer.writerow({
                    'full_name': photographer.full_name,
                    'first_name': photographer.first_name,
                    'last_name': photographer.profile.last_name,
                    'email_address': photographer.email_address,
                    'locality': photographer.braintree_merchant_account.individual_locality,
                    'region': photographer.braintree_merchant_account.individual_region,
                })

        with open(csv_file_path, 'r') as csv_file:
            response = HttpResponse(FileWrapper(csv_file), content_type='text/csv')
        response['Content-Disposition'] = 'attachment; filename=%s.csv' % csv_file_name
        return response

    create_csv_file.short_description = 'Download CSV file'

    def geocode_address(self, request, queryset):
        for photographer in queryset:
            photographer.address.geocode()
        self.message_user(request, '%d addresses were geocoded.' % queryset.count())
    geocode_address.short_description = 'Geocode photographer addresses'

    def full_name(self, obj):
        return obj.profile.full_name
    full_name.admin_order_field = 'profile__user__full_name'

    def email_address(self, obj):
        return obj.profile.email_address
    email_address.admin_order_field = 'profile__user__email_address'

    def street_address(self, obj):
        return obj.address.formatted_street_address
    street_address.admin_order_field = 'address__normalized_street_address'

    def locality(self, obj):
        return obj.address.formatted_locality
    locality.admin_order_field = 'address__normalized_locality'

    def region(self, obj):
        return obj.address.formatted_region
    region.admin_order_field = 'address__normalized_region'

    def postal_code(self, obj):
        return obj.address.formatted_postal_code
    postal_code.admin_order_field = 'address__normalized_postal_code'

    def point(self, obj):
        return obj.address.point
    point.admin_order_field = 'address__point'


@admin.register(BookingAlbum)
class BookingAlbumAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'booking',
        'width',
        'height',
        'id',
        'date_created'
    )
    readonly_fields = (
        'num_images',
    )
    raw_id_fields = ('images',)
    ordering = ('date_created',)
    fieldsets = (
        (None, {
            'fields': (
                'name',
                'width',
                'height',
            )
        }),
        ('Booking', {
            'fields': (
                'booking',
            )
        }),
        ('Images', {
            'fields': (
                'num_images',
                'images',
            )
        }),
    )

    def num_images(self, obj):
        return obj.images.all().count()

