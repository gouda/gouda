from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.conf.urls import url
from gouda.photographers.views import ScheduleBookingFormView
from gouda.photographers.views import PhotographerApprovalFormView
from gouda.photographers.views import StaffApprovalFormView
from gouda.photographers.views import ClientApprovalFormView
from gouda.photographers.views import ClientRejectionFormView
from gouda.photographers.views import BookingRequestDetailView
from gouda.photographers.views import PhotographerSearchFormView
from gouda.photographers.views import PhotographerSearchResultsView
from gouda.photographers.views import FoodBookingDetailsFormView
from gouda.photographers.views import FoodBookingAddressCreateView
from gouda.photographers.views import BookingDetailsFormView
from gouda.photographers.views import BookingCreateView
from gouda.photographers.views import FoodBookingCreateView
from gouda.photographers.views import BulkBookingCreateView
from gouda.photographers.views import PhotographerRequestCreateView
from gouda.photographers.views import BookingAddressCreateView
from gouda.photographers.views import CustomerCreateView
from gouda.photographers.views import PhotographerCreateView
from gouda.photographers.views import ReviewCreateView
from gouda.photographers.views import BulkBookingFileFormView
from gouda.photographers.views import AcceptBookingRequestView
from gouda.photographers.views import RejectBookingRequestView
from gouda.photographers.views import PhotographerVerificationFormView
from gouda.photographers.views import PhotographerCompletionView
from django.views.decorators.cache import cache_page, never_cache

urlpatterns = [
    url(r'^search/$', PhotographerSearchFormView.as_view(), name='search'),
    url(r'^search/results/$', PhotographerSearchResultsView.as_view(), name='search_results'),
    url(r'^review/(?P<key>[0-9A-Z]{4})/$', ReviewCreateView.as_view(), name='create_review'),

    url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/details/$', BookingDetailsFormView.as_view(), name='get_booking_details'),
    url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/review/$', BookingCreateView.as_view(), name='create_booking'),
    url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/location/$', BookingAddressCreateView.as_view(), name='create_booking_address'),
    # url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/$', BookingDetailsFormView.as_view(), name='checkout'),
    url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/payment/$', CustomerCreateView.as_view(), name='create_customer'),

    url(r'^checkout/food/details/$', FoodBookingDetailsFormView.as_view(), name='get_food_booking_details'),
    url(r'^checkout/food/location/$', FoodBookingAddressCreateView.as_view(), name='create_food_booking_address'),
    url(r'^checkout/food/payment/$', CustomerCreateView.as_view(), name='create_food_customer'),
    url(r'^checkout/food/review/$', FoodBookingCreateView.as_view(), name='create_food_booking'),
    # url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/payment/$', CustomerCreateView.as_view(), name='create_customer'),
    # url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/review/$', BookingCreateView.as_view(), name='create_booking'),
    # url(r'^checkout/(?P<booking_checkout_url>[0-9A-Z]{4})/$', BookingDetailsFormView.as_view(), name='checkout'),

    url(r'^checkout/bulk/file/$', BulkBookingFileFormView.as_view(), name='get_bulk_booking_file'),
    url(r'^checkout/bulk/payment/$', CustomerCreateView.as_view(), name='create_bulk_customer'),
    url(r'^checkout/bulk/review/$', BulkBookingCreateView.as_view(), name='create_bulk_booking'),

    url(r'^bookings/(?P<pk>[0-9]+)/approve/$', never_cache(ClientApprovalFormView.as_view()), name='client_approval'),
    url(r'^bookings/(?P<pk>[0-9]+)/reject/$', never_cache(ClientRejectionFormView.as_view()), name='client_rejection'),
    url(r'^bookings/(?P<pk>[0-9]+)/staff/approve/$', never_cache(StaffApprovalFormView.as_view()), name='staff_approval'),
    url(r'^shoots/(?P<pk>[0-9]+)/approve/$', never_cache(PhotographerApprovalFormView.as_view()), name='photographer_approval'),

    url(r'^bookings/(?P<pk>[0-9]+)/schedule/(?P<token>.*)/$', never_cache(ScheduleBookingFormView.as_view()), name='schedule_booking'),

    url(r'^request/(?P<pk>[0-9]+)/$', never_cache(BookingRequestDetailView.as_view()), name='booking_request_detail'),
    url(r'^request/(?P<pk>[0-9]+)/accept/(?P<token>.*)/$', never_cache(AcceptBookingRequestView.as_view()), name='accept_booking_request'),
    url(r'^request/(?P<pk>[0-9]+)/reject/(?P<token>.*)/$', never_cache(RejectBookingRequestView.as_view()), name='reject_booking_request'),

    url(r'^request/$', PhotographerRequestCreateView.as_view(), name='create_photographer_request'),
    url(r'^join/$', PhotographerCreateView.as_view(), name='create_photographer'),

    url(r'^verify/$', PhotographerVerificationFormView.as_view(), name='braintree_merchant_account_verification'),
    url(r'^complete/(?P<token>[0-9a-zA-Z:\-_]+)/$', PhotographerCompletionView.as_view(), name='photographer_completion')
]
