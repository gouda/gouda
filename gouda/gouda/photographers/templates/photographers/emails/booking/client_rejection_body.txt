{% load tz %}{% autoescape off %}{% timezone booking.address.time_zone|default:None %}Hey {{ booking.photographer.first_name }}!

{{ booking.client.first_name }} rejected the photos you submitted for the shoot below:

Date: {{ booking.time_range.lower|date }}
Time Period: {{ booking.time_range.lower|date:"g:i A" }} - {{ booking.time_range.upper|date:"g:i A" }}
{% if booking.foodbooking %}{{ booking.foodbooking.restaurant_name }}{% endif %}
{{ booking.address.formatted_street_address }}
{{ booking.address.formatted_locality }}, {{ booking.address.formatted_region }} {{ booking.address.formatted_postal_code }}

Here is the link to the rejected shoot:
{{ protocol }}://{{ domain }}{% url 'profiles:shoot_detail' booking.id %}

One of our team members will be in touch soon to help resolve this issue.

If you have any questions or concerns, please contact us ({{ support_email }}).

{{ email_closing }}
{% endtimezone %}{% endautoescape %}