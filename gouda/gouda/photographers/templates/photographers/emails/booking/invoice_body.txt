{% autoescape off %}{{ booking.get_display_name }}{% if booking.foodbooking %}
Restaurant Name: {{ booking.foodbooking.restaurant_name }}{% endif %}
Street Address: {{ booking.address.formatted_street_address }}
City: {{ booking.address.formatted_locality }}
State: {{ booking.address.formatted_region }}
Postal Code: {{ booking.address.formatted_postal_code }}
Number of shots: {{ booking.num_shots }}

Payment Card: {{ credit_card.card_type }} {{ credit_card.last_4 }}, Exp {{ credit_card.expiration_date }}
Approval Date: {{ booking.client_approval_date|date:"D d M Y" }} {{ booking.client_approval_date|time:"H:i T" }}
Payment Date: {{ payment_date|date:"D d M Y" }} {{ payment_date|time:"H:i T" }}
Amount: ${{ booking.total_amount|floatformat }}

If you have any questions or concerns, please contact us ({{ support_email }}).

{{ email_closing }}
{% endautoescape %}