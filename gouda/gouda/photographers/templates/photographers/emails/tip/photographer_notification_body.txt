{% autoescape off %}Hey {{ tip.photographer.first_name }}!

Good news! You just received a tip from {{ tip.client.full_name }}.

{{ tip.invoice }}

{% if tip.message %}Message from {{ tip.client.full_name }}:
{{ tip.message }}{% endif %}

Thanks again for choosing to work with {{ site_display_name }}! Keep up the good work =D.

{{ email_closing }}
{% endautoescape %}