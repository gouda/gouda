{% load tz %}{% autoescape off %}{% timezone "US/Eastern" %}Client: {{ tip.client.full_name }}
Photographer: {{ tip.photographer.full_name }}
Client Email Address: {{ tip.client.email_address }}
Amount: {{ tip.amount }}
Commission: {{ tip.service_fee_amount }}

{% if tip.message %}Message:
{{ tip.message }}{% endif %}
{% endtimezone %}{% endautoescape %}