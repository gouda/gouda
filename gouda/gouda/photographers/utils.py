from __future__ import absolute_import

import random
from datetime import timedelta
import base64

from gouda.users.models import User
from gouda.photographers.models import PhotographerStatusType
from gouda.profiles.utils import tear_down_profiles
from gouda.addresses.utils import get_street_address, get_locality, get_region
from gouda.addresses.utils import get_locality
from gouda.addresses.utils import get_region
from gouda.favorites.utils import tear_down_favorites
from gouda.customers.utils import create_customers
from gouda.utils import get_lorem_text
from gouda.utils import get_title
from gouda.utils import is_int
import os
from gouda.addresses.models import Address
from gouda.photographers.models import BulkBooking
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import BookingAlbum
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import BookingImage
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerImage
from gouda.photographers.models import Review
from gouda.photographers.models import ServiceType
from gouda.postmaster.models import Thread
from gouda.postmaster.models import Message
from gouda.postmaster.models import User as PostmasterUser
from gouda.profiles.utils import create_profiles
from django.utils import timezone
from psycopg2.extras import DateTimeTZRange
from django.core.files.base import ContentFile
from django.db.models import Q

FIXTURES_DIR = os.path.join(os.path.dirname(__file__), 'fixtures')


def get_photographer_image():
    photographer_images_path = os.path.join(FIXTURES_DIR, 'photographer_images/')
    files = os.listdir(photographer_images_path)
    files = [f for f in files if 'DS_Store' not in f]
    image_file_path = os.path.join(photographer_images_path, random.choice(files))
    return ContentFile(open(image_file_path).read(), name=os.path.basename(image_file_path))


def get_booking_image_file(file_name=None):
    booking_images_dir = os.path.join(FIXTURES_DIR, 'booking_images/')
    files = os.listdir(booking_images_dir)
    files = [f for f in files if 'DS_Store' not in f]
    if file_name:
        files = [f for f in files if file_name in f]
    file_paths = [os.path.join(booking_images_dir, f) for f in files]
    image_file_path = random.choice(file_paths)
    return ContentFile(open(image_file_path).read(), name=os.path.basename(image_file_path))


def get_booking_image_data(file_name=None):
    image_file = get_booking_image_file(file_name)
    image_data = {
        'image': base64.b64encode(image_file.file.read())
    }
    return image_data


def create_food_bookings(num_bookings, do_create_images=False, do_assign_photographers=True, do_create_messages=False):
    food_bookings = list()
    client = random.choice(Photographer.objects.all()).profile
    bulk_booking = BulkBooking.objects.create(**{
        'client': client,
        'file': ContentFile(open(os.path.join(FIXTURES_DIR, 'booking_files/postmates.csv')).read(), name='postmates.csv')
    })
    booking_data = bulk_booking.get_booking_data_from_file()
    for b in booking_data:
        start_time = timezone.now() + timedelta(days=random.randint(-10, 10))
        time_range = DateTimeTZRange(lower=start_time, upper=start_time + timedelta(hours=1))
        photographer = random.choice(Photographer.objects.all())
        client = random.choice(Photographer.objects.exclude(id=photographer.id)).profile
        food_booking = FoodBooking.objects.create(**{
            'thread': Thread.objects.create(),
            'client': client,
            'photographer': photographer if do_assign_photographers else None,
            'service_type': ServiceType.FOOD,
            'do_send_client_confirmation': False,
            'amount': b.get('amount'),
            'service_fee_amount': b.get('service_fee_amount'),
            'time_range': time_range,
            'is_sub_merchant_transaction': True,
            'do_hold_in_escrow': True,

            'restaurant_name': b.get('restaurant_name'),
            'contact_phone_number': b.get('contact').get('phone_number'),
            'contact_email_address': b.get('contact').get('email_address'),
            'contact_name': b.get('contact').get('full_name'),
            'is_full_menu_shoot': b.get('is_full_menu_shoot'),
            'is_confirmed_with_restaurant': b.get('is_confirmed_with_restaurant'),
            'items': b.get('items'),

            'address': Address.objects.create(**{
                'full_name': b.get('restaurant_name'),
                'street_address': b.get('address').get('street_address'),
                'sub_premise': b.get('address').get('sub_premise'),
                'postal_code': b.get('address').get('postal_code'),
                'locality': b.get('address').get('locality'),
                'region': b.get('address').get('region'),
                'country': b.get('address').get('country'),

                'normalized_street_address': b.get('address').get('street_address'),
                'normalized_postal_code': b.get('address').get('postal_code'),
                'normalized_locality': b.get('address').get('locality'),
                'normalized_region': b.get('address').get('region'),
            }),
            'available_dates': b.get('available_dates').strip()
        })
        if do_assign_photographers:
            food_booking.sale(do_hold_in_escrow=True)

        if do_create_messages:
            num_messages = random.randint(10, 30)
            for i in range(num_messages):
                users = PostmasterUser.objects.filter(Q(profile__user__email_address=food_booking.client.email_address) | Q(profile__user__email_address=food_booking.photographer.email_address))
                sender = random.choice(users)
                recipient = random.choice(users.exclude(id=sender.id))
                Message.objects.create(**{
                    'thread': food_booking.thread,
                    'body': get_lorem_text(random.randrange(10, 50), 'w'),
                    'sender': sender,
                    'recipient': recipient
                })

        if do_create_images:
            num_images = random.randint(5, 20)
            for i in range(num_images):
                BookingImage.objects.create(**dict({
                    'booking': food_booking,
                    'image': get_booking_image_file(),
                    'name': get_lorem_text(1, 'w')
                }))

        food_bookings.append(food_booking)
        if len(food_bookings) >= num_bookings:
            break

    bulk_booking.delete()

    return food_bookings


def create_bookings(num_bookings, do_create_images=False, do_send_client_confirmation=True, do_assign_photographers=True, do_create_messages=False, do_sale=True, num_albums=1):
    bookings = list()
    for i in range(num_bookings):
        start_time = timezone.now() + timedelta(days=random.randint(-10, 10))
        time_range = DateTimeTZRange(lower=start_time, upper=start_time + timedelta(hours=1))
        photographer = random.choice(Photographer.objects.all())
        client = random.choice(Photographer.objects.exclude(id=photographer.id)).profile

        street_address = get_street_address().strip()
        postal_code = random.randrange(10000, 99999)
        locality = get_locality().strip()
        region = get_region().strip()

        booking = Booking.objects.create(**dict({
            'do_send_client_confirmation': do_send_client_confirmation,
            'is_sub_merchant_transaction': True,
            'do_hold_in_escrow': True,
            'photographer': photographer if do_assign_photographers else None,
            'client': client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': random.randint(100, 500),
            'service_fee_amount': 10,
            'phone_number': '5555555555',
            'thread': Thread.objects.create(),
            'address': Address.objects.create(**{
                'full_name': client.full_name,
                'street_address': street_address,
                'postal_code': postal_code,
                'locality': locality,
                'region': region,
                'normalized_street_address': street_address,
                'normalized_postal_code': postal_code,
                'normalized_locality': locality,
                'normalized_region': region,
                'country': 'US'
            }),
            'display_name': get_lorem_text(random.randrange(5, 10), 'w')[:50]
        }))
        if do_assign_photographers:
            if do_sale:
                booking.sale(do_hold_in_escrow=True)
        bookings.append(booking)

        if do_create_messages:
            num_messages = random.randint(10, 30)
            for i in range(num_messages):
                users = PostmasterUser.objects.filter(Q(profile__user__email_address=booking.client.email_address) | Q(profile__user__email_address=booking.photographer.email_address))
                sender = random.choice(users)
                recipient = random.choice(users.exclude(id=sender.id))
                Message.objects.create(**{
                    'thread': booking.thread,
                    'body': get_lorem_text(random.randrange(10, 50), 'w'),
                    'sender': sender,
                    'recipient': recipient
                })

        if do_create_images:
            create_booking_images(booking, num_albums)
    return bookings


def create_booking_images(booking, num_albums=1):
    booking_images = list()
    albums = list()
    for i in range(num_albums):
        albums.append(BookingAlbum.objects.create(**{
            'booking': booking,
            'name': get_lorem_text(1, 'w'),
            'width': 500,
            'height': 500
        }))
    for album in albums:
        num_images = random.randint(5, 10)
        for i in range(num_images):
            booking_image = BookingImage.objects.create(**dict({
                'booking': booking,
                'image': get_booking_image_file(),
                'name': get_lorem_text(1, 'w')
            }))
            booking_images.append(booking_image)
            album.images.add(booking_image)
    return booking_images


def create_photographers(num_photographers, num_profiles=None, do_create_marketplace=False, is_superuser=False):
    profiles = create_profiles(num_profiles if num_profiles else num_photographers, is_superuser=is_superuser)
    photographers = list()
    regions = ['California', 'Texas', 'Maryland']
    for profile in profiles:
        address = Address.objects.create(
            full_name=profile.full_name,
            street_address='1234 Baker Street',
            postal_code='12345',
            locality='Bakersfield',
            region=random.choice(regions),
            country='US'
        )
        photographer = Photographer.objects.create(
            profile=profile,
            phone_number='555-555-5555',
            status=PhotographerStatusType.ACTIVE,
            address=address
        )
        photographers.append(photographer)

    # if num_images:
    #     for photographer in photographers:
    #         for i in range(num_images if is_int(num_images) else num_images()):
    #             image = get_photographer_image()
    #             is_primary = i == 0
    #             caption = get_lorem_text(random.randrange(0, 25), 'w')
    #             image = PhotographerImage.objects.create(
    #                 image=image,
    #                 is_primary=is_primary,
    #                 photographer=photographer,
    #                 caption=caption
    #             )
    #             assert(image is not None)

    # if num_reviews:
    #     for photographer in photographers:
    #         for i in range(num_reviews if is_int(num_reviews) else num_reviews()):
    #             review = Review.objects.create(
    #                 photographer=photographer,
    #                 profile=random.choice(profiles),
    #                 title=get_title(5, 30),
    #                 content=get_lorem_text(random.randrange(1, 3), 'b'),
    #                 rating=max(1, min(5, int(random.gauss(4, 2))))
    #             )
    if do_create_marketplace:
        create_customers()

    return photographers


def tear_down_photographers():
    Photographer.objects.all().delete()
    PhotographerImage.objects.all().delete()
    Review.objects.all().delete()
    Booking.objects.all().delete()
    Address.objects.all().delete()
    BulkBooking.objects.all().delete()
    BookingRequest.objects.all().delete()
    tear_down_profiles()
    tear_down_favorites()
