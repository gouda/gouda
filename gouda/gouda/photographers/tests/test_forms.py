from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import random
from datetime import datetime
from datetime import date
from datetime import time
from datetime import timedelta
from decimal import Decimal

import os
from django.conf import settings
from django.core import mail
from django.core.files.base import ContentFile
from django.test import TestCase
from django.utils import timezone
from gouda.addresses.models import Address
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import Coupon
from gouda.photographers.forms import BookingUpdateForm
from gouda.photographers.forms import PhotographerApprovalForm
from gouda.photographers.forms import ScheduleBookingForm
from gouda.photographers.forms import StaffApprovalForm
from gouda.photographers.forms import ClientApprovalForm
from gouda.photographers.forms import ClientRejectionForm
from gouda.photographers.forms import BookingDetailsForm
from gouda.photographers.forms import BookingForm
from gouda.photographers.forms import BulkBookingForm
from gouda.photographers.forms import FoodBookingAddressForm
from gouda.photographers.forms import FoodBookingDetailsForm
from gouda.photographers.forms import FoodBookingForm
from gouda.photographers.forms import PhotographerForm
from gouda.photographers.forms import PhotographerRequestForm
from gouda.photographers.forms import PhotographerSearchForm
from gouda.photographers.forms import ReviewForm
from gouda.photographers.forms import BookingAdminForm
from gouda.photographers.forms import FoodBookingAdminForm
from gouda.photographers.forms import CreateFoodBookingForm
from gouda.photographers.models import BulkBooking
from gouda.photographers.models import Booking
from gouda.profiles.models import Profile
from gouda.photographers.models import ServiceType
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import Review
from gouda.photographers.models import ServiceType
from gouda.photographers.models import TimePeriodType
from gouda.photographers.models import Tip
from gouda.photographers.utils import create_food_bookings
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers
from gouda.postmaster.models import Thread
from gouda.profiles.utils import create_profiles
from gouda.users.models import User
from psycopg2.extras import DateTimeTZRange
from gouda.customers.models import BraintreeEscrowStatusType
from gouda.customers.models import BraintreeTransactionStatusType
from gouda.customers.utils import braintree
from localflavor.us.us_states import STATE_CHOICES
from gouda.addresses.utils import get_locality
from gouda.photographers.forms import BookingListForm
import pytz
from unittest import skip

AMOUNT_WITHHELD_PERCENTAGE = getattr(settings, 'AMOUNT_WITHHELD_PERCENTAGE')
FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')
DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')


class CreateFoodBookingFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(CreateFoodBookingFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

        # Geocoding...lolz...
        for photographer in Photographer.objects.all():
            photographer.address.point = 'POINT(%f %f)' % (-118.593979, 33.801118)
            photographer.address.save()

    @classmethod
    def tearDownClass(cls):
        super(CreateFoodBookingFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.client = Profile.objects.all()[0]
        time_zone = 'US/Central'
        time_range = DateTimeTZRange(
            lower=pytz.timezone(time_zone).localize(datetime.today().replace(hour=9, microsecond=0)),
            upper=pytz.timezone(time_zone).localize(datetime.today().replace(hour=12, microsecond=0)),
        )
        self.data = {
            'restaurant_name': 'Sizzler\'s',
            'contact_email_address': 'success+sterlingarcher@simulator.amazonses.com',
            'contact_phone_number': '5555555',
            'contact_name': 'Sterling Archer',

            'shot_list': ContentFile(
                open(os.path.join(FIXTURES_DIR, 'shot_list.csv')).read(),
                name='shot_list'
            ),

            'street_address': '380 S. Lake Ave',
            'sub_premise': 'Suite 106',
            'postal_code': '91101',
            'locality': 'Pasadena',
            'region': 'CA',
            'date': date(time_range.lower.year, time_range.lower.month, time_range.lower.day),
            'start_time': time(
                time_range.lower.hour,
                time_range.lower.minute,
                time_range.lower.second,
                time_range.lower.microsecond
            ),
            'stop_time': time(
                time_range.upper.hour,
                time_range.upper.minute,
                time_range.upper.second,
                time_range.upper.microsecond
            ),
        }

    def tearDown(self):
        Booking.objects.all().delete()
        BookingRequest.objects.all().delete()

    def test_init(self):
        form = CreateFoodBookingForm(client=self.client)
        self.assertIsNotNone(form)

    def test_create_food_booking(self):
        data = self.data.copy()
        form = CreateFoodBookingForm(client=self.client, data=data)
        self.assertTrue(form.is_valid(), msg='%s' % form.errors)
        food_booking = form.save()
        self.assertEqual(FoodBooking.objects.all().count(), 1)
        self.assertEqual(FoodBooking.objects.filter(id=food_booking.id).count(), 1)
        self.assertEqual(food_booking.contact_name, data.get('contact_name'))

    def test_send_booking_requests(self):
        data = self.data.copy()
        form = CreateFoodBookingForm(client=self.client, data=data)
        self.assertTrue(form.is_valid(), msg='%s' % form.errors)
        food_booking = form.save()
        self.assertGreater(BookingRequest.objects.all().count(), 0)

    def test_contact_name_required(self):
        data = self.data.copy()
        data.pop('contact_name')
        form = CreateFoodBookingForm(client=self.client, data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('contact_name' in form.errors)

    def test_contact_email_address_required(self):
        data = self.data.copy()
        data.pop('contact_email_address')
        form = CreateFoodBookingForm(client=self.client, data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('contact_email_address' in form.errors)

    def test_contact_phone_number_required(self):
        data = self.data.copy()
        data.pop('contact_phone_number')
        form = CreateFoodBookingForm(client=self.client, data=data)
        self.assertFalse(form.is_valid())
        self.assertTrue('contact_phone_number' in form.errors)

    def test_staff_notification(self):
        data = self.data.copy()
        form = CreateFoodBookingForm(client=self.client, data=data)
        self.assertTrue(form.is_valid(), msg='%s' % form.errors)
        form.save()
        notification_mail = [m for m in mail.outbox if 'A new booking was created by' in m.subject][0]
        self.assertIsNotNone(notification_mail)
        self.assertEqual(SERVER_EMAIL, notification_mail.from_email)
        self.assertEqual(STAFF_EMAILS, notification_mail.to)


class BookingAdminFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAdminFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingAdminFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.data = {
            'client': Profile.objects.all()[0].id,
            'service_type': ServiceType.FASHION,

            'amount': 600,
            'service_fee_amount': 300,

            'street_address': '380 S. Lake Ave',
            'sub_premise': 'Suite 106',
            'postal_code': '91101',
            'locality': 'Pasadena',
            'region': 'CA',
            'country': 'US'
        }

    def test_init(self):
        BookingAdminForm()

    def test_create_food_booking(self):
        data = self.data.copy()
        form = BookingAdminForm(data)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertEqual(Booking.objects.filter(id=booking.id).count(), 1)
        self.assertEqual(booking.client.id, data.get('client'))
        self.assertEqual(booking.service_type, data.get('service_type'))


class FoodBookingAdminFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingAdminFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingAdminFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.data = {
            'service_type': 0,
            'client': Profile.objects.all()[0].id,

            'restaurant_name': 'Float',
            'items': '',

            'amount': 600,
            'service_fee_amount': 300,

            'street_address': '380 S. Lake Ave',
            'sub_premise': 'Suite 106',
            'postal_code': '91101',
            'locality': 'Pasadena',
            'region': 'CA',
            'country': 'US'
        }

    def test_init(self):
        FoodBookingAdminForm()

    def test_create_food_booking(self):
        form = FoodBookingAdminForm(self.data)
        self.assertTrue(form.is_valid())
        food_booking = form.save()
        self.assertEqual(FoodBooking.objects.filter(id=food_booking.id).count(), 1)
        self.assertEqual(FoodBooking.objects.get(id=food_booking.id).service_type, ServiceType.FOOD)


class BookingUpdateFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingUpdateFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingUpdateFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        time_zone = 'US/Central'
        self.booking = create_bookings(1)[0]
        self.booking.address.time_zone = time_zone
        self.booking.contact_name = 'Sterling Archer'
        self.booking.contact_email_address = 'sterlingarcher@gmail.com'
        self.booking.contact_phone_number = '5555555555'
        self.booking.save()
        self.booking.address.save()

        time_zone = 'US/Central'
        self.time_range = DateTimeTZRange(
            lower=pytz.timezone(time_zone).localize(datetime.today().replace(hour=9, microsecond=0)),
            upper=pytz.timezone(time_zone).localize(datetime.today().replace(hour=12, microsecond=0)),
        )
        self.data = {
            'date': date(self.time_range.lower.year, self.time_range.lower.month, self.time_range.lower.day),
            'start_time': time(
                self.time_range.lower.hour,
                self.time_range.lower.minute,
                self.time_range.lower.second,
                self.time_range.lower.microsecond
            ),
            'stop_time': time(
                self.time_range.upper.hour,
                self.time_range.upper.minute,
                self.time_range.upper.second,
                self.time_range.upper.microsecond
            ),
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_init(self):
        BookingUpdateForm()

    def test_update_date(self):
        form = BookingUpdateForm(self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        booking = Booking.objects.get(id=booking.id)
        self.assertIsNotNone(booking.time_range)
        self.assertEqual(booking.time_range.lower, self.time_range.lower)
        self.assertEqual(booking.time_range.upper, self.time_range.upper)

    @skip('')
    def test_invalid_time_range(self):
        data = self.data.copy()
        data['start_time'] = time(7, 0, 0, 0)
        data['stop_time'] = time(8, 0, 0, 0)
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    @skip('')
    def test_invalid_start_time_too_early(self):
        data = self.data.copy()
        data['start_time'] = time(7, 0, 0, 0)
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    @skip('')
    def test_invalid_start_time_too_late(self):
        data = self.data.copy()
        data['start_time'] = time(17, 0, 0, 0)
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    @skip('')
    def test_invalid_stop_time_too_early(self):
        data = self.data.copy()
        data['start_time'] = time(7, 0, 0, 0)
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    @skip('')
    def test_invalid_stop_time_too_late(self):
        data = self.data.copy()
        data['stop_time'] = time(17, 0, 0, 0)
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_date(self):
        data = self.data.copy()
        data['date'] = date(2000, 1, 1)
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_blank_data(self):
        data = {}
        form = BookingUpdateForm(data, instance=self.booking)
        self.assertTrue(form.is_valid())


class ScheduleBookingFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ScheduleBookingFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

        # Geocoding...lolz...
        for photographer in Photographer.objects.all():
            photographer.address.point = 'POINT(%f %f)' % (-122.4194, 37.7749)
            photographer.address.save()
        photographer = Photographer.objects.all()[0]
        photographer.address.point = 'POINT(%f %f)' % (-118.2437, 34.0522)
        photographer.address.save()
        photographer.save()

    @classmethod
    def tearDownClass(cls):
        super(ScheduleBookingFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1)[0]
        self.booking.time_range = None
        self.booking.contact_name = 'Sterling Archer'
        self.booking.contact_email_address = 'sterlingarcher@gmail.com'
        self.booking.contact_phone_number = '5555555555'
        self.booking.save()

        time_zone = 'US/Central'
        self.time_range = DateTimeTZRange(
            lower=pytz.timezone(time_zone).localize(datetime.today().replace(hour=9, microsecond=0)),
            upper=pytz.timezone(time_zone).localize(datetime.today().replace(hour=12, microsecond=0)),
        )

        self.data = {
            'time_zone': time_zone,
            'date': date(self.time_range.lower.year, self.time_range.lower.month, self.time_range.lower.day),
            'start_time': time(
                self.time_range.lower.hour,
                self.time_range.lower.minute,
                self.time_range.lower.second,
                self.time_range.lower.microsecond
            ),
            'stop_time': time(
                self.time_range.upper.hour,
                self.time_range.upper.minute,
                self.time_range.upper.second,
                self.time_range.upper.microsecond
            ),
            'latitude': random.uniform(33.801118, 34.357325),
            'longitude': random.uniform(-118.593979, -117.759018)
        }

    def tearDown(self):
        Booking.objects.all().delete()
        BookingRequest.objects.all().delete()

    def test_init(self):
        ScheduleBookingForm()

    def test_create_booking_requests(self):
        form = ScheduleBookingForm(self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertEqual(BookingRequest.objects.filter(booking=booking).count(), 1)

    def test_invalid_time_range(self):
        data = self.data.copy()
        data['start_time'] = time(7, 0, 0, 0)
        data['stop_time'] = time(8, 0, 0, 0)
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_start_time_too_early(self):
        data = self.data.copy()
        data['start_time'] = time(7, 0, 0, 0)
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_start_time_too_late(self):
        data = self.data.copy()
        data['start_time'] = time(18, 0, 0, 0)
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_stop_time_too_early(self):
        data = self.data.copy()
        data['start_time'] = time(7, 0, 0, 0)
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_stop_time_too_late(self):
        data = self.data.copy()
        data['stop_time'] = time(21, 0, 0, 0)
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_update_date(self):
        form = ScheduleBookingForm(self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        booking = Booking.objects.get(id=booking.id)
        self.assertIsNotNone(booking.time_range)
        self.assertEqual(booking.time_range.lower, self.time_range.lower)
        self.assertEqual(booking.time_range.upper, self.time_range.upper)

    def test_update_address_point(self):
        form = ScheduleBookingForm(self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertIsNotNone(booking.address.point)
        self.assertEqual(round(booking.address.point.y, 3), round(self.data.get('latitude'), 3))
        self.assertEqual(round(booking.address.point.x, 3), round(self.data.get('longitude'), 3))

    def test_update_address_time_zone(self):
        form = ScheduleBookingForm(self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertIsNotNone(booking.address.time_zone)
        self.assertEqual(booking.address.time_zone.zone, self.data.get('time_zone'))

    def test_contact_notification(self):
        form = ScheduleBookingForm(self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        form.save()

        # Make sure staff notification gets sent
        outbox = [m for m in mail.outbox if 'Your SHOTZU Booking' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(self.booking.contact_email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_invalid_date(self):
        data = self.data.copy()
        data['date'] = date(2000, 1, 1)
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_with_only_required_fields(self):
        data = self.data.copy()
        data.pop('latitude')
        data.pop('longitude')
        data.pop('time_zone')
        form = ScheduleBookingForm(data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()

    # def test_set_menu_items(self):
    #     food_bookings = create_food_bookings(1)
    #     data = self.data.copy()
    #     data['date'] = date(2000, 1, 1)
    #     form = ScheduleBookingForm(data, instance=self.booking)
    #     self.assertFalse(form.is_valid())


class PhotographerApprovalFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(PhotographerApprovalFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(PhotographerApprovalFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1)[0]

        self.data = {
            'did_photographer_approve': True,
            'num_shots': 150
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_init(self):
        PhotographerApprovalForm()

    def test_invalid_approval(self):
        data = self.data.copy()
        data['did_photographer_approve'] = False
        form = PhotographerApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_num_shots(self):
        data = self.data.copy()
        data['num_shots'] = 0
        form = PhotographerApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_booking_already_approved_by_photographer(self):
        self.booking.did_photographer_approve = True
        self.booking.save()
        data = self.data.copy()
        form = PhotographerApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_booking_already_approved_by_client(self):
        self.booking.did_client_approve = True
        self.booking.save()
        data = self.data.copy()
        form = PhotographerApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_photographer_approval(self):
        form = PhotographerApprovalForm(data=self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertTrue(booking.did_photographer_approve)
        self.assertEqual(booking.num_shots, self.data.get('num_shots'))

        # Make sure notification gets sent
        outbox = [m for m in mail.outbox if 'has finalized a booking' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(SERVER_EMAIL, notification_mail.from_email)
        self.assertEqual(STAFF_EMAILS, notification_mail.to)


class StaffApprovalFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(StaffApprovalFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(StaffApprovalFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_sale=False)[0]
        self.booking.did_photographer_approve = True
        self.booking.num_shots = 25
        self.booking.save()

        self.data = {
            'did_staff_approve': True,
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_init(self):
        StaffApprovalForm()

    def test_invalid_data(self):
        data = self.data.copy()
        data['did_staff_approve'] = False
        form = StaffApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_staff_approval(self):
        form = StaffApprovalForm(data=self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        booking = Booking.objects.get(id=booking.id)
        self.assertTrue(booking.did_staff_approve)
        self.assertIsNotNone(booking.staff_approval_date)
        self.assertTrue(booking.media_file)

    def test_staff_approval_without_photographer_approval(self):
        self.booking.did_photographer_approve = False
        self.booking.save()
        form = StaffApprovalForm(data=self.data, instance=self.booking)
        self.assertFalse(form.is_valid())


class ClientRejectionFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ClientRejectionFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ClientRejectionFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_sale=False)[0]
        self.booking.did_staff_approve = True
        self.booking.num_shots = 25
        self.booking.save()

        self.data = {
            'did_client_reject': True,
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_init(self):
        ClientRejectionForm()

    def test_invalid_data(self):
        data = self.data.copy()
        data['did_client_reject'] = False
        form = ClientRejectionForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_booking_rejection_not_approved_by_staff(self):
        self.booking.did_staff_approve = False
        self.booking.save()
        data = self.data.copy()
        form = ClientRejectionForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_booking_already_rejected_by_client(self):
        self.booking.did_client_reject = True
        self.booking.save()
        data = self.data.copy()
        form = ClientRejectionForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_client_rejection(self):
        form = ClientRejectionForm(data=self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertTrue(booking.did_client_reject)


class ClientApprovalFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ClientApprovalFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ClientApprovalFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_sale=False)[0]
        self.booking.did_staff_approve = True
        self.booking.num_shots = 25
        self.booking.save()

        self.data = {
            'did_client_approve': True,
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_init(self):
        ClientApprovalForm()

    def test_invalid_data(self):
        data = self.data.copy()
        data['did_client_approve'] = False
        form = ClientApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_booking_not_approved_by_staff(self):
        self.booking.did_staff_approve = False
        self.booking.save()
        data = self.data.copy()
        form = ClientApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_invalid_booking_already_approved_by_client(self):
        self.booking.did_client_approve = True
        self.booking.save()
        data = self.data.copy()
        form = ClientApprovalForm(data=data, instance=self.booking)
        self.assertFalse(form.is_valid())

    def test_client_approval(self):
        self.assertFalse(self.booking.did_client_approve)
        self.assertFalse(self.booking.did_client_reject)
        form = ClientApprovalForm(data=self.data, instance=self.booking)
        self.assertTrue(form.is_valid())
        booking = form.save()
        self.assertTrue(booking.did_client_approve)

        # Check
        self.assertEqual(booking.braintree_id, '')

        # Make sure invoice gets sent
        outbox = [m for m in mail.outbox if 'SHOTZU Invoice' == m.subject]
        self.assertEqual(len(outbox), 1)
        receipt_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, receipt_mail.from_email)
        self.assertEqual(booking.client.email_address, receipt_mail.to[0])
        self.assertEqual(STAFF_EMAILS, receipt_mail.bcc)


class ReviewFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ReviewFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        photographer = photographers[0]
        client = photographers[1].profile
        booking = Booking.objects.create(**dict({
            'thread': Thread.objects.create(),
            'photographer': photographer,
            'client': client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        }))

    @classmethod
    def tearDownClass(cls):
        super(ReviewFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.data = dict({
            'booking': self.booking.id,
            'content': 'Hello World!',
            'rating': 5,
            'title': 'This is the best evah',
            'reviewer': self.booking.client.id
        })

    def tearDown(self):
        super(ReviewFormTestCase, self).tearDown()
        Booking.objects.all().delete()
        Review.objects.all().delete()
        Tip.objects.all().delete()

    def test_init(self):
        ReviewForm()

    def test_create_review(self):
        review_form = ReviewForm(self.data)
        self.assertTrue(review_form.is_valid())
        review = review_form.save()
        self.assertIsNotNone(review)
        self.assertEqual(Review.objects.all().count(), 1)
        self.assertEqual(Tip.objects.all().count(), 0)

    def test_create_review_with_tip(self):
        data = self.data.copy()
        data['amount'] = 350
        review_form = ReviewForm(data)
        self.assertTrue(review_form.is_valid())
        review = review_form.save()
        self.assertIsNotNone(review)
        self.assertEqual(Review.objects.all().count(), 1)
        self.assertEqual(Tip.objects.all().count(), 1)


class PhotographerFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(PhotographerFormTestCase, cls).setUpClass()
        create_profiles(10)

    @classmethod
    def tearDownClass(cls):
        super(PhotographerFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def tearDown(self):
        super(PhotographerFormTestCase, self).tearDown()
        Photographer.objects.all().delete()
        BraintreeMerchantAccount.objects.all().delete()

    def setUp(self):
        super(PhotographerFormTestCase, self).setUp()
        self.user = User.objects.all()[0]
        date_of_birth = date.today() - timedelta(days=365 * 28)
        self.data = {
            'user': self.user.pk,

            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'CA',
            'individual_street_address': '1504 Parkpath Way',
            'individual_month': int(date_of_birth.strftime('%m')),
            'individual_day': int(date_of_birth.strftime('%d')),
            'individual_year': int(date_of_birth.strftime('%Y')),
            'individual_email': self.user.email_address,
            'individual_first_name': self.user.first_name,
            'individual_last_name': self.user.last_name,
            'individual_phone': '4437421240',

            # 'business_locality': 'Bakersfield',
            # 'business_postal_code': '93311',
            # 'business_region': 'CA',
            # 'business_street_address': '1504 Parkpath Way',
            # 'business_dba_name': 'Danger Zone R\' Us',
            # 'business_legal_name': 'Danger Zone R\' Us',
            # 'business_tax_id': '123457689',

            'funding_email': self.user.email_address,
            'funding_mobile_phone': '5555555555',
            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        }
        self.assertEqual(Photographer.objects.all().count(), 0)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)

    def test_init(self):
        PhotographerForm()

    def test_create_photographer(self):
        photographer_form = PhotographerForm(data=self.data)
        self.assertTrue(photographer_form.is_valid())
        photographer = photographer_form.save()
        self.assertIsNotNone(photographer)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

    def test_create_new_braintree_merchant_account(self):
        photographer_form = PhotographerForm(data=self.data)
        self.assertTrue(photographer_form.is_valid())
        photographer = photographer_form.save()
        self.assertIsNotNone(photographer)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

        # Delete Braintree merchant account
        BraintreeMerchantAccount.objects.all().delete()

        # Create another Braintree merchant account with the same photgrapher
        self.assertEqual(Photographer.objects.all().count(), 1)
        photographer_form = PhotographerForm(data=self.data)
        self.assertTrue(photographer_form.is_valid())
        photographer = photographer_form.save()
        self.assertIsNotNone(photographer)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

    def test_update_photographer(self):
        photographer_form = PhotographerForm(data=self.data)
        self.assertTrue(photographer_form.is_valid())
        photographer = photographer_form.save()
        self.assertIsNotNone(photographer)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

        # Update
        data = self.data.copy()
        data.update({
            'funding_mobile_phone': '4435106349',
            'funding_account_number': '1123581322',
            'funding_routing_number': '071000013',
        })
        photographer_form = PhotographerForm(data=data)
        self.assertTrue(photographer_form.is_valid())
        photographer = photographer_form.save()
        self.assertIsNotNone(photographer)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

        # Validate Braintree data
        merchant_account = braintree.MerchantAccount.find(photographer.braintree_merchant_account.braintree_id)
        self.assertEqual(merchant_account.individual_details.email, self.data.get('individual_email'))
        self.assertEqual(merchant_account.funding_details.email, self.data.get('funding_email'))
        self.assertEqual(merchant_account.funding_details.mobile_phone, data.get('funding_mobile_phone'))
        self.assertEqual(merchant_account.funding_details.account_number_last_4, data.get('funding_account_number')[-4:])
        self.assertEqual(merchant_account.funding_details.routing_number, data.get('funding_routing_number'))


class BookingFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.data = dict({
            'thread': Thread.objects.create().id,
            'display_name': 'DANGER ZONE!!!',
            'photographer': self.photographer.id,
            'client': self.client.id,
            'date': date.today(),
            'start_time': '4:00 PM',
            'end_time': '5:00 PM',
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            }).id
        })

    def tearDown(self):
        Booking.objects.all().delete()
        Coupon.objects.all().delete()

    def test_init(self):
        BookingForm()

    def test_create_booking(self):
        self.assertEqual(Booking.objects.all().count(), 0)
        booking_form = BookingForm(self.data)
        self.assertTrue(booking_form.is_valid())
        booking = booking_form.save()
        self.assertIsNotNone(booking)
        self.assertFalse(booking.braintree_id)
        self.assertEqual(Booking.objects.all().count(), 1)

    def test_create_booking_with_custom_amount(self):
        self.assertEqual(Booking.objects.all().count(), 0)
        booking_form = BookingForm(self.data, amount=9000, amount_withheld=900)
        self.assertTrue(booking_form.is_valid())
        booking = booking_form.save()
        self.assertIsNotNone(booking)
        self.assertEqual(Booking.objects.all().count(), 1)
        self.assertEqual(booking.amount, 9000)
        self.assertEqual(booking.service_fee_amount, 900)
        self.assertFalse(booking.braintree_id)

    def test_create_booking_with_coupon(self):
        coupon = Coupon.objects.create(**{
            'code': 'ILIKEPIE!!!',
            'amount_off': 50
        })
        self.assertEqual(Booking.objects.all().count(), 0)
        booking_form = BookingForm(self.data, amount=9000, amount_withheld=900, coupon_id=coupon.id)
        self.assertTrue(booking_form.is_valid())
        booking = booking_form.save()
        self.assertIsNotNone(booking)
        self.assertEqual(Booking.objects.all().count(), 1)
        self.assertEqual(booking.amount, Decimal(9000 - coupon.amount_off))
        self.assertEqual(booking.service_fee_amount, 900)
        self.assertIsNotNone(booking.coupon)
        self.assertEqual(booking.coupon.id, coupon.id)

    def verify_braintree_data(self, booking):
        transaction = braintree.Transaction.find(booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertEqual(Decimal(transaction.service_fee_amount), self.data.get('service_fee_amount'))
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        if booking.do_hold_in_escrow:
            self.assertEqual(
                transaction.escrow_status,
                BraintreeEscrowStatusType.labels.get(
                    BraintreeEscrowStatusType.HOLD_PENDING
                )
            )
        else:
            self.assertIsNone(transaction.escrow_status)


class PhotographerSearchFormTestCase(TestCase):
    data = {
        'region': 'CA',
        'service_type': ServiceType.INSPECTIONS,
        'date': date.today(),
        'time_period': TimePeriodType.EVENING
    }

    def test_init(self):
        PhotographerSearchForm()

    def test_valid_data(self):
        photographer_search_form = PhotographerSearchForm(self.data)
        self.assertTrue(photographer_search_form.is_valid())

    def test_blank_data(self):
        photographer_search_form = PhotographerSearchForm({})
        self.assertFalse(photographer_search_form.is_valid())
        for k in self.data.keys():
            self.assertIn(k, photographer_search_form.errors)


class PhotographerRequestFormTestCase(TestCase):
    data = {
        'full_name': 'Sterling Archer',
        'phone_number': '555-555-5555',
        'email_address': 'success+sterlingarcher@simulator.amazonses.com',
        'service_type': ServiceType.REAL_ESTATE,
        'locality': 'New York',
        'region': 'NY',
        'message': 'Danger ZONE!!!',
        'postal_code': '12345',
        'time_period': TimePeriodType.ANYTIME,
        'date': timezone.now()
    }

    def tearDown(self):
        PhotographerRequest.objects.all().delete()

    def test_init(self):
        PhotographerRequestForm()

    def test_create_photographer_request(self):
        self.assertEqual(PhotographerRequest.objects.all().count(), 0)
        photographer_request_form = PhotographerRequestForm(self.data)
        photographer_request_form.is_valid()
        self.assertTrue(photographer_request_form.is_valid())
        photographer_request = photographer_request_form.save()
        self.assertIsNotNone(photographer_request)
        self.assertEqual(PhotographerRequest.objects.all().count(), 1)

        # Make sure emails get sent
        self.assertGreaterEqual(len(mail.outbox), 2)

    def test_required_fields(self):
        for k in self.data.keys():
            if k in ['message', 'locality', 'region', 'postal_code']:
                continue
            data = self.data.copy()
            data.pop(k)
            photographer_request_form = PhotographerRequestForm(data)
            self.assertFalse(photographer_request_form.is_valid())

    def test_on_demand_request(self):
        self.assertEqual(PhotographerRequest.objects.all().count(), 0)
        data = self.data.copy()
        data.pop('locality')
        data.pop('postal_code')
        photographer_request_form = PhotographerRequestForm(data=self.data, is_on_demand=True)
        self.assertTrue(photographer_request_form.is_valid())
        photographer_request = photographer_request_form.save()
        self.assertIsNotNone(photographer_request)
        self.assertEqual(PhotographerRequest.objects.all().count(), 1)


class BookingDetailsFormTestCase(TestCase):
    data = {
        'display_name': 'DANGER ZONE!!!',
        'service_type': ServiceType.PHOTOGRAPHY,
        'date': date.today(),
        'start_time': time(0, 0, 0, 0),
        'end_time': time(23, 59, 59, 999999),
        'phone_number': '5555555555',
    }

    def test_init(self):
        BookingDetailsForm()

    def test_create_booking_details(self):
        booking_details_form = BookingDetailsForm(self.data)
        self.assertTrue(booking_details_form.is_valid())

    def test_invalid_date(self):
        data = self.data.copy()
        data['date'] = date.today() - timedelta(days=9000)
        booking_details_form = BookingDetailsForm(data)
        self.assertFalse(booking_details_form.is_valid())


class FoodBookingDetailsFormTestCase(TestCase):
    data = {
        'display_name': 'DANGER ZONE!!!',
        'service_type': ServiceType.FOOD,
        'date': date.today(),
        'start_time': time(0, 0, 0, 0),
        'end_time': time(23, 59, 59, 999999),
        'phone_number': '5555555555',

        'contact_name': 'Barry Dylan',
        'contact_phone_number': '5555555555',
        'is_full_menu_shoot': True,
        'is_confirmed_with_restaurant': True,
        'items': 'Crispy Duck, Pad Thai, Moo Krab'
    }

    def test_init(self):
        FoodBookingDetailsForm()

    def test_create_food_booking_details(self):
        form = FoodBookingDetailsForm(self.data)
        self.assertTrue(form.is_valid())

    def test_invalid_date(self):
        data = self.data.copy()
        data['date'] = date.today() - timedelta(days=9000)
        form = FoodBookingDetailsForm(data)
        self.assertFalse(form.is_valid())

    def test_without_is_full_menu_shoot(self):
        data = self.data.copy()
        data.pop('is_full_menu_shoot')
        form = FoodBookingDetailsForm(data)
        self.assertTrue(form.is_valid())

    def test_without_is_confirmed_with_restaurant(self):
        data = self.data.copy()
        data.pop('is_confirmed_with_restaurant')
        form = FoodBookingDetailsForm(data)
        self.assertTrue(form.is_valid())


class FoodBookingAddressFormTestCase(TestCase):
    data = {
        'restaurant_name': 'My Thai',
        'street_address': '5719 Old Buggy Court',
        'postal_code': '21045',
        'locality': 'Columbia',
        'region': 'Maryland',
        'country': 'US'
    }

    def tearDown(self):
        Address.objects.all().delete()

    def test_init(self):
        FoodBookingAddressForm()

    def test_create_address(self):
        self.assertEqual(Address.objects.all().count(), 0)
        form = FoodBookingAddressForm(self.data)
        self.assertTrue(form.is_valid())
        address = form.save()
        self.assertIsNotNone(address)
        self.assertEqual(Address.objects.all().count(), 1)

        data = self.data.copy()
        data.pop('restaurant_name')
        for k in data.keys():
            self.assertEqual(self.data.get(k), getattr(address, k))


class FoodBookingFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingFormTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.data = dict({
            'thread': Thread.objects.create().id,
            'display_name': 'DANGER ZONE!!!',
            'photographer': self.photographer.id,
            'client': self.client.id,
            'date': date.today(),
            'start_time': '4:00 PM',
            'end_time': '5:00 PM',
            'service_type': ServiceType.FOOD,
            'message': 'Hello World!',
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            }).id,
            'restaurant_name': 'My Thai',
            'contact_name': 'Barry Dylan',
            'contact_phone_number': '5555555555',
            'contact_email_address': 'Barry@gmail.com',
            'is_full_menu_shoot': True,
            'is_confirmed_with_restaurant': True,
            'items': 'Crispy Duck, Pad Thai, Moo Krob'
        })

    def tearDown(self):
        FoodBooking.objects.all().delete()
        Booking.objects.all().delete()
        Coupon.objects.all().delete()

    def test_init(self):
        BookingForm()

    def test_create_food_booking(self):
        self.assertEqual(FoodBooking.objects.all().count(), 0)
        form = FoodBookingForm(self.data)
        self.assertTrue(form.is_valid())
        food_booking = form.save()
        self.assertIsNotNone(food_booking)
        self.assertEqual(FoodBooking.objects.all().count(), 1)

    def test_create_food_booking_with_custom_amount(self):
        self.assertEqual(FoodBooking.objects.all().count(), 0)
        form = FoodBookingForm(self.data, amount=9000, amount_withheld=900)
        self.assertTrue(form.is_valid())
        food_booking = form.save()
        self.assertIsNotNone(food_booking)
        self.assertEqual(FoodBooking.objects.all().count(), 1)
        self.assertEqual(food_booking.amount, 9000)
        self.assertEqual(food_booking.service_fee_amount, 900)

    def test_create_food_booking_with_coupon(self):
        coupon = Coupon.objects.create(**{
            'code': 'ILIKEPIE!!!',
            'amount_off': 50
        })
        self.assertEqual(FoodBooking.objects.all().count(), 0)
        form = FoodBookingForm(self.data, amount=9000, amount_withheld=900, coupon_id=coupon.id)
        self.assertTrue(form.is_valid())
        food_booking = form.save()
        self.assertIsNotNone(food_booking)
        self.assertEqual(FoodBooking.objects.all().count(), 1)
        self.assertEqual(food_booking.amount, Decimal(9000 - coupon.amount_off))
        self.assertEqual(food_booking.service_fee_amount, 900)
        self.assertIsNotNone(food_booking.coupon)
        self.assertEqual(food_booking.coupon.id, coupon.id)

    def test_items_get_stripped(self):
        cleaned_items = [i.strip() for i in self.data.get('items').split(',')]
        form = FoodBookingForm(self.data)
        self.assertTrue(form.is_valid())
        food_booking = form.save()
        for i in food_booking.items:
            self.assertIn(i, cleaned_items)

    def verify_braintree_data(self, booking):
        transaction = braintree.Transaction.find(booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertEqual(Decimal(transaction.service_fee_amount), self.data.get('service_fee_amount'))
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        if booking.do_hold_in_escrow:
            self.assertEqual(
                transaction.escrow_status,
                BraintreeEscrowStatusType.labels.get(
                    BraintreeEscrowStatusType.HOLD_PENDING
                )
            )
        else:
            self.assertIsNone(transaction.escrow_status)


class BulkBookingFormTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BulkBookingFormTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(5, do_send_client_confirmation=False)

    def setUp(self):
        self.data = {
            'client': Photographer.objects.all()[0].profile.id,
            'bookings': [b.id for b in Booking.objects.all()],
            'message': 'Hello World!'
        }
        self.files = {
            'file': self.get_file('postmates.csv')
        }

    def tearDown(self):
        BulkBooking.objects.all().delete()

    def test_init(self):
        BulkBookingForm()

    def test_create_bulk_booking(self):
        form = BulkBookingForm(data=self.data, files=self.files)
        self.assertTrue(form.is_valid())
        bulk_booking = form.save()
        self.assertIsNotNone(bulk_booking)

    def test_create_bulk_booking_with_file_only(self):
        data = self.data.copy()
        data.pop('client')
        data.pop('bookings')
        data.pop('message')
        form = BulkBookingForm(data=data, files=self.files)
        self.assertTrue(form.is_valid())
        bulk_booking = form.save()
        self.assertIsNotNone(bulk_booking)

    def test_update_bulk_booking(self):
        data = self.data.copy()
        data.pop('client')
        data.pop('bookings')
        data.pop('message')
        form = BulkBookingForm(data=data, files=self.files)
        self.assertTrue(form.is_valid())
        bulk_booking = form.save()
        self.assertIsNotNone(bulk_booking)
        self.assertIsNone(bulk_booking.client)
        self.assertEqual(bulk_booking.bookings.all().count(), 0)

        # Update
        data = self.data.copy()
        form = BulkBookingForm(data=data, instance=bulk_booking)
        self.assertTrue(form.is_valid())
        bulk_booking = form.save()
        self.assertIsNotNone(bulk_booking)
        self.assertIsNotNone(bulk_booking.file)
        self.assertEqual(bulk_booking.client.id, data.get('client'))
        self.assertEqual(bulk_booking.message, data.get('message'))
        self.assertEqual(sorted([b.id for b in bulk_booking.bookings.all()]), sorted(data.get('bookings')))

    def test_empty_file(self):
        files = self.files.copy()
        files['file'] = self.get_file('empty.csv')
        form = BulkBookingForm(data=self.data, files=files)
        self.assertFalse(form.is_valid())

    def test_invalid_text_file(self):
        files = self.files.copy()
        files['file'] = self.get_file('script.csv')
        form = BulkBookingForm(data=self.data, files=files)
        self.assertFalse(form.is_valid())

    def test_invalid_image_file(self):
        files = self.files.copy()
        files['file'] = self.get_file('image.csv')
        form = BulkBookingForm(data=self.data, files=files)
        self.assertFalse(form.is_valid())

    def test_missing_required_fields(self):
        files = self.files.copy()
        files['file'] = self.get_file('missing_required_fields.csv')
        form = BulkBookingForm(data=self.data, files=files)
        self.assertFalse(form.is_valid())

    def get_file(self, name):
        booking_files_dir = os.path.join(FIXTURES_DIR, 'booking_files/')
        file_path = os.path.join(booking_files_dir, name)
        return ContentFile(open(file_path).read(), name=os.path.basename(file_path))


class BookingListFormTestCase(TestCase):
    def setUp(self):
        self.region_choices = [
            ('AL', 'Alabama'),
            ('AK', 'Alaska'),
            ('AZ', 'Arizona'),
            ('AR', 'Arkansas'),
        ]
        self.locality_choices = [
            ('Spring', 'Spring'),
            ('Bakersfield', 'Bakersfield'),
            ('Columbia', 'Columbia'),
            ('Pittsburgh', 'Pittsburgh'),
        ]
        self.data = {
            'query': 'Restaurant',
            'region': random.choice(self.region_choices)[0],
            'locality': random.choice(self.locality_choices)[0],
        }

    def test_init(self):
        BookingListForm(self.locality_choices, self.region_choices)

    def test_valid_data(self):
        data = self.data.copy()
        form = BookingListForm(self.locality_choices, self.region_choices, data=data)
        self.assertTrue(form.is_valid())

    def test_invalid_region(self):
        data = self.data.copy()
        data['region'] = 'Florida'
        form = BookingListForm(self.locality_choices, self.region_choices, data=data)
        self.assertFalse(form.is_valid())

    def test_invalid_locality(self):
        data = self.data.copy()
        data['locality'] = 'Miami'
        form = BookingListForm(self.locality_choices, self.region_choices, data=data)
        self.assertFalse(form.is_valid())

    def test_blank_keyword(self):
        data = self.data.copy()
        data['query'] = ''
        form = BookingListForm(self.locality_choices, self.region_choices, data=data)
        self.assertTrue(form.is_valid())
