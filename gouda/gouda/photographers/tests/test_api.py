from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from django.core.urlresolvers import reverse
from gouda.users.models import User
from gouda.profiles.models import Profile
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingImage
from gouda.photographers.models import BookingAlbum
from gouda.photographers.utils import get_booking_image_data
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_food_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers
from rest_framework import status
from rest_framework.test import APITestCase as ApiTestCase
from gouda.urlresolvers import reverse_with_query
from gouda.utils import get_lorem_text
from django.core import mail
from django.conf import settings
import base64
from django.test import TestCase, override_settings
import os
import unittest

THUMBOR_SERVER = getattr(settings, 'THUMBOR_SERVER')
FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')


class FoodBookingUpdateApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingUpdateApiTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingUpdateApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        create_food_bookings(1, do_create_images=False)
        self.food_booking = FoodBooking.objects.all()[0]
        self.user = self.food_booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        self.data = {
            'items': [
                'Crispy Duck',
                'Mee Krab',
                'Pad Thai'
            ]
        }
        mail.outbox = list()

    def test_update_items(self):
        self.assertEqual(FoodBooking.objects.filter(items__len=1).count(), 0)
        data = self.data.copy()
        response = self.client.patch(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(FoodBooking.objects.filter(items__len=len(data.get('items'))).count(), 1)
        food_booking = FoodBooking.objects.get(id=self.food_booking.id)
        self.assertEqual(food_booking.items, data.get('items'))

    def test_login_required(self):
        self.client.logout()
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_booking_ownership(self):
        self.client.logout()
        user = [p for p in Profile.objects.all() if p != self.food_booking.client and p != self.food_booking.photographer.profile][0].user
        self.client.login(username=user.email_address, password='%s123' % user.full_name.replace(' ', '').lower())

        data = self.data.copy()
        response = self.client.patch(self.url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @property
    def url(self):
        return reverse('api_photographers:food_booking_detail', kwargs={'pk': self.food_booking.id})


class BookingImageUpdateApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingImageUpdateApiTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingImageUpdateApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        create_bookings(1, do_create_images=True)
        self.booking_image = BookingImage.objects.all()[0]
        self.booking = Booking.objects.all()[0]
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        mail.outbox = list()

    def tearDown(self):
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()

    def test_simultaneous_rejection_and_approval(self):
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 0)
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        data = {
            'did_client_approve': True,
            'did_client_reject': True,
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 0)
        self.assertEqual(len(mail.outbox), 0)

    def test_invalid_rejection(self):
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 0)
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        data = {
            'did_client_reject': True,
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 0)
        self.assertEqual(len(mail.outbox), 0)

    def test_reject_image(self):
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 0)
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        data = {
            'did_client_reject': True,
            'did_client_approve': False
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 1)
        self.assertEqual(len(mail.outbox), 1)

    def test_reject_image_multiple_times(self):
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 0)
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        data = {
            'did_client_reject': True,
            'did_client_approve': False
        }
        for i in range(10):
            response = self.client.patch(url, data, format='json')
            self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookingImage.objects.filter(did_client_reject=True).count(), 1)
        self.assertEqual(len(mail.outbox), 1)

    def test_approve_image(self):
        self.assertEqual(BookingImage.objects.filter(did_client_approve=True).count(), 0)
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        data = {
            'did_client_approve': True,
            'did_client_reject': False,
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookingImage.objects.filter(did_client_approve=True).count(), 1)

    def test_comment_image(self):
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        data = {
            'comment': 'This image rocks!?!?!?!',
        }
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(BookingImage.objects.filter(comment=data.get('comment')).count(), 1)
        self.assertEqual(len(mail.outbox), 1)


class BookingImageDeletionApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingImageDeletionApiTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingImageDeletionApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_create_images=True)[0]
        self.album = BookingAlbum.objects.get(booking=self.booking)
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        self.booking_image = self.album.images.all()[0]

    def tearDown(self):
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()

    def test_delete_booking_image(self):
        url = reverse('api_photographers:booking_image_detail', kwargs={'pk': self.booking_image.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(BookingImage.objects.filter(id=self.booking_image.id).exists())
        self.assertFalse(self.album.images.filter(id=self.booking_image.id).exists())


class BookingImageCreationApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingImageCreationApiTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(1, do_create_images=False)

    @classmethod
    def tearDownClass(cls):
        super(BookingImageCreationApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        self.image_data = get_booking_image_data()
        self.image_data.update({
            'booking': self.booking.id,
            'name': get_lorem_text(1, 'w')
        })

    def tearDown(self):
        BookingImage.objects.all().delete()

    def test_create_booking_image(self):
        image_data = self.image_data.copy()
        url = reverse('api_photographers:booking_image_list')
        response = self.client.post(url, image_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingImage.objects.filter(booking=self.booking).count(), 1)

    def test_create_booking_image_with_query_params(self):
        url = reverse_with_query('api_photographers:booking_image_list', {'booking': self.booking.id})
        image_data = self.image_data.copy()
        image_data.pop('booking')
        response = self.client.post(url, image_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingImage.objects.filter(booking=self.booking).count(), 1)

    def test_create_booking_image_with_name(self):
        url = reverse('api_photographers:booking_image_list')
        image_data = self.image_data.copy()
        image_data['name'] = 'DANGER ZONE!'
        response = self.client.post(url, image_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingImage.objects.filter(booking=self.booking).count(), 1)
        self.assertEqual(BookingImage.objects.filter(booking=self.booking)[0].name, image_data.get('name'))

    @unittest.skip('No more TIFF support')
    def test_tiff_file(self):
        image_data = get_booking_image_data('cookies.tif')
        image_data.update({
            'booking': self.booking.id,
            'name': get_lorem_text(1, 'w')
        })
        self.assertTrue(isinstance(image_data.get('image'), basestring))
        url = reverse('api_photographers:booking_image_list')
        response = self.client.post(url, image_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingImage.objects.filter(booking=self.booking).count(), 1)
        booking_image = BookingImage.objects.filter(booking=self.booking)[0]
        # self.assertIsNotNone(booking_image.image['500x0'].url)
        # print(booking_image.image['500x0'].url)

    def test_weird_jpeg_file(self):
        image_file = open(os.path.join(FIXTURES_DIR, 'invalid_images/gelato.jpg'))
        image_data = {
            'image': base64.b64encode(image_file.read()),
            'booking': self.booking.id,
            'name': get_lorem_text(1, 'w')
        }
        self.assertTrue(isinstance(image_data.get('image'), basestring))
        url = reverse('api_photographers:booking_image_list')
        response = self.client.post(url, image_data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingImage.objects.filter(booking=self.booking).count(), 1)
        booking_image = BookingImage.objects.filter(booking=self.booking)[0]


class BookingImageRetrievalApiTestCase(ApiTestCase):
    list_view_name = 'api_photographers:booking_image_list'

    @classmethod
    def setUpClass(cls):
        super(BookingImageRetrievalApiTestCase, cls).setUpClass()
        create_photographers(10, do_create_marketplace=True)
        create_bookings(10, do_create_images=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingImageRetrievalApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.user = Booking.objects.all()[0].client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())

    def test_never_cache_headers(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('Last-Modified', response)
        self.assertIn('Expires', response)
        self.assertIn('Cache-Control', response)
        self.assertIn('max-age=0', response['Cache-Control'])

    def test_booking_image_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for booking_image in response.data.get('results'):
            self.assertIn('id', booking_image)
            self.assertIn('booking', booking_image)
            self.assertIn('dateCreated', booking_image)
            self.assertIn('comment', booking_image)
            self.assertIn('didClientApprove', booking_image)
            self.assertIn('didClientReject', booking_image)
            self.assertIn('url', booking_image)
            self.assertIn('image', booking_image)
            self.assertIn('name', booking_image)
            self.assertIn('url', booking_image.get('image'))
            self.assertIn('thumbnailUrl', booking_image.get('image'))
            self.assertIn('http', booking_image.get('image').get('url'))
            self.assertIn('http', booking_image.get('image').get('thumbnailUrl'))

    def test_filter_by_booking(self):
        booking = Booking.objects.filter(client=self.user.profile)[0]
        url = reverse_with_query(self.list_view_name, {'booking': booking.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        self.assertEqual(len(response.data.get('results')), booking.images.all().count())
        for booking_image in response.data.get('results'):
            self.assertEqual(len([i for i in booking.images.all() if i.pk == booking_image.get('id')]), 1)

    def test_login_required(self):
        url = reverse(self.list_view_name)
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_image_ownership(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for result in response.data.get('results'):
            booking_image = BookingImage.objects.get(id=result.get('id'))
            self.assertTrue((booking_image.booking.client.user.id == self.user.id) or (booking_image.booking.photographer.profile.user.id == self.user.id))


class BookingAlbumRetrievalApiTestCase(ApiTestCase):
    list_view_name = 'api_photographers:booking_album_list'

    @classmethod
    def setUpClass(cls):
        super(BookingAlbumRetrievalApiTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(5, do_create_images=True, num_albums=10)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumRetrievalApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.user = BookingAlbum.objects.all()[0].booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())

    def test_never_cache_headers(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIn('Last-Modified', response)
        self.assertIn('Expires', response)
        self.assertIn('Cache-Control', response)
        self.assertIn('max-age=0', response['Cache-Control'])

    def test_login_required(self):
        url = reverse(self.list_view_name)
        self.client.logout()
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_booking_image_list(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for album in response.data.get('results'):
            self.assertIn('id', album)
            self.assertIn('url', album)
            self.assertIn('dateCreated', album)
            self.assertIn('name', album)
            self.assertIn('width', album)
            self.assertIn('height', album)
            self.assertIn('images', album)
            self.assertIn('booking', album)
            for image in album.get('images'):
                self.assertIn('id', image)
                self.assertIn('booking', image)
                self.assertIn('dateCreated', image)
                self.assertIn('comment', image)
                self.assertIn('didClientApprove', image)
                self.assertIn('didClientReject', image)
                self.assertIn('url', image)
                self.assertIn('image', image)
                self.assertIn('name', image)
                self.assertIn('url', image.get('image'))
                self.assertIn('thumbnailUrl', image.get('image'))
                self.assertIn('metadataUrl', image.get('image'))
                self.assertIn('http', image.get('image').get('url'))
                self.assertIn('http', image.get('image').get('thumbnailUrl'))

    def test_album_ownership(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for result in response.data.get('results'):
            booking_album = BookingAlbum.objects.get(id=result.get('id'))
            self.assertTrue((booking_album.booking.client.user.id == self.user.id) or (booking_album.booking.photographer.profile.user.id == self.user.id))

    def test_superuser_access(self):
        booking = Booking.objects.filter(client=self.user.profile)[0]
        user = [u for u in User.objects.all() if u != self.user][0]
        user.is_superuser = True
        user.save()
        self.client.logout()
        self.client.login(username=user.email_address, password='%s123' % user.profile.full_name.replace(' ', '').lower())
        url = reverse_with_query(self.list_view_name, {'booking': booking.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)

        # Cleanup
        user.is_superuser = False
        user.save()

    def test_filter_by_booking(self):
        booking = Booking.objects.filter(client=self.user.profile)[0]
        url = reverse_with_query(self.list_view_name, {'booking': booking.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for booking_album in response.data.get('results'):
            self.assertIn('booking', booking_album)
            self.assertTrue(booking_album.get('booking'), booking.id)

    def test_thumbor_thumbnail(self):
        url = reverse(self.list_view_name)
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertGreater(len(response.data.get('results')), 0)
        for album in response.data.get('results'):
            for image in album.get('images'):
                self.assertIn('thumbnailUrl', image.get('image'))
                self.assertIn(THUMBOR_SERVER, image.get('image').get('thumbnailUrl'))


class BookingAlbumUpdateApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAlbumUpdateApiTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumUpdateApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_create_images=True)[0]
        self.album = BookingAlbum.objects.get(booking=self.booking)
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        self.image_data = get_booking_image_data()
        self.image_data.update({
            'booking': self.booking.id,
            'name': get_lorem_text(1, 'w')
        })

    def tearDown(self):
        BookingAlbum.objects.all().delete()
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()

    def test_create_image(self):
        num_images = BookingAlbum.objects.get(id=self.album.id).images.count()
        data = {
            'images': [
                self.image_data
            ]
        }
        url = reverse('api_photographers:booking_album_detail', kwargs={'pk': self.album.id})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(num_images + 1, BookingAlbum.objects.get(id=self.album.id).images.count())

    def test_change_name(self):
        name = 'Bisping'
        data = {
            'name': name
        }
        url = reverse('api_photographers:booking_album_detail', kwargs={'pk': self.album.id})
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue(BookingAlbum.objects.filter(id=self.album.id, name=name).count() == 1)


class BookingAlbumCreationApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAlbumCreationApiTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(1, do_create_images=False)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumCreationApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        self.data = {
            'booking': self.booking.id,
            'name': get_lorem_text(1, 'w')
        }

    def tearDown(self):
        BookingAlbum.objects.all().delete()
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()

    def test_create_booking_album(self):
        num_albums = BookingAlbum.objects.filter(booking=self.booking).count()
        url = reverse('api_photographers:booking_album_list')
        data = self.data.copy()
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingAlbum.objects.filter(booking=self.booking).count(), num_albums + 1)

    def test_create_booking_album_with_query_params(self):
        num_albums = BookingAlbum.objects.filter(booking=self.booking).count()
        url = reverse_with_query('api_photographers:booking_album_list', {'booking': self.booking.id})
        data = self.data.copy()
        data.pop('booking')
        response = self.client.post(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(BookingAlbum.objects.filter(booking=self.booking).count(), num_albums + 1)


class BookingAlbumDeletionApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAlbumDeletionApiTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumDeletionApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_create_images=True)[0]
        self.album = BookingAlbum.objects.get(booking=self.booking)
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())

    def tearDown(self):
        BookingAlbum.objects.all().delete()
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()

    def test_delete_booking_album(self):
        url = reverse('api_photographers:booking_album_detail', kwargs={'pk': self.album.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertFalse(BookingAlbum.objects.filter(id=self.album.id).exists())


AWS_STORAGE_BUCKET_NAME = 'gouda-test'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN
MEDIAFILES_LOCATION = 'media'


@override_settings(
    DEFAULT_FILE_STORAGE='gouda.storages.S3MediaStorage',
    MEDIAFILES_LOCATION='media',
    MEDIA_URL='%s/%s/' % (S3_URL, MEDIAFILES_LOCATION),
    THUMBOR_MEDIA_URL='%s/%s/' % (S3_URL, MEDIAFILES_LOCATION),
    THUMBNAIL_DEFAULT_STORAGE='gouda.storages.S3MediaStorage'
)
class BookingAlbumSmartCropApiTestCase(ApiTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAlbumSmartCropApiTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumSmartCropApiTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_create_images=True)[0]
        self.album = BookingAlbum.objects.get(booking=self.booking)
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        image = self.album.images.all()[0]

        self.data = {
            'images': [
                {
                    'image_url': image.image.url,
                    'album': self.album.id,
                    'name': image.name,
                    'width': 200,
                    'height': 200
                }
            ]
        }

    def tearDown(self):
        BookingAlbum.objects.all().delete()
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()

    def test_crop_image(self):
        data = self.data.copy()
        num_images = BookingAlbum.objects.get(id=self.album.id).images.count()
        url = reverse('api_photographers:booking_album_smart_crop', kwargs={'pk': self.album.id})
        print(url)
        response = self.client.patch(url, data, format='json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
