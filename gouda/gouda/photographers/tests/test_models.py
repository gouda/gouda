from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import random
from datetime import timedelta
from decimal import *

import os
from django.conf import settings
from django.core import mail
from django.core.files.base import ContentFile
from django.test import TestCase
from django.utils import timezone
from gouda.addresses.models import Address
from gouda.customers.models import BraintreeEscrowStatusType
from gouda.customers.models import BraintreeTransactionStatusType
from gouda.customers.utils import braintree
from gouda.profiles.models import Profile
from gouda.photographers.models import PhotographerStatusType
from gouda.photographers.models import BookingSurcharge
from gouda.photographers.models import BulkBooking
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import BookingCheckoutURL
from gouda.photographers.models import BookingAlbum
from gouda.photographers.models import BookingImage
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerImage
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import Review
from gouda.photographers.models import ReviewURL
from gouda.photographers.models import ServiceType
from gouda.photographers.models import TimePeriodType
from gouda.photographers.models import Tip
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers
from gouda.profiles.utils import create_profiles
from gouda.postmaster.models import Thread
from psycopg2.extras import DateTimeTZRange
from gouda.utils import get_lorem_text
from unittest import skip
import pytz
import csv
from django.utils.text import slugify

from zipfile import ZipFile

DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
AMOUNT_WITHHELD_PERCENTAGE = getattr(settings, 'AMOUNT_WITHHELD_PERCENTAGE')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')
FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
PAY_SCALE = getattr(settings, 'PAY_SCALE')
MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY = getattr(settings, 'MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY')
MAX_BOOKINGS_PER_PHOTOGRAPHER = getattr(settings, 'MAX_BOOKINGS_PER_PHOTOGRAPHER')


class BookingRequestTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingRequestTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(25, do_send_client_confirmation=False, do_assign_photographers=False)

        for photographer in Photographer.objects.all():
            photographer.address.point = 'POINT(%f %f)' % (-122.4194, 37.7749)
            photographer.address.save()
        photographer = Photographer.objects.all()[0]
        photographer.address.point = 'POINT(%f %f)' % (-118.2437, 34.0522)
        photographer.address.save()
        photographer.save()

        start_time = timezone.now().replace(hour=0, minute=0, second=0)
        for booking in Booking.objects.all():
            stop_time = start_time + timedelta(hours=1)
            booking.time_range = DateTimeTZRange(lower=start_time, upper=stop_time)
            start_time += timedelta(hours=1)
            booking.contact_name = random.choice(Profile.objects.all()).full_name
            booking.contact_email_address = random.choice(Profile.objects.all()).email_address
            booking.save()
            booking.address.point = 'POINT(%f %f)' % (
                random.uniform(-118.593979, -117.759018),
                random.uniform(33.801118, 34.357325)
            )
            booking.address.save()

    @classmethod
    def tearDownClass(cls):
        super(BookingRequestTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.photographer = Photographer.objects.all()[0]
        self.photographer.status = PhotographerStatusType.ACTIVE
        self.photographer.save()
        self.data = {
            'photographer': self.photographer,
            'booking': Booking.objects.all()[0]
        }
        mail.outbox = list()

    def tearDown(self):
        BookingRequest.objects.all().delete()

    def test_init(self):
        BookingRequest()

    def test_create_booking_request(self):
        booking_request = BookingRequest.objects.create(**self.data)
        self.assertIsNotNone(booking_request)

    def test_create_booking_requests_from_booking(self):
        self.assertEqual(BookingRequest.objects.all().count(), 0)
        booking = Booking.objects.all()[0]
        booking.create_booking_requests()
        self.assertEqual(BookingRequest.objects.all().count(), 1)

    def test_email_booking_requests(self):
        booking = Booking.objects.all()[0]
        booking.create_booking_requests()
        booking_request = BookingRequest.objects.all()[0]
        self.assertEqual(len(mail.outbox), 1)
        outbox = [m for m in mail.outbox if 'Booking Request' == m.subject]
        self.assertEqual(len(outbox), 1)
        request_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, request_mail.from_email)
        self.assertEqual(len(request_mail.to), 1)
        self.assertEqual(booking_request.photographer.email_address, request_mail.to[0])
        self.assertEqual(STAFF_EMAILS, request_mail.bcc)

    def test_accept_booking_request(self):
        booking_request = BookingRequest.objects.create(**self.data)
        booking_request.accept_booking()
        booking = booking_request.booking
        photographer = booking_request.photographer
        self.assertTrue(booking_request.did_accept_booking)
        self.assertFalse(booking_request.did_reject_booking)
        self.assertEqual(booking.photographer, photographer)
        self.assertTrue(booking.do_hold_in_escrow)
        self.assertTrue(booking.is_sub_merchant_transaction)
        self.assertEqual(booking.braintree_id, '')

    def test_prevent_pending_photographer_from_accepting(self):
        self.photographer.status = PhotographerStatusType.PENDING
        self.photographer.save()
        booking_request = BookingRequest.objects.create(**self.data)
        self.assertFalse(booking_request.can_photographer_accept_booking)
        self.assertRaises(AssertionError, booking_request.accept_booking)

    def test_prevent_inactive_photographer_from_accepting(self):
        self.photographer.status = PhotographerStatusType.INACTIVE
        self.photographer.save()
        booking_request = BookingRequest.objects.create(**self.data)
        self.assertFalse(booking_request.can_photographer_accept_booking)
        self.assertRaises(AssertionError, booking_request.accept_booking)

    @skip('')
    def test_limit_num_bookings_per_day(self):
        start_time = timezone.now().replace(hour=0, minute=0, second=0)
        for booking in Booking.objects.all():
            stop_time = start_time + timedelta(hours=1)
            booking.time_range = DateTimeTZRange(lower=start_time, upper=stop_time)
            start_time += timedelta(hours=3)
            booking.save()

        for booking in Booking.objects.all():
            booking_request = BookingRequest.objects.create(**{
                'photographer': self.photographer,
                'booking': booking
            })
            day_range = DateTimeTZRange(
                lower=booking.time_range.lower.replace(hour=0, minute=0, second=0),
                upper=booking.time_range.lower.replace(hour=23, minute=59, second=59)
            )
            if Booking.objects.filter(photographer=self.photographer, time_range__overlap=day_range).count() >= MAX_BOOKINGS_PER_PHOTOGRAPHER_PER_DAY:
                self.assertRaises(AssertionError, booking_request.accept_booking)
            else:
                booking_request.accept_booking()

    @skip('')
    def test_limit_total_bookings(self):
        start_time = timezone.now().replace(hour=0, minute=0, second=0)
        start_time += timedelta(hours=100)
        for booking in Booking.objects.all():
            stop_time = start_time + timedelta(hours=1)
            booking.time_range = DateTimeTZRange(lower=start_time, upper=stop_time)
            start_time += timedelta(hours=12)
            booking.save()

        for index, booking in enumerate(Booking.objects.all().order_by('time_range')):
            booking_request = BookingRequest.objects.create(**{
                'photographer': self.photographer,
                'booking': booking
            })
            if index < MAX_BOOKINGS_PER_PHOTOGRAPHER:
                booking_request.accept_booking()
            else:
                self.assertRaises(AssertionError, booking_request.accept_booking)

    @skip('')
    def test_limit_time_between_shoots(self):
        start_time = timezone.now().replace(hour=0, minute=0, second=0)
        for booking in Booking.objects.all():
            stop_time = start_time + timedelta(hours=1)
            booking.time_range = DateTimeTZRange(lower=start_time, upper=stop_time)
            start_time += timedelta(hours=1)
            booking.save()

        for index, booking in enumerate(Booking.objects.all().order_by('time_range')[:15]):
            booking_request = BookingRequest.objects.create(**{
                'photographer': self.photographer,
                'booking': booking
            })
            if index % 3 == 0 and index < 9:
                booking_request.accept_booking()
            else:
                self.assertRaises(AssertionError, booking_request.accept_booking)
        # transaction = braintree.Transaction.find(booking.braintree_id)
        # self.assertIsNotNone(transaction)
        # self.assertEqual(Decimal(transaction.amount), booking.amount)
        # self.assertEqual(Decimal(transaction.service_fee_amount), booking.service_fee_amount)
        # self.assertEqual(
        #     transaction.status,
        #     BraintreeTransactionStatusType.labels.get(
        #         BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
        #     )
        # )
        # self.assertEqual(
        #     transaction.escrow_status,
        #     BraintreeEscrowStatusType.labels.get(
        #         BraintreeEscrowStatusType.HOLD_PENDING
        #     )
        # )

    def test_send_client_notification(self):
        booking_request = BookingRequest.objects.create(**self.data)
        booking_request.accept_booking()
        outbox = [m for m in mail.outbox if 'was assigned to your shoot' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(booking_request.booking.client.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_reject_booking_request(self):
        booking_request = BookingRequest.objects.create(**self.data)
        self.assertIsNone(booking_request.booking.photographer)
        booking_request.reject_booking()
        booking = Booking.objects.get(id=booking_request.booking.id)
        self.assertFalse(booking_request.did_accept_booking)
        self.assertTrue(booking_request.did_reject_booking)
        self.assertIsNone(booking.photographer)

    def test_accept_booking_request_twice(self):
        booking_request = BookingRequest.objects.create(**self.data)
        booking_request.accept_booking()
        self.assertRaises(AssertionError, booking_request.accept_booking)

    def test_accept_fulfilled_booking_request(self):
        booking_request = BookingRequest.objects.create(**self.data)
        booking_request.booking.photographer = random.choice(Photographer.objects.all())
        booking_request.booking.save()
        self.assertRaises(AssertionError, booking_request.accept_booking)

    def test_reject_booking_request_after_acceptance(self):
        booking_request = BookingRequest.objects.create(**self.data)
        booking_request.accept_booking()
        self.assertRaises(AssertionError, booking_request.reject_booking)

    def test_reject_booking_request_twice(self):
        booking_request = BookingRequest.objects.create(**self.data)
        booking_request.reject_booking()
        self.assertRaises(AssertionError, booking_request.reject_booking)


class BulkBookingTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BulkBookingTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BulkBookingTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        create_bookings(5, do_send_client_confirmation=False)
        self.data = {
            'client': Photographer.objects.all()[0].profile,
            'file': self.get_file()
        }

    def get_file(self):
        booking_files_dir = os.path.join(FIXTURES_DIR, 'booking_files/')
        return ContentFile(open(os.path.join(booking_files_dir, 'postmates.csv')).read(), name='postmates.csv')

    def tearDown(self):
        Booking.objects.all().delete()
        BulkBooking.objects.all().delete()
        mail.outbox = []

    def test_init(self):
        BulkBooking()

    def test_create_bulk_booking(self):
        bulk_booking = self.create_bulk_booking()
        self.assertIsNotNone(bulk_booking)
        self.assertEqual(BulkBooking.objects.all().count(), 1)
        self.assertEqual(bulk_booking.bookings.all().count(), Booking.objects.all().count())

    def test_confirmation_email(self):
        bulk_booking = self.create_bulk_booking()
        bulk_booking.send_client_confirmation()
        outbox = [m for m in mail.outbox if 'Booking Confirmation' == m.subject]
        self.assertEqual(len(outbox), 1)
        confirmation_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, confirmation_mail.from_email)
        self.assertEqual(len(confirmation_mail.to), 1)
        self.assertEqual(bulk_booking.client.email_address, confirmation_mail.to[0])
        self.assertEqual(STAFF_EMAILS, confirmation_mail.bcc)

    def test_process_file(self):
        bulk_booking = self.create_bulk_booking()
        bookings = bulk_booking.get_booking_data_from_file()
        self.assertIsNotNone(bookings)
        self.assertGreater(len(bookings), 0)

    def test_create_bookings_from_file(self):
        Booking.objects.all().delete()
        self.assertEqual(Booking.objects.all().count(), 0)
        bulk_booking = self.create_bulk_booking()
        bulk_booking.create_bookings_from_file()
        self.assertGreater(BulkBooking.objects.all()[0].bookings.all().count(), 0)

    # def test_process_invalid_file(self):
    #     bulk_booking = self.create_bulk_booking()
    #     print(bulk_booking.get_bookings_from_file())

    def create_bulk_booking(self):
        bulk_booking = BulkBooking.objects.create(**self.data)
        for booking in Booking.objects.all():
            bulk_booking.bookings.add(booking)
        return bulk_booking


class FoodBookingTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        self.data = dict({
            'shot_list': ContentFile(
                open(os.path.join(FIXTURES_DIR, 'shot_list.csv')).read(),
                name='shot_list'
            ),
            'is_sub_merchant_transaction': True,
            'do_hold_in_escrow': True,
            'thread': Thread.objects.create(),
            'photographer': self.photographer,
            'client': self.client,
            'time_range': time_range,
            'service_type': ServiceType.FOOD,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            }),
            'restaurant_name': 'My Thai',
            'contact_name': 'Barry Dylan',
            'contact_phone_number': '5555555555',
            'contact_email_address': 'success+sterlingarcher@simulator.amazonses.com',
            'is_full_menu_shoot': True,
            'is_confirmed_with_restaurant': True,
            'items': [
                'Crispy Duck',
                'Mee Krab',
                'Tom Yum Curry'
            ],
        })
        mail.outbox = []

    def tearDown(self):
        FoodBooking.objects.all().delete()
        Booking.objects.all().delete()
        mail.outbox = []

    def test_init(self):
        FoodBooking()

    def test_create_food_booking(self):
        food_booking = FoodBooking.objects.create(**self.data)
        self.assertIsNotNone(food_booking)
        self.assertEqual(FoodBooking.objects.all().count(), 1)

    def test_braintree_data(self):
        food_booking = FoodBooking.objects.create(**self.data)

        # Sale
        food_booking.sale(do_hold_in_escrow=True)

        # Verify data in Braintree
        self.verify_braintree_data(food_booking)

    def verify_braintree_data(self, food_booking):
        transaction = braintree.Transaction.find(food_booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertEqual(Decimal(transaction.service_fee_amount), self.data.get('amount') * AMOUNT_WITHHELD_PERCENTAGE / 100)
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        self.assertEqual(
            transaction.escrow_status,
            BraintreeEscrowStatusType.labels.get(
                BraintreeEscrowStatusType.HOLD_PENDING
            )
        )

    def test_photographer_notification(self):
        data = self.data.copy()
        food_booking = FoodBooking.objects.create(**data)
        food_booking.send_photographer_notification()
        outbox = [m for m in mail.outbox if 'You\'ve been assigned a SHOTZU booking' == m.subject]
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(food_booking.photographer.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_items_notification(self):
        data = self.data.copy()
        food_booking = FoodBooking.objects.create(**data)
        food_booking.send_items_notification()
        outbox = [m for m in mail.outbox if 'has updated the menu items for your shoot' in m.subject]
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(food_booking.photographer.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_send_merchant_notification(self):
        data = self.data.copy()
        food_booking = FoodBooking.objects.create(**data)
        food_booking.send_contact_notification()
        outbox = [m for m in mail.outbox if 'Your upcoming photoshoot with SHOTZU' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(notification_mail.to, [
            food_booking.client.email_address,
            food_booking.contact_email_address,
            food_booking.photographer.email_address,
            'julian@shotzu.com'
        ])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)
        self.assertEqual(len(notification_mail.attachments), 1)


class BookingTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        self.client.cc_recipients = [p.email_address for p in Profile.objects.all() if p != self.photographer and p != self.client]
        self.client.save()
        self.data = dict({
            'photographer': self.photographer,
            'client': self.client,
            'thread': Thread.objects.create(),
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 150,
            'service_fee_amount': 50,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        })
        self.assertTrue(len(self.client.cc_recipients) > 0)
        mail.outbox = []

    def tearDown(self):
        self.client.cc_recipients = list()
        self.client.save()
        Booking.objects.all().delete()
        BookingImage.objects.all().delete()
        BookingSurcharge.objects.all().delete()
        mail.outbox = []

    def test_is_date_unavailable_for_too_many_bookings(self):
        bookings = create_bookings(10)
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        for b in bookings:
            b.time_range = time_range
            b.save()
            b.address.point = 'POINT(%f %f)' % (95.3698, 29.7604)
            b.address.save()
        time_range = DateTimeTZRange(lower=timezone.now() + timedelta(minutes=30), upper=timezone.now() + timedelta(hours=2))
        self.assertFalse(Booking.is_appointment_available(time_range, 29.7604, 95.3698))

    def test_is_date_available(self):
        bookings = create_bookings(4)
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        for b in bookings:
            b.time_range = time_range
            b.save()
            b.address.point = 'POINT(%f %f)' % (95.3698, 29.7604)
            b.address.save()
        time_range = DateTimeTZRange(lower=timezone.now() + timedelta(minutes=30), upper=timezone.now() + timedelta(hours=2))
        self.assertTrue(Booking.is_appointment_available(time_range, 29.7604, 95.3698))

    def test_init(self):
        Booking()

    def test_create_booking(self):
        booking = Booking.objects.create(**self.data)
        self.assertIsNotNone(booking)

    def test_braintree_data(self):
        booking = Booking.objects.create(**self.data)

        # Sale
        booking.sale(do_hold_in_escrow=True)

        # Verify data in Braintree
        self.verify_braintree_data(booking)

    def verify_braintree_data(self, booking):
        transaction = braintree.Transaction.find(booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertEqual(Decimal(transaction.service_fee_amount), self.data.get('service_fee_amount'))
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        if booking.do_hold_in_escrow:
            self.assertEqual(
                transaction.escrow_status,
                BraintreeEscrowStatusType.labels.get(
                    BraintreeEscrowStatusType.HOLD_PENDING
                )
            )
        else:
            self.assertIsNone(transaction.escrow_status)

    def test_check_for_duplicate_transactions(self):
        self.assertEqual(Booking.objects.all().count(), 0)
        booking = Booking.objects.create(**self.data)
        booking.sale(do_hold_in_escrow=True, do_force_duplicate_check=True)
        booking = Booking.objects.create(**self.data)
        self.assertRaises(AssertionError, booking.sale, do_hold_in_escrow=True, do_force_duplicate_check=True)
        self.assertEqual(Booking.objects.exclude(braintree_id='').count(), 1)

    def test_staff_notification_email(self):
        booking = Booking.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'A new booking was created by %s' % booking.client.full_name == m.subject]
        self.assertEqual(len(outbox), 1)
        server_mail = outbox[0]
        self.assertEqual(SERVER_EMAIL, server_mail.from_email)
        self.assertEqual(STAFF_EMAILS, server_mail.recipients())

    def test_photographer_notification(self):
        booking = Booking.objects.create(**self.data)
        booking.send_photographer_notification()
        outbox = [m for m in mail.outbox if 'You\'ve been assigned a SHOTZU booking' == m.subject]
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(booking.photographer.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_disable_confirmation_email(self):
        data = self.data.copy()
        data['do_send_client_confirmation'] = False
        booking = Booking.objects.create(**data)
        outbox = [m for m in mail.outbox if 'Booking Confirmation' == m.subject]
        self.assertEqual(len(outbox), 0)

    def test_confirmation_email(self):
        booking = Booking.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'Booking Confirmation' == m.subject]
        self.assertEqual(len(outbox), 1)
        confirmation_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, confirmation_mail.from_email)
        self.assertEqual(len(confirmation_mail.recipients()), 1)
        self.assertEqual(booking.client.email_address, confirmation_mail.recipients()[0])

    def test_create_booking_without_photographer(self):
        self.create_booking('photographer')

    def test_save_booking_with_photographer(self):
        booking = self.create_booking('photographer')
        booking.photographer = self.data.get('photographer')
        booking.save()
        booking.sale(do_hold_in_escrow=True)
        booking = Booking.objects.all()[0]
        self.verify_braintree_data(booking)

    def test_transaction_directly_to_company(self):
        data = self.data.copy()
        data['photographer'] = None
        data['is_sub_merchant_transaction'] = False
        booking = Booking.objects.create(**data)
        booking.sale(do_hold_in_escrow=False)
        self.assertIsNone(booking.service_fee_amount)

        # Verify data in Braintree
        transaction = braintree.Transaction.find(booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertIsNone(transaction.service_fee_amount)
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        self.assertEqual(transaction.merchant_account_id, BRAINTREE_MERCHANT_ACCOUNT_ID)
        self.assertIsNone(transaction.escrow_status)

    def test_transaction_directly_to_company_with_photographer(self):
        data = self.data.copy()
        data['is_sub_merchant_transaction'] = False
        booking = Booking.objects.create(**data)
        booking.sale(do_hold_in_escrow=False)
        self.assertIsNone(booking.service_fee_amount)

        # Verify data in Braintree
        transaction = braintree.Transaction.find(booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertIsNone(transaction.service_fee_amount)
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        self.assertEqual(transaction.merchant_account_id, BRAINTREE_MERCHANT_ACCOUNT_ID)
        self.assertIsNone(transaction.escrow_status)

    @skip('')
    def test_create_zip_file(self):
        num_albums = 3
        bookings = create_bookings(5, do_create_images=True, num_albums=num_albums)
        booking = bookings[0]
        zip_file_path = booking.create_media_file()
        self.assertIsNotNone(zip_file_path)
        self.assertTrue(os.path.exists(zip_file_path))

        # Check names
        file_names = list()
        for album in booking.albums.all():
            for image in album.images.all():
                file_names.append('%s/%s' % (album.name, image.file_name))

        with ZipFile(zip_file_path, 'r') as zip_file:
            albums = list()
            self.assertEqual(zip_file.namelist(), file_names)
            for file_name in zip_file.namelist():
                albums.append(os.path.dirname(file_name))
            self.assertEqual(num_albums, len(set(albums)))

        # Cleanup
        os.remove(zip_file_path)
        for booking in bookings:
            for booking_image in booking.images.all():
                booking_image.delete()
            booking.delete()

    def test_create_media_file(self):
        bookings = create_bookings(1, do_create_images=True)
        booking = bookings[0]

        # Create duplicates
        for booking_image in BookingImage.objects.filter(booking=booking):
            booking_image.name = 'pizza'
            booking_image.save()

        zip_file_path = booking.create_media_file()
        self.assertIsNotNone(zip_file_path)
        self.assertIsNotNone(booking.media_file)
        self.assertTrue(os.path.exists(zip_file_path))

        # Check names
        with ZipFile(booking.media_file, 'r') as zip_file:
            file_names = [os.path.splitext(f)[0] for f in zip_file.namelist()]
            self.assertEqual(len(set(file_names)), BookingImage.objects.filter(booking=booking).count())

        # Cleanup
        os.remove(zip_file_path)
        for booking in bookings:
            for booking_image in booking.images.all():
                booking_image.delete()
            booking.delete()

    def test_skip_escrow(self):
        # Create booking
        data = self.data.copy()
        data['do_hold_in_escrow'] = False
        booking = Booking.objects.create(**data)
        booking.sale(do_hold_in_escrow=False)
        self.assertIsNotNone(booking)
        self.assertEqual(Booking.objects.all().count(), 1)

        # Verify Braintree data
        self.verify_braintree_data(booking)

    def test_client_approval(self):
        data = self.data.copy()
        data['num_shots'] = 49
        data['did_staff_approve'] = True
        booking = Booking.objects.create(**data)
        self.assertIsNone(booking.client_approval_date)

        # Approve
        booking.on_client_approval()

        # Check
        self.assertTrue(booking.did_client_approve)
        self.assertFalse(booking.did_client_reject)
        self.assertIsNotNone(booking.client_approval_date)
        self.assertEqual(booking.braintree_id, '')
        self.assertEqual(booking.amount, 270)
        self.assertEqual(booking.service_fee_amount, 70)

        # Make sure receipt gets sent
        outbox = [m for m in mail.outbox if 'SHOTZU Invoice' == m.subject]
        self.assertEqual(len(outbox), 1)
        invoice_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, invoice_mail.from_email)
        self.assertEqual(booking.client.email_address, invoice_mail.to[0])
        self.assertEqual(STAFF_EMAILS, invoice_mail.bcc)
        self.assertEqual(self.client.cc_recipients, invoice_mail.cc)

        # Make sure notification gets sent
        outbox = [m for m in mail.outbox if 'Your shoot was approved' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(booking.photographer.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_client_approval_twice(self):
        data = self.data.copy()
        data['num_shots'] = 49
        data['did_staff_approve'] = True
        booking = Booking.objects.create(**data)
        booking.on_client_approval()
        self.assertRaises(AssertionError, booking.on_client_approval)

    def test_client_approval_without_staff_approval(self):
        data = self.data.copy()
        data['num_shots'] = 49
        booking = Booking.objects.create(**data)
        self.assertRaises(AssertionError, booking.on_client_approval)

    def test_client_approval_after_rejection(self):
        data = self.data.copy()
        data['num_shots'] = 49
        data['did_staff_approve'] = True
        booking = Booking.objects.create(**data)
        booking.on_client_approval()
        self.assertRaises(AssertionError, booking.on_client_rejection)

    def test_client_rejection(self):
        data = self.data.copy()
        data['num_shots'] = 49
        data['photographer_approval_date'] = timezone.now()
        data['staff_approval_date'] = timezone.now()
        data['did_photographer_approve'] = True
        data['did_staff_approve'] = True
        booking = Booking.objects.create(**data)
        self.assertIsNone(booking.client_approval_date)
        self.assertIsNone(booking.client_rejection_date)

        # Reject
        booking.on_client_rejection()

        # Check
        self.assertFalse(booking.did_client_approve)
        self.assertTrue(booking.did_client_reject)
        self.assertIsNotNone(booking.client_rejection_date)
        self.assertEqual(booking.braintree_id, '')
        self.assertFalse(booking.did_staff_approve)
        self.assertFalse(booking.did_photographer_approve)
        self.assertIsNone(booking.photographer_approval_date)
        self.assertIsNone(booking.staff_approval_date)

        # Make sure notification gets sent
        outbox = [m for m in mail.outbox if 'Your shoot was rejected' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(booking.photographer.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_client_rejection_twice(self):
        data = self.data.copy()
        data['num_shots'] = 49
        data['did_staff_approve'] = True
        booking = Booking.objects.create(**data)
        booking.on_client_rejection()
        self.assertRaises(AssertionError, booking.on_client_rejection)

    def test_client_rejection_after_client_approval(self):
        data = self.data.copy()
        data['num_shots'] = 49
        data['did_staff_approve'] = True
        booking = Booking.objects.create(**data)
        booking.on_client_rejection()
        self.assertRaises(AssertionError, booking.on_client_approval)

    @skip('')
    def test_booking_image_group_automagically_created_for_booking_lol(self):
        booking = Booking.objects.create(**self.data)
        self.assertTrue(BookingAlbum.objects.filter(booking=booking).exists())

    # def test_client_approval(self):
    #     data = self.data.copy()
    #     data['num_shots'] = 49
    #     booking = Booking.objects.create(**data)
    #     booking.sale(do_hold_in_escrow=True)
    #
    #     # Change transaction status
    #     transaction = braintree.Transaction.find(booking.braintree_id)
    #     testing_gateway = braintree.TestingGateway(braintree.Configuration.gateway())
    #     testing_gateway.settle_transaction(transaction.id)
    #
    #     # Approve
    #     booking.on_client_approval()
    #
    #     # Check
    #     transaction = braintree.Transaction.find(booking.braintree_id)
    #     self.assertIsNotNone(transaction)
    #     self.assertEqual(Decimal(transaction.amount), booking.amount)
    #     self.assertEqual(Decimal(transaction.service_fee_amount), booking.service_fee_amount)
    #     self.assertEqual(
    #         transaction.status,
    #         BraintreeTransactionStatusType.labels.get(
    #             BraintreeTransactionStatusType.SETTLED
    #         )
    #     )
    #     self.assertEqual(
    #         transaction.escrow_status,
    #         BraintreeEscrowStatusType.labels.get(
    #             BraintreeEscrowStatusType.RELEASE_PENDING
    #         )
    #     )
    #
    #     # Make sure receipt gets sent
    #     outbox = [m for m in mail.outbox if 'SHOTZU Payment Receipt' == m.subject]
    #     self.assertEqual(len(outbox), 1)
    #     receipt_mail = outbox[0]
    #     self.assertEqual(DEFAULT_FROM_EMAIL, receipt_mail.from_email)
    #     self.assertEqual(booking.client.email_address, receipt_mail.to[0])
    #     self.assertEqual(STAFF_EMAILS, receipt_mail.bcc)
    #     print(receipt_mail.subject)
    #     print(receipt_mail.body)

    def test_create_surcharge(self):
        data = self.data.copy()
        data['amount'] = 135
        data['service_fee_amount'] = 35
        data['num_shots'] = 100
        data['did_photographer_approve'] = True
        booking = Booking.objects.create(**data)
        booking.sale(do_hold_in_escrow=True)
        booking.create_surcharge()

        # Check for surcharges
        self.assertTrue(BookingSurcharge.objects.filter(amount=270, service_fee_amount=70, booking=booking).exclude(braintree_id='').exists())

    def test_pay_scale(self):
        data = self.data.copy()
        data['amount'] = 135
        data['service_fee_amount'] = 35
        data['did_photographer_approve'] = True
        booking = Booking.objects.create(**data)
        booking.sale(do_hold_in_escrow=True)

        for pay_scale in PAY_SCALE:
            upper = pay_scale.get('shot_range').upper if pay_scale.get('shot_range').upper else 99
            for num_shots in range(pay_scale.get('shot_range').lower, upper + 1):
                booking.num_shots = num_shots
                booking.save()
                booking.create_surcharge()
                self.assertEqual(booking.total_amount, pay_scale.get('amount'))
                self.assertEqual(booking.total_service_fee_amount, pay_scale.get('service_fee_amount'))
                BookingSurcharge.objects.all().delete()

    @skip('')
    def test_client_approval_with_surcharge(self):
        data = self.data.copy()
        data['amount'] = 150
        data['service_fee_amount'] = 50
        data['num_shots'] = 100
        data['did_photographer_approve'] = True
        booking = Booking.objects.create(**data)
        booking.sale(do_hold_in_escrow=True)

        # Change transaction status
        transaction = braintree.Transaction.find(booking.braintree_id)
        testing_gateway = braintree.TestingGateway(braintree.Configuration.gateway())
        testing_gateway.settle_transaction(transaction.id)

        # Approve
        booking.on_client_approval()

        # Check for surcharges
        self.assertTrue(BookingSurcharge.objects.filter(amount=300, service_fee_amount=100, booking=booking).exclude(braintree_id='').exists())

        # Check final amounts
        self.assertEqual(booking.total_amount, 450)
        self.assertEqual(booking.total_service_fee_amount, 150)

    def test_photographer_approval(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = False
        data['did_client_approve'] = False
        booking = Booking.objects.create(**data)
        # booking.sale(do_hold_in_escrow=True)
        #
        # # Change transaction status
        # transaction = braintree.Transaction.find(booking.braintree_id)
        # testing_gateway = braintree.TestingGateway(braintree.Configuration.gateway())
        # testing_gateway.settle_transaction(transaction.id)

        # Approve
        self.assertFalse(booking.did_photographer_approve)
        self.assertIsNone(booking.photographer_approval_date)
        booking.on_photographer_approval()
        self.assertTrue(booking.did_photographer_approve)
        self.assertIsNotNone(booking.photographer_approval_date)

        # Make sure notification gets sent
        outbox = [m for m in mail.outbox if 'has finalized a booking' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(SERVER_EMAIL, notification_mail.from_email)
        self.assertEqual(STAFF_EMAILS, notification_mail.to)
        # self.assertEqual(booking.client.email_address, notification_mail.to[0])
        # self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_photographer_approval_after_client_rejection(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = False
        data['did_client_approve'] = False
        data['did_client_reject'] = True
        data['client_rejection_date'] = timezone.now()
        booking = Booking.objects.create(**data)
        # booking.sale(do_hold_in_escrow=True)
        #
        # # Change transaction status
        # transaction = braintree.Transaction.find(booking.braintree_id)
        # testing_gateway = braintree.TestingGateway(braintree.Configuration.gateway())
        # testing_gateway.settle_transaction(transaction.id)

        # Approve
        self.assertFalse(booking.did_photographer_approve)
        self.assertIsNone(booking.photographer_approval_date)
        booking.on_photographer_approval()
        self.assertTrue(booking.did_photographer_approve)
        self.assertIsNotNone(booking.photographer_approval_date)
        self.assertFalse(booking.did_client_reject)
        self.assertIsNone(booking.client_rejection_date)

    def test_photographer_approval_twice(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = False
        data['did_client_approve'] = False
        booking = Booking.objects.create(**data)
        booking.on_photographer_approval()
        self.assertRaises(AssertionError, booking.on_photographer_approval)

    def test_staff_approval(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = True
        data['did_staff_approve'] = False
        data['did_client_approve'] = False
        booking = Booking.objects.create(**data)

        # Approve
        self.assertFalse(booking.did_staff_approve)
        self.assertIsNone(booking.staff_approval_date)
        self.assertFalse(booking.media_file)
        booking.on_staff_approval()
        self.assertTrue(booking.did_staff_approve)
        self.assertIsNotNone(booking.staff_approval_date)
        self.assertTrue(booking.media_file)

        # Make sure notification gets sent to client
        outbox = [m for m in mail.outbox if 'We have finalized your SHOTZU booking' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(booking.client.email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)
        self.assertEqual(self.client.cc_recipients, notification_mail.cc)

    def test_staff_approval_twice(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = True
        data['did_staff_approve'] = False
        data['did_client_approve'] = False
        booking = Booking.objects.create(**data)
        booking.on_staff_approval()
        self.assertRaises(AssertionError, booking.on_staff_approval)

    def test_staff_approval_without_photographer_approval(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = False
        data['did_staff_approve'] = False
        data['did_client_approve'] = False
        booking = Booking.objects.create(**data)
        self.assertRaises(AssertionError, booking.on_staff_approval)

    def test_staff_approval_after_client_rejection(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = True
        data['did_staff_approve'] = False
        data['did_client_approve'] = False
        data['did_client_reject'] = True
        data['client_rejection_date'] = timezone.now()
        booking = Booking.objects.create(**data)

        # Approve
        self.assertFalse(booking.did_staff_approve)
        self.assertIsNone(booking.staff_approval_date)
        self.assertFalse(booking.media_file)
        booking.on_staff_approval()
        self.assertTrue(booking.did_staff_approve)
        self.assertIsNotNone(booking.staff_approval_date)
        self.assertTrue(booking.media_file)
        self.assertFalse(booking.did_client_approve)
        self.assertFalse(booking.did_client_reject)
        self.assertIsNone(booking.client_approval_date)
        self.assertIsNone(booking.client_rejection_date)

    def test_submit_for_settlement(self):
        data = self.data.copy()
        data['num_shots'] = 25
        data['did_photographer_approve'] = True
        data['did_staff_approve'] = True
        data['did_client_approve'] = True
        data['photographer_approval_date'] = timezone.now()
        data['staff_approval_date'] = timezone.now()
        data['client_approval_date'] = timezone.now()
        booking = Booking.objects.create(**data)
        booking.submit_for_settlement()

        # Check transaction status
        transaction = braintree.Transaction.find(booking.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), booking.amount)
        self.assertEqual(Decimal(transaction.service_fee_amount), booking.service_fee_amount)
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        self.assertIsNone(transaction.escrow_status)

        # Check for receipt
        outbox = [m for m in mail.outbox if 'SHOTZU Payment Receipt' == m.subject]
        receipt = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, receipt.from_email)
        self.assertEqual(booking.client.email_address, receipt.to[0])
        self.assertEqual(STAFF_EMAILS, receipt.bcc)
        self.assertEqual(booking.client.cc_recipients, receipt.cc)

    def test_send_contact_notification(self):
        data = self.data.copy()
        data['time_range'] = None
        data['contact_email_address'] = self.client.email_address
        data['contact_name'] = self.client.full_name
        data['contact_phone_number'] = '5555555555'
        booking = Booking.objects.create(**data)

        # Notify contact
        booking.send_contact_notification()

        # Make sure notification gets sent
        outbox = [m for m in mail.outbox if 'Postmates x Shotzu: Schedule Your Photoshoot Time' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(booking.contact_email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_send_contact_mistake_notification(self):
        data = self.data.copy()
        data['time_range'] = None
        data['contact_email_address'] = self.client.email_address
        data['contact_name'] = self.client.full_name
        data['contact_phone_number'] = '5555555555'
        booking = Booking.objects.create(**data)

        # Notify contact
        booking.send_contact_mistake_notification()

        # Make sure notification gets sent
        outbox = [m for m in mail.outbox if 'Postmates x Shotzu: Please Reschedule Your Photoshoot Time' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(booking.contact_email_address, notification_mail.to[0])
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_create_csv_file(self):
        bookings = create_bookings(5, do_create_images=False)
        csv_file_path = Booking.create_csv_file(Booking.objects.all())
        self.assertIsNotNone(csv_file_path)
        self.assertTrue(os.path.exists(csv_file_path))

        with open(csv_file_path, 'r') as csv_file:
            reader = csv.reader(csv_file)

        # Cleanup
        os.remove(csv_file_path)
        for booking in bookings:
            for booking_image in booking.images.all():
                booking_image.delete()
            booking.delete()

    def test_send_photographer_completion(self):
        data = self.data.copy()
        booking = Booking.objects.create(**data)
        booking.address.time_zone = pytz.timezone('US/Eastern')
        booking.address.save()
        booking.send_photographer_completion()
        outbox = [m for m in mail.outbox if 'Have you completed your SHOTZU photoshoot?' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual([booking.photographer.email_address], notification_mail.to)
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def create_booking(self, key_to_remove):
        data = self.data.copy()
        data.pop(key_to_remove)
        booking = Booking.objects.create(**data)
        self.assertIsNotNone(booking)
        self.assertEqual(booking.braintree_id, '')
        self.assertEqual(Booking.objects.all().count(), 1)
        return booking


class PhotographerTestCase(TestCase):
    def setUp(self):
        photographers = create_photographers(1)
        self.photographer = photographers[0]
        mail.outbox = []

    def tearDown(self):
        tear_down_photographers()
        mail.outbox = []

    def test_init(self):
        Photographer()

    # def test_rating(self):
    #     photographer = Photographer.objects.all()[0]
    #     sum = 0
    #     for review in photographer.reviews.all():
    #         sum += review.rating
    #     self.assertEqual(float(sum) / photographer.reviews.all().count(), photographer.rating)

    def test_notification_email(self):
        tear_down_photographers()
        photographer = create_photographers(1)[0]
        self.assertIsNotNone(photographer)

        outbox = [m for m in mail.outbox if 'A new photographer was created by %s' % photographer.profile.full_name == m.subject]
        self.assertEqual(len(outbox), 1)
        server_mail = outbox[0]
        self.assertEqual(SERVER_EMAIL, server_mail.from_email)
        self.assertEqual(STAFF_EMAILS, server_mail.recipients())


@skip('')
class PhotographerImageTestCase(TestCase):
    def test_init(self):
        PhotographerImage()

    def tearDown(self):
        tear_down_photographers()

    def setUp(self):
        photographers = create_photographers(1, num_images=5)
        self.photographer = photographers[0]

    def test_thumbnail_generation(self):
        aliases = [
            '480x260',
            '0x120',
            '1024x0'
        ]
        for image in self.photographer.images.all():
            for alias in aliases:
                self.assertIsNotNone(image.image[alias].url)


class ReviewTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ReviewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ReviewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        self.booking = Booking.objects.create(**dict({
            'thread': Thread.objects.create(),
            'photographer': self.photographer,
            'client': self.client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        }))
        self.data = dict({
            'booking': self.booking,
            'content': 'Hello World!',
            'rating': 5,
            'title': 'This is the best evah',
            'reviewer': self.client
        })
        mail.outbox = []

    def test_init(self):
        Review()

    def test_create_review(self):
        review = Review.objects.create(**self.data)
        self.assertIsNotNone(review)


class PhotographerRequestTestCase(TestCase):
    data = {
        'full_name': 'Sterling Archer',
        'email_address': 'success+sterlingarcher@simulator.amazonses.com',
        'service_type': ServiceType.REAL_ESTATE,
        'locality': 'New York',
        'region': 'New York',
        'postal_code': '12345',
        'message': 'Danger ZONE!!!',
        'time_period': TimePeriodType.ANYTIME,
        'date': timezone.now()
    }

    def test_init(self):
        PhotographerRequest()

    def test_notification_email_gets_sent(self):
        photographer_request = PhotographerRequest.objects.create(**self.data)
        self.assertIsNotNone(photographer_request)

        # Make sure email gets sent
        self.assertGreaterEqual(len(mail.outbox), 2)

        # Check sender
        self.assertEqual(mail.outbox[0].from_email, SERVER_EMAIL)

    def test_confirmation_email_gets_sent(self):
        photographer_request = PhotographerRequest.objects.create(**self.data)
        self.assertIsNotNone(photographer_request)

        # Make sure email gets sent
        self.assertGreaterEqual(len(mail.outbox), 2)

        # Check sender
        self.assertEqual(mail.outbox[1].from_email, DEFAULT_FROM_EMAIL)

    def test_create_photographer_request_without_optional_fields(self):
        data = self.data.copy()
        data.pop('locality')
        data.pop('region')
        data.pop('postal_code')
        data.pop('message')
        photographer_request = PhotographerRequest.objects.create(**data)
        self.assertIsNotNone(photographer_request)


class BookingCheckoutURLTestCase(TestCase):
    def setUp(self):
        self.profile = create_profiles(1)[0]
        self.data = {
            'line_item': 'Video Services',
            'price': 500,
            'amount_withheld': 50,
            'customer': self.profile
        }
        mail.outbox = list()

    def test_init(self):
        BookingCheckoutURL()

    def test_create_booking_checkout_url(self):
        BookingCheckoutURL.objects.create(**self.data)

    def test_multiple_booking_checkout_urls(self):
        for i in range(1000):
            data = self.data.copy()
            data['price'] = random.randint(500, 9000)
            create_booking_url = BookingCheckoutURL.objects.create(**data)

    def test_checkout_email(self):
        booking_checkout_url = BookingCheckoutURL.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'Your SHOTZU Booking' == m.subject]
        self.assertEqual(len(outbox), 1)
        checkout_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, checkout_mail.from_email)
        self.assertEqual(len(checkout_mail.to), 1)
        self.assertEqual(booking_checkout_url.customer.email_address, checkout_mail.to[0])
        self.assertEqual(STAFF_EMAILS, checkout_mail.bcc)


class ReviewURLTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ReviewURLTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ReviewURLTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        booking = Booking.objects.create(**dict({
            'thread': Thread.objects.create(),
            'photographer': self.photographer,
            'client': self.client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        }))
        self.data = {
            'client': self.client,
            'booking': booking,
            'download_url': 'http://www.google.com/',
        }
        mail.outbox = list()

    def tearDown(self):
        ReviewURL.objects.all().delete()

    def test_init(self):
        ReviewURL()

    def test_create_review_url(self):
        ReviewURL.objects.create(**self.data)

    def test_multiple_review_urls(self):
        review_urls = list()
        for i in range(1000):
            data = self.data.copy()
            review_url = ReviewURL.objects.create(**data)
            self.assertFalse(bool([i for i in review_urls if i.key == review_url.key]))
            review_urls.append(review_url)

    def test_download_email(self):
        review_url = ReviewURL.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'Files from your SHOTZU booking' == m.subject]
        self.assertEqual(len(outbox), 1)
        review_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, review_mail.from_email)
        self.assertEqual(len(review_mail.to), 1)
        self.assertEqual(review_url.client.email_address, review_mail.to[0])
        self.assertEqual(STAFF_EMAILS, review_mail.bcc)

    def test_review_email(self):
        data = self.data.copy()
        data.pop('download_url')
        review_url = ReviewURL.objects.create(**data)
        outbox = [m for m in mail.outbox if 'How would you rate your SHOTZU booking?' == m.subject]
        self.assertEqual(len(outbox), 1)
        review_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, review_mail.from_email)
        self.assertEqual(len(review_mail.to), 1)
        self.assertEqual(review_url.client.email_address, review_mail.to[0])
        self.assertEqual(STAFF_EMAILS, review_mail.bcc)


class BookingAlbumTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAlbumTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(5, do_create_images=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingAlbumTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = random.choice(Booking.objects.all())
        self.data = {
            'booking': self.booking,
            'name': 'Original',
            'width': 500,
            'height': 500
        }

    def test_init(self):
        BookingAlbum()

    def test_create_booking_album(self):
        booking_album = BookingAlbum.objects.create(**self.data)
        for booking_image in BookingImage.objects.filter(booking=self.booking):
            booking_album.images.add(booking_image)


class BookingImageTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingImageTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingImageTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        booking = Booking.objects.create(**dict({
            'thread': Thread.objects.create(),
            'photographer': self.photographer,
            'client': self.client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        }))
        self.data = {
            'booking': booking,
            'image': self.get_booking_image_file(),
            'name': get_lorem_text(1, 'w')
        }
        mail.outbox = []

    def get_booking_image_file(self):
        booking_images_dir = os.path.join(FIXTURES_DIR, 'booking_images/')
        files = os.listdir(booking_images_dir)
        files = [f for f in files if 'DS_Store' not in f]
        file_paths = [os.path.join(booking_images_dir, f) for f in files]
        file_path = random.choice(file_paths)
        return ContentFile(open(file_path).read(), name=os.path.basename(file_path))

    def test_init(self):
        BookingImage()

    def test_create_booking_image(self):
        booking_image = BookingImage.objects.create(**self.data)
        self.assertIsNotNone(booking_image)
        self.assertIsNotNone(booking_image.image.url)
        self.assertTrue(booking_image.image.url)

    def test_thumbnail_generation(self):
        booking_image = BookingImage.objects.create(**self.data)
        self.assertIsNotNone(booking_image)
        aliases = [
            '500x0',
        ]
        for alias in aliases:
            thumbnail = booking_image.image[alias]
            self.assertIsNotNone(thumbnail)
            self.assertIsNotNone(thumbnail.url)

    def test_staff_notification_email(self):
        booking_image = BookingImage.objects.create(**self.data)
        self.assertIsNotNone(booking_image)
        outbox = [m for m in mail.outbox if 'Images have been uploaded for your SHOTZU Booking' in m.subject]
        self.assertEqual(len(outbox), 1)
        notification_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), len(STAFF_EMAILS))
        # self.assertEqual(notification_mail.to[0], booking_image.booking.client.email_address)
        # self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_send_photographer_notification_when_rejected(self):
        self.assertEqual(len(mail.outbox), 0)
        booking_image = BookingImage.objects.create(**self.data)
        mail.outbox = list()
        booking_image.did_client_reject = True
        booking_image.save()
        booking_image.send_rejection_notification(self.client)
        self.assertEqual(len(mail.outbox), 1)
        notification_mail = mail.outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(notification_mail.to[0], booking_image.booking.photographer.email_address)
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_send_photographer_notification_when_commented(self):
        self.assertEqual(len(mail.outbox), 0)
        booking_image = BookingImage.objects.create(**self.data)
        mail.outbox = list()
        booking_image.comment = 'DANGER ZONE!>!>!?!?!'
        booking_image.save()
        booking_image.send_comment_notification(self.client)
        self.assertEqual(len(mail.outbox), 1)
        notification_mail = mail.outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
        self.assertEqual(len(notification_mail.to), 1)
        self.assertEqual(notification_mail.to[0], booking_image.booking.photographer.email_address)
        self.assertEqual(STAFF_EMAILS, notification_mail.bcc)

    def test_tiff_file(self):
        data = self.data.copy()
        booking_images_dir = os.path.join(FIXTURES_DIR, 'booking_images/')
        files = os.listdir(booking_images_dir)
        files = [f for f in files if 'cookies' in f]
        file_paths = [os.path.join(booking_images_dir, f) for f in files]
        file_path = random.choice(file_paths)
        data['image'] = ContentFile(open(file_path).read(), os.path.basename(file_path))
        booking_image = BookingImage.objects.create(**data)
        self.assertIsNotNone(booking_image)


class BookingSurchargeTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingSurchargeTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BookingSurchargeTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1)[0]
        self.data = dict({
            'booking': self.booking,
            'amount': 350.00,
            'service_fee_amount': 100.00
        })

    def tearDown(self):
        Booking.objects.all().delete()
        BookingSurcharge.objects.all().delete()

    def verify_braintree_data(self, booking_surcharge):
        transaction = braintree.Transaction.find(booking_surcharge.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertEqual(Decimal(transaction.service_fee_amount), self.data.get('service_fee_amount'))
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )
        self.assertIsNone(
            transaction.escrow_status,
        )

    def test_init(self):
        BookingSurcharge()

    def test_create_booking_surcharge(self):
        booking_surcharge = BookingSurcharge.objects.create(**self.data)
        self.assertIsNotNone(booking_surcharge)

    def test_braintree_data(self):
        booking_surcharge = BookingSurcharge.objects.create(**self.data)
        booking_surcharge.sale()

        # Verify data in Braintree
        self.verify_braintree_data(booking_surcharge)
        self.assertEqual(BookingSurcharge.objects.exclude(braintree_id='').count(), 1)

    def test_check_for_duplicate_transactions(self):
        self.assertEqual(BookingSurcharge.objects.all().count(), 0)
        booking_surcharge = BookingSurcharge.objects.create(**self.data)
        booking_surcharge.sale(do_force_duplicate_check=True)
        self.verify_braintree_data(booking_surcharge)
        booking_surcharge = BookingSurcharge.objects.create(**self.data)
        self.assertRaises(AssertionError, booking_surcharge.sale, do_force_duplicate_check=True)
        self.assertEqual(BookingSurcharge.objects.exclude(braintree_id='').count(), 1)


class TipTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(TipTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(TipTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        self.booking = Booking.objects.create(**dict({
            'thread': Thread.objects.create(),
            'photographer': self.photographer,
            'client': self.client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': self.client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        }))
        self.data = dict({
            'booking': self.booking,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 0
        })
        mail.outbox = []

    def verify_braintree_data(self, tip):
        transaction = braintree.Transaction.find(tip.braintree_id)
        self.assertIsNotNone(transaction)
        self.assertEqual(Decimal(transaction.amount), self.data.get('amount'))
        self.assertEqual(Decimal(transaction.service_fee_amount), self.data.get('service_fee_amount'))
        self.assertEqual(
            transaction.status,
            BraintreeTransactionStatusType.labels.get(
                BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
            )
        )

    def test_init(self):
        Tip()

    def test_create_tip(self):
        tip = Tip.objects.create(**self.data)
        self.assertIsNotNone(tip)

    def test_braintree_data(self):
        tip = Tip.objects.create(**self.data)

        # Verify data in Braintree
        self.verify_braintree_data(tip)

    def test_check_for_duplicate_transactions(self):
        self.assertEqual(Tip.objects.all().count(), 0)
        tip = Tip.objects.create(**self.data)
        self.assertRaises(AssertionError, Tip.objects.create, **self.data)
        self.assertEqual(Tip.objects.all().count(), 1)

    def test_staff_notification_email(self):
        tip = Tip.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'A new tip was created by %s' % tip.client.full_name == m.subject]
        self.assertEqual(len(outbox), 1)
        server_mail = outbox[0]
        self.assertEqual(SERVER_EMAIL, server_mail.from_email)
        self.assertEqual(STAFF_EMAILS, server_mail.recipients())

    def test_confirmation_email(self):
        tip = Tip.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'Tip Confirmation' == m.subject]
        self.assertEqual(len(outbox), 1)
        confirmation_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, confirmation_mail.from_email)
        self.assertEqual(len(confirmation_mail.recipients()), 1)
        self.assertEqual(tip.client.email_address, confirmation_mail.recipients()[0])

    def test_photographer_notification_email(self):
        tip = Tip.objects.create(**self.data)
        outbox = [m for m in mail.outbox if 'You received a tip from %s' % tip.client.full_name == m.subject]
        self.assertEqual(len(outbox), 1)
        confirmation_mail = outbox[0]
        self.assertEqual(DEFAULT_FROM_EMAIL, confirmation_mail.from_email)
        self.assertEqual(len(confirmation_mail.recipients()), 1)
        self.assertEqual(tip.photographer.email_address, confirmation_mail.recipients()[0])


# class BookingFileTestCase(TestCase):
#     @classmethod
#     def setUpClass(cls):
#         super(BookingFileTestCase, cls).setUpClass()
#         create_photographers(2, do_create_marketplace=True)
#
#     @classmethod
#     def tearDownClass(cls):
#         super(BookingFileTestCase, cls).tearDownClass()
#         tear_down_photographers()
#
#     def setUp(self):
#         photographers = Photographer.objects.all()
#         time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
#         self.photographer = photographers[0]
#         self.client = photographers[1].profile
#         booking = Booking.objects.create(**dict({
#             'photographer': self.photographer,
#             'client': self.client,
#             'time_range': time_range,
#             'service_type': ServiceType.REAL_ESTATE,
#             'message': 'Hello World!',
#             'amount': 350.00,
#             'service_fee_amount': 350.00 * AMOUNT_WITHHELD_PERCENTAGE / 100,
#             'phone_number': '5555555555',
#             'address': Address.objects.create(**{
#                 'full_name': self.client.full_name,
#                 'street_address': '27 Treasure Cove Drive',
#                 'postal_code': '77381',
#                 'locality': 'Spring',
#                 'region': 'TX',
#                 'country': 'US'
#             })
#         }))
#         self.data = {
#             'booking': booking,
#             'file': self.get_file('images.zip')
#         }
#         mail.outbox = []
#
#     def test_init(self):
#         BookingFile()
#
#     def test_create_booking_image(self):
#         booking_image = BookingImage.objects.create(**self.data)
#         self.assertIsNotNone(booking_image)
#
#     def test_thumbnail_generation(self):
#         booking_image = BookingImage.objects.create(**self.data)
#         self.assertIsNotNone(booking_image)
#         aliases = [
#             '500x0',
#         ]
#         for alias in aliases:
#             thumbnail = booking_image.image[alias]
#             self.assertIsNotNone(thumbnail)
#             self.assertIsNotNone(thumbnail.url)
#
#     def test_notification_email(self):
#         booking_image = BookingImage.objects.create(**self.data)
#         self.assertIsNotNone(booking_image)
#         self.assertEqual(len(mail.outbox), 1)
#         notification_mail = mail.outbox[0]
#         self.assertEqual(DEFAULT_FROM_EMAIL, notification_mail.from_email)
#         self.assertEqual(len(notification_mail.to), 1)
#         self.assertEqual(notification_mail.to[0], booking_image.booking.client.email_address)
#         self.assertEqual(STAFF_EMAILS, notification_mail.bcc)
#
#     def get_file(self, name):
#         booking_files_dir = os.path.join(FIXTURES_DIR, 'booking_files/')
#         file_path = os.path.join(booking_files_dir, name)
#         return File(open(file_path))
