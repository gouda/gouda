from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import date
from datetime import datetime
from datetime import time
from datetime import timedelta
from importlib import import_module

import os
import pytz
from braintree.test.nonces import Nonces
from django.conf import settings
from django.core import signing
from django.core.files.base import ContentFile
from django.core.urlresolvers import reverse
from django.utils import timezone
from django.utils.http import urlquote
from django_webtest import WebTest
from gouda.addresses.models import Address
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.utils import braintree
from gouda.photographers import salt
from gouda.photographers.keys import BookingSessionKeys
from gouda.photographers.keys import BulkBookingSessionKeys
from gouda.photographers.keys import FoodBookingSessionKeys
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingCheckoutURL
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import BulkBooking
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Photographer
from gouda.photographers.models import PhotographerRequest
from gouda.photographers.models import Review
from gouda.photographers.models import ReviewURL
from gouda.photographers.models import ServiceType
from gouda.photographers.models import TimePeriodType
from gouda.photographers.models import Tip
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers
from gouda.postmaster.models import Thread
from gouda.profiles.models import Profile
from gouda.profiles.models import StatusType
from gouda.profiles.utils import create_profiles
from gouda.profiles.utils import tear_down_profiles
from gouda.users.models import User
from psycopg2.extras import DateTimeTZRange
from rest_framework import status
from webtest import Upload
from gouda.customers.utils import tear_down_customers
from gouda.customers.utils import create_customers
from django.utils.http import urlquote

FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')


def get_booking_file_path():
    booking_files_dir = os.path.join(FIXTURES_DIR, 'booking_files/')
    return os.path.join(booking_files_dir, 'postmates.csv')


class BaseTestCases(object):
    class BaseCheckoutViewTestCase(WebTest):
        user = None
        booking_checkout_url = None

        def get_view_name(self):
            raise NotImplementedError()

        def get_booking_checkout_url(self, user):
            self.booking_checkout_url = BookingCheckoutURL.objects.create(**{
                'customer': user.profile,
                'line_item': 'Photography Services',
                'price': 500,
                'amount_withheld': 50,
            })
            return self.booking_checkout_url

        def get_booking_details(self, user=None):
            if user is None:
                user = self.user
            self.get_booking_checkout_url(user)
            url = reverse('photographers:get_booking_details', kwargs={'booking_checkout_url': self.booking_checkout_url.key})
            data = {
                'display_name': 'DANGERZONE!!!',
                'service_type': ServiceType.PHOTOGRAPHY,
                'date': date.today(),
                'start_time': '3:00 AM',
                'end_time': '4:00 AM',
                'phone_number': '5555555555',
            }
            response = self.app.get(url, user=user.email_address)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            form = response.form
            for k in data.keys():
                form[k] = data.get(k)
            response = form.submit()
            self.assertRedirects(response, reverse('photographers:create_booking_address', kwargs={'booking_checkout_url': self.booking_checkout_url.key}))

        def create_booking_address(self, user=None):
            if user is None:
                user = self.user
            num_addresses = Address.objects.all().count()
            data = {
                'full_name': user.full_name,
                'street_address': '5719 Old Buggy Court',
                'postal_code': '21045',
                'locality': 'Columbia',
                'region': 'MD',
            }
            url = reverse('photographers:create_booking_address', kwargs={'booking_checkout_url': self.booking_checkout_url.key})
            response = self.app.get(url, user=user.email_address)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            form = response.form
            for k in data.keys():
                form[k] = data.get(k)
            response = form.submit()
            self.assertRedirects(response, reverse('photographers:create_customer', kwargs={'booking_checkout_url': self.booking_checkout_url.key}))
            self.assertEqual(Address.objects.all().count(), num_addresses + 1)

        def create_customer(self):
            data = {
                'payment_method_nonce': Nonces.Transactable,
            }
            url = reverse('photographers:create_customer', kwargs={'booking_checkout_url': self.booking_checkout_url.key})
            response = self.app.get(url, user=self.user.email_address)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            form = response.form
            for k in data.keys():
                form[k] = data.get(k)
            response = form.submit()
            self.assertRedirects(response, reverse('photographers:create_booking', kwargs={'booking_checkout_url': self.booking_checkout_url.key}))
            braintree_customer = BraintreeCustomer.objects.all()[0]
            customer = braintree.Customer.find(braintree_customer.braintree_id)
            self.assertIsNotNone(customer)

        def test_invalid_checkout_url(self):
            url = reverse(self.get_view_name(), kwargs={'booking_checkout_url': 'ABCD'})
            response = self.app.get(url, user=self.user.email_address, expect_errors=True)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        def test_mismatched_checkout_url(self):
            other_user = User.objects.exclude(pk=self.user.pk)[0]
            booking_checkout_url = BookingCheckoutURL.objects.create(**{
                'customer': other_user.profile,
                'line_item': 'Photography Services',
                'price': 9000,
                'amount_withheld': 900
            })
            url = reverse(self.get_view_name(), kwargs={'booking_checkout_url': booking_checkout_url.key})
            response = self.app.get(url, user=self.user.email_address, expect_errors=True)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

        def test_no_authentication(self):
            url = reverse('profiles:logout')
            self.app.get(url)
            url = self.get_url()
            response = self.app.get(url, expect_errors=True)
            self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

        def test_used_checkout_url(self):
            booking_checkout_url = BookingCheckoutURL.objects.create(**{
                'customer': self.user.profile,
                'line_item': 'Photography Services',
                'price': 9000,
                'amount_withheld': 900,
                'has_been_used': True
            })
            url = reverse(self.get_view_name(), kwargs={'booking_checkout_url': booking_checkout_url.key})
            response = self.app.get(url, user=self.user.email_address, expect_errors=True)
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    class BasePhotographerViewTestCase(WebTest):
        view_name = None
        template_name = None
        do_never_cache = True
        user = None

        def get_url(self):
            raise NotImplementedError()

        @classmethod
        def setUpClass(cls):
            super(BaseTestCases.BasePhotographerViewTestCase, cls).setUpClass()
            create_photographers(1)
            # for user in User.objects.all():
            #     BraintreeCustomer.objects.update_or_create_braintree_customer(user, Nonces.Transactable)
            # user = Listing.objects.all()[0].profile.user
            # individual = {
            #     'locality': 'Bakersfield',
            #     'postal_code': '93311',
            #     'region': 'CA',
            #     'street_address': '1504 Parkpath Way',
            #     'date_of_birth': date.today() - timedelta(days=365 * 28),
            #     'email': user.email_address,
            #     'first_name': user.first_name,
            #     'last_name': user.last_name,
            #     'phone': '4437421240'
            # }
            # funding = {
            #     'email': user.email_address,
            #     'mobile_phone': '5555555555',
            #     'account_number': '1123581321',
            #     'routing_number': '071101307',
            # }
            # BraintreeMerchantAccount.objects.update_or_create_braintree_merchant_account(
            #     user=user,
            #     tos_accepted=True,
            #     individual=individual,
            #     funding=funding
            # )
            # cache.clear()

        @classmethod
        def tearDownClass(cls):
            super(BaseTestCases.BasePhotographerViewTestCase, cls).tearDownClass()
            tear_down_photographers()

        def test_never_cache_headers(self):
            if not self.do_never_cache:
                return
            url = self.get_url()
            response = self.app.get(url, user=self.user.email_address if self.user else None)
            self.assertEqual(response.status_code, status.HTTP_200_OK)

            # Check caching
            self.assertIn('Last-Modified', response.headers)
            self.assertIn('Expires', response.headers)
            self.assertIn('Cache-Control', response.headers)
            self.assertIn('max-age=0', response.headers['Cache-Control'])


class ReviewCreateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ReviewCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        photographers = Photographer.objects.all()
        time_range = DateTimeTZRange(lower=timezone.now(), upper=timezone.now() + timedelta(hours=1))
        photographer = photographers[0]
        client = photographers[1].profile
        booking = Booking.objects.create(**dict({
            'thread': Thread.objects.create(),
            'photographer': photographer,
            'client': client,
            'time_range': time_range,
            'service_type': ServiceType.REAL_ESTATE,
            'message': 'Hello World!',
            'amount': 350.00,
            'service_fee_amount': 10,
            'phone_number': '5555555555',
            'address': Address.objects.create(**{
                'full_name': client.full_name,
                'street_address': '27 Treasure Cove Drive',
                'postal_code': '77381',
                'locality': 'Spring',
                'region': 'TX',
                'country': 'US'
            })
        }))

    @classmethod
    def tearDownClass(cls):
        super(ReviewCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def get_url(self, review_url=None):
        return reverse('photographers:create_review', kwargs={'key': review_url.key if review_url else self.review_url.key})

    def setUp(self):
        self.booking = Booking.objects.all()[0]
        self.profile = self.booking.client
        self.review_url = ReviewURL.objects.create(**{
            'client': self.booking.client,
            'booking': self.booking
        })
        self.data = dict({
            'content': 'Hello World!',
            'rating': 5,
            'title': 'This is the best evah',
        })

    def tearDown(self):
        ReviewURL.objects.all().delete()
        Review.objects.all().delete()
        Tip.objects.all().delete()

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/create_review.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertIsNotNone(response.form)

    def test_create_review(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:search'))
        self.assertEqual(Review.objects.all().count(), 1)
        self.assertEqual(Tip.objects.all().count(), 0)

    def test_create_review_with_tip(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        data['amount'] = 350
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:search'))
        self.assertEqual(Review.objects.all().count(), 1)
        self.assertEqual(Tip.objects.all().count(), 1)

    def test_invalid_review_url(self):
        url = reverse('photographers:create_review', kwargs={'key': 'ABCD'})
        response = self.app.get(url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_mismatched_review_url(self):
        other_user = User.objects.exclude(pk=self.profile.user.pk)[0]
        review_url = ReviewURL.objects.create(**{
            'client': other_user.profile,
            'booking': self.booking
        })
        url = self.get_url(review_url)
        response = self.app.get(url, user=self.profile.user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_no_authentication(self):
        url = reverse('profiles:logout')
        self.app.get(url)
        url = self.get_url()
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_used_review_url(self):
        review_url = ReviewURL.objects.create(**{
            'client': self.profile,
            'booking': self.booking,
            'date_used': timezone.now()
        })
        url = self.get_url(review_url)
        response = self.app.get(url, user=self.profile.user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)


class PhotographerCreateViewTestCase(WebTest):
    def setUp(self):
        self.profile = create_profiles(1)[0]
        self.user = self.profile.user
        date_of_birth = date.today() - timedelta(days=365 * 28)
        self.data = {
            'individual_locality': 'Bakersfield',
            'individual_postal_code': '93311',
            'individual_region': 'MD',
            'individual_street_address': '1504 Parkpath Way',
            'individual_month': int(date_of_birth.strftime('%m')),
            'individual_day': int(date_of_birth.strftime('%d')),
            'individual_year': int(date_of_birth.strftime('%Y')),
            'individual_email': self.user.email_address,
            'individual_first_name': self.user.first_name,
            'individual_last_name': self.user.last_name,
            'individual_phone': '(443) 742-1240',

            # 'business_locality': 'Bakersfield',
            # 'business_postal_code': '93311',
            # 'business_region': 'CA',
            # 'business_street_address': '1504 Parkpath Way',
            # 'business_dba_name': 'Danger Zone R\' Us',
            # 'business_legal_name': 'Danger Zone R\' Us',
            # 'business_tax_id': '123457689',

            # 'funding_email': self.user.email_address,
            # 'funding_mobile_phone': '555-555-5555',
            'funding_account_number': '1123581321',
            'funding_routing_number': '071101307',
        }

    def tearDown(self):
        tear_down_photographers()
        BraintreeMerchantAccount.objects.all().delete()

    def get_url(self):
        return reverse('photographers:create_photographer')

    def test_never_cache_headers(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/create_photographer.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertIsNotNone(response.form)

    def test_create_photographer(self):
        self.assertEqual(Photographer.objects.all().count(), 0)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)
        self.create_photographer()
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

    def test_create_photographer_with_business(self):
        data = self.data.copy()
        data.update({
            'business_locality': 'Bakersfield',
            'business_postal_code': '93311',
            'business_region': 'CA',
            'business_street_address': '1504 Parkpath Way',
            'business_dba_name': 'Danger Zone R\' Us',
            'business_legal_name': 'Danger Zone R\' Us',
            'business_tax_id': '123457689',
        })
        self.assertEqual(Photographer.objects.all().count(), 0)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)
        self.create_photographer(data=data)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

    def create_photographer(self, do_expect_errors=False, data=None, profile=None):
        if not data:
            data = self.data.copy()
        url = self.get_url()
        response = self.app.get(url, user=profile.email_address if profile else self.profile.email_address)
        form = response.form
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit(expect_errors=do_expect_errors)
        if do_expect_errors:
            self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
        else:
            self.assertRedirects(response, reverse('photographers:search'))

    def test_create_photographer_twice(self):
        self.assertEqual(Photographer.objects.all().count(), 0)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)
        self.create_photographer()
        self.create_photographer(do_expect_errors=True)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)

    def test_create_photographer_without_date_of_birth(self):
        profile = create_profiles(1)[0]
        profile.date_of_birth = None
        profile.save()
        self.assertEqual(Photographer.objects.all().count(), 0)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 0)
        self.create_photographer(profile=profile)
        self.assertEqual(Photographer.objects.all().count(), 1)
        self.assertEqual(BraintreeMerchantAccount.objects.all().count(), 1)


class PhotographerSearchViewTestCase(BaseTestCases.BasePhotographerViewTestCase):
    data = {
        'region': 'MD',
        'service_type': ServiceType.INSPECTIONS,
        'date': date.today(),
        'time_period': TimePeriodType.EVENING
    }

    def setUp(self):
        self.profile = create_profiles(1)[0]

    def tearDown(self):
        PhotographerRequest.objects.all().delete()
        tear_down_profiles()

    def test_food_photography_redirects_to_checkout(self):
        data = self.data.copy()
        data['service_type'] = ServiceType.FOOD
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:get_food_booking_details'))

        # Check session
        self.assertIn(BookingSessionKeys.date, self.app.session)
        self.assertIn(BookingSessionKeys.service_type, self.app.session)
        self.assertIn(BookingSessionKeys.region, self.app.session)
        self.assertIn(BookingSessionKeys.start_time, self.app.session)
        self.assertIn(BookingSessionKeys.end_time, self.app.session)
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.date))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.service_type))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.region))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.start_time))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.end_time))

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/search.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertIsNotNone(response.form)

    def test_create_booking_query(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_photographer_request'))

    def test_anonymous_booking_query(self):
        url = self.get_url()
        response = self.app.get(url)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_photographer_request'))

    def test_query_with_unverified_profile(self):
        url = self.get_url()
        self.profile.status = StatusType.UNVERIFIED
        self.profile.save()
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_photographer_request'))
        self.profile.status = StatusType.ACTIVE
        self.profile.save()

    def test_on_demand_request(self):
        data = self.data.copy()
        data['service_type'] = ServiceType.ON_DEMAND
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_photographer_request'))
        # self.assertRedirects(response, reverse_with_query('photographers:create_photographer_request', data))

    def get_url(self):
        return reverse('photographers:search')


class PhotographerRequestCreateViewTestCase(WebTest):
    data = {
        'full_name': 'Sterling Archer',
        'phone_number': '5555555555',
        'email_address': 'success+sterlingarcher@simulator.amazonses.com',
        'service_type': ServiceType.REAL_ESTATE,
        'locality': 'New York',
        'region': 'MD',
        'message': 'Danger ZONE!!!',
        'postal_code': '12345',
        'time_period': TimePeriodType.ANYTIME,
        'date': date.today()
    }

    def setUp(self):
        self.profile = create_profiles(1)[0]

    def tearDown(self):
        PhotographerRequest.objects.all().delete()
        tear_down_profiles()

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/create_photographer_request.html')

    # def test_login_required(self):
    #     url = self.get_url()
    #     response = self.app.get(url)
    #     self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertIsNotNone(response.form)

    def test_create_photographer_request(self):
        self.assertEqual(PhotographerRequest.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:search'))
        self.assertEqual(PhotographerRequest.objects.all().count(), 1)

    # def test_block_verified_profiles(self):
    #     url = self.get_url()
    #     response = self.app.get(url, user=self.profile.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_photographer_request_with_required_fields_only(self):
        self.assertEqual(PhotographerRequest.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url)
        form = response.form
        for k in self.data.keys():
            if k in ['locality', 'region', 'message', 'postal_code']:
                continue
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:search'))
        self.assertEqual(PhotographerRequest.objects.all().count(), 1)

    def get_url(self):
        return reverse('photographers:create_photographer_request')


class BookingCreateViewTestCase(BaseTestCases.BaseCheckoutViewTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_profiles(10)

    @classmethod
    def tearDownClass(cls):
        super(BookingCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        self.profile = self.client
        self.user = self.profile.user
        self.data = {
            'message': 'Hello World!',
        }
        self.get_booking_details()
        self.create_booking_address()
        self.create_customer()

    def tearDown(self):
        Booking.objects.all().delete()

    def create_booking_without_session_data(self, session_key):
        session = self.app.session
        del session[session_key]
        session.save()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_booking_checkout_url_gets_used(self):
        self.create_booking()
        booking_checkout_url = BookingCheckoutURL.objects.get(key=self.booking_checkout_url.key)
        self.assertTrue(booking_checkout_url.has_been_used)
        self.assertIsNotNone(booking_checkout_url.date_used)

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/create_booking.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.form)

    def test_create_booking(self):
        self.create_booking()

    def create_booking(self):
        self.assertEqual(Booking.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:search'))
        self.assertEqual(Booking.objects.all().count(), 1)

    def test_create_booking_without_payment_details(self):
        profile = Profile.objects.filter(user__braintree_customer=None)[0]
        self.get_booking_details(profile.user)
        self.create_booking_address(profile.user)
        self.assertEqual(Booking.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_booking_without_booking_address(self):
        self.create_booking_without_session_data(BookingSessionKeys.address_id)

    def test_create_booking_without_service_type(self):
        self.create_booking_without_session_data(BookingSessionKeys.service_type)

    def test_create_booking_without_start_time(self):
        self.create_booking_without_session_data(BookingSessionKeys.start_time)

    def test_create_booking_without_end_time(self):
        self.create_booking_without_session_data(BookingSessionKeys.end_time)

    def test_create_booking_without_date(self):
        self.create_booking_without_session_data(BookingSessionKeys.date)

    def test_create_booking_without_credit_card(self):
        self.create_booking_without_session_data(BookingSessionKeys.credit_card)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:create_booking'

    def get_kwargs(self):
        return {'booking_checkout_url': self.booking_checkout_url.key}


class CustomerCreateViewTestCase(BaseTestCases.BaseCheckoutViewTestCase):
    @classmethod
    def setUpClass(cls):
        super(CustomerCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=False)

    @classmethod
    def tearDownClass(cls):
        super(CustomerCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(CustomerCreateViewTestCase, self).setUp()
        self.user = User.objects.all()[0]
        self.data = {
            'payment_method_nonce': Nonces.Transactable,
        }
        self.get_booking_details()
        self.create_booking_address()

    def tearDown(self):
        super(CustomerCreateViewTestCase, self).tearDown()
        BraintreeCustomer.objects.all().delete()
        Address.objects.all().delete()
        BookingCheckoutURL.objects.all().delete()

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:create_customer'

    def get_kwargs(self):
        return {'booking_checkout_url': self.booking_checkout_url.key}

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/create_customer.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertGreater(len(response.forms), 0)

    def test_create_customer(self):
        num_customers = BraintreeCustomer.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_booking', kwargs=self.get_kwargs()))
        self.assertEqual(BraintreeCustomer.objects.all().count(), num_customers + 1)
        braintree_customer = BraintreeCustomer.objects.all()[0]
        customer = braintree.Customer.find(braintree_customer.braintree_id)
        self.assertIsNotNone(customer)

    # def test_create_customer_without_booking_address(self):
    #     session = self.app.session
    #     del session[BookingSessionKeys.address_id]
    #     session.save()
    #     url = self.get_url()
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_repeat_customer(self):
        num_customers = BraintreeCustomer.objects.all().count()
        for i in range(3):
            url = self.get_url()
            response = self.app.get(url, user=self.user.email_address)
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            form = response.form
            for k in self.data.keys():
                form[k] = self.data.get(k)
            response = form.submit()
            self.assertRedirects(response, reverse('photographers:create_booking', kwargs=self.get_kwargs()))
        self.assertEqual(BraintreeCustomer.objects.all().count(), num_customers + 1)
        braintree_customer = BraintreeCustomer.objects.all()[0]
        customer = braintree.Customer.find(braintree_customer.braintree_id)
        self.assertIsNotNone(customer)

    def test_blank_data(self):
        num_customers = BraintreeCustomer.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(BraintreeCustomer.objects.all().count(), num_customers)
        for k in self.data.keys():
            self.assertFormError(response, 'form', k, 'This field is required.')


class BookingDetailsFormViewTestCase(BaseTestCases.BaseCheckoutViewTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingDetailsFormViewTestCase, cls).setUpClass()
        create_profiles(2)

    @classmethod
    def tearDownClass(cls):
        super(BookingDetailsFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BookingDetailsFormViewTestCase, self).setUp()
        self.user = User.objects.all()[0]
        self.data = {
            'display_name': 'DANGERZONE!!!',
            'service_type': ServiceType.PHOTOGRAPHY,
            'date': date.today(),
            'start_time': '3:00 AM',
            'end_time': '4:00AM',
            'phone_number': '5555555555',
        }
        self.booking_checkout_url = BookingCheckoutURL.objects.create(**{
            'customer': self.user.profile,
            'line_item': 'Photography Services',
            'price': 500,
            'amount_withheld': 50,
        })

    def tearDown(self):
        super(BookingDetailsFormViewTestCase, self).tearDown()
        BookingCheckoutURL.objects.all().delete()

    def get_url(self):
        return reverse(self.get_view_name(), kwargs={'booking_checkout_url': self.booking_checkout_url.key})

    def get_view_name(self):
        return 'photographers:get_booking_details'

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/get_booking_details.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_get_booking_details(self):
        self.get_booking_details()

    def get_booking_details(self, user=None):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_booking_address',
                                               kwargs={'booking_checkout_url': self.booking_checkout_url.key}))

    def test_blank_data(self):
        num_addresses = Address.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(Address.objects.all().count(), num_addresses)
        for k in self.data.keys():
            if k in ['service_type', 'time_period']:
                continue
            self.assertFormError(response, 'form', k, 'This field is required.')

    # def test_invalid_checkout_url(self):
    #     url = reverse('photographers:get_booking_details', kwargs={'booking_checkout_url': 'ABCD'})
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_mismatched_checkout_url(self):
    #     other_user = User.objects.exclude(pk=self.user.pk)[0]
    #     booking_checkout_url = BookingCheckoutURL.objects.create(**{
    #         'customer': other_user.profile,
    #         'line_item': 'Photography Services',
    #         'price': 9000,
    #     })
    #     url = reverse('photographers:get_booking_details', kwargs={'booking_checkout_url': booking_checkout_url.key})
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_no_authentication(self):
    #     url = self.get_url()
    #     response = self.app.get(url, expect_errors=True)
    #     self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_booking_details_in_session_data(self):
        self.get_booking_details()
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.date, None))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.service_type, None))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.start_time, None))
        self.assertIsNotNone(self.app.session.get(BookingSessionKeys.end_time, None))


class BookingAddressCreateViewTestCase(BaseTestCases.BaseCheckoutViewTestCase):
    @classmethod
    def setUpClass(cls):
        super(BookingAddressCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=False)

    @classmethod
    def tearDownClass(cls):
        super(BookingAddressCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BookingAddressCreateViewTestCase, self).setUp()
        Address.objects.all().delete()
        self.user = User.objects.all()[0]
        self.data = {
            'full_name': self.user.full_name,
            'street_address': '5719 Old Buggy Court',
            'postal_code': '21045',
            'locality': 'Columbia',
            'region': 'MD',
        }
        self.query_params = {
            'region': 'MD',
            'service_type': ServiceType.REAL_ESTATE,
            'time_period': TimePeriodType.ANYTIME,
            'date': date.today()
        }
        self.booking_checkout_url = BookingCheckoutURL.objects.create(**{
            'customer': self.user.profile,
            'line_item': 'Photography Services',
            'price': 500,
            'amount_withheld': 50,
        })

    def tearDown(self):
        super(BookingAddressCreateViewTestCase, self).tearDown()
        Address.objects.all().delete()
        BookingCheckoutURL.objects.all().delete()

    def get_url(self):
        return reverse(self.get_view_name(), kwargs={'booking_checkout_url': self.booking_checkout_url.key})
        # return reverse_with_query('photographers:create_booking_address', self.query_params)

    def get_view_name(self):
        return 'photographers:create_booking_address'

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/create_booking_address.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def setup_session(self):
        if 'django.contrib.sessions' in settings.INSTALLED_APPS:
            engine = import_module(settings.SESSION_ENGINE)
            cookie = self.app.cookies.get(settings.SESSION_COOKIE_NAME, None)
            if cookie:
                session = engine.SessionStore(cookie)
                session[BookingSessionKeys.service_type] = ServiceType.FOOD
                session.save()

    def test_create_booking_address(self):
        num_addresses = Address.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Setup session
        self.setup_session()

        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_customer', kwargs={'booking_checkout_url': self.booking_checkout_url.key}))
        self.assertEqual(Address.objects.all().count(), num_addresses + 1)
        booking_address = Address.objects.all()[0]
        for k in self.data.keys():
            self.assertEqual(self.data.get(k), getattr(booking_address, k))

    def test_blank_data(self):
        num_addresses = Address.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(Address.objects.all().count(), num_addresses)
        for k in self.data.keys():
            if k in ['postal_code', 'sub_premise', 'region']:
                continue
            self.assertFormError(response, 'form', k, 'This field is required.')


    # def test_invalid_checkout_url(self):
    #     url = reverse('photographers:create_booking_address', kwargs={'booking_checkout_url': 'ABCD'})
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_mismatched_checkout_url(self):
    #     other_user = User.objects.exclude(pk=self.user.pk)[0]
    #     booking_checkout_url = BookingCheckoutURL.objects.create(**{
    #         'customer': other_user.profile,
    #         'line_item': 'Photography Services',
    #         'price': 9000,
    #     })
    #     url = reverse('photographers:create_booking_address', kwargs={'booking_checkout_url': booking_checkout_url.key})
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_no_authentication(self):
    #     url = self.get_url()
    #     response = self.app.get(url, expect_errors=True)
    #     self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    # def test_access_page_without_query_params(self):
    #     url = reverse('photographers:create_booking_address')
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_access_page_with_some_query_params(self):
    #     for k in self.query_params:
    #         query_params = self.query_params.copy()
    #         query_params.pop(k)
    #         url = reverse_with_query('photographers:create_booking_address', query_params)
    #         response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #         self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_access_page_with_invalid_date(self):
    #     query_params = self.query_params.copy()
    #     query_params['date'] = 'fdasfdafdasfsafadsfdasfdsfsafsdfsa'
    #     url = reverse_with_query('photographers:create_booking_address', query_params)
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)
    #
    # def test_access_page_with_past_date(self):
    #     query_params = self.query_params.copy()
    #     query_params['date'] = date.today() - timedelta(days=10)
    #     url = reverse_with_query('photographers:create_booking_address', query_params)
    #     response = self.app.get(url, user=self.user.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

class FoodBookingDetailsFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingDetailsFormViewTestCase, cls).setUpClass()
        create_profiles(2)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingDetailsFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(FoodBookingDetailsFormViewTestCase, self).setUp()
        self.user = User.objects.all()[0]
        self.data = {
            'display_name': 'DANGERZONE!!!',
            'service_type': ServiceType.FOOD,
            'date': date.today(),
            'start_time': '3:00 AM',
            'end_time': '4:00AM',
            'contact_name': 'Barry Dylan',
            'contact_phone_number': '5555555555',
            'is_full_menu_shoot': True,
            'is_confirmed_with_restaurant': True,
            'items': 'Crispy Duck, Pad Thai, Pad See Ewe'
        }

    def tearDown(self):
        super(FoodBookingDetailsFormViewTestCase, self).tearDown()
        self.app.session.clear()

    def get_view_name(self):
        return 'photographers:get_food_booking_details'

    def get_url(self):
        return reverse(self.get_view_name())

    def test_no_authentication(self):
        url = reverse('profiles:logout')
        self.app.get(url)
        url = self.get_url()
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/get_food_booking_details.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_get_food_booking_details(self):
        self.get_food_booking_details()

    def test_blank_data(self):
        num_addresses = Address.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(Address.objects.all().count(), num_addresses)
        for k in self.data.keys():
            if k in ['service_type', 'items', 'contact_name', 'time_period', 'is_full_menu_shoot', 'is_confirmed_with_restaurant']:
                continue
            self.assertFormError(response, 'form', k, 'This field is required.')

    def test_session_keys(self):
        self.get_food_booking_details()
        self.assertIn(BookingSessionKeys.display_name, self.app.session)
        self.assertIn(BookingSessionKeys.service_type, self.app.session)
        self.assertIn(BookingSessionKeys.date, self.app.session)
        self.assertIn(BookingSessionKeys.start_time, self.app.session)
        self.assertIn(BookingSessionKeys.end_time, self.app.session)
        self.assertIn(BookingSessionKeys.phone_number, self.app.session)
        self.assertIn(FoodBookingSessionKeys.contact_name, self.app.session)
        self.assertIn(FoodBookingSessionKeys.contact_phone_number, self.app.session)
        self.assertIn(FoodBookingSessionKeys.is_full_menu_shoot, self.app.session)
        self.assertIn(FoodBookingSessionKeys.is_confirmed_with_restaurant, self.app.session)
        self.assertIn(FoodBookingSessionKeys.items, self.app.session)

    def get_food_booking_details(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_food_booking_address'))


class FoodBookingAddressCreateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingAddressCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=False)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingAddressCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(FoodBookingAddressCreateViewTestCase, self).setUp()
        Address.objects.all().delete()
        self.user = User.objects.all()[0]
        self.data = {
            'restaurant_name': 'My Thai',
            'street_address': '5719 Old Buggy Court',
            'postal_code': '21045',
            'locality': 'Columbia',
            'region': 'MD',
        }
        self.details = {
            'display_name': 'DANGERZONE!!!',
            'service_type': ServiceType.FOOD,
            'date': date.today(),
            'start_time': '3:00 AM',
            'end_time': '4:00AM',
            'phone_number': '5555555555',
            'contact_name': 'Barry Dylan',
            'contact_phone_number': '5555555555',
            'is_full_menu_shoot': True,
            'is_confirmed_with_restaurant': True,
            'items': 'Crispy Duck, Pad Thai, Moo Krab'
        }

        # Setup session
        self.app.get(reverse('index'), user=self.user.email_address)
        if 'django.contrib.sessions' in settings.INSTALLED_APPS:
            engine = import_module(settings.SESSION_ENGINE)
            cookie = self.app.cookies.get(settings.SESSION_COOKIE_NAME, None)
            if cookie:
                session = engine.SessionStore(cookie)
                session[BookingSessionKeys.service_type] = ServiceType.FOOD
                session.save()

    def tearDown(self):
        super(FoodBookingAddressCreateViewTestCase, self).tearDown()
        Address.objects.all().delete()
        self.app.session.clear()

    def get_url(self):
        return reverse(self.get_view_name())
        # return reverse_with_query('photographers:create_booking_address', self.query_params)

    def get_view_name(self):
        return 'photographers:create_food_booking_address'

    def test_no_authentication(self):
        url = reverse('profiles:logout')
        self.app.get(url)
        url = self.get_url()
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/create_food_booking_address.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_create_food_booking_address(self):
        num_addresses = Address.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_food_customer'))
        self.assertEqual(Address.objects.all().count(), num_addresses + 1)
        address = Address.objects.all()[0]
        for k in self.data.keys():
            if k in ['restaurant_name']:
                continue
            self.assertEqual(self.data.get(k), getattr(address, k))

        # Session keys
        self.assertIn(FoodBookingSessionKeys.restaurant_name, self.app.session)

    def test_blank_data(self):
        num_addresses = Address.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(Address.objects.all().count(), num_addresses)
        for k in self.data.keys():
            if k in ['postal_code', 'sub_premise', 'region']:
                continue
            self.assertFormError(response, 'form', k, 'This field is required.')


class FoodBookingCreateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(FoodBookingCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_profiles(10)

    @classmethod
    def tearDownClass(cls):
        super(FoodBookingCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        photographers = Photographer.objects.all()
        self.photographer = photographers[0]
        self.client = photographers[1].profile
        self.profile = self.client
        self.user = self.profile.user
        self.data = {
            'message': 'Hello World!',
        }
        self.details = {
            'display_name': 'DANGERZONE!!!',
            'service_type': ServiceType.FOOD,
            'date': date.today(),
            'start_time': '3:00 AM',
            'end_time': '4:00AM',
            'phone_number': '5555555555',
            'contact_name': 'Barry Dylan',
            'contact_phone_number': '5555555555',
            'is_full_menu_shoot': True,
            'is_confirmed_with_restaurant': True,
            'items': 'Crispy Duck, Pad Thai, Pad See Ewe',
            'restaurant_name': 'My Thai'
        }
        self.address = Address.objects.create(**{
            'full_name': self.client.full_name,
            'street_address': '27 Treasure Cove Drive',
            'postal_code': '77381',
            'locality': 'Spring',
            'region': 'TX',
            'country': 'US'
        })
        self.credit_card = {
            'last_4': '1234',
            'card_type': 'VISA',
            'expiration_month': '08',
            'expiration_year': '2020'
        }

        # Setup session
        self.app.get(reverse('index'), user=self.user.email_address)
        if 'django.contrib.sessions' in settings.INSTALLED_APPS:
            engine = import_module(settings.SESSION_ENGINE)
            cookie = self.app.cookies.get(settings.SESSION_COOKIE_NAME, None)
            if cookie:
                session = engine.SessionStore(cookie)
                session[BookingSessionKeys.display_name] = self.details.get('display_name')
                session[BookingSessionKeys.service_type] = self.details.get('service_type')
                session[BookingSessionKeys.date] = self.details.get('date').isoformat()
                session[BookingSessionKeys.start_time] = self.details.get('start_time')
                session[BookingSessionKeys.end_time] = self.details.get('end_time')
                session[BookingSessionKeys.phone_number] = self.details.get('phone_number')
                session[BookingSessionKeys.address_id] = self.address.id
                session[BookingSessionKeys.credit_card] = self.credit_card

                session[FoodBookingSessionKeys.restaurant_name] = self.details.get('restaurant_name')
                session[FoodBookingSessionKeys.contact_name] = self.details.get('contact_name')
                session[FoodBookingSessionKeys.contact_phone_number] = self.details.get('contact_phone_number')
                session[FoodBookingSessionKeys.is_full_menu_shoot] = self.details.get('is_full_menu_shoot')
                session[FoodBookingSessionKeys.is_confirmed_with_restaurant] = self.details.get('is_confirmed_with_restaurant')
                session[FoodBookingSessionKeys.items] = self.details.get('items')
                session.save()

    def tearDown(self):
        Address.objects.all().delete()
        Booking.objects.all().delete()
        FoodBooking.objects.all().delete()

    def create_booking_without_session_data(self, session_key):
        session = self.app.session
        session.pop(session_key, None)
        session.save()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/create_food_booking.html')

    def test_no_authentication(self):
        url = reverse('profiles:logout')
        self.app.get(url)
        url = self.get_url()
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertIsNotNone(response.form)

    def test_create_booking(self):
        self.create_booking()

    def create_booking(self):
        self.assertEqual(Booking.objects.all().count(), 0)
        self.assertEqual(FoodBooking.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.profile.email_address)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('index'))
        self.assertEqual(FoodBooking.objects.all().count(), 1)
        self.assertEqual(Booking.objects.all().count(), 1)

    # def test_create_booking_without_payment_details(self):
    #     profile = Profile.objects.filter(user__braintree_customer=None)[0]
    #     self.get_booking_details(profile.user)
    #     self.create_booking_address(profile.user)
    #     self.assertEqual(Booking.objects.all().count(), 0)
    #     url = self.get_url()
    #     response = self.app.get(url, user=profile.email_address, expect_errors=True)
    #     self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_create_booking_without_booking_address(self):
        self.create_booking_without_session_data(BookingSessionKeys.address_id)

    def test_create_booking_without_service_type(self):
        self.create_booking_without_session_data(BookingSessionKeys.service_type)

    def test_create_booking_without_start_time(self):
        self.create_booking_without_session_data(BookingSessionKeys.start_time)

    def test_create_booking_without_end_time(self):
        self.create_booking_without_session_data(BookingSessionKeys.end_time)

    def test_create_booking_without_date(self):
        self.create_booking_without_session_data(BookingSessionKeys.date)

    def test_create_booking_without_credit_card(self):
        self.create_booking_without_session_data(BookingSessionKeys.credit_card)

    def test_create_booking_without_contact_name(self):
        self.create_booking_without_session_data(FoodBookingSessionKeys.contact_name)

    def test_create_booking_without_contact_phone_number(self):
        self.create_booking_without_session_data(FoodBookingSessionKeys.contact_phone_number)

    def test_create_booking_without_is_full_menu_shoot(self):
        self.create_booking_without_session_data(FoodBookingSessionKeys.is_full_menu_shoot)

    def test_create_booking_without_is_confirmed_with_restaurant(self):
        self.create_booking_without_session_data(FoodBookingSessionKeys.is_confirmed_with_restaurant)

    def test_create_booking_without_items(self):
        self.create_booking_without_session_data(FoodBookingSessionKeys.items)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:create_food_booking'

    def get_kwargs(self):
        return None


class BulkBookingFileFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BulkBookingFileFormViewTestCase, cls).setUpClass()
        create_profiles(2)

    @classmethod
    def tearDownClass(cls):
        super(BulkBookingFileFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BulkBookingFileFormViewTestCase, self).setUp()
        self.user = User.objects.all()[0]
        self.data = {
            'file': Upload(self.get_file_path())
        }
        # self.data = {
        #     'display_name': 'DANGERZONE!!!',
        #     'service_type': ServiceType.FOOD,
        #     'date': date.today(),
        #     'start_time': '3:00 AM',
        #     'end_time': '4:00AM',
        #     'contact_name': 'Barry Dylan',
        #     'contact_phone_number': '5555555555',
        #     'is_full_menu_shoot': True,
        #     'is_confirmed_with_restaurant': True,
        #     'items': 'Crispy Duck, Pad Thai, Pad See Ewe'
        # }

    def tearDown(self):
        super(BulkBookingFileFormViewTestCase, self).tearDown()
        BulkBooking.objects.all().delete()
        self.app.session.clear()

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/get_bulk_booking_file.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_get_bulk_booking_file(self):
        self.assertEqual(BulkBooking.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_bulk_customer'))
        self.assertEqual(BulkBooking.objects.all().count(), 1)

        # Session keys
        self.assertIn(BulkBookingSessionKeys.bulk_booking_id, self.app.session)

    def test_blank_data(self):
        self.assertEqual(BulkBooking.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        self.assertEqual(BulkBooking.objects.all().count(), 0)
        for k in self.data.keys():
            self.assertFormError(response, 'form', 'file', 'This field is required.')

    def test_no_authentication(self):
        url = reverse('profiles:logout')
        self.app.get(url)
        url = self.get_url()
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:get_bulk_booking_file'

    def get_kwargs(self):
        return None

    def get_file_path(self):
        booking_files_dir = os.path.join(FIXTURES_DIR, 'booking_files/')
        return os.path.join(booking_files_dir, 'postmates.csv')


class BulkCustomerCreateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BulkCustomerCreateViewTestCase, cls).setUpClass()
        create_profiles(2)
        file_path = get_booking_file_path()
        BulkBooking.objects.create(**{
            'file': ContentFile(open(file_path).read(), name=os.path.basename(file_path))
        })

    @classmethod
    def tearDownClass(cls):
        super(BulkCustomerCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BulkCustomerCreateViewTestCase, self).setUp()
        self.user = User.objects.all()[0]
        self.data = {
            'payment_method_nonce': Nonces.Transactable,
        }

        # Setup session
        self.app.get(reverse('index'), user=self.user.email_address)
        if 'django.contrib.sessions' in settings.INSTALLED_APPS:
            engine = import_module(settings.SESSION_ENGINE)
            cookie = self.app.cookies.get(settings.SESSION_COOKIE_NAME, None)
            if cookie:
                session = engine.SessionStore(cookie)
                session[BulkBookingSessionKeys.bulk_booking_id] = BulkBooking.objects.all()[0].id
                session.save()

    def tearDown(self):
        super(BulkCustomerCreateViewTestCase, self).tearDown()
        self.app.session.clear()

    def test_create_bulk_customer(self):
        num_customers = BraintreeCustomer.objects.all().count()
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('photographers:create_bulk_booking', kwargs=self.get_kwargs()))
        self.assertEqual(BraintreeCustomer.objects.all().count(), num_customers + 1)
        braintree_customer = BraintreeCustomer.objects.all()[0]
        customer = braintree.Customer.find(braintree_customer.braintree_id)
        self.assertIsNotNone(customer)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:create_bulk_customer'

    def get_kwargs(self):
        return None


class BulkBookingCreateViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BulkBookingCreateViewTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(BulkBookingCreateViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BulkBookingCreateViewTestCase, self).setUp()
        self.user = Photographer.objects.all()[0].profile.user
        self.data = {
            'message': 'Hello World!',
        }
        file_path = get_booking_file_path()
        bulk_booking = BulkBooking.objects.create(**{
            'file': ContentFile(open(file_path).read(), name=os.path.basename(file_path))
        })
        self.credit_card = {
            'last_4': '1234',
            'card_type': 'VISA',
            'expiration_month': '08',
            'expiration_year': '2020'
        }

        # Setup session
        self.app.get(reverse('index'), user=self.user.email_address)
        if 'django.contrib.sessions' in settings.INSTALLED_APPS:
            engine = import_module(settings.SESSION_ENGINE)
            cookie = self.app.cookies.get(settings.SESSION_COOKIE_NAME, None)
            if cookie:
                session = engine.SessionStore(cookie)
                session[BulkBookingSessionKeys.bulk_booking_id] = bulk_booking.id
                session[BookingSessionKeys.credit_card] = self.credit_card
                session.save()

    def tearDown(self):
        super(BulkBookingCreateViewTestCase, self).tearDown()
        self.app.session.clear()
        Booking.objects.all().delete()
        BulkBooking.objects.all().delete()

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/checkout/create_bulk_booking.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_create_bulk_booking(self):
        self.assertEqual(BulkBooking.objects.all().count(), 1)
        self.assertEqual(Booking.objects.all().count(), 0)
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        for k in self.data.keys():
            form[k] = self.data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('index', kwargs=self.get_kwargs()))
        self.assertEqual(BulkBooking.objects.all().count(), 1)
        self.assertTrue(BulkBooking.objects.filter(client=self.user.profile).exists())
        self.assertTrue(BulkBooking.objects.filter(message=self.data.get('message')).exists())
        self.assertGreater(Booking.objects.all().count(), 0)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:create_bulk_booking'

    def get_kwargs(self):
        return None


class BookingRequestDetailViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(BookingRequestDetailViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(1, do_send_client_confirmation=False, do_assign_photographers=False)

        for photographer in Photographer.objects.all():
            photographer.address.region = 'MD'
            photographer.address.save()
        photographer = Photographer.objects.all()[0]
        photographer.address.region = 'TX'
        photographer.address.save()
        photographer.save()

        for booking in Booking.objects.all():
            booking.address.region = 'TX'
            booking.address.save()

        BookingRequest.objects.create(**{
            'photographer': Photographer.objects.all()[0],
            'booking': Booking.objects.all()[0],
        })

    @classmethod
    def tearDownClass(cls):
        super(BookingRequestDetailViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(BookingRequestDetailViewTestCase, self).setUp()
        self.booking_request = BookingRequest.objects.all()[0]
        self.user = self.booking_request.photographer.profile.user

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/booking_request_detail.html')

    def test_invalid_access(self):
        user = None
        for photographer in Photographer.objects.all():
            if photographer != self.booking_request.photographer:
                user = photographer.profile.user
                break
        url = self.get_url()
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:booking_request_detail'

    def get_kwargs(self):
        return {
            'pk': self.booking_request.id
        }


class AcceptBookingRequestViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(AcceptBookingRequestViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(1, do_send_client_confirmation=False, do_assign_photographers=False)

        for photographer in Photographer.objects.all():
            photographer.address.region = 'MD'
            photographer.address.save()
        photographer = Photographer.objects.all()[0]
        photographer.address.region = 'TX'
        photographer.address.save()
        photographer.save()

        for booking in Booking.objects.all():
            booking.address.region = 'TX'
            booking.address.save()


    @classmethod
    def tearDownClass(cls):
        super(AcceptBookingRequestViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(AcceptBookingRequestViewTestCase, self).setUp()
        self.booking_request = BookingRequest.objects.create(**{
            'photographer': Photographer.objects.all()[0],
            'booking': Booking.objects.all()[0],
        })
        self.user = self.booking_request.photographer.profile.user
        for booking in Booking.objects.all():
            booking.photographer = None
            booking.save()

    def tearDown(self):
        BookingRequest.objects.all().delete()

    def test_accept_booking(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertRedirects(response, reverse('profiles:shoot_detail', kwargs={'pk': self.booking_request.booking.id}))
        self.booking_request = BookingRequest.objects.get(id=self.booking_request.id)
        self.assertTrue(self.booking_request.did_accept_booking)
        self.assertFalse(self.booking_request.did_reject_booking)
        self.assertEqual(self.booking_request.booking.photographer, self.booking_request.photographer)

    def test_accept_booking_twice(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        response = self.app.get(url, user=self.user.email_address, expect_errors=True)
        # print(response.follow())
        self.assertRedirects(response, reverse('profiles:shoot_detail', kwargs={'pk': self.booking_request.booking.id}))

    def test_fake_token(self):
        url = reverse('photographers:accept_booking_request', kwargs={'pk': self.booking_request.id, 'token': 'invalid'})
        response = self.app.get(url, user=self.user.email_address if self.user else None, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_mismatched_token(self):
        payload = {
            'pk': self.booking_request.pk + 1
        }
        url = reverse('photographers:accept_booking_request', kwargs={
            'pk': self.booking_request.id,
            'token': signing.dumps(payload, salt=salt.BOOKING_REQUEST)
        })
        response = self.app.get(url, user=self.user.email_address if self.user else None, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_invalid_access(self):
        user = None
        for photographer in Photographer.objects.all():
            if photographer != self.booking_request.photographer:
                user = photographer.profile.user
                break
        url = self.get_url()
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        url = self.get_url()
        response = self.app.get(url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + urlquote(url))

    def get_url(self):
        return self.booking_request.accept_booking_url


class RejectBookingRequestViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(RejectBookingRequestViewTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)
        create_bookings(1, do_send_client_confirmation=False, do_assign_photographers=False)

        for photographer in Photographer.objects.all():
            photographer.address.region = 'MD'
            photographer.address.save()
        photographer = Photographer.objects.all()[0]
        photographer.address.region = 'TX'
        photographer.address.save()
        photographer.save()

        for booking in Booking.objects.all():
            booking.address.region = 'TX'
            booking.address.save()


    @classmethod
    def tearDownClass(cls):
        super(RejectBookingRequestViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        super(RejectBookingRequestViewTestCase, self).setUp()
        self.booking_request = BookingRequest.objects.create(**{
            'photographer': Photographer.objects.all()[0],
            'booking': Booking.objects.all()[0],
        })
        self.user = self.booking_request.photographer.profile.user

    def tearDown(self):
        BookingRequest.objects.all().delete()

    def test_reject_booking(self):
        url = self.url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertRedirects(response, reverse('profiles:shoot_list'))
        self.booking_request = BookingRequest.objects.get(id=self.booking_request.id)
        self.assertFalse(self.booking_request.did_accept_booking)
        self.assertTrue(self.booking_request.did_reject_booking)
        self.assertIsNone(self.booking_request.booking.photographer)

    def test_reject_booking_twice(self):
        url = self.url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        response = self.app.get(url, user=self.user.email_address if self.user else None, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_fake_token(self):
        url = reverse(self.view_name, kwargs={'pk': self.booking_request.id, 'token': 'invalid'})
        response = self.app.get(url, user=self.user.email_address if self.user else None, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_mismatched_token(self):
        payload = {
            'pk': self.booking_request.pk + 1
        }
        url = reverse(self.view_name, kwargs={
            'pk': self.booking_request.id,
            'token': signing.dumps(payload, salt=salt.BOOKING_REQUEST)
        })
        response = self.app.get(url, user=self.user.email_address if self.user else None, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN)

    def test_invalid_access(self):
        user = None
        for photographer in Photographer.objects.all():
            if photographer != self.booking_request.photographer:
                user = photographer.profile.user
                break
        url = self.url()
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        url = self.url()
        response = self.app.get(url, user=self.user.email_address if self.user else None)
        self.assertEqual(response.status_code, status.HTTP_302_FOUND)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        url = self.url()
        response = self.app.get(url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + urlquote(url))

    def url(self):
        return self.booking_request.reject_booking_url

    @property
    def view_name(self):
        return 'photographers:reject_booking_request'


class ClientRejectionFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ClientRejectionFormViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ClientRejectionFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_sale=False)[0]
        self.booking.did_staff_approve = True
        self.booking.num_shots = 25
        self.booking.save()
        self.profile = self.booking.client

        self.data = {
            'did_client_reject': True
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/client_rejection.html')

    def test_invalid_access(self):
        user = Profile.objects.exclude(id=self.profile.id)[0].user
        response = self.app.get(self.url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        response = self.app.get(self.url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + self.url)

    def test_form_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertIsNotNone(response.form)

    def test_client_rejection(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:booking_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_client_approve=False, did_client_reject=True).exists())

    def test_invalid_access_after_rejection(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:booking_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_client_approve=False, did_client_reject=True).exists())

        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_access_without_staff_approval(self):
        self.booking.did_staff_approve = False
        self.booking.num_shots = None
        self.booking.save()
        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @property
    def url(self):
        return reverse(self.view_name, kwargs=self.kwargs)

    @property
    def view_name(self):
        return 'photographers:client_rejection'

    @property
    def kwargs(self):
        return {
            'pk': self.booking.id
        }


class ClientApprovalFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ClientApprovalFormViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ClientApprovalFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_sale=False)[0]
        self.booking.did_staff_approve = True
        self.booking.num_shots = 25
        self.booking.save()
        self.profile = self.booking.client

        self.data = {
            'did_client_approve': True
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/client_approval.html')

    def test_invalid_access(self):
        user = Profile.objects.exclude(id=self.profile.id)[0].user
        response = self.app.get(self.url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        response = self.app.get(self.url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + self.url)

    def test_form_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertIsNotNone(response.form)

    def test_client_approval(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:booking_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_client_approve=True).exists())

    def test_invalid_access_after_approval(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:booking_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_client_approve=True).exists())

        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_access_without_staff_approval(self):
        self.booking.did_staff_approve = False
        self.booking.num_shots = None
        self.booking.save()
        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @property
    def url(self):
        return reverse(self.view_name, kwargs=self.kwargs)

    @property
    def view_name(self):
        return 'photographers:client_approval'

    @property
    def kwargs(self):
        return {
            'pk': self.booking.id
        }


class StaffApprovalFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(StaffApprovalFormViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(StaffApprovalFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_sale=False)[0]
        self.booking.did_photographer_approve = True
        self.booking.num_shots = 25
        self.booking.save()
        self.profile = [p for p in Profile.objects.all() if p != self.booking.client][0]
        self.profile.user.is_staff = True
        self.profile.user.save()

        self.data = {
            'did_staff_approve': True
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/staff_approval.html')

    def test_never_cache_headers(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_staff_login_required(self):
        self.profile.user.is_staff = False
        self.profile.user.save()
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + self.url)

    def test_form_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertIsNotNone(response.form)

    def test_staff_approval(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:booking_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_staff_approve=True).exists())

    def test_invalid_access_after_approval(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:booking_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_staff_approve=True).exists())

        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_access_without_photographer_approval(self):
        self.booking.did_photographer_approve = False
        self.booking.num_shots = None
        self.booking.save()
        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @property
    def url(self):
        return reverse(self.view_name, kwargs=self.kwargs)

    @property
    def view_name(self):
        return 'photographers:staff_approval'

    @property
    def kwargs(self):
        return {
            'pk': self.booking.id
        }


class PhotographerApprovalFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(PhotographerApprovalFormViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(PhotographerApprovalFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1)[0]
        self.profile = self.booking.photographer.profile

        self.data = {
            'did_photographer_approve': True,
            'num_shots': 150
        }

        self.assertFalse(self.booking.did_photographer_approve)
        self.assertFalse(self.booking.did_client_approve)

    def tearDown(self):
        Booking.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/photographer_approval.html')

    def test_invalid_access(self):
        user = Profile.objects.exclude(id=self.profile.id)[0].user
        response = self.app.get(self.url, user=user.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_never_cache_headers(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_login_required(self):
        response = self.app.get(self.url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + self.url)

    def test_form_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertIsNotNone(response.form)

    def test_photographer_approval(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_photographer_approve=True, num_shots=self.data.get('num_shots')).exists())

    def test_invalid_access_after_approval(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('profiles:shoot_detail', kwargs={'pk': self.booking.id}))
        self.assertTrue(Booking.objects.filter(id=self.booking.id, did_photographer_approve=True, num_shots=self.data.get('num_shots')).exists())

        response = self.app.get(self.url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @property
    def url(self):
        return reverse(self.view_name, kwargs=self.kwargs)

    @property
    def view_name(self):
        return 'photographers:photographer_approval'

    @property
    def kwargs(self):
        return {
            'pk': self.booking.id
        }


class ScheduleBookingFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(ScheduleBookingFormViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ScheduleBookingFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1)[0]
        self.booking.time_range = None
        self.booking.save()

        time_zone = 'US/Central'
        self.time_range = DateTimeTZRange(
            lower=pytz.timezone(time_zone).localize(datetime.today().replace(hour=8, microsecond=0) + timedelta(seconds=86400 * 10)),
            upper=pytz.timezone(time_zone).localize(datetime.today().replace(hour=11, microsecond=0) + timedelta(seconds=86400 * 10 + 300)),
        )

        self.data = {
            'time_zone': time_zone,
            'date': date(self.time_range.lower.year, self.time_range.lower.month, self.time_range.lower.day),
            'start_time': time(
                self.time_range.lower.hour,
                self.time_range.lower.minute,
                self.time_range.lower.second,
                self.time_range.lower.microsecond
            ),
            'stop_time': time(
                self.time_range.upper.hour,
                self.time_range.upper.minute,
                self.time_range.upper.second,
                self.time_range.upper.microsecond
            ),
            'latitude': 41.9028,
            'longitude': 12.4964
        }

    def tearDown(self):
        Booking.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/schedule_booking.html')

    def test_never_cache_headers(self):
        response = self.app.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_form_exists(self):
        response = self.app.get(self.url)
        self.assertIsNotNone(response.form)

    def test_schedule_booking(self):
        response = self.app.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('index'))

        # Check data
        booking = Booking.objects.get(id=self.booking.id)
        self.assertIsNotNone(booking.time_range)
        self.assertEqual(booking.time_range.lower, self.time_range.lower)
        self.assertEqual(booking.time_range.upper, self.time_range.upper)
        self.assertIsNotNone(booking.address.point)
        self.assertEqual(booking.address.point.y, self.data.get('latitude'))
        self.assertEqual(booking.address.point.x, self.data.get('longitude'))
        self.assertIsNotNone(booking.address.time_zone)
        self.assertEqual(booking.address.time_zone.zone, self.data.get('time_zone'))

    def test_invalid_token(self):
        kwargs = self.kwargs.copy()
        kwargs['token'] = '209'
        url = reverse(self.view_name, kwargs=kwargs)
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_invalid_booking(self):
        kwargs = self.kwargs.copy()
        kwargs['pk'] = 99999
        url = reverse(self.view_name, kwargs=kwargs)
        response = self.app.get(url, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    @property
    def url(self):
        return reverse(self.view_name, kwargs=self.kwargs)

    @property
    def view_name(self):
        return 'photographers:schedule_booking'

    @property
    def kwargs(self):
        return {
            'pk': self.booking.id,
            'token': self.booking.token
        }


class PhotographerVerificationFormViewTestCase(WebTest):
    def setUp(self):
        super(PhotographerVerificationFormViewTestCase, self).setUp()
        create_profiles(1)
        create_customers()
        self.braintree_merchant_account = BraintreeMerchantAccount.objects.all()[0]
        self.user = self.braintree_merchant_account.user
        self.data = {
            'government_issued_id': Upload(os.path.join(FIXTURES_DIR, 'government_issued_ids/id.png'))
        }

    def tearDown(self):
        super(PhotographerVerificationFormViewTestCase, self).tearDown()
        tear_down_customers()
        tear_down_photographers()

    def test_page_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/braintree_merchant_account_verification.html')

    def test_form_exists(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(len(response.forms), 1)

    def test_blank_data(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        form = response.form
        response = form.submit()
        for k in self.data.keys():
            self.assertFormError(response, 'form', k, 'This field is required.')

    def test_no_authentication(self):
        url = reverse('profiles:logout')
        self.app.get(url)
        url = self.get_url()
        response = self.app.get(url, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_no_braintree_merchant_account(self):
        tear_down_customers()
        tear_down_photographers()
        create_profiles(1)
        user = User.objects.all()[0]
        url = self.get_url()
        response = self.app.get(url, user=user.email_address, expect_errors=True)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + url)

    def test_form_valid(self):
        url = self.get_url()
        response = self.app.get(url, user=self.user.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Submit form
        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('index'))

        # Check field
        braintree_merchant_account = BraintreeMerchantAccount.objects.get(id=self.braintree_merchant_account.id)
        self.assertIsNotNone(braintree_merchant_account.government_issued_id)

    def get_url(self):
        return reverse(self.get_view_name(), kwargs=self.get_kwargs())

    def get_view_name(self):
        return 'photographers:braintree_merchant_account_verification'

    def get_kwargs(self):
        return None


class CreateFoodBookingFormViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(CreateFoodBookingFormViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

        # Geocoding...lolz...
        for photographer in Photographer.objects.all():
            photographer.address.point = 'POINT(%f %f)' % (-118.593979, 33.801118)
            photographer.address.save()

    @classmethod
    def tearDownClass(cls):
        super(CreateFoodBookingFormViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.client = Profile.objects.all()[0]
        time_zone = 'US/Pacific'
        time_range = DateTimeTZRange(
            lower=pytz.timezone(time_zone).localize(datetime.today().replace(hour=9, microsecond=0)),
            upper=pytz.timezone(time_zone).localize(datetime.today().replace(hour=12, microsecond=0)),
        )
        self.data = {
            'restaurant_name': 'Sizzler\'s',
            'contact_email_address': 'success+sterlingarcher@simulator.amazonses.com',
            'contact_phone_number': '5555555',
            'contact_name': 'Sterling Archer',

            'shot_list': Upload(os.path.join(FIXTURES_DIR, 'shot_list.csv')),

            'street_address': '380 S. Lake Ave',
            'sub_premise': 'Suite 106',
            'postal_code': '91101',
            'locality': 'Pasadena',
            'region': 'CA',

            'date': date(time_range.lower.year, time_range.lower.month, time_range.lower.day).isoformat(),
            'start_time': time(
                time_range.lower.hour,
                time_range.lower.minute,
                time_range.lower.second,
                time_range.lower.microsecond
            ).isoformat(),
            'stop_time': time(
                time_range.upper.hour,
                time_range.upper.minute,
                time_range.upper.second,
                time_range.upper.microsecond
            ).isoformat(),
        }

    def tearDown(self):
        Booking.objects.all().delete()
        BookingRequest.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url, user=self.client.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTemplateUsed(response, 'photographers/create_food_booking.html')

    def test_never_cache_headers(self):
        response = self.app.get(self.url, user=self.client.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Check caching
        self.assertIn('Last-Modified', response.headers)
        self.assertIn('Expires', response.headers)
        self.assertIn('Cache-Control', response.headers)
        self.assertIn('max-age=0', response.headers['Cache-Control'])

    def test_form_exists(self):
        response = self.app.get(self.url, user=self.client.email_address)
        self.assertIsNotNone(response.form)

    def test_create_food_booking(self):
        response = self.app.get(self.url, user=self.client.email_address)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        form = response.form
        data = self.data.copy()
        for k in data.keys():
            form[k] = data.get(k)
        response = form.submit()
        self.assertRedirects(response, reverse('index'))

        self.assertEqual(FoodBooking.objects.all().count(), 1)
        food_booking = FoodBooking.objects.all()[0]
        self.assertEqual(food_booking.client, self.client)
        self.assertGreater(BookingRequest.objects.all().count(), 0)

    def test_login_required(self):
        response = self.app.get(self.url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + self.url)

    @property
    def url(self):
        return reverse(self.view_name)

    @property
    def view_name(self):
        return 'create_food_booking'


class PhotographerCompletionViewTestCase(WebTest):
    @classmethod
    def setUpClass(cls):
        super(PhotographerCompletionViewTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(PhotographerCompletionViewTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1)[0]
        self.profile = self.booking.photographer.profile
        self.assertFalse(self.booking.did_photographer_complete)

    def tearDown(self):
        Booking.objects.all().delete()

    def test_page_exists(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertRedirects(response, reverse('index'))

    def test_login_required(self):
        url = self.url
        response = self.app.get(url)
        self.assertRedirects(response, reverse('profiles:login') + '?next=' + urlquote(url))

    def test_photographer_completion(self):
        response = self.app.get(self.url, user=self.profile.email_address)
        self.assertRedirects(response, reverse('index'))
        booking = Booking.objects.get(id=self.booking.id)
        self.assertTrue(booking.did_photographer_complete)
        self.assertIsNotNone(booking.photographer_completion_date)

    def test_invalid_token(self):
        url = reverse(self.view_name, kwargs={
            'token': 'fakeasstoken'
        })
        response = self.app.get(url, user=self.profile.email_address, expect_errors=True)
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    @property
    def url(self):
        return reverse(self.view_name, kwargs={
            'token': signing.dumps({
                'booking': self.booking.id
            })
        })

    @property
    def view_name(self):
        return 'photographers:photographer_completion'
