from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import random
from datetime import timedelta
from decimal import *

import os
from django.conf import settings
from django.core import mail
from django.test import TestCase, override_settings
from django.utils import timezone
from freezegun import freeze_time
from gouda.customers.models import BraintreeTransactionStatusType
from gouda.customers.utils import braintree
from gouda.profiles.models import Profile
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingAlbum
from gouda.photographers.models import BookingImage
from gouda.photographers.models import BookingRequest
from gouda.photographers.tasks import process_client_approved_bookings
from gouda.photographers.tasks import process_photographer_approved_bookings
from gouda.photographers.tasks import smart_crop_booking_image
from gouda.photographers.tasks import send_postmates_notification
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_food_bookings
from gouda.photographers.utils import create_photographers
from gouda.photographers.utils import tear_down_photographers
from gouda.photographers.tasks import send_photographer_completion
import unittest
from psycopg2.extras import DateTimeTZRange


DEFAULT_FROM_EMAIL = getattr(settings, 'DEFAULT_FROM_EMAIL')
SERVER_EMAIL = getattr(settings, 'SERVER_EMAIL')
AMOUNT_WITHHELD_PERCENTAGE = getattr(settings, 'AMOUNT_WITHHELD_PERCENTAGE')
STAFF_EMAILS = getattr(settings, 'STAFF_EMAILS')
FIXTURES_DIR = os.path.join(os.path.dirname(os.path.dirname(__file__)), 'fixtures')
BRAINTREE_MERCHANT_ACCOUNT_ID = getattr(settings, 'BRAINTREE_MERCHANT_ACCOUNT_ID')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
PAY_SCALE = getattr(settings, 'PAY_SCALE')
POSTMATES_BOOKING_CHARGE_DELAY = getattr(settings, 'POSTMATES_BOOKING_CHARGE_DELAY')
CLIENT_APPROVAL_TIMEOUT = getattr(settings, 'CLIENT_APPROVAL_TIMEOUT')


@unittest.skip('No longer used')
class ProcessPhotographerApprovedBookingsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ProcessPhotographerApprovedBookingsTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ProcessPhotographerApprovedBookingsTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        create_bookings(3, do_send_client_confirmation=False, do_sale=True)
        self.bookings = create_bookings(3, do_send_client_confirmation=False, do_sale=False)
        for booking in Booking.objects.all():
            booking.num_shots = random.randint(20, 75)
            booking.on_photographer_approval()
        mail.outbox = list()

    def tearDown(self):
        BookingRequest.objects.all().delete()
        Booking.objects.all().delete()

    def test_process_photographer_approved_bookings(self):
        with freeze_time(timezone.now() + CLIENT_APPROVAL_TIMEOUT):
            process_photographer_approved_bookings()
        bookings = [Booking.objects.get(id=b.id) for b in self.bookings]
        for booking in bookings:
            self.assertTrue(booking.did_photographer_approve)
            self.assertTrue(booking.did_client_approve)

    def test_invalid_process_photographer_approved_bookings_too_early(self):
        with freeze_time(timezone.now() + CLIENT_APPROVAL_TIMEOUT - timedelta(days=1)):
            process_photographer_approved_bookings()
        bookings = [Booking.objects.get(id=b.id) for b in self.bookings]
        for booking in bookings:
            self.assertFalse(booking.did_client_approve)


class ProcessClientApprovedBookingsTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(ProcessClientApprovedBookingsTestCase, cls).setUpClass()
        create_photographers(5, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(ProcessClientApprovedBookingsTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        create_bookings(3, do_send_client_confirmation=False, do_sale=True)
        self.bookings = create_bookings(3, do_send_client_confirmation=False, do_sale=False)
        for booking in self.bookings:
            booking.num_shots = random.randint(20, 75)
            booking.on_photographer_approval()
            booking.on_staff_approval()
            booking.on_client_approval()
        mail.outbox = list()

    def tearDown(self):
        BookingRequest.objects.all().delete()
        Booking.objects.all().delete()

    def test_process_client_approved_bookings(self):
        with freeze_time(timezone.now() + POSTMATES_BOOKING_CHARGE_DELAY):
            process_client_approved_bookings()

        bookings = [Booking.objects.get(id=b.id) for b in self.bookings]
        for booking in bookings:
            transaction = braintree.Transaction.find(booking.braintree_id)
            self.assertIsNotNone(transaction)
            self.assertEqual(Decimal(transaction.amount), booking.amount)
            self.assertEqual(Decimal(transaction.service_fee_amount), booking.service_fee_amount)
            self.assertEqual(
                transaction.status,
                BraintreeTransactionStatusType.labels.get(
                    BraintreeTransactionStatusType.SUBMITTED_FOR_SETTLEMENT
                )
            )
            self.assertIsNone(transaction.escrow_status)

        # Check for receipts
        outbox = [m for m in mail.outbox if 'SHOTZU Payment Receipt' == m.subject]
        self.assertEqual(len(outbox), len(bookings))
        for receipt in outbox:
            self.assertEqual(DEFAULT_FROM_EMAIL, receipt.from_email)
            self.assertEqual(STAFF_EMAILS, receipt.bcc)

    def test_invalid_process_client_approved_bookings_too_early(self):
        with freeze_time(timezone.now() + POSTMATES_BOOKING_CHARGE_DELAY - timedelta(days=1)):
            process_client_approved_bookings()
        bookings = [Booking.objects.get(id=b.id) for b in self.bookings]
        for booking in bookings:
            self.assertEqual(booking.braintree_id, '')


AWS_STORAGE_BUCKET_NAME = 'gouda-test'
AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME
S3_URL = 'https://%s' % AWS_S3_CUSTOM_DOMAIN
MEDIAFILES_LOCATION = 'media'


@override_settings(
    DEFAULT_FILE_STORAGE='gouda.storages.S3MediaStorage',
    MEDIAFILES_LOCATION='media',
    MEDIA_URL='%s/%s/' % (S3_URL, MEDIAFILES_LOCATION),
    THUMBOR_MEDIA_URL='%s/%s/' % (S3_URL, MEDIAFILES_LOCATION),
    THUMBNAIL_DEFAULT_STORAGE='gouda.storages.S3MediaStorage'
)
@unittest.skip('Will refactor this in the future...')
class SmartCropBookingImageTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(SmartCropBookingImageTestCase, cls).setUpClass()
        create_photographers(3, do_create_marketplace=True)

    @classmethod
    def tearDownClass(cls):
        super(SmartCropBookingImageTestCase, cls).tearDownClass()
        tear_down_photographers()

    def setUp(self):
        self.booking = create_bookings(1, do_create_images=True)[0]
        self.album = BookingAlbum.objects.get(booking=self.booking)
        self.user = self.booking.client.user
        self.client.login(username=self.user.email_address, password='%s123' % self.user.profile.full_name.replace(' ', '').lower())
        image = self.album.images.all()[0]

        self.data = {
            'url': image.image.url,
            'name': image.name,
            'width': 1000,
            'height': 1000,
            'album_id': self.album.id,
        }

    def test_smart_crop_booking_image(self):
        smart_crop_booking_image(**self.data)

    def tearDown(self):
        BookingAlbum.objects.all().delete()
        BookingImage.objects.all().delete()
        Booking.objects.all().delete()


class SendPhotographerCompletionTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(SendPhotographerCompletionTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_bookings(10)
        for booking in Booking.objects.all():
            lower = timezone.now() + timedelta(days=random.randint(7, 30))
            upper = lower + timedelta(hours=1)
            booking.time_range = DateTimeTZRange(
                lower=lower,
                upper=upper
            )
            booking.save()

    @classmethod
    def tearDownClass(cls):
        super(SendPhotographerCompletionTestCase, cls).tearDownClass()
        tear_down_photographers()

    def test_send_photographer_completion(self):
        with freeze_time(timezone.now() + timedelta(days=9000)):
            send_photographer_completion()
            self.assertEqual(len(mail.outbox), 10)


class SendPostmatesNotificationTestCase(TestCase):
    @classmethod
    def setUpClass(cls):
        super(SendPostmatesNotificationTestCase, cls).setUpClass()
        create_photographers(2, do_create_marketplace=True)
        create_food_bookings(10)
        client = random.choice(Profile.objects.all())
        client.user.email_address = 'images@postmates.com'
        client.user.save()
        for booking in Booking.objects.all():
            booking.client = client
            booking.save()

    @classmethod
    def tearDownClass(cls):
        super(SendPostmatesNotificationTestCase, cls).tearDownClass()
        tear_down_photographers()

    def test_send_postmates_notification(self):
        send_postmates_notification()
        self.assertEqual(len(mail.outbox), 1)
        notification_mail = mail.outbox[0]
        self.assertEqual(notification_mail.to, STAFF_EMAILS)
        self.assertEqual(len(notification_mail.attachments), 1)