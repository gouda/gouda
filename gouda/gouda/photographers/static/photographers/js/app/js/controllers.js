'use strict';

var US_REGIONS = [
    {label: 'Alabama', value: 'Alabama'},
    {label: 'Alaska', value: 'Alaska'},
    {label: 'American Samoa', value: 'American Samoa'},
    {label: 'Arizona', value: 'Arizona'},
    {label: 'Arkansas', value: 'Arkansas'},
    {label: 'California', value: 'California'},
    {label: 'Colorado', value: 'Colorado'},
    {label: 'Connecticut', value: 'Connecticut'},
    {label: 'Delaware', value: 'Delaware'},
    {label: 'District of Columbia', value: 'District of Columbia'},
    {label: 'Florida', value: 'Florida'},
    {label: 'Georgia', value: 'Georgia'},
    {label: 'Guam', value: 'Guam'},
    {label: 'Hawaii', value: 'Hawaii'},
    {label: 'Idaho', value: 'Idaho'},
    {label: 'Illinois', value: 'Illinois'},
    {label: 'Indiana', value: 'Indiana'},
    {label: 'Iowa', value: 'Iowa'},
    {label: 'Kansas', value: 'Kansas'},
    {label: 'Kentucky', value: 'Kentucky'},
    {label: 'Louisiana', value: 'Louisiana'},
    {label: 'Maine', value: 'Maine'},
    {label: 'Maryland', value: 'Maryland'},
    {label: 'Massachusetts', value: 'Massachusetts'},
    {label: 'Michigan', value: 'Michigan'},
    {label: 'Minnesota', value: 'Minnesota'},
    {label: 'Mississippi', value: 'Mississippi'},
    {label: 'Missouri', value: 'Missouri'},
    {label: 'Montana', value: 'Montana'},
    {label: 'Nebraska', value: 'Nebraska'},
    {label: 'Nevada', value: 'Nevada'},
    {label: 'New Hampshire', value: 'New Hampshire'},
    {label: 'New Jersey', value: 'New Jersey'},
    {label: 'New Mexico', value: 'New Mexico'},
    {label: 'New York', value: 'New York'},
    {label: 'North Carolina', value: 'North Carolina'},
    {label: 'North Dakota', value: 'North Dakota'},
    {label: 'Northern Mariana Islands', value: 'Northern Mariana Islands'},
    {label: 'Ohio', value: 'Ohio'},
    {label: 'Oklahoma', value: 'Oklahoma'},
    {label: 'Oregon', value: 'Oregon'},
    {label: 'Pennsylvania', value: 'Pennsylvania'},
    {label: 'Puerto Rico', value: 'Puerto Rico'},
    {label: 'Rhode Island', value: 'Rhode Island'},
    {label: 'South Carolina', value: 'South Carolina'},
    {label: 'South Dakota', value: 'South Dakota'},
    {label: 'Tennessee', value: 'Tennessee'},
    {label: 'Texas', value: 'Texas'},
    {label: 'U.S. Virgin Islands', value: 'U.S. Virgin Islands'},
    {label: 'Utah', value: 'Utah'},
    {label: 'Vermont', value: 'Vermont'},
    {label: 'Virginia', value: 'Virginia'},
    {label: 'Washington', value: 'Washington'},
    {label: 'West Virginia', value: 'West Virginia'},
    {label: 'Wisconsin', value: 'Wisconsin'},
    {label: 'Wyoming', value: 'Wyoming'}
];

/* Controllers */
var LOCATION_DROP_DOWN_LIST_OPTIONS = {
    dataSource: [
        {
            label: 'Colorado',
            value: 'CO'
        },
        {
            label: 'Jamaica',
            value: 'JM'
        },
        {
            label: 'Washington D.C.',
            value: 'DC'
        },
        {
            label: 'All Locations',
            value: 'ALL'
        }
    ],
    dataTextField: 'label',
    dataValueField: 'value',
    valueTemplate: '<span class="fa fa-map-marker fa-1x kendo-input-addon"></span><span>{{dataItem.label}}</span>'
};

var CONDITION_DROP_DOWN_LIST_OPTIONS = {
    dataTextField: 'label',
    dataValueField: 'value',
    valueTemplate: '<span class="fa fa-map-marker fa-1x kendo-input-addon"></span><span>{{dataItem.label}}</span>'
};

var module = angular.module('gouda.photographers.controllers', []);

module.controller('FavoriteController', ['$scope', '$attrs', 'FavoriteResource',
    function($scope, $attrs, FavoriteResource) {
        // Listing
        var favoriteId = ($attrs.favoriteId) ? parseInt($attrs.favoriteId):-1;
        $scope.listing = {
            isFavorite: favoriteId >= 0,
            id: $attrs.listingId
        };

        // Favorites
        $scope.favoriteResource = {
            id: favoriteId
        };

        $scope.toggleFavorite = function() {
            $scope.listing.isFavorite = !angular.isDefined($scope.listing.isFavorite) || !$scope.listing.isFavorite;
            var favoriteResource = new FavoriteResource();
            if ($scope.listing.isFavorite) {
                favoriteResource.objectId = $scope.listing.id;
                favoriteResource.appLabel = 'listings';
                favoriteResource.modelName = 'Listing';
                favoriteResource.$save(function(favoriteObjectResource){
                    $scope.favoriteResource = favoriteObjectResource;
                });
            } else {
                favoriteResource.id = $scope.favoriteResource.id;
                favoriteResource.$delete(function(){
                    $scope.favoriteResource = {};
                });
            }
        };
    }
]);

module.controller('PhotographerSearchResultsController', ['$scope', '$window', '$interval', '$state', 'listingSearchResultsService', 'ListingResource', 'FavoriteResource',
    function($scope, $window, $interval, $state, listingSearchResultsService, ListingResource, FavoriteResource) {
        $scope.listingResource = null;
        $scope.limit = 21;
        $scope.offset = 0;
        $scope.didInitialize = false;

        // The watch
        $scope.$watch('offset', function(newValue){
            $state.go('searchResults', {offset: newValue});
        });

        // Use $state to query server
        var queryPromise;
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            if (!$scope.didInitialize) {
                if (toParams.offset) {
                    $scope.offset = toParams.offset;
                }
                $scope.didInitialize = true;
            }
            if (angular.isDefined(queryPromise)) {
                $interval.cancel(queryPromise);
                listingSearchResultsService.lastParams = undefined;
            }
            queryPromise = $interval(function() {
                var params = angular.extend({}, $state.params);
                if (!angular.isDefined(params.offset)) {
                    params.offset = 0;
                }
                params.limit = $scope.limit;

                $scope.query(params);

                // Scroll
                var element = angular.element('html');
                angular.element('html, body, .search-container').scrollTop(0);
            }, 250, 1);
        });

        $scope.query = function(params) {
            listingSearchResultsService.listings = [];
            if (angular.isDefined(params.maxPrice) && params.maxPrice >= 1000) {
                params.maxPrice = undefined;
            }
            if (angular.isDefined(params.condition) && params.condition > 2) {
                params.condition = undefined;
            }
            params.listingId = '';

            if (angular.isDefined(listingSearchResultsService.lastParams) && angular.equals(listingSearchResultsService.lastParams, params)) {
                return;
            }

            $scope.listingResourcePromise = ListingResource.get(params, function(listingResource){
                $scope.listingResource = listingResource;
                $scope.listingResourcePromise = null;
                listingSearchResultsService.listings = listingResource.results;
                $scope.refreshFavorites();
                listingSearchResultsService.lastParams = params;
            });

            if (!angular.isObject($scope.favoriteResourcePromise) && !angular.isObject($scope.favoriteResource)) {
                $scope.queryFavorites();
            }
        };

        // Spinner
        $scope.isLoading = function() {
            return angular.isDefined($scope.listingResourcePromise) && $scope.listingResourcePromise != null;
        };

        // Favorites
        $scope.favoriteResource = null;
        $scope.refreshFavorites = function() {
            if (!angular.isObject($scope.listingResource) || !angular.isObject($scope.favoriteResource)) {
                return;
            }
            angular.forEach($scope.listingResource.results, function(listing, key) {
                angular.forEach($scope.favoriteResource, function(favorite, key){
                    if (favorite.objectId == listing.id) {
                        listing.isFavorite = true;
                    }
                });
            });
        };
        $scope.queryFavorites = function() {
            $scope.favoriteResourcePromise = FavoriteResource.query({favoriteId:''}, function(favoriteResource){
                $scope.favoriteResource = favoriteResource;
                $scope.favoriteResourcePromise = null;
                $scope.refreshFavorites();
            });
        };
        $scope.toggleFavorite = function(listing) {
            listing.isFavorite = !angular.isDefined(listing.isFavorite) || !listing.isFavorite;
            if (listing.isFavorite) {
                var favorite = new FavoriteResource();
                favorite.objectId = listing.id;
                favorite.appLabel = 'listings';
                favorite.modelName = 'Listing';
                favorite.$save(function(favoriteObjectResource){
                    if (!angular.isArray($scope.favoriteResource)) {
                        $scope.favoriteResource = [];
                    }
                    $scope.favoriteResource.push(favoriteObjectResource);
                });
            } else {
                angular.forEach($scope.favoriteResource, function(favorite, key){
                    if (favorite.objectId != listing.id) {
                        return;
                    }
                    favorite.$delete(function(){
                        $scope.favoriteResource.splice(key, 1);
                    });
                });
            }
        };
    }
]);

module.controller('PhotographerSearchResultsFormController', ['$scope', '$window', '$interval', '$state',
    function($scope, $window, $interval, $state) {
        $scope.didInitialize = false;

        // Query
        var queryPromise;
        $scope.query = '';
        $scope.$watch('query', function(newVal) {
            if (angular.isDefined(queryPromise)) {
                $interval.cancel(queryPromise);
            }
            queryPromise = $interval(function(){
                if (!$scope.didInitialize) {
                    return;
                }
                $state.go('searchResults', {
                    query: newVal,
                    offset: 0
                });
            }, 250, 1);
        });

        // Condition
        $scope.condition = 3;
        $scope.conditionDropDownListOptions = CONDITION_DROP_DOWN_LIST_OPTIONS;
        $scope.$watch('condition', function(newVal){
            if (!$scope.didInitialize) {
                return;
            }
            $state.go('searchResults', {
                condition: newVal,
                offset: 0
            });
        });

        // Price Range
        $scope.price = [0, 1000];
        $scope.priceRangeSliderOptions = {
            min: 0,
            max: 1000,
            tooltip: {
                enabled: false,
                template: '$#= selectionStart # - $#= selectionEnd #'
            }
        };

        $scope.getMinPrice = function() {
            return $scope.price[0];
        };

        $scope.getMaxPrice = function() {
            return $scope.price[1];
        };

        var priceQueryPromise;
        $scope.onPriceRangeSliderSlide = function(kendoEvent) {
            $scope.price = kendoEvent.value;
            if (angular.isDefined(priceQueryPromise)) {
                $interval.cancel(priceQueryPromise);
            }
            priceQueryPromise = $interval(function(){
                if (!$scope.didInitialize) {
                    return;
                }
                $state.go('searchResults', {
                    minPrice: $scope.price[0],
                    maxPrice: $scope.price[1],
                    offset: 0
                });
            }, 250, 1);
        };

        $(window).resize(function() {  // For redrawing the slider when the window is resized
            if (angular.isDefined($scope.priceRangeSlider)) {
                $scope.priceRangeSlider.resize();
            }
        });

        // Disable controls
        $scope.areControlsEnabled = function() {
            return true;
        };

        // Use $state to initialize controls
        $scope.$on('$stateChangeSuccess', function(event, toState, toParams, fromState, fromParams){
            if ($scope.didInitialize) {
                return;
            }
            if (angular.isDefined(toParams.query)) {
                $scope.query = toParams.query;
            }
            if (angular.isDefined(toParams.condition)) {
                $scope.condition = toParams.condition;
            }
            if (angular.isDefined(toParams.minPrice)) {
                $scope.price[0] = toParams.minPrice;
            }
            if (angular.isDefined(toParams.maxPrice)) {
                $scope.price[1] = toParams.maxPrice;
            }
            $scope.didInitialize = true;
        });
    }
]);

module.controller('PhotographerBookingQueryFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        $scope.region = 'DC';

        // DatePicker
        $scope.datePickerOptions = {
            start: 'month',
            depth: 'month',
            readonly: false,
            min: new Date(),
            month: {
                //empty: '<a class="" style="background-color: rgb(240, 240, 240); min-height: 2.571em; line-height: 2.571em; padding: 0; display: block"></a>'
            }
        };
        $scope.date = moment(new Date()).add(0, 'days').format('L');
    }
]);

module.controller('BookingDetailsFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        // DatePicker
        $scope.datePickerOptions = {
            start: 'month',
            depth: 'month',
            readonly: false,
            min: new Date(),
            month: {
                //empty: '<a class="" style="background-color: rgb(240, 240, 240); min-height: 2.571em; line-height: 2.571em; padding: 0; display: block"></a>'
            }
        };
        //$scope.date = moment(new Date()).add(0, 'days').toDate('L');

        $scope.timePickerOptions = {
            //value: new Date(2011, 0, 1, 10, 30)
        };
        $scope.startTime = moment('6:00 PM', 'h:mm A').toDate();
        $scope.endTime = moment('7:00 PM', 'h:mm A').toDate();
    }
]);

module.controller('MessageCreateFormController', ['$scope', '$attrs', 'MessageResource',
    function($scope, $attrs, MessageResource) {
        $scope.onSubmit = function() {
            var messageResource = new MessageResource();
            messageResource.body = $scope.body;
            messageResource.receiver = {
                id: $attrs.receiverId
            };
            messageResource.$save(function(messageObjectResource){
            });
        };
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

module.controller('BookingAddressFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.fullName = $attrs.fullName;
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

module.controller('ReviewFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        // Submit
        $scope.submit = function(event) {
            if (!$scope.isFormValid()) {
                angular.forEach($scope.form.$error.required, function(field) {
                    field.$setDirty();
                });
                event.preventDefault();
            }
        }
    }
]);

module.controller('PhotographerRequestFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        function clean(x) {
            if (x == 'None') {
                return '';
            }
            return x;
        }
        $scope.fullName = clean($attrs.fullName);
        $scope.phoneNumber = clean($attrs.phoneNumber);
        $scope.emailAddress = clean($attrs.emailAddress);
        $scope.serviceType = clean($attrs.serviceType);
        $scope.locality = clean($attrs.locality);
        $scope.region = clean($attrs.region);
        $scope.postalCode = clean($attrs.postalCode);
        $scope.message = clean($attrs.message);
        $scope.timePeriod = clean($attrs.timePeriod);
        $scope.date = clean($attrs.date);

        // DatePicker
        $scope.datePickerOptions = {
            start: 'month',
            depth: 'month',
            readonly: false,
            min: new Date(),
            month: {
                //empty: '<a class="" style="background-color: rgb(240, 240, 240); min-height: 2.571em; line-height: 2.571em; padding: 0; display: block"></a>'
            }
        };

        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

module.controller('PhotographerFormController', ['$scope', '$attrs',
    function($scope, $attrs) {
        function clean(x) {
            if (x == 'None') {
                return '';
            }
            return x;
        }
        $scope.individualLocality = clean($attrs.individualLocality);
        $scope.individualPostalCode = clean($attrs.individualPostalCode);
        $scope.individualRegion = clean($attrs.individualRegion);
        $scope.individualStreetAddress = clean($attrs.individualStreetAddress);
        $scope.individualMonth = parseInt(clean($attrs.individualMonth));
        $scope.individualDay = parseInt(clean($attrs.individualDay));
        $scope.individualYear = parseInt(clean($attrs.individualYear));
        $scope.individualEmail = clean($attrs.individualEmail);
        $scope.individualFirstName = clean($attrs.individualFirstName);
        $scope.individualLastName = clean($attrs.individualLastName);
        $scope.individualPhone = clean($attrs.individualPhone);
        $scope.fundingAccountNumber = clean($attrs.fundingAccountNumber);
        $scope.fundingRoutingNumber = clean($attrs.fundingRoutingNumber);
        $scope.businessLocality = clean($attrs.businessLocality);
        $scope.businessPostalCode = clean($attrs.businessPostalCode);
        $scope.businessRegion = clean($attrs.businessRegion);
        $scope.businessStreetAddress = clean($attrs.businessStreetAddress);
        $scope.businessDbaName = clean($attrs.businessDbaName);
        $scope.businessLegalName = clean($attrs.businessLegalName);
        $scope.businessTaxId = clean($attrs.businessTaxId);
        $scope.status = clean($attrs.status);
        $scope.doEnableBusinessForm = false;

        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
    }
]);

module.controller('CustomerFormController', ['$scope', '$attrs', '$timeout', 'BraintreeClientTokenResource',
    function($scope, $attrs, $timeout, BraintreeClientTokenResource) {
        $scope.didFail = false;
        $scope.paymentMethodNonce = undefined;

        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };

        // Submit
        $scope.submit = function(event) {
            if ($scope.paymentMethodNonce) {
                return;
            }
            $scope.isUploading = true;
            event.preventDefault();

            $scope.braintreeClient.tokenizeCard({
                number: $scope.cardNumber,
                expirationDate: $scope.expirationDate,
                cvv: $scope.securityCode
            }, function (error, paymentMethodNonce) {
                if (error) {
                    $timeout(function () {
                        $scope.didFail = true;
                        $scope.isUploading = false;
                    });
                } else {
                    $timeout(function () {
                        $scope.paymentMethodNonce = paymentMethodNonce;
                        angular.element('#customer-create-form').submit();
                    });
                }
                //console.log(error);
                //console.log(paymentMethodNonce);
            });
        };

        // Fetch client token for Braintree
        function success(response, responseHeaders) {
            $scope.braintreeClient = new braintree.api.Client({clientToken: response.clientToken});
        }
        function error(response) {
            $scope.didFail = true;
        }
        var clientTokenPromise = BraintreeClientTokenResource.get({}, success, error);
    }
]);

module.controller('BookingFormController', ['$scope', '$attrs', 'checkoutFormService',
    function($scope, $attrs, checkoutFormService) {
        $scope.totalCost = $attrs.totalCost;
        $scope.isFormValid = function() {
            return $scope.form.$valid;
        };
        $scope.isCouponValid = function() {
            return checkoutFormService.isCouponValid();
        };
        $scope.getDiscountTotal = function() {
            var discountTotal = $scope.totalCost;
            if ($scope.isCouponValid()) {
                discountTotal -= checkoutFormService.coupon.amountOff;
            }
            return discountTotal;
        };
    }
]);