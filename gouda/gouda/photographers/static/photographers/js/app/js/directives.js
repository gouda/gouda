var module = angular.module('gouda.photographers.directives', []);

module.directive('jwInfoWindow', function(){
    function link(scope, element, attrs) {
        var infoWindow = $('.gm-style-iw');
        infoWindow.css('top', '15px');
        infoWindow.css('left', '0');

        var infoWindowBackground = infoWindow.prev();
        infoWindowBackground.children(':nth-child(2)').css({'display' : 'none'});
        infoWindowBackground.children(':nth-child(4)').css({'display' : 'none'});

        infoWindow.next().css({'display' : 'none'});
    }
    return {
        link: link,
        restrict: 'E',
        transclude: true,
        template: '<div ng-transclude></div>'
    }
});

module.directive('jwPagination', function(){
    function link(scope, element, attrs) {
        function refresh() {
            var i;
            scope.currentPageOffset = scope.offset / scope.limit;
            scope.currentPageNumber = scope.currentPageOffset + 1;
            var remainder = scope.count % scope.limit;
            var lastOffset = (remainder == 0) ? scope.count - scope.limit : scope.count - remainder;
            var lastPageOffset = Math.ceil(lastOffset / scope.limit);

            scope.doShowPreviousLink = scope.offset != 0;
            scope.doShowNextLink = scope.offset < (scope.count - scope.limit);

            // Build pages
            scope.pages = [];
            var forwardPadding = 1;
            var backPadding = 1;
            if (scope.currentPageOffset < 3) {
                forwardPadding = 2;
            } else if (scope.currentPageOffset > lastPageOffset - 3) {
                backPadding = 2;
            }
            for (i = 0; i <= lastPageOffset; i++) {
                if (!((i <= scope.currentPageOffset + forwardPadding && i >= scope.currentPageOffset - backPadding) || (i == 0) || (i == lastPageOffset))) {
                    continue;
                }
                scope.pages.push({
                    offset: i,
                    number: i + 1,
                    isSelected: scope.isSelectedPage(i + 1),
                    isEllipsis: false,
                    onClick: function() {
                        scope.offset = scope.getOffsetFromPageNumber(this.number);
                    }
                });
            }

            // Add ellipsises...?
            if (scope.currentPageOffset > 2) {
                scope.pages.splice(1, 0, {
                    isEllipsis: true
                });
            }
            if (scope.currentPageOffset < (lastPageOffset - 2)) {
                scope.pages.splice(scope.pages.length - 1, 0, {
                    isEllipsis: true
                });
            }
        }
        scope.$watch('count', function(newValue) {
            refresh();
        });
        scope.$watch('offset', function(newValue) {
            refresh();
        });
        scope.$watch('limit', function(newValue) {
            refresh();
        });
        scope.$watch("ngClass", function (newValue) {
            refresh();
            $(element).attr("class", newValue);
        });
        scope.doShowPagination = function() {
            return scope.count > scope.limit;
        };
        scope.isSelectedPage = function(pageNumber) {
            var ret = pageNumber == (scope.offset / scope.limit) + 1;
            return ret;
        };

        scope.getOffsetFromPageNumber = function(pageNumber) {
            return (pageNumber - 1) * scope.limit;
        };

        scope.previousPage = function() {
            scope.offset = scope.getOffsetFromPageNumber(scope.currentPageNumber - 1);
        };

        scope.nextPage = function() {
            scope.offset = scope.getOffsetFromPageNumber(scope.currentPageNumber + 1);
        };

        refresh();
    }
    return {
        link: link,
        restrict: 'E',
        templateUrl: 'listings/pagination.html',
        scope: {
            limit: '=',
            offset: '=',
            count: '=',
            href: '@'
        }
    }
});

module.directive('jwDropzone', function(){
    function link(scope, element, attrs, ctrl) {
        scope.maxFiles = 20;
        var addPhotoButton = element.find('.add-photo-button')[0];
        var previewsContainer = element.find('.dropzone-previews')[0];
        var previewTemplate = element.find('.preview-template')[0];

        scope.getNumValidFiles = function() {
            var i;
            var count = 0;
            for (i = 0; i < scope.files.length; i++) {
                if (scope.files[i].isValid) {
                    count++;
                }
            }
            return count;
        };

        element.dropzone({
            init: function() {
                var dropzone = this;
                angular.forEach(scope.files, function(file, key) {
                    if (!file.isValid) {
                        return;
                    }
                    file.wasBootstrapped = true;
                    dropzone.addFile(file);
                });
            },
            url: '/',
            autoProcessQueue: false,
            clickable: addPhotoButton,
            previewsContainer: previewsContainer,
            maxFilesize: 5,
            maxFiles: scope.maxFiles,
            acceptedFiles: '.jpg,.jpeg,.png',
            thumbnailWidth: 500,
            thumbnailHeight: 500,
            previewTemplate: previewTemplate.innerHTML,
            addedfile: function(file) {
                if (file.wasBootstrapped) {
                    $('.dropzone-previews').append(file.previewElement);
                    return;
                }
                var dropzone = this;
                //file.previewElement = Dropzone.createElement(this.options.previewTemplate);
                file.isValid = true;
                file.id = (new Date()).getTime();
                file.previewElement = $($.parseHTML(this.options.previewTemplate));
                scope.files.push(file);

                // Read the data into base64
                var fileReader = new FileReader();
                fileReader.onload = function(e) {
                    var dataURL = fileReader.result;
                    file.dataURL = dataURL;

                    // Now attach this new element some where in your page
                    $('.dropzone-previews').append(file.previewElement);

                    if (scope.getNumValidFiles() >= dropzone.options.maxFiles) {
                        dropzone.disable();
                    } else {
                        dropzone.enable();
                    }
                    if(!scope.$$phase) {
                        scope.$apply();
                    }
                };
                fileReader.readAsDataURL(file);

            },
            removedfile: function(file) {
                var i;
                var dropzone = this;
                file.previewElement.remove();
                for (i = 0; i < scope.files.length; i++) {
                    if (file.id == scope.files[i].id) {
                        scope.files.splice(i, 1);
                        break;
                    }
                }
                if (scope.getNumValidFiles() >= dropzone.options.maxFiles) {
                    dropzone.disable();
                } else {
                    dropzone.enable();
                }
                if(!scope.$$phase) {
                    scope.$apply();
                }

            },
            complete: function(file) {
            },
            error: function(file, errorMessage, xhr) {
                if (errorMessage == 'Upload canceled.' || errorMessage == 'You can not upload any more files.') {
                    return;
                }
                file.isValid = false;
                file.dataURL = null;
                file.previewElement.addClass('ng-invalid');
                if(!scope.$$phase) {
                    scope.$apply();
                }
            },
            maxfilesreached: function(file) {

            },
            maxfilesexceeded: function(file) {

            },
            thumbnail: function(file, dataUrl) {
                var dropzone = this;
                var removeButton = file.previewElement.find('.dz-remove');
                removeButton.click(function(){
                    dropzone.removeFile(file);
                });
                var imageContainer = file.previewElement.find('.dz-image');
                imageContainer.css('background-image', 'url(' + dataUrl + ')');
                imageContainer.css('background-size', 'cover');
                imageContainer.css('background-position', 'center center');
                imageContainer.css('width', '100%');
                imageContainer.css('height', '100%');
                if(!scope.$$phase) {
                    scope.$apply();
                }
            }
        });
        scope.dropzone = element[0].dropzone;
    }

    return {
        link: link,
        templateUrl: 'listings/dropzone.html',
        scope: {
            files: '='
        }
    }
});

module.directive('jwDate', function(){
    function link(scope, element, attrs, ctrl) {
        ctrl.$validators.date = function(modelValue, viewValue) {
            var time = Date.parse(viewValue);
            if (isNaN(time)) {
                return false;
            }

            var nowDate = new Date();
            var date = new Date();
            date.setTime(time);
            if (date.getTime() < (new Date()).getTime()) {
                return date.getMonth() == nowDate.getMonth() && date.getDay() == nowDate.getDay() && date.getFullYear() == nowDate.getFullYear();
            }
            return true;
        };

        if (angular.isDefined(scope.jwMin)) {
            scope.$watch('jwMin', function(newValue) {
                ctrl.$setDirty();
            });
            ctrl.$validators.jwMin = function(modelValue, viewValue) {
                var minDate = Date.parse(scope.jwMin);
                if (angular.isUndefined(minDate)) {
                    return true;
                }
                var afterDate = Date.parse(viewValue);
                return minDate < afterDate;
            };
        }

    }

    return {
        link: link,
        require: 'ngModel',
        scope: {
            jwMin: '='
        }
    }
});

//module.directive('jwBeforeDate', function(){
//    function link(scope, element, attrs, ctrl) {
//        ctrl.$validators.jwBeforeDate = function(modelValue, viewValue) {
//            var beforeDate = Date.parse(attrs.jwBeforeDate);
//            var afterDate = Date.parse(viewValue);
//            console.log(attrs);
//            console.log(attrs.jwBeforeDate + ' vs. ' + afterDate);
//            return beforeDate < afterDate;
//        };
//        scope.$watch('jwBeforeDate', function(newValue, oldValue) {
//            console.log("WATCHING!!!");
//            if (newValue != oldValue) {
//                ctrl.$setDirty();
//            }
//        });
//    }
//
//    return {
//        link: link,
//        require: 'ngModel',
//        scope: {
//            jwBeforeDate: '='
//        }
//    }
//});

module.directive('jwShowMore', function(){
    return {
        restrict: 'E',
        transclude: true,
        templateUrl: 'listings/show_more.html',
        scope: {
            initialHeight: '@',
            jwClass: '@'
        }
    }
});

module.directive('jwRating', function(){
    function link(scope, element, attrs) {
        scope.filledStars = [];
        for (var i = 0; i < parseInt(scope.rating); i++) {
            scope.filledStars.push(i);
        }
        scope.doShowExtraStar = (scope.rating - parseInt(scope.rating)) > 0.75;
        scope.doShowHalfStar = !scope.doShowExtraStar && ((scope.rating - parseInt(scope.rating)) > 0.5);
        scope.emptyStars = [0, 1, 2, 3, 4];
    }
    return {
        link: link,
        restrict: 'E',
        templateUrl: 'listings/rating.html',
        scope: {
            rating: '='
        }
    }
});

module.directive('jwOldPagination', function(){
    function link(scope, element, attrs) {
        function update() {
            scope.currentPageOffset = scope.offset / scope.limit;
            scope.currentPageNumber = scope.currentPageOffset + 1;
            var remainder = scope.count % scope.limit;
            var lastOffset = (remainder == 0) ? scope.count - scope.limit : scope.count - remainder;
            var lastPageOffset = Math.ceil(lastOffset / scope.limit);

            scope.doShowPreviousLink = scope.offset != 0;
            scope.doShowNextLink = scope.offset < (scope.count - scope.limit);

            // In the beginning
            if (scope.currentPageOffset < 3) {
                scope.doShowFirstEllipsis = false;
                scope.doShowSecondEllipsis = lastPageOffset > 4;
                scope.lastPageNumber = lastPageOffset + 1;
                scope.firstPageNumber = 1;
                scope.secondPageNumber = 2;
                scope.thirdPageNumber = 3;
                scope.fourthPageNumber = 4;
                scope.doShowSecondPageNumber = scope.count > (2 * scope.limit);
                scope.doShowThirdPageNumber = scope.count > (3 * scope.limit);
                scope.doShowFourthPageNumber = scope.count > (4 * scope.limit);
            } else if (scope.currentPageOffset > (lastPageOffset - 4)) { // The end
                scope.doShowFirstEllipsis = lastPageOffset > 4;
                scope.doShowSecondEllipsis = false;
                scope.lastPageNumber = lastPageOffset + 1;
                scope.firstPageNumber = 1;
                scope.secondPageNumber = scope.lastPageNumber - 3;
                scope.thirdPageNumber = scope.lastPageNumber - 2;
                scope.fourthPageNumber = scope.lastPageNumber - 1;
                scope.doShowFourthPageNumber = scope.count > (2 * scope.limit);
                scope.doShowThirdPageNumber = scope.count > (3 * scope.limit);
                scope.doShowSecondPageNumber = scope.count > (4 * scope.limit);
            } else { // For the middle
                scope.doShowFirstEllipsis = true;
                scope.doShowSecondEllipsis = true;
                scope.lastPageNumber = lastPageOffset + 1;
                scope.firstPageNumber = 1;
                scope.thirdPageNumber = scope.currentPageOffset + 1;
                scope.secondPageNumber = scope.thirdPageNumber - 1;
                scope.fourthPageNumber = scope.thirdPageNumber + 1;
                scope.doShowSecondPageNumber = true;
                scope.doShowThirdPageNumber = true;
                scope.doShowFourthPageNumber = true;
            }

            // To show or hide...
            scope.doShowPagination = scope.count > scope.limit;


        }
        update();
        scope.$watch('count', function(newValue, oldValue) {
            update();
        });
        scope.$watch('offset', function(newValue, oldValue) {
            update();
        });
        scope.$watch('limit', function(newValue, oldValue) {
            update();
        });
        scope.$watch("ngClass", function (value) {
            update();
            $(element).attr("class", value);
        });
        scope.isSelectedPage = function(pageNumber) {
            var ret = pageNumber == (scope.offset / scope.limit) + 1;
            return ret;
        };

        scope.getOffsetFromPageNumber = function(pageNumber) {
            return (pageNumber - 1) * scope.limit;
        };

        scope.previousPage = function() {
            scope.offset = scope.getOffsetFromPageNumber(scope.currentPageNumber - 1);
        };

        scope.nextPage = function() {
            scope.offset = scope.getOffsetFromPageNumber(scope.currentPageNumber + 1);
        };
    }

    return {
        link: link,
        restrict: 'E',
        templateUrl: 'listings/pagination.html',
        scope: {
            limit: '=',
            offset: '=',
            count: '=',
            href: '@'
        }
    }
});