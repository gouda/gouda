module.exports = function(config){
    config.set({

        basePath : '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/maryjane/maryjane/rentals/static/rentals/js',

        files : [
            'http://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.js',
            'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-rc.1/angular.js',
            'https://da7xgjtj801h2.cloudfront.net/2015.1.408/js/kendo.all.min.js',
            'https://da7xgjtj801h2.cloudfront.net/2015.1.408/js/jszip.min.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/bower_components/angular-sanitize/angular-sanitize.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/bower_components/angular-material/angular-material.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/bower_components/angular-aria/angular-aria.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/bower_components/angular-ui-router/release/angular-ui-router.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/maryjane/maryjane/static/js/angularjs/angular-google-maps/angular-google-maps.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/maryjane/maryjane/static/js/angularjs/angular-bootstrap/ui-bootstrap-tpls.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/maryjane/maryjane/static/js/angularjs/angular-touch/angular-touch.js',
            '/Users/chukwuemeka/Documents/Projects/python/maryjane-web/maryjane/maryjane/static/js/angularjs/angular-slick/slick.js',
            'https://ajax.googleapis.com/ajax/libs/angularjs/1.4.0-rc.1/angular-animate.js',
            'http://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-resource.js',
            'http://ajax.googleapis.com/ajax/libs/angularjs/1.3.15/angular-mocks.js',
            'app/js/**/*.js',
            'test/unit/**/*.js'
        ],

        autoWatch : true,

        frameworks: ['jasmine'],

        browsers : ['Chrome'],

        plugins : [
            'karma-chrome-launcher',
            'karma-jasmine',
            'karma-phantomjs-launcher'
        ],

        junitReporter : {
            outputFile: 'test_out/unit.xml',
            suite: 'unit'
        }

    });
};