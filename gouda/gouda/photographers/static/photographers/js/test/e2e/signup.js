
describe('Registration', function() {
    it('register a new user and login', function() {
        browser.ignoreSynchronization = true;

        // Register
        var emailAddress = 'sterlingarcher' + parseInt(Math.random() * 999999) + '@gmail.com';
        var password = 'dangerzone123';
        browser.get('profiles/signup');
        browser.driver.findElement(by.id('id_full_name')).sendKeys('Sterling Archer');
        browser.driver.findElement(by.id('id_month')).sendKeys('January');
        browser.driver.findElement(by.id('id_day')).sendKeys('1');
        browser.driver.findElement(by.id('id_year')).sendKeys('1970');
        browser.driver.findElement(by.id('id_gender')).sendKeys('Male');
        browser.driver.findElement(by.id('id_email_address')).sendKeys(emailAddress);
        browser.driver.findElement(by.id('id_password')).sendKeys(password);
        browser.driver.findElement(by.css('.btn--submit')).click();
    });
});