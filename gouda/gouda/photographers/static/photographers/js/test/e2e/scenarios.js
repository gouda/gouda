'use strict';


describe('Rentals', function() {

    describe('Search results for rentals', function() {
        beforeEach(function() {
            // Go to query page
            browser.ignoreSynchronization = false;
            browser.get('rentals/search/results/?destination=Colorado&check_in_date=4%2F29%2F2015&check_out_date=4%2F29%2F2015&number_of_guests=1');
        });

        function filterByPrice() {
            var leftDragHandleElement = element.all(by.css('.k-draghandle')).get(0);
            var rightDragHandleElement = element.all(by.css('.k-draghandle')).get(1);
            //var rightDragHandleElement = element.all(by.className('k-draghandle'))[1];
            browser.actions()
                .mouseMove(leftDragHandleElement)
                .mouseDown()
                .mouseMove(leftDragHandleElement, {x: 100, y: 0})
                .mouseUp()
                .mouseMove(rightDragHandleElement)
                .mouseDown()
                .mouseMove(rightDragHandleElement, {x: -100, y: 0})
                .mouseUp()
                .perform();
        }

        it('this page should exist', function(done) {
            expect(true).toBe(true);
            done();
        });

        it('spinner should only appear while loading', function(){
            browser.sleep(1000);
            expect(element(by.className('spinner-container')).isDisplayed()).toBeFalsy();
            filterByPrice();
            //expect(element(by.className('spinner-container')).isDisplayed()).toBeTruthy();
            browser.sleep(1000);
            expect(element(by.className('spinner-container')).isDisplayed()).toBeFalsy();
        });

        it('should say "Vacation Rentals | ISLAND" in the title', function() {
            expect(browser.getTitle()).toMatch('Vacation Rentals | ISLAND');
        });

        it('favorite button should be clickable', function() {
            element.all(by.repeater('rental in rentalResource.results')).then(function(results) {
                browser.actions().mouseMove(results[0].element(by.className('rental-image'))).perform();
                var favoriteElement = results[0].element(by.className('rental-favorite'));
                expect(favoriteElement.getAttribute('class')).toMatch(/unselected/);

                browser.actions().mouseMove(favoriteElement, {x: 15, y: 15}).click().perform();
                browser.sleep(1000);
                expect(element(by.css('.rental-favorite.selected')).isDisplayed()).toBeTruthy();
            });

        });

        it('should filter rentals by price', function() {
            filterByPrice();
            browser.sleep(1000);
            element.all(by.repeater('rental in rentalResource.results')).then(function(results) {
                element(by.binding('getMinPrice()')).getText().then(function(text){
                    var minPrice = parseInt(text.replace(/\$/g,''));
                    element(by.binding('getMaxPrice()')).getText().then(function(text){
                        var maxPrice = parseInt(text.replace(/\$/g,''));
                        for (var i = 0; i < results.length; i++) {
                            var rental = results[i];
                            rental.element(by.binding('rental.nightlyPrice.price')).getText().then(function(text){
                                var price = parseInt(text.replace(/\$/g,''));
                                expect(price).toBeLessThan(maxPrice + 1);
                                expect(price).toBeGreaterThan(minPrice - 1);
                            });
                        }
                    });
                });
            });
        });

        it('should filter rentals by state', function() {
            element(by.css('#input-gang-1')).click();
            browser.sleep(1000);
            element(by.cssContainingText('li', 'Washington State')).click();
            element.all(by.repeater('rental in rentalResource.results')).then(function(results){
                for (var i = 0; i < results.length; i++) {
                    var rental = results[i];
                    expect(rental.evaluate('rental.region | unabbreviate')).toBe('Washington');
                }
            });
        });

        it('should only have at most 21 results per page', function(){
            element.all(by.repeater('rental in rentalResource.results')).then(function(results){
                expect(results.length).toBeLessThan(22);
            });
        });

        it('pagination controls should work', function(){
            element(by.id('lastPageNumber')).getText().then(function(lastPageNumber){
                var expectedPageNumber = null;
                for (var i = 0; i < lastPageNumber-1; i++) {
                    expectedPageNumber = i + 1;
                    expect(element(by.css('a.selected')).getText()).toBe(expectedPageNumber.toString());
                    element(by.css('a.next')).click();
                    browser.sleep(125);
                    expect(element(by.css('a.previous')).isDisplayed()).toBeTruthy();
                }
                expect(element(by.css('a.next')).isDisplayed()).toBeFalsy();
                for (var i = lastPageNumber-1; i > 0; i--) {
                    expectedPageNumber = i + 1;
                    expect(element(by.css('.pagination li a.selected')).getText()).toBe(expectedPageNumber.toString());
                    element(by.css('a.previous')).click();
                    browser.sleep(125);
                    expect(element(by.css('a.next')).isDisplayed()).toBeTruthy();
                }
                expect(element(by.css('a.previous')).isDisplayed()).toBeFalsy();
            });
        });

        it('rental detail page should be accessible', function(){
            browser.sleep(1000);
            element(by.css('.rental-search-result-container')).click();
            browser.sleep(1000);
            expect(element(by.css('.rental-summary')).isDisplayed()).toBeTruthy();
        });


    });

});