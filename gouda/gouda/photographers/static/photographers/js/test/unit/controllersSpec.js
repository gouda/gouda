'use strict';

/* jasmine specs for controllers go here */
describe('Rental Controllers', function() {
    beforeEach(function(){
        jasmine.addMatchers({
            toEqualData: function(util, customEqualityTesters) {
                return {
                    compare: function (actual, expected) {
                        return {
                            pass: angular.equals(actual, expected)
                        };
                    }
                }
            }
        });
    });
    beforeEach(angular.mock.module('gouda.listings.apps.ListingApp'));
    beforeEach(angular.mock.module('gouda.listings.apps.RentalCreateApp'));
    beforeEach(angular.mock.module('gouda.listings.services'));
    beforeEach(angular.mock.module('gouda.listings.directives'));
    beforeEach(angular.mock.module('gouda.listings.filters'));
    beforeEach(angular.mock.module('gouda.listings.templates'));


    describe('RentalCreateController', function(){
        var $scope;
        var $controller;
        var controller;
        var $state;

        beforeEach(inject(function(_$rootScope_, _$state_, _$controller_) {
            $controller = _$controller_;
            $scope = _$rootScope_.$new();
            $state = _$state_;
            controller = $controller('RentalCreateController', {
                $scope: $scope,
                $state: $state
            });
        }));

        describe('scope', function(){
            it('the scope should exist...?', function(){
                expect($scope).toBeDefined();
            });
        });
    });

    describe('RentalDetailController', function(){
        var $scope;
        var $controller;
        var controller;
        beforeEach(inject(function(_$rootScope_, _$controller_) {
            $controller = _$controller_;
            $scope = _$rootScope_.$new();
            controller = $controller('RentalDetailController', {
                $scope: $scope,
            });
        }));

        //describe('map', function(){
        //    it('the map should at the very least exist', function(){
        //        expect($scope.map).toBeDefined();
        //        expect($scope.mapOptions).toBeDefined();
        //        expect($scope.circle).toBeDefined();
        //    });
        //});
    });

    describe('RentalSearchResultsController', function(){
        var $httpBackend;
        var $controller;
        var $scope;
        var $window;
        var $interval;
        var controller;
        var favoritesResponse = {
            count: 5,
            next: null,
            previous: null,
            results: [
                {
                    id: 5,
                    url: "http://testserver/api/favorite/5/",
                    objectId: 49
                },
                {
                    id: 4,
                    url: "http://testserver/api/favorite/4/",
                    objectId: 6
                },
                {
                    id: 3,
                    url: "http://testserver/api/favorite/3/",
                    objectId: 45
                },
                {
                    id: 2,
                    url: "http://testserver/api/favorite/2/",
                    objectId: 52
                },
                {
                    id: 1,
                    url: "http://testserver/api/favorite/1/",
                    objectId: 34
                }
            ]
        };
        var rentalsResponse = {
            count: 1,
            next: null,
            previous: null,
            results: [
                {
                    pageUrl: "http://testserver/rentals/1/",
                    profile: {
                        id: 5,
                        user: {
                            fullName: "Lana Kane"
                        },
                        image: "http://testserver/media/profile_images/lana_kane_kpoFUP4.jpg"
                    },
                    title: "Lana Kane's Single Family Home",
                    nightlyPrice: {
                        price: 466,
                        currencyType: 10
                    },
                    city: "Denver",
                    region: "CO",
                    rentalImage: {
                        image: "http://testserver/media/rental_images/modern-bedroom-inspiration_5554mMH.jpg",
                        isPrimary: true
                    },
                    dateCreated: "2015-05-02T02:56:04.434094Z"
                }
            ]
        };

        beforeEach(inject(function(_$rootScope_, _$httpBackend_, _$controller_, _$window_, _$interval_) {
            var nowDate = new Date();
            var checkInDate = (nowDate.getMonth() + 1) + '%2F' + nowDate.getDate() + '%2F' + nowDate.getFullYear();
            var checkOutDate = (nowDate.getMonth() + 1) + '%2F' + (nowDate.getDate() + 1) + '%2F' + nowDate.getFullYear();
            $httpBackend = _$httpBackend_;
            $httpBackend.expectGET('/api/rental/?checkInDate=' + checkInDate + '&checkOutDate=' + checkOutDate + '&limit=21&minGuests=1&minPrice=0&offset=0&region=CO').respond(rentalsResponse);
            $httpBackend.expectGET('/api/favorite/').respond(favoritesResponse);

            $controller = _$controller_;
            $window = _$window_;
            $interval = _$interval_;
            $scope = _$rootScope_.$new();
            $scope.isInitialized = true;
            controller = $controller('RentalSearchResultsController', {
                $scope: $scope,
                $window: $window
            });
        }));

        it('scope should be defined', function() {
            expect($scope).toBeDefined();
        });

        describe('locations', function(){
            it('for now the locations should be Colorado, Washington State, and Alaska', function(){
                expect($scope.locationDropDownListOptions.dataSource).toBeDefined();
                var locations = ['Colorado', 'Washington State', 'Alaska'];
                for (var i = 0; i < $scope.locations; i++) {
                    expect(locations.indexOf($scope.locations[0].locationName)).toBeGreaterThan(0);
                }
            });
        });

        describe('rentals', function(){
            beforeEach(function(){
                var nowDate = new Date();
                var checkInDate = (nowDate.getMonth() + 1) + '%2F' + nowDate.getDate() + '%2F' + nowDate.getFullYear();
                var checkOutDate = (nowDate.getMonth() + 1) + '%2F' + (nowDate.getDate() + 1) + '%2F' + nowDate.getFullYear();
                $httpBackend.expectGET('/api/rental/?checkInDate=' + checkInDate + '&checkOutDate=' + checkOutDate + '&limit=21&minGuests=1&minPrice=0&offset=0&region=CO').respond(rentalsResponse);
            });
            it('make sure rentals exist', function() {
                expect($scope.rentalResource).toBeDefined();
            });
            it('should fetch the rental from xhr', function() {
                expect($scope.rentalResourcePromise.$resolved).toBeFalsy();
                $httpBackend.flush();
                expect($scope.rentalResource).toEqualData(rentalsResponse);
            });
            it('validate the contents of each rental', function() {
                $httpBackend.flush();

                var results = $scope.rentalResource.results;
                for (var i = 0; i < results.length; i++) {
                    var rental = results[i];
                    expect(rental.nightlyPrice).toBeDefined();
                    expect(rental.nightlyPrice.price).toBeDefined();
                    expect(rental.city).toBeDefined();
                    expect(rental.title).toBeDefined();
                    expect(rental.region).toBeDefined();
                    expect(rental.rentalImage).toBeDefined();
                    expect(rental.rentalImage.image).toBeDefined();
                    expect(rental.profile).toBeDefined();
                }
            });
            it('rental should have a primary image', function(){
                $httpBackend.flush();
                var results = $scope.rentalResource.results;
                for (var i = 0; i < results.length; i++) {
                    var rental = results[i];
                    expect(rental.rentalImage).toBeDefined();
                    expect(rental.rentalImage.image).toMatch('^http');
                }
            });
        });

        describe('price range slider', function(){
            it('initially minimum price should be 0', function() {
                expect($scope.getMinPrice()).toBe(0);
            });
            it('initially minimum price should be 1000', function() {
                expect($scope.getMaxPrice()).toBe(1000);
            });
            it('when the price range slider is slid the price should match the values from Kendo event', function(){
                var value = [300, 500];
                var kendoEvent = {value: value};
                $scope.onPriceRangeSliderSlide(kendoEvent);
                expect($scope.getMinPrice()).toBe(value[0]);
                expect($scope.getMaxPrice()).toBe(value[1]);
            });
        });
    });
});