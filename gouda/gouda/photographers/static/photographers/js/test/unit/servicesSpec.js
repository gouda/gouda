describe('Rental Services', function() {

    beforeEach(angular.mock.module('gouda.listings.apps.ListingApp'));

    it('check the existence of RentalResource factory', inject(function(RentalResource) {
        expect(RentalResource).toBeDefined();
    }));

    it('check the existence of FavoriteResource factory', inject(function(FavoriteResource) {
        expect(FavoriteResource).toBeDefined();
    }));

    it('check the existence of ReviewResource factory', inject(function(ReviewResource) {
        expect(ReviewResource).toBeDefined();
    }));
});
