from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from gouda.celeryapp import app
from django.core import management
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingRequest
from gouda.photographers.models import BookingImage
from gouda.photographers.models import BookingAlbum
from django.utils import timezone
from django.conf import settings
from libthumbor import CryptoURL
from django.conf import settings
import requests
import os
import string
import random
from django.core.files.base import File
import urlparse
import time
from celery import Task
from datetime import timedelta
from gouda.customers.models import BraintreeTransactionStatusType
from celery.utils.log import get_task_logger
from django.template import loader
from django.core.mail import EmailMultiAlternatives
from datetime import datetime
import pytz
from django.core.cache import cache
from psycopg2.extras import DateTimeTZRange
from StringIO import StringIO
import csv

logger = get_task_logger(__name__)

POSTMATES_BOOKING_CHARGE_DELAY = getattr(settings, 'POSTMATES_BOOKING_CHARGE_DELAY')
CLIENT_APPROVAL_TIMEOUT = getattr(settings, 'CLIENT_APPROVAL_TIMEOUT')
THUMBOR_SECURITY_KEY = getattr(settings, 'THUMBOR_SECURITY_KEY')
THUMBOR_SERVER = getattr(settings, 'THUMBOR_SERVER')
RUN_DIR = getattr(settings, 'RUN_DIR')
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')


@app.task
def send_postmates_notification():
    # Send
    context = {
    }
    subject = loader.render_to_string('photographers/emails/postmates_notification_subject.txt', context)
    subject = ''.join(subject.splitlines())
    body = loader.render_to_string('photographers/emails/postmates_notification_body.txt', context)
    email_message = EmailMultiAlternatives(
        subject=subject,
        body=body,
        from_email=settings.DEFAULT_FROM_EMAIL,
        to=settings.STAFF_EMAILS
        # to=['images@postmates.com'],
        # bcc=settings.STAFF_EMAILS
    )

    # Make CSV in memory
    ram_file = StringIO()
    fieldnames = [
        'restaurant_name',
        'start_time',
        'date_created',
        'photographer',
        'contact_notification_date',
        'photographer_completion_date',
        'booking_cancellation_date',
        'staff_approval_date',
        'client_approval_date',
    ]
    csv_writer = csv.DictWriter(ram_file, fieldnames=fieldnames)
    ram_file.write('Restaurant Name,Photoshoot Scheduling,Shotzu Notified,Photographer Assigned,Prep Email Sent,Photoshoot Completed,Photoshoot Canceled,Images Ready,Images Approved\n')
    for booking in FoodBooking.objects.filter(client__user__email_address='images@postmates.com').order_by('-date_created'):
        photographer = booking.photographer.first_name if booking.photographer else ''
        csv_writer.writerow({
            'restaurant_name': booking.restaurant_name,
            'start_time': booking.time_range.lower.isoformat(),
            'date_created': booking.date_created.isoformat(),
            'photographer': photographer,
            'contact_notification_date': booking.contact_notification_date.isoformat() if booking.contact_notification_date else '',
            'photographer_completion_date': booking.photographer_completion_date.isoformat() if booking.photographer_completion_date else '',
            'booking_cancellation_date': '',
            'staff_approval_date': booking.staff_approval_date.isoformat() if booking.staff_approval_date else '',
            'client_approval_date': booking.client_approval_date.isoformat() if booking.client_approval_date else '',
        })
    email_message.attach('daily_report.csv', ram_file.getvalue(), 'text/csv')
    email_message.send()
    ram_file.close()


@app.task
def send_items_notification(food_booking_id):
    food_booking = FoodBooking.objects.get(id=food_booking_id)
    food_booking.send_items_notification()


@app.task
def process_photographer_approved_bookings():
    for booking in Booking.objects.filter(
            braintree_id='',
            did_client_approve=False,
            did_photographer_approve=True,
            did_staff_approve=True,
            staff_approval_date__isnull=False,
            photographer_approval_date__lte=timezone.now() - CLIENT_APPROVAL_TIMEOUT):
        booking.on_client_approval()  # If the client does not approve fast enough...


@app.task
def process_client_approved_bookings():
    i = 0
    for booking in Booking.objects.filter(
            braintree_id='',
            photographer__isnull=False,
            did_client_approve=True,
            did_photographer_approve=True,
            did_staff_approve=True,
            photographer_approval_date__isnull=False,
            staff_approval_date__isnull=False,
            client_approval_date__isnull=False,
            client_approval_date__lte=timezone.now() - POSTMATES_BOOKING_CHARGE_DELAY):
        submit_for_settlement.apply_async(
            countdown=i * (BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS + 5),
            kwargs={'booking_id': booking.id}
        )
        i += 1


@app.task
def smart_crop_booking_image(url, name, width, height, album_id):
    album = BookingAlbum.objects.get(id=album_id)
    thumbor_url = THUMBOR_SERVER + CryptoURL(key=THUMBOR_SECURITY_KEY).generate(
        image_url=url,
        width=width,
        height=height,
        smart=True,
        # filters=['format(png)']
    )

    # Download cropped image
    ext = os.path.splitext(urlparse.urlsplit(url).path)[1]
    response = requests.get(thumbor_url)
    choices = [c for c in string.ascii_uppercase] + [str(i) for i in range(10)]
    temp_file_name = ''.join([random.choice(choices) for i in range(16)])
    temp_file_path = os.path.join(RUN_DIR, temp_file_name + ext)
    with open(temp_file_path, 'wb') as temp_file:
        temp_file.write(response.content)

    # Create booking image
    image = BookingImage.objects.create(**{
        'booking': album.booking,
        'name': name,
        'image': File(open(temp_file_path))
    })
    album.images.add(image)
    os.remove(temp_file_path)
    # print(url)
    # print(image.image.url)

    return image


@app.task
def on_staff_approval(booking_id):
    booking = Booking.objects.get(id=booking_id)
    booking.on_staff_approval()


class UpdateBookingsTask(Task):
    ignore_result = True

    def run(self, *args, **kwargs):
        for braintree_transaction in Booking.objects.exclude(braintree_id__exact='')\
                .filter(transaction_status=BraintreeTransactionStatusType.NONE)\
                .filter(transaction_status=BraintreeTransactionStatusType.SETTLED):
            braintree_transaction.update_braintree_data()


@app.task
def submit_for_settlement(booking_id):
    booking = Booking.objects.get(id=booking_id)
    if booking.braintree_id == '':
        booking.submit_for_settlement()
    else:
        logger.error('Cannot submit booking (%d) for settlement' % booking_id)


@app.task
def send_photographer_completion():
    cutoff_date = datetime(
        year=2017,
        month=7,
        day=24,
        tzinfo=pytz.utc
    )
    cutoff_range = DateTimeTZRange(lower=cutoff_date, upper=cutoff_date + timedelta(hours=1))
    for booking in Booking.objects.exclude(time_range__isnull=True, photographer__isnull=True).filter(time_range__gt=cutoff_range, did_photographer_complete=False):
        key = 'gouda.photographers.tasks.send_photographer_completion:%d' % booking.id
        if cutoff_date < booking.time_range.upper < timezone.now() and not cache.get(key):
            booking.send_photographer_completion()
            cache.set(key, True, 60 * 60 * 24)
