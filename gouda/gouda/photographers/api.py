from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import django_filters
from django.db.models import Q
from drf_extra_fields import fields
from gouda.photographers.models import FoodBooking
from gouda.photographers.models import Booking
from gouda.photographers.models import BookingImage
from rest_framework import filters
from rest_framework import serializers
from rest_framework import viewsets
from rest_framework import pagination
from rest_framework.permissions import IsAuthenticated
from rest_framework.exceptions import PermissionDenied
from easy_thumbnails.files import get_thumbnailer
from rest_framework import mixins
from rest_framework import status
from gouda.photographers.tasks import send_items_notification
from django.core.cache import cache
from gouda.photographers.models import BookingAlbum
import logging
import base64
import imghdr
import uuid
from django.core.exceptions import ValidationError
from django.core.files.base import ContentFile
from django.utils.translation import ugettext_lazy as _
from easy_thumbnails.exceptions import InvalidImageFormatError
from rest_framework.fields import ImageField
from django.contrib.staticfiles.templatetags.staticfiles import static
from rest_framework.decorators import detail_route, list_route
from rest_framework.response import Response
from gouda.photographers.tasks import smart_crop_booking_image
from libthumbor import CryptoURL
from django.conf import settings
import requests
from django.core.files.base import File
import os
import string
import cStringIO
import random
from PIL import Image
from gouda.thumbor.utils import generate_url

logger = logging.getLogger(__name__)

ALLOWED_IMAGE_TYPES = (
    "jpeg",
    "jpg",
    "png",
    "gif",
    'tif',
    'tiff'
)
EMPTY_VALUES = (None, '', [], (), {})


class LargePagination(pagination.LimitOffsetPagination):
    default_limit = 9000


class BookingImageFilter(filters.FilterSet):
    booking = django_filters.NumberFilter(name='booking__id')

    class Meta:
        model = BookingImage
        fields = ['booking']


class BookingAlbumFilter(filters.FilterSet):
    booking = django_filters.NumberFilter(name='booking__id')

    class Meta:
        model = BookingAlbum
        fields = ['booking']


class Base64ImageField(fields.Base64ImageField):
    def to_internal_value(self, base64_data):
        # Check if this is a base64 string
        if base64_data in EMPTY_VALUES:
            return None

        if isinstance(base64_data, basestring):
            # Try to decode the file. Return validation error if it fails.
            try:
                decoded_file = base64.b64decode(base64_data)
            except TypeError:
                raise ValidationError(_("Please upload a valid image."))
            # Generate file name:
            file_name = str(uuid.uuid4())[:12]  # 12 characters are more than enough.
            # Get the file name extension:
            file_extension = self.get_file_extension(file_name, decoded_file)
            if file_extension not in ALLOWED_IMAGE_TYPES:
                print('WTF... %s' % file_name)
                raise ValidationError(_("The type of the image couldn't been determined."))
            complete_file_name = file_name + "." + file_extension
            data = ContentFile(decoded_file, name=complete_file_name)
            return super(ImageField, self).to_internal_value(data)
        raise ValidationError(_('This is not an base64 string'))

    def get_file_extension(self, filename, decoded_file):
        buffer = cStringIO.StringIO(decoded_file)
        image = Image.open(buffer)
        extension = image.format
        if extension:
            extension = extension.lower()
            extension = "jpg" if extension == "jpeg" else extension
        return extension

    def to_representation(self, value):
        return value.url


class BookingImageField(Base64ImageField):
    def to_representation(self, value):
        thumbor_url = generate_url(
            value.url,
            width=500,
        )
        metadata_url = generate_url(
            value.url,
            meta=True,
            width=500,
        )
        try:
            thumbnail_url = thumbor_url
        except KeyError:
            thumbnail_url = static('img/photo_placeholder.png')
        except InvalidImageFormatError:
            thumbnail_url = static('img/photo_placeholder.png')
        return {
            'url': self.context.get('request').build_absolute_uri(value.url),
            'thumbnail_url': self.context.get('request').build_absolute_uri(thumbnail_url),
            'metadata_url': self.context.get('request').build_absolute_uri(metadata_url)
        }


class BookingSmartCropImageSerializer(serializers.Serializer):
    album = serializers.PrimaryKeyRelatedField(queryset=BookingAlbum.objects.all())
    image_url = serializers.URLField()
    name = serializers.CharField()
    width = serializers.IntegerField()
    height = serializers.IntegerField()

    def create(self, validated_data):
        kwargs = {
            'album_id': validated_data.get('album').id,
            'url': validated_data.get('image_url'),
            'name': validated_data.get('name'),
            'width': validated_data.get('width'),
            'height': validated_data.get('height'),
        }
        smart_crop_booking_image.delay(**kwargs)
        return None


class BookingImageSerializer(serializers.HyperlinkedModelSerializer):
    image = BookingImageField(required=True)
    booking = serializers.PrimaryKeyRelatedField(queryset=Booking.objects.all())

    def validate(self, data):
        if 'did_client_reject' in data and 'did_client_approve' not in data:
            raise serializers.ValidationError('Missing did_client_approve')
        if 'did_client_approve' in data and 'did_client_reject' not in data:
            raise serializers.ValidationError('Missing did_client_reject')
        if data.get('did_client_approve') is True and data.get('did_client_reject') is True:
            raise serializers.ValidationError('Image cannot be both approved and rejected')
        return data

    class Meta:
        model = BookingImage
        fields = (
            'id',
            'image',
            'url',
            'booking',
            'name',
            'did_client_approve',
            'did_client_reject',
            'comment',
            'date_created'
        )
        extra_kwargs = {
            'url': {
                'view_name': 'api_photographers:booking_image_detail',
                'lookup_field': 'pk'
            }
        }


class BookingImageViewSet(viewsets.ModelViewSet):
    serializer_class = BookingImageSerializer
    filter_backends = (filters.DjangoFilterBackend, )
    filter_class = BookingImageFilter
    permission_classes = [IsAuthenticated]
    pagination_class = LargePagination

    def perform_update(self, serializer):
        super(BookingImageViewSet, self).perform_update(serializer)
        instance = self.get_object()
        if serializer.data.get('did_client_reject') is True:
            if instance.booking.photographer:
                instance.send_rejection_notification(self.request.user.profile)
        if 'comment' in serializer.data and serializer.data.get('comment'):
            if instance.booking.photographer:
                instance.send_comment_notification(self.request.user.profile)
        return instance

    def create(self, request, *args, **kwargs):
        if 'booking' in request.query_params and 'booking' not in request.data:
            request.data['booking'] = request.query_params.get('booking')
        return super(BookingImageViewSet, self).create(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.is_staff:
            return BookingImage.objects.all()
        else:
            return BookingImage.objects.filter(
                Q(booking__photographer__profile=self.request.user.profile) |
                Q(booking__client=self.request.user.profile)).order_by('-date_created')


class BookingAlbumSerializer(serializers.HyperlinkedModelSerializer):
    images = BookingImageSerializer(many=True, required=False)
    booking = serializers.PrimaryKeyRelatedField(queryset=Booking.objects.all())

    def update(self, instance, validated_data):
        if 'images' in validated_data:
            for data in validated_data.get('images'):
                if 'id' not in data:
                    booking_image = BookingImage.objects.create(**data)
                    instance.images.add(booking_image)
            validated_data.pop('images')
        return super(BookingAlbumSerializer, self).update(instance, validated_data)

    class Meta:
        model = BookingAlbum
        fields = (
            'id',
            'url',
            'images',
            'booking',
            'name',
            'width',
            'height',
            'date_created'
        )
        extra_kwargs = {
            'url': {
                'view_name': 'api_photographers:booking_album_detail',
                'lookup_field': 'pk'
            }
        }


class BookingAlbumViewSet(viewsets.ModelViewSet):
    serializer_class = BookingAlbumSerializer
    filter_backends = (filters.DjangoFilterBackend, )
    filter_class = BookingAlbumFilter
    permission_classes = [IsAuthenticated]
    pagination_class = LargePagination

    @detail_route(methods=['patch'])
    def smart_crop(self, request, pk=None):
        album = self.get_object()
        serializer = BookingSmartCropImageSerializer(data=request.data.get('images'), many=True, context={
            'request': request
        })
        serializer.is_valid(raise_exception=True)
        serializer.save()
        return Response(self.get_serializer(album, partial=True).data)

    def create(self, request, *args, **kwargs):
        if 'booking' in request.query_params and 'booking' not in request.data:
            request.data['booking'] = request.query_params.get('booking')
        return super(BookingAlbumViewSet, self).create(request, *args, **kwargs)

    def get_queryset(self):
        if self.request.user.is_staff:
            return BookingAlbum.objects.all()
        else:
            return BookingAlbum.objects.filter(Q(booking__photographer__profile=self.request.user.profile) | Q(booking__client=self.request.user.profile))


class FoodBookingSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = FoodBooking
        fields = (
            'id',
            'items'
        )
        extra_kwargs = {
            'url': {
                'view_name': 'api_photographers:booking_detail',
                'lookup_field': 'pk'
            }
        }


class FoodBookingViewSet(mixins.UpdateModelMixin,
                         mixins.RetrieveModelMixin,
                         viewsets.GenericViewSet):
    serializer_class = FoodBookingSerializer
    permission_classes = [IsAuthenticated]

    def update(self, request, *args, **kwargs):
        response = super(FoodBookingViewSet, self).update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            if 'items' in request.data:
                key = 'food_booking:%s:items_notification' % kwargs.get('pk')
                if not cache.get(key, None):
                    cache.set(key, True, 30 * 15)
                    send_items_notification.apply_async(kwargs={'food_booking_id': kwargs.get('pk')}, countdown=15 * 60)
        return response

    def get_queryset(self):
        if self.request.user.is_staff:
            return FoodBooking.objects.all()
        else:
            return FoodBooking.objects.filter(Q(photographer__profile=self.request.user.profile) | Q(client=self.request.user.profile)).order_by('-date_created')
