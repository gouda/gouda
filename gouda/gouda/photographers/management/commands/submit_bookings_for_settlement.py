# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging
import time

from django.conf import settings
from django.core.management.base import BaseCommand
from gouda.photographers.models import Booking

IS_PRODUCTION = getattr(settings, 'IS_PRODUCTION', False)
BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS = getattr(settings, 'BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS')
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('-p', '--pk', action='append', type=int, dest='pks')

    def handle(self, *args, **options):
        for pk in options.get('pks'):
            booking = Booking.objects.get(pk=pk)
            assert booking.is_sub_merchant_transaction
            assert not booking.braintree_id
            assert booking.photographer
            assert booking.did_photographer_approve
            assert booking.did_staff_approve
            assert booking.did_client_approve
            assert booking.photographer_approval_date
            assert booking.staff_approval_date
            assert booking.client_approval_date
            assert booking.num_shots

            booking.submit_for_settlement()
            self.stdout.write('Submitted $%.2f ($%.2f) for settlement' % (booking.amount, booking.service_fee_amount))
            time.sleep(BRAINTREE_DUPLICATE_TRANSACTION_TIMEOUT_SECONDS + 5)
