# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

from datetime import date
from datetime import timedelta

from django.core.management.base import BaseCommand
from gouda.customers.utils import create_customers
from gouda.photographers.utils import create_bookings
from gouda.photographers.utils import create_food_bookings
from gouda.photographers.utils import FIXTURES_DIR
from gouda.photographers.utils import create_booking_images
from gouda.photographers.utils import create_photographers
from gouda.photographers.models import Photographer
from gouda.users.models import User
from gouda.customers.models import BraintreeCustomer
from gouda.customers.models import BraintreeMerchantAccount
from gouda.customers.models import BraintreeMerchantAccountStatusType
from django.conf import settings
from braintree.test.nonces import Nonces
import random
from django.core.files.base import ContentFile
from gouda.photographers.models import BulkBooking
import logging
import os

IS_PRODUCTION = getattr(settings, 'IS_PRODUCTION', False)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('num_photographers', type=int)

    def handle(self, *args, **options):
        assert not IS_PRODUCTION
        logger.info('Creating photographers...')
        photographers = create_photographers(
            options.get('num_photographers'),
            do_create_marketplace=True,
            is_superuser=True
        )

        profile = random.choice(photographers).profile
        csv_file = ContentFile(open(os.path.join(FIXTURES_DIR, 'booking_files/corrected_postmates.csv')).read(), name='corrected_postmates.csv')

        bulk_booking = BulkBooking.objects.create(**{
            'client': profile,
            'file': csv_file
        })
        logger.info('Creating bookings...')
        bulk_booking.create_bookings_from_file()

        # Correct the address
        logger.info('Geocoding addresses...')
        for booking in bulk_booking.bookings.all():
            geocode_result = booking.address.geocode()
            logger.info(geocode_result)

        # Images
        logger.info('Creating images...')
        for booking in bulk_booking.bookings.all():
            booking.photographer = random.choice(Photographer.objects.all())
            booking.client = random.choice(Photographer.objects.all()).profile
            booking.save()
            self.stdout.write(create_booking_images(booking, num_albums=2).__str__())


        # create_food_bookings(5, do_create_images=False, do_create_messages=False, do_assign_photographers=False)
        # create_food_bookings(200, do_create_images=True, do_create_messages=True)
        # create_bookings(100, do_create_images=True, do_create_messages=True)

        # for user in User.objects.all():
        #     BraintreeCustomer.objects.update_or_create_braintree_customer(user, Nonces.Transactable)
        #
        #     individual = {
        #         'locality': 'Bakersfield',
        #         'postal_code': '93311',
        #         'region': 'CA',
        #         'street_address': '1504 Parkpath Way',
        #         'date_of_birth': date.today() - timedelta(days=365 * 28),
        #         'email': user.email_address,
        #         'first_name': user.first_name,
        #         'last_name': user.last_name,
        #         'phone': '4437421240'
        #     }
        #     funding = {
        #         'email': user.email_address,
        #         'mobile_phone': '5555555555',
        #         'account_number': '1123581321',
        #         'routing_number': '071101307',
        #     }
        #     braintree_merchant_account, is_created = BraintreeMerchantAccount.objects.update_or_create_braintree_merchant_account(
        #         user=user,
        #         tos_accepted=True,
        #         individual=individual,
        #         funding=funding
        #     )
        #     braintree_merchant_account.status = BraintreeMerchantAccountStatusType.ACTIVE
        #     braintree_merchant_account.save()
