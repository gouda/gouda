# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand
from gouda.photographers.models import BulkBooking
from gouda.profiles.models import Profile
import os

IS_PRODUCTION = getattr(settings, 'IS_PRODUCTION', False)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('client_email_address', type=str)
        parser.add_argument('csv_file_path', type=str)

    def handle(self, *args, **options):
        profile = Profile.objects.get(user__email_address=options.get('client_email_address'))
        csv_file = ContentFile(open(options.get('csv_file_path')).read(), name=os.path.basename(options.get('csv_file_path')))

        bulk_booking = BulkBooking.objects.create(**{
            'client': profile,
            'file': csv_file
        })

        field_names = (
            'restaurant_name',
            'contact_email_address',
            'contact_phone_number',
            'contact_full_name',
            'street_address',
            'locality',
            'region',
            'postal_code',
            'photographer',
            'appointment_time',
            'num_images',
        )
        bookings = bulk_booking.create_bookings_from_file(field_names=field_names)
        for booking in bookings:
            assert booking.foodbooking
            assert booking.foodbooking.restaurant_name
            # assert booking.foodbooking.menu_url
            assert booking.contact_email_address
            assert booking.contact_name, 'Failed to find contact name for %s' % booking.foodbooking.restaurant_name
            self.stdout.write(booking.__unicode__())

        # Correct the address
        for booking in bulk_booking.bookings.all():
            geocode_result = booking.address.geocode()
            assert geocode_result
            assert len(geocode_result) > 0
            assert booking.address.formatted
            self.stdout.write(geocode_result.__str__())
