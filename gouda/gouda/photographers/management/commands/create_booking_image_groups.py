# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.conf import settings
from django.core.management.base import BaseCommand

from gouda.photographers.models import Booking
from gouda.photographers.models import BookingAlbum
from gouda.photographers.models import BookingImage

IS_PRODUCTION = getattr(settings, 'IS_PRODUCTION', False)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def handle(self, *args, **options):
        for booking in Booking.objects.all():
            if not BookingAlbum.objects.filter(booking=booking).exists():
                booking_image_group = BookingAlbum.objects.create(**{
                    'name': 'Original',
                    'booking': booking
                })
                for booking_image in BookingImage.objects.filter(booking=booking):
                    booking_image_group.images.add(booking_image)
