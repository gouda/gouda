# -*- coding: utf-8 -*-

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from __future__ import unicode_literals

import logging

from django.conf import settings
from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand
from gouda.photographers.models import BulkBooking
from gouda.profiles.models import Profile
from gouda.photographers.models import Booking
from gouda.photographers.models import FoodBooking

import os

IS_PRODUCTION = getattr(settings, 'IS_PRODUCTION', False)
logger = logging.getLogger(__name__)


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('client_email_address', type=str)
        parser.add_argument('csv_file_path', type=str)

    def handle(self, *args, **options):
        profile = Profile.objects.get(user__email_address=options.get('client_email_address'))
        csv_file = ContentFile(open(options.get('csv_file_path')).read(), name=os.path.basename(options.get('csv_file_path')))

        num_bulk_bookings = BulkBooking.objects.all().count()
        bulk_booking = BulkBooking(**{
            'client': profile,
            'file': csv_file
        })
        assert BulkBooking.objects.all().count() == num_bulk_bookings

        booking_data = bulk_booking.get_booking_data_from_file()
        num_bookings = Booking.objects.all().count()
        for b in booking_data:
            filter_kwargs = {
                'client': profile,
                'restaurant_name': b.get('restaurant_name'),
                'contact_phone_number': b.get('contact').get('phone_number'),
                'contact_email_address': b.get('contact').get('email_address'),
                'contact_name': b.get('contact').get('full_name'),
                'address__full_name': b.get('restaurant_name'),
                'address__street_address': b.get('address').get('street_address'),
                'address__sub_premise': b.get('address').get('sub_premise'),
                'address__postal_code': b.get('address').get('postal_code'),
                'address__locality': b.get('address').get('locality'),
                'address__region': b.get('address').get('region'),
            }
            self.stdout.write(b.__str__())
            food_bookings = FoodBooking.objects.filter(**filter_kwargs)
            num_food_bookings = food_bookings.count()
            assert num_food_bookings > 0
            assert food_bookings.update(menu_url=b.get('menu_url')) == num_food_bookings
            assert num_bookings == Booking.objects.all().count()
