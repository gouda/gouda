import argparse
import requests
import json
from datetime import timedelta
from datetime import datetime
import logging
import time

logger = logging.getLogger('clean_index')
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
stream_handler = logging.StreamHandler()
stream_handler.setFormatter(formatter)
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)

if __name__ == "__main__":
    argument_parser = argparse.ArgumentParser(description='Remove old indices from Elasticsearch')
    argument_parser.add_argument('-p', '--pattern', dest='pattern', required=True, action='store', help='Pattern to match index')
    argument_parser.add_argument(
        '-a',
        '--age',
        default=timedelta(days=30).total_seconds(),
        action='store', help='Age in seconds'
    )
    argument_parser.add_argument(
        '-e',
        '--elasticsearch-host',
        dest='elasticsearch_host',
        action='store',
        default='http://elasticsearch:9200',
        help='Elasticsearch host'
    )

    args = argument_parser.parse_args()

    while True:
        response = requests.get(args.elasticsearch_host + '/%s/' % args.pattern)
        for key, value in response.json().items():
            creation_date = datetime.fromtimestamp(float(value.get('settings').get('index').get('creation_date')) / 1000.0)
            if creation_date < (datetime.utcnow() - timedelta(seconds=args.age)):
                # Delete!
                response = requests.delete(args.elasticsearch_host + '/%s/' % key)
                logger.info('Deleting %s' % key)
            else:
                logger.debug('Ignoring "%s". Deletion date is %s' % (key, (creation_date + timedelta(seconds=args.age)).isoformat()))
        time.sleep(60)
