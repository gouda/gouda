# nginx-gouda.conf

# the upstream component nginx needs to connect to
upstream django {
    server unix:///var/run/gunicorn/gouda.sock; # for a file socket
}

# configuration of the server
server {
    listen localhost:80;

}
server {
    listen         80;
    charset     utf-8;
    server_name shotzu.com www.shotzu.com;

    location / {
        return 301 https://shotzu.com$request_uri;
    }
}

server {
    listen      443 ssl;
    charset     utf-8;
    server_name www.shotzu.com;
    ssl_certificate /etc/nginx/ssl/shotzu.com.chained.crt;
    ssl_certificate_key /etc/nginx/ssl/shotzu.com.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers AES256+EECDH:AES256+EDH:!aNULL;
    return 301 https://shotzu.com$request_uri;
}

server {
    access_log /var/log/nginx/access.log timed_combined;
    error_log /var/log/nginx/error.log info;
    listen 443 ssl;
    charset utf-8;

    server_name shotzu.com;
    ssl_certificate /etc/nginx/ssl/shotzu.com.chained.crt;
    ssl_certificate_key /etc/nginx/ssl/shotzu.com.key;
    ssl_protocols TLSv1 TLSv1.1 TLSv1.2;
    ssl_prefer_server_ciphers on;
    ssl_ciphers AES256+EECDH:AES256+EDH:!aNULL;

    # max upload size
    client_max_body_size 120M;   # adjust to taste

    # Django media
    location /media  {
        alias /var/run/gouda/media;  # your Django project's media files - amend as required
    }

    location /static {
        alias /var/run/gouda/static; # your Django project's static files - amend as required
    }

    # Finally, send all non-media requests to the Django server.
    location / {
        uwsgi_pass  django;
        include     /opt/gouda/deploy/compose/services/web/uwsgi/uwsgi_params; # the uwsgi_params file you installed
    }

}
