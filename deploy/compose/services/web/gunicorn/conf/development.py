bind = 'unix:///var/run/gunicorn/gouda.sock'
workers = 1
max_requests = 1
max_requests_jitter = 25

reload = True
