#!/bin/bash
mkdir -p /var/log/celery/
touch /var/log/celery/beat.log
celery -A gouda --logfile=/var/log/celery/beat.log --pidfile= -l info beat