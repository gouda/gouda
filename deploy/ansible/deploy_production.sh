#!/usr/bin/env bash

OPTIND=1
VERSION=""
BRANCH_NAME=""

while getopts "v:b:" o; do
    case "${o}" in
    v)  VERSION=${OPTARG}
        ;;
    b)  BRANCH_NAME=${OPTARG}
        ;;
    esac
done

shift $((OPTIND-1))

echo "VERSION = ${VERSION}"
echo "BRANCH_NAME = ${BRANCH_NAME}"
echo "Deploying to production environment..."

ansible-playbook --extra-vars "version=${VERSION} branch_name=${BRANCH_NAME} is_production_environment=True hosts=production" --vault-password-file /home/vagrant/.vault_pass.txt build.yml
ansible-playbook --extra-vars "version=${VERSION} branch_name=${BRANCH_NAME} is_production_environment=True hosts=production" --vault-password-file /home/vagrant/.vault_pass.txt deploy.yml
