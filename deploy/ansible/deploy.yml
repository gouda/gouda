---
# Install Python 2 on Droplet
- hosts: '{{ hosts }}'
  gather_facts: no
  tasks:
    - name: Install Python2
      raw: sudo apt-get -y install python-simplejson

# Install app
- hosts: '{{ hosts }}'
  vars:
    staticfiles_version: "{{ lookup('file', '{{ working_directory }}/.staticfiles_version') }}"
  environment:
    VERSION: "{{ version }}"
  pre_tasks:
    - include_vars: vault.yml
    - name: Set facts for test environment
      when: "{{ not(is_production_environment | bool) }}"
      set_fact:
        compose_file_name: "test.yml"
    - name: Set facts for production environment
      when: "{{ is_production_environment | bool }}"
      set_fact:
        compose_file_name: "production.yml"
  roles:
    - common
    - { role: docker, user: 'root'}
  post_tasks:
    - name: Backup database
      ignore_errors: yes
      shell: "docker-compose -f common.yml -f {{ compose_file_name }} -p {{ app_name }} run worker ./manage.py backup_postgres"
      args:
        chdir: "{{ app_directory }}"
    - name: Add user
      user:
        name: "{{ user.username }}"
        password: "{{ user.password }}"
        groups: "sudo,docker"
    - name: Install CA certificates
      notify:
        - "restart docker service"
        - "restart {{ app_name }} service"
      synchronize:
        src: "{{ working_directory }}/deploy/ssl/ca/blhackers.com.ca.crt"
        dest: "/usr/local/share/ca-certificates/blhackers.com.ca.crt"
      register: install_ca_certificates_result
    - name: Update CA certificates
      when: install_ca_certificates_result.changed
      shell: /usr/sbin/update-ca-certificates
    - name: Setup firewall
      ufw:
        rule: allow
        port: "{{ item }}"
        proto: tcp
        state: enabled
        direction: in
      with_items:
        - ssh
        - 80
        - 443
        - 8888
        - 8889
    - name: Prevent root from logging in with a password
      notify:
        - "restart sshd service"
      lineinfile:
        dest: /etc/ssh/sshd_config
        regexp: ^PermitRootLogin
        state: present
        line: PermitRootLogin without-password
    - name: Log into private registry and force re-authorization
      docker_login:
        registry: https://registry.gitlab.com
        username: "{{ registry.username }}"
        password: "{{ registry.password }}"
        reauthorize: yes
    - name: Create app directory
      file:
        path: "{{ app_directory }}"
        state: directory
        owner: "{{ ansible_user }}"
    - name: Install compose files
      notify:
        - "restart gouda service"
      synchronize:
        src: "{{ working_directory }}/deploy/compose/"
        dest: "{{ app_directory }}/"
    - name: Update environment file
      template:
        src: "{{ working_directory }}/deploy/compose/env/common.env.j2"
        dest: "{{ app_directory }}/.env"
      notify:
        - "restart gouda service"
    - name: Pull images
      shell: docker-compose -p {{ app_name }}
        -f {{ app_directory }}/common.yml
        -f {{ app_directory }}/test.yml
        pull
      args:
        chdir: "{{ app_directory }}"
    - name: Install newest version of gouda service
      template:
        src: "{{ working_directory }}/deploy/systemd/{{ app_name }}.service.j2"
        dest: /etc/systemd/system/{{ app_name }}-{{ version }}.service
      notify:
        - "reload systemctl daemon"
        - "restart gouda service"
    - name: Link gouda service
      file:
        src: /etc/systemd/system/{{ app_name }}-{{ version }}.service
        dest: /etc/systemd/system/{{ app_name }}.service
        state: link
        force: yes
      notify:
        - "reload systemctl daemon"
        - "restart gouda service"
  handlers:
    - name: Restart SSH
      listen: "restart sshd service"
      service:
        name: "sshd"
        state: restarted
        enabled: yes
    - name: Reload systemctl daemon
      shell: systemctl daemon-reload
      listen: "reload systemctl daemon"
    - name: Restart docker
      service:
        name: "docker"
        state: restarted
        enabled: yes
      listen: "restart docker service"
    - name: Restart gouda
      service:
        name: "{{ app_name }}"
        state: restarted
        enabled: yes
      listen: "restart gouda service"
