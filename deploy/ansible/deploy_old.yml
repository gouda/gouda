---
# Build app
- hosts: local
  become: true
  vars:
    app_name: gouda
    source_directory: /vagrant/
    working_directory: "/staging/{{ app_name }}"
    public_key_file: "/tmp/ansible/{{ app_name }}/.ssh/id_rsa.pub"
    private_key_file: "/tmp/ansible/{{ app_name }}/.ssh/id_rsa"
  environment:
    VERSION: "{{ version }}"
  pre_tasks:
    - include_vars: vault.yml
    - name: Set facts for test environment
      when: "{{ not(is_production_environment | bool) }}"
      set_fact:
        compose_file_name: "test.yml"
        statics_build_service: "test"
    - name: Set facts for production environment
      when: "{{ is_production_environment | bool }}"
      set_fact:
        compose_file_name: "production.yml"
        statics_build_service: "production"
  tasks:
    - name: Ensure .ssh directory exists.
      file:
        dest: "{{ private_key_file | dirname }}"
        mode: 0777
        owner: "{{ ansible_user }}"
        state: directory
    - name: Install SSH public key
      copy:
        content: "{{ ssh.public_key }}"
        dest: "{{ public_key_file }}"
        mode: 0600
        owner: "{{ ansible_user }}"
    - name: Install SSH private key
      copy:
        content: "{{ ssh.private_key }}"
        dest: "{{ private_key_file }}"
        mode: 0600
        owner: "{{ ansible_user }}"
#
#    - name: Create a staging area
#      file:
#        path: "{{ working_directory }}"
#        state: directory
#        owner: "{{ ansible_user }}"
#    - name: Copy app files to staging area
#      synchronize:
#        src: "{{ source_directory }}"
#        dest: "{{ working_directory }}"
#        partial: yes
#        delete: yes
#    - name: Check out the branch
#      git:
#        repo: "{{ working_directory }}/.git"
#        clone: no
#        version: "{{ branch_name }}"
#
#    - name: Generate version for static files
#      shell: cat /dev/urandom | head | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1
#      register: staticfiles_version
#    - name: Set fact for staticfiles_version
#      set_fact:
#        staticfiles_version: "{{ staticfiles_version.stdout_lines[0] }}"
#    - name: Write out staticfiles_version
#      copy:
#        content: "{{ staticfiles_version }}"
#        dest: "{{ working_directory }}/.staticfiles_version"
#    - debug: msg="staticfiles_version = {{ staticfiles_version }}"
#
#    - name: Kill containers
#      shell: docker-compose -p build{{ app_name }}
#        -f {{ working_directory }}/deploy/compose/common.yml
#        -f {{ working_directory }}/deploy/compose/build_statics.yml
#        kill
#      args:
#        chdir: "{{ working_directory }}"
#    - name: Remove containers
#      shell: docker-compose -p build{{ app_name }}
#        -f {{ working_directory }}/deploy/compose/common.yml
#        -f {{ working_directory }}/deploy/compose/build_statics.yml
#        rm -f --all
#      args:
#        chdir: "{{ working_directory }}"
#
#    - name: Build static files
#      shell: docker-compose -p build{{ app_name }}
#        -f {{ working_directory }}/deploy/compose/common.yml
#        -f {{ working_directory }}/deploy/compose/build_statics.yml
#        run -e STATICFILES_VERSION={{ staticfiles_version }} {{ statics_build_service }} gulp dist --httpPath /static/{{ staticfiles_version }}
#      args:
#        chdir: "{{ working_directory }}"
#    - name: Collect static files
#      shell: docker-compose -p build{{ app_name }}
#        -f {{ working_directory }}/deploy/compose/common.yml
#        -f {{ working_directory }}/deploy/compose/build_statics.yml
#        run -e STATICFILES_VERSION={{ staticfiles_version }} {{ statics_build_service }} ./manage.py collectstatic --noinput -i *.less -i *.scss -i *.sass -i *.map -i *.md
#      args:
#        chdir: "{{ working_directory }}"
#    - name: Compress static files
#      shell: docker-compose -p build{{ app_name }}
#        -f {{ working_directory }}/deploy/compose/common.yml
#        -f {{ working_directory }}/deploy/compose/build_statics.yml
#        run -e STATICFILES_VERSION={{ staticfiles_version }} {{ statics_build_service }} ./manage.py compress --force
#      args:
#        chdir: "{{ working_directory }}"
#
#    - name: Build images
#      shell: docker-compose -p build{{ app_name }} -f {{ working_directory }}/deploy/compose/build.yml build
#      args:
#        chdir: "{{ working_directory }}"
#    - name: Push images
#      shell: docker push registry.gitlab.com/gouda/{{ item }}:{{ version }}
#      with_items:
#        - redis
#        - rabbitmq
#        - postgres
#        - haproxy
#        - gouda
#        - thumbor
#      become: false
#      async: 9000
#      poll: 5

# Create a Droplet
- hosts: local
  vars:
    app_name: gouda
    ssh_key_name: "MacPro Vagrant SSH Key"
    digital_ocean_api_token: "edd49018f5de01646212a86f1e731f3673741509b57c6dc34bf0272ebb7b9cd4"
    droplet_user: 'root'
    droplet_name: "{{ droplet_name }}"
    droplet_size_id: 2gb
    droplet_region_id: nyc2
    droplet_image_id: ubuntu-16-04-x64
    ssh_public_key_file_path: "/tmp/ansible/{{ app_name }}/.ssh/id_rsa.pub"
    ssh_private_key_file_path: "/tmp/ansible/{{ app_name }}/.ssh/id_rsa"
  become: true
  pre_tasks:
    - include_vars: vault.yml
  roles:
    - { role: create_droplet }
  post_tasks:
    - debug: msg="Droplet is at {{ droplet.droplet.ip_address }}"
    - name: Add Droplet to inventory
      add_host:
        hostname: "{{ droplet.droplet.ip_address }}"
        groups: droplets
        ansible_user: root
        ansible_ssh_private_key_file: '{{ ssh_private_key_file_path }}'
        ansible_connection: ssh

# Install Python 2 on Droplet
- hosts: droplets
  gather_facts: no
  tasks:
    - name: Install Python2
      raw: sudo apt-get -y install python-simplejson

# Install app
- hosts: droplets
  vars:
    app_name: 'gouda'
    working_directory: "/staging/{{ app_name }}"
    app_directory: /opt/{{ app_name }}
    staticfiles_version: "{{ lookup('file', '{{ working_directory }}/.staticfiles_version') }}"
  environment:
    VERSION: "{{ version }}"
  pre_tasks:
    - include_vars: vault.yml
    - name: Set facts for test environment
      when: "{{ not(is_production_environment | bool) }}"
      set_fact:
        compose_file_name: "test.yml"
    - name: Set facts for production environment
      when: "{{ is_production_environment | bool }}"
      set_fact:
        compose_file_name: "production.yml"
  roles:
    - common
    - { role: docker, user: 'root'}
  post_tasks:
    - name: Add user
      user:
        name: "{{ user.username }}"
        password: "{{ user.password }}"
        groups: "sudo,docker"
    - name: Install CA certificates
      notify:
        - "restart docker service"
        - "restart {{ app_name }} service"
      synchronize:
        src: "{{ working_directory }}/deploy/ssl/ca/blhackers.com.ca.crt"
        dest: "/usr/local/share/ca-certificates/blhackers.com.ca.crt"
      register: install_ca_certificates_result
    - name: Update CA certificates
      when: install_ca_certificates_result.changed
      shell: /usr/sbin/update-ca-certificates
    - name: Setup firewall
      ufw:
        rule: allow
        port: "{{ item }}"
        proto: tcp
        state: enabled
        direction: in
      with_items:
        - ssh
        - 80
        - 443
        - 8888
        - 8889
    - name: Prevent root from logging in with a password
      notify:
        - "restart sshd service"
      lineinfile:
        dest: /etc/ssh/sshd_config
        regexp: ^PermitRootLogin
        state: present
        line: PermitRootLogin without-password
    - name: Log into private registry and force re-authorization
      docker_login:
        registry: https://registry.gitlab.com
        username: "{{ registry.username }}"
        password: "{{ registry.password }}"
        reauthorize: yes
    - name: Create app directory
      file:
        path: "{{ app_directory }}"
        state: directory
        owner: "{{ ansible_user }}"
    - name: Backup database
      ignore_errors: yes
      shell: "docker-compose -f common.yml -f {{ compose_file_name }} -p {{ app_name }} run worker ./manage.py backup_postgres"
      args:
        chdir: "{{ app_directory }}"
    - name: Install compose files
      notify:
        - "restart gouda service"
      synchronize:
        src: "{{ working_directory }}/deploy/compose/"
        dest: "{{ app_directory }}/"
    - name: Update environment file
      template:
        src: "{{ working_directory }}/deploy/compose/env/common.env.j2"
        dest: "{{ app_directory }}/.env"
      notify:
        - "restart gouda service"
    - name: Pull images
      shell: docker-compose -p {{ app_name }}
        -f {{ app_directory }}/common.yml
        -f {{ app_directory }}/test.yml
        pull
      args:
        chdir: "{{ app_directory }}"
    - name: Install newest version of gouda service
      template:
        src: "{{ working_directory }}/deploy/systemd/{{ app_name }}.service.j2"
        dest: /etc/systemd/system/{{ app_name }}-{{ version }}.service
      notify:
        - "reload systemctl daemon"
        - "restart gouda service"
    - name: Link gouda service
      file:
        src: /etc/systemd/system/{{ app_name }}-{{ version }}.service
        dest: /etc/systemd/system/{{ app_name }}.service
        state: link
        force: yes
      notify:
        - "reload systemctl daemon"
        - "restart gouda service"
  handlers:
    - name: Restart SSH
      listen: "restart sshd service"
      service:
        name: "sshd"
        state: restarted
        enabled: yes
    - name: Reload systemctl daemon
      shell: systemctl daemon-reload
      listen: "reload systemctl daemon"
    - name: Restart docker
      service:
        name: "docker"
        state: restarted
        enabled: yes
      listen: "restart docker service"
    - name: Restart gouda
      service:
        name: "{{ app_name }}"
        state: restarted
        enabled: yes
      listen: "restart gouda service"
